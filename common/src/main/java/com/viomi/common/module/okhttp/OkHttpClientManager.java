package com.viomi.common.module.okhttp;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Looper;
import android.widget.ImageView;


import com.google.gson.Gson;
import com.google.gson.internal.$Gson$Types;
import com.viomi.common.module.okhttp.progress.ProgressListener;

import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.net.FileNameMap;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by zhy on 15/8/17.
 */
public class OkHttpClientManager {
    //   private static final String TAG = "OkHttpClientManager";
    private static OkHttpClientManager mInstance;
    private OkHttpClient mOkHttpClient;
    private Handler mDelivery;
    private Gson mGson;

    private OkHttpClientManager() {
        mOkHttpClient = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .build();
        mDelivery = new Handler(Looper.getMainLooper());
        mGson = new Gson();
    }

    /***
     * 单例模式
     * @return
     */
    public static OkHttpClientManager getInstance() {
        if (mInstance == null) {
            synchronized (OkHttpClientManager.class) {
                if (mInstance == null) {
                    mInstance = new OkHttpClientManager();
                }
            }
        }
        return mInstance;
    }


    /*************对外公布的方法************/

    /***
     * 取消所有okhttp请求
     */
    public static void cancelAll() {
        getInstance()._cancelAll();
    }

    /**
     * 同步的Get请求
     *
     * @param url
     * @return 字符串
     */
    public static String getSync(String url) throws IOException {
        return getInstance()._getAsString(url);
    }

    /**
     * 异步的get请求
     *
     * @param url
     * @param callback 回调
     */
    public static Call getAsync(String url, ResultCallback callback) {
        return getInstance()._getAsync(url, callback);
    }

    /**
     * 异步的get请求带header
     *
     * @param url
     * @param callback 回调
     */
    public static Call getAsyncWithHeader(String url, Param[] params, ResultCallback callback) {
        return getInstance()._getAsyncWithHeader(url, params, callback);
    }

    /***
     * 同步的post请求
     * @param url
     * @param params  键值对
     * @return 字符串
     * @throws IOException
     */
    public static String postSync(String url, Param... params) throws IOException {
        return getInstance()._postAsString(url, params);
    }

    /***
     * 同步的post请求
     * @param url
     * @param json  json字符串
     * @return 字符串
     * @throws IOException
     */
    public static String postSync(String url, JSONObject json) throws IOException {
        return getInstance()._postAsString(url, json.toString());
    }

    /***
     * 异步post请求
     * @param url
     * @param callback
     * @param params 键值对
     */
    public static Call postAsync(String url, final ResultCallback callback, Param... params) {
        return getInstance()._postAsync(url, callback, params);
    }

    /***
     * 异步post请求
     * @param url
     * @param callback
     * @param params map集合
     */
    public static Call postAsync(String url, final ResultCallback callback, Map<String, String> params) {
        return getInstance()._postAsync(url, callback, params);
    }

    /***
     * 异步post请求
     * @param url
     * @param callback
     * @param json
     */
    public static Call postAsync(String url, final ResultCallback callback, JSONObject json) {
        return getInstance()._postAsync(url, callback, json.toString());
    }

    /***
     * 异步put请求
     * @param url
     * @param callback
     * @param json
     */
    public static Call putAsync(String url, JSONObject json, final ResultCallback callback) {
        return getInstance()._putAsync(url, callback, json.toString());
    }

    /***
     * 同步基于post的文件上传,多文件
     * @param url
     * @param files
     * @param fileKeys
     * @param params 键值对
     * @return
     * @throws IOException
     */
    public static Response postSync(String url, File[] files, String[] fileKeys, Param... params) throws IOException {
        return getInstance()._postSync(url, files, fileKeys, params);
    }

    /***
     * 同步基于post的文件上传,多文件,带监听进度
     * @param url
     * @param files
     * @param fileKeys
     * @param params 键值对
     * @return
     * @throws IOException
     */
    public static Response postSync(String url, File[] files, String[] fileKeys, ProgressListener progressListener, Param... params) throws IOException {
        return getInstance()._postSync(url, files, fileKeys, progressListener, params);
    }

    /***
     * 同步基于post的文件上传
     * @param url
     * @param file 单一文件
     * @param fileKey
     * @return
     * @throws IOException
     */
    public static Response postSync(String url, File file, String fileKey, Param... params) throws IOException {
        return getInstance()._postSync(url, file, fileKey, params);
    }

    /***
     * 异步基于post的文件上传
     * @param url
     * @param callback
     * @param files 多文件
     * @param fileKeys
     * @param params
     * @throws IOException
     */
    public static Call postAsync(String url, ResultCallback callback, File[] files, String[] fileKeys, Param... params) throws IOException {
        return getInstance()._postAsync(url, callback, files, fileKeys, params);
    }

    /***
     * 异步基于post的文件上传
     * @param url
     * @param callback
     * @param file 单文件
     * @param fileKey
     * @param params
     * @throws IOException
     */
    public static Call postAsync(String url, ResultCallback callback, File file, String fileKey, Param... params) throws IOException {
        return getInstance()._postAsync(url, callback, file, fileKey, params);
    }

    /***
     * 加载图片
     * @param view
     * @param url
     * @param errorResId
     * @throws IOException
     */
    public static Call displayImage(final ImageView view, String url, int errorResId) throws IOException {
        return getInstance()._displayImage(view, url, errorResId);
    }

    /**
     * 异步下载文件
     *
     * @param url
     * @param destDir  本地文件存储的文件夹
     * @param callback
     */
    public static Call downloadAsync(String url, String destDir, ResultCallback callback) {
        return getInstance()._downloadAsync(url, destDir, callback);
    }

    /**
     * 异步下载文件,带进度监听
     *
     * @param url
     * @param destDir          本地文件存储的文件夹
     * @param callback
     * @param progressListener
     */
    public static Call downloadAsync(String url, String destDir, ResultCallback callback, ProgressListener progressListener) {
        return getInstance()._downloadAsync(url, destDir, callback, progressListener);
    }

    /***
     * 连接前判断网络连通性
     * @param context
     */
    public static boolean isNetworkConnect(Context context) {
        return getInstance()._isNetworkConnect(context);
    }
    /****************************/


    /***
     * 连接前判断网络连通性
     * @param context
     */
    private boolean _isNetworkConnect(Context context) {
        if (context != null) {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
            if (networkInfo != null) {
                return networkInfo.isAvailable();
            }
        }
        return false;
    }


    /**
     * 取消所有操作
     * 除了带进度监听的请求外，因为带进度监听的是克隆了一份请求，要取消只能单个call取消
     */
    private void _cancelAll() {

        mOkHttpClient.dispatcher().cancelAll();
    }


    /**
     * 同步的Get请求
     *
     * @param url
     * @return Response
     */
    private Response _getAsync(String url) throws IOException {
        final Request request = new Request.Builder()
                .url(url)
                .build();
        Call call = mOkHttpClient.newCall(request);
        Response execute = call.execute();
        return execute;
    }

    /**
     * 同步的Get请求
     *
     * @param url
     * @return 字符串
     */
    private String _getAsString(String url) throws IOException {
        Response execute = _getAsync(url);
        return execute.body().string();
    }


    /**
     * 异步的get请求
     *
     * @param url
     * @param callback
     */
    private Call _getAsync(String url, final ResultCallback callback) {
        final Request request = new Request.Builder()
                .url(url)
                .build();
        return deliveryResult(callback, request);

    }

    /**
     * 异步的get请求,带header
     *
     * @param url
     * @param callback
     */
    private Call _getAsyncWithHeader(String url, Param[] params, final ResultCallback callback) {
        final Request request = new Request.Builder()
                .url(url)
                .header(params[0].key, params[0].value)
                .addHeader(params[1].key, params[1].value)
                .addHeader(params[2].key, params[2].value)
                .addHeader(params[3].key, params[3].value)
                .addHeader(params[4].key, params[4].value)
                .addHeader(params[5].key, params[5].value)
                .build();

        return deliveryResult(callback, request);

    }

    /**
     * 同步的Post请求
     *
     * @param url
     * @param json json字符串
     * @return
     */
    private Response _postSync(String url, String json) throws IOException {
        Request request = buildPostRequest(url, json);
        Response response = mOkHttpClient.newCall(request).execute();
        return response;

    }

    /**
     * 同步的Post请求
     *
     * @param url
     * @param params post的参数
     * @return
     */
    private Response _postSync(String url, Param... params) throws IOException {
        Request request = buildPostRequest(url, params);
        Response response = mOkHttpClient.newCall(request).execute();
        return response;
    }

    /**
     * 同步的Post请求
     *
     * @param url
     * @param params post的参数
     * @return 字符串
     */
    private String _postAsString(String url, Param... params) throws IOException {
        Response response = _postSync(url, params);
        return response.body().string();
    }

    /**
     * 同步的Post请求
     *
     * @param url
     * @param json
     * @return 字符串
     */
    private String _postAsString(String url, String json) throws IOException {
        Response response = _postSync(url, json);
        return response.body().string();
    }

    /**
     * 异步的post请求
     *
     * @param url
     * @param callback
     * @param json
     */
    private Call _postAsync(String url, final ResultCallback callback, String json) {
        Request request = buildPostRequest(url, json);
        return deliveryResult(callback, request);
    }

    /**
     * 异步的put请求
     *
     * @param url
     * @param callback
     * @param json
     */
    private Call _putAsync(String url, final ResultCallback callback, String json) {
        Request request = buildPutRequest(url, json);
        return deliveryResult(callback, request);
    }

    /**
     * 异步的post请求
     *
     * @param url
     * @param callback
     * @param params
     */
    private Call _postAsync(String url, final ResultCallback callback, Param... params) {
        Request request = buildPostRequest(url, params);
        return deliveryResult(callback, request);
    }

    /**
     * 异步的post请求
     *
     * @param url
     * @param callback
     * @param params
     */
    private Call _postAsync(String url, final ResultCallback callback, Map<String, String> params) {
        Param[] paramsArr = map2Params(params);
        Request request = buildPostRequest(url, paramsArr);
        return deliveryResult(callback, request);
    }

    /**
     * 同步基于post的文件上传 单个或多个文件
     *
     * @param params
     * @return
     */
    private Response _postSync(String url, File[] files, String[] fileKeys, Param... params) throws IOException {
        Request request = buildMultipartFormRequest(url, files, fileKeys, params);
        return mOkHttpClient.newCall(request).execute();
    }

    /**
     * 同步基于post的文件上传 单个或多个文件 带进度监听
     *
     * @param params
     * @return
     */
    private Response _postSync(String url, File[] files, String[] fileKeys, ProgressListener progressListener, Param... params) throws IOException {
        Request request = buildMultipartFormRequest(url, files, fileKeys, progressListener, params);
        return mOkHttpClient.newCall(request).execute();
    }

    /***
     * 同步基于post的文件上传 单个文件
     * @param url
     * @param file
     * @param fileKey
     * @param params
     * @return
     * @throws IOException
     */
    private Response _postSync(String url, File file, String fileKey, Param... params) throws IOException {
        Request request = buildMultipartFormRequest(url, new File[]{file}, new String[]{fileKey}, params);
        return mOkHttpClient.newCall(request).execute();
    }

    /**
     * 异步基于post的文件上传
     *
     * @param url
     * @param callback
     * @param files
     * @param fileKeys
     * @throws IOException
     */
    private Call _postAsync(String url, ResultCallback callback, File[] files, String[] fileKeys, Param... params) throws IOException {
        Request request = buildMultipartFormRequest(url, files, fileKeys, params);
        return deliveryResult(callback, request);
    }


    /**
     * 异步基于post的文件上传，单文件
     *
     * @param url
     * @param callback
     * @param file
     * @param fileKey
     * @param params
     * @throws IOException
     */
    private Call _postAsync(String url, ResultCallback callback, File file, String fileKey, Param... params) throws IOException {
        Request request = buildMultipartFormRequest(url, new File[]{file}, new String[]{fileKey}, params);
        return deliveryResult(callback, request);
    }

    /**
     * 异步下载文件
     *
     * @param url
     * @param destFileDir 本地文件存储的文件夹
     * @param callback
     */
    private Call _downloadAsync(final String url, final String destFileDir, final ResultCallback callback) {
        final Request request = new Request.Builder()
                .url(url)
                .build();
        final Call call = mOkHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                sendFailedStringCallback(call, e, callback);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                InputStream is = null;
                byte[] buf = new byte[2048];
                int len = 0;
                FileOutputStream fos = null;
                try {
                    is = response.body().byteStream();
                    File file = new File(destFileDir, getFileName(url));
                    fos = new FileOutputStream(file);
                    while ((len = is.read(buf)) != -1) {
                        fos.write(buf, 0, len);
                    }
                    fos.flush();
                    //如果下载文件成功，第一个参数为文件的绝对路径
                    sendSuccessResultCallback(file.getAbsolutePath(), callback);
                } catch (IOException e) {
                    sendFailedStringCallback(call, e, callback);
                } finally {
                    try {
                        if (is != null) is.close();
                    } catch (IOException e) {
                    }
                    try {
                        if (fos != null) fos.close();
                    } catch (IOException e) {
                    }
                }

            }
        });
        return call;
    }

    /**
     * 异步下载文件,带进度监听
     *
     * @param url
     * @param destFileDir      本地文件存储的文件夹
     * @param callback
     * @param progressListener
     */
    private Call _downloadAsync(final String url, final String destFileDir, final ResultCallback callback, ProgressListener progressListener) {
        final Request request = new Request.Builder()
                .url(url)
                .build();

        final Call call = ProgressHelper.addProgressResponseListener(mOkHttpClient, progressListener).newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                sendFailedStringCallback(call, e, callback);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                InputStream is = null;
                byte[] buf = new byte[2048];
                int len = 0;
                FileOutputStream fos = null;
                try {
                    is = response.body().byteStream();
                    File file = new File(destFileDir, getFileName(url));
                    fos = new FileOutputStream(file);
                    while ((len = is.read(buf)) != -1) {
                        fos.write(buf, 0, len);
                    }
                    fos.flush();
                    //如果下载文件成功，第一个参数为文件的绝对路径
                    sendSuccessResultCallback(file.getAbsolutePath(), callback);
                } catch (IOException e) {
                    sendFailedStringCallback(call, e, callback);
                } finally {
                    try {
                        if (is != null) is.close();
                    } catch (IOException e) {
                    }
                    try {
                        if (fos != null) fos.close();
                    } catch (IOException e) {
                    }
                }

            }
        });
        return call;
    }

    private String getFileName(String path) {
        int separatorIndex = path.lastIndexOf("/");
        return (separatorIndex < 0) ? path : path.substring(separatorIndex + 1, path.length());
    }

    /**
     * 加载图片
     *
     * @param view
     * @param url
     * @throws IOException
     */
    private Call _displayImage(final ImageView view, final String url, final int errorResId) {
        final Request request = new Request.Builder()
                .url(url)
                .build();
        Call call = mOkHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                setErrorResId(view, errorResId);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                InputStream is = null;
                try {
                    is = response.body().byteStream();
                    ImageUtils.ImageSize actualImageSize = ImageUtils.getImageSize(is);
                    ImageUtils.ImageSize imageViewSize = ImageUtils.getImageViewSize(view);
                    int inSampleSize = ImageUtils.calculateInSampleSize(actualImageSize, imageViewSize);
                    try {
                        is.reset();
                    } catch (IOException e) {
                        response = _getAsync(url);
                        is = response.body().byteStream();
                    }

                    BitmapFactory.Options ops = new BitmapFactory.Options();
                    ops.inJustDecodeBounds = false;
                    ops.inSampleSize = inSampleSize;
                    final Bitmap bm = BitmapFactory.decodeStream(is, null, ops);
                    mDelivery.post(new Runnable() {
                        @Override
                        public void run() {
                            view.setImageBitmap(bm);
                        }
                    });
                } catch (Exception e) {
                    setErrorResId(view, errorResId);

                } finally {
                    if (is != null) try {
                        is.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

        });
        return call;

    }

    private void setErrorResId(final ImageView view, final int errorResId) {
        mDelivery.post(new Runnable() {
            @Override
            public void run() {
                view.setImageResource(errorResId);
            }
        });
    }

    /***
     * 文件上传请求
     * @param url
     * @param files
     * @param fileKeys
     * @param params
     * @return
     */
    private Request buildMultipartFormRequest(String url, File[] files,
                                              String[] fileKeys, Param[] params) {
        params = validateParam(params);

        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);

        for (Param param : params) {
            builder.addPart(Headers.of("Content-Disposition", "form-data; name=\"" + param.key + "\""),
                    RequestBody.create(null, param.value));
        }
        if (files != null) {
            RequestBody fileBody = null;
            for (int i = 0; i < files.length; i++) {
                File file = files[i];
                String fileName = file.getName();
                fileBody = RequestBody.create(MediaType.parse(guessMimeType(fileName)), file);
                //TODO 根据文件名设置contentType
                builder.addPart(Headers.of("Content-Disposition",
                        "form-data; name=\"" + fileKeys[i] + "\"; filename=\"" + fileName + "\""),
                        fileBody);
            }
        }

        RequestBody requestBody = builder.build();
        return new Request.Builder()
                .url(url)
                .post(requestBody)
                .build();
    }

    /***
     * 带进度监听的文件上传请求
     * @param url
     * @param files
     * @param fileKeys
     * @param params
     * @param progressListener
     * @return
     */
    private Request buildMultipartFormRequest(String url, File[] files,
                                              String[] fileKeys, ProgressListener progressListener, Param[] params) {
        params = validateParam(params);

        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);

        for (Param param : params) {
            builder.addPart(Headers.of("Content-Disposition", "form-data; name=\"" + param.key + "\""),
                    RequestBody.create(null, param.value));
        }
        if (files != null) {
            RequestBody fileBody = null;
            for (int i = 0; i < files.length; i++) {
                File file = files[i];
                String fileName = file.getName();
                fileBody = RequestBody.create(MediaType.parse(guessMimeType(fileName)), file);
                //TODO 根据文件名设置contentType
                builder.addPart(Headers.of("Content-Disposition",
                        "form-data; name=\"" + fileKeys[i] + "\"; filename=\"" + fileName + "\""),
                        fileBody);
            }
        }

        RequestBody requestBody = builder.build();
        return new Request.Builder()
                .url(url)
                .post(ProgressHelper.addProgressRequestListener(requestBody, progressListener))
                .build();
    }

    private String guessMimeType(String path) {
        FileNameMap fileNameMap = URLConnection.getFileNameMap();
        String contentTypeFor = fileNameMap.getContentTypeFor(path);
        if (contentTypeFor == null) {
            contentTypeFor = "application/octet-stream";
        }
        return contentTypeFor;
    }


    private Param[] validateParam(Param[] params) {
        if (params == null)
            return new Param[0];
        else return params;
    }

    /***
     * map转键值对
     * @param params
     * @return
     */
    private Param[] map2Params(Map<String, String> params) {
        if (params == null) return new Param[0];
        int size = params.size();
        Param[] res = new Param[size];
        Set<Map.Entry<String, String>> entries = params.entrySet();
        int i = 0;
        for (Map.Entry<String, String> entry : entries) {
            res[i++] = new Param(entry.getKey(), entry.getValue());
        }
        return res;
    }

    private static final String SESSION_KEY = "Set-Cookie";
    private static final String mSessionKey = "JSESSIONID";

    private Map<String, String> mSessions = new HashMap<String, String>();

    private Call deliveryResult(final ResultCallback callback, Request request) {
        Call call = mOkHttpClient.newCall(request);
        call.enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                sendFailedStringCallback(call, e, callback);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try {
                    final String string = response.body().string();

                    if (callback.mType == String.class) {
                        sendSuccessResultCallback(string, callback);
                    } else {
                        Object o = mGson.fromJson(string, callback.mType);
                        sendSuccessResultCallback(o, callback);
                    }


                } catch (IOException e) {
                    sendFailedStringCallback(call, e, callback);
                } catch (com.google.gson.JsonParseException e)//Json解析的错误
                {
                    sendFailedStringCallback(call, e, callback);
                }
            }
        });
        return call;
    }

    private void sendFailedStringCallback(final Call call, final Exception e, final ResultCallback callback) {
        mDelivery.post(new Runnable() {
            @Override
            public void run() {
                if (callback != null)
                    callback.onError(call, e);
            }
        });
    }

    private void sendSuccessResultCallback(final Object object, final ResultCallback callback) {
        mDelivery.post(new Runnable() {
            @Override
            public void run() {
                if (callback != null) {
                    callback.onResponse(object);
                }
            }
        });
    }

    /***
     * 建立请求内容,键值对
     * @param url
     * @param params
     * @return
     */
    private Request buildPostRequest(String url, Param[] params) {
        if (params == null) {
            params = new Param[0];
        }
        FormBody.Builder formBody = new FormBody.Builder();
        for (Param param : params) {
            formBody.add(param.key, param.value);
        }
        RequestBody requestBody = formBody.build();
        return new Request.Builder()
                .url(url)
                .post(requestBody)
                .build();
    }

    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    private Request buildPostRequest(String url, String json) {
        if (json == null) {
            json = new JSONObject().toString();
        }
        RequestBody requestBody = RequestBody.create(JSON, json);
        return new Request.Builder()
                .url(url)
                .post(requestBody)
                .build();
    }

    private Request buildPutRequest(String url, String json) {
        if (json == null) {
            json = new JSONObject().toString();
        }
        RequestBody requestBody = RequestBody.create(JSON, json);
        return new Request.Builder()
                .url(url)
                .put(requestBody)
                .build();
    }


    public static abstract class ResultCallback<T> {
        Type mType;

        public ResultCallback() {
            mType = getSuperclassTypeParameter(getClass());
        }

        static Type getSuperclassTypeParameter(Class<?> subclass) {
            Type superclass = subclass.getGenericSuperclass();
            if (superclass instanceof Class) {
                throw new RuntimeException("Missing type parameter.");
            }
            ParameterizedType parameterized = (ParameterizedType) superclass;
            return $Gson$Types.canonicalize(parameterized.getActualTypeArguments()[0]);
        }

        public abstract void onError(Call call, Exception e);

        public abstract void onResponse(T response);
    }

    /***
     * 键值对
     * @author young2
     */
    public static class Param {
        public Param() {
        }

        public Param(String key, String value) {
            this.key = key;
            this.value = value;
        }

        public String key;
        public String value;
    }


}
