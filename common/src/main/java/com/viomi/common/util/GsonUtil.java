package com.viomi.common.util;

import android.util.Log;

import com.google.gson.Gson;

import java.lang.reflect.Type;

/**
 * Created by young2 on 2016/12/20.
 * 封装的GSON解析工具类，提供泛型参数
 */

public class GsonUtil {
    //将Json数据解析成相应的映射对象
    public static <T> T parseJsonWithGson(String jsonData, Class<T> type) {
        Gson gson = new Gson();
        T result = null;
        try {
            result = gson.fromJson(jsonData, type);
        } catch (Exception e) {
            Log.e("GsonUtil", "parseJsonWithGson Error!msg=" + e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

    //将Json数据解析成相应的映射对象
    public static <T> T parseJsonWithGson(String jsonData, Type type) {
        Gson gson = new Gson();
        T result = null;
        try {
            result = gson.fromJson(jsonData, type);
        } catch (Exception e) {
            Log.e("GsonUtil", "parseJsonWithGson Error!msg=" + e.getMessage());
            e.printStackTrace();
        }
        return result;
    }
}
