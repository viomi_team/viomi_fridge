package com.xiaomi.defined.action;

import com.xiaomi.defined.XiaomiDefined;
import com.xiaomi.defined.property.Power;
import com.xiaomi.defined.property.WifiLed;
import com.xiaomi.defined.property.Temp;
import com.xiaomi.miot.typedef.device.operable.ActionOperable;
import com.xiaomi.miot.typedef.urn.ActionType;

public class SetwifiLed extends ActionOperable {

    public static final ActionType TYPE = XiaomiDefined.Action.setwifiLed.toActionType();

    public SetwifiLed() {
        super(TYPE);

        super.addArgument(WifiLed.TYPE.toString());
    }
}