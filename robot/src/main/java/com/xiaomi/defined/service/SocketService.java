package com.xiaomi.defined.service;

import android.util.Log;

import com.xiaomi.defined.XiaomiDefined;
import com.xiaomi.defined.property.Power;
import com.xiaomi.defined.property.WifiLed;
import com.xiaomi.defined.property.Temp;
import com.xiaomi.defined.action.Set_test;
import com.xiaomi.defined.action.SetwifiLed;
import com.xiaomi.defined.action.Setpower;
import com.xiaomi.miot.typedef.data.DataValue;
import com.xiaomi.miot.typedef.data.value.Vbool;
import com.xiaomi.miot.typedef.data.value.Vfloat;
import com.xiaomi.miot.typedef.data.value.Vint;
import com.xiaomi.miot.typedef.data.value.Vstring;
import com.xiaomi.miot.typedef.data.value.Vuint8;
import com.xiaomi.miot.typedef.data.value.Vuint16;
import com.xiaomi.miot.typedef.data.value.Vuint32;
import com.xiaomi.miot.typedef.data.value.Vuint64;
import com.xiaomi.miot.typedef.device.Action;
import com.xiaomi.miot.typedef.device.ActionInfo;
import com.xiaomi.miot.typedef.device.operable.ServiceOperable;
import com.xiaomi.miot.typedef.error.MiotError;
import com.xiaomi.miot.typedef.property.Property;
import com.xiaomi.miot.typedef.urn.ServiceType;

public class SocketService extends ServiceOperable {

    public static final ServiceType TYPE = XiaomiDefined.Service.SocketService.toServiceType();
    private static final String TAG = "SocketService";

    public SocketService(boolean hasOptionalProperty) {
        super(TYPE);

        super.addProperty(new Power());
        super.addProperty(new WifiLed());
        super.addProperty(new Temp());

        if (hasOptionalProperty) {
        }

        super.addAction(new Set_test());
        super.addAction(new SetwifiLed());
        super.addAction(new Setpower());
    }

    /**
     * Properties
     */
    public Power power() {
        Property p = super.getProperty(Power.TYPE);
        if (p != null) {
            if (p instanceof Power) {
                return (Power) p;
            }
        }

        return null;
    }
    public WifiLed wifiLed() {
        Property p = super.getProperty(WifiLed.TYPE);
        if (p != null) {
            if (p instanceof WifiLed) {
                return (WifiLed) p;
            }
        }

        return null;
    }
    public Temp temp() {
        Property p = super.getProperty(Temp.TYPE);
        if (p != null) {
            if (p instanceof Temp) {
                return (Temp) p;
            }
        }

        return null;
    }

    /**
     * Actions
     */
    public Set_test set_test(){
        Action a = super.getAction(Set_test.TYPE);
        if (a != null) {
            if (a instanceof Set_test) {
                return (Set_test) a;
            }
        }

        return null;
    }
    public SetwifiLed setwifiLed(){
        Action a = super.getAction(SetwifiLed.TYPE);
        if (a != null) {
            if (a instanceof SetwifiLed) {
                return (SetwifiLed) a;
            }
        }

        return null;
    }
    public Setpower setpower(){
        Action a = super.getAction(Setpower.TYPE);
        if (a != null) {
            if (a instanceof Setpower) {
                return (Setpower) a;
            }
        }

        return null;
    }

    /**
     * PropertyGetter
     */
    public interface PropertyGetter {
        String getpower();

        String getwifiLed();

        int gettemp();

    }

    /**
     * PropertySetter
     */
    public interface PropertySetter {
        void setpower(String value);

        void setwifiLed(String value);

    }

    /**
     * ActionsHandler
     */
    public interface ActionHandler {
        void onset_test(String power, String wifiLed);
        void onsetwifiLed(String wifiLed);
        void onsetpower(String power);
    }


    private MiotError onset_test(ActionInfo action) {
        String power = ((Vstring) action.getArgumentValue(Power.TYPE)).getValue();
        String wifiLed = ((Vstring) action.getArgumentValue(WifiLed.TYPE)).getValue();
        actionHandler.onset_test(power, wifiLed);

        return MiotError.OK;
    }
    private MiotError onsetwifiLed(ActionInfo action) {
        String wifiLed = ((Vstring) action.getArgumentValue(WifiLed.TYPE)).getValue();
        actionHandler.onsetwifiLed(wifiLed);

        return MiotError.OK;
    }
    private MiotError onsetpower(ActionInfo action) {
        String power = ((Vstring) action.getArgumentValue(Power.TYPE)).getValue();
        actionHandler.onsetpower(power);

        return MiotError.OK;
    }

    /**
     * Handle actions invocation & properties operation
     */
    private ActionHandler actionHandler;
    private PropertyGetter propertyGetter;
    private PropertySetter propertySetter;

    public void setHandler(ActionHandler handler, PropertyGetter getter, PropertySetter setter) {
        actionHandler = handler;
        propertyGetter = getter;
        propertySetter = setter;
    }

    @Override
    public MiotError onSet(Property property) {
        Log.e(TAG, "onSet");

        if (propertySetter == null) {
            return super.onSet(property);
        }

        XiaomiDefined.Property p = XiaomiDefined.Property.valueOf(property.getDefinition().getType());
        switch (p) {
            case power:
                propertySetter.setpower(((Vstring) property.getCurrentValue()).getValue());
                break;
            case wifiLed:
                propertySetter.setwifiLed(((Vstring) property.getCurrentValue()).getValue());
                break;

            default:
                return MiotError.HAP_RESOURCE_NOT_EXIST;
        }

        return MiotError.OK;
    }

    @Override
    public MiotError onGet(Property property) {
        Log.e(TAG, "onGet");

        if (propertyGetter == null) {
            return super.onGet(property);
        }

        XiaomiDefined.Property p = XiaomiDefined.Property.valueOf(property.getDefinition().getType());
        switch (p) {
            case power:
                property.setValue(propertyGetter.getpower());
                break;
            case wifiLed:
                property.setValue(propertyGetter.getwifiLed());
                break;
            case temp:
                property.setValue(propertyGetter.gettemp());
                break;

            default:
                return MiotError.HAP_RESOURCE_NOT_EXIST;
        }

        return MiotError.OK;
    }

    @Override
    public MiotError onAction(ActionInfo action) {
        Log.e(TAG, "onAction: " + action.getType().toString());

        if (actionHandler == null) {
            return super.onAction(action);
        }

        XiaomiDefined.Action a = XiaomiDefined.Action.valueOf(action.getType());
        switch (a) {
            case set_test:
                return onset_test(action);
            case setwifiLed:
                return onsetwifiLed(action);
            case setpower:
                return onsetpower(action);

            default:
                Log.e(TAG, "invalid action: " + a);
                break;
        }

        return MiotError.HAP_RESOURCE_NOT_EXIST;
    }
}