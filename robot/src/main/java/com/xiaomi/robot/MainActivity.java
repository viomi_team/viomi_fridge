package com.xiaomi.robot;

import android.content.Context;
import android.net.DhcpInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.nuva.defined.device.Robot;
import com.nuva.defined.service.Speaker;

import com.xiaomi.miot.host.manager.MiotDeviceConfig;
import com.xiaomi.miot.host.manager.MiotHostManager;
import com.xiaomi.miot.typedef.data.value.Vint;
import com.xiaomi.miot.typedef.device.DiscoveryType;
import com.xiaomi.miot.typedef.error.MiotError;
import com.xiaomi.miot.typedef.exception.MiotException;
import com.xiaomi.miot.typedef.listener.CommandListener;
import com.xiaomi.miot.typedef.listener.CompletedListener;
import com.xiaomi.miot.typedef.listener.DeviceListener;
import com.xiaomi.miot.typedef.listener.SessionListener;
import com.xiaomi.miot.typedef.property.PropertySetter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    private Robot robot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        try {
//            initMiot();
//        } catch (MiotException e) {
//            Log.e(TAG,"initMiot fail!");
//            e.printStackTrace();
//        }
    }

    public void onButtonStart(View button ){
        try {
            initMiot();
        } catch (MiotException e) {
            Log.e(TAG,"initMiot fail!");
            e.printStackTrace();
        }
    }

    public void onButtonStop(View button){
        try {
            destroyMiot();
        } catch (MiotException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
//        try {
//            destroyMiot();
//        } catch (MiotException e) {
//            e.printStackTrace();
//        }

        super.onDestroy();
    }

    public void onButtonSendEvents(View view) {
        Log.e(TAG, "onButtonSendEvents");

        try {
            sendEvents();
        } catch (MiotException e) {
            e.printStackTrace();
        }
    }

    public void onButtonCreateSession(View button) {
        Log.e(TAG, "onButtonCreateSession");

        try {
            creatSession();
        } catch (MiotException e) {
            e.printStackTrace();
        }
    }

    private String theSessionId;

    private void creatSession() throws MiotException {
        JSONObject config = new JSONObject();

        try {
            config.put("user_ctx", "1");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        MiotHostManager.getInstance().createSession(config, new SessionListener() {
            @Override
            public void onSucceed(String sessionId, int expire) {
                /**
                 * expire: 此次回话的有效时间，单位为秒，超时之后再调用getDeviceList和invokeCommand会失败。
                 */
                Log.e(TAG, "createSession: " + sessionId + " expire: " + expire);
                theSessionId = sessionId;
            }

            @Override
            public void onFailed(MiotError error) {
                Log.e(TAG, "createSession: " + error.toString());
            }
        });
    }

    public void onButtonGetDeviceList(View button) {
        Log.e(TAG, "onButtonGetDeviceList");

        try {
            getDeviceList();
        } catch (MiotException e) {
            e.printStackTrace();
        }
    }

    private void getDeviceList() throws MiotException {
        MiotHostManager.getInstance().getDeviceList(theSessionId, new DeviceListener() {

            @Override
            public void onSucceed(String deviceList) {
                Log.e(TAG, "getDeviceList: " + deviceList);
            }

            @Override
            public void onFailed(MiotError error) {
                Log.e(TAG, "getDeviceList: " + error.toString());
            }
        });
    }

    public void onButtonInvokeCommand(View button) throws MiotException {
        Log.e(TAG, "onButtonInvokeCommand");

        int type = 3;

        /**
         *  {
         *       "did":"82418",        //找个已绑定在你账号下的空气净化器id
         *       "pd_id":34,
         *       "method":"common.power.set_power",
         *       "params":["on"] ​
         *  }
         */
        JSONObject cmd1 = new JSONObject();

        try {
            cmd1.put("did", "82418");
            cmd1.put("pd_id", 34);
            cmd1.put("method", "common.power.set_power");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JSONArray params = new JSONArray();
        params.put("on");

        try {
            cmd1.put("params", params);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        /**
         *  {
         *      "success":"已打开空气净化器",
         *      "fail":"打开空气净化器失败"
         *  }
         */
        JSONObject reply_txt = new JSONObject();

        try {
            reply_txt.put("success", "已打开空气净化器");
            reply_txt.put("fail", "打开空气净化器失败");
        } catch (JSONException e) {
            e.printStackTrace();
        }

//        MiotHostManager.getInstance().invokeCommand(theSessionId, type, cmd1, reply_txt,
//                new CommandListener() {
//                    @Override
//                    public void onSucceed(String txt, String extra_data) {
//                        Log.e(TAG, "invokeCommand: " + txt);
//                        Log.e(TAG, "extra_data: " + extra_data);
//                    }
//
//                    @Override
//                    public void onFailed(MiotError error) {
//                        Log.e(TAG, "invokeCommand: " + error.toString());
//                    }
//                });
    }

    private void initMiot() throws MiotException {
        MiotHostManager.getInstance().bind(this, new CompletedListener() {
            @Override
            public void onSucceed() {
                Log.d(TAG, "bind onSucceed");

                try {
                    MiotHostManager.getInstance().start();
                } catch (MiotException e) {
                    e.printStackTrace();
                }

//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        try {
//                            createDemoDevice();
//                        } catch (MiotException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                });
                try {
                    createDemoDevice();
                } catch (MiotException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailed(MiotError error) {
                Log.d(TAG, "bind failed: " + error);
            }
        });
    }

    private void destroyMiot() throws MiotException {
        MiotHostManager.getInstance().stop();
        MiotHostManager.getInstance().unbind(this);
    }

    private String getMiotInfo() {
        WifiManager wifiMng = (WifiManager) this.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        WifiInfo wi = wifiMng.getConnectionInfo();
        DhcpInfo di = wifiMng.getDhcpInfo();
        JSONObject jo = new JSONObject();

        try {
            jo.put("method", "_internal.info");
            jo.put("partner_id", "");
            JSONObject jop = new JSONObject();
            jop.put("hw_ver", "Linux");
            jop.put("fw_ver", "unknown");
            JSONObject jopa = new JSONObject();
            jopa.put("ssid", wi.getSSID());
            jopa.put("bssid", wi.getBSSID());
            jop.put("ap", jopa);
            JSONObject jopn = new JSONObject();
            jopn.put("localIp", IpUtils.intToIp(di.ipAddress));
            jopn.put("mask", IpUtils.intToIp(di.netmask));
            jopn.put("gw", IpUtils.intToIp(di.gateway));
            jop.put("netif", jopn);
            jop.put("uid", "888819681");
            jo.put("params", jop);
            return jo.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    public String getMacAddress() {
        Log.d(TAG, "getMacAddress");

        String macStr = null;
        WifiManager wifiManager = (WifiManager) this.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        if (wifiInfo.getMacAddress() != null) {
            macStr = wifiInfo.getMacAddress();
        } else {
            macStr = wifiInfo.getBSSID();
        }

        Log.d(TAG, "getMacAddress finished");

        return macStr;
    }

    private void createDemoDevice() throws MiotException {
        MiotDeviceInfo info = new MiotDeviceInfo();
        info.deviceId = "9991";
        info.macAddress = "8C:BE:BE:71:E1:45";
        info.miotToken = "BynbBPWqnHMqQV2y";
        info.miotInfo = getMiotInfo();

        try {
            initRobot(info);
        } catch (MiotException e) {
            e.printStackTrace();
        }
    }

    private void initRobot(MiotDeviceInfo info) throws MiotException {
        Log.d(TAG, "initRobot");

        /**
         * 1. Initialize Configuration
         */
        MiotDeviceConfig config = new MiotDeviceConfig();
        config.addDiscoveryType(DiscoveryType.MIOT);
        config.friendlyName("Ouyang-Robot");
        config.deviceId(info.deviceId);
        config.macAddress(info.macAddress);
        config.manufacturer("rokid");
        config.modelName("rokid.robot.alien");
        config.miotToken(info.miotToken);
        config.miotInfo(info.miotInfo);

        /**
         * 2. Create Robot
         */
        robot = new Robot(config);

        /**
         * 3. set Action Handler, setter & getter for property
         */
        robot.Speaker().setHandler(
                new Speaker.ActionHandler() {
                    @Override
                    public void onPlay(String source, int position, Speaker.PlayResult result) {
                        Log.d(TAG, "onPlay: " + source + " " + position);
                        result.Playing = true;
                    }
                },
                new Speaker.PropertyGetter() {
                    @Override
                    public String getAudioSource() {
                        Log.d(TAG, "getAudioSource");
                        return "";
                    }

                    @Override
                    public int getAudioPosition() {
                        Log.d(TAG, "getAudioPosition");
                        return 10;
                    }

                    @Override
                    public boolean getPlaying() {
                        Log.d(TAG, "getPlaying");
                        return false;
                    }
                },
                new Speaker.PropertySetter() {
                    @Override
                    public void setAudioSource(String value) {
                        Log.d(TAG, "setAudioSource: " + value);
                    }

                    @Override
                    public void setPosition(int value) {
                        Log.d(TAG, "setPosition: " + value);
                    }

                    @Override
                    public void setPlaying(boolean value) {
                        Log.d(TAG, "setPlaying: " + value);
                    }
                }
        );

        /**
         * 4. Start
         */
        robot.start(new CompletedListener() {
            @Override
            public void onSucceed() {
                Log.d(TAG, "start onSucceed");
            }

            @Override
            public void onFailed(MiotError error) {
                Log.d(TAG, "start onFailed: " + error);
            }
        });
    }

    /**
     * 5. update properties
     */
    private void sendEvents() throws MiotException {
        robot.Speaker().Playing().setValue(false);
        robot.Speaker().AudioSource().setValue("hello");
        robot.Speaker().AudioPosition().setValue(17);
        robot.sendEvents();
    }
}