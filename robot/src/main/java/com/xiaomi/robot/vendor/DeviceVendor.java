package com.xiaomi.robot.vendor;

import android.util.Log;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Headers;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class DeviceVendor {

    private static final String TAG = "DeviceVendor";

    private DeviceVendor() {
    }

    public interface DeviceInfoListener {
        void onSucceed(DeviceInfo info);
        void onFailed();
    }

    public static void getDeviceInfo(final DeviceInfoListener listener) throws IOException {
        Log.d(TAG, "getDeviceInfo");

        String url = "http://120.24.83.5:8002/api/user/rokidinfo";
        OkHttpClient client = new OkHttpClient();
        Request.Builder builder = new Request.Builder();
        Request request = builder.url(url).build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
                listener.onFailed();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

                Headers responseHeaders = response.headers();
                for (int i = 0; i < responseHeaders.size(); i++) {
                    Log.d(TAG, responseHeaders.name(i) + ": " + responseHeaders.value(i));
                }

                String result = response.body().string();
                Log.d(TAG, "async reponse : " + result);

                listener.onSucceed(DeviceInfo.valueOf(result));
            }
        });
    }
}
