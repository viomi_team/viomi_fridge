package com.xiaomi.robot.vendor;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

public class DeviceInfo {

    private String sn;
    private String token; // from rokid server: 用来检查 rokid 合法性的参数, 一分钟更新一次
    private String bssid;
    private String mac;
    private String ip;

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getBssid() {
        return bssid;
    }

    public void setBssid(String bssid) {
        this.bssid = bssid;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public static DeviceInfo valueOf(String info) {
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(info);
        String payload = je.getAsJsonObject().get("payload").getAsString();

        Gson gson = new GsonBuilder().enableComplexMapKeySerialization().create();

        return gson.fromJson(payload, DeviceInfo.class);
    }
}
