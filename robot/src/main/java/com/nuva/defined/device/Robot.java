package com.nuva.defined.device;

import android.util.Log;

import com.nuva.defined.service.RobotInformation;
import com.nuva.defined.service.Speaker;
import com.xiaomi.miot.host.manager.MiotDeviceConfig;
import com.xiaomi.miot.host.manager.MiotHostManager;
import com.xiaomi.miot.typedef.device.operable.DeviceOperable;
import com.xiaomi.miot.typedef.exception.MiotException;
import com.xiaomi.miot.typedef.listener.CompletedListener;
import com.xiaomi.miot.typedef.property.Property;
import com.xiaomi.miot.typedef.urn.DeviceType;

import java.util.List;

public class Robot extends DeviceOperable {

    private static final DeviceType DEVICE_TYPE = new DeviceType("Ouyang", "Robot", "1");
    private static final String TAG = "Robot";

    private RobotInformation _RobotInformation = new RobotInformation(false);
    private Speaker _Speaker = new Speaker(false);

    public Robot(MiotDeviceConfig config) {
        super(DEVICE_TYPE);
        super.setDiscoveryTypes(config.discoveryTypes());
        super.setFriendlyName(config.friendlyName());
        super.setDeviceId(config.deviceId());
        super.setMacAddress(config.macAddress());
        super.setManufacturer(config.manufacturer());
        super.setModelName(config.modelName());
        super.setMiotToken(config.miotToken());
        super.setMiotInfo(config.miotInfo());
        super.addService(_RobotInformation);
        super.addService(_Speaker);
        super.initializeInstanceID();
    }

    public RobotInformation RobotInformation() {
        return _RobotInformation;
    }

    public Speaker Speaker() {
        return _Speaker;
    }

    public void start(CompletedListener listener) throws MiotException {
        MiotHostManager.getInstance().register(this, listener, this);
    }

    public void stop(CompletedListener listener) throws MiotException {
        MiotHostManager.getInstance().unregister(this, listener);
    }

    public void sendEvents() throws MiotException {
        MiotHostManager.getInstance().sendEvent(super.getChangedProperties());
    }
}