package com.nuva.defined.property;

import com.nuva.defined.NuvaDefined;
import com.xiaomi.miot.typedef.data.DataType;
import com.xiaomi.miot.typedef.data.value.Vint;
import com.xiaomi.miot.typedef.property.AccessType;
import com.xiaomi.miot.typedef.property.PropertyDefinition;
import com.xiaomi.miot.typedef.property.PropertyOperable;
import com.xiaomi.miot.typedef.urn.PropertyType;

/**
 * {
 *     "type": "urn:nuva:property:AudioPosition:0000",
 *     "description": "位置",
 *     "access": ["get", "set"],
 *     "dataType": "int",
 * }
 */
public class AudioPosition extends PropertyOperable<Vint> {

    public static PropertyType TYPE = NuvaDefined.Property.AudioPosition.toPropertyType();

    private static AccessType PERMISSIONS = AccessType.valueOf(AccessType.GET | AccessType.SET | AccessType.NOTIFY);
    private static DataType FORMAT = DataType.INT;

    public AudioPosition() {
        super(new PropertyDefinition(TYPE, PERMISSIONS, FORMAT));
    }

    public int getValue() {
        return ((Vint) super.getCurrentValue()).getValue();
    }

    public void setValue(int value) {
        super.setDataValue(new Vint(value));
    }
}