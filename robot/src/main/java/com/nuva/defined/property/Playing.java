package com.nuva.defined.property;

import com.nuva.defined.NuvaDefined;
import com.xiaomi.miot.typedef.data.DataType;
import com.xiaomi.miot.typedef.data.value.Vbool;
import com.xiaomi.miot.typedef.property.AccessType;
import com.xiaomi.miot.typedef.property.PropertyDefinition;
import com.xiaomi.miot.typedef.property.PropertyOperable;
import com.xiaomi.miot.typedef.urn.PropertyType;

/**
 * {
 *     "type": "urn:nuva-defined:property:Playing:0007",
 *     "description": "状态",
 *     "access": ["get"],
 *     "dataType": "bool",
 * }
 */
public class Playing extends PropertyOperable<Vbool> {

    public static PropertyType TYPE = NuvaDefined.Property.Playing.toPropertyType();

    private static AccessType PERMISSIONS = AccessType.valueOf(AccessType.GET | AccessType.NOTIFY);
    private static DataType FORMAT = DataType.BOOL;

    public Playing() {
        super(new PropertyDefinition(TYPE, PERMISSIONS, FORMAT));
    }

    public boolean getValue() {
        return ((Vbool) super.getCurrentValue()).getValue();
    }

    public void setValue(boolean value) {
        super.setDataValue(new Vbool(value));
    }
}