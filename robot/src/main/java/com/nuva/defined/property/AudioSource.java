package com.nuva.defined.property;

import com.nuva.defined.NuvaDefined;
import com.xiaomi.miot.typedef.data.DataType;
import com.xiaomi.miot.typedef.data.value.Vstring;
import com.xiaomi.miot.typedef.property.AccessType;
import com.xiaomi.miot.typedef.property.PropertyDefinition;
import com.xiaomi.miot.typedef.property.PropertyOperable;
import com.xiaomi.miot.typedef.urn.PropertyType;

/**
 * {
 *     "type": "urn:nuva-defined:property:AudioSource:0005",
 *     "description": "音乐源",
 *     "access": ["get", "set"],
 *     "dataType": "string",
 * }
 */
public class AudioSource extends PropertyOperable<Vstring> {

    public static PropertyType TYPE = NuvaDefined.Property.AudioSource.toPropertyType();

    private static AccessType PERMISSIONS = AccessType.valueOf(AccessType.GET);
    private static DataType FORMAT = DataType.STRING;

    public AudioSource() {
        super(new PropertyDefinition(TYPE, PERMISSIONS, FORMAT));
    }

    public String getValue() {
        return ((Vstring) super.getCurrentValue()).getValue();
    }

    public void setValue(String value) {
        super.setDataValue(new Vstring(value));
    }
}