package com.nuva.defined;

import android.util.Log;

import com.xiaomi.miot.typedef.urn.ActionType;
import com.xiaomi.miot.typedef.urn.PropertyType;
import com.xiaomi.miot.typedef.urn.ServiceType;

public class NuvaDefined {

    private static final String TAG = "NuvaDefined";
    private static final String DOMAIN = "nuva";
    private static final String _UUID = "-0000-1000-2000-000000AABBCC";

    private NuvaDefined() {
    }

    /**
     * Properties
     * 00000001-0000-1000-2000-000000AABBCC
     * 00000002-0000-1000-2000-000000AABBCC
     * 00000003-0000-1000-2000-000000AABBCC
     * ...
     */
    public enum Property {
        Undefined(0),
        Name(1),
        Manufacturer(2),
        Model(3),
        SerialNumber(4),
        AudioSource(5),
        AudioPosition(6),
        Playing(7);

        private int value;

        Property(int value) {
            this.value = value;
        }

        public static Property valueOf(int value) {
            for (Property c : values()) {
                if (c.value() == value) {
                    return c;
                }
            }

            Log.e(TAG, "invalid value: " + value);

            return Undefined;
        }

        public static Property valueOf(PropertyType type) {
            if (! type.getDomain().equals(DOMAIN)) {
                return Undefined;
            }

            for (Property c : values()) {
                if (c.toString().equals(type.getSubType())) {
                    return c;
                }
            }

            return Undefined;
        }

        public int value() {
            return value;
        }

        public PropertyType toPropertyType() {
            return new PropertyType(DOMAIN, this.toString(), toShortUUID());
        }

        public String toUUID() {
            return String.format("%08X%s", value, _UUID);
        }

        public String toShortUUID() {
            return String.format("%04X", value);
        }
    }

    /**
     * Actions
     * 00001001-0000-1000-2000-000000AABBCC
     * 00001002-0000-1000-2000-000000AABBCC
     * 00001003-0000-1000-2000-000000AABBCC
     * ...
     */
    public enum Action {
        Undefined(0x0),
        Play(0x1001);

        private int value;

        Action(int value) {
            this.value = value;
        }

        public static Action valueOf(int value) {
            for (Action c : values()) {
                if (c.value() == value) {
                    return c;
                }
            }

            Log.e(TAG, "invalid value: " + value);

            return Undefined;
        }

        public static Action valueOf(ActionType type) {
            if (! type.getDomain().equals(DOMAIN)) {
                return Undefined;
            }

            for (Action v : values()) {
                if (v.toString().equals(type.getSubType())) {
                    return v;
                }
            }

            return Undefined;
        }

        public int value() {
            return value;
        }

        public ActionType toActionType() {
            return new ActionType(DOMAIN, this.toString(), toShortUUID());
        }

        public String toUUID() {
            return String.format("%08X%s", value, _UUID);
        }

        public String toShortUUID() {
            return String.format("%04X", value);
        }
    }

    /**
     * Servics
     * 00002001-0000-1000-2000-000000AABBCC
     * 00002002-0000-1000-2000-000000AABBCC
     * 00002003-0000-1000-2000-000000AABBCC
     * ...
     */
    public enum Service {
        Undefined(0x0),
        RobotInformation(0x2001),
        Speaker(0x2001);

        private int value;

        Service(int value) {
            this.value = value;
        }

        public static Service valueOf(int value) {
            for (Service c : values()) {
                if (c.value() == value) {
                    return c;
                }
            }

            Log.e(TAG, "invalid value: " + value);

            return Undefined;
        }

        public static Service valueOf(ServiceType type) {
            if (! type.getDomain().equals(DOMAIN)) {
                return Undefined;
            }

            for (Service v : values()) {
                if (v.toString().equals(type.getSubType())) {
                    return v;
                }
            }

            return Undefined;
        }

        public int value() {
            return value;
        }

        public ServiceType toServiceType() {
            return new ServiceType(DOMAIN, this.toString(), toShortUUID());
        }

        public String toUUID() {
            return String.format("%08X%s", value, _UUID);
        }

        public String toShortUUID() {
            return String.format("%04X", value);
        }
    }
}