package com.nuva.defined.service;

import com.nuva.defined.NuvaDefined;
import com.nuva.defined.property.Manufacturer;
import com.nuva.defined.property.Model;
import com.nuva.defined.property.Name;
import com.nuva.defined.property.SerialNumber;
import com.xiaomi.miot.typedef.device.operable.ServiceOperable;
import com.xiaomi.miot.typedef.property.Property;
import com.xiaomi.miot.typedef.urn.ServiceType;

public class RobotInformation extends ServiceOperable {

    public static final ServiceType TYPE = NuvaDefined.Service.RobotInformation.toServiceType();

    public RobotInformation(boolean hasOptionalCharacteristics) {
        super(TYPE);

        super.addProperty(new Name());
        super.addProperty(new Model());
        super.addProperty(new Manufacturer());
        super.addProperty(new SerialNumber());

        if (hasOptionalCharacteristics) {
        }
    }

    public Name Name() {
        Property p = super.getProperty(Name.TYPE);
        if (p != null) {
            if (p instanceof Name) {
                return (Name) p;
            }
        }

        return null;
    }

    public Model Model() {
        Property p = super.getProperty(Model.TYPE);
        if (p != null) {
            if (p instanceof Model) {
                return (Model) p;
            }
        }

        return null;
    }

    public Manufacturer Manufacturer() {
        Property p = super.getProperty(Manufacturer.TYPE);
        if (p != null) {
            if (p instanceof Manufacturer) {
                return (Manufacturer) p;
            }
        }

        return null;
    }

    public SerialNumber SerialNumber() {
        Property p = super.getProperty(SerialNumber.TYPE);
        if (p != null) {
            if (p instanceof SerialNumber) {
                return (SerialNumber) p;
            }
        }

        return null;
    }
//
//    public MiotError onSet(Property property) {
//        NuvaDefined.Property p = NuvaDefined.Property.valueOf(property.getDefinition().getMethod());
//        switch (p) {
//            case Name:
//                return Name().onSet((Vstring) property.getCurrentValue());
//
//            case Model:
//                return Model().onSet((Vstring) property.getCurrentValue());
//
//            case Manufacturer:
//                return Manufacturer().onSet((Vstring) property.getCurrentValue());
//
//            case SerialNumber:
//                return SerialNumber().onSet((Vstring) property.getCurrentValue());
//        }
//
//        return MiotError.HAP_RESOURCE_NOT_EXIST;
//    }
//
//    public MiotError onGet(Property property) {
//        NuvaDefined.Property p = NuvaDefined.Property.valueOf(property.getDefinition().getMethod());
//        switch (p) {
//            case Name:
//                return Name().onGet(property.getPropertyValue());
//
//            case Model:
//                return Model().onGet(property.getPropertyValue());
//
//            case Manufacturer:
//                return Manufacturer().onGet(property.getPropertyValue());
//
//            case SerialNumber:
//                return SerialNumber().onGet(property.getPropertyValue());
//        }
//
//        return MiotError.HAP_RESOURCE_NOT_EXIST;
//    }
}