package com.nuva.defined.service;

import android.util.Log;

import com.nuva.defined.NuvaDefined;
import com.nuva.defined.action.Play;
import com.nuva.defined.property.AudioPosition;
import com.nuva.defined.property.AudioSource;
import com.nuva.defined.property.Playing;
import com.xiaomi.miot.typedef.data.DataValue;
import com.xiaomi.miot.typedef.data.value.Vbool;
import com.xiaomi.miot.typedef.data.value.Vint;
import com.xiaomi.miot.typedef.data.value.Vstring;
import com.xiaomi.miot.typedef.device.Action;
import com.xiaomi.miot.typedef.device.ActionInfo;
import com.xiaomi.miot.typedef.device.operable.ServiceOperable;
import com.xiaomi.miot.typedef.error.MiotError;
import com.xiaomi.miot.typedef.property.Property;
import com.xiaomi.miot.typedef.urn.ServiceType;

public class Speaker extends ServiceOperable {

    public static final ServiceType TYPE = NuvaDefined.Service.Speaker.toServiceType();
    private static final String TAG = "Speaker";

    public Speaker(boolean hasOptionalProperty) {
        super(TYPE);

        super.addProperty(new AudioSource());
        super.addProperty(new AudioPosition());
        super.addProperty(new Playing());

        if (hasOptionalProperty) {
        }

        super.addAction(new Play());
    }

    /**
     * Properties (3)
     */
    public AudioSource AudioSource() {
        Property p = super.getProperty(AudioSource.TYPE);
        if (p != null) {
            if (p instanceof AudioSource) {
                return (AudioSource) p;
            }
        }

        return null;
    }

    public AudioPosition AudioPosition() {
        Property p = super.getProperty(AudioPosition.TYPE);
        if (p != null) {
            if (p instanceof AudioPosition) {
                return (AudioPosition) p;
            }
        }

        return null;
    }

    public Playing Playing() {
        Property p = super.getProperty(Playing.TYPE);
        if (p != null) {
            if (p instanceof Playing) {
                return (Playing) p;
            }
        }

        return null;
    }

    /**
     * Actions (1)
     */
    public Play Play() {
        Action a = super.getAction(Play.TYPE);
        if (a != null) {
            if (a instanceof Play) {
                return (Play) a;
            }
        }

        return null;
    }

    /**
     * PropertyGetter (2)
     */
    public interface PropertyGetter {
        String getAudioSource();

        int getAudioPosition();

        boolean getPlaying();
    }

    /**
     * PropertySetter (1)
     */
    public interface PropertySetter {
        void setAudioSource(String value);

        void setPosition(int value);

        void setPlaying(boolean value);
    }

    /**
     * ActionsHandler (1)
     */
    public interface ActionHandler {
        void onPlay(String source, int position, PlayResult result);
    }

    public class PlayResult {
        public boolean Playing;
    }

    private MiotError onPlay(ActionInfo action) {
        PlayResult result = new PlayResult();

        String source = ((Vstring) action.getArgumentValue(AudioSource.TYPE)).getValue();
        int position = ((Vint) action.getArgumentValue(AudioPosition.TYPE)).getValue();

        actionHandler.onPlay(source, position, result);

        if (!action.getResult(Playing.TYPE).setValue(result.Playing)) {
            return MiotError.INTERNAL_INVALID_ARGS;
        }

        return MiotError.OK;
    }

    /**
     * Handle actions invocation & properties operation
     */
    private ActionHandler actionHandler;
    private PropertyGetter propertyGetter;
    private PropertySetter propertySetter;

    public void setHandler(ActionHandler handler, PropertyGetter getter, PropertySetter setter) {
        actionHandler = handler;
        propertyGetter = getter;
        propertySetter = setter;
    }

    @Override
    public MiotError onSet(Property property) {
        Log.e(TAG, "onSet");

        if (propertySetter == null) {
            return super.onSet(property);
        }

        NuvaDefined.Property p = NuvaDefined.Property.valueOf(property.getDefinition().getType());
        switch (p) {
            case AudioSource:
                propertySetter.setAudioSource(((Vstring) property.getCurrentValue()).getValue());
                break;

            case AudioPosition:
                propertySetter.setPosition(((Vint) property.getCurrentValue()).getValue());
                break;

            case Playing:
                propertySetter.setPlaying(((Vbool) property.getCurrentValue()).getValue());
                break;

            default:
                return MiotError.HAP_RESOURCE_NOT_EXIST;
        }

        return MiotError.OK;
    }

    @Override
    public MiotError onGet(Property property) {
        Log.e(TAG, "onGet");

        if (propertyGetter == null) {
            return super.onGet(property);
        }

        NuvaDefined.Property p = NuvaDefined.Property.valueOf(property.getDefinition().getType());
        switch (p) {
            case AudioSource:
                property.setValue(propertyGetter.getAudioSource());
                break;

            case AudioPosition:
                property.setValue(propertyGetter.getAudioPosition());
                break;

            case Playing:
                property.setValue(propertyGetter.getPlaying());
                break;

            default:
                return MiotError.HAP_RESOURCE_NOT_EXIST;
        }

        return MiotError.OK;
    }

    @Override
    public MiotError onAction(ActionInfo action) {
        Log.e(TAG, "onAction: " + action.getType().toString());

        if (actionHandler == null) {
            return super.onAction(action);
        }

        NuvaDefined.Action a = NuvaDefined.Action.valueOf(action.getType());
        switch (a) {
            case Play:
                return onPlay(action);

            default:
                Log.e(TAG, "invalid action: " + a);
                break;
        }

        return MiotError.HAP_RESOURCE_NOT_EXIST;
    }
}