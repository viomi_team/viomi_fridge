package com.nuva.defined.action;

import com.nuva.defined.NuvaDefined;
import com.nuva.defined.property.AudioPosition;
import com.nuva.defined.property.AudioSource;
import com.nuva.defined.property.Playing;
import com.xiaomi.miot.typedef.device.operable.ActionOperable;
import com.xiaomi.miot.typedef.urn.ActionType;

/**
 * {
 *     "type": "urn:nuva:action:Play:0000",
 *     "description": "播放",
 *     "args": {
 *         "in": ["urn:miot-org:property:AudioSource:0000",
 *                "urn:miot-org:property:AudioPosition:0000"],
 *         "out": ["urn:miot-org:property:Playing:0000"],
 *     }
 * }
 */
public class Play extends ActionOperable {

    public static final ActionType TYPE = NuvaDefined.Action.Play.toActionType();

    public Play() {
        super(TYPE);

        super.addArgument(AudioSource.TYPE.toString());
        super.addArgument(AudioPosition.TYPE.toString());
        super.addResult(Playing.TYPE.toString());
    }
}