package com.viomi.defined.action;

import com.viomi.defined.ViomiDefined;
import com.viomi.defined.property.Mode;
import com.viomi.defined.property.RCSetTemp;
import com.viomi.defined.property.RCMinTemp;
import com.viomi.defined.property.RCMaxTemp;
import com.viomi.defined.property.CCSetTemp;
import com.viomi.defined.property.CCMinTemp;
import com.viomi.defined.property.CCMaxTemp;
import com.viomi.defined.property.FCSetTemp;
import com.viomi.defined.property.FCMinTemp;
import com.viomi.defined.property.FCMaxTemp;
import com.viomi.defined.property.RCRealTemp;
import com.viomi.defined.property.CCRealTemp;
import com.viomi.defined.property.FCRealTemp;
import com.viomi.defined.property.RCSet;
import com.viomi.defined.property.CCSet;
import com.viomi.defined.property.OneKeyClean;
import com.viomi.defined.property.FilterLifeBase;
import com.viomi.defined.property.FilterLife;
import com.viomi.defined.property.SceneAll;
import com.viomi.defined.property.SceneChoose;
import com.viomi.defined.property.SceneName;
import com.viomi.defined.property.OutdoorTemp;
import com.viomi.defined.property.IndoorTemp;
import com.viomi.defined.property.StartDays;
import com.viomi.defined.property.Error;
import com.viomi.defined.property.VoiceEnable;
import com.viomi.defined.property.Wakeup;
import com.viomi.defined.property.LightUpScreen;
import com.viomi.defined.property.Weather;
import com.viomi.defined.property.PlayMusic;
import com.viomi.defined.property.PlayLightMusic;
import com.viomi.defined.property.StopMusic;
import com.viomi.defined.property.SmartCool;
import com.viomi.defined.property.SmartFreeze;
import com.viomi.defined.property.CoolBeverage;
import com.viomi.defined.property.ComReceiveData;
import com.viomi.defined.property.SceneMode;
import com.viomi.defined.property.ScreenStatus;
import com.viomi.defined.property.WaterAlarm;
import com.viomi.defined.property.SmokeAlarm;
import com.viomi.defined.property.InvadeAlarm;
import com.viomi.defined.property.GasLeakAlarm;
import com.viomi.defined.property.IpAddr;
import com.xiaomi.miot.typedef.device.operable.ActionOperable;
import com.xiaomi.miot.typedef.urn.ActionType;

public class SetMode extends ActionOperable {

    public static final ActionType TYPE = ViomiDefined.Action.setMode.toActionType();

    public SetMode() {
        super(TYPE);

        super.addArgument(Mode.TYPE.toString());
    }
}