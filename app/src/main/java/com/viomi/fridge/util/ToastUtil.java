package com.viomi.fridge.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;

import com.viomi.fridge.common.ViomiApplication;

/**
 * Created by viomi on 2016/10/24.
 * toast封装
 */

public class ToastUtil {
    private static Toast toast = null;
    private static Toast mToastCenter = null;
    private static Toast mToastDefined = null;
    private static TextView mTextView;

    @SuppressLint("ShowToast")
    public static void showCenter(Context context, String text) {
        if (mToastCenter == null) {
            mToastCenter = Toast.makeText(context, text, Toast.LENGTH_SHORT);
            mToastCenter.setGravity(Gravity.CENTER, 0, 0);
        } else {
            mToastCenter.setText(text);
        }
        mToastCenter.show();
    }

    public static void show(String result) {
        if (toast == null) {
            toast = Toast.makeText(ViomiApplication.getContext(), result, Toast.LENGTH_SHORT);
        } else {
            toast.setText(result);
        }
        toast.show();
    }

    public static void cancel() {
        if (toast != null) toast.cancel();
        if (mToastCenter != null) mToastCenter.cancel();
    }
}
