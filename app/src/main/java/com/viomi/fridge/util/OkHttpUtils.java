package com.viomi.fridge.util;

import android.os.Handler;

import com.google.gson.Gson;

import org.android.agoo.common.CallBack;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.FileNameMap;
import java.net.URLConnection;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


public class OkHttpUtils {
    public static volatile OkHttpUtils instance;
    private OkHttpClient client;
    Handler handler = new Handler();

    private OkHttpUtils() {
        client = getUnsafeOkHttpClient().newBuilder()
                //设置最长读写时间
                .readTimeout(100000, TimeUnit.SECONDS)
                .writeTimeout(100000, TimeUnit.SECONDS)
                .connectTimeout(100000, TimeUnit.SECONDS).build();
    }

    public static OkHttpUtils getInstance() {
        if (instance == null) {
            synchronized (OkHttpUtils.class) {
                if (instance == null) {
                    instance = new OkHttpUtils();
                }
            }
        }
        return instance;
    }

    private String guessMimeType(String path) {
        FileNameMap fileNameMap = URLConnection.getFileNameMap();
        String contentTypeFor = fileNameMap.getContentTypeFor(path);
        if (contentTypeFor == null) {
            contentTypeFor = "application/octet-stream";
        }
        return contentTypeFor;
    }

    public void uploadFile(String url,  File file, final CallBack callBack) {
        MultipartBody.Builder multipartBody = new MultipartBody.Builder();
        //form 表单上传
        multipartBody.setType(MultipartBody.FORM);
        //拼接参数
//        for (String key : paramsMap.keySet()) {
//            Object object = paramsMap.get(key);
//            if (object instanceof String) {
//                multipartBody.addFormDataPart(key, object.toString());
//            } else if (object instanceof File) {
//                File file = (File) object;
////                multipartBody.addFormDataPart(key,file.getName(),MultipartBody.create(MediaType.parse("multipart/form-data"),file));
//                String fileName = file.getName();
//                RequestBody fileBody = RequestBody.create(MediaType.parse(guessMimeType(fileName)), file);
//                //TODO 根据文件名设置contentType
//                multipartBody.addPart(Headers.of("Content-Disposition",
//                        "form-data; name=\"" + "file" + "\"; filename=\"" + fileName + "\""),
//                        fileBody);
//            }
//        }
        String fileName = file.getName();
        RequestBody fileBody = RequestBody.create(MediaType.parse(guessMimeType(fileName)), file);
        //TODO 根据文件名设置contentType
        multipartBody.addPart(Headers.of("Content-Disposition",
                "form-data; name=\"" + "file" + "\"; filename=\"" + fileName + "\""),
                fileBody);
        RequestBody requestBody = multipartBody.build();
        //创建Request对象
        Request request = new Request.Builder().url(url).post(requestBody).build();
        client.newCall(request)
                .enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, final IOException e) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                String msg = e.getMessage();
                                callBack.onFailure("上传失败!",msg);
                            }
                        });
                    }

                    @Override
                    public void onResponse(Call call, final Response response) throws IOException {
//                        final String str = response.body().string();
                        if (response.code() == 200) {
                            //解析
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callBack.onSuccess();
                                }
                            });
                        }else {
                            callBack.onFailure("上传失败!","");
                        }
                    }
                });
    }


    private static OkHttpClient getUnsafeOkHttpClient() {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();
            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.sslSocketFactory(sslSocketFactory);
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });

            OkHttpClient okHttpClient = builder.build();
            return okHttpClient;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
