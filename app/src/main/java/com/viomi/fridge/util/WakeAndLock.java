package com.viomi.fridge.util;

import android.content.Context;
import android.os.PowerManager;

import com.viomi.fridge.common.ViomiApplication;

/**
 * Created by young2 on 2017/3/9.
 */

public class WakeAndLock {

    private static String TAG=WakeAndLock.class.getName();
    Context context;
    PowerManager pm;
    PowerManager.WakeLock wakeLock;


    public WakeAndLock() {
        context = ViomiApplication.getContext();
        pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        wakeLock = pm.newWakeLock(PowerManager.ACQUIRE_CAUSES_WAKEUP
                | PowerManager.FULL_WAKE_LOCK, "WakeAndLock");
    }

    /**
     * 唤醒屏幕
     */
    public void screenOn() {
        wakeLock.acquire();
        log.d(TAG, "screenOn");
    }

    /***
     * 是否熄屏
     */
    public boolean isScreenOn(){
       return pm.isScreenOn();
    }

//    /**
//     * 熄灭屏幕
//     */
//    public void screenOff() {
//        pm.goToSleep(SystemClock.uptimeMillis());
//        android.util.Log.i("cxq", "screenOff");
//    }
}
