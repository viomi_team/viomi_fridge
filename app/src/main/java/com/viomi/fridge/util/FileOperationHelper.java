package com.viomi.fridge.util;

import android.content.ContentProviderOperation;
import android.content.Context;
import android.content.OperationApplicationException;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.RemoteException;
import android.provider.MediaStore;

import com.viomi.fridge.model.bean.FileInfo;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Ljh on 2017/11/27.
 */

public class FileOperationHelper {

    private static final String TAG = "FileOperationHelper";
    private ArrayList<FileInfo> mCurFileNameList = new ArrayList<FileInfo>();
    private ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();
    private IOperationProgressListener moperationListener;
    private FilenameFilter mFilter = null;

    private Context mContext;

    private static String ANDROID_SECURE = "/mnt/sdcard/.android_secure";

    public FileOperationHelper(IOperationProgressListener l, Context context) {
        moperationListener = l;
        mContext = context;
    }

    public void clear() {
        synchronized (mCurFileNameList) {
            mCurFileNameList.clear();
        }
    }

    /*
     *创建异步线程
     */
    private void asnycExecute(Runnable r) {
        final Runnable _r = r;
        new AsyncTask() {
            @Override
            protected Object doInBackground(Object... params) {
                synchronized (mCurFileNameList) {
                    _r.run();
                }

                try {
                    mContext.getContentResolver().applyBatch(
                            MediaStore.AUTHORITY, ops);
                } catch (RemoteException e) {
                    e.printStackTrace();
                } catch (OperationApplicationException e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }

                //if (moperationListener != null) {
                //    ((Activity)mContext).runOnUiThread(new Runnable() {
                //        @Override
                //        public void run() {
                //            moperationListener.onFinish();//
                //        }
                //    });
                //    LogUtils.d(TAG,"asnycExecute moperationListener.onFinish");
                //}
                ops.clear();
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                moperationListener.onFinish();//删除操作完毕
                LogUtils.d(TAG, "asnycExecute onPostExecute");
            }
        }.execute();
    }

    /*
     * 删除文件
     */
    public boolean Delete(ArrayList<FileInfo> files) {
        copyFileList(files);
        asnycExecute(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                for (FileInfo f : mCurFileNameList) {
                    DeleteFile(f);
                }
                moperationListener.onFileChanged(Environment.getExternalStorageDirectory().getAbsolutePath());
                clear();
            }
        });
        return true;
    }

    protected void DeleteFile(FileInfo f) {
        if (f == null)
            return;
        File file = new File(f.getAbsolutePath());
        boolean directory = file.isDirectory();
        if (directory) {
            for (File child : file.listFiles(mFilter)) {
                if (isNormalFile(child.getAbsolutePath())) {
                    DeleteFile(GetFileInfo(child));
                }
            }
        } else {
            ops.add(ContentProviderOperation
                    .newDelete(getMediaUriFromFilename(f.getNameFromFilepath()))
                    .withSelection("_data = ?", new String[]{f.getAbsolutePath()})
                    .build());
        }
        file.delete();
    }

    public static FileInfo GetFileInfo(File f) {
        FileInfo lFileInfo = new FileInfo();
        lFileInfo.setAbsolutePath(f.getAbsolutePath());
        //String filePath = f.getPath();
        //File lFile = new File(filePath);
        //lFileInfo.fileName = f.getName();
        //lFileInfo.ModifiedDate = lFile.lastModified();
        //lFileInfo.IsDir = lFile.isDirectory();
        //lFileInfo.filePath = filePath;
        return lFileInfo;
    }

    private void copyFileList(ArrayList<FileInfo> files) {
        synchronized (mCurFileNameList) {
            mCurFileNameList.clear();
            for (FileInfo f : files) {
                mCurFileNameList.add(f);
            }
        }
    }

    public static boolean isNormalFile(String fullName) {
        return !fullName.equals(ANDROID_SECURE);
    }

    public static Uri getMediaUriFromFilename(String filename) {
        String extString = getExtFromFilename(filename);
        String volumeName = "external";
        FileUtil.FileType fileCategoryType = fileExtCategoryType.get(extString);

        Uri uri = null;
        if (fileCategoryType == FileUtil.FileType.FILE_MUSIC) {
            uri = MediaStore.Audio.Media.getContentUri(volumeName);
        } else if (fileCategoryType == FileUtil.FileType.FILE_PIC) {
            uri = MediaStore.Images.Media.getContentUri(volumeName);
        } else if (fileCategoryType == FileUtil.FileType.FILE_VIDEO) {
            uri = MediaStore.Video.Media.getContentUri(volumeName);
        } else {
            uri = MediaStore.Files.getContentUri(volumeName);
        }
        return uri;
    }

    public static String getExtFromFilename(String filename) {
        int dotPosition = filename.lastIndexOf('.');
        if (dotPosition != -1) {
            return filename.substring(dotPosition + 1, filename.length());
        }
        return "";
    }

    public static HashMap<String, FileUtil.FileType> fileExtCategoryType = new HashMap<String, FileUtil.FileType>();

    static {
        addItem(new String[]{"mp4", "wmv", "mpeg", "m4v", "3gp", "3gpp",
                "3g2", "3gpp2", "asf", "rmvb", "avi"}, FileUtil.FileType.FILE_VIDEO);
        addItem(new String[]{"jpg", "jpeg", "gif", "png", "bmp", "wbmp"},
                FileUtil.FileType.FILE_PIC);
        addItem(new String[]{"mp3", "wma", "wav", "ogg"},
                FileUtil.FileType.FILE_MUSIC);

    }

    private static void addItem(String[] exts, FileUtil.FileType categoryType) {
        if (exts != null) {
            for (String ext : exts) {
                fileExtCategoryType.put(ext.toLowerCase(), categoryType);
            }
        }
    }

    public static interface IOperationProgressListener {
        void onFinish();

        void onFileChanged(String path);
    }
}
