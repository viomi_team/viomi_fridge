package com.viomi.fridge.util;

import com.viomi.fridge.common.ViomiApplication;
import com.viomi.fridge.model.bean.MiIndentify;
import com.viomi.fridge.model.bean.ViomiUser;
import com.zhuge.analysis.stat.ZhugeSDK;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * 诸葛IO埋点工具
 * Created by young2 on 2017/6/9.
 */

public class ZhugeIoUtil {
    /***
     * 埋点事件
     * @param event 事件名称
     * @param key 事件类型名
     * @param desc 事件描述
     */
    public static void sendEvent(String event,String key,String desc){
        //定义与事件相关的属性信息
        JSONObject eventObject = new JSONObject();
        try {
            eventObject.put(key, desc);
            //记录事件
            ZhugeSDK.getInstance().track(ViomiApplication.getContext(), event, eventObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    /***
     * 上报获取用户信息
     * @param viomiUser
     */
    public static void setUserInfo(ViomiUser viomiUser){
        if(viomiUser==null){
            viomiUser=new ViomiUser();
        }
        try {
            JSONObject personObject = new JSONObject();
            MiIndentify miIdentify=PhoneUtil.getMiIdentify();
            personObject.put("mac", miIdentify.mac);
            personObject.put("token", miIdentify.token);
            personObject.put("name", viomiUser.getNickname());
            String gender="未知";
            if(viomiUser.getGender()==1){
                gender="男";
            }else  if(viomiUser.getGender()==0){
                gender="女";
            }
            personObject.put("gender", gender);
            personObject.put("miid", viomiUser.getMiId());
            personObject.put("mobile", viomiUser.getMobile());
            personObject.put("cid", viomiUser.getCid());
            personObject.put("usercode", viomiUser.getUserCode());
            personObject.put("account", viomiUser.getAccount());
            personObject.put("logType", viomiUser.getType());
            ZhugeSDK.getInstance().identify(ViomiApplication.getContext(), miIdentify.did, personObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
