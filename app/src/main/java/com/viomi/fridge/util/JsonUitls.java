package com.viomi.fridge.util;

import com.alibaba.fastjson.JSON;
import com.viomi.fridge.model.bean.recipe.Secondlevel;
import com.viomi.fridge.model.bean.recipe.categoryInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by viomi on 2016/10/25.
 * json解析封装
 */

public class JsonUitls {

    public static String getString(JSONObject json, String name) {
        String result;
        if (json != null) {
            try {
                result = json.getString(name);
            } catch (JSONException e) {
                e.printStackTrace();
                result = "-";
            }
        } else {
            result = "-";
        }
        result = "null".equals(result) ? "-" : result;
        return result;
    }


    public static JSONObject getJSONObject(JSONObject json, String name) {
        JSONObject jsonObject;

        if (json != null) {
            try {
                jsonObject = json.getJSONObject(name);
            } catch (JSONException e) {
                e.printStackTrace();
                jsonObject = new JSONObject();
            }
        } else {
            jsonObject = new JSONObject();
        }
        return jsonObject;
    }


    public static JSONArray getJSONArray(JSONObject json, String name) {
        JSONArray jsonArray;

        if (json != null) {
            try {
                jsonArray = json.getJSONArray(name);
            } catch (JSONException e) {
                e.printStackTrace();
                jsonArray = new JSONArray();
            }

        } else {
            jsonArray = new JSONArray();
        }
        return jsonArray;
    }

    public static int getInt(JSONObject json, String name) {
        int result = 0;
        if (json == null) {
            return result;
        }
        try {
            result = json.getInt(name);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static double getDouble(JSONObject json, String name) {
        double result = 0d;
        if (json == null) {
            return result;
        }
        try {
            result = json.getDouble(name);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static long getLong(JSONObject json, String name) {
        long result = 0L;
        if (json == null) {
            return result;
        }
        try {
            result = json.getLong(name);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static Secondlevel parseSecondlevel(String json){
        Secondlevel secondlevel=null;
        try {
            secondlevel=new Secondlevel();
            JSONObject  object=new JSONObject(json);
            categoryInfo level=parseCategory(object.getJSONObject("categoryInfo"));
            JSONArray array=object.getJSONArray("childs");
            secondlevel.setCategoryInfo(level);

            ArrayList<categoryInfo> list1=new ArrayList<>();
            for (int j=0;j<array.length();j++) {
                JSONObject object2 = (JSONObject) array.get(j);
                categoryInfo categoryInfo1=parseCategory(object2.getJSONObject("categoryInfo"));
                list1.add(categoryInfo1);
            }
            secondlevel.setChilds(list1);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return secondlevel;
    }

    private static categoryInfo parseCategory(JSONObject object){
        String json=object.toString();
        return JSON.parseObject(json,categoryInfo.class);
    }
}
