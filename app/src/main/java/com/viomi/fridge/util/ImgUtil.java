package com.viomi.fridge.util;

import android.graphics.Bitmap;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

/**
 * Created by Ljh on 17/12/4
 */

public class ImgUtil {
    private static final String TAG = "ImgUtil";

    public static void showImage(String url, ImageView iv) {
        ImageLoader.getInstance().displayImage(url, iv);
    }


    public static void showDefinedImage(String url, ImageView iv,int resouceId) {
        LogUtils.d(TAG+" the url is:"+url);
        DisplayImageOptions bannerOption = new DisplayImageOptions.Builder()
                .cacheInMemory(true).bitmapConfig(Bitmap.Config.RGB_565)
                .cacheOnDisk(true).imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .resetViewBeforeLoading(true)
                .showImageForEmptyUri(resouceId)
                .showImageOnFail(resouceId)
                .showImageOnLoading(resouceId)
                .build();
        ImageLoader.getInstance().displayImage(url, iv, bannerOption);
    }

    public static void showDefinedImageListener(String url, ImageView iv, int resouceId, ImageLoadingListener listener) {
        LogUtils.d(TAG+" the url is:"+url);
        DisplayImageOptions bannerOption = new DisplayImageOptions.Builder()
                .cacheInMemory(true).bitmapConfig(Bitmap.Config.RGB_565)
                .cacheOnDisk(true).imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .resetViewBeforeLoading(true)
                .showImageForEmptyUri(resouceId)
                .showImageOnFail(resouceId)
                .showImageOnLoading(resouceId)
                .build();
        ImageLoader.getInstance().displayImage(url, iv, bannerOption,listener);
    }

    public static void showImageListener(String url, ImageView iv, ImageLoadingListener listener) {
        LogUtils.d(TAG+" the url is:"+url);
        ImageLoader.getInstance().displayImage(url, iv,listener);
    }
}
