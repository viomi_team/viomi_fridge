package com.viomi.fridge.util;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by viomi on 2017/4/25.
 */

public class SystemFileUtils {

    public enum HumanSensorType {close, open}

    public enum HumanSensorPathEnum {

        DOOR3("/sys/bus/platform/drivers/mtk-kpd/motion_sensor_enable"),
        DOOR4("/sys/bus/platform/drivers/mtk-kpd/irda_poll_ctl"),
        DOOR462("/sys/bus/platform/drivers/mtk-kpd/irda_poll_ctl"),
        DOOR455("/sys/bus/platform/drivers/mtk-kpd/irda_poll_ctl");

        private String path;

        HumanSensorPathEnum(String path) {
            this.path = path;
        }
    }

    public static void setHumanSensor(HumanSensorType type) {
        OutputStream os = null;
        try {
            os = new FileOutputStream("/sys/bus/platform/drivers/mtk-kpd/motion_sensor_enable", false);
            switch (type) {
                case close:
                    os.write(48);
                    os.flush();
                    log.myE("human_sensor", "set-ok-close");
                    break;
                case open:
                    os.write(49);
                    os.flush();
                    log.myE("human_sensor", "set-ok-open");
                    break;
                default:
                    break;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            log.myE("human_sensor", "set-fail-FileNotFoundException");
        } catch (IOException e) {
            e.printStackTrace();
            log.myE("human_sensor", "set-fail-IOException");
        } finally {
            if (os != null) {
                try {
                    os.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void setHumanSensor(HumanSensorType type, HumanSensorPathEnum path) {
        OutputStream os = null;
        try {
            os = new FileOutputStream(path.path, false);
            switch (type) {
                case close:
                    os.write(48);
                    os.flush();
                    log.myE("human_sensor", "set-ok-close");
                    break;
                case open:
                    os.write(49);
                    os.flush();
                    log.myE("human_sensor", "set-ok-open");
                    break;
                default:
                    break;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            log.myE("human_sensor", "set-fail-FileNotFoundException");
        } catch (IOException e) {
            e.printStackTrace();
            log.myE("human_sensor", "set-fail-IOException");
        } finally {
            if (os != null) {
                try {
                    os.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
