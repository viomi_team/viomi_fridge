package com.viomi.fridge.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import com.viomi.fridge.model.bean.WebBaseData;
import com.viomi.fridge.view.activity.CommonWebActivity;
import com.viomi.fridge.view.activity.VideoSearchActivity;
import com.viomi.fridge.view.activity.VmallWebActivity;

public class UMUtil {

	//转换dip为px
	public static int dipToPx(Context context, float dip) {
		float scale = context.getResources().getDisplayMetrics().density;
		return (int)(dip*scale + 0.5f*(dip>=0?1:-1));
	}

	 //转换px为dip
	public static float pxTodip(Context context, int px) {
		float scale = context.getResources().getDisplayMetrics().density;
		return px/scale + 0.5f*(px>=0?1:-1);
	}

	public static int spToPx(Context context, float sp) {
		float fontScale = context.getResources().getDisplayMetrics().scaledDensity;
		return (int) (sp * fontScale + 0.5f);
	}

	 public static int pxToSp(Context context, float pxValue) {
		float fontScale = context.getResources().getDisplayMetrics().scaledDensity;
		return (int) (pxValue / fontScale + 0.5f);
	}

	public static void onWebPageJump(Activity activity,String title, String url, boolean rightIcon){
		Intent intent=new Intent(activity, VmallWebActivity.class);
		WebBaseData model=new WebBaseData();
		model.url=url;
		model.name=title;
		intent.putExtra(WebBaseData.Intent_String, model);
		activity.startActivity(intent);
	}

	public static void onWebPageJump(Context context,String title, String url, boolean rightIcon){
		Intent intent=new Intent(context, VmallWebActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		WebBaseData model=new WebBaseData();
		model.url=url;
		model.name=title;
		intent.putExtra(WebBaseData.Intent_String, model);
		context.startActivity(intent);
	}

	public static void onCommonWebPageJump(Context context, String url){
		Intent intent=new Intent(context, CommonWebActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		WebBaseData model=new WebBaseData();
		model.url=url;
		intent.putExtra(WebBaseData.Intent_String, model);
		context.startActivity(intent);
	}

	/***
	 * 跳转视频搜索电影
	 * @param context
	 * @param name
	 */
	public static void onSearchVideoPageJump(Context context,String name){
		Intent intent=new Intent(context, VideoSearchActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.putExtra("vioce_control_keyword", name);
		context.startActivity(intent);
	}
}
