package com.viomi.fridge.util;

import android.widget.ImageView;

import com.viomi.fridge.R;

/**
 * Created by viomi on 2017/3/6.
 */

public class WeatherIconUtil {

    public static void setWeatherIcon(ImageView iv, String type) {

        switch (type) {

            case "暴雪": {
                iv.setImageResource(R.drawable.weather_ic_1);
                break;
            }
            case "暴雨": {
                iv.setImageResource(R.drawable.weather_ic_2);
                break;
            }
            case "暴雨到大暴雨": {
                iv.setImageResource(R.drawable.weather_ic_3);
                break;
            }
            case "大暴雨": {
                iv.setImageResource(R.drawable.weather_ic_4);
                break;
            }
            case "大暴雨到特大暴雨": {
                iv.setImageResource(R.drawable.weather_ic_5);
                break;
            }
            case "大到暴雪": {
                iv.setImageResource(R.drawable.weather_ic_6);
                break;
            }
            case "大到暴雨": {
                iv.setImageResource(R.drawable.weather_ic_7);
                break;
            }
            case "大雪": {
                iv.setImageResource(R.drawable.weather_ic_8);
                break;
            }
            case "大雨": {
                iv.setImageResource(R.drawable.weather_ic_9);
                break;
            }
            case "冻雨": {
                iv.setImageResource(R.drawable.weather_ic_10);
                break;
            }
            case "多云": {
                iv.setImageResource(R.drawable.weather_ic_11);
                break;
            }
            case "浮尘": {
                iv.setImageResource(R.drawable.weather_ic_12);
                break;
            }
            case "雷阵雨": {
                iv.setImageResource(R.drawable.weather_ic_13);
                break;
            }
            case "雷阵雨伴有冰雹": {
                iv.setImageResource(R.drawable.weather_ic_14);
                break;
            }
            case "霾": {
                iv.setImageResource(R.drawable.weather_ic_15);
                break;
            }
            case "强沙尘暴": {
                iv.setImageResource(R.drawable.weather_ic_16);
                break;
            }
            case "晴": {
                iv.setImageResource(R.drawable.weather_ic_17);
                break;
            }
            case "沙尘暴": {
                iv.setImageResource(R.drawable.weather_ic_18);
                break;
            }
            case "特大暴雨": {
                iv.setImageResource(R.drawable.weather_ic_19);
                break;
            }
            case "雾": {
                iv.setImageResource(R.drawable.weather_ic_20);
                break;
            }
            case "小到中雪": {
                iv.setImageResource(R.drawable.weather_ic_21);
                break;
            }
            case "小到中雨": {
                iv.setImageResource(R.drawable.weather_ic_22);
                break;
            }
            case "小雪": {
                iv.setImageResource(R.drawable.weather_ic_23);
                break;
            }
            case "小雨": {
                iv.setImageResource(R.drawable.weather_ic_24);
                break;
            }
            case "扬沙": {
                iv.setImageResource(R.drawable.weather_ic_25);
                break;
            }
            case "阴": {
                iv.setImageResource(R.drawable.weather_ic_26);
                break;
            }
            case "雨夹雪": {
                iv.setImageResource(R.drawable.weather_ic_27);
                break;
            }
            case "阵雪": {
                iv.setImageResource(R.drawable.weather_ic_28);
                break;
            }
            case "阵雨": {
                iv.setImageResource(R.drawable.weather_ic_29);
                break;
            }
            case "中到大雪": {
                iv.setImageResource(R.drawable.weather_ic_30);
                break;
            }
            case "中到大雨": {
                iv.setImageResource(R.drawable.weather_ic_31);
                break;
            }
            case "中雪": {
                iv.setImageResource(R.drawable.weather_ic_32);
                break;
            }
            case "中雨": {
                iv.setImageResource(R.drawable.weather_ic_33);
                break;
            }
            case "无": {
                iv.setImageResource(R.drawable.weather_ic_34);
                break;
            }
            default: {
                if (type == null) {
                    iv.setImageResource(R.drawable.weather_ic_34);
                    return;
                }

                if (type.contains("小雪")) {
                    iv.setImageResource(R.drawable.weather_ic_23);
                    return;
                }
                if (type.contains("中雪")) {
                    iv.setImageResource(R.drawable.weather_ic_32);
                    return;
                }
                if (type.contains("大雪")) {
                    iv.setImageResource(R.drawable.weather_ic_8);
                    return;
                }
                if (type.contains("暴雪")) {
                    iv.setImageResource(R.drawable.weather_ic_1);
                    return;
                }
                if (type.contains("雪")) {
                    iv.setImageResource(R.drawable.weather_ic_32);
                    return;
                }


                if (type.contains("小雨")) {
                    iv.setImageResource(R.drawable.weather_ic_24);
                    return;
                }
                if (type.contains("中雨")) {
                    iv.setImageResource(R.drawable.weather_ic_33);
                    return;
                }
                if (type.contains("大雨")) {
                    iv.setImageResource(R.drawable.weather_ic_9);
                    return;
                }
                if (type.contains("暴雨")) {
                    iv.setImageResource(R.drawable.weather_ic_2);
                    return;
                }

                if (type.contains("雨")) {
                    iv.setImageResource(R.drawable.weather_ic_33);
                    return;
                }

                if (type.contains("冰雹")) {
                    iv.setImageResource(R.drawable.weather_ic_14);
                    return;
                }
                if (type.contains("尘")) {
                    iv.setImageResource(R.drawable.weather_ic_12);
                    return;
                }
                if (type.contains("霾")) {
                    iv.setImageResource(R.drawable.weather_ic_15);
                    return;
                }
                if (type.contains("雾")) {
                    iv.setImageResource(R.drawable.weather_ic_20);
                    return;
                }
                if (type.contains("阴")) {
                    iv.setImageResource(R.drawable.weather_ic_26);
                    return;
                }
                if (type.contains("云")) {
                    iv.setImageResource(R.drawable.weather_ic_11);
                    return;
                }

                iv.setImageResource(R.drawable.weather_ic_34);
                break;
            }

        }
    }

    public static void setWeatherIcon2(ImageView iv, String type) {

        switch (type) {

            case "暴雪": {
                iv.setImageResource(R.drawable.a_weather_ic_1);
                break;
            }
            case "暴雨": {
                iv.setImageResource(R.drawable.a_weather_ic_2);
                break;
            }
            case "暴雨到大暴雨": {
                iv.setImageResource(R.drawable.a_weather_ic_3);
                break;
            }
            case "大暴雨": {
                iv.setImageResource(R.drawable.a_weather_ic_4);
                break;
            }
            case "大暴雨到特大暴雨": {
                iv.setImageResource(R.drawable.a_weather_ic_5);
                break;
            }
            case "大到暴雪": {
                iv.setImageResource(R.drawable.a_weather_ic_6);
                break;
            }
            case "大到暴雨": {
                iv.setImageResource(R.drawable.a_weather_ic_7);
                break;
            }
            case "大雪": {
                iv.setImageResource(R.drawable.a_weather_ic_8);
                break;
            }
            case "大雨": {
                iv.setImageResource(R.drawable.a_weather_ic_9);
                break;
            }
            case "冻雨": {
                iv.setImageResource(R.drawable.a_weather_ic_10);
                break;
            }
            case "多云": {
                iv.setImageResource(R.drawable.a_weather_ic_11);
                break;
            }
            case "浮尘": {
                iv.setImageResource(R.drawable.a_weather_ic_12);
                break;
            }
            case "雷阵雨": {
                iv.setImageResource(R.drawable.a_weather_ic_13);
                break;
            }
            case "雷阵雨伴有冰雹": {
                iv.setImageResource(R.drawable.a_weather_ic_14);
                break;
            }
            case "霾": {
                iv.setImageResource(R.drawable.a_weather_ic_15);
                break;
            }
            case "强沙尘暴": {
                iv.setImageResource(R.drawable.a_weather_ic_16);
                break;
            }
            case "晴": {
                iv.setImageResource(R.drawable.a_weather_ic_17);
                break;
            }
            case "沙尘暴": {
                iv.setImageResource(R.drawable.a_weather_ic_18);
                break;
            }
            case "特大暴雨": {
                iv.setImageResource(R.drawable.a_weather_ic_19);
                break;
            }
            case "雾": {
                iv.setImageResource(R.drawable.a_weather_ic_20);
                break;
            }
            case "小到中雪": {
                iv.setImageResource(R.drawable.a_weather_ic_21);
                break;
            }
            case "小到中雨": {
                iv.setImageResource(R.drawable.a_weather_ic_22);
                break;
            }
            case "小雪": {
                iv.setImageResource(R.drawable.a_weather_ic_23);
                break;
            }
            case "小雨": {
                iv.setImageResource(R.drawable.a_weather_ic_24);
                break;
            }
            case "扬沙": {
                iv.setImageResource(R.drawable.a_weather_ic_25);
                break;
            }
            case "阴": {
                iv.setImageResource(R.drawable.a_weather_ic_26);
                break;
            }
            case "雨夹雪": {
                iv.setImageResource(R.drawable.a_weather_ic_27);
                break;
            }
            case "阵雪": {
                iv.setImageResource(R.drawable.a_weather_ic_28);
                break;
            }
            case "阵雨": {
                iv.setImageResource(R.drawable.a_weather_ic_29);
                break;
            }
            case "中到大雪": {
                iv.setImageResource(R.drawable.a_weather_ic_30);
                break;
            }
            case "中到大雨": {
                iv.setImageResource(R.drawable.a_weather_ic_31);
                break;
            }
            case "中雪": {
                iv.setImageResource(R.drawable.a_weather_ic_32);
                break;
            }
            case "中雨": {
                iv.setImageResource(R.drawable.a_weather_ic_33);
                break;
            }
            case "无": {
                iv.setImageResource(R.drawable.a_weather_ic_34);
                break;
            }
            default: {
                if (type == null) {
                    iv.setImageResource(R.drawable.a_weather_ic_34);
                    return;
                }

                if (type.contains("小雪")) {
                    iv.setImageResource(R.drawable.a_weather_ic_23);
                    return;
                }
                if (type.contains("中雪")) {
                    iv.setImageResource(R.drawable.a_weather_ic_32);
                    return;
                }
                if (type.contains("大雪")) {
                    iv.setImageResource(R.drawable.a_weather_ic_8);
                    return;
                }
                if (type.contains("暴雪")) {
                    iv.setImageResource(R.drawable.a_weather_ic_1);
                    return;
                }
                if (type.contains("雪")) {
                    iv.setImageResource(R.drawable.a_weather_ic_32);
                    return;
                }


                if (type.contains("小雨")) {
                    iv.setImageResource(R.drawable.a_weather_ic_24);
                    return;
                }
                if (type.contains("中雨")) {
                    iv.setImageResource(R.drawable.a_weather_ic_33);
                    return;
                }
                if (type.contains("大雨")) {
                    iv.setImageResource(R.drawable.a_weather_ic_9);
                    return;
                }
                if (type.contains("暴雨")) {
                    iv.setImageResource(R.drawable.a_weather_ic_2);
                    return;
                }

                if (type.contains("雨")) {
                    iv.setImageResource(R.drawable.a_weather_ic_33);
                    return;
                }

                if (type.contains("冰雹")) {
                    iv.setImageResource(R.drawable.a_weather_ic_14);
                    return;
                }
                if (type.contains("尘")) {
                    iv.setImageResource(R.drawable.a_weather_ic_12);
                    return;
                }
                if (type.contains("霾")) {
                    iv.setImageResource(R.drawable.a_weather_ic_15);
                    return;
                }
                if (type.contains("雾")) {
                    iv.setImageResource(R.drawable.a_weather_ic_20);
                    return;
                }
                if (type.contains("阴")) {
                    iv.setImageResource(R.drawable.a_weather_ic_26);
                    return;
                }
                if (type.contains("云")) {
                    iv.setImageResource(R.drawable.a_weather_ic_11);
                    return;
                }

                iv.setImageResource(R.drawable.a_weather_ic_34);
                break;
            }

        }
    }


}
