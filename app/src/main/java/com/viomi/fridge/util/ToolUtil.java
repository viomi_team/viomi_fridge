package com.viomi.fridge.util;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.Browser;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.viomi.fridge.model.bean.recipe.RecipeDetail;
import com.viomi.fridge.model.bean.recipe.RecipeModel;
import com.viomi.fridge.model.bean.recipe.Step;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 工具方法类
 * Created by young2 on 2016/12/28.
 */

public class ToolUtil {
    /**
     * 判断是否数字
     *
     * @param str：字符串
     * @return boolean
     */
    public static boolean isNumber(String str) {
        if (str == null) {
            return false;
        }
        String reg = "^[0-9]+(.[0-9]+)?$";
        return str.matches(reg);
    }

    /**
     * 格式化mac，去掉冒号
     *
     * @param mac：mac地址
     * @return 格式化mac
     */
    public static String formatMac(String mac) {
        if (mac == null || mac.length() == 0) {
            return mac;
        }
        return mac.replace(":", "");
    }

    public static byte[] longToByte(long number) {
        byte[] statusByte = new byte[32];
        long temp = number;
        for (int i = 0; i < statusByte.length; i++) {
            statusByte[i] = (byte) (temp & 1);
            temp = temp >> 1;
        }
        return statusByte;
    }

    /**
     * 时间格式化
     *
     * @param time：时间（单位：分钟）
     * @return 字符串
     */
    public static String timeFormat(int time) {
        String str;
        int hour = time / 60;
        int minute = time % 60;
        if (hour == 0) str = "<font><big><big>" + minute + "</big></big></font>分钟";
        else
            str = "<font><big><big>" + hour + "</big></big></font>小时" + "<font><big><big>" + minute + "</big></big></font>分钟";
        return str;
    }

    /**
     * 判断某个服务是否正在运行
     *
     * @param serviceName 是包名+ 服务的类名
     * @return true 代表正在运行，false 代表服务没有正在运行
     */
    public static boolean isServiceWork(Context mContext, String serviceName) {
        boolean isWork = false;
        ActivityManager myAM = (ActivityManager) mContext
                .getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningServiceInfo> myList = myAM.getRunningServices(Integer.MAX_VALUE);
        if (myList.size() <= 0) {
            return false;
        }
        for (int i = 0; i < myList.size(); i++) {
            String mName = myList.get(i).service.getClassName();
            if (mName.equals(serviceName)) {
                isWork = true;
                break;
            }
        }
        return isWork;
    }

    /**
     * 设置 IPC 录像保存路径
     */
    public static String getRecordDirectory(String did) {
        String dir = "SDKTestbed/" + did + "/Video/";
        final File recDir = new File(Environment.getExternalStorageDirectory(), dir);
        boolean suc = recDir.mkdirs();
        String dirPath = recDir.getAbsolutePath();
        log.d("LiveRecord", "save Dir:" + dirPath + ",mkdir:" + suc);
        return dirPath;
    }

    /**
     * 设备 Get Prop 请求返回数据格式解析判断
     *
     * @param result: 返回数据
     * @return 是否有效
     */
    public static JSONArray getPropJsonParse(String result) {
        if (result == null) return null;
        else {
            try {
                JSONObject jsonObject = new JSONObject(result);
                String code = jsonObject.optString("code");// 返回码
                if (code.equals("0")) {
                    JSONArray jsonArray = jsonObject.optJSONArray("result");
                    if (jsonArray == null || jsonArray.length() == 0) return null;
                    else return jsonArray;
                } else return null;
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    /**
     * 设备方法设置返回数据解析
     *
     * @param result: 返回数据
     * @return 是否成功
     */
    public static boolean setPropertyJsonParse(String result) {
        if (result == null) return false;
        else {
            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.optJSONArray("result");
                if (jsonArray == null) return false;
                else {
                    String msg = jsonArray.optString(0);
                    return msg.equals("ok");
                }
            } catch (JSONException e) {
                e.printStackTrace();
                return false;
            }
        }
    }

    /**
     * 动态计算 ListView 高度(适用于嵌套在 Scrollview)
     */
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        // 获取ListView对应的Adapter
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            return;
        }
        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) { // listAdapter.getCount()返回数据项的数目
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0); // 计算子项View 的宽高
            totalHeight += listItem.getMeasuredHeight(); // 统计所有子项的总高度
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight
                + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        // listView.getDividerHeight()获取子项间分隔符占用的高度
        // params.height最后得到整个ListView完整显示需要的高度
        listView.setLayoutParams(params);
    }

    public static SpannableStringBuilder formatUrlString(String contentStr) {

        SpannableStringBuilder sp;
        if (!TextUtils.isEmpty(contentStr)) {

            sp = new SpannableStringBuilder(contentStr);
            try {
                //处理url匹配
                Pattern urlPattern = Pattern.compile("(http|https|ftp|svn)://([a-zA-Z0-9]+[/?.?])" +
                        "+[a-zA-Z0-9]*\\??([a-zA-Z0-9]*=[a-zA-Z0-9]*&?)*");
                Matcher urlMatcher = urlPattern.matcher(contentStr);

                while (urlMatcher.find()) {
                    final String url = urlMatcher.group();
                    if (!TextUtils.isEmpty(url)) {
                        sp.setSpan(new SpannableClickable() {
                            @Override
                            public void onClick(View widget) {
                                Uri uri = Uri.parse(url);
                                Context context = widget.getContext();
                                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                intent.putExtra(Browser.EXTRA_APPLICATION_ID, context.getPackageName());
                                context.startActivity(intent);
                            }
                        }, urlMatcher.start(), urlMatcher.end(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    }
                }

                //处理电话匹配
                Pattern phonePattern = Pattern.compile("[1][34578][0-9]{9}");
                Matcher phoneMatcher = phonePattern.matcher(contentStr);
                while (phoneMatcher.find()) {
                    final String phone = phoneMatcher.group();
                    if (!TextUtils.isEmpty(phone)) {
                        sp.setSpan(new SpannableClickable() {
                            @Override
                            public void onClick(View widget) {
                                Context context = widget.getContext();
                                //用intent启动拨打电话
                                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phone));
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                context.startActivity(intent);

                            }
                        }, phoneMatcher.start(), phoneMatcher.end(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            sp = new SpannableStringBuilder();
        }
        return sp;
    }

    public static ArrayList<RecipeDetail> getList(ArrayList<RecipeDetail> details) {
        ArrayList<RecipeDetail> list = new ArrayList<>();
        if (details != null) {
            if (details.size() != 0) {
                for (int i = 0; i < details.size(); i++) {
                    RecipeDetail detail = details.get(i);
                    RecipeDetail.recipe recipe = detail.getRecipe();
                    if (isExist(recipe)) {
                        list.add(detail);
                    }
                }
            }
        }
        return list;
    }

    private static boolean isExist(RecipeDetail.recipe recipe) {
        boolean flag = false;
        if (recipe == null)
            return flag;
        if (isContain(recipe.getImg())) {
            ArrayList<Step> steps = recipe.getSteps();
            int size = steps.size();
            if (steps != null && size != 0) {
                int sum = 0;
                for (int i = 0; i < size; i++) {
                    Step step = steps.get(i);
                    if (isContain(step.getImg())) {
                        sum++;
                    }
                }
                if (sum != size) {
                    flag = false;
                } else {
                    flag = true;
                }
            } else {
                flag = false;
            }
        }
        return flag;
    }


    public static boolean isRecipeExist(RecipeModel recipe) {
        boolean flag = false;
        if (recipe == null)
            return flag;
        if (isContain(recipe.getImgUrl())) {
            ArrayList<RecipeModel.StepMethod> steps = (ArrayList<RecipeModel.StepMethod>) recipe.getStepsWithImg();
            int size = steps.size();
            if (steps != null && size != 0) {
                int sum = 0;
                for (int i = 0; i < size; i++) {
                    RecipeModel.StepMethod step = steps.get(i);
                    if (isContain(step.getImage())) {
                        sum++;
                    }
                }
                if (sum != size) {
                    flag = false;
                } else {
                    flag = true;
                }
            } else {
                flag = false;
            }
        }
        return flag;
    }

    private static boolean isContain(String str) {
        if (!TextUtils.isEmpty(str) && !str.equals("-")) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 获取屏幕宽度(px)
     */
    public static int getScreenWidth(Context context) {
        return context.getResources().getDisplayMetrics().widthPixels;
    }
}
