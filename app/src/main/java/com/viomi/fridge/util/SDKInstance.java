package com.viomi.fridge.util;

import android.content.Context;

import com.arcsoft.closeli.Closeli;
import com.v2.clsdk.ServerConfig;
import com.v2.proxy.SDKProtocol;

/**
 * IPC SDK 配置
 * Created by William on 2017/11/8.
 */

public class SDKInstance {

    private final static String ProductKey = "edcbd053-18b";
    private final static String ProductSecret = "6rJrZPatJRfv2ZtojUoL";
    private final static String QRCodeKey = "o1";

    public static void init(final Context context) {
        Closeli.init(context, ProductKey, ProductSecret, ServerConfig.ServerType.IPC);
    }

    public static SDKProtocol getInstance() {
        return Closeli.getInstance();
    }

    public static String getQRCodeKey() {
        return QRCodeKey;
    }

}
