package com.viomi.fridge.device;

public class MiotDeviceInfo {

    public String deviceId;
    public String macAddress;
    public String miotToken;
    public String miotInfo;
}
