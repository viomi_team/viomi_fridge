package com.viomi.fridge.device;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.miot.api.MiotManager;
import com.miot.common.ReturnCode;
import com.miot.common.config.AppConfiguration;
import com.miot.common.model.DeviceModel;
import com.miot.common.model.DeviceModelException;
import com.miot.common.model.DeviceModelFactory;
import com.viomi.common.callback.AppCallback;
import com.viomi.fridge.common.ViomiApplication;

/**
 * Created by young2 on 2017/2/25.
 */

public class MiotConnectManager  {
    private final static String TAG=MiotConnectManager.class.getSimpleName();
    private Context mContext=ViomiApplication.getContext();
    private AppCallback<String> mMiotOpenCallBack;
    private AppCallback<String> mMiotCloseCallBack;

    public void MiotOpen(String type ,AppCallback<String> callback){
        mMiotOpenCallBack=callback;
        new MiotOpenTask().execute(type);
    }

    public void MiotClose(AppCallback<String> callback){
        mMiotCloseCallBack=callback;
        new MiotCloseTask().execute();
    }

    private class MiotOpenTask extends AsyncTask<String, Void, Integer> {
        @Override
        protected Integer doInBackground(String... params) {

            AppConfiguration appConfig = new AppConfiguration();
            if(params[0].equals("ios")){
                appConfig.setAppId(AppConfig.OAUTH_IOS_APP_ID);
                appConfig.setAppKey(AppConfig.OAUTH_IOS_APP_KEY);
            }else {
                appConfig.setAppId(AppConfig.OAUTH_ANDROID_APP_ID);
                appConfig.setAppKey(AppConfig.OAUTH_ANDROID_APP_KEY);
            }
            MiotManager.getInstance().setAppConfig(appConfig);

            try {
                com.miot.common.model.DeviceModel water1 = DeviceModelFactory.createDeviceModel(mContext, AppConfig.YUNMI_WATERPURIFIER_V1,
                        AppConfig.YUNMI_WATERPURIFIER_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(water1);
                DeviceModel water2 = DeviceModelFactory.createDeviceModel(mContext, AppConfig.YUNMI_WATERPURIFIER_V2,
                        AppConfig.YUNMI_WATERPURIFIER_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(water2);
                DeviceModel water3 = DeviceModelFactory.createDeviceModel(mContext, AppConfig.YUNMI_WATERPURIFIER_V3,
                        AppConfig.YUNMI_WATERPURIFIER_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(water3);
                DeviceModel water4 = DeviceModelFactory.createDeviceModel(mContext, AppConfig.YUNMI_WATERPURI_LX2,
                        AppConfig.YUNMI_WATERPURIFIER_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(water4);
                DeviceModel water5 = DeviceModelFactory.createDeviceModel(mContext, AppConfig.YUNMI_WATERPURI_LX3,
                        AppConfig.YUNMI_WATERPURIFIER_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(water5);
                DeviceModel water6 = DeviceModelFactory.createDeviceModel(mContext, AppConfig.YUNMI_WATERPURI_V1,
                        AppConfig.YUNMI_WATERPURI_V1_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(water6);
                DeviceModel water7 = DeviceModelFactory.createDeviceModel(mContext, AppConfig.YUNMI_WATERPURI_V2,
                        AppConfig.YUNMI_WATERPURI_V2_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(water7);
                DeviceModel water8 = DeviceModelFactory.createDeviceModel(mContext, AppConfig.YUNMI_WATERPURI_S1,
                        AppConfig.YUNMI_WATERPURI_S1_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(water8);
                DeviceModel water9 = DeviceModelFactory.createDeviceModel(mContext, AppConfig.YUNMI_WATERPURI_C1,
                        AppConfig.YUNMI_WATERPURI_C1_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(water9);
                DeviceModel water10 = DeviceModelFactory.createDeviceModel(mContext, AppConfig.YUNMI_WATERPURI_C2,
                        AppConfig.YUNMI_WATERPURI_C2_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(water10);
                DeviceModel water11 = DeviceModelFactory.createDeviceModel(mContext, AppConfig.YUNMI_WATERPURI_S2,
                        AppConfig.YUNMI_WATERPURI_S2_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(water11);
                DeviceModel water14 = DeviceModelFactory.createDeviceModel(mContext, AppConfig.YUNMI_WATERPURI_X3,
                        AppConfig.YUNMI_WATERPURI_X3_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(water14);
                DeviceModel water15 = DeviceModelFactory.createDeviceModel(mContext, AppConfig.YUNMI_WATERPURI_X5,
                        AppConfig.YUNMI_WATERPURI_X5_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(water15);
                DeviceModel water17 = DeviceModelFactory.createDeviceModel(mContext, AppConfig.VIOMI_FRIDGE_V1,
                        AppConfig.VIOMI_FRIDGE_V1_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(water17);

            } catch (DeviceModelException e) {
                e.printStackTrace();
            }
            int result= MiotManager.getInstance().open();
            return result;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);

            do {
                int result = integer;
                Log.d(TAG, "MiotOpen result: " + result);
                Intent intent = new Intent(AppConfig.ACTION_BIND_SERVICE_FAILED);
                if (result == ReturnCode.OK) {
                    mMiotOpenCallBack.onSuccess(null);
                    MiDeviceManager.getInstance().getWanDeviceList();
                }else {
                    mMiotOpenCallBack.onFail(-1,"open fail");
                }
            }
            while (false);
        }

    }


    private class MiotCloseTask extends AsyncTask<Void, Void, Integer> {
        @Override
        protected Integer doInBackground(Void... params) {
            return MiotManager.getInstance().close();
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);

                int result = integer;
                Log.d(TAG, "MiotClose result: " + result);
            if (result == ReturnCode.OK) {
                mMiotCloseCallBack.onSuccess(null);
                MiDeviceManager.getInstance().getWanDeviceList();
            }else {
                mMiotCloseCallBack.onFail(-1,"open fail");
            }
        }
    }



}
