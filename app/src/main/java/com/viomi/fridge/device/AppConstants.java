package com.viomi.fridge.device;

public class AppConstants {
    public static final String USER_INFO_FILE = "ViomiUser.dat";// 用户信息
    public static final String URL_VMALL_RELEASE = "https://viomi-fridgex-vmall.mi-ae.net";// 云米商城正式环境
    public static final String WEB_URL = "url";// 网址
}
