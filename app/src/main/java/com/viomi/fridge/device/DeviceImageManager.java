package com.viomi.fridge.device;


import com.viomi.fridge.R;

/**
 * 设备对应图标
 * Created by young2 on 2016/7/3.
 */
public class DeviceImageManager {
    public static int getIconByModel(String model) {
        int drawableId;
        switch (model) {
            case AppConfig.YUNMI_WATERPURI_C1:
            case AppConfig.YUNMI_WATERPURI_C2:
                drawableId = R.drawable.c;
                break;
            case AppConfig.YUNMI_WATERPURI_V1:
                drawableId = R.drawable.v1_400g;
                break;
            case AppConfig.YUNMI_WATERPURI_V2:
                drawableId = R.drawable.v1_600g;
                break;
            case AppConfig.YUNMI_WATERPURI_S1:
                drawableId = R.drawable.s1_75g;
                break;
            case AppConfig.YUNMI_WATERPURI_S2:
                drawableId = R.drawable.s1_50g;
                break;
            case AppConfig.YUNMI_WATERPURI_X3:
                drawableId = R.drawable.x3;
                break;
            case AppConfig.YUNMI_WATERPURI_X5:
                drawableId = R.drawable.x5;
                break;
            case AppConfig.YUNMI_WATERPURIFIER_V1:
            case AppConfig.YUNMI_WATERPURIFIER_V2:
            case AppConfig.YUNMI_WATERPURIFIER_V3:
            case AppConfig.YUNMI_WATERPURI_LX2:
            case AppConfig.YUNMI_WATERPURI_LX3:
                drawableId = R.drawable.icon_water;
                break;
            case AppConfig.VIOMI_FRIDGE_V1:
                drawableId = R.drawable.fridge_v1;
                break;
            case AppConfig.VIOMI_FRIDGE_V2:
                drawableId = R.drawable.fridge_v2;
                break;
            case AppConfig.VIOMI_FRIDGE_U1:
                drawableId = R.drawable.fridge_u1;
                break;
            case AppConfig.VIOMI_HOOD_T8:
            case AppConfig.VIOMI_HOOD_A5:
                drawableId = R.drawable.hood_t;
                break;
            case AppConfig.VIOMI_HOOD_A6:
                drawableId = R.drawable.hood_a6;
                break;
            case AppConfig.VIOMI_HOOD_A7:
                drawableId = R.drawable.hood_a7;
                break;
            case AppConfig.VIOMI_HOOD_C6:
            case AppConfig.VIOMI_HOOD_A4:
                drawableId = R.drawable.hood_a4;
                break;
            case AppConfig.VIOMI_HOOD_C1:
                drawableId = R.drawable.hood_c1;
                break;
            case AppConfig.VIOMI_HOOD_H1:
                drawableId = R.drawable.hood_h1;
                break;
            case AppConfig.VIOMI_HOOD_H2:
                drawableId = R.drawable.hood_h2;
                break;
            case AppConfig.YUNMI_KETTLE_R1:
                drawableId = R.drawable.r1;
                break;
            case AppConfig.YUNMI_PL_MACHINE_MG2:
                drawableId = R.drawable.machine_mg2;
                break;
            default:
                drawableId = R.drawable.device_default;
                break;
        }
        return drawableId;
    }
}
