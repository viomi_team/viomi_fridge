package com.viomi.fridge.mvp.model;

import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;

import com.alibaba.fastjson.JSON;
import com.viomi.fridge.R;
import com.viomi.fridge.api.http.HttpConnect;
import com.viomi.fridge.model.bean.recipe.BaseResponse;
import com.viomi.fridge.model.bean.recipe.RecipeDetail;
import com.viomi.fridge.model.bean.recipe.RecipeSeries;
import com.viomi.fridge.model.bean.recipe.Step;
import com.viomi.fridge.util.JsonUitls;
import com.viomi.fridge.util.ToolUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class RecipeSelectedWellModel {

    private RecipeSelectedWellModel.UIHandler mHandler = new RecipeSelectedWellModel.UIHandler();
    private RecipeSelectedWellModel.LoadDataCallback callback;
    private static final int MSG_WHAT_SUCCESS_CODE = 1;
    private static final int MSG_WHAT_FAIL_CODE = 2;
    private ArrayList<RecipeDetail> details;
    private HashMap<Integer, String[]> Maps;
    private Random random = new Random();

    public RecipeSelectedWellModel(LoadDataCallback callback) {
        this.callback = callback;
    }

    private class UIHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MSG_WHAT_SUCCESS_CODE:
                    String json = (String) msg.obj;
                    if (!TextUtils.isEmpty(json)) {
                        BaseResponse baseResponse = JSON.parseObject(json, BaseResponse.class);
                        String code = baseResponse.getRetCode();
                        String result = baseResponse.getResult();
//                        Log.i("info","==============code:"+code);
                        if (!TextUtils.isEmpty(code) && code.equals("200")) {
                            if (!TextUtils.isEmpty(result)) {
//                                callback.onSuccess(JSON.parseObject(result, RecipeSeries.class));
//                                Log.i("info","==============parseRecipeSeries");
                                doHandData(parseRecipeSeries(result));
                            } else {
                                callback.onFail("返回数据为空！");
                            }
                        } else {
                            callback.onFail(baseResponse.getMsg());
                        }
                    }
                    break;
                case MSG_WHAT_FAIL_CODE:
                    if (callback != null) {
                        callback.onFail((String) msg.obj);
                    }
                    break;
            }
        }
    }

    private RecipeSeries parseRecipeSeries(String json) {
        RecipeSeries recipeSeries = null;
        try {
            recipeSeries = new RecipeSeries();
            JSONObject object = new JSONObject(json);
            if (object.has("list")) {
                ArrayList<RecipeDetail> list = new ArrayList<>();
                JSONArray array = object.getJSONArray("list");
                for (int i = 0; i < array.length(); i++) {
                    JSONObject obj = array.getJSONObject(i);
                    RecipeDetail detail = new RecipeDetail();

                    detail.setName(getString(obj, "name"));
                    detail.setCtgTitles(getString(obj, "ctgTitles"));
                    detail.setMenuId(getString(obj, "menuId"));
                    detail.setThumbnail(getString(obj, "thumbnail"));
                    if (obj.has("recipe")) {
                        RecipeDetail.recipe recipe = new RecipeDetail.recipe();
                        JSONObject object1 = obj.getJSONObject("recipe");
                        recipe.setImg(getString(object1, "img"));
                        recipe.setIngredients(getString(object1, "ingredients"));
                        recipe.setTitle(getString(object1, "title"));
                        recipe.setSumary(getString(object1, "sumary"));

                        if (object1.has("method")) {
                            ArrayList<Step> steps = new ArrayList<>();
                            String arryStr = object1.getString("method");
                            JSONArray array1 = new JSONArray(arryStr);
                            for (int j = 0; j < array1.length(); j++) {
                                JSONObject object2 = array1.getJSONObject(j);
                                Step step = new Step();
                                step.setImg(getString(object2, "img"));
                                step.setStep(getString(object2, "step"));
                                steps.add(step);
                            }
                            recipe.setSteps(steps);
                        }
                        detail.setRecipe(recipe);
                    }
                    list.add(detail);
                }
                recipeSeries.setList(list);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            if (callback != null) {
                callback.onFail("数据解析异常!");
            }
        }
        return recipeSeries;
    }

    private String getString(JSONObject object, String key) {
        return JsonUitls.getString(object, key);
    }

    private void doHandData(RecipeSeries recipeSeries) {
        if (recipeSeries != null) {
            ArrayList<RecipeDetail> list = ToolUtil.getList(recipeSeries.getList());
            if (list != null && list.size() != 0) {
                int index = random.nextInt(list.size());
                RecipeDetail detail = list.get(index);
                callback.onSuccess(detail);
            }else {
//                Log.i("info","==============list数据为空！");
                callback.onFail("");
            }
        }
    }

    public void loadData(HashMap<String, String> map) {
        HttpConnect.getRequestHandler(HttpConnect.RECIPE_LIST_URL, map, mHandler, MSG_WHAT_SUCCESS_CODE, MSG_WHAT_FAIL_CODE);
    }

    public interface LoadDataCallback {
        void onSuccess(RecipeDetail detail);

        void onFail(String result);
    }
}
