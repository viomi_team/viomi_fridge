package com.viomi.fridge.mvp.model;


import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;

import com.alibaba.fastjson.JSON;
import com.viomi.fridge.api.http.HttpConnect;
import com.viomi.fridge.model.bean.recipe.BaseResponse;
import com.viomi.fridge.model.bean.recipe.RecipeDetail;
import com.viomi.fridge.model.bean.recipe.Step;
import com.viomi.fridge.util.JsonUitls;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class RecipeDetailsModel implements RecipeModel {

    private UIHandler mHandler = new UIHandler();
    private LoadDataCallback callback;
    private static final int MSG_WHAT_SUCCESS_CODE = 1;
    private static final int MSG_WHAT_FAIL_CODE = 2;

    public RecipeDetailsModel(LoadDataCallback callback) {
        this.callback = callback;
    }

    private class UIHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MSG_WHAT_SUCCESS_CODE:
                    String json = (String) msg.obj;
                    if (!TextUtils.isEmpty(json)) {
                        BaseResponse baseResponse = JSON.parseObject(json, BaseResponse.class);
                        String code = baseResponse.getRetCode();
                        String result = baseResponse.getResult();
                        if (!TextUtils.isEmpty(code) && code.equals("200")) {
                            if (!TextUtils.isEmpty(result)) {
//                                callback.onSuccess(JSON.parseObject(result, RecipeDetail.class));
                                callback.onSuccess(parseRecipe(result));
                            } else {
                                callback.onFail("返回数据为空！");
                            }
                        } else {
                            callback.onFail(baseResponse.getMsg());
                        }
                    }
                    break;
                case MSG_WHAT_FAIL_CODE:
                    if (callback != null) {
                        callback.onFail((String) msg.obj);
                    }
                    break;
            }
        }
    }


    private RecipeDetail parseRecipe(String json) {
        RecipeDetail detail = null;
        try {
            detail = new RecipeDetail();
            JSONObject object = new JSONObject(json);
            detail.setMenuId(JsonUitls.getString(object, "menuId"));
            detail.setThumbnail(JsonUitls.getString(object, "thumbnail"));
            detail.setName(JsonUitls.getString(object, "name"));

            JSONObject obj = object.getJSONObject("recipe");
            RecipeDetail.recipe recipe = new RecipeDetail.recipe();
            recipe.setImg(JsonUitls.getString(obj, "img"));
            recipe.setSumary(JsonUitls.getString(obj, "sumary"));
            recipe.setTitle(JsonUitls.getString(obj, "title"));
            recipe.setIngredients(JsonUitls.getString(obj, "ingredients"));

            if (obj.has("method")){
                String str = JsonUitls.getString(obj, "method");
                if (!TextUtils.isEmpty(str)){
                    JSONArray array = new JSONArray(str);
                    ArrayList<Step> steps = new ArrayList<>();
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject ob = (JSONObject) array.get(i);
                        Step step = new Step();
                        step.setImg(JsonUitls.getString(ob, "img"));
                        step.setStep(JsonUitls.getString(ob, "step"));
                        steps.add(step);
                    }
                    recipe.setSteps(steps);
                }
            }
            detail.setRecipe(recipe);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return detail;
    }

    @Override
    public void loadData(HashMap<String, String> map) {
        HttpConnect.getRequestHandler(HttpConnect.RECIPE_DETAIL_URL, map, mHandler, MSG_WHAT_SUCCESS_CODE, MSG_WHAT_FAIL_CODE);
    }


    public interface LoadDataCallback {
        void onSuccess(RecipeDetail data);

        void onFail(String result);
    }

}
