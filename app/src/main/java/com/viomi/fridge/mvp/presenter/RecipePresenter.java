package com.viomi.fridge.mvp.presenter;

import com.viomi.fridge.model.bean.recipe.RecipeCate;
import com.viomi.fridge.mvp.model.NewMainModel;
import com.viomi.fridge.mvp.model.RecipeCateModelImpl;
import com.viomi.fridge.mvp.model.RecipeModel;
import com.viomi.fridge.mvp.view.RecipeCateView;

import java.util.HashMap;

public class RecipePresenter implements RecipeCateModelImpl.LoadDataCallback {
    private RecipeCateView recipeCateView;
    private RecipeModel recipeModel;

    public RecipePresenter(RecipeCateView recipeCateView) {
        this.recipeCateView = recipeCateView;
        recipeModel = new RecipeCateModelImpl(this);
    }

    public void getCategory(HashMap<String, String> map) {
        recipeCateView.showProgress();
        recipeModel.loadData(map);
    }

    @Override
    public void onSuccess(RecipeCate data) {
        recipeCateView.hideProgress();
        recipeCateView.loadData(data);
    }

    @Override
    public void onFail(String result) {
        recipeCateView.hideProgress();
        recipeCateView.onError(result);
    }
}
