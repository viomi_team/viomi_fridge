package com.viomi.fridge.mvp.presenter;

import com.viomi.fridge.model.bean.recipe.RecipeDetail;
import com.viomi.fridge.model.bean.recipe.RecipeSeries;
import com.viomi.fridge.mvp.model.RecipeDetailsModel;
import com.viomi.fridge.mvp.model.RecipeSeriesModel;
import com.viomi.fridge.mvp.view.RecipeDetailsView;
import com.viomi.fridge.mvp.view.RecipeSeriesView;

import java.util.HashMap;

public class RecipeSeriesPresenter implements RecipeSeriesModel.LoadDataCallback {
    private RecipeSeriesView recipeCateView;
    private RecipeSeriesModel recipeModel;

    public RecipeSeriesPresenter(RecipeSeriesView recipeCateView) {
        this.recipeCateView = recipeCateView;
        recipeModel = new RecipeSeriesModel(this);
    }

    public void getRecipeSeries(HashMap<String, String> map) {
        recipeCateView.showProgress();
        recipeModel.loadData(map);
    }

    @Override
    public void onSuccess(RecipeSeries data) {
        recipeCateView.hideProgress();
        recipeCateView.loadData(data);
    }

    @Override
    public void onFail(String result) {
        recipeCateView.hideProgress();
        recipeCateView.onError(result);
    }
}
