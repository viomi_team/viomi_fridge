package com.viomi.fridge.mvp.presenter;

import com.viomi.fridge.model.bean.recipe.RecipeCate;
import com.viomi.fridge.model.bean.recipe.RecipeDetail;
import com.viomi.fridge.mvp.model.RecipeCateModelImpl;
import com.viomi.fridge.mvp.model.RecipeDetailsModel;
import com.viomi.fridge.mvp.model.RecipeModel;
import com.viomi.fridge.mvp.view.RecipeCateView;
import com.viomi.fridge.mvp.view.RecipeDetailsView;

import java.util.HashMap;

public class RecipeDetailsPresenter implements RecipeDetailsModel.LoadDataCallback {
    private RecipeDetailsView recipeCateView;
    private RecipeDetailsModel recipeModel;

    public RecipeDetailsPresenter(RecipeDetailsView recipeCateView) {
        this.recipeCateView = recipeCateView;
        recipeModel = new RecipeDetailsModel(this);
    }

    public void getDetails(HashMap<String, String> map) {
        recipeCateView.showProgress();
        recipeModel.loadData(map);
    }

    @Override
    public void onSuccess(RecipeDetail data) {
        recipeCateView.hideProgress();
        recipeCateView.loadData(data);
    }

    @Override
    public void onFail(String result) {
        recipeCateView.hideProgress();
        recipeCateView.onError(result);
    }
}
