package com.viomi.fridge.mvp.model;


import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;

import com.alibaba.fastjson.JSON;
import com.viomi.fridge.api.http.HttpConnect;
import com.viomi.fridge.model.bean.recipe.BaseResponse;
import com.viomi.fridge.model.bean.recipe.RecipeCate;
import com.viomi.fridge.model.bean.recipe.Secondlevel;
import com.viomi.fridge.model.bean.recipe.categoryInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class RecipeCateModelImpl implements RecipeModel {

    private UIHandler mHandler = new UIHandler();
    private LoadDataCallback callback;
    private static final int MSG_WHAT_SUCCESS_CODE = 1;
    private static final int MSG_WHAT_FAIL_CODE = 2;

    public RecipeCateModelImpl(LoadDataCallback callback) {
        this.callback = callback;
    }

    private class UIHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MSG_WHAT_SUCCESS_CODE:
                    String json = (String) msg.obj;
                    Log.i("info", "===========json:" + json);
                    if (!TextUtils.isEmpty(json)) {
//                        BaseResponse baseResponse = GsonUtil.parseJsonWithGson(json, new TypeToken<BaseResponse>() {
//                        }.getType());
                        BaseResponse baseResponse = JSON.parseObject(json, BaseResponse.class);
                        String code = baseResponse.getRetCode();
                        String result = baseResponse.getResult();
                        if (!TextUtils.isEmpty(code) && code.equals("200")) {
                            RecipeCate recipeCate = parseJson(result);
                            callback.onSuccess(recipeCate);
                        } else {
                            callback.onFail(baseResponse.getMsg());
                        }
                    }
                    break;
                case MSG_WHAT_FAIL_CODE:
                    if (callback != null) {
                        callback.onFail((String) msg.obj);
                    }
                    break;
            }
        }
    }


    private RecipeCate parseJson(String json){
        RecipeCate recipeCate=null;
        try {
             recipeCate=new RecipeCate();
            JSONObject  object=new JSONObject(json);
            categoryInfo categoryInfo=parseCategory(object.getJSONObject("categoryInfo"));
            recipeCate.setCategoryInfo(categoryInfo);

            ArrayList<Secondlevel> list=new ArrayList<>();
            JSONArray array=object.getJSONArray("childs");

            for (int i=0;i<array.length();i++){
               JSONObject object1= (JSONObject) array.get(i);

               categoryInfo level2=parseCategory(object1.getJSONObject("categoryInfo"));
               JSONArray array2=object1.getJSONArray("childs");

                Secondlevel secondlevel=new Secondlevel();
                secondlevel.setCategoryInfo(level2);

                ArrayList<categoryInfo> list1=new ArrayList<>();
                for (int j=0;j<array2.length();j++) {
                    JSONObject object2 = (JSONObject) array2.get(j);
                    categoryInfo categoryInfo1=parseCategory(object2.getJSONObject("categoryInfo"));
                    list1.add(categoryInfo1);
                }
                secondlevel.setChilds(list1);
                list.add(secondlevel);
            }
            recipeCate.setChilds(list);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return recipeCate;
    }

    private categoryInfo parseCategory(JSONObject object){
        String json=object.toString();
        return JSON.parseObject(json,categoryInfo.class);
    }

    @Override
    public void loadData(HashMap<String, String> map) {
        HttpConnect.getRequestHandler(HttpConnect.RECIPE_CATE_URL, map, mHandler, MSG_WHAT_SUCCESS_CODE, MSG_WHAT_FAIL_CODE);
    }


    public interface LoadDataCallback {
        void onSuccess(RecipeCate data);

        void onFail(String result);
    }

}
