package com.viomi.fridge.mvp.view;

import com.viomi.fridge.model.bean.recipe.RecipeCate;

public interface RecipeCateView {
    void showProgress();
    void hideProgress();
    void loadData(RecipeCate data);
    void onError(String error);
}
