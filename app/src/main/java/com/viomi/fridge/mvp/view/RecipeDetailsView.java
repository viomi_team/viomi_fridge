package com.viomi.fridge.mvp.view;

import com.viomi.fridge.model.bean.recipe.RecipeCate;
import com.viomi.fridge.model.bean.recipe.RecipeDetail;

public interface RecipeDetailsView {
    void showProgress();
    void hideProgress();
    void loadData(RecipeDetail data);
    void onError(String error);
}
