package com.viomi.fridge.mvp.model;

import com.viomi.fridge.model.bean.GoodsEntity;
import com.viomi.fridge.model.bean.HomeWidgetBean;

import java.util.HashMap;
import java.util.List;

public interface RecipeModel {

    void loadData(HashMap<String,String> map);
}
