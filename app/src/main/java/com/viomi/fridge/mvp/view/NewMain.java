package com.viomi.fridge.mvp.view;

import com.viomi.fridge.model.bean.GoodsEntity;
import com.viomi.fridge.model.bean.HomeWidgetBean;
import com.viomi.fridge.mvp.bean.FridgeTempBean;
import com.viomi.fridge.mvp.bean.WeatherBean;

import java.util.List;

/**
 * Created by Mocc on 2017/6/15
 */

public interface NewMain {

    void showProgressbar();

    void dismissProgressbar();

    void updateHeaderIcon_talk(int resId);

    void updateHeaderIcon_voice(boolean isSilent,boolean isBTConnected);

    void updateIcon_blueTooth(boolean isopen);

    void updateIcon_user();

    void updateIcon_update(boolean isReadyUpdate);

    void setSoundBarValue(int value);

    void updateFridgeStatus(FridgeTempBean bean);

    void updateAlbum();

    void updateMessage();

    void updateFoodList();

    void updateWeather(WeatherBean weather);

    void updateFoodMenu(List<HomeWidgetBean> cooklist);

    void updateAdsRecommend(List<List<GoodsEntity>> list, int times);

    void loationWeather();

    void refershInfoNew(String tips,int advertNews, int userNews, int deviceNews);

    void showConnectHint();

    void disConnectHint();

    interface EventCallback{
        void onCallback();
    }

}
