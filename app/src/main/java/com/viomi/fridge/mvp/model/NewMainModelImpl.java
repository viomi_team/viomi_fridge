package com.viomi.fridge.mvp.model;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.miot.api.MiotManager;
import com.miot.common.people.People;
import com.viomi.common.callback.AppCallback;
import com.viomi.fridge.R;
import com.viomi.fridge.api.dao.InfoManager;
import com.viomi.fridge.api.http.HttpConnect;
import com.viomi.fridge.common.GlobalParams;
import com.viomi.fridge.common.ViomiApplication;
import com.viomi.fridge.device.AppConfig;
import com.viomi.fridge.device.DeviceConfig;
import com.viomi.fridge.manager.AccountManager;
import com.viomi.fridge.manager.ControlManager;
import com.viomi.fridge.manager.RoomSceneManager;
import com.viomi.fridge.manager.VoiceManager;
import com.viomi.fridge.model.bean.GoodsEntity;
import com.viomi.fridge.model.bean.HomeWidgetBean;
import com.viomi.fridge.model.bean.TCRoomScene;
import com.viomi.fridge.model.bean.ViomiUser;
import com.viomi.fridge.mvp.bean.FridgeTempBean;
import com.viomi.fridge.mvp.bean.WeatherBean;
import com.viomi.fridge.mvp.view.NewMain;
import com.viomi.fridge.util.JsonUitls;
import com.viomi.fridge.util.ResponseCode;
import com.viomi.fridge.util.log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by Mocc on 2017/6/15
 */

public class NewMainModelImpl implements NewMainModel {

    private MyHandler mHandler = new MyHandler();

    private LoadDataCallback callback;
    private final static String TAG = "NewMainModelImpl";

    private List<HomeWidgetBean> list1;
    private List<HomeWidgetBean> list2;
    private List<List<GoodsEntity>> list3;

    private WeatherCallback weatherCallback;
    private FridgeCallback fridgeCallback;
    private VoiceStatusCallback statusCallback;
    private HomeRefreshCallback homeRefreshCallback;
    private WeatherRefleshCallback weatherRefleshCallback;
    private NewMain.EventCallback eventCallback;
    private InfoCallback mNewInfoCallback;
    private int timeCount;

    private final static int MSG_WHAT_CHECK_INFO_NEW = 100;//获取最新消息


    private class MyHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {

                case 0: {
                    int t1 = ControlManager.getInstance().getDataSendInfo().cold_closet_temp_set;
                    String st1 = t1 == -50 ? "--" : t1 + "";
                    if(!ControlManager.getInstance().getDataSendInfo().cold_closet_room_enable){
                        st1="--";
                    }

                    int t2 = ControlManager.getInstance().getDataSendInfo().temp_changeable_room_temp_set;
                    String st2 = t2 == -50 ? "--" : t2 + "";
                    if(!ControlManager.getInstance().getDataSendInfo().temp_changeable_room_room_enable){
                        st2="--";
                    }

                    String scene = GlobalParams.getInstance().getSceneChoose();
                    ArrayList<TCRoomScene> roomScenes = RoomSceneManager.getInstance().getSceneChooseList();
                    int temp = 100;
                    for (int i = 0; i < roomScenes.size(); i++) {
                        if (roomScenes.get(i).name.equals(scene)) {
                            temp = roomScenes.get(i).value;
                            break;
                        }
                    }
                    if (!((!scene.equals(GlobalParams.Value_Scene_ChooseName)) && temp == ControlManager.getInstance().getDataSendInfo().temp_changeable_room_temp_set
                            && ControlManager.getInstance().getDataSendInfo().temp_changeable_room_room_enable)) {
                        scene = "变温室℃";
                    }

                    int t3 = ControlManager.getInstance().getDataSendInfo().freezing_room_temp_set;
                 //   String st3 = t3 == -50 ? "--" : t3 + "";
                    int mMinTemp=-25;//最低可显示温度，用于速冻时温度低于最大可调范围显示
                    if(DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V2)||DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V3)
                            ||DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V31)){
                        mMinTemp=-24;
                    }
                    if(t3<mMinTemp){
                        t3=mMinTemp;
                    }
                    String st3= t3 + "";

                    FridgeTempBean bean = new FridgeTempBean(st1, st2, scene, st3);
                    fridgeCallback.onCallback(bean);
                    statusCallback.onCallback(getVoiceStatus());
                    getVoiceStatus();

                    if (timeCount % (4 * 6 * 60) == 0) {
                        //check update per 4 hours event callback
                        eventCallback.onCallback();
                    }

                    //定时刷新首页和天气
                    if (timeCount > 0) {
                        if (timeCount % (24 * 6 * 60) == 0) {
                            homeRefreshCallback.onCallback();
                        }

                        if (timeCount % (4 * 6 * 60) == 0) {
                            weatherRefleshCallback.onCallback();
                        }

                        if (timeCount > 2000000000) {
                            timeCount = 0;
                        }
                    }
                    timeCount++;

                    mHandler.sendEmptyMessageDelayed(0, 10 * 1000);
                    break;
                }

                case 1: {
                    String result = (String) msg.obj;
                    log.myE(TAG + "ok", result + "");
                    parseJson(result);
                    break;
                }

                case 2: {
                    String result = msg.obj.toString();
                    log.myE(TAG + "fail", result + "");
                    callback.onFail(result);
                    break;
                }

                case 3: {
                    String result = (String) msg.obj;
                    parseWeatherJson(result);
                    break;
                }
                case MSG_WHAT_CHECK_INFO_NEW:

                    int lastDeviceInfoId = 0, lastUserInfoId = 0, lastAdvertInfoId = 0;
                    if (msg.obj != null) {
                        try {
                            JSONObject jsonObject = new JSONObject((String) msg.obj);
                            // lastDeviceInfoId=jsonObject.getInt("systemInfoId")-InfoManager.getInstance().getLastDeviceInfoId();
                            lastDeviceInfoId = 0;
                            lastUserInfoId = jsonObject.getInt("userInfoId") - InfoManager.getInstance().getLastUserInfoId();
                            lastAdvertInfoId = jsonObject.getInt("activityInfoId") - InfoManager.getInstance().getLastAdvertInfoId();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    //有本地未读消息，显示未读显示title
                    if (InfoManager.getInstance().getLastAdvertInfoTitle() != null) {
                        mNewInfoCallback.onNewInfo(InfoManager.getInstance().getLastAdvertInfoTitle(), lastAdvertInfoId, lastUserInfoId, lastDeviceInfoId);
                        return;
                    } else if (InfoManager.getInstance().getLastUserInfoTitle() != null) {
                        mNewInfoCallback.onNewInfo(InfoManager.getInstance().getLastUserInfoTitle(), lastAdvertInfoId, lastUserInfoId, lastDeviceInfoId);
                        return;
                    } else if (InfoManager.getInstance().getLastDeviceInfoTitle() != null) {
                        mNewInfoCallback.onNewInfo(InfoManager.getInstance().getLastDeviceInfoTitle(), lastAdvertInfoId, lastUserInfoId, lastDeviceInfoId);
                        return;
                    }
                    int sum ;
                    if(AppConfig.VIOMI_FRIDGE_V31.equals(DeviceConfig.MODEL)){
                        sum = lastDeviceInfoId ;
                    }else {
                        sum = lastDeviceInfoId + lastUserInfoId + lastAdvertInfoId;
                    }
                    if (sum > 0) {
                        String text = ViomiApplication.getContext().getString(R.string.text_exist_unread_message);
                        text = text.replace("**", "" + sum);
                        mNewInfoCallback.onNewInfo(text, lastAdvertInfoId, lastUserInfoId, lastDeviceInfoId);
                        return;
                    } else {
                        mNewInfoCallback.onNewInfo(ViomiApplication.getContext().getString(R.string.text_no_unread_message), lastAdvertInfoId, lastUserInfoId, lastDeviceInfoId);
                    }

                    break;
                default:
                    break;
            }
        }
    }


    @Override
    public void loadData(LoadDataCallback callback) {

        this.callback = callback;
        Map<String, String> map = new HashMap<>();
        map.put("layoutType", "1");
        HttpConnect.getRequestHandler(HttpConnect.HOME_PAGE_NEW, map, mHandler, 1, 2);
    }


    private void parseJson(String result) {

        list1 = new ArrayList<>();
        list2 = new ArrayList<>();
        list3 = new ArrayList<>();

        try {
            JSONObject json = new JSONObject(result);
            JSONObject mobBaseRes = JsonUitls.getJSONObject(json, "mobBaseRes");
            String code = JsonUitls.getString(mobBaseRes, "code");
            String desc = JsonUitls.getString(mobBaseRes, "desc");

            if (ResponseCode.isSuccess2(code, desc)) {

                JSONObject jsonResult = JsonUitls.getJSONObject(mobBaseRes, "result");
                JSONArray widgets = JsonUitls.getJSONArray(jsonResult, "widgets");

                for (int i = 0; i < widgets.length(); i++) {
                    JSONObject item = widgets.getJSONObject(i);
                    int widgetType = JsonUitls.getInt(item, "widgetType");

                    switch (widgetType) {
                        case 2: {
                            //主轮播图
                            parseBean1(list1, item);
                            break;
                        }
                        case 4: {
                            //菜谱
                            parseBean2(list2, item);
                            break;
                        }
                        case 5: {
                            //二级轮播图
                            parseBean3(list3, item);
                            break;
                        }
                        default:
                            break;
                    }
                }
                callback.onSuccess(list1, list2, list3);
            } else {
                callback.onFail(result);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            callback.onFail(result);
        }

    }

    private void parseBean1(List<HomeWidgetBean> list, JSONObject item) throws JSONException {
        JSONArray children = JsonUitls.getJSONArray(item, "children");
        for (int j = 0; j < children.length(); j++) {
            JSONObject data = children.getJSONObject(j);
            int idx = JsonUitls.getInt(data, "idx");
            JSONObject attrs = JsonUitls.getJSONObject(data, "attrs");
            String linkUrl = JsonUitls.getString(attrs, "link");
            String imageUrl = JsonUitls.getString(attrs, "img");
            String type = JsonUitls.getString(attrs, "type");
            list.add(new HomeWidgetBean(linkUrl, imageUrl, type, idx));
        }
        sortArray(list);
    }

    private List<HomeWidgetBean> sortArray(List<HomeWidgetBean> dataList) {
        for (int i = 0; i < dataList.size() - 1; i++) {
            for (int j = 0; j < dataList.size() - 1 - i; j++) {
                if (dataList.get(j).getIdx() > dataList.get(j + 1).getIdx()) {
                    HomeWidgetBean temp1 = dataList.get(j);
                    HomeWidgetBean temp2 = dataList.get(j + 1);
                    dataList.set(j, temp2);
                    dataList.set(j + 1, temp1);
                }
            }
        }
        return dataList;
    }

    private void parseBean2(List<HomeWidgetBean> list, JSONObject item) throws JSONException {
        JSONObject attrs = JsonUitls.getJSONObject(item, "attrs");
        String linkUrl = JsonUitls.getString(attrs, "link");
        String imageUrl = JsonUitls.getString(attrs, "img");
        if (!"-".equals(linkUrl)) {
            list.add(new HomeWidgetBean(linkUrl, imageUrl));
        }
    }

    private void parseBean3(List<List<GoodsEntity>> list, JSONObject item) throws JSONException {
        JSONArray children = JsonUitls.getJSONArray(item, "children");
        for (int i = 0; i < children.length(); i++) {
            JSONObject data = children.getJSONObject(i);
            JSONArray children_sec = JsonUitls.getJSONArray(data, "children");
            List<GoodsEntity> beanList = new ArrayList<>();
            for (int j = 0; j < children_sec.length(); j++) {
                JSONObject data2 = children_sec.getJSONObject(j);
                JSONObject attrs = JsonUitls.getJSONObject(data2, "attrs");
                String name = JsonUitls.getString(attrs, "name");
                String imageUrl = JsonUitls.getString(attrs, "img");
                double price = JsonUitls.getDouble(attrs, "prod_real_price");

                String sku_id = JsonUitls.getString(attrs, "sku_id");
                String spu_id = JsonUitls.getString(attrs, "spu_id");

                String targetId = "";
                int targetType = 0;
                if (!"-".equals(sku_id)) {
                    targetType = 2;
                    targetId = sku_id;
                } else {
                    if (!"-".equals(spu_id)) {
                        targetType = 1;
                        targetId = spu_id;
                    }
                }

                beanList.add(new GoodsEntity(name, imageUrl, price, targetId, targetType));
            }
            list.add(beanList);
        }
    }

    @Override
    public int getTimes(List list) {
        int times = 0;
        switch (list.size()) {
            case 0:
                break;
            case 1:
                times = 4;
                break;
            case 2:
                times = 2;
                break;
            default:
                times = 1;
                break;
        }
        return times;
    }


    //获取城市天气
    @Override
    public void getCityWeather(String cityName, WeatherCallback callback) {
        this.weatherCallback = callback;
        Log.i(TAG, "getCityWeather");
        VoiceManager.getInstance().getWeather(cityName, new AppCallback<String>() {
            @Override
            public void onSuccess(String data) {
                log.myE(TAG, "weather---onSuccess---" + data + Thread.currentThread());
                Message msg = mHandler.obtainMessage();
                msg.what = 3;
                msg.obj = data;
                mHandler.sendMessage(msg);
            }

            @Override
            public void onFail(int errorCode, String msg) {
                log.myE(TAG, "weather---onFail---" + msg);
                weatherCallback.onFail(errorCode, msg);
            }
        });
    }

    //语音播放刷新首页天气
    @Override
    public void referWeather(String info) {
        if (mHandler != null && info != null && weatherCallback != null) {
            Message msg = mHandler.obtainMessage();
            msg.what = 3;
            msg.obj = info;
            mHandler.sendMessage(msg);
        }
    }

    private void parseWeatherJson(String result) {
        try {
            JSONObject json = new JSONObject(result);
            JSONObject data = JsonUitls.getJSONObject(json, "data");
            JSONArray resultArray = JsonUitls.getJSONArray(data, "result");
            if (resultArray.length() > 0) {
                JSONObject todayData = resultArray.getJSONObject(0);

                String city = JsonUitls.getString(todayData, "city");
                String weather = JsonUitls.getString(todayData, "weather");
                String tempRange_s = JsonUitls.getString(todayData, "tempRange");
                String pm25 = JsonUitls.getString(todayData, "pm25");
                String airQuality = JsonUitls.getString(todayData, "airQuality");
                String wind = JsonUitls.getString(todayData, "wind");

                weatherCallback.onSuccess(new WeatherBean(city, weather, tempRange_s, pm25, airQuality, wind));

                String report = city + "," + weather + "," + tempRange_s + "," + wind + ",空气质量:" + airQuality + ",Pm2.5：" + pm25;
                GlobalParams.getInstance().setWeatherReport(report);
                GlobalParams.getInstance().setOutdoorTemp(tempRange_s);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            weatherCallback.onFail(-1, "weather-json-error");
        }
    }

    @Override
    public void refresh10Seconds(FridgeCallback callback, VoiceStatusCallback statusCallback, HomeRefreshCallback homeRefreshCallback, WeatherRefleshCallback weatherRefleshCallback, NewMain.EventCallback eventCallback) {
        this.fridgeCallback = callback;
        this.statusCallback = statusCallback;
        this.homeRefreshCallback = homeRefreshCallback;
        this.weatherRefleshCallback = weatherRefleshCallback;
        this.eventCallback=eventCallback;
        mHandler.sendEmptyMessage(0);
    }

    @Override
    public void release() {
        mHandler.removeCallbacksAndMessages(null);
    }

    @Override
    public int getVoiceStatus() {
        int resId = 0;
        if (GlobalParams.getInstance().isVoiceEnabe()) {
            if (VoiceManager.getInstance().isVoiceOccupy()) {
                resId = R.drawable.main_icon_voice_2_v2;
            } else {
                resId = R.drawable.main_icon_voice_1_v2;
            }
        } else {
            resId =  R.drawable.main_icon_voice_0_v2;
        }
        return resId;
    }

    @Override
    public void getNewInfo(InfoCallback callback) {
        mNewInfoCallback = callback;
        String mid, yid;
        ViomiUser user = AccountManager.getViomiUser(ViomiApplication.getContext());
        People people = MiotManager.getPeople();
        if (user == null) {
            yid = "0";
        } else {
            yid = user.getAccount();
        }
        if (people == null) {
            mid = "0";
        } else {
            mid = people.getUserId();
        }

        if(!AppConfig.VIOMI_FRIDGE_V31.equals(DeviceConfig.MODEL)){
            HttpConnect.checkNewInfos(mid, yid, new AppCallback<String>() {
                @Override
                public void onSuccess(String data) {
                    log.d(TAG, "checkNewInfos onResponse!msg=" + data);
                    if (mHandler != null) {
                        Message message = mHandler.obtainMessage();
                        message.obj = data;
                        message.what = MSG_WHAT_CHECK_INFO_NEW;
                        mHandler.sendMessage(message);
                    }
                }

                @Override
                public void onFail(int errorCode, String msg) {
                    Log.e(TAG, "checkNewInfos fail!msg=" + msg);
                    if (mHandler != null) {
                        Message message = mHandler.obtainMessage();
                        message.obj = null;
                        message.what = MSG_WHAT_CHECK_INFO_NEW;
                        mHandler.sendMessage(message);
                    }
                }
            });
        }

    }

}
