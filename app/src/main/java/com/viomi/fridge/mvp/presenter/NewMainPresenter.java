package com.viomi.fridge.mvp.presenter;

import com.viomi.fridge.model.bean.GoodsEntity;
import com.viomi.fridge.model.bean.HomeWidgetBean;
import com.viomi.fridge.mvp.bean.FridgeTempBean;
import com.viomi.fridge.mvp.bean.WeatherBean;
import com.viomi.fridge.mvp.model.NewMainModel;
import com.viomi.fridge.mvp.model.NewMainModelImpl;
import com.viomi.fridge.mvp.view.NewMain;
import com.viomi.fridge.util.log;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Created by Mocc on 2017/6/15
 */

public class NewMainPresenter<T extends NewMain> {

    private WeakReference<T> viewRef;
    private NewMainModel model = new NewMainModelImpl();
    private final static String TAG = "NewMainPresenter";
    private NewMain.EventCallback eventCallback;

    public NewMainPresenter() {
    }

    public NewMain getView() {
        NewMain newMain =null;
        if (viewRef!=null) {
            newMain = viewRef.get();
        }
        return newMain;
    }

    public void attacthView(T view) {
        viewRef = new WeakReference<T>(view);
    }

    public void detacthView() {
        if (viewRef != null) {
            viewRef.clear();
        }
        model.release();
    }


    public void loadData() {
        if (getView() != null) {
            getView().showProgressbar();
        }

        model.loadData(new NewMainModel.LoadDataCallback() {
            @Override
            public void onSuccess(List<HomeWidgetBean> list1, List<HomeWidgetBean> list2, List<List<GoodsEntity>> list3) {
                if (getView() != null) {
                    getView().dismissProgressbar();
                    getView().updateFoodMenu(list2);
                    getView().updateAdsRecommend(list3, model.getTimes(list3));
                    log.myE(TAG, "loadData-onSuccess");
                }
            }

            @Override
            public void onFail(String result) {
                if (getView() != null) {
                    getView().dismissProgressbar();
                }
                log.myE(TAG, "loadData-onFail-" + result);
            }
        });
    }


    public void locationToWeather(String cityName) {

        model.getCityWeather(cityName, new NewMainModel.WeatherCallback() {
            @Override
            public void onSuccess(WeatherBean weather) {
                if (getView() != null) {
                    getView().updateWeather(weather);
                }
            }

            @Override
            public void onFail(int errorCode, String msg) {

            }
        });
    }

    public void referWeather(String info){
        model.referWeather(info);
    }

    public void refresh10Seconds() {
        model.refresh10Seconds(new NewMainModel.FridgeCallback() {
            @Override
            public void onCallback(FridgeTempBean bean) {
                if (getView() != null) {
                    getView().updateFridgeStatus(bean);
                }
            }
        }, new NewMainModel.VoiceStatusCallback() {
            @Override
            public void onCallback(int resId) {
                if (getView() != null) {
                    getView().updateHeaderIcon_talk(resId);
                }
            }
        }, new NewMainModel.HomeRefreshCallback() {
            @Override
            public void onCallback() {
                loadData();
            }
        }, new NewMainModel.WeatherRefleshCallback() {
            @Override
            public void onCallback() {
                if (getView() != null) {
                    getView().loationWeather();
                }
            }
        },eventCallback);
    }

    public void setEventCallback(NewMain.EventCallback eventCallback){
        this.eventCallback=eventCallback;
    }

    public void refreshTalkStatus() {
        if (getView() != null) {
            getView().updateHeaderIcon_talk(model.getVoiceStatus());
        }
    }

    public void refershNewInfo(){
        model.getNewInfo(new NewMainModel.InfoCallback() {
            @Override
            public void onNewInfo(String string,int advertNews, int userNews, int deviceNews) {
                getView().refershInfoNew(string,advertNews,userNews,deviceNews);
            }
        });
    }



}
