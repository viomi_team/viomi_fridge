package com.viomi.fridge.mvp.bean;

/**
 * Created by Mocc on 2017/6/22
 */

public class FridgeTempBean {

    private String temp1;
    private String temp2;
    private String temp2_model;
    private String temp3;

    public FridgeTempBean() {
    }

    public FridgeTempBean(String temp1, String temp2, String temp2_model, String temp3) {
        this.temp1 = temp1;
        this.temp2 = temp2;
        this.temp2_model = temp2_model;
        this.temp3 = temp3;
    }

    public String getTemp1() {
        return temp1;
    }

    public void setTemp1(String temp1) {
        this.temp1 = temp1;
    }

    public String getTemp2() {
        return temp2;
    }

    public void setTemp2(String temp2) {
        this.temp2 = temp2;
    }

    public String getTemp2_model() {
        return temp2_model;
    }

    public void setTemp2_model(String temp2_model) {
        this.temp2_model = temp2_model;
    }

    public String getTemp3() {
        return temp3;
    }

    public void setTemp3(String temp3) {
        this.temp3 = temp3;
    }
}
