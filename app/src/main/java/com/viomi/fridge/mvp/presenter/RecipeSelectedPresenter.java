package com.viomi.fridge.mvp.presenter;

import com.viomi.fridge.model.bean.recipe.RecipeDetail;
import com.viomi.fridge.model.bean.recipe.RecipeSeries;
import com.viomi.fridge.mvp.model.RecipeSelectedWellModel;
import com.viomi.fridge.mvp.model.RecipeSeriesModel;
import com.viomi.fridge.mvp.view.RecipeSelectedView;
import com.viomi.fridge.mvp.view.RecipeSeriesView;

import java.util.HashMap;

public class RecipeSelectedPresenter implements RecipeSelectedWellModel.LoadDataCallback {
    private RecipeSelectedView recipeCateView;
    private RecipeSelectedWellModel recipeModel;

    public RecipeSelectedPresenter(RecipeSelectedView recipeCateView) {
        this.recipeCateView = recipeCateView;
        recipeModel = new RecipeSelectedWellModel(this);
    }


    public void loadData(HashMap<String, String> map){
        recipeCateView.showProgress();
        recipeModel.loadData(map);
    }

    @Override
    public void onSuccess(RecipeDetail data) {
        recipeCateView.hideProgress();
        recipeCateView.loadData(data);
    }

    @Override
    public void onFail(String result) {
        recipeCateView.hideProgress();
        recipeCateView.onError(result);
    }
}
