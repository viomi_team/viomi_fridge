package com.viomi.fridge.mvp.bean;

/**
 * Created by Mocc on 2017/6/21
 */

public class WeatherBean {

    private String city;
    private String weather;
    private String tempRange_s;
    private String pm25;
    private String airQuality;
    private String wind;

    public WeatherBean() {

    }

    public WeatherBean(String city, String weather, String tempRange_s, String pm25, String airQuality, String wind) {
        this.city = city;
        this.weather = weather;
        this.tempRange_s = tempRange_s;
        this.pm25 = pm25;
        this.airQuality = airQuality;
        this.wind = wind;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getWeather() {
        return weather;
    }

    public void setWeather(String weather) {
        this.weather = weather;
    }

    public String getTempRange_s() {
        return tempRange_s;
    }

    public void setTempRange_s(String tempRange_s) {
        this.tempRange_s = tempRange_s;
    }

    public String getPm25() {
        return pm25;
    }

    public void setPm25(String pm25) {
        this.pm25 = pm25;
    }

    public String getAirQuality() {
        return airQuality;
    }

    public void setAirQuality(String airQuality) {
        this.airQuality = airQuality;
    }

    public String getWind() {
        return wind;
    }

    public void setWind(String wind) {
        this.wind = wind;
    }
}
