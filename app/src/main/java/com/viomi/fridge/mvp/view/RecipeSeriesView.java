package com.viomi.fridge.mvp.view;

import com.viomi.fridge.model.bean.recipe.RecipeDetail;
import com.viomi.fridge.model.bean.recipe.RecipeSeries;

public interface RecipeSeriesView {
    void showProgress();

    void hideProgress();

    void loadData(RecipeSeries data);

    void onError(String error);
}
