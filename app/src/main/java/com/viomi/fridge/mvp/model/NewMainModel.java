package com.viomi.fridge.mvp.model;

import com.viomi.fridge.model.bean.GoodsEntity;
import com.viomi.fridge.model.bean.HomeWidgetBean;
import com.viomi.fridge.mvp.bean.FridgeTempBean;
import com.viomi.fridge.mvp.bean.WeatherBean;
import com.viomi.fridge.mvp.view.NewMain;

import java.util.List;

/**
 * Created by Mocc on 2017/6/15
 */

public interface NewMainModel {


    interface LoadDataCallback {
        void onSuccess(List<HomeWidgetBean> list1, List<HomeWidgetBean> list2, List<List<GoodsEntity>> list3);

        void onFail(String result);
    }

    void loadData(LoadDataCallback callback);

    int getTimes(List list);

    interface WeatherCallback {
        void onSuccess(WeatherBean weather);

        void onFail(int errorCode, String msg);
    }

    void getCityWeather(String cityName, WeatherCallback callback);

    void referWeather(String info);//语音播放天气时刷新首页天气

    interface FridgeCallback {
        void onCallback(FridgeTempBean bean);
    }

    interface VoiceStatusCallback {
        void onCallback(int resId);
    }

    interface HomeRefreshCallback {
        void onCallback();
    }

    interface WeatherRefleshCallback {
        void onCallback();
    }

    void refresh10Seconds(FridgeCallback callback,
                          VoiceStatusCallback statusCallback,
                          HomeRefreshCallback homeRefreshCallback,
                          WeatherRefleshCallback weatherRefleshCallback,
                          NewMain.EventCallback eventCallback
    );

    int getVoiceStatus();

    interface InfoCallback {
        void onNewInfo(String string, int advertNews, int userNews, int deviceNews);
    }

    void getNewInfo(InfoCallback callback);//获取最新消息

    void release();
}
