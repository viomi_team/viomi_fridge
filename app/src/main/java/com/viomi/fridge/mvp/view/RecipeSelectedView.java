package com.viomi.fridge.mvp.view;

import com.viomi.fridge.model.bean.recipe.RecipeDetail;
import com.viomi.fridge.model.bean.recipe.RecipeSeries;

public interface RecipeSelectedView {
    void showProgress();

    void hideProgress();

    void loadData(RecipeDetail data);

    void onError(String error);
}
