package com.viomi.fridge.model.bean.recipe;

import com.viomi.fridge.model.bean.xunfei.XFMenuResp;

import java.util.ArrayList;
import java.util.List;

public class RecipeModel {
    public String tag;//描述 ，午餐,煎锅,热菜,荤菜,下酒菜,家常味,咸鲜,煎,家常菜,晚餐,一人食,一家三口,朋友聚餐,锅具,咸,中国菜
    public String ingredient;//主要食材
    public String title;//菜名
    public String accessory;//辅助食材
    public String steps;//步骤
    public String imgUrl;
    public List<StepMethod> stepsWithImg=new ArrayList<>();

    public  class StepMethod{
        public String image;
        public String content;

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getIngredient() {
        return ingredient;
    }

    public void setIngredient(String ingredient) {
        this.ingredient = ingredient;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAccessory() {
        return accessory;
    }

    public void setAccessory(String accessory) {
        this.accessory = accessory;
    }

    public String getSteps() {
        return steps;
    }

    public void setSteps(String steps) {
        this.steps = steps;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public List<StepMethod> getStepsWithImg() {
        return stepsWithImg;
    }

    public void setStepsWithImg(List<StepMethod> stepsWithImg) {
        this.stepsWithImg = stepsWithImg;
    }
}
