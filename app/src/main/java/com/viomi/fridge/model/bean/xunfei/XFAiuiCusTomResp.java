package com.viomi.fridge.model.bean.xunfei;

import java.util.List;

/**
 * Created by young2 on 2017/8/9.
 */

public class XFAiuiCusTomResp extends XunFeiResp {

    public List<Semantic> semantic;

      public class Semantic{
          public String intent;
          public List<Slot> slots;
      }

    public class Slot{
        public String name;//实体名
        public String normValue;//实体对应
        public String value;//返回结果
    }
}
