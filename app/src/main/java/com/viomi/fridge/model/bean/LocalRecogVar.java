package com.viomi.fridge.model.bean;

/**
 * 本地语音识别变量
 * Created by young2 on 2017/3/5.
 */

public class LocalRecogVar {

    public final static String  ModeAccess="<ModeAccess>";//冰箱模式变量
    public final static String  ModelFunc="<ModelFunc>";
    public final static String  ModeOprate="<ModeOprate>";
    public final static String  ModelType="<ModelType>";
    public final static String  ModeStr="<ModeStr>";

    public final static String  call="<call>";//呼叫冰箱

    public final static String  CleanEnable="<CleanEnable>";//一键净化使能
    public final static String  CleanAccess="<CleanAccess>";
    public final static String  CleanStr="<CleanStr>";
    public final static String  CleanSet="<CleanSet>";

    public final static String  RoomEnable="<RoomEnable>";//冰箱各室使能
    public final static String  RoomAccess="<RoomAccess>";
    public final static String  RoomType="<RoomType>";
    public final static String  RoomSet="<RoomSet>";

    public final static String  CCRoomAccess="<CCRoomAccess>";//变温室场景
    public final static String  CCRoomStr="<CCRoomStr>";
    public final static String  CCRoomOprate="<CCRoomOprate>";
    public final static String  CCRoomType="<CCRoomType>";
    public final static String  CCRoomSStr="<CCRoomSStr>";

    public final static String  RoomSetAccess="<RoomSetAccess>";//各室温度设置
    public final static String  RoomSetStr="<RoomSetStr>";
    public final static String  RoomSetOprate="<RoomSetOprate>";
    public final static String  RoomSetPre="<RoomSetPre>";
    public final static String  RoomSetNum="<RoomSetNum>";
    public final static String  RoomSetTen="<RoomSetTen>";
    public final static String  RoomSetTenU="<RoomSetTenU>";

    public final static String  TimerModeStr="<TimerModeStr>";//定时器模式
    public final static String  TimerOprate="<TimerOprate>";
    public final static String  TimerMode="<TimerMode>";

    public final static String  TimerHourStr="<TimerHourStr>";//定时器小时设置
    public final static String  TimerHour="<TimerHour>";
    public final static String  TimerHourUint="<TimerHourUint>";

    public final static String  TimerMinStr="<TimerMinStr>";//定时器分钟设置
    public final static String  TimerSetHundred="<TimerSetHundred>";
    public final static String  TimerSetHundredU="<TimerSetHundredU>";
    public final static String  TimerSetTen="<TimerSetTen>";
    public final static String  TimerSetTenU="<TimerSetTenU>";
    public final static String  TimerSetNum="<TimerSetNum>";
    public final static String  TimerMinUint="<TimerMinUint>";

    public final static String  TimerCloseStr="<TimerCloseStr>";//关闭定时器

    public final static String  MusicAssecc="<MusicAssecc>";//音乐播放
    public final static String  MusicStr="<MusicStr>";
    public final static String  PhotoStr="<PhotoStr>";
    public final static String  MusicStopStr="<MusicStopStr>";//停止音乐

    public final static String  ServiceTelNum="<ServiceTelNum>";//售后

    public final static String  MenuStr="<MenuStr>";//菜谱
    public final static String  FilmStr="<FilmStr>";//电影


    public final static int ID_SMART=101;//智能模式
    public final static int ID_HOLIDAY=102;//假日模式
    public final static int ID_CALL_XIAOXIAN1=103;//呼叫小鲜
    public final static int ID_CALL_XIAOXIAN2=104;//呼叫小鲜
    public final static int ID_CALL_XIAOXIAN3=105;//呼叫小鲜
    public final static int ID_CLEAN=106;//一键净化
    public final static int ID_RCROOM_ENABLE=107;//冷藏室开关
    public final static int ID_ROOM_ENABLE=108;//变温室开关

    public final static int ID_ROOM_LENGCANG=210;//变温室设冷藏区
    public final static int ID_ROOM_LENGDONG=211;//变温室设冷藏区
    public final static int ID_ROOM_RUANLENGDONG=212;//变温室设软冷冻区
    public final static int ID_ROOM_JIEDONG=213;//变温室设解冻区
    public final static int ID_ROOM_ROULEI=214;//变温室设肉类区
    public final static int ID_ROOM_GANHUO=215;//变温室设干货区
    public final static int ID_ROOM_SHUCAI=216;//变温室设蔬菜区
    public final static int ID_ROOM_SHUIGUO=217;//变温室设水果区
    public final static int ID_ROOM_SHENCAI=218;//变温室设剩菜区
    public final static int ID_ROOM_JIDAN=219;//变温室设鸡蛋区
    public final static int ID_ROOM_BINGPI=220;//变温室设冰啤区
    public final static int ID_ROOM_YU=221;//变温室设鱼区
    public final static int ID_ROOM_XIANPI=222;//变温室设鲜啤区
    public final static int ID_ROOM_CHAYE=223;//变温室设茶叶区
    public final static int ID_ROOM_XUEGAO=224;//变温室设雪糕区
    public final static int ID_ROOM_MIANMO=225;//变温室设面膜区
    public final static int ID_ROOM_MURU=226;//变温室设母乳区

    public final static int ID_TIMER_ZHUDAN=126;//蒸蛋
    public final static int ID_TIMER_ZHUZHOU=127;//煮粥
    public final static int ID_TIMER_BAOTANG=128;//煲汤
    public final static int ID_TIMER_MENGFAN=129;//焖饭
    public final static int ID_TIMER_DUNYU=130;//炖鱼
    public final static int ID_TIMER_JIANYAO=131;//煎药

    public final static int ID_CCROOM_TEMP_SET=123;//冷藏室温度设置
    public final static int ID_TCROOM_TEMP_SET=124;//变温室温度设置
    public final static int ID_FZROOM_TEMP_SET=125;//冷冻室温度设置

    public final static int ID_TIMER_MIN_SET=133;//定时器分钟设置
    public final static int ID_TIMER_HOUR_SET=132;//定时器小时设置

    public final static int ID_MUSIC_1=134;//音乐
    public final static int ID_MUSIC_2=135;//音乐
    public final static int ID_MUSIC_3=153;//音乐
    public final static int ID_MUSIC_STOP1=136;//停止音乐
    public final static int ID_MUSIC_STOP2=137;//停止音乐

    public final static int ID_PHOTO_PLAY=139;//电子相册
    public final static int ID_PHOTO_PLAY1=140;//打开电子相册
    public final static int ID_PHOTO_SET=151;//设置电子相册

    public final static int ID_SERVICE_TEL1=141;//售后电话
    public final static int ID_SERVICE_TEL2=152;//售后电话

    public final static int ID_MENU_CHECK1=142;//菜谱查询
    public final static int ID_MENU_CHECK2=143;//菜谱查询
    public final static int ID_MENU_CHECK3=149;//菜谱查询
    public final static int ID_MENU_CHECK4=150;//菜谱查询

    public final static int ID_FILM_PLAY1=144;//播放电影
    public final static int ID_FILM_PLAY2=145;//播放电影
    public final static int ID_FILM_PLAY3=146;//播放电影
    public final static int ID_TIMER_CLOSE=147;//关闭定时器
    public final static int ID_TIMER_CANCEL=148;//取消定时器
}

