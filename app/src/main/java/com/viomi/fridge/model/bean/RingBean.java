package com.viomi.fridge.model.bean;

/**
 * Created by viomi on 2017/2/27.
 */

public class RingBean {

    private String name;
    private boolean isSelect;
    private int track;

    public RingBean() {
    }

    public RingBean(String name, boolean isSelect) {
        this.name = name;
        this.isSelect = isSelect;
    }

    public RingBean(String name, boolean isSelect, int track) {
        this.name = name;
        this.isSelect = isSelect;
        this.track = track;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }

    public int getTrack() {
        return track;
    }

    public void setTrack(int track) {
        this.track = track;
    }
}
