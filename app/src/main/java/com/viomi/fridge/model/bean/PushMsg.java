package com.viomi.fridge.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by young2 on 2016/7/14.
 */
public class PushMsg implements Parcelable{
    public static final String PUSH_MESSAGE_TYPE_ABNORMAL="abnormal";//设备异常
    public static final String PUSH_MESSAGE_TYPE_TIMER="timer";//定时
    public static final String PUSH_MESSAGE_TYPE_FILTER="filter";//滤芯到期
    public static final String PUSH_MESSAGE_TYPE_SHARE="share";//分享
    public static final String PUSH_MESSAGE_TYPE_ORDER="store";//订单相关
    public static final String PUSH_MESSAGE_TYPE_COMMENT="comment";//评论
    public static final String PUSH_MESSAGE_TYPE_EVALUATE="evaluate";//评价
    public static final String PUSH_MESSAGE_TYPE_ADVERT="activity";//促销

    public static final String PUSH_MESSAGE_TYPE_FOOD="food";//食材管理
    public static final String PUSH_MESSAGE_TYPE_UPDATE="update";//升级

    public String title;
    public String content;
    public String type;
    public String link;

    public PushMsg(){

    }

    protected PushMsg(Parcel in) {
        title = in.readString();
        content = in.readString();
        type = in.readString();
        link = in.readString();
    }

    public static final Creator<PushMsg> CREATOR = new Creator<PushMsg>() {
        @Override
        public PushMsg createFromParcel(Parcel in) {
            return new PushMsg(in);
        }

        @Override
        public PushMsg[] newArray(int size) {
            return new PushMsg[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(title);
        parcel.writeString(content);
        parcel.writeString(type);
        parcel.writeString(link);
    }
}
