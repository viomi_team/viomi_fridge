package com.viomi.fridge.model.bean;

import android.content.Context;
import android.graphics.Bitmap;
import android.provider.MediaStore;

import com.viomi.fridge.util.FileUtil;

/**
 * Created by Ljh on 2017/11/17
 */

public class FileInfo {
    private String fileName = "";
    private String absolutePath = "";
    private int fileId = 0;
    private FileUtil.FileType fileType;
    private long fileSize = 0;
    private long timeLong = 0;
    private long ModifiedDate = 0;   //文件修改时间
    private boolean Selected = false;   //是否选中
    private long album_id = 0;

    public FileInfo() {
    }

    public FileInfo(FileUtil.FileType fileType, int fileId, String fileName, String absolutePath, long fileSize) {
        this.fileType = fileType;
        this.fileName = fileName;
        this.absolutePath = absolutePath;
        this.fileId = fileId;
        this.fileSize = fileSize;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public long getFileSize() {
        return fileSize;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    public int getFileId() {
        return fileId;
    }

    public void setFileId(int fileId) {
        this.fileId = fileId;
    }

    public long getTimeLong() {
        return timeLong;
    }

    public void setTimeLong(long timeLong) {
        this.timeLong = timeLong;
    }

    public String getAbsolutePath() {
        return absolutePath;
    }

    public void setAbsolutePath(String absolutePath) {
        this.absolutePath = absolutePath;
    }

    public long getModifiedDate() {
        return ModifiedDate;
    }

    public void setModifiedDate(long modifiedDate) {
        ModifiedDate = modifiedDate;
    }

    public boolean isSelected() {
        return Selected;
    }

    public void setSelected(boolean selected) {
        Selected = selected;
    }

    public FileUtil.FileType getFileType() {
        return fileType;
    }

    public void setFileType(FileUtil.FileType fileType) {
        fileType = fileType;
    }

    public Bitmap getBitmap(Context mContext) {
        if (fileType == FileUtil.FileType.FILE_VIDEO)
            return MediaStore.Video.Thumbnails.getThumbnail(mContext.getContentResolver(),
                    fileId, MediaStore.Images.Thumbnails.MICRO_KIND, null);
        else if (fileType == FileUtil.FileType.FILE_PIC)
            return MediaStore.Images.Thumbnails.getThumbnail(
                    mContext.getContentResolver(), fileId, MediaStore.Images.Thumbnails.MICRO_KIND,
                    null);
        else if (fileType == FileUtil.FileType.FILE_MUSIC) {
            return null;
        }
        return null;
    }

    public long getAlbum_id() {
        return album_id;
    }

    public void setAlbum_id(long album_id) {
        this.album_id = album_id;
    }

    public String getNameFromFilepath() {
        int pos = absolutePath.lastIndexOf('/');
        if (pos != -1) {
            return absolutePath.substring(pos + 1);
        }
        return "";
    }
}
