package com.viomi.fridge.model.bean.xunfei;

import java.util.List;

/**
 * Created by young2 on 2017/7/31.
 */

public class XFStoryResp extends XFAnswerResp {

    public Data data;

    public class Data{
        public List<Result> result;
    }

    public class Result{
        public String playUrl;//链接
        public String name;//标题
        public String category;//分类
    }
}
