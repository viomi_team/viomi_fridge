package com.viomi.fridge.model.bean;

/**
 * Created by young2 on 2017/7/20.
 */

public class UmengInfo {
    public Body body;
    public class Body{
        public String title;//标题
        public String custom;//传输数据
        public String text;//描述内容
    }
}
