package com.viomi.fridge.model.bean;

/**
 * Created by young2 on 2016/7/6.
 */
public class XiaoMiOAuthResultMsg {

    public   String accessToken ;
    public   String expiresIn ;
    public   String scope ;
    public   String state ;
    public   String tokenType ;
    public   String macKey ;
    public   String macAlgorithm ;
}
