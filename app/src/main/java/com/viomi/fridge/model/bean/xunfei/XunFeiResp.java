package com.viomi.fridge.model.bean.xunfei;

/**
 * Created by young2 on 2016/12/26.
 */

public class XunFeiResp {
    public int rc;//应答码(response code)
 //   public Object error;//错误信息
    public String text;//用户的输入，可能和请求中的原始text不完全一致，因服务器可能会对text进行语言纠错
//    public String history;//上下文信息，客户端需要将该字段结果传给下一次请求的history字段
    public String service;//服务的全局唯一名称
//    public String operation;//服务的细分操作编码，各业务服务自定义
//    public Object semantic;//语义结构化表示，各服务自定义
//    public Object data;//数据结构化表示，各服务自定义
//    public Object webPage;//该字段提供了结果内容的HTML5页面，客户端可以无需解析处理直接展现
//    public Object answer;//对结果内容的最简化文本/图片描述，各服务自定义
//    public Object tips;//结果内容的关联信息，作为用户后续交互的引导展现
//    public Object moreResults;//在存在多个候选结果时，用于提供更多的结果描述
}
