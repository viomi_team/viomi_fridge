package com.viomi.fridge.model.bean;

/**
 * Created by viomi on 2017/3/21.
 */

public class ScreenSleepBean {

    private int time;
    private String timeText;
    private boolean isSelect;

    public ScreenSleepBean() {
    }

    public ScreenSleepBean(int time, String timeText, boolean isSelect) {
        this.time = time;
        this.timeText = timeText;
        this.isSelect = isSelect;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }

    public String getTimeText() {
        return timeText;
    }

    public void setTimeText(String timeText) {
        this.timeText = timeText;
    }
}
