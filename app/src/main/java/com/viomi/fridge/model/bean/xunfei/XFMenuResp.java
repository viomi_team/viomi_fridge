package com.viomi.fridge.model.bean.xunfei;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.List;

/**
 * Created by young2 on 2017/7/31.
 */

public class XFMenuResp extends XFAnswerResp implements Parcelable{

    public Data data;

    public class Data{
        public List<Result> result;
    }

    public class Result implements Serializable{
        public String tag;//描述 ，午餐,煎锅,热菜,荤菜,下酒菜,家常味,咸鲜,煎,家常菜,晚餐,一人食,一家三口,朋友聚餐,锅具,咸,中国菜
        public String ingredient;//主要食材
        public String title;//菜名
        public String accessory;//辅助食材
        public String steps;//步骤
        public String imgUrl;
        public List<StepMethod> stepsWithImg;

    }


    public  class StepMethod{
        public String image;
        public String content;

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }
    }

    public XFMenuResp(){

    }

    protected XFMenuResp(Parcel in) {
    }

    public static final Creator<XFMenuResp> CREATOR = new Creator<XFMenuResp>() {
        @Override
        public XFMenuResp createFromParcel(Parcel in) {
            return new XFMenuResp(in);
        }

        @Override
        public XFMenuResp[] newArray(int size) {
            return new XFMenuResp[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }


}
