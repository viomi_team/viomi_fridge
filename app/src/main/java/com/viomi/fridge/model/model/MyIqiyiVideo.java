package com.viomi.fridge.model.model;

import android.util.Log;

import com.unilife.common.content.beans.iqiyi.IqiyiProgramListData;
import com.unilife.common.content.beans.param.iqiyi.RequestIqiyiProgram;
import com.unilife.common.content.beans.param.iqiyi.RequestIqiyiProgramSearch;
import com.unilife.common.content.dao.IUMBaseDAO;
import com.unilife.common.content.models.UMModel;
import com.unilife.common.content.networks.IUMModelListener;
import com.unilife.common.utils.IqiyiDeviceManager;
import com.unilife.content.logic.dao.iqiyi.UMIqiyiVideoInfoDao;
import com.unilife.content.logic.logic.IUMLogicListener;

import java.util.List;

/**
 * Created by Mocc on 2017/8/4
 */

public class MyIqiyiVideo extends UMModel<IqiyiProgramListData> {


    private MyIqiyiVideo() {
    }

    public static synchronized MyIqiyiVideo getInstance() {
        return new MyIqiyiVideo();
    }

    private RequestIqiyiProgram getVideoByCatalog(int orderType, String str_CatalogId, int offset, int pageSize) {
        RequestIqiyiProgram info = new RequestIqiyiProgram();
        info.setCatagoryIdStr(str_CatalogId);
        info.setMac(IqiyiDeviceManager.getInstance().getMac());
        info.setImei(IqiyiDeviceManager.getInstance().getImei());
        info.setOpenudid(IqiyiDeviceManager.getInstance().getOpenUDID());
        info.setOrderType(Integer.valueOf(orderType));
        info.setPageSize(pageSize);
        info.setOffset(offset);
        return info;
    }

    private RequestIqiyiProgramSearch getVideoInfoBySearch(String keyword, int offset, int pageSize) {
        RequestIqiyiProgramSearch info = new RequestIqiyiProgramSearch();
        info.setMac(IqiyiDeviceManager.getInstance().getMac());
        info.setImei(IqiyiDeviceManager.getInstance().getImei());
        info.setOpenudid(IqiyiDeviceManager.getInstance().getOpenUDID());
        info.setKeyWord(keyword);
        info.setOffset(offset);
        info.setPageSize(pageSize);
        return info;
    }

    protected IUMBaseDAO initDAO() {
        return new UMIqiyiVideoInfoDao();
    }

    public void getVideoByCatalog(String accessToken, int orderType, String str_CatalogId, int offset, int pageSize, final IUMLogicListener listener) {
        this.setAccessToken(accessToken);
        this.setListener(new IUMModelListener() {
            public void onStarted(OperateType type) {
            }

            public void onFinished(OperateType type, ResultType result) {
                if(listener != null) {
                    if(result != ResultType.Error && type == OperateType.Fetch) {
                        if(type == OperateType.Fetch && result == ResultType.Success) {
                            Log.d("---->", "list=" + MyIqiyiVideo.this.getData().size());
                            listener.onSuccess(MyIqiyiVideo.this.getData().get(0), MyIqiyiVideo.this.getOffset().longValue(), MyIqiyiVideo.this.getTotalCount().longValue());
                        }

                    } else {
                        listener.onError("");
                    }
                }
            }
        });
        this.filter(this.getVideoByCatalog(orderType, str_CatalogId, offset, pageSize));
        this.fetch();
    }

    public void getVideoBySearch(String accessToken, String keyword, int offset, int pageSize, final IUMLogicListener listener) {
        this.setAccessToken(accessToken);
        this.setListener(new IUMModelListener() {
            public void onStarted(OperateType type) {
            }

            public void onFinished(OperateType type, ResultType result) {
                if(listener != null) {
                    if(result != ResultType.Error && type == OperateType.Fetch) {
                        if(type == OperateType.Fetch && result == ResultType.Success) {
                            Log.d("---->", "list=" + MyIqiyiVideo.this.getData().size());
                            listener.onSuccess(MyIqiyiVideo.this.getData().get(0), MyIqiyiVideo.this.getOffset().longValue(), MyIqiyiVideo.this.getTotalCount().longValue());
                        }

                    } else {
                        listener.onError("");
                    }
                }
            }
        });
        this.filter(this.getVideoInfoBySearch(keyword, offset, pageSize));
        this.fetch();
    }


    private List<IqiyiProgramListData> getData() {
        return this.select();
    }

}
