package com.viomi.fridge.model.bean.xunfei;

/**
 * 冰箱语义场景返回
 * Created by young2 on 2016/12/28.
 */

public class XFFridgeResp extends XunFeiResp {

    public Semantic semantic;

    public class Semantic{
        public Slots slots;
    }

    public class Slots{
        public Location location;//地点语义
        public Datetime datetime;//时间语义
        public String repeat;//时间周期性语义
        public String duration;//持续时间（定时分钟数）
        public String modifier;//设备修饰语义，有多个修饰语义时用‘|’分割取值
        public String temperatureZone;//冷藏室、冷冻室、变温室
        public String attr;//属性名称，表示冰箱的开关、模式、文档，具体取值参考 attr 表格介绍
        public String attrValue;//属性取值
        public String attrType;//属性取值类型
    }

    public class Location{
        public String type;
        public String room;
    }

    public class Datetime{
        public String date;
        public String type;
        public String time;
        public String dateOrig;
    }

}
