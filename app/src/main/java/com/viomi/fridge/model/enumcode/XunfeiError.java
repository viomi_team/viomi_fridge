package com.viomi.fridge.model.enumcode;

/**
 * 部分讯飞错误吗
 *全部见http://bbs.xfyun.cn/forum.php?mod=viewthread&tid=13056
 * Created by young2 on 2017/2/15.
 */

public class XunfeiError {
    //通用错误
    public static  int  MSP_ERROR_NO_ENOUGH_BUFFER=10117;//没有足够的内存，合成字数超过限制8192
    public static  int MSP_ERROR_NO_DATA=10118;//没有数据，vad_bos设置问题,超时


    //一般错误
    public static  int ERROR_NO_MATCH=20005;//无匹配结果
    public static  int ERROR_NETWORK_TIMEOUT=10114;//网络超时


}
