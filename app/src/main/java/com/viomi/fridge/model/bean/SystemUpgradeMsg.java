package com.viomi.fridge.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by young2 on 2016/5/20.
 */
public class SystemUpgradeMsg implements Parcelable {
    public String url="";//rom下载地址
    public String latestVersion="";//最新平台版本
    public int versionCode;//版本代码
    public String upgradeDescription="";//平台描述
    public String channel="";//升级渠道，差分还是rom升级
    public SystemUpgradeMsg(){

    }

    protected SystemUpgradeMsg(Parcel in) {
        url = in.readString();
        latestVersion = in.readString();
        versionCode = in.readInt();
        upgradeDescription = in.readString();
        channel = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(url);
        dest.writeString(latestVersion);
        dest.writeInt(versionCode);
        dest.writeString(upgradeDescription);
        dest.writeString(channel);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SystemUpgradeMsg> CREATOR = new Creator<SystemUpgradeMsg>() {
        @Override
        public SystemUpgradeMsg createFromParcel(Parcel in) {
            return new SystemUpgradeMsg(in);
        }

        @Override
        public SystemUpgradeMsg[] newArray(int size) {
            return new SystemUpgradeMsg[size];
        }
    };
}
