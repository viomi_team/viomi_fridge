package com.viomi.fridge.model.bean;

import java.util.List;

/**
 * 扫描登陆二维码返回信息
 * Created by young2 on 2017/2/10.
 */

public class QrCodeScanResult {
    public MobBaseRes mobBaseRes;
    public class MobBaseRes{
        public int code;
        public String desc;
        public String token;
        public String appendAttr;
        public LoginData loginData;
    }
    public class LoginData{
        public int accountType;
        public int loginType;
        public int userId;
        public String userCode;
        public List<String> roles;
    }

}
