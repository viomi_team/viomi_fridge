package com.viomi.fridge.model.parser;

import com.viomi.fridge.model.bean.WaterPuriProp;
import com.viomi.fridge.util.ToolUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class DeviceGetPropDecode {

    public static WaterPuriProp decode(JSONObject object, boolean firstClick) {

        WaterPuriProp prop = new WaterPuriProp();
        if (object == null)
            return null;
        String result;
        try {
            result = object.getString("result");
        } catch (JSONException e) {
            return null;
        }
        if (result.equals("")) return null;
        Pattern pattern = Pattern.compile("\\d+");
        Matcher matcher = pattern.matcher(result);
        try {
            matcher.find();
            String ttds = matcher.group(0);
            matcher.find();
            String ptds = matcher.group(0);
            if (ttds != null)
                prop.tTds = Integer.parseInt(ttds);
            if (ptds != null)
                prop.pTds = Integer.parseInt(ptds);
            matcher.find();
            prop.oneUsedFlow = Integer.parseInt(matcher.group(0));
            matcher.find();
            prop.oneUsedTime = Integer.parseInt(matcher.group(0));
            matcher.find();
            prop.twoUsedFlow = Integer.parseInt(matcher.group(0));
            matcher.find();
            prop.twoUsedTime = Integer.parseInt(matcher.group(0));
            matcher.find();
            prop.threeUsedFlow = Integer.parseInt(matcher.group(0));
            matcher.find();
            prop.threeUsedTime = Integer.parseInt(matcher.group(0));
            matcher.find();
            prop.fourUsedFlow = Integer.parseInt(matcher.group(0));
            matcher.find();
            prop.fourUsedTime = Integer.parseInt(matcher.group(0));
            matcher.find();
            prop.oneTotalUsedFlow = Integer.parseInt(matcher.group(0));
            matcher.find();
            prop.oneTotalUsedTime = Integer.parseInt(matcher.group(0));
            matcher.find();
            prop.twoTotalUsedFlow = Integer.parseInt(matcher.group(0));
            matcher.find();
            prop.twoTotalUsedTime = Integer.parseInt(matcher.group(0));
            matcher.find();
            prop.threeTotalUsedFlow = Integer.parseInt(matcher.group(0));
            matcher.find();
            prop.threeTotalUsedTime = Integer.parseInt(matcher.group(0));
            matcher.find();
            prop.fourTotalUsedFlow = Integer.parseInt(matcher.group(0));
            matcher.find();
            prop.fourTotalUsedTime = Integer.parseInt(matcher.group(0));
        } catch (Exception e) {

        }
        try {
            prop.oneLifeInt = (prop.oneTotalUsedTime - prop.oneUsedTime) * 100 / (float) prop.oneTotalUsedTime;
            prop.twoLifeInt = (prop.twoTotalUsedTime - prop.twoUsedTime) * 100 / (float) prop.twoTotalUsedTime;
            prop.threeLifeInt = (prop.threeTotalUsedTime - prop.threeUsedTime) * 100 / (float) prop.threeTotalUsedTime;
            prop.fourLifeInt = (prop.fourTotalUsedTime - prop.fourUsedTime) * 100 / (float) prop.fourTotalUsedTime;
        } catch (Exception e) {

        }
        if (prop.oneLifeInt < 0)
            prop.oneLifeInt = 0;
        if (prop.twoLifeInt < 0)
            prop.twoLifeInt = 0;
        if (prop.threeLifeInt < 0)
            prop.threeLifeInt = 0;
        if (prop.fourLifeInt < 0)
            prop.fourLifeInt = 0;
        float oneFlow = 0, twoFlow = 0, threeFlow = 0, fourFlow = 0;
        try {
            oneFlow = (prop.oneTotalUsedFlow - prop.oneUsedFlow) * 100 / (float) prop.oneTotalUsedFlow;
            twoFlow = (prop.twoTotalUsedFlow - prop.twoUsedFlow) * 100 / (float) prop.twoTotalUsedFlow;
            threeFlow = (prop.threeTotalUsedFlow - prop.threeUsedFlow) * 100 / (float) prop.threeTotalUsedFlow;
            fourFlow = (prop.fourTotalUsedFlow - prop.fourUsedFlow) * 100 / (float) prop.fourTotalUsedFlow;
        } catch (Exception e) {

        }
        if (oneFlow < 0)
            oneFlow = 0;
        if (twoFlow < 0)
            twoFlow = 0;
        if (threeFlow < 0)
            threeFlow = 0;
        if (fourFlow < 0)
            fourFlow = 0;
//		if (oneFlow < prop.oneLifeInt && oneFlow >= 0)
//			prop.oneLifeInt = oneFlow;
//		if (twoFlow < prop.twoLifeInt && twoFlow >= 0)
//			prop.twoLifeInt = twoFlow;
//		if (threeFlow < prop.threeLifeInt && threeFlow >= 0)
//			prop.threeLifeInt = threeFlow;
//		if (fourFlow < prop.fourLifeInt && fourFlow >= 0)
//			prop.fourLifeInt = fourFlow;
        try {
            matcher.find();
            prop.status = Integer.parseInt(matcher.group(0));
            if (prop.oneLifeInt == 0) {
                prop.status = prop.status | 0x00010000;
            }
            if (prop.twoLifeInt == 0) {
                prop.status = prop.status | 0x00020000;
            }
            if (prop.threeLifeInt == 0) {
                prop.status = prop.status | 0x00040000;
            }
            if (prop.fourLifeInt == 0) {
                prop.status = prop.status | 0x00080000;
            }
            if (firstClick) {
                prop.status = prop.status | 0x00100000;
            }
            try {
                matcher.find();
                prop.isOpenDisplay = Integer.parseInt(matcher.group(0));
            } catch (Exception e) {

            }
            try {
                matcher.find();
                prop.isRinse = Integer.parseInt(matcher.group(0));
                if (prop.isRinse == 1) {
                    prop.status = prop.status | 0x00200000;
                }
            } catch (Exception e) {

            }

//			if(prop.oneLifeInt<=0.0833) {
//				prop.status = prop.status | 0x00200000;
//			}
//			if(prop.twoLifeInt<=0.0416) {
//				prop.status = prop.status | 0x00400000;
//			}
//			if(prop.threeLifeInt<=0.0208) {
//				prop.status = prop.status | 0x00800000;
//			}
//			if(prop.fourLifeInt<=0.0416) {
//				prop.status = prop.status | 0x01000000;
//			}
            prop.statusByte = ToolUtil.longToByte(prop.status);
            prop.statusCount = 0;
            for (int i = 0; i < 22; i++) {
                if (prop.statusByte[i] == 1) {
                    prop.statusCount++;
                }
            }
        } catch (IllegalStateException e) {
            return prop;
        }


        return prop;
    }
}
