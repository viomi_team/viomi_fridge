package com.viomi.fridge.model.bean.xunfei;

import java.util.List;

/**
 * Created by young2 on 2017/7/31.
 */

public class XFTranslationResp extends XFAnswerResp {

    public Data data;

    public class Data{
        public List<Result> result;
    }

    public class Result{
        public String translated;//翻译

    }
}
