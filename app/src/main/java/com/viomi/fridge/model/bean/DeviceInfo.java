package com.viomi.fridge.model.bean;

import com.miot.common.abstractdevice.AbstractDevice;
import com.v2.clsdk.model.CameraInfo;

/**
 * 智能互联设备信息
 * Created by William on 2017/11/9.
 */

public class DeviceInfo {

    private AbstractDevice abstractDevice = null;   // 小米平台设备信息
    private CameraInfo cameraInfo = null;   // 绿联 IPC 设备信息

    public void setAbstractDevice(AbstractDevice abstractDevice) {
        this.abstractDevice = abstractDevice;
    }

    public void setCameraInfo(CameraInfo cameraInfo) {
        this.cameraInfo = cameraInfo;
    }

    public AbstractDevice getAbstractDevice() {
        return abstractDevice;
    }

    public CameraInfo getCameraInfo() {
        return cameraInfo;
    }

}
