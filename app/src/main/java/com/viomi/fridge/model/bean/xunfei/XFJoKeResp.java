package com.viomi.fridge.model.bean.xunfei;

import java.util.List;

/**
 * Created by young2 on 2017/7/31.
 */

public class XFJoKeResp extends XFAnswerResp {

    public Data data;

    public class Data{
        public List<Result> result;
    }

    public class Result{
        public String mp3Url;//笑话
        public String title;//标题
        public String category;//分类
    }
}
