package com.viomi.fridge.model.bean.recipe;


import java.util.ArrayList;

public class RecipeDetail {
  private String menuId="";
  private String name="";
  private String thumbnail="";

  private String ctgTitles;

  private recipe recipe;

    public String getCtgTitles() {
        return ctgTitles;
    }

    public void setCtgTitles(String ctgTitles) {
        this.ctgTitles = ctgTitles;
    }

    public String getMenuId() {
    return menuId;
  }

  public void setMenuId(String menuId) {
    this.menuId = menuId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getThumbnail() {
    return thumbnail;
  }

  public void setThumbnail(String thumbnail) {
    this.thumbnail = thumbnail;
  }

  public RecipeDetail.recipe getRecipe() {
    return recipe;
  }

  public void setRecipe(RecipeDetail.recipe recipe) {
    this.recipe = recipe;
  }

  public static class recipe{
    private String img="";
    private String ingredients="";
    private ArrayList<Step> steps=new ArrayList<>();
    private String sumary="";
    private String title="";
    public String getImg() {
      return img;
    }

    public void setImg(String img) {
      this.img = img;
    }

    public String getIngredients() {
      return ingredients;
    }

    public void setIngredients(String ingredients) {
      this.ingredients = ingredients;
    }


    public String getSumary() {
      return sumary;
    }

    public void setSumary(String sumary) {
      this.sumary = sumary;
    }

    public String getTitle() {
      return title;
    }

    public void setTitle(String title) {
      this.title = title;
    }

      public ArrayList<Step> getSteps() {
          return steps;
      }

      public void setSteps(ArrayList<Step> steps) {
          this.steps = steps;
      }

      //      public ArrayList<Step> getMethod() {
//          return method;
//      }
//
//      public void setMethod(ArrayList<Step> method) {
//          this.method = method;
//      }
  }
}
