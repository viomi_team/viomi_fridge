package com.viomi.fridge.model.bean;

import java.util.List;

/**
 * Created by young2 on 2017/3/5.
 *
 *
 *
 {"sn":1,
 "ls":true,
 "bg":0,
 "ed":0,
 "ws":[{
 "bg":0,
 "cw":[{
 "id":65535,
 "gm":0,
 "sc":100,
 "w":"我要听"
 }],
 "slot":"我要听"
 },{
 "bg":0,
 "cw":[{
 "id":135,
 "gm":0,
 "sc":100,
 "w":"歌"
 }],
 "slot":"歌"
 }],
 "sc":100
 }
 */

public class LocalRecogInfo {
    public int sn;
    public boolean ls;
    public int bg;
    public int ed;
    public List<Ws> ws;
    public int src;

    public class Ws{
        public int bg;
        public List<Cw> cw;
        public String slot;
    }

    public class Cw{
        public int id;
        public int gm;
        public int src;
        public String  w;
    }

}
