package com.viomi.fridge.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by young2 on 2016/5/20.
 */
public class AppUpgradeMsg implements Parcelable {
    public String url="";//apk下载地址
    public String latestVersion="";//最新平台版本
    public int versionCode;//app版本代码
    public String upgradeDescription="";//平台固件描述

    public AppUpgradeMsg(){

    }

    protected AppUpgradeMsg(Parcel in) {
        url = in.readString();
        latestVersion = in.readString();
        versionCode = in.readInt();
        upgradeDescription = in.readString();
    }

    public static final Creator<AppUpgradeMsg> CREATOR = new Creator<AppUpgradeMsg>() {
        @Override
        public AppUpgradeMsg createFromParcel(Parcel in) {
            return new AppUpgradeMsg(in);
        }

        @Override
        public AppUpgradeMsg[] newArray(int size) {
            return new AppUpgradeMsg[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(url);
        dest.writeString(latestVersion);
        dest.writeInt(versionCode);
        dest.writeString(upgradeDescription);
    }
}
