package com.viomi.fridge.model.bean;

import android.bluetooth.BluetoothDevice;

/**
 * Created by Ljh on 2017/10/24.
 */

public class BleDevice {
    private boolean connected = false;
    private BluetoothDevice mBluetoothDevice;

    public BleDevice(BluetoothDevice device) {
        this.mBluetoothDevice = device;
    }

    public BleDevice(boolean connected, BluetoothDevice device) {
        this.connected = connected;
        this.mBluetoothDevice = device;
    }

    public boolean isConnected() {
        return connected;
    }

    public void setConnected(boolean connected) {
        this.connected = connected;
    }

    public BluetoothDevice getBluetoothDevice() {
        return mBluetoothDevice;
    }

    public void setBluetoothDevice(BluetoothDevice bluetoothDevice) {
        mBluetoothDevice = bluetoothDevice;
    }
}
