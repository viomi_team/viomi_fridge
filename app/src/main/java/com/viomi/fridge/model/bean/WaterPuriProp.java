package com.viomi.fridge.model.bean;

import java.io.Serializable;
import java.util.ArrayList;

public class WaterPuriProp implements Serializable {
    private static final long serialVersionUID = 1L;
    public int tTds;                                    //自来水tds
    public int pTds = 100;                            //纯净水tds
    public int oneUsedFlow;
    public int oneUsedTime;
    public int twoUsedFlow;
    public int twoUsedTime;
    public int threeUsedFlow;
    public int threeUsedTime;
    public int fourUsedFlow;
    public int fourUsedTime;
    public int oneTotalUsedFlow;
    public int twoTotalUsedFlow;
    public int oneTotalUsedTime = 4320;
    public int twoTotalUsedTime = 4320 * 2;
    public int threeTotalUsedTime = 8640 * 2;
    public int fourTotalUsedTime = 4320 * 2;
    public int threeTotalUsedFlow;
    public int fourTotalUsedFlow;
    public float oneLifeInt;
    public float twoLifeInt;
    public float threeLifeInt;
    public float fourLifeInt;            // 四级滤芯寿命状态
    public float oneFlowInt;
    public float twoFlowInt;
    public float threeFlowInt;
    public int status = -1;
    public byte[] statusByte;             // 异常状态
    public ArrayList<String> statusString = null;
    public int statusCount = 0;              // 异常数量
    public int[] hisPFlowList = new int[7];
    public int openStatus;      // 目前是否处于制水状态，0表示空闲，1表示制水
    public int isOpenDisplay = 0;         // 水龙头是否打开tds实时显示功能，0表示关闭，1表示打开
    public int isRinse = 0;                   // 设备是否处于冲洗模式
    public int isInstalled = 0; // UV灯是否安装
    public int uvStatus = 0;    // UV灯工作状态，1表示打开，0表示关闭
    public int temp;    // 水温
    public int waterFre;    // 水位频率
    public int washStatus;  // 冲洗状态
    public int outStatus;   // 出水状态，1表示出水，0表示空闲
}