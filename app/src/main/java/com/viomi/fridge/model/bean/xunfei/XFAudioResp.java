package com.viomi.fridge.model.bean.xunfei;

import java.util.List;

/**
 * 音频类
 * Created by young2 on 2017/7/31.
 */

public class XFAudioResp extends XFAnswerResp {

    public Data data;

    public class Data{
        public List<Result> result;
    }

    public class Result{
        public String url;//链接
        public String name;//标题
        public String album;//分类
    }
}
