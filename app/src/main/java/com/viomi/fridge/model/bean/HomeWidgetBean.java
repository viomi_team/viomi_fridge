package com.viomi.fridge.model.bean;

/**
 * Created by viomi on 2017/2/21.
 */

public class HomeWidgetBean {

    private String linkUrl;
    private String imageUrl;
    private String type;
    private int idx;

    public HomeWidgetBean() {
    }

    public HomeWidgetBean(String linkUrl, String imageUrl) {
        this.linkUrl = linkUrl;
        this.imageUrl = imageUrl;
    }

    public HomeWidgetBean(String linkUrl, String imageUrl, String type) {
        this.linkUrl = linkUrl;
        this.imageUrl = imageUrl;
        this.type = type;
    }

    public HomeWidgetBean(String linkUrl, String imageUrl, String type, int idx) {
        this.linkUrl = linkUrl;
        this.imageUrl = imageUrl;
        this.type = type;
        this.idx = idx;
    }

    public String getLinkUrl() {
        return linkUrl;
    }

    public void setLinkUrl(String linkUrl) {
        this.linkUrl = linkUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getIdx() {
        return idx;
    }

    public void setIdx(int idx) {
        this.idx = idx;
    }
}
