package com.viomi.fridge.model.bean;

/**
 * 商城通用返回
 * Created by young2 on 2016/8/19.
 */
public class VmallResponseMsg {
    public final  static int RESPONSE_OK=100;
    public final  static int RESPONSE_TOKEN_EXPIRED_EXCEPTION=919;
    public int code=-1;
    public String desc;
    public String jsonObject;
}
