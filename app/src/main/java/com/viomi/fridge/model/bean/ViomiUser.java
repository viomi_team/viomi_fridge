package com.viomi.fridge.model.bean;

import java.io.Serializable;

/**
 *
 * Created by young2 on 2017/2/11.
 */

public class ViomiUser implements Serializable {
    private String miId;//小米账号
    private String type;//android,ios
    private String token;//上传token
    private String nickname;//昵称
    private String account;//账号
    private String headImg;//头像url
    private String userCode;
    private String mobile;//手机
    private int cid;//渠道id
    private int gender=-1;//性别 0女1男,-1未知

    public ViomiUser() {
    }

    public ViomiUser(String miId, String token, String nickname, String account, String headImg, String userCode, String mobile, int cid, int gender) {
        this.miId = miId;
        this.token = token;
        this.nickname = nickname;
        this.account = account;
        this.headImg = headImg;
        this.userCode = userCode;
        this.mobile = mobile;
        this.cid = cid;
        this.gender = gender;
    }

    public String getMiId() {
        return miId;
    }

    public void setMiId(String miId) {
        this.miId = miId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getHeadImg() {
        return headImg;
    }

    public void setHeadImg(String headImg) {
        this.headImg = headImg;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
