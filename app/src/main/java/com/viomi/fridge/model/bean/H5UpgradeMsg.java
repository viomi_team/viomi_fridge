package com.viomi.fridge.model.bean;

/**
 * Created by young2 on 2016/5/20.
 */
public class H5UpgradeMsg {
    public String url="";//zip下载地址
    public int versionCode;//h5插件版本代码
    public String upgradeDescription="";//平台固件描述
    public String minAppVersion="1.0";//插件包最低适配app的版本，假如该最低版本要求比本app版本高，则不下载h5插件包
}
