package com.viomi.fridge.model.bean.recipe;

public class categoryInfo {
    private String ctgId;
    private String name;
    private String parentId;

    public categoryInfo() {
    }

    public String getCtgId() {
        return ctgId;
    }

    public void setCtgId(String ctgId) {
        this.ctgId = ctgId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }
}
