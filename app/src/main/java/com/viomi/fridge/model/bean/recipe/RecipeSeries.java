package com.viomi.fridge.model.bean.recipe;

import java.util.ArrayList;

public class RecipeSeries {

    private int curPage;
    private int total;

    private ArrayList<RecipeDetail> list = new ArrayList<>();

    public int getCurPage() {
        return curPage;
    }

    public void setCurPage(int curPage) {
        this.curPage = curPage;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public ArrayList<RecipeDetail> getList() {
        return list;
    }

    public void setList(ArrayList<RecipeDetail> list) {
        this.list = list;
    }
}
