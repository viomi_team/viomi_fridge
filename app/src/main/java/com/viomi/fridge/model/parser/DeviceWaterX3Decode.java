package com.viomi.fridge.model.parser;

import com.viomi.fridge.model.bean.WaterPuriProp;

import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by William on 2017/4/14.
 */

public class DeviceWaterX3Decode {

    public static WaterPuriProp decode(JSONObject jsonObject) {
        WaterPuriProp waterPuriProp = new WaterPuriProp();
        if (jsonObject == null) return null;

        String result = jsonObject.optString("result");
        if (result.equals("")) return null;
        Pattern pattern = Pattern.compile("\\d+");
        Matcher matcher = pattern.matcher(result);

        try {
            matcher.find();
            waterPuriProp.tTds = Integer.parseInt(matcher.group(0));    // 自来水TDS
            matcher.find();
            waterPuriProp.pTds = Integer.parseInt(matcher.group(0));    // 纯净水TDS
            matcher.find();
            waterPuriProp.oneUsedFlow = Integer.parseInt(matcher.group(0)); // F1使用流量（/L）
            matcher.find();
            waterPuriProp.oneUsedTime = Integer.parseInt(matcher.group(0)); // F1使用时间（/h）
            matcher.find();
            waterPuriProp.twoUsedFlow = Integer.parseInt(matcher.group(0)); // RO使用流量（/L）
            matcher.find();
            waterPuriProp.twoUsedTime = Integer.parseInt(matcher.group(0)); // RO使用时间（/h）
            matcher.find();
            waterPuriProp.threeUsedFlow = Integer.parseInt(matcher.group(0)); // 水袋使用流量（/L）
            matcher.find();
            waterPuriProp.threeUsedTime = Integer.parseInt(matcher.group(0)); // 水袋使用时间（/h）
            matcher.find();
            waterPuriProp.oneTotalUsedFlow = Integer.parseInt(matcher.group(0));    // F1额定流量（/L）
            matcher.find();
            waterPuriProp.oneTotalUsedTime = Integer.parseInt(matcher.group(0));  // F1额定时间（/h）
            matcher.find();
            waterPuriProp.twoTotalUsedFlow = Integer.parseInt(matcher.group(0));  // RO额定流量（/h）
            matcher.find();
            waterPuriProp.twoTotalUsedTime = Integer.parseInt(matcher.group(0));  // RO额定时间（/h）
            matcher.find();
            waterPuriProp.threeTotalUsedFlow = Integer.parseInt(matcher.group(0));  // 水袋额定流量（/h）
            matcher.find();
            waterPuriProp.threeTotalUsedTime = Integer.parseInt(matcher.group(0));  // 水袋额定时间（/h）
            matcher.find();
            waterPuriProp.status = Integer.parseInt(matcher.group(0));  // 异常状态
            matcher.find();
            waterPuriProp.isOpenDisplay = Integer.parseInt(matcher.group(0));  // TDS实时显示状态
            matcher.find();
            waterPuriProp.isInstalled = Integer.parseInt(matcher.group(0));  // UV灯有没安装
            matcher.find();
            waterPuriProp.uvStatus = Integer.parseInt(matcher.group(0));  // UV灯工作状态
            matcher.find();
            waterPuriProp.temp = Integer.parseInt(matcher.group(0));  // 水温
            matcher.find();
            waterPuriProp.waterFre = Integer.parseInt(matcher.group(0));  // 水位频率
            matcher.find();
            waterPuriProp.washStatus = Integer.parseInt(matcher.group(0));    // 冲洗状态
            matcher.find();
            waterPuriProp.openStatus = Integer.parseInt(matcher.group(0));  // 制水状态
            matcher.find();
            waterPuriProp.outStatus = Integer.parseInt(matcher.group(0)); // 出水状态

            waterPuriProp.oneFlowInt = (waterPuriProp.oneTotalUsedFlow - waterPuriProp.oneUsedFlow) * 100 / (float) waterPuriProp.oneTotalUsedFlow;
            waterPuriProp.twoFlowInt = (waterPuriProp.twoTotalUsedFlow - waterPuriProp.twoUsedFlow) * 100 / (float) waterPuriProp.twoTotalUsedFlow;
            waterPuriProp.threeFlowInt = (waterPuriProp.threeTotalUsedFlow - waterPuriProp.threeUsedFlow) * 100 / (float) waterPuriProp.threeTotalUsedFlow;

            waterPuriProp.oneLifeInt = (waterPuriProp.oneTotalUsedTime - waterPuriProp.oneUsedTime) * 100 / (float) waterPuriProp.oneTotalUsedTime;
            waterPuriProp.twoLifeInt = (waterPuriProp.twoTotalUsedTime - waterPuriProp.twoUsedTime) * 100 / (float) waterPuriProp.twoTotalUsedTime;
            waterPuriProp.threeLifeInt = (waterPuriProp.threeTotalUsedTime - waterPuriProp.threeUsedTime) * 100 / (float) waterPuriProp.threeTotalUsedTime;

            if (waterPuriProp.oneFlowInt < 0) waterPuriProp.oneFlowInt = 0;
            if (waterPuriProp.twoFlowInt < 0) waterPuriProp.twoFlowInt = 0;
            if (waterPuriProp.threeFlowInt < 0) waterPuriProp.threeFlowInt = 0;

            if (waterPuriProp.oneLifeInt < 0) waterPuriProp.oneLifeInt = 0;
            if (waterPuriProp.twoLifeInt < 0) waterPuriProp.twoLifeInt = 0;
            if (waterPuriProp.threeLifeInt < 0) waterPuriProp.threeLifeInt = 0;

        } catch (Exception e) {
            return waterPuriProp;
        }

        return waterPuriProp;
    }

}
