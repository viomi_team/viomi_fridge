package com.viomi.fridge.model.bean;

import java.util.List;

/**
 * Created by young2 on 2016/12/20
 */

public class WeatherResp {
    public final static int CODE_NORMAL=0;
    public int error=-100;
    public String status;
    public String date;
    public List<Result> results;

    public class Result{
        public String currentCity;
        public String pm25;
        public List<WeatherData> weather_data;
    }

    public  class WeatherData {
        public String date;
        public String dayPictureUrl;
        public String nightPictureUrl;
        public String weather;
        public String wind;
        public String temperature;
    }
}
