package com.viomi.fridge.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by young2 on 2017/1/10.
 */

public class WebBaseData implements Parcelable {
    public static final String Intent_String="Intent_String";
    public String url;
    public String name;

    public WebBaseData(){

    }

    protected WebBaseData(Parcel in) {
        url = in.readString();
        name = in.readString();
    }

    public static final Creator<WebBaseData> CREATOR = new Creator<WebBaseData>() {
        @Override
        public WebBaseData createFromParcel(Parcel in) {
            return new WebBaseData(in);
        }

        @Override
        public WebBaseData[] newArray(int size) {
            return new WebBaseData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(url);
        dest.writeString(name);
    }
}
