package com.viomi.fridge.model.bean.recipe;

import java.util.ArrayList;

public class Secondlevel {
    private categoryInfo categoryInfo;
    private ArrayList<categoryInfo> childs = new ArrayList<>();

    public categoryInfo getCategoryInfo() {
        return categoryInfo;
    }

    public void setCategoryInfo(categoryInfo categoryInfo) {
        this.categoryInfo = categoryInfo;
    }

    public ArrayList<categoryInfo> getChilds() {
        return childs;
    }

    public void setChilds(ArrayList<categoryInfo> childs) {
        this.childs = childs;
    }

}
