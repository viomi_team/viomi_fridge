package com.viomi.fridge.model.bean;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * 食材信息
 * Created by William on 2017/6/22.
 */

public class FoodDetailData implements Serializable {

    public String type; // 存放类型(1: 生鲜区；2: 冷藏区；3: 冷冻区)
    public String name; // 食材名称
    public String addTime;  // 食材添加时间
    public String imgPath;  // 食材绘图保存目录
    public String deadLine; // 食材到期时间
    public String isPush;   // 是否已推送
    public String no_filter;    // 无滤芯保存时间
    public String filter;   // 有滤芯保存时间
    public boolean isSelected = false;

    public boolean getSelect() {
        return isSelected;
    }

    public String toJson() {
        JSONObject object=new JSONObject();
        try {
            object.put("name",name);
            object.put("addTime",addTime);
            object.put("type",type);
            object.put("deadLine",deadLine);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object.toString();
    }

    public JSONObject toJsonObject() {
        JSONObject object=new JSONObject();
        try {
            object.put("name",name);
            object.put("addTime",addTime);
            object.put("type",type);
            object.put("deadLine",deadLine);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object;
    }
}
