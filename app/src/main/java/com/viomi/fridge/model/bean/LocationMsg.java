package com.viomi.fridge.model.bean;

/**
 * Created by young2 on 2016/12/19.
 */

public class LocationMsg {
    public String address;//当前地址
    public String province;//省份
    public String city;//城市
    public String district;//区县
    public String locationX;//纬度
    public String locationY;//经度
}
