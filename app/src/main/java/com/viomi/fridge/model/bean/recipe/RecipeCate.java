package com.viomi.fridge.model.bean.recipe;

import java.util.ArrayList;

public class RecipeCate {
    private categoryInfo categoryInfo;
    private ArrayList<Secondlevel> childs = new ArrayList<>();

    public categoryInfo getCategoryInfo() {
        return categoryInfo;
    }

    public void setCategoryInfo(categoryInfo categoryInfo) {
        this.categoryInfo = categoryInfo;
    }

    public ArrayList<Secondlevel> getChilds() {
        return childs;
    }

    public void setChilds(ArrayList<Secondlevel> childs) {
        this.childs = childs;
    }
}
