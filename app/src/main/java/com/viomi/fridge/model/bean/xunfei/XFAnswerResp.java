package com.viomi.fridge.model.bean.xunfei;

import com.viomi.fridge.model.bean.xunfei.XunFeiResp;

import java.util.List;

/**
 * Created by young2 on 2017/3/31.
 */

public class XFAnswerResp extends XunFeiResp {
    public Answer answer;
    public List<MoreResult> moreResults;

    public class  MoreResult{
        public String text;
        public String service;
        public Data data;
    }

    public class Data{
        public List<Result> result;
    }

    public class Result{
        public String mp3Url;
        public String title;
    }


    public class Answer{
        public String type;
        public String text;
    }
}
