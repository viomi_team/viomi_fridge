package com.viomi.fridge.model.bean;

import java.io.Serializable;

/**
 * Created by young2 on 2016/9/7.
 */
public class PushSetInfo implements Serializable {

    private boolean UserEnable=true;
    private boolean DeviceEnable=true;
    private boolean AdvertEnable=true;
    private String userAlias;
    private String deviceAlias;
    private boolean  pushBlockEnable=true;
    private int pushBlockTimeStart=22;
    private int pushBlockTimeEnd=8;

    public boolean isPushBlockEnable() {
        return pushBlockEnable;
    }

    public void setPushBlockEnable(boolean pushBlockEnable) {
        this.pushBlockEnable = pushBlockEnable;
    }

    public boolean isUserEnable() {
        return UserEnable;
    }

    public void setUserEnable(boolean userEnable) {
        UserEnable = userEnable;
    }

    public boolean isDeviceEnable() {
        return DeviceEnable;
    }

    public void setDeviceEnable(boolean deviceEnable) {
        DeviceEnable = deviceEnable;
    }

    public boolean isAdvertEnable() {
        return AdvertEnable;
    }

    public void setAdvertEnable(boolean advertEnable) {
        AdvertEnable = advertEnable;
    }

    public String getUserAlias() {
        return userAlias;
    }

    public void setUserAlias(String userAlias) {
        this.userAlias = userAlias;
    }

    public String getDeviceAlias() {
        return deviceAlias;
    }

    public void setDeviceAlias(String deviceAlias) {
        this.deviceAlias = deviceAlias;
    }

    public int getPushBlockTimeStart() {
        return pushBlockTimeStart;
    }

    public void setPushBlockTimeStart(int pushBlockTimeStart) {
        this.pushBlockTimeStart = pushBlockTimeStart;
    }

    public int getPushBlockTimeEnd() {
        return pushBlockTimeEnd;
    }

    public void setPushBlockTimeEnd(int pushBlockTimeEnd) {
        this.pushBlockTimeEnd = pushBlockTimeEnd;
    }
}
