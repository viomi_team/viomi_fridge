package com.viomi.fridge.model.bean;

/**
 * Created by viomi on 2017/2/22.
 */

public class GoodsEntity {
    private String name;
    private String imageUrl;
    private double price;
    private String targetId;
    private int targetType;

    public GoodsEntity() {
    }

    public GoodsEntity(String name, String imageUrl, double price, String targetId, int targetType) {
        this.name = name;
        this.imageUrl = imageUrl;
        this.price = price;
        this.targetId = targetId;
        this.targetType = targetType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getTargetId() {
        return targetId;
    }

    public void setTargetId(String targetId) {
        this.targetId = targetId;
    }

    public int getTargetType() {
        return targetType;
    }

    public void setTargetType(int targetType) {
        this.targetType = targetType;
    }
}
