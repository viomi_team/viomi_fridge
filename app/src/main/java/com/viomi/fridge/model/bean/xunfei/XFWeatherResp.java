package com.viomi.fridge.model.bean.xunfei;

import java.util.List;

/**
 * Created by young2 on 2016/12/26.
 * {
 "webPage": {
 "header": "",
 "url": "http://kcgz.openspeech.cn/service/iss?wuuid=41aef379e598b974697fc794c1cbdd8d&ver=2.0&method=webPage&uuid=7cbe18b1328f8b8ee74347e76e01da10query"
 },
 "semantic": {
 "slots": {
 "location": {
 "type": "LOC_POI",
 "city": "CURRENT_CITY",
 "poi": "CURRENT_POI"
 },
 "datetime": {
 "date": "2016-12-26",
 "type": "DT_BASIC",
 "dateOrig": "今天"
 }
 }
 },
 "rc": 0,
 "operation": "QUERY",
 "service": "weather",
 "data": {
 "result": [
 {
 "airQuality": "轻微污染",
 "sourceName": "中国天气网",
 "date": "2016-12-26",
 "lastUpdateTime": "2016-12-26 12:39:46",
 "dateLong": 1482681600,
 "pm25": "83",
 "city": "佛山",
 "humidity": "67%",
 "windLevel": 0,
 "weather": "多云",
 "tempRange": "12℃~22℃",
 "wind": "无持续风向微风"
 },
 {
 "dateLong": 1482768000,
 "sourceName": "中国天气网",
 "date": "2016-12-27",
 "lastUpdateTime": "2016-12-26 12:39:46",
 "city": "佛山",
 "windLevel": 1,
 "weather": "多云转晴",
 "tempRange": "9℃~19℃",
 "wind": "北风3-4级"
 },
 {
 "dateLong": 1482854400,
 "sourceName": "中国天气网",
 "date": "2016-12-28",
 "lastUpdateTime": "2016-12-26 12:39:46",
 "city": "佛山",
 "windLevel": 1,
 "weather": "晴",
 "tempRange": "8℃~16℃",
 "wind": "北风3-4级"
 },
 {
 "dateLong": 1482940800,
 "sourceName": "中国天气网",
 "date": "2016-12-29",
 "lastUpdateTime": "2016-12-26 12:39:46",
 "city": "佛山",
 "windLevel": 1,
 "weather": "多云",
 "tempRange": "8℃~17℃",
 "wind": "北风3-4级"
 },
 {
 "dateLong": 1483027200,
 "sourceName": "中国天气网",
 "date": "2016-12-30",
 "lastUpdateTime": "2016-12-26 12:39:46",
 "city": "佛山",
 "windLevel": 0,
 "weather": "多云",
 "tempRange": "10℃~19℃",
 "wind": "无持续风向微风"
 },
 {
 "dateLong": 1483113600,
 "sourceName": "中国天气网",
 "date": "2016-12-31",
 "lastUpdateTime": "2016-12-26 12:39:46",
 "city": "佛山",
 "windLevel": 0,
 "weather": "多云",
 "tempRange": "12℃~21℃",
 "wind": "无持续风向微风"
 },
 {
 "dateLong": 1483200000,
 "sourceName": "中国天气网",
 "date": "2017-01-01",
 "lastUpdateTime": "2016-12-26 12:39:46",
 "city": "佛山",
 "windLevel": 0,
 "weather": "多云转中雨",
 "tempRange": "11℃~21℃",
 "wind": "无持续风向微风"
 }
 ]
 },
 "text": "今天天气。"
 }
 */

public class XFWeatherResp extends XunFeiResp {

    public WebPage webPage;//该字段提供了结果内容的HTML5页面，客户端可以无需解析处理直接展现
    public Error error;
  //  public Semantic semantic;
    public Data data;
    public Answer answer;

    public class Answer{
        public String text;
    }

    public class Error{
        public String code;
        public String message;
    }
    public class WebPage{
        public String header;
         public String url;
    }

    public class Datetime{
        public String date;
        public String type;
        public String dateOrig;
    }
    public class Location {
        public String type;
        public String city;
        public String poi;
    }

    public class Slots{
        public Location location;
        public Datetime datetime;
    }

    public class Semantic{
        public Slots slots;
    }

    public class Result{
        public String airQuality;
        public String date;
       // public int airData;
        public String lastUpdateTime;
        public String dateLong;
        public String pm25;
        public String city;
        public String humidity;
        public String windLevel;
        public String weather;
//        public int weatherType;
        public String tempRange;
        public String wind;
        public int temp;
    }

    public class Data{
        public List<Result> result;
    }



}
