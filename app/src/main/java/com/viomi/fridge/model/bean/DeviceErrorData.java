package com.viomi.fridge.model.bean;

/**
 * 设备故障信心
 * Created by William on 2017/12/13.
 */

public class DeviceErrorData {
    public String title;
    public String detail;
}
