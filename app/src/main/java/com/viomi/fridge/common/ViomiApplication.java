package com.viomi.fridge.common;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Process;
import android.support.multidex.MultiDex;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.iflytek.cloud.SpeechConstant;
import com.iflytek.cloud.SpeechUtility;
import com.miot.api.MiotManager;
import com.miot.common.ReturnCode;
import com.miot.common.config.AppConfiguration;
import com.miot.common.model.DeviceModel;
import com.miot.common.model.DeviceModelException;
import com.miot.common.model.DeviceModelFactory;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.umeng.message.IUmengRegisterCallback;
import com.umeng.message.PushAgent;
import com.unilife.common.ui.apps.UMApplication;
import com.viomi.fridge.device.AppConfig;
import com.viomi.fridge.device.DeviceConfig;
import com.viomi.fridge.device.MiDeviceManager;
import com.viomi.fridge.device.WaterPurifierBase;
import com.viomi.fridge.manager.StatsManager;
import com.viomi.fridge.model.bean.MiIndentify;
import com.viomi.fridge.util.PhoneUtil;
import com.viomi.fridge.util.log;
import com.zhuge.analysis.stat.ZhugeSDK;

import java.io.File;


/**
 * Created by young2 on 2016/12/14.
 */

public class ViomiApplication extends UMApplication {
    private static final String TAG = ViomiApplication.class.getSimpleName();
    private static Context mContext;
    private static String SpeechAppid = "58a40571";
    private LocalBroadcastManager mBindBroadcastManager;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate------");
        mContext = getApplicationContext();
        mBindBroadcastManager = LocalBroadcastManager.getInstance(this);
        if (isMainProcess()) {
            Log.d(TAG, "MiotManager init");
            MiotManager.getInstance().initialize(this);
            new MiotOpenTask().execute();

            MiIndentify miIndentify = PhoneUtil.getMiIdentify();
            if (!DeviceConfig.DefaultMac.equals(miIndentify.mac)) {//读不到正常的小米标识，不启动语音初始化
                Log.d(TAG, "SpeechUtility init");
                SpeechUtility.createUtility(mContext, SpeechConstant.APPID + "=" + SpeechAppid);
            }
            Fresco.initialize(this);
            //NoHttp.initialize(this);
            initImageLoader();

            //初始化分析跟踪
            ZhugeSDK.getInstance().init(getApplicationContext());
            if (GlobalParams.LOG_DEBUG) {
                ZhugeSDK.getInstance().openDebug();
            }
            CrashHandler.getInstance().initCrashHandler(this);
        }
        pushInit();
        StatsManager.init(this);//埋点统计初始化
    }

    public static Context getContext() {
        return mContext;
    }

    private void pushInit() {
        PushAgent mPushAgent = PushAgent.getInstance(ViomiApplication.getContext());
        //注册推送服务，每次调用register方法都会回调该接口
        mPushAgent.register(new IUmengRegisterCallback() {
            @Override
            public void onSuccess(String deviceToken) {
                //注册成功会返回device token
                log.d(TAG, "deviceToken=" + deviceToken);
            }

            @Override
            public void onFailure(String s, String s1) {
                Log.e(TAG, "push register fail!s=" + s + ",s1=" + s1);
            }
        });
        mPushAgent.setDebugMode(GlobalParams.LOG_DEBUG);
    }

    private void initImageLoader() {
        DisplayImageOptions mOptions = new DisplayImageOptions.Builder()
                // 设置图片在下载期间显示的图片
                .showImageOnLoading(android.R.color.white)
                // 设置图片Uri为空或是错误的时候显示的图片
                .showImageForEmptyUri(android.R.color.white)
                // 设置图片加载/解码过程中错误时候显示的图片
                .showImageOnFail(android.R.color.white)
                // 设置下载的图片是否缓存在内存中
                .cacheInMemory(true)
                // 设置下载的图片是否缓存在SD卡中
                .cacheOnDisc(true)
                // 是否考虑JPEG图像EXIF参数（旋转，翻转）
                .considerExifParams(true)
                // 设置图片以如何的编码方式显示
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                // 设置图片的解码类型
                .bitmapConfig(Bitmap.Config.RGB_565)
                // 设置图片的解码配置
                // .decodingOptions(options)
                // .delayBeforeLoading(int delayInMillis)//int
                // delayInMillis为你设置的下载前的延迟时间
                // 设置图片加入缓存前，对bitmap进行设置
                // .preProcessor(BitmapProcessor preProcessor)
                // 设置图片在下载前是否重置，复位
                .resetViewBeforeLoading(true)
                // 是否设置为圆角，弧度为多少
//                .displayer(new RoundedBitmapDisplayer(PhoneUtil.dipToPx(context, 12)))
                // 是否图片加载好后渐入的动画时间
//                .displayer(new FadeInBitmapDisplayer(100))
                // 构建完成
                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(mContext)
                .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
                .diskCacheSize(50 * 1024 * 1024)
                .diskCacheFileCount(100)
                .defaultDisplayImageOptions(mOptions)//设置默认的显示图片选择
                .denyCacheImageMultipleSizesInMemory()
                .memoryCacheExtraOptions(400, 400)
                .memoryCache(new WeakMemoryCache())
                .threadPriority(Thread.NORM_PRIORITY - 2) // 线程数量,默认5个
                .tasksProcessingOrder(QueueProcessingType.LIFO)//设置任务订单处理(队列处理类型)
                // default
                .denyCacheImageMultipleSizesInMemory()//否认在内存中缓存图像多种尺寸
                .memoryCache(new LruMemoryCache(2 * 1024 * 1024))
                .memoryCacheSize(2 * 1024 * 1024)
                .memoryCacheSizePercentage(13)
                //.diskCache(new UnlimitedDiscCache(StorageUtils.getCacheDirectory(this, true)))
                .build();
        ImageLoader.getInstance().init(config);
    }

    private class MiotOpenTask extends AsyncTask<Void, Void, Integer> {
        @Override
        protected Integer doInBackground(Void... params) {
            AppConfiguration appConfig = new AppConfiguration();
            if (GlobalParams.getInstance().getScanPhoneType() == 0) {
                appConfig.setAppId(AppConfig.OAUTH_ANDROID_APP_ID);
                appConfig.setAppKey(AppConfig.OAUTH_ANDROID_APP_KEY);
            } else {
                appConfig.setAppId(AppConfig.OAUTH_IOS_APP_ID);
                appConfig.setAppKey(AppConfig.OAUTH_IOS_APP_KEY);
            }

            MiotManager.getInstance().setAppConfig(appConfig);

            try {
                DeviceModel water1 = DeviceModelFactory.createDeviceModel(ViomiApplication.this, AppConfig.YUNMI_WATERPURIFIER_V1,
                        AppConfig.YUNMI_WATERPURIFIER_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(water1);
                DeviceModel water2 = DeviceModelFactory.createDeviceModel(ViomiApplication.this, AppConfig.YUNMI_WATERPURIFIER_V2,
                        AppConfig.YUNMI_WATERPURIFIER_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(water2);
                DeviceModel water3 = DeviceModelFactory.createDeviceModel(ViomiApplication.this, AppConfig.YUNMI_WATERPURIFIER_V3,
                        AppConfig.YUNMI_WATERPURIFIER_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(water3);
                DeviceModel water4 = DeviceModelFactory.createDeviceModel(ViomiApplication.this, AppConfig.YUNMI_WATERPURI_LX2,
                        AppConfig.YUNMI_WATERPURIFIER_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(water4);
                DeviceModel water5 = DeviceModelFactory.createDeviceModel(ViomiApplication.this, AppConfig.YUNMI_WATERPURI_LX3,
                        AppConfig.YUNMI_WATERPURIFIER_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(water5);
                DeviceModel water6 = DeviceModelFactory.createDeviceModel(ViomiApplication.this, AppConfig.YUNMI_WATERPURI_V1,
                        AppConfig.YUNMI_WATERPURI_V1_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(water6);
                DeviceModel water7 = DeviceModelFactory.createDeviceModel(ViomiApplication.this, AppConfig.YUNMI_WATERPURI_V2,
                        AppConfig.YUNMI_WATERPURI_V2_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(water7);
                DeviceModel water8 = DeviceModelFactory.createDeviceModel(ViomiApplication.this, AppConfig.YUNMI_WATERPURI_S1,
                        AppConfig.YUNMI_WATERPURI_S1_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(water8);
                DeviceModel water9 = DeviceModelFactory.createDeviceModel(ViomiApplication.this, AppConfig.YUNMI_WATERPURI_C1,
                        AppConfig.YUNMI_WATERPURI_C1_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(water9);
                DeviceModel water10 = DeviceModelFactory.createDeviceModel(ViomiApplication.this, AppConfig.YUNMI_WATERPURI_C2,
                        AppConfig.YUNMI_WATERPURI_C2_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(water10);
                DeviceModel water11 = DeviceModelFactory.createDeviceModel(ViomiApplication.this, AppConfig.YUNMI_WATERPURI_S2,
                        AppConfig.YUNMI_WATERPURI_S2_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(water11);
                DeviceModel water12 = DeviceModelFactory.createDeviceModel(ViomiApplication.this, AppConfig.YUNMI_WATERPURI_X3,
                        AppConfig.YUNMI_WATERPURI_X3_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(water12);
                DeviceModel water13 = DeviceModelFactory.createDeviceModel(ViomiApplication.this, AppConfig.YUNMI_WATERPURI_X5,
                        AppConfig.YUNMI_WATERPURI_X5_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(water13);

                DeviceModel fridgeV1Model = DeviceModelFactory.createDeviceModel(ViomiApplication.this, AppConfig.VIOMI_FRIDGE_V1,
                        AppConfig.VIOMI_FRIDGE_V1_URL, WaterPurifierBase.class);
                // MiotManager.getInstance().addModel(water17);
                DeviceModel fridgeV2Model = DeviceModelFactory.createDeviceModel(ViomiApplication.this, AppConfig.VIOMI_FRIDGE_V2,
                        AppConfig.VIOMI_FRIDGE_V1_URL, WaterPurifierBase.class);
                //  MiotManager.getInstance().addModel(fridgeV2Model);
                DeviceModel fridgeU1Model = DeviceModelFactory.createDeviceModel(ViomiApplication.this, AppConfig.VIOMI_FRIDGE_U1,
                        AppConfig.VIOMI_FRIDGE_V1_URL, WaterPurifierBase.class);
                //  MiotManager.getInstance().addModel(fridgeU1Model);

                DeviceModel hoodT8Model = DeviceModelFactory.createDeviceModel(ViomiApplication.this, AppConfig.VIOMI_HOOD_T8,
                        AppConfig.VIOMI_HOOD_T8_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(hoodT8Model);
                DeviceModel hoodA6Model = DeviceModelFactory.createDeviceModel(ViomiApplication.this, AppConfig.VIOMI_HOOD_A6,
                        AppConfig.VIOMI_HOOD_A6_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(hoodA6Model);
                DeviceModel hoodA7Model = DeviceModelFactory.createDeviceModel(ViomiApplication.this, AppConfig.VIOMI_HOOD_A7,
                        AppConfig.VIOMI_HOOD_A7_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(hoodA7Model);
                DeviceModel hoodC6Model = DeviceModelFactory.createDeviceModel(ViomiApplication.this, AppConfig.VIOMI_HOOD_C6,
                        AppConfig.VIOMI_HOOD_C6_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(hoodC6Model);
                DeviceModel hoodA4Model = DeviceModelFactory.createDeviceModel(ViomiApplication.this, AppConfig.VIOMI_HOOD_A4,
                        AppConfig.VIOMI_HOOD_A4_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(hoodA4Model);
                DeviceModel hoodA5Model = DeviceModelFactory.createDeviceModel(ViomiApplication.this, AppConfig.VIOMI_HOOD_A5,
                        AppConfig.VIOMI_HOOD_A5_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(hoodA5Model);
                DeviceModel hoodC1Model = DeviceModelFactory.createDeviceModel(ViomiApplication.this, AppConfig.VIOMI_HOOD_C1,
                        AppConfig.VIOMI_HOOD_C1_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(hoodC1Model);
                DeviceModel hoodH1Model = DeviceModelFactory.createDeviceModel(ViomiApplication.this, AppConfig.VIOMI_HOOD_H1,
                        AppConfig.VIOMI_HOOD_H1_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(hoodH1Model);
                DeviceModel hoodH2Model = DeviceModelFactory.createDeviceModel(ViomiApplication.this, AppConfig.VIOMI_HOOD_H2,
                        AppConfig.VIOMI_HOOD_H2_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(hoodH2Model);

                DeviceModel R1Model = DeviceModelFactory.createDeviceModel(ViomiApplication.this, AppConfig.YUNMI_KETTLE_R1,
                        AppConfig.YUNMI_KETTLE_R1_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(R1Model);

                DeviceModel mg2Model = DeviceModelFactory.createDeviceModel(ViomiApplication.this, AppConfig.YUNMI_PL_MACHINE_MG2,
                        AppConfig.YUNMI_PL_MACHINE_MG2_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(mg2Model);
            } catch (DeviceModelException e) {
                e.printStackTrace();
            }
            return MiotManager.getInstance().open();
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);

            do {
                int result = integer;
                Log.d(TAG, "MiotOpen result: " + result);
                Intent intent = new Intent(AppConfig.ACTION_BIND_SERVICE_FAILED);
                if (result == ReturnCode.OK) {
                    intent = new Intent(AppConfig.ACTION_BIND_SERVICE_SUCCEED);
                    MiDeviceManager.getInstance().getWanDeviceList();
                }
                mBindBroadcastManager.sendBroadcast(intent);
            }
            while (false);
        }

    }

    private boolean isMainProcess() {
        String mainProcessName = getPackageName();
        String processName = getProcessName();
        return TextUtils.equals(processName, mainProcessName);
    }

    private String getProcessName() {
        int pid = Process.myPid();
        ActivityManager activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningAppProcessInfo processInfo : activityManager.getRunningAppProcesses()) {
            if (processInfo.pid == pid) {
                return processInfo.processName;
            }
        }
        return null;
    }

    private class MiotCloseTask extends AsyncTask<Void, Void, Integer> {
        @Override
        protected Integer doInBackground(Void... params) {
            return MiotManager.getInstance().close();
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);

            do {
                int result = integer;
                Log.d(TAG, "MiotClose result: " + result);
            }
            while (false);
        }
    }


    @Override
    public void onTerminate() {
        Log.e(TAG, "onTerminate");
        new MiotCloseTask().execute();
        super.onTerminate();
    }

    @Override
    public String getBrand() {
        return null;
    }

    @Override
    public String getModel() {
        return null;
    }

    @Override
    public String getDeviceType() {
        return null;
    }

    @Override
    public String getRemoteCtrlServiceAddress() {
        return null;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(base);
    }
}


