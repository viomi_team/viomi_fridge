package com.viomi.fridge.common_api;

import android.content.Context;
import android.content.Intent;

import com.viomi.fridge.view.activity.Timer2Activity;

/**
 * Created by Mocc on 2017/5/17
 * 定时器控制API
 */

public class TimerApi {

    private TimerCallback callback;
    private static TimerApi timerApi;

    private TimerApi() {
    }

    public static TimerApi getInstance() {
        if (timerApi == null) {
            synchronized (TimerApi.class) {
                if (timerApi == null) {
                    timerApi = new TimerApi();
                }
            }
        }
        return timerApi;
    }

    public void removeCallback() {
        this.callback = null;
    }

    public void setCallback(TimerCallback callback) {
        this.callback = callback;
    }

    private static boolean isActivityResume(Context context, int startMode, int time, int mode) {
        boolean isResume = false;
        if (getInstance().callback == null) {
            Intent intent = new Intent(context, Timer2Activity.class);
            intent.putExtra("startMode", startMode);
            intent.putExtra("time", time);
            intent.putExtra("mode", mode);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        } else {
            isResume = true;
        }
        return isResume;
    }


    public interface TimerCallback {

        void start();

        void start(int time);

        void start(int time, int mode);

        void pause();

        void continue_t();

        void cancel();
    }


    //启动计时器
    public static void start(Context context) {
        if (isActivityResume(context, 1,-1,-1)) {
            getInstance().callback.start();
        }
    }

    //启动计时器，设定时间
    public static void start(Context context, int time) {
        if (isActivityResume(context, 2,time,-1)) {
            getInstance().callback.start(time);
        }
    }
    //启动计时器，设定模式（模式为1,2,3,4,5,6对应6种模式，当模式为-1时，默认为设定时间模式）
    public static void start(Context context, int time, int mode) {
        if (isActivityResume(context, 3,time,mode)) {
            getInstance().callback.start(time, mode);
        }
    }

    //暂停
    public static void pause(Context context) {
        if (isActivityResume(context, 4,-1,-1)) {
            getInstance().callback.pause();
        }
    }

    //继续
    public static void continue_t(Context context) {
        if (isActivityResume(context, 5,-1,-1)) {
            getInstance().callback.continue_t();
        }
    }

    //取消
    public static void cancel(Context context) {
        if (isActivityResume(context, 6,-1,-1)) {
            getInstance().callback.cancel();
        }
    }

}
