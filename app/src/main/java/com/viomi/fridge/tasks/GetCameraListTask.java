package com.viomi.fridge.tasks;

import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;

import com.viomi.fridge.manager.CameraListManager;
import com.viomi.fridge.util.log;
import com.viomi.fridge.view.activity.ConnectUnionActivity;

/**
 * 获取 IPC 列表 Task
 * Created by William on 2017/11/9.
 */

public class GetCameraListTask extends AsyncTask<Void, Void, Integer> {

    private Handler mHandler;
    private final static String TAG = GetCameraListTask.class.getSimpleName();

    public GetCameraListTask(Handler handler) {
        this.mHandler = handler;
    }

    @Override
    protected Integer doInBackground(Void... params) {
        if (isCancelled()) {
            log.d(TAG, "null");
            return null;
        }
        else return CameraListManager.getInstance().getCameraListFromServer();
    }

    @Override
    protected void onPostExecute(Integer result) {
        if (isCancelled()) {
            log.d(TAG, "GetCameraListTask is stop");
        } else {
            log.d(TAG, "result=" + result);
            if (result == 0) {   // 正常返回
                Message.obtain(mHandler, ConnectUnionActivity.IPC_GET_DEVICE_SUCCESS).sendToTarget();
            } else {
                Message.obtain(mHandler, ConnectUnionActivity.IPC_GET_DEVICE_FAIL).sendToTarget();
            }
        }
    }

}
