package com.viomi.fridge.tasks;

import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;

import com.v2.clsdk.model.LoginResult;
import com.viomi.fridge.util.SDKInstance;
import com.viomi.fridge.util.log;
import com.viomi.fridge.view.activity.ConnectUnionActivity;

/**
 * IPC 登录 Task
 * Created by William on 2017/11/9.
 */

public class CameraLoginTask extends AsyncTask<String, Void, LoginResult> {

    private final static String TAG = CameraLoginTask.class.getSimpleName();
    private Handler mHandler;

    public CameraLoginTask(Handler handler) {
        mHandler = handler;
    }

    @Override
    protected LoginResult doInBackground(String... strings) {
        if (isCancelled()) {
            log.d(TAG, "null");
            return null;
        } else return SDKInstance.getInstance().login(strings[0], strings[1]);
    }

    @Override
    protected void onPostExecute(LoginResult loginResult) {
        if (isCancelled()) {
            log.d(TAG, "CameraLoginTask is stop");
        } else {
            log.d(TAG, loginResult.getCode() + "");
            if (loginResult.getCode() == 0) {    // 登录成功
                Message.obtain(mHandler, ConnectUnionActivity.IPC_LOGIN_SUCCESS).sendToTarget();
            } else {
                Message.obtain(mHandler, ConnectUnionActivity.IPC_LOGIN_FAIL).sendToTarget();
            }
        }
    }

}
