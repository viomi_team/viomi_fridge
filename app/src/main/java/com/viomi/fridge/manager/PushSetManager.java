package com.viomi.fridge.manager;

import android.util.Log;

import com.viomi.fridge.common.ViomiApplication;
import com.viomi.fridge.model.bean.PushSetInfo;
import com.viomi.fridge.util.FileUtil;

/**
 * Created by young2 on 2016/9/7.
 */
public class PushSetManager {
    public final static String PushSetFile="PushSet.dat";

    public static void savePushSetInfo( PushSetInfo pushSetInfo){
        FileUtil.saveObject(ViomiApplication.getContext(),PushSetFile,pushSetInfo);
    }

    public static PushSetInfo getPushSetInfo(){
        PushSetInfo pushSetInfo= (PushSetInfo) FileUtil.getObject(ViomiApplication.getContext(),PushSetFile);
        if(pushSetInfo!=null){
            pushSetInfo.setUserAlias(formatNull(pushSetInfo.getUserAlias()));
            pushSetInfo.setDeviceAlias(formatNull(pushSetInfo.getDeviceAlias()));
        }else {
            Log.e(PushSetFile,"read PushSetFile error!");
            pushSetInfo=new PushSetInfo();
        }
        return pushSetInfo;
    }

    public static void deletePushSetInfo(){
        FileUtil.saveObject(ViomiApplication.getContext(),PushSetFile,null);
    }

    private static String formatNull(String input){
        if(input==null||input.equals("null")){
            return null;
        }
        return input;
    }
}
