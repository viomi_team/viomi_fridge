package com.viomi.fridge.manager;

import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.viomi.common.callback.AppCallback;
import com.viomi.common.module.okhttp.OkHttpClientManager;
import com.viomi.fridge.api.http.HttpConnect;
import com.viomi.fridge.api.http.HttpParser;
import com.viomi.fridge.common.GlobalParams;
import com.viomi.fridge.common.ViomiApplication;
import com.viomi.fridge.model.bean.H5UpgradeMsg;
import com.viomi.fridge.util.ApkUtil;
import com.viomi.fridge.util.FileUtil;
import com.viomi.fridge.util.ZipUtils;
import com.viomi.fridge.util.log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import okhttp3.Call;

/**
 * Created by young2 on 2016/9/13.
 */
public class H5UrlManager {
    private final static String TAG="H5UrlManager";

 //  private final static String Vmall_H5_Path=WaterApplication.getContext().getFilesDir().getPath();
    private static String Vmall_H5_Path= ViomiApplication.getContext().getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();
    private final static String Vmall_H5_File="fridge_vmall_h5";
    private final static String Vmall_H5_File_Zip= "fridge_vmall_h5.zip";
    public final static String VmallH5Path=Vmall_H5_Path+"/"+Vmall_H5_File;//H5代码存放位置
    public final static String VmallH5ZipPath=Vmall_H5_Path+"/"+Vmall_H5_File_Zip;//H5压缩包路径
    public final static String VmallDomainRelease=VmallH5Path+"/asset";//读取源码路径
    private final static String Vmall_H5_Profile="package.json";//商城H5配置文件

    //  public final static String VmallDomainRelease="file:///android_asset/asset";
    public final static String VmallDomain="192.168.0.129:3000";
    public final static String MainDomainSuffix="/index.html";
    public final static String CouponDomainSuffix="/coupon.html";
    public final static String AddressDomainSuffix="/addr.html";
    public final static String OrderDomainSuffix="/orderlist.html";
    public final static String InstallDomainSuffix="/install.html";
    private final static String mVmallUrlWeb = "http://viomi-fridge-vmall.mi-ae.net";

    private final static  int MSG_GET_H5_UPGRADE_FAIL=0;//取h5信息失败
    private final static  int MSG_GET_H5_UPGRADE_SUCCESS=1;//取h5信息

    private static H5UrlManager INSTANCE;
    private AppCallback<String> mCallback;
    private Call mGetInfoCall;
    private Call mDownloadCall;

    private Handler mHandler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            try {
                switch (msg.what){
                    case MSG_GET_H5_UPGRADE_FAIL:
                        if(mCallback!=null){
                            mCallback.onFail(-100,"get h5 update msg error!");
                        }
                        break;
                    case MSG_GET_H5_UPGRADE_SUCCESS:
                        String response= (String) msg.obj;
                        H5UpgradeMsg h5UpgradeMsg= HttpParser.parserH5UpgradeInfo(response);
                        if(h5UpgradeMsg.url==null||h5UpgradeMsg.url.length()==0){
                            Log.e(TAG,"get h5 url null");
                            mCallback.onFail(-100,"get h5 url null");
                        }else {
                          //  Log.e(TAG,"aaaa="+GlobalParams.Version_Vmall_H5+",ddd="+h5UpgradeMsg.versionCode);
                            if(ApkUtil.getVersion().compareToIgnoreCase(h5UpgradeMsg.minAppVersion)>=0) {
                                if (GlobalParams.getInstance().getVmallH5Version() < h5UpgradeMsg.versionCode) {
                                    downloadH5Zip(h5UpgradeMsg.url);
                                }
                            }
                        }
                        break;
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    };

    public static H5UrlManager getInstance(){
        if(INSTANCE==null){
            synchronized (H5UrlManager.class){
                if(INSTANCE==null){
                    INSTANCE=new H5UrlManager();
                }
            }
        }
        return INSTANCE;
    }

    /***
     * 获取通用页url
     * @param url 后缀
     * @return
     */
    public static String getCommonUrl(String url){
        if(url.contains(mVmallUrlWeb)){
            int index=url.indexOf(mVmallUrlWeb);
            url=url.substring(0,index)+url.substring(index+mVmallUrlWeb.length());
        }
        if(GlobalParams.H5_DEBUG){
      //      if(GlobalParams.getInstance().isVmallDebug()){
                if(true){
                //String debugUrl=GlobalParams.getInstance().getVmallDebugUrl();
                    String  debugUrl="0";
                if("0".equals(debugUrl)){
                    url=mVmallUrlWeb+url;

                    return url;
                }else if(debugUrl.startsWith("192.")){
                    url=debugUrl+url;

                    return url;
                }

            }
                url=VmallDomainRelease+url;
                int index=url.indexOf("Android");
                if(index>0){
                    url="file:///mnt/sdcard/"+url.substring(index);//直接访问sd文件，不能打开H5
                }

        }else {
            url=VmallDomainRelease+url;
            int index=url.indexOf("Android");
            if(index>0){
                url="file:///mnt/sdcard/"+url.substring(index);//直接访问sd文件，不能打开H5
            }
        }

        log.d("getCommonUrl",url);
        return url;
    }

    /***
     * 更新商城H5插件包
     * @param callback
     */
    public Call updateVmallH5Zip(AppCallback<String> callback){

        mCallback=callback;
       return getH5UpdateInfo();
    }

    /***
     * 获取h5插件包信息
     */
    private Call getH5UpdateInfo(){
        mGetInfoCall= HttpConnect.getVmallUpgradeInfo(new OkHttpClientManager.ResultCallback<String>() {
            @Override
            public void onError(Call call, Exception e) {
                Log.e(TAG,"getVmallUpgradeInfo fail! msg:"+e.getMessage());
                if(mHandler!=null){
                    mHandler.sendEmptyMessage(MSG_GET_H5_UPGRADE_FAIL);
                }
            }

            @Override
            public void onResponse(String response) {
                log.d(TAG,"getVmallUpgradeInfo response， msg:"+response);
                if(mHandler!=null){
                    Message message=mHandler.obtainMessage();
                    message.obj=response;
                    message.what=MSG_GET_H5_UPGRADE_SUCCESS;
                    mHandler.sendMessage(message);
                }
            }
        });
        return mGetInfoCall;
    }

    /***
     * 下载h5插件
     */
    private void downloadH5Zip(String url){

       mDownloadCall= HttpConnect.downloadFileAsync(url, Vmall_H5_Path, new OkHttpClientManager.ResultCallback<String>() {
            @Override
            public void onError(Call call, Exception e) {
                Log.e(TAG,"downloadFileAsync fail! msg:"+e.getMessage());
                mCallback.onFail(-100,"downloadFileAsync error!");
            }

            @Override
            public void onResponse(String response) {
                log.d(TAG,"downloadFileAsync file path:"+response);
                if(response!=null){
                    mCallback.onSuccess(response);
                }else {
                    mCallback.onFail(-100,"downloadFileAsync response null!");
                }
            }
        });
    }

    /***
     * 清除h5文件，用于新安装app时，防止有旧的文件残留，影响下载
     */
    public static void clearH5Files(){
        try{
            FileUtil.deleteAll(new File(VmallH5Path));
            FileUtil.deleteAll(new File(VmallH5ZipPath));
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    /***
     * 商城H5文件处理
     *(1) 先判断目标目录下有没有Zip文件，如果有：先删除目录下h5文件，然后解压缩；如果没有，判断目录下有没有H5文件，
     *(2)有H5文件不处理，无H5文件的话，则把assets带的zip文件解压到目录下
     *(3)删除zip文件
     */
    public static boolean vmallH5Process(){
        boolean result=true;
        File file=new File(VmallH5ZipPath);
        if(file.exists()){
            Log.i(TAG,"zip file exist");
            FileUtil.deleteAll(new File(VmallH5Path));
            result= upZipH5File();
        }else {
            Log.i(TAG,"zip file not exist");
            File indexFile=new File(VmallDomainRelease+MainDomainSuffix);
            if(!indexFile.exists()){
                Log.i(TAG,"h5 file not exist");
                if(getH5AssetsFile()){
                    Log.i(TAG,"load assets H5 source success!");
                    result= upZipH5File();
                }else {
                    Log.e(TAG,"load assets H5 source fail!!!!!!!!!!!!!!!!!!!");
                    result= false;
                }
            }else {
                Log.i(TAG,"h5 file exist");
            }
        }
       FileUtil.deleteAll(new File(VmallH5ZipPath));
        return result;
    }

    /***
     * assets里的zip包转移到对应文件夹
     * @return
     */
    private static  boolean getH5AssetsFile()
    {
        if(GlobalParams.hasSdcard()) {
            try {
                FileUtil.creatFile(VmallH5ZipPath);

                //得到资源中的asset数据流
                InputStream inputStream = ViomiApplication.getContext().getResources().getAssets().open(Vmall_H5_File_Zip);
                int len;
                byte[] buffer = new byte[1024];
                FileOutputStream outputStream = new FileOutputStream(VmallH5ZipPath);
                while ((len = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, len);
                }
                outputStream.flush();
                outputStream.close();
                inputStream.close();
                return true;
            } catch (Exception e) {
                Log.e(TAG,"getH5AssetsFile fail");
                e.printStackTrace();
            }
        }
        return false;
    }

    /***
     * 解压缩商城H5文件
     */
    private static  boolean upZipH5File(){
        File file=new File(VmallH5ZipPath);
        if(!file.exists()){
            Log.e(TAG,"upZipH5File file not exist!");
            return false;
        }
        try {
            ZipUtils.upZipFile(file,VmallH5Path+"/");
        } catch (IOException e) {
            Log.e(TAG,"upZipH5File fail!");
            e.printStackTrace();
            return false;
        }
        Log.i(TAG,"upZipH5File file success!");
        return true;
    }

    /***
     * 获取h5插件版本号
     * @return
     */
    public static int getH5Version(){
        String profile=FileUtil.readFile(VmallDomainRelease+"/"+Vmall_H5_Profile);
        if(profile==null||profile.length()==0){
            Log.e(TAG,"getH5Version null!");
            return 0;
        }
        try {
            JSONObject jsonObject=new JSONObject(profile);
            return jsonObject.getInt("versionCode");

        } catch (JSONException e) {
            Log.e(TAG,"getH5Version fail");
            e.printStackTrace();
            return 0;
        }
    }



    public void close(){
        mHandler=null;
        mCallback=null;
        if(mGetInfoCall!=null){
            mGetInfoCall.cancel();
        }
        if(mDownloadCall!=null){
            mDownloadCall.cancel();
        }
    }


}
