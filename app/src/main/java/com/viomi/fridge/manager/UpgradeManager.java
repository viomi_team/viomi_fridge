package com.viomi.fridge.manager;

import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.viomi.common.callback.ProgressCallback;
import com.viomi.common.module.okhttp.OkHttpClientManager;
import com.viomi.common.module.okhttp.progress.ProgressListener;
import com.viomi.fridge.api.http.HttpConnect;
import com.viomi.fridge.broadcast.BroadcastAction;
import com.viomi.fridge.common.GlobalParams;
import com.viomi.fridge.common.ViomiApplication;
import com.viomi.fridge.device.AppConfig;
import com.viomi.fridge.device.DeviceConfig;
import com.viomi.fridge.model.bean.AppUpgradeMsg;
import com.viomi.fridge.model.bean.McuUpgradeProgress;
import com.viomi.fridge.model.bean.SystemUpgradeMsg;
import com.viomi.fridge.model.bean.UpgradeProgress;
import com.viomi.fridge.util.ApkUtil;
import com.viomi.fridge.util.log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.Call;

/**
 * Created by young2 on 2017/2/16.
 */

public class UpgradeManager {
    private final static String TAG="UpgradeManager";
    private static UpgradeManager INSTANCE=null;
    private LocalBroadcastManager mBroadcastManager;
    public final static  int OPERATE_MANUAL=0;//人工检测升级，要UI界面
    public final static  int OPERATE_AUTO=1;//后台自动检测升级，不需要UI
    private final static  int RESULT_SUCCESS=0;//成功
    private final static  int RESULT_FAIL=-1;//失败
    private Context mContext;
    private ProgressCallback mUpgradeCallback;

    private UpgradeProgress mAppUpgradeProgress,mSystemUpgradeProgress;
    private McuUpgradeProgress mMcuUpgradeProgress;

    private static String App_Download_Path= ViomiApplication.getContext().getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();
    private static String System_Download_Path= Environment.getExternalStorageDirectory().getAbsolutePath();
    private static String Mcu_Download_Path= ViomiApplication.getContext().getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();
    private static String System_Package_V1="viomi.fridge.system.v1";//V1冰箱系统包名，升级后台统一
    private static String System_Package_V2="viomi.fridge.system.v2";//V2冰箱系统包名，升级后台统一

    private static String Fridge_V1_Package="viomi.fridge.v1.mcu";//冰箱v1固件包名，升级后台统一
    private static String Fridge_V4_Package="viomi.fridge.v4.mcu";//冰箱v4固件包名，升级后台统一
    private static String Upgrade_Channel_Rom="rom";//系统rom升级模式
    private static String Upgrade_Channel_Diff="diff";//系统差分包升级模式

    public static final String NeedUpgradeYes="yes";//需要升级
    public static final String NeedUpgradeNo="no";//不需要升级
    public static final String UpgradeSuccess="yes";//升级成功
    public static final String UpgradeFail="no";//升级失败
    public static final String NeedUpgradeError="error";//检测失败

    public static UpgradeManager getInstance(){
        synchronized (UpgradeManager.class){
            if(INSTANCE==null){
                synchronized (UpgradeManager.class){
                    if(INSTANCE==null){
                        INSTANCE=new UpgradeManager();
                    }
                }
            }
        }
        return INSTANCE;
    }
    public UpgradeManager(){
        mBroadcastManager = LocalBroadcastManager.getInstance(ViomiApplication.getContext());
        mAppUpgradeProgress=new UpgradeProgress();
        mSystemUpgradeProgress=new UpgradeProgress();
        mMcuUpgradeProgress=new McuUpgradeProgress();
    }

    public void init(Context context){
        mContext=context;
    }

    /***
     * 获取app升级状态
     * @return
     */
    public UpgradeProgress getAppUpgradeProgress(){
        return mAppUpgradeProgress;
    }

    /***
     * 获取系统升级状态
     * @return
     */
    public UpgradeProgress getSystemUpgradeProgress(){
        return mSystemUpgradeProgress;
    }

    /***
     * 获取Mcu升级状态
     * @return
     */
    public UpgradeProgress getMcuUpgradeProgress(){
        return mMcuUpgradeProgress;
    }

    /***
     * 检测app版本
     * @param type 操作类型{@link #OPERATE_AUTO,#OPERATE_MANUAL}
     */
    public Call checkAppUpgrade(Context context,final int type) {
            mContext=context;
            String realPackageName=ApkUtil.getPackageName();
            String packageName=GlobalParams.HTTP_DEBUG ?
                    (GlobalParams.getInstance().isUpgradeTestEnable() ? realPackageName+".debug" : realPackageName) :realPackageName;

            Call call=HttpConnect.getAppUpgradeInfo(packageName,  new OkHttpClientManager.ResultCallback<String>() {
                    @Override
                    public void onError(Call call, Exception e) {
                        Log.e(TAG,"getAppUpgradeInfo fail! msg:"+e.getMessage());
                        appCheckProgress(null,type,RESULT_FAIL);
                    }

                    @Override
                    public void onResponse(String response) {
                        log.d(TAG,"getAppUpgradeInfo success， msg:"+response);

                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            JSONArray dataList=jsonObject.getJSONArray("data");
                            if(dataList==null||dataList.length()==0){
                                Log.d(TAG,"getAppUpgradeInfo， data null!");
                                appCheckProgress(null,type,RESULT_SUCCESS);
                                return;
                            }
                            JSONObject data= (JSONObject) dataList.get(0);
                            AppUpgradeMsg appUpgradeMsg=new AppUpgradeMsg();
                            appUpgradeMsg.versionCode=data.getInt("code");
                            appUpgradeMsg.upgradeDescription=data.getString("detail");
                            appUpgradeMsg.url=data.getString("url");
                            appCheckProgress(appUpgradeMsg,type,RESULT_SUCCESS);
                        } catch (JSONException e) {
                            Log.e(TAG,"getAppUpgradeInfo fail， msg:"+e.getMessage());
                            appCheckProgress(null,type,RESULT_FAIL);
                        }catch (Exception e){
                            Log.e(TAG,"getAppUpgradeInfo fail， msg:"+e.getMessage());
                            appCheckProgress(null,type,RESULT_FAIL);
                        }
                    }
             });
            return call;
        }

    /***
     * 下载并静默升级app
     * @param type 操作类型{@link #OPERATE_AUTO,#OPERATE_MANUAL}
     */
    public Call upgradeApp(AppUpgradeMsg msg,final int type){
        if(msg==null||msg.url==null){
            Log.e(TAG,"upgradeApp null!");
            return null;
        }
        mAppUpgradeProgress.isUpgrading=true;
        mAppUpgradeProgress.progress=0;
        Call call=HttpConnect.downloadFileAsync(msg.url, App_Download_Path, new OkHttpClientManager.ResultCallback<String>() {
            @Override
            public void onError(Call call, Exception e) {
                Log.e(TAG,"upgradeApp fail! msg:"+e.getMessage());
                mAppUpgradeProgress.isUpgrading=false;
                appDownloadProgress(null,type,RESULT_FAIL);
            }

            @Override
            public void onResponse(String response) {
                if(response==null||response.length()==0){
                    appDownloadProgress(null,type,RESULT_FAIL);
                    Log.e(TAG,"upgradeApp fail! response null!");
                    mAppUpgradeProgress.isUpgrading=false;
                    return;
                }
                log.d(TAG,"upgradeApp successs!path="+response);
                appDownloadProgress(response,type,RESULT_SUCCESS);
                mAppUpgradeProgress.isUpgrading=false;
            }
        }, new ProgressListener() {
            @Override
            public void onProgress(long currentBytes, long contentLength, boolean isFinish) {
                if(contentLength==0){
                    Log.e(TAG,"contentLength is 0 !");
                    appDownloadProgress(null,type,RESULT_FAIL);
                    mAppUpgradeProgress.isUpgrading=false;
                    return;
                }
                log.d(TAG,"downloadFileAsync progress="+currentBytes*100/contentLength+",isFinish="+isFinish);
                mAppUpgradeProgress.progress= (int) (currentBytes*100/contentLength);
                if(mBroadcastManager!=null){
                    Intent intent=new Intent(BroadcastAction.ACTION_APP_DOWNLOAD_PROGRESS);
                    intent.putExtra("progress",(int) (currentBytes*100/contentLength));
                    log.d(TAG,"downloadFileAsync progress="+(int) (currentBytes*100/contentLength));
                    mBroadcastManager.sendBroadcast(intent);
                }

            }
        });
        return call;
    }

    /***
     * 检测系统版本
     * @param type 操作类型{@link #OPERATE_AUTO,#OPERATE_MANUAL}
     */
    public Call checkSystemUpgrade(Context context,final int type) {
        mContext=context;
        String system="";
        if(DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V1)){
            system=System_Package_V1;
        }else if(DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V2)){
            system=System_Package_V2;
        }
        Call call=HttpConnect.getAppUpgradeInfo(system,  new OkHttpClientManager.ResultCallback<String>() {
            @Override
            public void onError(Call call, Exception e) {
                Log.e(TAG,"checkSystemUpgrade fail! msg:"+e.getMessage());
                systemCheckProgress(null,type,RESULT_FAIL);
            }

            @Override
            public void onResponse(String response) {
                log.d(TAG,"checkSystemUpgrade success， msg:"+response);

                try {
                    JSONObject jsonObject=new JSONObject(response);
                    JSONArray dataList=jsonObject.getJSONArray("data");
                    if(dataList==null||dataList.length()==0){
                        Log.d(TAG,"checkSystemUpgrade， data null!");
                        systemCheckProgress(null,type,RESULT_SUCCESS);
                        return;
                    }
                    JSONObject data= (JSONObject) dataList.get(0);
                    SystemUpgradeMsg systemUpgradeMsg=new SystemUpgradeMsg();
                    systemUpgradeMsg.versionCode=data.getInt("code");;
                    systemUpgradeMsg.upgradeDescription=data.getString("detail");
                    systemUpgradeMsg.url=data.getString("url");
                    systemUpgradeMsg.channel=data.getString("channel");
                    systemCheckProgress(systemUpgradeMsg,type,RESULT_SUCCESS);
                } catch (JSONException e) {
                    Log.e(TAG,"checkSystemUpgrade fail， msg:"+e.getMessage());
                    systemCheckProgress(null,type,RESULT_FAIL);
                }catch (Exception e){
                    Log.e(TAG,"checkSystemUpgrade fail， msg:"+e.getMessage());
                    systemCheckProgress(null,type,RESULT_FAIL);
                }
            }
        });
        return call;
    }


    /***
     * 下载并静默升级系统
     * @param type 操作类型{@link #OPERATE_AUTO,#OPERATE_MANUAL}
     */
    public Call upgradeSystem(final SystemUpgradeMsg msg, final int type){
        if(msg==null||msg.url==null){
            Log.e(TAG,"upgradeSystem null!");
            return null;
        }

        mSystemUpgradeProgress.isUpgrading=true;
        mSystemUpgradeProgress.progress=0;
        Call call=HttpConnect.downloadFileAsync(msg.url, System_Download_Path, new OkHttpClientManager.ResultCallback<String>() {
            @Override
            public void onError(Call call, Exception e) {
                Log.e(TAG,"upgradeSystem fail! msg:"+e.getMessage());
                mSystemUpgradeProgress.isUpgrading=false;
                systemDownloadProgress(null,msg.channel,type,RESULT_FAIL);
            }

            @Override
            public void onResponse(String response) {
                if(response==null||response.length()==0){
                    systemDownloadProgress(null,msg.channel,type,RESULT_FAIL);
                    Log.e(TAG,"upgradeSystem fail! response null!");
                    mSystemUpgradeProgress.isUpgrading=false;
                    return;
                }
                log.d(TAG,"upgradeSystem successs!path="+response);
                systemDownloadProgress(response,msg.channel,type,RESULT_SUCCESS);
                mSystemUpgradeProgress.isUpgrading=false;
            }
        }, new ProgressListener() {
            @Override
            public void onProgress(long currentBytes, long contentLength, boolean isFinish) {
                if(contentLength==0){
                    Log.e(TAG,"contentLength is 0 !");
                    systemDownloadProgress(null,msg.channel,type,RESULT_FAIL);
                    mSystemUpgradeProgress.isUpgrading=false;
                    return;
                }
                log.d(TAG,"downloadFileAsync progress="+currentBytes*100/contentLength+",isFinish="+isFinish);
                mSystemUpgradeProgress.progress= (int) (currentBytes*100/contentLength);
                if(mBroadcastManager!=null){
                    Intent intent=new Intent(BroadcastAction.ACTION_SYSTEM_DOWNLOAD_PROGRESS);
                    intent.putExtra("progress",(int) (currentBytes*100/contentLength));
                    mBroadcastManager.sendBroadcast(intent);
                }

            }
        });
        return call;
    }

    /***
     * 检测固件版本
     * @param type 操作类型{@link #OPERATE_AUTO,#OPERATE_MANUAL}
     */
    public Call checkMcuUpgrade(Context context,final int type) {
        mContext=context;
        String packageStr=Fridge_V1_Package;
        if(AppConfig.VIOMI_FRIDGE_V1.equals(DeviceConfig.MODEL)){
            packageStr=Fridge_V1_Package;
        }else  if(AppConfig.VIOMI_FRIDGE_V4.equals(DeviceConfig.MODEL)){
            packageStr=Fridge_V4_Package;
        }

        Call call=HttpConnect.getAppUpgradeInfo(packageStr,  new OkHttpClientManager.ResultCallback<String>() {
            @Override
            public void onError(Call call, Exception e) {
                Log.e(TAG,"checkMcuUpgrade fail! msg:"+e.getMessage());
                mcuCheckProgress(null,type,RESULT_FAIL);
            }

            @Override
            public void onResponse(String response) {
                log.d(TAG,"checkMcuUpgrade success， msg:"+response);

                try {
                    JSONObject jsonObject=new JSONObject(response);
                    JSONArray dataList=jsonObject.getJSONArray("data");
                    if(dataList==null||dataList.length()==0){
                        Log.d(TAG,"checkMcuUpgrade， data null!");
                        mcuCheckProgress(null,type,RESULT_SUCCESS);
                        return;
                    }
                    JSONObject data= (JSONObject) dataList.get(0);
                    AppUpgradeMsg appUpgradeMsg=new AppUpgradeMsg();
                    appUpgradeMsg.versionCode=data.getInt("code");;
                    appUpgradeMsg.upgradeDescription=data.getString("detail");
                    appUpgradeMsg.url=data.getString("url");
                    mcuCheckProgress(appUpgradeMsg,type,RESULT_SUCCESS);
                } catch (JSONException e) {
                    Log.e(TAG,"checkMcuUpgrade fail， msg:"+e.getMessage());
                    mcuCheckProgress(null,type,RESULT_FAIL);
                }catch (Exception e){
                    Log.e(TAG,"checkMcuUpgrade fail， msg:"+e.getMessage());
                    mcuCheckProgress(null,type,RESULT_FAIL);
                }
            }
        });
        return call;
    }

    /***
     * 下载并升级mcu固件
     * @param type 操作类型{@link #OPERATE_AUTO,#OPERATE_MANUAL}
     */
    public void upgradeMcu(final AppUpgradeMsg msg, final int type){
        if(msg==null||msg.url==null){
            Log.e(TAG,"upgradeMcu null!");
            return;
        }

        mMcuUpgradeProgress.isUpgrading=true;
        mMcuUpgradeProgress.progress=0;
        mMcuUpgradeProgress.isDownloadFinish=false;
        HttpConnect.downloadFileAsync(msg.url, Mcu_Download_Path, new OkHttpClientManager.ResultCallback<String>() {
            @Override
            public void onError(Call call, Exception e) {
                Log.e(TAG,"upgradeMcu fail! msg:"+e.getMessage());
                mMcuUpgradeProgress.isUpgrading=false;
                mcuDownloadProgress(null,type,RESULT_FAIL);
            }

            @Override
            public void onResponse(String response) {
                if(response==null||response.length()==0){
                    mcuDownloadProgress(null,type,RESULT_FAIL);
                    Log.e(TAG,"upgradeMcu fail! response null!");
                    mMcuUpgradeProgress.isUpgrading=false;
                    return;
                }
                log.d(TAG,"upgradeMcu successs!path="+response);
                mMcuUpgradeProgress.isDownloadFinish=true;
                mcuDownloadProgress(response,type,RESULT_SUCCESS);
            }
        }, new ProgressListener() {
            @Override
            public void onProgress(long currentBytes, long contentLength, boolean isFinish) {
                if(contentLength==0){
                    Log.e(TAG,"contentLength is 0 !");
                    mcuDownloadProgress(null,type,RESULT_FAIL);
                    mMcuUpgradeProgress.isUpgrading=false;
                    return;
                }
                log.d(TAG,"downloadFileAsync progress="+currentBytes*100/contentLength+",isFinish="+isFinish);
                mSystemUpgradeProgress.progress= (int) (currentBytes*100/contentLength);
                if(mBroadcastManager!=null){
                    Intent intent=new Intent(BroadcastAction.ACTION_MCU_DOWNLOAD_PROGRESS);
                    intent.putExtra("progress",(int) (currentBytes*100/contentLength));
                    mBroadcastManager.sendBroadcast(intent);
                }

            }
        });
    }

    /***
     * app升级检测处理
     * @param data 升级信息
     * @param type 操作类型{@link #OPERATE_AUTO,#OPERATE_MANUAL}
     * @param result 操作类型{@link #RESULT_SUCCESS,#RESULT_FAIL}
     */
    private void appCheckProgress(AppUpgradeMsg data,int type,int result){
            if(result==RESULT_SUCCESS){
                if(data!=null&&data.versionCode>ApkUtil.getVersionCode()){
                    if(type==OPERATE_MANUAL){//人工检测更新，发消息给ui弹出提示
                        if(mBroadcastManager!=null){
                            log.myE("getAppUpgradeInfo","appCheckProgress - ACTION_APP_UPGRADE_CHECK");
                            Intent intent=new Intent(BroadcastAction.ACTION_APP_UPGRADE_CHECK);
                            intent.putExtra("need_upgrade","yes");
                            intent.putExtra("data",data);
                            mContext.sendBroadcast(intent);
                            mBroadcastManager.sendBroadcast(intent);
                        }
                    }else {//后台检测自动升级
                        upgradeApp(data,type);
                    }
                }else {
                    if(type==OPERATE_MANUAL){
                        if(mBroadcastManager!=null) {
                            Intent intent = new Intent(BroadcastAction.ACTION_APP_UPGRADE_CHECK);
                            intent.putExtra("need_upgrade", "no");
                            mBroadcastManager.sendBroadcast(intent);
                        }
                    }
                }
            }else {
                if(type==OPERATE_MANUAL){
                    if(mBroadcastManager!=null) {
                        Intent intent = new Intent(BroadcastAction.ACTION_APP_UPGRADE_CHECK);
                        intent.putExtra("need_upgrade", NeedUpgradeError);
                        mBroadcastManager.sendBroadcast(intent);
                    }
                }
            }
    }

    /***
     * app下载升级处理
     * @param path 安装路径
     * @param type 操作类型{@link #OPERATE_AUTO,#OPERATE_MANUAL}
     * @param result 操作类型{@link #RESULT_SUCCESS,#RESULT_FAIL}
     */
    private void appDownloadProgress(String path,int type,int result){
        if(result==RESULT_SUCCESS){
            if(type==OPERATE_MANUAL){
                if(mBroadcastManager!=null) {
                    Intent intent = new Intent(BroadcastAction.ACTION_APP_DOWNLOAD_FINISH);
                    intent.putExtra("result", "yes");
                    mBroadcastManager.sendBroadcast(intent);
                }
            }
            //开始升级
            log.d(TAG,"start ungrade app!path="+path);
            Intent mIntent = new Intent();
            mIntent.setAction("android.intent.action.SILENCE_INSTALL");
            mIntent.putExtra("apkPath", path);
            mContext.sendBroadcast(mIntent);

        }else {
            if(type==OPERATE_MANUAL){
                if(mBroadcastManager!=null) {
                    Intent intent = new Intent(BroadcastAction.ACTION_APP_DOWNLOAD_FINISH);
                    intent.putExtra("result", "no");
                    mBroadcastManager.sendBroadcast(intent);
                }
            }
        }
    }


    /***
     * 系统升级检测处理
     * @param data 升级信息
     * @param type 操作类型{@link #OPERATE_AUTO,#OPERATE_MANUAL}
     * @param result 操作类型{@link #RESULT_SUCCESS,#RESULT_FAIL}
     */
    private void systemCheckProgress(SystemUpgradeMsg data,int type,int result){
        if(result==RESULT_SUCCESS){
            //屏蔽系统升级
//            if(data!=null&&data.versionCode> PhoneUtil.getSystemVersionCode()){
//                if(type==OPERATE_MANUAL){//人工检测更新，发消息给ui弹出提示
//                    if(mBroadcastManager!=null){
//                        Intent intent=new Intent(BroadcastAction.ACTION_SYSTEM_UPGRADE_CHECK);
//                        intent.putExtra("need_upgrade","yes");
//                        intent.putExtra("data",data);
//                        mBroadcastManager.sendBroadcast(intent);
//                    }
//                }else {//后台检测自动升级
//                    upgradeSystem(data,type);
//                }
//            }else {
                if(type==OPERATE_MANUAL){
                    if(mBroadcastManager!=null) {
                        Intent intent = new Intent(BroadcastAction.ACTION_SYSTEM_UPGRADE_CHECK);
                        intent.putExtra("need_upgrade", "no");
                        mBroadcastManager.sendBroadcast(intent);
                    }
                }
            //}
        }else {
            if(type==OPERATE_MANUAL){
                if(mBroadcastManager!=null) {
                    Intent intent = new Intent(BroadcastAction.ACTION_SYSTEM_UPGRADE_CHECK);
                    intent.putExtra("need_upgrade", NeedUpgradeError);
                    mBroadcastManager.sendBroadcast(intent);
                }
            }
        }
    }

    /***
     * 系统下载升级处理
     * @param path 安装路径
     *@param channel  安装方式 {@link #Upgrade_Channel_Rom,#Upgrade_Channel_Diff}
     * @param type 操作类型{@link #OPERATE_AUTO,#OPERATE_MANUAL}
     * @param result 操作类型{@link #RESULT_SUCCESS,#RESULT_FAIL}
     */
    private void systemDownloadProgress(String path,String channel,int type,int result){
        if(result==RESULT_SUCCESS){
            if(type==OPERATE_MANUAL){
                if(mBroadcastManager!=null) {
                    Intent intent = new Intent(BroadcastAction.ACTION_SYSTEM_DOWNLOAD_FINISH);
                    intent.putExtra("result", "yes");
                    mBroadcastManager.sendBroadcast(intent);
                }
            }
            //开始升级
            path=path.replace("/storage/emulated/0","/sdcard");
            Log.e(TAG,"start ungrade system!channel="+channel);
            log.d(TAG,"upgrade path="+path);
            if(channel!=null&&channel.equals(Upgrade_Channel_Rom)){
                //ROM包
//                Intent intent2 = new Intent();
//                intent2.setAction("com.gmt.fridge.action.dameon.updaterom");
//                intent2.putExtra("romfile", path);
//                mContext.sendBroadcast(intent2);
                log.d(TAG,"upgrade rom");
                Intent intent2 = new Intent();
                intent2.setAction("com.gmt.fridge.action.dameon.updaterom");
                intent2.putExtra("romfile", path);
                mContext.sendBroadcast(intent2);
            }else {
                //差分包
                Intent intent1 = new Intent();
                intent1.setAction("com.gmt.fridge.action.dameon.updatefile");
                intent1.putExtra("zipfile", path);
                mContext.sendBroadcast(intent1);
            }

        }else {
            if(type==OPERATE_MANUAL){
                if(mBroadcastManager!=null) {
                    Intent intent = new Intent(BroadcastAction.ACTION_SYSTEM_DOWNLOAD_FINISH);
                    intent.putExtra("result", "no");
                    mBroadcastManager.sendBroadcast(intent);
                }
            }
        }
    }


    /***
     * mcu升级检测处理
     * @param data 升级信息
     * @param type 操作类型{@link #OPERATE_AUTO,#OPERATE_MANUAL}
     * @param result 操作类型{@link #RESULT_SUCCESS,#RESULT_FAIL}
     */
    private void mcuCheckProgress(AppUpgradeMsg data,int type,int result){
        if(result==RESULT_SUCCESS){
            if(data!=null&&data.versionCode>ControlManager.getInstance().getDataReceiveInfo().version){
                if(type==OPERATE_MANUAL){//人工检测更新，发消息给ui弹出提示
                    if(mBroadcastManager!=null){
                        Intent intent=new Intent(BroadcastAction.ACTION_MCU_UPGRADE_CHECK);
                        intent.putExtra("need_upgrade","yes");
                        intent.putExtra("data",data);
                        mBroadcastManager.sendBroadcast(intent);
                    }
                }else {//后台检测自动升级
                    upgradeMcu(data,type);
                }
            }else {
                if(type==OPERATE_MANUAL){
                    if(mBroadcastManager!=null) {
                        Intent intent = new Intent(BroadcastAction.ACTION_MCU_UPGRADE_CHECK);
                        intent.putExtra("need_upgrade", "no");
                        mBroadcastManager.sendBroadcast(intent);
                    }
                }
            }
        }else {
            if(type==OPERATE_MANUAL){
                if(mBroadcastManager!=null) {
                    Intent intent = new Intent(BroadcastAction.ACTION_MCU_UPGRADE_CHECK);
                    intent.putExtra("need_upgrade", NeedUpgradeError);
                    mBroadcastManager.sendBroadcast(intent);
                }
            }
        }
    }

    /***
     * mcu下载升级处理
     * @param path 安装路径
     * @param type 操作类型{@link #OPERATE_AUTO,#OPERATE_MANUAL}
     * @param result 操作类型{@link #RESULT_SUCCESS,#RESULT_FAIL}
     */
    private void mcuDownloadProgress(String path,int type,int result){
        if(result==RESULT_SUCCESS){
            if(type==OPERATE_MANUAL){
                if(mBroadcastManager!=null) {
                    Intent intent = new Intent(BroadcastAction.ACTION_MCU_DOWNLOAD_FINISH);
                    intent.putExtra("result", "yes");
                    mBroadcastManager.sendBroadcast(intent);
                }
            }
            //开始升级
            mMcuUpgradeProgress.progress=0;
            boolean upgradeResult=ControlManager.getInstance().upgradeFirmware(path,new ProgressCallback() {
                @Override
                public void onSuccess(Object data) {
                }

                @Override
                public void onFail(int errorCode, String msg) {
                }

                @Override
                public void onProgress(int progress) {
                    mMcuUpgradeProgress.progress=progress;
                    if(mBroadcastManager!=null){
                        Intent intent=new Intent(BroadcastAction.ACTION_MCU_UPGRADE_PROGRESS);
                        intent.putExtra("progress",progress);
                        mBroadcastManager.sendBroadcast(intent);
                    }
                }
            });
            if(upgradeResult){
                if(mBroadcastManager!=null) {
                    Intent intent = new Intent(BroadcastAction.ACTION_MCU_UPGRADE_FINISH);
                    intent.putExtra("result", "yes");
                    mBroadcastManager.sendBroadcast(intent);
                }
            }else {
                if(mBroadcastManager!=null) {
                    Intent intent = new Intent(BroadcastAction.ACTION_MCU_UPGRADE_FINISH);
                    intent.putExtra("result", "no");
                    mBroadcastManager.sendBroadcast(intent);
                }
            }
            mMcuUpgradeProgress.isUpgrading=false;
        }else {
            if(type==OPERATE_MANUAL){
                if(mBroadcastManager!=null) {
                    Intent intent = new Intent(BroadcastAction.ACTION_MCU_DOWNLOAD_FINISH);
                    intent.putExtra("result", "no");
                    mBroadcastManager.sendBroadcast(intent);
                }
            }
        }
    }
}
