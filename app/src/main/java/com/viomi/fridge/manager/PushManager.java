package com.viomi.fridge.manager;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.icu.text.SimpleDateFormat;
import android.os.Process;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.umeng.message.IUmengRegisterCallback;
import com.umeng.message.MsgConstant;
import com.umeng.message.PushAgent;
import com.umeng.message.UTrack;
import com.umeng.message.UmengMessageHandler;
import com.umeng.message.common.UmengMessageDeviceConfig;
import com.umeng.message.common.inter.ITagManager;
import com.umeng.message.entity.UMessage;
import com.umeng.message.tag.TagManager;
import com.viomi.common.module.okhttp.OkHttpClientManager;
import com.viomi.common.util.GsonUtil;
import com.viomi.fridge.R;
import com.viomi.fridge.api.http.HttpConnect;
import com.viomi.fridge.broadcast.BroadcastAction;
import com.viomi.fridge.common.GlobalParams;
import com.viomi.fridge.common.ViomiApplication;
import com.viomi.fridge.device.AppConfig;
import com.viomi.fridge.device.DeviceConfig;
import com.viomi.fridge.model.bean.PushMsg;
import com.viomi.fridge.model.bean.PushSetInfo;
import com.viomi.fridge.model.bean.UmengInfo;
import com.viomi.fridge.model.bean.ViomiUser;
import com.viomi.fridge.util.PhoneUtil;
import com.viomi.fridge.util.log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

import okhttp3.Call;

/**
 * Created by young2 on 2016/7/13.
 */
public class PushManager {
    private static  final String TAG="PushManager";
    private static PushManager INSTANCE=null;
    private  Context mConText=ViomiApplication.getContext();
    private PushAgent mPushAgent;

    public static PushManager getInstance(){
        synchronized (PushManager.class){
            if(INSTANCE==null){
                synchronized (PushManager.class){
                    if(INSTANCE==null){
                        INSTANCE=new PushManager();
                    }
                }
            }
        }
        return INSTANCE;
    }

    public  void init(Context context){
         mPushAgent = PushAgent.getInstance(context);
          appInfo(context);
         UmengMessageHandler messageHandler = new UmengMessageHandler() {
            /**
             * 自定义消息的回调方法
             * */
            @Override
            public void dealWithCustomMessage(final Context context, final UMessage msg) {
                Log.e(TAG,"dealWithCustomMessage="+msg.getRaw().toString());
            }

            /**
             * 自定义通知栏样式的回调方法
             * */
            @Override
            public Notification getNotification(Context context, UMessage msg) {
                log.d(TAG,"getNotification="+msg.getRaw().toString());
                try {
                    if(msg.getRaw().toString()!=null){
                        UmengInfo umengInfo= GsonUtil.parseJsonWithGson(msg.getRaw().toString(), UmengInfo.class);
                        pushDataPeocess(umengInfo);
                    }
                }catch (Exception e){
                    Log.e(TAG,"push receive paser error!msg="+e.getMessage());
                    e.printStackTrace();
                }
                return super.getNotification(context, msg);
            }

        };
        mPushAgent.setMessageHandler(messageHandler);
    }

    private void pushDataPeocess(UmengInfo umengInfo){
        if(umengInfo==null||umengInfo.body==null){
            Log.e(TAG,"umengInfo null");
            return;
        }
        PushMsg pushMsg=new PushMsg();
        pushMsg.title=umengInfo.body.title;
        pushMsg.content=umengInfo.body.text;
        if(umengInfo.body.custom!=null){
            JSONObject jsonObject= null;
            try {
                jsonObject = new JSONObject(umengInfo.body.custom);
                pushMsg.type=jsonObject.getString("type");
                JSONObject data=jsonObject.getJSONObject("data");
                pushMsg.link=data.optString("link");
            } catch (JSONException e) {
                Log.e(TAG,"paser custom error!custom="+umengInfo.body.custom+",errmsg="+e.getMessage());
                e.printStackTrace();
            }
        }

        pushNotificationToStatusBar(pushMsg);

    }

    private void appInfo(Context context) {
        String pkgName = ViomiApplication.getContext().getPackageName();
        String info = String.format("DeviceToken:%s\n" + "SdkVersion:%s\nAppVersionCode:%s\nAppVersionName:%s",
                mPushAgent.getRegistrationId(), MsgConstant.SDK_VERSION,
                UmengMessageDeviceConfig.getAppVersionCode(context), UmengMessageDeviceConfig.getAppVersionName(context));
        log.d(TAG,"应用包名:" + pkgName + "\n" + info);
    }


    /***
     * 用户推送设置处理
     */
    public void userPushSet(){
        DeviceConfig.MODEL=PhoneUtil.getDeviceModel();
        if(AppConfig.VIOMI_FRIDGE_V31.equals(DeviceConfig.MODEL)){
            return;
        }
        PushSetInfo pushSetInfo=PushSetManager.getPushSetInfo();
        boolean  flag=pushSetInfo.isUserEnable();
       ViomiUser viomiUser=AccountManager.getViomiUser(mConText);

        if(viomiUser!=null&&flag){
            String alias="fridge_"+ PhoneUtil.getMiIdentify().did+"_yid_"+viomiUser.getAccount();
            Log.d(TAG,"setAlias:"+alias);
            setAlias("YID",alias);
            pushSetInfo.setUserAlias(alias);
            PushSetManager.savePushSetInfo(pushSetInfo);
            HttpConnect.reportAlias(alias, new OkHttpClientManager.ResultCallback<String>() {
                @Override
                public void onError(Call call, Exception e) {
                    Log.e(TAG,"reportUserAlias,fail,msg="+e.getMessage());
                }
                @Override
                public void onResponse(String response) {
                    log.d(TAG,"reportUserAlias,response="+response);
                }
            });
        }else {
            String alias= pushSetInfo.getUserAlias();
            Log.d(TAG,"unsetAlias:"+alias);
            if(alias!=null){
                unSetAlias("YID",alias);
            }
        }
    }

    /***
     * 单台设备升级推送
     */
    public void singleUpgradePushSet(){
        String alias= PhoneUtil.getMiIdentify().did;
        setAlias("DID",alias);
    }

    /***
     * 广告推送设置处理
     */
    public  void advertPushSet(){
        DeviceConfig.MODEL=PhoneUtil.getDeviceModel();
        if(AppConfig.VIOMI_FRIDGE_V31.equals(DeviceConfig.MODEL)){
            return;
        }
        String topic_relase="viomi_activities";
        String topic_test="test_viomi_activities";
        boolean debug=GlobalParams.HTTP_DEBUG && GlobalParams.getInstance().isVmallHttpDebug();

        PushSetInfo pushSetInfo=PushSetManager.getPushSetInfo();
        boolean  flag=pushSetInfo.isAdvertEnable();
        if(debug){
            unSetTag(topic_relase);
            String topic=topic_test;
            if(flag){
                setTag(topic);
            }else {
                unSetTag(topic);
            }
        }else {
            unSetTag(topic_test);
            String topic=topic_relase;
            if(flag){
                setTag(topic);
            }else {
                unSetTag(topic);
            }
        }
    }

    /***
     * 所有设备升级推送
     */
    public  void upgradePushSet(){
        String topic_relase="viomi_device";
        String topic_test="test_viomi_device";
        if(GlobalParams.HTTP_DEBUG &&GlobalParams.getInstance().isVmallHttpDebug()){
            unSetTag(topic_relase);
            setTag(topic_test);
        }else {
            unSetTag(topic_test);
            setTag(topic_relase);
        }
    }

    private void setTag(String tag){
        Log.d(TAG,"setTag:"+tag);
        mPushAgent.getTagManager().add(new TagManager.TCallBack() {
            @Override
            public void onMessage(final boolean isSuccess, final ITagManager.Result result) {
                logResult("setTag",isSuccess,result);
            }
        },tag);
    }

    private void unSetTag(String tag){
        Log.d(TAG,"unSetTag:"+tag);
        mPushAgent.getTagManager().delete(new TagManager.TCallBack() {
            @Override
            public void onMessage(final boolean isSuccess, final ITagManager.Result result) {
                logResult("unSetTag",isSuccess,result);
            }
        }, tag);
    }

    private void logResult(String method,final boolean isSuccess, final ITagManager.Result result){
        if (isSuccess){
            log.d(TAG,method+" success");
        }else {
            String errorStr="";
            if(result!=null){
                errorStr=result.jsonString;
            }
            Log.e(TAG,method+"fail!msg="+errorStr);
        }

    }

    private void setAlias(String type,String alias){
        Log.d(TAG,"setAlias:"+"type="+type+",alias="+alias);
        mPushAgent.addExclusiveAlias(alias,type,
                new UTrack.ICallBack() {
                    @Override
                    public void onMessage(boolean isSuccess, String message) {
                        if (isSuccess){
                            log.d(TAG,"setAlias success");
                        }else {
                            Log.e(TAG,"setAlias fail!msg="+message);
                        }
                    }
                });
    }

    private void unSetAlias(String type,String alias){
        Log.d(TAG,"unSetAlias:"+"type="+type+",alias="+alias);
        mPushAgent.removeAlias(alias,type,
                new UTrack.ICallBack() {
                    @Override
                    public void onMessage(boolean isSuccess, String message) {
                        if (isSuccess){
                            log.d(TAG,"unSetAlias success");
                        }else {
                            Log.e(TAG,"unSetAlias fail!msg="+message);
                        }
                    }
                });
    }

    /***
     * 设置接收MiPush服务推送的时段，不在该时段的推送消息会被缓存起来，到了合适的时段再向app推送原先被缓存的消息。
     */
    public void setAcceptTime(){
        boolean enable;
        int startHour;
        int endHour;

        PushSetInfo pushSetInfo=PushSetManager.getPushSetInfo();
        enable=pushSetInfo.isPushBlockEnable();
        startHour=pushSetInfo.getPushBlockTimeStart();
        endHour=pushSetInfo.getPushBlockTimeEnd();
        log.d(TAG,"enable="+enable+",startHour="+startHour+",endHour="+endHour);
        if(enable){
            mPushAgent.setNoDisturbMode(startHour, 0, endHour, 0);
        }else {
            mPushAgent.setNoDisturbMode(0, 0, 0, 0);
        }
    }

    /***
     * 是否开启促销，本地判断
     * @return
     */
    public boolean isAdvertNotifyEnable(){
        PushSetInfo pushSetInfo= PushSetManager.getPushSetInfo();
        return pushSetInfo.isAdvertEnable();
    }

    /***
     * 是否开启设备，本地判断
     * @return
     */
    public boolean isDeviceNotifyEnable(){
        PushSetInfo pushSetInfo= PushSetManager.getPushSetInfo();
        return pushSetInfo.isDeviceEnable();
    }

    /***
     * 是否在接受时间内，本地判断
     * @return
     */
    public boolean isInAcceptTime(){
        boolean enable;
        int startHour;
        int endHour;
        PushSetInfo pushSetInfo=PushSetManager.getPushSetInfo();
        enable=pushSetInfo.isPushBlockEnable();
        startHour=pushSetInfo.getPushBlockTimeStart();
        endHour=pushSetInfo.getPushBlockTimeEnd();
        if(!enable){
            return true;
        }
       java.text.SimpleDateFormat simpleDateFormat = new java.text.SimpleDateFormat("HH");
        int curHour=0;
        try {
             curHour=Integer.parseInt(simpleDateFormat.format(new Date()));
        }catch (Exception e){
            e.printStackTrace();
        }
        if(startHour<endHour){
            if(curHour>=startHour&&curHour<endHour){
                return false;
            }else {
                return true;
            }
        }else if(startHour>endHour){
            if(curHour>=startHour||curHour<endHour){
                return false;
            }else {
                return true;
            }
        }else {
            if(curHour==startHour){
                return false;
            }else {
                return true;
            }
        }
    }

    /****
     * 推送通知到状态栏
     * @param pushMsg 推送数据
     */
    public void pushNotificationToStatusBar(PushMsg pushMsg){
        if(pushMsg==null){
            return;
        }
        Log.d(TAG,"push message ,type="+pushMsg.type);
        DeviceConfig.MODEL=PhoneUtil.getDeviceModel();
        if(AppConfig.VIOMI_FRIDGE_V31.equals(DeviceConfig.MODEL)){
            if((pushMsg.type!=PushMsg.PUSH_MESSAGE_TYPE_ABNORMAL)&&(pushMsg.type!=PushMsg.PUSH_MESSAGE_TYPE_FILTER)
                    &&(pushMsg.type!=PushMsg.PUSH_MESSAGE_TYPE_FOOD))
            {
                return;
            }
        }

        Intent intent=new Intent(BroadcastAction.ACTION_PUSH_MESSAGE);
        intent.putExtra(BroadcastAction.EXTRA_PUSH_MSG,pushMsg);
        LocalBroadcastManager.getInstance(ViomiApplication.getContext()).sendBroadcast(intent);
    }




    public void close(){
        INSTANCE=null;
    }

}
