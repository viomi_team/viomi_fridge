package com.viomi.fridge.manager;

import com.v2.clsdk.ErrorDef;
import com.v2.clsdk.esd.GetCameraListResult;
import com.v2.clsdk.model.CameraInfo;
import com.viomi.fridge.util.SDKInstance;
import com.viomi.fridge.util.log;

import java.util.ArrayList;

/**
 * IPC 管理
 * Created by William on 2017/11/9.
 */

public class CameraListManager {

    private static final String TAG = CameraListManager.class.getSimpleName();

    private static CameraListManager mInstance;
    private ArrayList<CameraInfo> mCameraList;
    private ArrayList<ICameraListListener> mCameraListListenerList;

    public static synchronized CameraListManager getInstance() {
        if (mInstance == null) {
            mInstance = new CameraListManager();
        }
        return mInstance;
    }

    private CameraListManager() {
        mCameraList = new ArrayList<>();
    }

    public synchronized ArrayList<CameraInfo> getCameraList() {
        return mCameraList;
    }

    /**
     * 从服务器获取摄像头设备列表
     */
    public int getCameraListFromServer() {
        log.d(TAG, "start get camera list");
        GetCameraListResult result = SDKInstance.getInstance().getCameraList();

        synchronized (CameraListManager.this) {
            if (result.getCode() == ErrorDef.SUCCESS && result.getCameraList() != null) {   // 成功返回
                log.d(TAG, result.getCameraList().size() + "");
                for (CameraInfo cameraInfo : result.getCameraList()) {
                    log.d(TAG, cameraInfo.toString());
                    CameraInfo existCameraInfo = getCameraInfo(cameraInfo.getSrcId());
                    if (existCameraInfo != null) {
                        existCameraInfo.parse(cameraInfo);
                    } else {
                        mCameraList.add(cameraInfo);
                    }
                }
                // 过期的设备
                ArrayList<CameraInfo> overdueCameraList = new ArrayList<>();
                for (CameraInfo cameraInfo : mCameraList) {
                    boolean exist = false;
                    for (CameraInfo cameraInfo2 : result.getCameraList()) {
                        if (cameraInfo.getSrcId().equalsIgnoreCase(cameraInfo2.getSrcId())) {
                            exist = true;
                            break;
                        }
                    }
                    if (!exist) overdueCameraList.add(cameraInfo);
                }
                mCameraList.removeAll(overdueCameraList);
                notifyCameraListChanged();
            }
        }
        return result.getCode();
    }

    private synchronized void notifyCameraListChanged() {
        if (mCameraListListenerList != null) {
            for (ICameraListListener listener : mCameraListListenerList) {
                try {
                    if (listener != null) {
                        listener.onChanged(getCameraList());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public synchronized CameraInfo getCameraInfo(final String srcId) {
        if (mCameraList == null) {
            log.d(TAG, "getCameraInfo failed, list is null");
            return null;
        }
        for (CameraInfo cameraInfo : mCameraList) {
            if (cameraInfo.getSrcId().equalsIgnoreCase(srcId)) {
                return cameraInfo;
            }
        }
        return null;
    }

    interface ICameraListListener {
        /**
         * Invoked when camera list size changed or the content of camera list is changed.
         *
         * @param latestList The latest camera list after modified.
         */
        void onChanged(ArrayList<CameraInfo> latestList);
    }

}
