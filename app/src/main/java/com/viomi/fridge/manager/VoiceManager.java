package com.viomi.fridge.manager;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

import com.alibaba.fastjson.JSON;
import com.iflytek.aiui.AIUIAgent;
import com.iflytek.aiui.AIUIConstant;
import com.iflytek.aiui.AIUIEvent;
import com.iflytek.aiui.AIUIListener;
import com.iflytek.aiui.AIUIMessage;
import com.iflytek.cloud.ErrorCode;
import com.iflytek.cloud.GrammarListener;
import com.iflytek.cloud.InitListener;
import com.iflytek.cloud.RecognizerListener;
import com.iflytek.cloud.RecognizerResult;
import com.iflytek.cloud.Setting;
import com.iflytek.cloud.SpeechConstant;
import com.iflytek.cloud.SpeechError;
import com.iflytek.cloud.SpeechEvent;
import com.iflytek.cloud.SpeechRecognizer;
import com.iflytek.cloud.SpeechSynthesizer;
import com.iflytek.cloud.SynthesizerListener;
import com.iflytek.cloud.TextUnderstander;
import com.iflytek.cloud.TextUnderstanderListener;
import com.iflytek.cloud.UnderstanderResult;
import com.iflytek.cloud.VoiceWakeuper;
import com.iflytek.cloud.WakeuperListener;
import com.iflytek.cloud.WakeuperResult;
import com.iflytek.cloud.util.ResourceUtil;
import com.miot.api.MiotManager;
import com.viomi.common.callback.AppCallback;
import com.viomi.common.util.GsonUtil;
import com.viomi.fridge.R;
import com.viomi.fridge.albumhttp.AlbumActivity;
import com.viomi.fridge.api.http.HttpConnect;
import com.viomi.fridge.broadcast.BroadcastAction;
import com.viomi.fridge.common.GlobalParams;
import com.viomi.fridge.common.ViomiApplication;
import com.viomi.fridge.common_api.TimerApi;
import com.viomi.fridge.device.AppConfig;
import com.viomi.fridge.device.DeviceConfig;
import com.viomi.fridge.model.bean.LocalRecogInfo;
import com.viomi.fridge.model.bean.LocalRecogVar;
import com.viomi.fridge.model.bean.SerialInfo;
import com.viomi.fridge.model.bean.TCRoomScene;
import com.viomi.fridge.model.bean.TalkInfo;
import com.viomi.fridge.model.bean.recipe.RecipeModel;
import com.viomi.fridge.model.bean.xunfei.XFAiuiCusTomResp;
import com.viomi.fridge.model.bean.xunfei.XFAnswerResp;
import com.viomi.fridge.model.bean.xunfei.XFFridgeResp;
import com.viomi.fridge.model.bean.xunfei.XFJoKeResp;
import com.viomi.fridge.model.bean.xunfei.XFMenuResp;
import com.viomi.fridge.model.bean.xunfei.XFAudioResp;
import com.viomi.fridge.model.bean.xunfei.XFNewsResp;
import com.viomi.fridge.model.bean.xunfei.XFStoryResp;
import com.viomi.fridge.model.bean.xunfei.XFTranslationResp;
import com.viomi.fridge.model.bean.xunfei.XFWeatherResp;
import com.viomi.fridge.model.bean.xunfei.XunFeiResp;
import com.viomi.fridge.model.enumcode.XunfeiError;
import com.viomi.fridge.service.BackgroudService;
import com.viomi.fridge.util.ApkUtil;
import com.viomi.fridge.util.FileUtil;
import com.viomi.fridge.util.NetWorkUtils;
import com.viomi.fridge.util.ToastUtil;
import com.viomi.fridge.util.ToolUtil;
import com.viomi.fridge.util.UMUtil;
import com.viomi.fridge.util.WakeAndLock;
import com.viomi.fridge.util.log;
import com.viomi.fridge.view.activity.CommonHeaderActivity;
import com.viomi.fridge.view.activity.ConnectUnionActivity;
import com.viomi.fridge.view.activity.MainNewV2Activity;
import com.viomi.fridge.view.activity.MenuShowActivity;
import com.viomi.fridge.view.activity.MusicPlayActivity;
import com.viomi.fridge.view.activity.RecipesDetailsActivity;
import com.viomi.fridge.view.activity.RecipesHealthyActivity;
import com.viomi.fridge.view.activity.ScreenSaverActivity;
import com.viomi.fridge.view.activity.WeatherActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by young2 on 2017/2/15.
 */
public class VoiceManager {
    private final static String TAG = VoiceManager.class.getSimpleName();
    public boolean mIsBothWay = false;//双向语音
    private static final String SERVICE_WEATHER = "weather";//天气
    private static final String SERVICE_FRIDGE = "freezer_smartHome";//冰箱
    private static final String SERVICE_OPENQA = "openQA";//问答
    private static final String SERVICE_CHAT = "chat";//聊天
    private static final String SERVICE_DATETIME = "datetime";//节日
    private static final String SERVICE_CALC = "calc";//计算
    private static final String SERVICE_FAQ = "faq";//faq
    private static final String SERVICE_BAIKE = "baike";//faq
    private static final String SERVICE_PEOTRY = "poetry";//诗词
    private static final String SERVICE_STORY = "story";//故事
    private static final String SERVICE_JOKE = "joke";//笑话
    private static final String SERVICE_NEWS = "news";//新闻
    private static final String SERVICE_MUSIC = "musicX";//音乐
    private static final String SERVICE_COOKBOOK = "cookbook";//菜谱
    private static final String SERVICE_TRANSLATION = "translation";//翻译
    private static final String SERVICE_ENGLISHEVERYDAY = "englishEveryday";//英语每日一句
    private static final String SERVICE_WORDFINDING = "wordFinding";//同反义词
    private static final String SERVICE_CONSTELLATION = "constellation";//星座运程
    private static final String SERVICE_DREAM = "dream";//周公解梦
    private static final String SERVICE_IDIOM = "idiom";//成语出处
    private static final String SERVICE_HOlIDAY = "holiday";//放假安排
    private static final String SERVICE_CHINESEZODIAC = "chineseZodiac";//生肖
    private static final String SERVICE_DRAMA = "drama";//戏曲
    private static final String SERVICE_CROSSTALK = "crossTalk";//小品
    private static final String SERVICE_HEALTH = "health";//健康知识
    private static final String SERVICE_STORYTELLING = "storyTelling";//评书
    private static final String SERVICE_HISTORY = "history";//历史
    private static final String SERVICE_FUNNY = "LEIQIAO.funnyPassage";//搞笑段子
    private static final String SERVICE_FOREX = "AIUI.forex";//汇率
    private static final String SERVICE_OPENCLASS = "LEIQIAO.openClass";//公开课
    private static final String SERVICE_GUESSNUMBER = "AIUI.guessNumber";//猜数字游戏
    private static final String SERVICE_MORAGAME = "LEIQIAO.moraGame";//石头剪刀布游戏
    private static final String SERVICE_RELATIONSHIP = "LEIQIAO.relationShip";//亲戚关系
    private static final String SERVICE_MEASURE = "PHOTO.measure";//单位换算

    //自定义技能
    private static final String SERVICE_VIDEO = "VIOMI.video";//电影
    private static final String SERVICE_COMMON = "VIOMI.common";//通用
    private static final String SERVICE_ROOM_OPERATE = "VIOMI.room_operate";//间室操作
    private static final String SERVICE_TIMER = "VIOMI.timer";//定时器技能
    private static final String SERVICE_SMART_HOME = "VIOMI.smart_home";//智能互联

    private String mVioceCity = "";//语音天气预报
    private Context mContext;
    private Handler mHandler;
    private static VoiceManager INSTANCE;
    private SpeechRecognizer mAsr;// 语音识别对象
    private SpeechSynthesizer mTts;    // 语音合成对象
    public static String voicerPeople = "xiaoyan";    // 默认云端发音人

    private boolean mIsListenning = false;
    private int mAIUIState = AIUIConstant.STATE_IDLE;
    //aiui对象
    private AIUIAgent mAIUIAgent = null;

    // 语义理解对象（文本到语义）。
    private TextUnderstander mTextUnderstander;
    // 引擎类型
    private String mEngineType = SpeechConstant.TYPE_LOCAL;
    // 本地语法文件
    private String mLocalGrammar = null;
    // 本地语法构建路径
    private String grmPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/msc/test";
    private final static int MAX_WAIT_TIME = 1200;//最长无语音输入等待时间，过后关闭语音聆听，单位秒
    private int mWakpupTime = MAX_WAIT_TIME;//唤醒的时间
    private Timer mTimer;
    private TimerTask mTimeTask;
    private boolean mIsWaking;//语音唤醒中
    private boolean isSpeaking;//是否合成中
    private boolean mIgnoreLister;//忽略监听标志，语音说话和播放音乐时时
    private AppCallback<String> mWeatherCallback;
    private AppCallback<String> mDeviceSetCallback;
    private int mOperateType;
    //语音操作类型
    private final static int OPERATE_TYPE_ENABLE_SMART = 0;
    private final static int OPERATE_TYPE_ENABLE_HOLIDAY = 1;
    private final static int OPERATE_TYPE_ENABLE_CLEAN = 2;
    private final static int OPERATE_TYPE_OPEAN_CCROOM = 3;
    private final static int OPERATE_TYPE_CLOSE_CCROOM = 4;
    private final static int OPERATE_TYPE_OPEN_TCROOM = 5;
    private final static int OPERATE_TYPE_CLOSE_TCROOM = 6;
    private final static int OPERATE_TYPE_SCENE_SET = 7;
    private final static int OPERATE_TYPE_CCROOM_TEMP_SET = 8;
    private final static int OPERATE_TYPE_TCROOM_TEMP_SET = 9;
    private final static int OPERATE_TYPE_FZROOM_TEMP_SET = 10;

    private String ASK_SUCCESS_STR = "";//操作成功后回复
    private String ASK_FAIL_STR = "";//操作失败后回复
    private String mOldScene = "";//切换前的场景，失败后恢复

    private int mVoiceNetworkFailTime = 0;//语音访问失败次数

    AudioManager mAudioManager = (AudioManager) ViomiApplication.getContext().getSystemService(Context.AUDIO_SERVICE);
    private WakeAndLock wakeAndLock = new WakeAndLock();


    public static VoiceManager getInstance() {
        synchronized (VoiceManager.class) {
            if (INSTANCE == null) {
                synchronized (ControlManager.class) {
                    if (INSTANCE == null) {
                        INSTANCE = new VoiceManager();
                    }
                }
            }
        }
        return INSTANCE;
    }

    public void init(Context context, Handler handler) {

        if (DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V3) || DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V31)
                || DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V4)) {
            mIsBothWay = true;
        }
        mHandler = handler;
        mContext = context;
        try {
            wakeupInit(context);
            ttsInit(context);
            understanderInit();
            //  asrInit(context);
            aiuiAgentInit();
        } catch (Exception e) {
            Log.e(TAG, "init error");
        }
        Setting.setShowLog(true);
        setDeviceSetCallback();
    }

    public void close() {
        VoiceWakeuper mIvw = VoiceWakeuper.getWakeuper();
        if (mIvw != null) {
            mIvw.destroy();
        }
        if (mTts != null) {
            mTts.stopSpeaking();
            mTts.destroy();
        }
        if (mAsr != null) {
            mAsr.cancel();
            mAsr.destroy();
        }

        if (null != this.mAIUIAgent) {
            AIUIMessage stopMsg = new AIUIMessage(AIUIConstant.CMD_STOP, 0, 0, null, null);
            mAIUIAgent.sendMessage(stopMsg);

            this.mAIUIAgent.destroy();
            this.mAIUIAgent = null;
        }
    }

    public boolean isSpeaking() {
        return isSpeaking;
    }

    /***
     * 获取天气
     * @param City
     * @param callback
     */
    public void getWeather(String City, AppCallback<String> callback) {
        if (mTextUnderstander == null) {
            understanderInit();
        }
        mWeatherCallback = callback;
        if (mTextUnderstander.isUnderstanding()) {
            mTextUnderstander.cancel();
            Log.e(TAG, "取消");
            mWeatherCallback.onFail(-1, "isUnderstanding");
        } else {
            int ret = mTextUnderstander.understandText(City + "天气", mTextUnderstanderListener);
            if (ret != 0) {
                Log.e(TAG, "语义理解失败,错误码:" + ret);
                mWeatherCallback.onFail(-2, "understanding error!");
            }
        }
    }

    public void textUnderstandertTest(String text) {
        int ret = mTextUnderstander.understandText(text, mTextUnderstanderListener);
        if (ret != 0) {
            Log.e(TAG, "语义理解失败,错误码:" + ret);
        }
    }

    public void weatherProcess(String text) {
        XFWeatherResp weatherResp = GsonUtil.parseJsonWithGson(text, XFWeatherResp.class);
        String report = getWeatherReport(weatherResp);
        log.d(TAG, "report:" + report);
        if (report != null) {
            Intent intent = new Intent(mContext, WeatherActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mContext.startActivity(intent);
            startSpeak(report);
        }
    }


    private void understanderInit() {
        mTextUnderstander = TextUnderstander.createTextUnderstander(mContext, textUnderstanderListener);
        mTextUnderstander.setParameter(SpeechConstant.NLP_VERSION, "3.0");//通过此参数，设置开放语义协议版本号。
    }


    /***
     * 语音唤醒初始化
     */
    private void wakeupInit(Context context) {
        //1.创建VoiceWakeuper对象
        VoiceWakeuper mIvw = VoiceWakeuper.createWakeuper(context, null);
        if (mIvw == null) {
            Log.e(TAG, "Speech wakeup fail!");
            return;
        }
        //2.加载唤醒词资源，resPath为唤醒资源路径
        String resPath = ResourceUtil.generateResourcePath(ViomiApplication.getContext(), ResourceUtil.RESOURCE_TYPE.assets, "ivw/58a40571.jet");
        mIvw.setParameter(SpeechConstant.IVW_RES_PATH, resPath);

        //3.设置唤醒参数
        //唤醒门限值，根据资源携带的唤醒词个数按照“id:门限;id:门限”的格式传入
        mIvw.setParameter(SpeechConstant.IVW_THRESHOLD, "0:10;1:10;2:10");
        //设置当前业务类型为唤醒
        mIvw.setParameter(SpeechConstant.IVW_SST, "wakeup");
        //设置唤醒一直保持，直到调用stopListening，传入0则完成一次唤醒后，会话立即结束（默认0）
        mIvw.setParameter(SpeechConstant.KEEP_ALIVE, "1");
        //4.开始唤醒
        mIvw.startListening(mWakeuperListener);
    }

    private String getAIUIParams() {
        String params = "";
        AssetManager assetManager = mContext.getResources().getAssets();
        try {
            InputStream ins = assetManager.open("cfg/aiui_phone.cfg");
            byte[] buffer = new byte[ins.available()];

            ins.read(buffer);
            ins.close();
            params = new String(buffer);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return params;
    }

    private boolean aiuiAgentInit() {
        if (null == mAIUIAgent) {
            Log.i(TAG, "create aiui agent");
            mAIUIAgent = AIUIAgent.createAgent(mContext, getAIUIParams(), mAIUIListener);
            AIUIMessage startMsg = new AIUIMessage(AIUIConstant.CMD_START, 0, 0, null, null);
            mAIUIAgent.sendMessage(startMsg);
        }

        if (null == mAIUIAgent) {
            final String strErrorTip = "创建 AIUI Agent 失败！";
            Log.e(TAG, strErrorTip);
        }
        return null != mAIUIAgent;
    }

    /***
     * 语音识别
     */
    private void asrInit(Context context) {
        // 初始化识别对象
        mAsr = SpeechRecognizer.createRecognizer(context, new InitListener() {
            @Override
            public void onInit(int code) {
                Log.d(TAG, "SpeechRecognizer init() code = " + code);
                if (code != ErrorCode.SUCCESS) {
                    Log.e(TAG, "SpeechRecognizer init() code = " + code);
                }
            }
        });
        // 初始化语法、命令词
        mLocalGrammar = FileUtil.readAssetsFile(context, "fridge.bnf", "utf-8");
        buildGrammar(context);
        setMixParam(context);
    }

    /**
     * 构建离线识别的语法
     */
    private void buildGrammar(Context context) {
        String content = new String(mLocalGrammar);
        mAsr.setParameter(SpeechConstant.PARAMS, null);
        // 设置文本编码格式
        mAsr.setParameter(SpeechConstant.TEXT_ENCODING, "utf-8");
        // 设置引擎类型
        mAsr.setParameter(SpeechConstant.ENGINE_TYPE, SpeechConstant.TYPE_LOCAL);
        // 设置语法构建路径
        mAsr.setParameter(ResourceUtil.GRM_BUILD_PATH, grmPath);
        // 设置资源路径
        mAsr.setParameter(ResourceUtil.ASR_RES_PATH, getResourcePath(context));

        int ret = mAsr.buildGrammar("bnf", content, new GrammarListener() {
            @Override
            public void onBuildFinish(String grammarId, SpeechError error) {
                if (error == null) {
                    Log.d(TAG, "语法构建成功：" + grammarId);
                } else {
                    Log.e(TAG, "语法构建失败,错误码：" + error.getErrorCode());
                }
            }
        });
        if (ret != ErrorCode.SUCCESS) {
            Log.e(TAG, "语法构建失败,错误码：" + ret);
        }
    }

    /**
     * 混合模式参数设置
     *
     * @return
     */
    private boolean setMixParam(Context context) {
        // 先清空之前的参数
        mAsr.setParameter(SpeechConstant.PARAMS, null);

        /***************************以下是开启混合模式的设置********************************/
        mAsr.setParameter(SpeechConstant.ENGINE_TYPE, "mix");
        mAsr.setParameter("asr_sch", "1");//是否进行语义识别
        mAsr.setParameter(SpeechConstant.NLP_VERSION, "3.0");//通过此参数，设置开放语义协议版本号。
        mAsr.setParameter(SpeechConstant.RESULT_TYPE, "json");//返回文本结果类型
        /**********************************************************************************/

        /*****************以下是控制本地结果和在线结果的优先级的*****************************/
        mAsr.setParameter("mixed_type", "realtime");//混合模式的类型
        mAsr.setParameter(SpeechConstant.MIXED_TIMEOUT, "4000");//在线结果超时控制. 0-30000, def:2000
        mAsr.setParameter(SpeechConstant.MIXED_THRESHOLD, "30");//离线结果混合门限. 0-100, def:60
        mAsr.setParameter(SpeechConstant.ASR_THRESHOLD, "10");//离线结果识别门限0-100, default:0
        mAsr.setParameter("local_prior", "1");

//        // 设置语音前端点:静音超时时间，即用户多长时间不说话则当做超时处理
//        mSpeechUnderstander.setParameter(SpeechConstant.VAD_ENABLE,"1");
//        mSpeechUnderstander.setParameter(SpeechConstant.VAD_BOS,"10000");//最大10s
//        // 设置语音后端点:后端点静音检测时间，即用户停止说话多长时间内即认为不再输入， 自动停止录音
//        mSpeechUnderstander.setParameter(SpeechConstant.VAD_EOS, "1000");//最大10s
        /*****************具体的解释可以查阅一下《混合识别策略文档》**************************/

        // 设置本地识别资源
        mAsr.setParameter(ResourceUtil.ASR_RES_PATH, getResourcePath(context));
        // 设置语法构建路径
        mAsr.setParameter(ResourceUtil.GRM_BUILD_PATH, grmPath);
        // 设置返回结果格式
        mAsr.setParameter(SpeechConstant.RESULT_TYPE, "json");
        // 设置本地识别使用语法id
        mAsr.setParameter(SpeechConstant.LOCAL_GRAMMAR, "fridge");
        return true;
    }


    //获取识别资源路径
    private String getResourcePath(Context context) {
        StringBuffer tempBuffer = new StringBuffer();
        //识别通用资源
        tempBuffer.append(ResourceUtil.generateResourcePath(context, ResourceUtil.RESOURCE_TYPE.assets, "asr/common.jet"));
        return tempBuffer.toString();
    }

    /***
     * 语音合成初始化
     */
    private void ttsInit(Context context) {
        // 初始化合成对象
        mTts = SpeechSynthesizer.createSynthesizer(context, new InitListener() {
            @Override
            public void onInit(int code) {
                Log.d(TAG, "TtsInitListener init() code = " + code);
                if (code != ErrorCode.SUCCESS) {
                    Log.e(TAG, "TtsInitListener init() fail !");
                }
            }
        });

        // 清空参数
        mTts.setParameter(SpeechConstant.PARAMS, null);
        //设置合成
        if (mEngineType.equals(SpeechConstant.TYPE_CLOUD)) {
            //设置使用云端引擎
            mTts.setParameter(SpeechConstant.ENGINE_TYPE, SpeechConstant.TYPE_CLOUD);
            //设置发音人
            mTts.setParameter(SpeechConstant.VOICE_NAME, voicerPeople);
        } else {
            //设置使用本地引擎
            mTts.setParameter(SpeechConstant.ENGINE_TYPE, SpeechConstant.TYPE_LOCAL);
            //设置发音人资源路径
            mTts.setParameter(ResourceUtil.TTS_RES_PATH, getResourcePath());
            //设置发音人
            mTts.setParameter(SpeechConstant.VOICE_NAME, voicerPeople);
        }
        //设置合成情感
//        mTts.setParameter(SpeechConstant.EMOT, "neutral");//neutral, happy, sad, angry
        //设置合成语速
        mTts.setParameter(SpeechConstant.SPEED, "50");
        //设置合成音调
        mTts.setParameter(SpeechConstant.PITCH, "50");
        //设置合成音量
        mTts.setParameter(SpeechConstant.VOLUME, "50");
        //设置播放器音频流类型
        mTts.setParameter(SpeechConstant.STREAM_TYPE, "3");

        // 设置播放合成音频打断音乐播放，默认为true
        mTts.setParameter(SpeechConstant.KEY_REQUEST_FOCUS, "true");

        // 设置音频保存路径，保存音频格式支持pcm、wav，设置路径为sd卡请注意WRITE_EXTERNAL_STORAGE权限
        // 注：AUDIO_FORMAT参数语记需要更新版本才能生效
        mTts.setParameter(SpeechConstant.AUDIO_FORMAT, "wav");
//        mTts.setParameter(SpeechConstant.TTS_AUDIO_PATH, Environment.getExternalStorageDirectory()+"/msc11/tts.wav");
//        log.d(TAG,"@@@@@@@@@@@@@@@="+Environment.getExternalStorageDirectory()+"/msc11/tts.wav");
    }

    //获取发音人资源路径
    private String getResourcePath() {
        StringBuffer tempBuffer = new StringBuffer();
        //合成通用资源
        tempBuffer.append(ResourceUtil.generateResourcePath(mContext, ResourceUtil.RESOURCE_TYPE.assets, "tts/common.jet"));
        tempBuffer.append(";");
        //发音人资源
        tempBuffer.append(ResourceUtil.generateResourcePath(mContext, ResourceUtil.RESOURCE_TYPE.assets, "tts/" + voicerPeople + ".jet"));
        return tempBuffer.toString();
    }


    /***
     * 语音唤醒listener
     */
    private WakeuperListener mWakeuperListener = new WakeuperListener() {
        public void onResult(WakeuperResult result) {
            log.d(TAG, "wakeup onResult=" + result.getResultString());

            if (!mIsBothWay) {
                if (isSpeaking) {
                    return;
                }
                if (mAudioManager.isMusicActive()) {
                    Log.e(TAG, "isMusicActive");
                    return;
                }
            }

            if (!GlobalParams.getInstance().isVoiceEnabe()) {
                Log.i(TAG, "isVoiceEnabe false");
                return;
            }
            //{"bos":20030,"id":0,"sst":"wakeup","score":35,"eos":21100}
            try {
                String text = result.getResultString();
                JSONObject object = new JSONObject(text);
                int id = object.optInt("id");
                int score = object.optInt("score");
                if (id >= 0) {
                    if (score >= 10) {
                        wakeupProcess();
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            VoiceWakeuper.getWakeuper().startListening(mWakeuperListener);
        }

        public void onError(SpeechError error) {
            Log.e(TAG, "wakeup onError=" + error.getPlainDescription(true));
            VoiceWakeuper.getWakeuper().startListening(mWakeuperListener);
        }

        public void onBeginOfSpeech() {
        }

        public void onEvent(int eventType, int arg1, int arg2, Bundle obj) {
            if (SpeechEvent.EVENT_IVW_RESULT == eventType) {
                //当使用唤醒+识别功能时获取识别结果
                //arg1:是否最后一个结果，1:是，0:否。
                RecognizerResult reslut = ((RecognizerResult) obj.get(SpeechEvent.KEY_EVENT_IVW_RESULT));
            }
        }

        @Override
        public void onVolumeChanged(int i) {

        }
    };

    /***
     * 唤醒后处理
     */
    public void wakeupProcess() {
        mVoiceNetworkFailTime = 0;
        WakeAndLock wakeAndLock = new WakeAndLock();
        if (!wakeAndLock.isScreenOn()) {
            wakeAndLock.screenOn();
        }
        wakeAndLock = null;

        String text = "";
        //   VoiceWakeuper.getWakeuper().stopListening();
        if (!mIsWaking) {
            int index = getTime();
            if (index == 0) {
                text = mContext.getResources().getString(R.string.text_wakeup_ask_morning);
            } else if (index == 1) {
                text = mContext.getResources().getString(R.string.text_wakeup_ask_noon);
            } else if (index == 2) {
                text = mContext.getResources().getString(R.string.text_wakeup_ask_afternoon);
            } else if (index == 3) {
                text = mContext.getResources().getString(R.string.text_wakeup_ask_night);
            } else {
                text = mContext.getResources().getString(R.string.text_wakeup_ask_repeat);
            }
        } else {
            text = mContext.getResources().getString(R.string.text_wakeup_ask_repeat);
        }
        if (!NetWorkUtils.isNetWorkEnable(mContext)) {
            text += "!\n连上网络后，我才能愉快的和您交流哦！";
        }
        startWakeupSpeak(text);
        boolean flag = GlobalParams.getInstance().getVoiceNerverTips();
        log.d(TAG, "getVoiceNerverTips=" + flag);

        resetWakeupTime();
        startListenTimer();
    }

    private AIUIListener mAIUIListener = new AIUIListener() {

        @Override
        public void onEvent(AIUIEvent aiuiEvent) {
            switch (aiuiEvent.eventType) {
                case AIUIConstant.EVENT_WAKEUP:
                    Log.i(TAG, "进入识别状态，on event: " + aiuiEvent.eventType);
                    // showTip( "进入识别状态" );
                    break;

                case AIUIConstant.EVENT_RESULT: {
                    mVoiceNetworkFailTime = 0;
                    Log.i(TAG, "收到结果，on event: " + aiuiEvent.eventType);
                    log.d(TAG, "on result -----: " + aiuiEvent.info);
                    try {
                        JSONObject bizParamJson = new JSONObject(aiuiEvent.info);
                        JSONObject data = bizParamJson.getJSONArray("data").getJSONObject(0);
                        JSONObject params = data.getJSONObject("params");
                        JSONObject content = data.getJSONArray("content").getJSONObject(0);

                        if (content.has("cnt_id")) {
                            String cnt_id = content.getString("cnt_id");
                            JSONObject cntJson = new JSONObject(new String(aiuiEvent.data.getByteArray(cnt_id), "utf-8"));

                            log.d(TAG, "on result: " + cntJson.toString());
                            String sub = params.optString("sub");
                            String resultStr = cntJson.optString("intent");
                            if ("nlp".equals(sub) && resultStr != null && resultStr.length() != 0 && (!resultStr.equals("{}"))) {
                                // 解析得到语义结果
                                log.d(TAG, "on result npl:" + resultStr);
                                if (!GlobalParams.getInstance().isVoiceEnabe()) {
                                    return;
                                }
                                if (!mIsBothWay) {
                                    if (isSpeaking) {
                                        Log.e(TAG, "isSpeaking");
                                        return;
                                    }
                                    if (mAudioManager.isMusicActive() || mIgnoreLister) {
                                        Log.e(TAG, "isMusicActive");
                                        return;
                                    }
                                }

                                if (resultStr != null) {
                                    log.d(TAG, "on result mRecognizerListener:" + resultStr);
                                    String resultText = parserResult(resultStr);
                                    if (resultText != null) {
                                        WakeAndLock wakeAndLock = new WakeAndLock();
                                        if (!wakeAndLock.isScreenOn()) {
                                            wakeAndLock.screenOn();
                                        }
                                        wakeAndLock = null;
                                        return;
                                    }
                                } else {
                                    Log.e(TAG, "on result mRecognizerListener null!");
                                }
                            }
                        }
                    } catch (Throwable e) {
                        e.printStackTrace();
                        Log.e(TAG, "AIUIConstant.EVENT_RESULT：" + e.getLocalizedMessage());
                    }

                }
                break;

                case AIUIConstant.EVENT_ERROR: {
                    Log.i(TAG, "on event: " + aiuiEvent.eventType);
                    Log.e(TAG, "aiui错误: " + aiuiEvent.arg1 + "\n" + aiuiEvent.info);
                    mIsListenning = false;
                    if (aiuiEvent.arg1 == 10120) {
                        mVoiceNetworkFailTime++;
                        if (mVoiceNetworkFailTime <= 5) {
                            ToastUtil.show("语音网络访问失败，请重试");
                        }
                    }

                }
                break;

                case AIUIConstant.EVENT_VAD: {
                    if (AIUIConstant.VAD_BOS == aiuiEvent.arg1) {
                        Log.i(TAG, "找到vad_bos");
                    } else if (AIUIConstant.VAD_EOS == aiuiEvent.arg1) {
                        Log.i(TAG, "找到vad_eos");
                    } else {
                        Log.i(TAG, "EVENT_VAD：" + aiuiEvent.arg2);
                    }
                }
                break;

                case AIUIConstant.EVENT_START_RECORD: {
                    Log.i(TAG, "on event: " + aiuiEvent.eventType);
                    Log.i(TAG, "开始录音: ");
                    mIsListenning = true;
                }
                break;

                case AIUIConstant.EVENT_STOP_RECORD: {
                    Log.i(TAG, "on event: " + aiuiEvent.eventType);
                    Log.i(TAG, "停止录音");
                    mIsListenning = false;
                    //         startVoiceNlp();
                }
                break;

                case AIUIConstant.EVENT_STATE: {    // 状态事件
                    mAIUIState = aiuiEvent.arg1;
                    Log.i(TAG, "mAIUIState: " + mAIUIState);
                    if (AIUIConstant.STATE_IDLE == mAIUIState) {
                        // 闲置状态，AIUI未开启
                        Log.i(TAG, "闲置状态，AIUI未开启");
                    } else if (AIUIConstant.STATE_READY == mAIUIState) {
                        // AIUI已就绪，等待唤醒
                        Log.i(TAG, " AIUI已就绪，等待唤醒");
                    } else if (AIUIConstant.STATE_WORKING == mAIUIState) {
                        // AIUI工作中，可进行交互
                        Log.i(TAG, " AIUI工作中，可进行交互");
                    }
                }
                break;

                case AIUIConstant.EVENT_CMD_RETURN: {
                    if (AIUIConstant.CMD_UPLOAD_LEXICON == aiuiEvent.arg1) {
                        Log.i(TAG, "上传" + (0 == aiuiEvent.arg2 ? "成功" : "失败"));
                    }
                }
                break;

                default:
                    break;
            }
        }

    };


    private void startVoiceNlp() {
        if (mAIUIAgent == null) {
            return;
        }
        Log.i(TAG, "start voice nlp,state=" + AIUIConstant.STATE_WORKING);
        // 先发送唤醒消息，改变AIUI内部状态，只有唤醒状态才能接收语音输入
        if (AIUIConstant.STATE_WORKING != this.mAIUIState) {
            AIUIMessage wakeupMsg = new AIUIMessage(AIUIConstant.CMD_WAKEUP, 0, 0, "", null);
            mAIUIAgent.sendMessage(wakeupMsg);
        }

        // 打开AIUI内部录音机，开始录音
        String params = "sample_rate=16000,data_type=audio";
        AIUIMessage writeMsg = new AIUIMessage(AIUIConstant.CMD_START_RECORD, 0, 0, params, null);
        mAIUIAgent.sendMessage(writeMsg);
        mIsListenning = true;
    }

    private void stopVoiceNlp() {
        mIsListenning = false;
        if (mAIUIAgent == null) {
            return;
        }
        Log.i(TAG, "stop voice nlp");
        // 停止录音
        String params = "sample_rate=16000,data_type=audio";
        AIUIMessage stopWriteMsg = new AIUIMessage(AIUIConstant.CMD_STOP_RECORD, 0, 0, params, null);
        mAIUIAgent.sendMessage(stopWriteMsg);
    }

    private void resetVoiceNlp() {
        mIsListenning = false;
        if (mAIUIAgent == null) {
            return;
        }
        Log.i(TAG, "stop voice nlp");
        // 停止录音
        String params = "sample_rate=16000,data_type=audio";
        AIUIMessage stopWriteMsg = new AIUIMessage(AIUIConstant.CMD_RESET, 0, 0, params, null);
        mAIUIAgent.sendMessage(stopWriteMsg);
    }


    /**
     * 初始化监听器（文本到语义）。
     */
    private InitListener textUnderstanderListener = new InitListener() {

        @Override
        public void onInit(int code) {
            Log.d(TAG, "textUnderstanderListener init() code = " + code);
            if (code != ErrorCode.SUCCESS) {
                Log.e(TAG, "初始化失败,错误码：" + code);
            }
        }
    };

    private TextUnderstanderListener mTextUnderstanderListener = new TextUnderstanderListener() {
        @Override
        public void onResult(UnderstanderResult understanderResult) {
            if (understanderResult == null) {
                mWeatherCallback.onFail(-3, "understanderResult null!");
                return;
            }
            mWeatherCallback.onSuccess(understanderResult.getResultString());
            log.d(TAG, "understanderResult = " + understanderResult.getResultString());
            //    XFWeatherResp weatherResp= GsonUtil.parseJsonWithGson(understanderResult.getResultString(), XFWeatherResp.class);
        }

        @Override
        public void onError(SpeechError speechError) {
            mWeatherCallback.onFail(speechError.getErrorCode(), speechError.getErrorDescription());
        }
    };

    /***
     * 开始录音计时，解析语义
     */
    private void startAsrListener() {
        stopAsrListener();
        int ret = mAsr.startListening(mRecognizerListener);
        if (ret != ErrorCode.SUCCESS) {
            Log.e(TAG, "识别失败,错误码: " + ret);
        } else {
            log.d(TAG, "startAsrListener ");
        }
    }

    /***
     * 停止录音
     */
    private void stopAsrListener() {

        if (mAsr != null & mAsr.isListening()) {
            Log.e(TAG, "stopListener");
            mAsr.stopListening();
        }
    }

    private void stopListenTimer() {
        if (mTimer != null) {
            mTimer.cancel();
            mTimer = null;
        }
        if (mTimeTask != null) {
            mTimeTask.cancel();
            mTimeTask = null;
        }
    }

    /***
     * 语音识别计时
     */
    private void startListenTimer() {
        stopListenTimer();
        // stopVoiceNlp();
        resetVoiceNlp();
        resetWakeupTime();
        mTimer = new Timer();
        mTimeTask = new TimerTask() {
            @Override
            public void run() {
                mWakpupTime--;
                if (mWakpupTime <= 0) {
                    stopListenning();

                } else {
                    if (mWakpupTime <= 600) {
                        if (mHandler != null && mWakpupTime % 10 == 0) {//关闭聊天dialog倒数
                            Message message = mHandler.obtainMessage();
                            message.what = BackgroudService.WHAT_ABOUT_TO_CLOSE;
                            message.obj = mWakpupTime / 10;
                            mHandler.sendMessage(message);
                        }
                    }

                    if (mIsBothWay) {
                        if (!mIsListenning) {//没开始监听的话，开启
                            startVoiceNlp();
                        }
                    } else {
                        if (!isSpeaking && (!mAudioManager.isMusicActive())) {//没有合成和播放音乐时
                            if (!mIsListenning) {//没开始监听的话，开启
                                startVoiceNlp();
                            }
                        } else {
                            if (mIsListenning) {
                                stopVoiceNlp();
                            }
                        }
                    }
                }
            }
        };
        mTimer.schedule(mTimeTask, 1000, 100);
        mIsWaking = true;
    }

    /***
     * 停止语义监听
     */
    private void stopListenning() {
        resetWakeupTime();
        mIsWaking = false;
        stopListenTimer();
        stopVoiceNlp();
        if (mHandler != null) {
            Message message = mHandler.obtainMessage();
            message.what = BackgroudService.WHAT_ABOUT_TO_CLOSE;
            message.obj = 0;
            mHandler.sendMessage(message);
        }
    }

    /***
     * 复位唤醒时间，有语音说话，重新计时
     */
    private void resetWakeupTime() {
        mWakpupTime = MAX_WAIT_TIME;
    }

    /***
     * 识别监听
     */
    private RecognizerListener mRecognizerListener = new RecognizerListener() {
        @Override
        public void onVolumeChanged(int i, byte[] bytes) {

        }

        @Override
        public void onBeginOfSpeech() {
            log.d(TAG, "onBeginOfSpeech,isSpeaking=" + isSpeaking + ",music=" + mAudioManager.isMusicActive());
            mIgnoreLister = false;
            if (isSpeaking || mAudioManager.isMusicActive()) {
                mIgnoreLister = true;
            }
        }

        @Override
        public void onEndOfSpeech() {

        }

        @Override
        public void onResult(RecognizerResult recognizerResult, boolean b) {
            log.d(TAG, "onResult");
            if (!GlobalParams.getInstance().isVoiceEnabe()) {
                return;
            }
            if (!mIsBothWay) {
                if (isSpeaking) {
                    return;
                }
                if (mAudioManager.isMusicActive() || mIgnoreLister) {
                    Log.e(TAG, "isMusicActive");
                    return;
                }
            }

            if (recognizerResult != null) {
                Log.d(TAG, "mRecognizerListener result=" + recognizerResult.getResultString());
                String resultText = parserResult(recognizerResult.getResultString());
                if (resultText != null) {
                    return;
                }
            } else {
                Log.e(TAG, "mRecognizerListener result null!");
            }

        }

        @Override
        public void onError(SpeechError speechError) {
            Log.e(TAG, "mRecognizerListener onError,msg=" + speechError.getPlainDescription(true));
            int code = speechError.getErrorCode();
            if (code == XunfeiError.MSP_ERROR_NO_DATA || code == XunfeiError.ERROR_NO_MATCH || code == XunfeiError.ERROR_NETWORK_TIMEOUT) {
//                if(mIsWaking){
//                    startAsrListener();
//                }
            }

        }

        @Override
        public void onEvent(int i, int i1, int i2, Bundle bundle) {

        }
    };

    /***
     * 开始语音合成
     * @param text
     */
    public void startSpeak(String text) {
        log.d(TAG, "startSpeak:" + text);
        resetWakeupTime();
        sendTalkText(TalkInfo.TYPE_TALK, text, false);
        isSpeaking = true;
        if (mTts != null) {
            int code = mTts.startSpeaking(text, mTtsListener);
            if (code != ErrorCode.SUCCESS) {
                Log.e(TAG, "startSpeaking fail !");
                isSpeaking = false;
                //showTip("语音合成失败,错误码: " + code);
            }
        }
    }

    /***
     * 开始语音合成，唤醒后
     * @param text
     */
    public void startWakeupSpeak(String text) {
        log.d(TAG, "startSpeak:" + text);
        sendTalkText(TalkInfo.TYPE_TALK, text, true);
        isSpeaking = true;
        if (mTts != null) {
            int code = mTts.startSpeaking(text, mTtsListener);
            if (code != ErrorCode.SUCCESS) {
                Log.e(TAG, "startSpeaking fail !");
                isSpeaking = false;
                //showTip("语音合成失败,错误码: " + code);
            }
        }
    }

    /**
     * 合成回调监听。
     */
    private SynthesizerListener mTtsListener = new SynthesizerListener() {

        @Override
        public void onSpeakBegin() {
            isSpeaking = true;
            if (mHandler != null) {
                mHandler.sendEmptyMessage(BackgroudService.WHAT_TALK_DIALOG_TALKING);
            }
        }

        @Override
        public void onBufferProgress(int i, int i1, int i2, String s) {
        }

        @Override
        public void onSpeakPaused() {
        }

        @Override
        public void onSpeakResumed() {
        }

        @Override
        public void onSpeakProgress(int i, int i1, int i2) {
        }

        @Override
        public void onCompleted(SpeechError speechError) {
            log.d(TAG, "mTtsListener onCompleted");
            isSpeaking = false;
            if (mHandler != null) {
                mHandler.sendEmptyMessage(BackgroudService.WHAT_TALK_DIALOG_LISTENING);
            }
        }

        @Override
        public void onEvent(int i, int i1, int i2, Bundle bundle) {
        }
    };


    /***
     * 语义识别结果
     * @param text
     * @return 是否有合成语音
     */
    private String parserResult(String text) {
        if (text == null || TextUtils.isEmpty(text)) {
            return null;
        }

        XunFeiResp baseResp = GsonUtil.parseJsonWithGson(text, XunFeiResp.class);
        if (baseResp != null && baseResp.text != null) {//发送听到的字符串
            sendTalkText(TalkInfo.TYPE_LISTEN, baseResp.text, false);
        }

        XunFeiResp xunFeiResp = GsonUtil.parseJsonWithGson(text, XunFeiResp.class);
        if ((xunFeiResp != null) && (xunFeiResp.service != null)) {
            if (xunFeiResp.service.equals(SERVICE_WEATHER)) {//天气
                XFWeatherResp weatherResp = GsonUtil.parseJsonWithGson(text, XFWeatherResp.class);
                String report = getWeatherReport(weatherResp);
                log.d(TAG, "report:" + report);
                if (report != null) {
                    Intent intent = new Intent(mContext, WeatherActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("city", mVioceCity);
                    mContext.startActivity(intent);
                    startSpeak(report);
                    removeTalkDialog();
                    return report;
                }
            } else if (xunFeiResp.service.equals(SERVICE_OPENQA) || xunFeiResp.service.equals(SERVICE_CHAT)
                    || xunFeiResp.service.equals(SERVICE_DATETIME) || xunFeiResp.service.equals(SERVICE_CALC)
                    || xunFeiResp.service.equals(SERVICE_FAQ) || xunFeiResp.service.equals(SERVICE_BAIKE)
                    || xunFeiResp.service.equals(SERVICE_PEOTRY) || xunFeiResp.service.equals(SERVICE_WORDFINDING)
                    || xunFeiResp.service.equals(SERVICE_CONSTELLATION) || xunFeiResp.service.equals(SERVICE_DREAM)
                    || xunFeiResp.service.equals(SERVICE_IDIOM) || xunFeiResp.service.equals(SERVICE_HOlIDAY)
                    || xunFeiResp.service.equals(SERVICE_CHINESEZODIAC) || xunFeiResp.service.equals(SERVICE_HEALTH)
                    || xunFeiResp.service.equals(SERVICE_FOREX) || xunFeiResp.service.equals(SERVICE_GUESSNUMBER)
                    || xunFeiResp.service.equals(SERVICE_MORAGAME) || xunFeiResp.service.equals(SERVICE_RELATIONSHIP)
                    || xunFeiResp.service.equals(SERVICE_MEASURE)) {//问答等

                XFAnswerResp xfAnswerResp = GsonUtil.parseJsonWithGson(text, XFAnswerResp.class);
                if (xfAnswerResp.answer != null) {
                    String answerText = xfAnswerResp.answer.text;
                    if (answerText != null) {
                        answerText = answerText.replace("[k3]", "").replace("[k0]", "");
                    }
                    startSpeak(answerText);
                    return answerText;
                }
            } else if ((xunFeiResp.service.equals(SERVICE_NEWS))) {//新闻
                XFNewsResp xfNewsResp = GsonUtil.parseJsonWithGson(text, XFNewsResp.class);
                if (xfNewsResp != null && xfNewsResp.answer != null && xfNewsResp.answer.text != null) {
                    startSpeak(xfNewsResp.answer.text);
                    try {
                        if (xfNewsResp.data.result.get(0).url != null) {
                            jumpMusicActivity(xfNewsResp.data.result.get(0).url, xfNewsResp.data.result.get(0).title,
                                    xfNewsResp.data.result.get(0).category);
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "xfNewsResp null");
                        e.printStackTrace();
                    }
                    return xfNewsResp.answer.text;
                }
            } else if ((xunFeiResp.service.equals(SERVICE_JOKE))) {//笑话
                XFJoKeResp xfJoKeResp = GsonUtil.parseJsonWithGson(text, XFJoKeResp.class);
                if (xfJoKeResp != null && xfJoKeResp.answer != null && xfJoKeResp.answer.text != null) {
                    startSpeak(xfJoKeResp.answer.text);
                    try {
                        if (xfJoKeResp.data.result.get(0).mp3Url != null) {
                            jumpMusicActivity(xfJoKeResp.data.result.get(0).mp3Url, xfJoKeResp.data.result.get(0).title,
                                    xfJoKeResp.data.result.get(0).category);
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "xfJoKeResp null");
                        e.printStackTrace();
                    }
                    return xfJoKeResp.answer.text;
                }
            } else if ((xunFeiResp.service.equals(SERVICE_STORY))) {//故事
                XFStoryResp xFStoryResp = GsonUtil.parseJsonWithGson(text, XFStoryResp.class);
                if (xFStoryResp != null && xFStoryResp.answer != null && xFStoryResp.answer.text != null) {
                    startSpeak(xFStoryResp.answer.text);
                    try {
                        if (xFStoryResp.data.result.get(0).playUrl != null) {
                            jumpMusicActivity(xFStoryResp.data.result.get(0).playUrl, xFStoryResp.data.result.get(0).name,
                                    xFStoryResp.data.result.get(0).category);
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "xfJoKeResp null");
                        e.printStackTrace();
                    }
                    return xFStoryResp.answer.text;
                }
            } else if (xunFeiResp.service.equals(SERVICE_STORYTELLING) || xunFeiResp.service.equals(SERVICE_CROSSTALK)
                    || xunFeiResp.service.equals(SERVICE_DRAMA) || xunFeiResp.service.equals(SERVICE_HISTORY)
                    || xunFeiResp.service.equals(SERVICE_FUNNY) || xunFeiResp.service.equals(SERVICE_OPENCLASS)) {
                //评书、历史、戏曲、相声小品
                XFAudioResp xfAudioResp = GsonUtil.parseJsonWithGson(text, XFAudioResp.class);
                if (xfAudioResp != null && xfAudioResp.answer != null && xfAudioResp.answer.text != null) {
                    startSpeak(xfAudioResp.answer.text);
                    try {
                        if (xfAudioResp.data.result.get(0).url != null) {
                            jumpMusicActivity(xfAudioResp.data.result.get(0).url, xfAudioResp.data.result.get(0).name,
                                    xfAudioResp.data.result.get(0).album);
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "xfAudioResp null");
                        e.printStackTrace();
                    }
                    return xfAudioResp.answer.text;
                }
            } else if ((xunFeiResp.service.equals(SERVICE_COOKBOOK))) {//菜谱
                XFMenuResp xfMenuResp = GsonUtil.parseJsonWithGson(text, XFMenuResp.class);
                if (xfMenuResp != null && xfMenuResp.answer != null && xfMenuResp.answer.text != null) {
                    XFMenuResp.Result result = xfMenuResp.data.result.get(0);
                    String json = JSON.toJSONString(result);
                    RecipeModel recipeModel = JSON.parseObject(json, RecipeModel.class);
                    if (recipeModel != null && ToolUtil.isRecipeExist(recipeModel)) {
                        startSpeak(xfMenuResp.answer.text);
//                        try {
//                            if (result != null) {
//                            Intent menuIntent=new Intent(mContext, MenuShowActivity.class);
//                            menuIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                            menuIntent.putExtra("title",result.title);
//                            menuIntent.putExtra("ingredient",result.ingredient);
//                            menuIntent.putExtra("accessory",result.accessory);
//                            menuIntent.putExtra("steps",result.steps);
//                            menuIntent.putExtra("imgUrl",result.imgUrl);
//                            mContext.startActivity(menuIntent);
                        Intent menuIntent = new Intent(mContext, RecipesDetailsActivity.class);
                        menuIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        menuIntent.putExtra("json", json);
                        mContext.startActivity(menuIntent);
//                            }
                        removeTalkDialog();
//                        } catch (Exception e) {
//                            Log.e(TAG, "xfMenuResp null");
//                            e.printStackTrace();
//                        }
                        return xfMenuResp.answer.text;
                    }
                }
            } else if (xunFeiResp.service.equals(SERVICE_COMMON)) {//自定义通用技能
                XFAiuiCusTomResp xfAiuiCusTomResp = GsonUtil.parseJsonWithGson(text, XFAiuiCusTomResp.class);
                try {
                    String intentString = xfAiuiCusTomResp.semantic.get(0).intent;
                    return commonServiceProcess(intentString);
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG, "SERVICE_COMMON xfAiuiCusTomResp null");
                }
                return null;
            } else if (xunFeiResp.service.equals(SERVICE_ROOM_OPERATE)) {//自定义技能，间室操作

                try {
                    XFAiuiCusTomResp xfAiuiCusTomResp = GsonUtil.parseJsonWithGson(text, XFAiuiCusTomResp.class);
                    return roomOperateServiceProcess(xfAiuiCusTomResp);
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG, "SERVICE_ROOM_OPERATE xfAiuiCusTomResp null");
                }
                return null;
            } else if (xunFeiResp.service.equals(SERVICE_TIMER)) {//自定义技能，定时器
                try {
                    XFAiuiCusTomResp xfAiuiCusTomResp = GsonUtil.parseJsonWithGson(text, XFAiuiCusTomResp.class);
                    return timerServiceProcess(xfAiuiCusTomResp);
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG, "SERVICE_TIMER xfAiuiCusTomResp null");
                }
                return null;
            } else if (xunFeiResp.service.equals(SERVICE_SMART_HOME)) {
                try {
                    XFAiuiCusTomResp xfAiuiCusTomResp = GsonUtil.parseJsonWithGson(text, XFAiuiCusTomResp.class);
                    return smarthomeServiceProcess(xfAiuiCusTomResp);
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG, "SERVICE_SMART_HOME xfAiuiCusTomResp null");
                }
                return null;

            } else if (xunFeiResp.service.equals(SERVICE_VIDEO)) {//电影
                XFAiuiCusTomResp xfAiuiCusTomResp = GsonUtil.parseJsonWithGson(text, XFAiuiCusTomResp.class);
                try {
                    String intentString = xfAiuiCusTomResp.semantic.get(0).intent;
                    if (intentString != null) {
                        if ("random".equals(intentString)) {
                            String answerText = "为您打开影音娱乐";
                            removeTalkDialog();
//                            Intent intent = new Intent(mContext, VideoUMActivity.class);
//                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                            mContext.startActivity(intent);
                            String packageName = "com.qiyi.video.pad";
                            if (ApkUtil.checkApkExist(ViomiApplication.getContext(), packageName)) {
                                Log.e(TAG, "startOtherApp,packageName1=" + packageName);
                                ApkUtil.startOtherApp(mContext, packageName, false);
                            }
                            startSpeak(answerText);
                            return answerText;
                        } else {
                            String videoName = xfAiuiCusTomResp.semantic.get(0).slots.get(0).value;
                            if (videoName != null) {
                                String answerText = "为您查找" + videoName;
                                removeTalkDialog();
//                                Intent intent=new Intent(BroadcastAction.ACTION_APP_DOWNLOAD_FINISH);
//                                ViomiApplication.getContext().sendBroadcast(intent);
                                UMUtil.onSearchVideoPageJump(mContext, videoName);

                                startSpeak(answerText);
                                return answerText;
                            }
                        }
                    }
                } catch (Exception e) {
                    Log.e(TAG, "xfAiuiCusTomResp null");
                    e.printStackTrace();
                }
                return null;
            } else if ((xunFeiResp.service.equals(SERVICE_TRANSLATION))) {//翻译
                XFTranslationResp xFTranslationResp = GsonUtil.parseJsonWithGson(text, XFTranslationResp.class);
                if (xFTranslationResp != null && xFTranslationResp.answer != null && xFTranslationResp.answer.text != null) {
                    String resultStr = xFTranslationResp.answer.text + "：\n" + xFTranslationResp.data.result.get(0).translated;
                    startSpeak(resultStr);
                    return resultStr;
                }
            } else if ((xunFeiResp.service.equals(SERVICE_ENGLISHEVERYDAY))) {//英语每日一句
                XFAnswerResp xfAnswerResp = GsonUtil.parseJsonWithGson(text, XFAnswerResp.class);
                if (xfAnswerResp.answer != null) {
                    String answerText = xfAnswerResp.answer.text;

                    startSpeak(answerText);
                    return answerText;
                }
            } else {
                sendTalkText(TalkInfo.TYPE_TALK, "我好像不太明白", false);
            }
        } else {
            String regResult = localRecognizerProcess(text);
            if (regResult == null) {
                sendTalkText(TalkInfo.TYPE_TALK, "我好像不太明白", false);
            }
            return regResult;
        }
        return null;
    }

    /***
     * 自定义通用技能处理
     * @param intentString
     * @return
     */
    private String commonServiceProcess(String intentString) {
        if (intentString == null) {
            return null;
        }
        String talkText = null;
        switch (intentString) {
            case "call_xiaoxian":
                WakeAndLock wakeAndLock = new WakeAndLock();
                if (!wakeAndLock.isScreenOn()) {
                    wakeAndLock.screenOn();
                }
                wakeAndLock = null;
                talkText = mContext.getResources().getString(R.string.text_wakeup_ask_repeat);
                startSpeak(talkText);
                return talkText;

            case "entry_kfc":
                if (GlobalParams.getInstance().isVoiceFirstKFC()) {
                    GlobalParams.getInstance().setVoiceFirstKFC(false);
                    talkText = "首次订餐，需要您先填写送餐地址哦。";
                } else {
                    talkText = "欢迎您来点餐!";
                }
                removeTalkDialog();
                UMUtil.onWebPageJump(mContext, "KFC", HttpConnect.kfc_url, false);
                startSpeak(talkText);
                return talkText;

            case "service_phone":
                talkText = "我们的售后电话是：4001002632";
                startSpeak(talkText);
                return talkText;

            case "photo_play":
                SharedPreferences sp = ViomiApplication.getContext().getSharedPreferences("screen_imgs", Context.MODE_PRIVATE);
                Set<String> screen_set = sp.getStringSet("screen_set", new HashSet<String>());
                if (screen_set == null || screen_set.size() == 0) {
                    talkText = "请先设置电子相册照片";
                    startSpeak(talkText);
                    removeTalkDialog();
                    Intent intentAlum = new Intent(mContext, AlbumActivity.class);
                    intentAlum.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(intentAlum);
                    return talkText;
                }
                talkText = "已为您打开电子相册";
                startSpeak(talkText);
                removeTalkDialog();
                Intent intent1 = new Intent(mContext, ScreenSaverActivity.class);
                intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent1);
                return talkText;

            case "photo_set":
                talkText = "已为您跳转电子相册设置页面";
                startSpeak(talkText);
                removeTalkDialog();
                Intent intenXiangce = new Intent(mContext, AlbumActivity.class);
                intenXiangce.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intenXiangce);
                return talkText;

            case "music_play":
                talkText = "已为您打开音乐";
                startSpeak(talkText);
                removeTalkDialog();
                String packageName = "com.tencent.qqmusicpad";
                if (ApkUtil.checkApkExist(ViomiApplication.getContext(), packageName)) {
                    Log.e(TAG, "startOtherApp,packageName=" + packageName);
                    ApkUtil.startOtherApp(mContext, packageName, false);
                }
                return talkText;

            case "smart_mode":
                if (ControlManager.getInstance().getDataSendInfo().mode == SerialInfo.MODE_SMART) {
                    talkText = "已经处于智能模式";
                } else {
                    mOperateType = OPERATE_TYPE_ENABLE_SMART;
                    boolean smart = ControlManager.getInstance().enableSmartMode(true);
                    if (smart) {
                        talkText = "已为您开启智能模式";
                    } else {
                        talkText = "开启智能模式失败";
                    }
                }
                startSpeak(talkText);
                return talkText;

            case "holiday_mode":
                if (ControlManager.getInstance().getDataSendInfo().mode == SerialInfo.MODE_HOLIDAY) {
                    talkText = "已经处于假日模式";
                    startSpeak(talkText);
                } else {
                    mOperateType = OPERATE_TYPE_ENABLE_HOLIDAY;
                    ASK_SUCCESS_STR = "已为您开启假日模式";
                    ASK_FAIL_STR = "开启假日模式失败";
                    boolean holiday = ControlManager.getInstance().enableHolidayMode(true, mDeviceSetCallback);
                    if (!holiday) {
                        talkText = "开启假日模式失败";
                        startSpeak(talkText);
                    }
                }
                return talkText;

            case "clean_cmd":
                if (DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V2) || DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V3)
                        || DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V31)) {
                    sendTalkText(TalkInfo.TYPE_TALK, "我好像不太明白", false);
                    return null;
                }
                if (ControlManager.getInstance().isOneKeyCleanRunning()) {
                    talkText = "已经在净化中";
                } else {
                    mOperateType = OPERATE_TYPE_ENABLE_CLEAN;
                    boolean smart = ControlManager.getInstance().enableOneKeyClean(true);
                    if (smart) {
                        talkText = "已为您开启一键净化";
                    } else {
                        talkText = "开启一键净化失败";
                    }
                }
                startSpeak(talkText);
                return talkText;

            case "answer_phone":
                if (AppConfig.VIOMI_FRIDGE_V1.equals(DeviceConfig.MODEL) || AppConfig.VIOMI_FRIDGE_V2.equals(DeviceConfig.MODEL)
                        || (!BleManager.getInstance().getBleAdapter().isEnabled())) {
                    sendTalkText(TalkInfo.TYPE_TALK, "我好像不太明白", false);
                    return null;
                }

                Intent intent = new Intent();
                intent.setAction("com.action.audiocall.accept");
                mContext.sendBroadcast(intent);
                stopListenning();
                return null;

            case "hangup_phone":
                Log.i(TAG, "hangup_phone");
                Intent intentReject = new Intent();
                intentReject.setAction("com.action.audiocall.reject");
                mContext.sendBroadcast(intentReject);
                talkText = "好的";
                startSpeak(talkText);
                return talkText;

            case "speak_stop":
                stopSpeak();
                talkText = "好的";
                return talkText;

            case "main_page":
                talkText = "好的";
                startSpeak(talkText);
                removeTalkDialog();
                Intent intent11 = new Intent(mContext, MainNewV2Activity.class);
                intent11.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent11.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                mContext.startActivity(intent11);
                return talkText;

//            case "open_door":
//                JSONObject jsonObject2 = new JSONObject();
//                try {
//                    jsonObject2.put("did", "79227662");
//                    log.d(TAG, "json=" + jsonObject2);
//                    OkHttpClientManager.postAsync("http://vj.viomi.com.cn/wlink/door/unlock", new OkHttpClientManager.ResultCallback<String>() {
//                        @Override
//                        public void onError(Call call, Exception e) {
//                            log.d(TAG, "error=" + e.getMessage());
//                        }
//
//                        @Override
//                        public void onResponse(String response) {
//
//                        }
//                    },jsonObject2);
//                }catch (JSONException e){
//
//                }
//                talkText="好的,已开门";
//                startSpeak(talkText);
//                return talkText;
//            case "open_light":
//
//                    JSONObject jsonObject = new JSONObject();
//                    try {
//                        jsonObject.put("did", "79227662");
//                        log.d(TAG, "json=" + jsonObject);
//                        OkHttpClientManager.postAsync("http://vj.viomi.com.cn/wlink/switch/turnOn", new OkHttpClientManager.ResultCallback<String>() {
//                            @Override
//                            public void onError(Call call, Exception e) {
//                                log.d(TAG, "error=" + e.getMessage());
//                            }
//
//                            @Override
//                            public void onResponse(String response) {
//
//                            }
//                        },jsonObject);
//                    }catch (JSONException e){
//
//                    }
//
//                talkText="好的，已开灯";
//                startSpeak(talkText);
//                return talkText;
//            case "close_light":
//                JSONObject jsonObject1 = new JSONObject();
//                try {
//                    jsonObject1.put("did", "79227662");
//                    log.d(TAG, "json=" + jsonObject1);
//                    OkHttpClientManager.postAsync("http://vj.viomi.com.cn/wlink/switch/turnOff", new OkHttpClientManager.ResultCallback<String>() {
//                        @Override
//                        public void onError(Call call, Exception e) {
//                            log.d(TAG, "error=" + e.getMessage());
//                        }
//
//                        @Override
//                        public void onResponse(String response) {
//
//                        }
//                    },jsonObject1);
//                }catch (JSONException e){
//
//                }
//                talkText="好的,已关灯";
//                startSpeak(talkText);
//                return talkText;

        }
        return null;
    }

    public void stopSpeak() {
        sendTalkText(TalkInfo.TYPE_TALK, "好的", false);
        isSpeaking = false;
        if (mTts != null) {
            mTts.stopSpeaking();
        }
        Intent intentStop = new Intent(BroadcastAction.ACTION_STOP_MUSIC);
        LocalBroadcastManager.getInstance(ViomiApplication.getContext()).sendBroadcast(intentStop);
    }

    /***
     * 间室操作
     * @param xfAiuiCusTomResp
     * @return
     */
    private String roomOperateServiceProcess(XFAiuiCusTomResp xfAiuiCusTomResp) {
        if (xfAiuiCusTomResp == null) {
            return null;
        }
        String talkText = null;
        boolean roomResult;
        int roomTemp;
        String intentString = xfAiuiCusTomResp.semantic.get(0).intent;
        if (intentString == null) {
            Log.e(TAG, "roomOperateServiceProcess intentString null");
            return null;
        }
        switch (intentString) {
            case "open_rc_room":
                log.d(TAG, "open_rc_room");
                boolean ccresult;
                if (ControlManager.getInstance().getDataSendInfo().cold_closet_room_enable) {
                    talkText = "冷藏室已经处于打开状态";
                    startSpeak(talkText);
                } else {
                    mOperateType = OPERATE_TYPE_OPEAN_CCROOM;
                    ASK_SUCCESS_STR = "已为您打开冷藏室";
                    ASK_FAIL_STR = "冷藏室打开失败";
                    roomResult = ControlManager.getInstance().enableRoom(true, SerialInfo.ROOM_COLD_COLSET, mDeviceSetCallback);
                    if (!roomResult) {
                        talkText = "冷藏室打开失败";
                        startSpeak(talkText);
                    }
                }
                return talkText;

            case "close_rc_room":
                log.d(TAG, "close_rc_room");
                if (!ControlManager.getInstance().getDataSendInfo().cold_closet_room_enable) {
                    talkText = "冷藏室已经处于关闭状态";
                    startSpeak(talkText);
                } else {
                    mOperateType = OPERATE_TYPE_CLOSE_CCROOM;
                    ASK_SUCCESS_STR = "已为您关闭冷藏室";
                    ASK_FAIL_STR = "冷藏室关闭失败";
                    roomResult = ControlManager.getInstance().enableRoom(false, SerialInfo.ROOM_COLD_COLSET, mDeviceSetCallback);
                    if (!roomResult) {
                        talkText = "冷藏室关闭失败";
                        startSpeak(talkText);
                    }
                }
                return talkText;

            case "open_cc_room":
                if (DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V3) || DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V31)) {
                    return null;
                }
                log.d(TAG, "open_cc_room");

                if (ControlManager.getInstance().getDataSendInfo().temp_changeable_room_room_enable) {
                    talkText = "变温室已经处于打开状态";
                    startSpeak(talkText);
                } else {
                    mOperateType = OPERATE_TYPE_OPEN_TCROOM;
                    ASK_SUCCESS_STR = "已为您打开变温室";
                    ASK_FAIL_STR = "变温室打开失败";
                    roomResult = ControlManager.getInstance().enableRoom(true, SerialInfo.ROOM_CHANGEABLE_ROOM, mDeviceSetCallback);
                    if (!roomResult) {
                        talkText = "变温室打开失败";
                        startSpeak(talkText);
                    }
                }
                return talkText;

            case "close_cc_room":
                if (DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V3) || DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V31)) {
                    return null;
                }
                log.d(TAG, "open_cc_room");
                if (!ControlManager.getInstance().getDataSendInfo().temp_changeable_room_room_enable) {
                    talkText = "变温室已经处于关闭状态";
                    startSpeak(talkText);
                } else {
                    mOperateType = OPERATE_TYPE_CLOSE_TCROOM;
                    ASK_SUCCESS_STR = "已为您关闭变温室";
                    ASK_FAIL_STR = "变温室关闭失败";
                    roomResult = ControlManager.getInstance().enableRoom(false, SerialInfo.ROOM_CHANGEABLE_ROOM, mDeviceSetCallback);
                    if (roomResult) {
                        mOldScene = GlobalParams.getInstance().getSceneChoose();
                        GlobalParams.getInstance().setSceneChoose("");
                    } else {
                        talkText = "变温室关闭失败";
                        startSpeak(talkText);
                    }
                }
                return talkText;

            case "cc_room_scene":
                if (DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V3) || DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V31)) {
                    return null;
                }
                String sceneValue = xfAiuiCusTomResp.semantic.get(0).slots.get(1).value;
                sceneValue = sceneValue.replace("区", "");

                log.d(TAG, "set room type=" + sceneValue);
                int temp = 100;
                ArrayList<TCRoomScene> sceneAllList = RoomSceneManager.getInstance().getSceneAllList();
                for (int i = 0; i < sceneAllList.size(); i++) {
                    if (sceneAllList.get(i).name.equals(sceneValue)) {
                        temp = sceneAllList.get(i).value;
                        break;
                    }
                }
                if (temp == 100) {
                    talkText = "抱歉，没有找到对应场景";
                    startSpeak(talkText);
                    return talkText;
                }

                mOperateType = OPERATE_TYPE_SCENE_SET;
                ASK_SUCCESS_STR = "已为您把变温室设为" + sceneValue + "区";
                ASK_FAIL_STR = "变温室设置失败";
                boolean sceneResult = ControlManager.getInstance().setRoomTemp(temp, SerialInfo.ROOM_CHANGEABLE_ROOM, mDeviceSetCallback);
                if (sceneResult) {
                    mOldScene = GlobalParams.getInstance().getSceneChoose();
                    GlobalParams.getInstance().setSceneChoose(sceneValue);
                } else {
                    talkText = "变温室设置失败";
                    startSpeak(talkText);
                }
                return talkText;

            case "set_rc_room":
                roomTemp = Integer.parseInt(xfAiuiCusTomResp.semantic.get(0).slots.get(1).normValue);

                if (roomTemp < 2) {
                    talkText = "冷藏室不能设置低于2度";
                    startSpeak(talkText);
                    return talkText;
                } else if (roomTemp > 8) {
                    talkText = "冷藏室不能设置高于8度";
                    startSpeak(talkText);
                    return talkText;
                }
                mOperateType = OPERATE_TYPE_CCROOM_TEMP_SET;

                ASK_SUCCESS_STR = "已为您把冷藏室设为" + roomTemp + "度";
                ASK_FAIL_STR = "抱歉，温度设置失败";
                roomResult = ControlManager.getInstance().setRoomTemp(roomTemp, SerialInfo.ROOM_COLD_COLSET, mDeviceSetCallback);
                if (roomResult) {
                } else {
                    talkText = "抱歉，温度设置失败";
                    startSpeak(talkText);
                }
                return talkText;

            case "set_cc_room":
                if (DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V3) || DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V31)) {
                    return null;
                }
                roomTemp = Integer.parseInt(xfAiuiCusTomResp.semantic.get(0).slots.get(1).normValue);
                String startStr = xfAiuiCusTomResp.semantic.get(0).slots.get(0).value;
                String endStr = xfAiuiCusTomResp.semantic.get(0).slots.get(1).value;
                log.d(TAG, "xfAiuiCusTomResp.text=" + ",startStr=" + startStr + ",endStr=" + endStr);
                String countStr = xfAiuiCusTomResp.text.substring(xfAiuiCusTomResp.text.indexOf(startStr) + startStr.length(), xfAiuiCusTomResp.text.indexOf(endStr));
                log.d(TAG, "countStr=" + countStr);
                if (countStr.contains("负") || countStr.contains("零下")) {
                    roomTemp = 0 - roomTemp;
                }
                if (roomTemp < -18) {
                    talkText = "变温室不能设置低于-18度";
                    startSpeak(talkText);
                    return talkText;
                }
                int maxTcTemp = 8;
                if (DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V2)) {
                    maxTcTemp = -3;
                }
                if (roomTemp > maxTcTemp) {
                    talkText = "变温室不能设置高于" + maxTcTemp + "度";
                    startSpeak(talkText);
                    return talkText;
                }
                mOperateType = OPERATE_TYPE_TCROOM_TEMP_SET;

                ASK_SUCCESS_STR = "已为您把变温室设为" + roomTemp + "度";
                ASK_FAIL_STR = "抱歉，温度设置失败";
                roomResult = ControlManager.getInstance().setRoomTemp(roomTemp, SerialInfo.ROOM_CHANGEABLE_ROOM, mDeviceSetCallback);
                if (roomResult) {
                    mOldScene = GlobalParams.getInstance().getSceneChoose();
                    GlobalParams.getInstance().setSceneChoose(GlobalParams.Value_Scene_ChooseName);
                } else {
                    talkText = "抱歉，温度设置失败";
                    startSpeak(talkText);
                }
                return talkText;

            case "set_fc_room":
                roomTemp = Integer.parseInt(xfAiuiCusTomResp.semantic.get(0).slots.get(1).normValue);
                String startStrfc = xfAiuiCusTomResp.semantic.get(0).slots.get(0).value;
                String endStrfc = xfAiuiCusTomResp.semantic.get(0).slots.get(1).value;
                log.d(TAG, "xfAiuiCusTomResp.text=" + ",startStr=" + startStrfc + ",endStr=" + endStrfc);
                String countStrfc = xfAiuiCusTomResp.text.substring(xfAiuiCusTomResp.text.indexOf(startStrfc) + startStrfc.length(), xfAiuiCusTomResp.text.indexOf(endStrfc));
                log.d(TAG, "countStr=" + countStrfc);
                if (countStrfc.contains("负") || countStrfc.contains("零下")) {
                    roomTemp = 0 - roomTemp;
                }
                int minfzTemp = -25;
                if (DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V2) || DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V3)
                        || DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V31)) {
                    minfzTemp = -24;
                }
                if (roomTemp < minfzTemp) {
                    talkText = "冷冻室不能设置低于" + minfzTemp + "度";
                    startSpeak(talkText);
                    return talkText;
                }
                int maxfzTemp = -15;
                if (DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V2) || DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V3)
                        || DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V31)) {
                    maxfzTemp = -16;
                }
                if (roomTemp > maxfzTemp) {
                    talkText = "冷冻室不能设置高于" + maxfzTemp + "度";
                    startSpeak(talkText);
                    return talkText;
                }
                mOperateType = OPERATE_TYPE_FZROOM_TEMP_SET;

                ASK_SUCCESS_STR = "已为您把冷冻室设为" + roomTemp + "度";
                ASK_FAIL_STR = "抱歉，温度设置失败";
                roomResult = ControlManager.getInstance().setRoomTemp(roomTemp, SerialInfo.ROOM_FREEZING_ROOM, mDeviceSetCallback);
                if (roomResult) {
                } else {
                    talkText = "抱歉，温度设置失败";
                    startSpeak(talkText);
                }
                return talkText;
        }
        return null;
    }

    /***
     *定时器技能
     * @param xfAiuiCusTomResp
     * @return
     */
    private String timerServiceProcess(XFAiuiCusTomResp xfAiuiCusTomResp) {
        if (xfAiuiCusTomResp == null) {
            return null;
        }
        String talkText = null;
        String intentString = xfAiuiCusTomResp.semantic.get(0).intent;
        if (intentString == null) {
            Log.e(TAG, "timerServiceProcess intentString null");
            return null;
        }
        switch (intentString) {
            case "minute_timer":
                if (GlobalParams.Clock_Start) {
                    talkText = "定时器已启动，请先关闭定时器";
                    startSpeak(talkText);
                    return talkText;
                }
                int minTime = Integer.parseInt(xfAiuiCusTomResp.semantic.get(0).slots.get(0).normValue);
                if (minTime >= 10000) {
                    Log.e(TAG, "timer too large !");
                    return null;
                }

                talkText = "已为您把定时器设置为" + minTime + "分钟";
                startSpeak(talkText);
                removeTalkDialog();
                TimerApi.start(mContext, minTime);
                return talkText;

            case "hour_timer":
                if (GlobalParams.Clock_Start) {
                    talkText = "定时器已启动，请先关闭定时器";
                    startSpeak(talkText);
                    return talkText;
                }
                int hourTime = Integer.parseInt(xfAiuiCusTomResp.semantic.get(0).slots.get(0).normValue);
                int hourToMinTime = hourTime * 60;
                if (hourToMinTime >= 10000) {
                    Log.e(TAG, "paserMinNumber fail");
                    return null;
                }

                talkText = "已为您把定时器设置为" + hourTime + "小时";
                startSpeak(talkText);
                removeTalkDialog();
                TimerApi.start(mContext, hourToMinTime);
                return talkText;

            case "scene_timer":
                if (GlobalParams.Clock_Start) {
                    talkText = "定时器已启动，请先关闭定时器";
                    startSpeak(talkText);
                    return talkText;
                }
                String sceneValue = xfAiuiCusTomResp.semantic.get(0).slots.get(1).value;
                sceneValue = sceneValue.replace("模式", "");
                int mode = 0;
                switch (sceneValue) {
                    case "蒸蛋":
                        mode = 1;
                        talkText = "已为您把定时器设置为蒸蛋模式";
                        break;
                    case "煮粥":
                        mode = 2;
                        talkText = "已为您把定时器设置为煮粥模式";
                        break;
                    case "煲汤":
                        mode = 3;
                        talkText = "已为您把定时器设置为煲汤模式";
                        break;
                    case "焖饭":
                        mode = 4;
                        talkText = "已为您把定时器设置为焖饭模式";
                        break;
                    case "炖鱼":
                        mode = 5;
                        talkText = "已为您把定时器设置为炖鱼模式";
                        break;
                    case "煎药":
                        mode = 6;
                        talkText = "已为您把定时器设置为煎药模式";
                        break;
                }
                startSpeak(talkText);
                removeTalkDialog();
                TimerApi.start(mContext, 0, mode);
                return talkText;
            case "close_timer":
                if (!GlobalParams.Clock_Start) {
                    talkText = "定时器已关闭";
                    startSpeak(talkText);
                    return talkText;
                }
                talkText = "已为您关闭定时器";
                startSpeak(talkText);
                removeTalkDialog();
                TimerApi.cancel(mContext);
                return talkText;

        }
        return null;
    }

    /***
     * 智能互联技能
     * @param xfAiuiCusTomResp
     * @return
     */
    private String smarthomeServiceProcess(XFAiuiCusTomResp xfAiuiCusTomResp) {
        if (xfAiuiCusTomResp == null) {
            return null;
        }
        String talkText = null;
        String intentString = xfAiuiCusTomResp.semantic.get(0).intent;
        if (intentString == null) {
            Log.e(TAG, "timerServiceProcess intentString null");
            return null;
        }
        switch (intentString) {
            case "device_contorl":
                if (MiotManager.getPeopleManager().getPeople() == null) {
                    talkText = "登录后才能体验智能互联哦";
                    startSpeak(talkText);
                    return talkText;
                }
                talkText = "为您跳转智能互联";
                startSpeak(talkText);
                removeTalkDialog();
                Intent intent1 = new Intent(mContext, ConnectUnionActivity.class);
                intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent1.putExtra("voice_entry", true);
                mContext.startActivity(intent1);
                return talkText;

            case "device_choose":
                int index = Integer.parseInt(xfAiuiCusTomResp.semantic.get(0).slots.get(0).normValue);
                if (index <= 0) {
                    return null;
                }
                Intent intentChoose = new Intent(BroadcastAction.ACTION_DEVICE_CHOOSE);
                intentChoose.putExtra(BroadcastAction.EXTRA_DATA, index);
                LocalBroadcastManager.getInstance(ViomiApplication.getContext()).sendBroadcast(intentChoose);
                break;

            case "waterpuri_temp":
                int temp = Integer.parseInt(xfAiuiCusTomResp.semantic.get(0).slots.get(1).normValue);
                Intent tempChoose = new Intent(BroadcastAction.ACTION_WATERPURI_TEMP);
                tempChoose.putExtra(BroadcastAction.EXTRA_DATA, temp);
                LocalBroadcastManager.getInstance(ViomiApplication.getContext()).sendBroadcast(tempChoose);
                break;
        }
        return null;
    }


    private void jumpMusicActivity(String url, String title, String category) {
        Intent storyIntent = new Intent(mContext, MusicPlayActivity.class);
        storyIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        log.d(TAG, "playUrl=" + url + ",title=" + title);
        storyIntent.putExtra("playUrl", url);
        storyIntent.putExtra("title", title);
        removeTalkDialog();
        mContext.startActivity(storyIntent);
    }

    /***
     * 发送聊天信息
     * @param type 发送的类型，听到的还是说的 参考TalkInfo
     * @param text 发送内容
     * @param isNew 是否新唤醒窗口
     */
    private void sendTalkText(int type, String text, boolean isNew) {
        if (mHandler != null) {
            log.d(TAG, "sendTalkText,type=" + type + ",isNew=" + isNew + ",text=" + text);
            Message message = mHandler.obtainMessage();
            if (isNew) {
                message.what = BackgroudService.WHAT_TALK_DIALOG_ADD;
            } else {
                message.what = BackgroudService.WHAT_TALK_DIALOG_UPDATE;
            }
            message.arg1 = type;
            message.obj = text;
            mHandler.sendMessage(message);
        }
    }

    /***
     * 隐藏聊天窗口
     */
    private void removeTalkDialog() {
        if (mHandler != null) {
            mHandler.sendEmptyMessage(BackgroudService.WHAT_TALK_DIALOG_REMOVE);
        }
    }

    /***
     * 天气播报字符串
     * @param xfWeatherResp
     * @return
     */
    private String getWeatherReport(XFWeatherResp xfWeatherResp) {

        String report = null;
        if (xfWeatherResp == null || xfWeatherResp.data == null || xfWeatherResp.data.result == null
                || xfWeatherResp.data.result.size() == 0) {
            Log.e(TAG, "getWeatherReport null");
            return report;
        }

        XFWeatherResp.Result result = xfWeatherResp.data.result.get(0);
        if (result.city == null || result.weather == null) {
            Log.e(TAG, "getWeatherReport content null");
            return null;
        }
        if (xfWeatherResp != null && xfWeatherResp.answer != null) {
            report = xfWeatherResp.answer.text;
        }
        mVioceCity = result.city;
        return report;
    }

    /***
     * 处理冰箱命令
     * @param xfFridgeResp
     * @return 要播报的语句
     */
    private String processFridgeResp(XFFridgeResp xfFridgeResp) {
        if (xfFridgeResp == null || xfFridgeResp.semantic == null || xfFridgeResp.semantic.slots == null) {
            return null;
        }
        XFFridgeResp.Slots slots = xfFridgeResp.semantic.slots;
        if (slots.attr != null && slots.attr.equals("开关")) {
            String temperatureZone = slots.temperatureZone;
            if (temperatureZone == null) {
                temperatureZone = "";
            }
            String result = null;
            String sttrValue = slots.attrValue;
            if (sttrValue != null && sttrValue.equals("开")) {
                result = temperatureZone + "已打开";
            } else if (sttrValue != null && sttrValue.equals("关")) {
                result = temperatureZone + "已关闭";
            }
            return result;
        }
        return null;
    }

    /***
     * 离线语音识别
     * @param text 返回说的话
     */
    private String localRecognizerProcess(String text) {
        String talkText = "";
        LocalRecogInfo localRecogInfo = GsonUtil.parseJsonWithGson(text, LocalRecogInfo.class);
        if (localRecogInfo == null || localRecogInfo.ws == null || localRecogInfo.ws.size() == 0) {
            Log.e(TAG, "localRecognizerProcess parse error");
            return null;
        }
        int index = 65535;
        String judgeStr = null;
        String listenStr = "";
        for (int i = 0; i < localRecogInfo.ws.size(); i++) {//判断id
            String slot = localRecogInfo.ws.get(i).slot;
            if (slot == null) {
                Log.e(TAG, "localRecognizerProcess slot null");
                return null;
            }
            if (slot.equals(LocalRecogVar.RoomType) || slot.equals(LocalRecogVar.ModelType) || slot.equals(LocalRecogVar.call)
                    || slot.equals(LocalRecogVar.CleanStr) || slot.equals(LocalRecogVar.CCRoomType) || slot.equals(LocalRecogVar.RoomSetStr)
                    || slot.equals(LocalRecogVar.TimerMode) || slot.equals(LocalRecogVar.TimerHourStr) || slot.equals(LocalRecogVar.TimerMinStr)
                    || slot.equals(LocalRecogVar.MusicStr) || slot.equals(LocalRecogVar.PhotoStr) || slot.equals(LocalRecogVar.MusicStopStr)
                    || slot.equals(LocalRecogVar.ServiceTelNum) || slot.equals(LocalRecogVar.MenuStr) || slot.equals(LocalRecogVar.FilmStr)
                    || slot.equals(LocalRecogVar.TimerCloseStr)) {
                index = i;
            }
            if (slot.equals(LocalRecogVar.RoomEnable) || slot.equals(LocalRecogVar.RoomSet) || slot.equals(LocalRecogVar.CCRoomType) ||
                    slot.equals(LocalRecogVar.TimerHour)) {
                judgeStr = localRecogInfo.ws.get(i).cw.get(0).w;
            }
            listenStr += localRecogInfo.ws.get(i).cw.get(0).w;
        }
        if (listenStr.length() > 0) {
            listenStr = listenStrFormat(listenStr);
            sendTalkText(TalkInfo.TYPE_LISTEN, listenStr, false);
        }

        if (index == 65535) {
            Log.e(TAG, "localRecognizerProcess not found");
            return null;
        }

        int id = localRecogInfo.ws.get(index).cw.get(0).id;
        log.d(TAG, "localRecognizerProcess id=" + id);

        if (listenStr == null || listenStr.length() < 4) {//返回字符串太小，当误听
            return null;
        }

        switch (id) {
            case LocalRecogVar.ID_SMART://智能模式
                if (ControlManager.getInstance().getDataSendInfo().mode == SerialInfo.MODE_SMART) {
                    talkText = "已经处于智能模式";
                } else {
                    mOperateType = OPERATE_TYPE_ENABLE_SMART;
                    boolean smart = ControlManager.getInstance().enableSmartMode(true);
                    if (smart) {
                        talkText = "已为您开启智能模式";
                    } else {
                        talkText = "开启智能模式失败";
                    }
                }
                startSpeak(talkText);
                return talkText;

            case LocalRecogVar.ID_HOLIDAY://假日模式
                if (ControlManager.getInstance().getDataSendInfo().mode == SerialInfo.MODE_HOLIDAY) {
                    talkText = "已经处于假日模式";
                    startSpeak(talkText);
                } else {
                    mOperateType = OPERATE_TYPE_ENABLE_HOLIDAY;
                    ASK_SUCCESS_STR = "已为您开启假日模式";
                    ASK_FAIL_STR = "开启假日模式失败";
                    boolean holiday = ControlManager.getInstance().enableHolidayMode(true, mDeviceSetCallback);
                    if (!holiday) {
                        talkText = "开启假日模式失败";
                        startSpeak(talkText);
                    }
                }
                return talkText;

            case LocalRecogVar.ID_CLEAN://一键净化
                if (DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V2) || DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V3)
                        || DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V31)) {
                    return null;
                }
                if (ControlManager.getInstance().isOneKeyCleanRunning()) {
                    talkText = "已经在净化中";
                } else {
                    mOperateType = OPERATE_TYPE_ENABLE_CLEAN;
                    boolean smart = ControlManager.getInstance().enableOneKeyClean(true);
                    if (smart) {
                        talkText = "已为您开启一键净化";
                    } else {
                        talkText = "开启一键净化失败";
                    }
                }
                startSpeak(talkText);
                return talkText;

            case LocalRecogVar.ID_RCROOM_ENABLE://打开/关闭冷藏室
//                if(DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V2)){
//                    return false;
//                }
                log.d(TAG, "enable cc room ,str=" + judgeStr);
                if (judgeStr == null) {
                    Log.e(TAG, "enable cc room null");
                    return null;
                }

                boolean ccresult;
                if (judgeStr.equals("打开")) {
                    if (ControlManager.getInstance().getDataSendInfo().cold_closet_room_enable) {
                        talkText = "冷藏室已经处于打开状态";
                        startSpeak(talkText);
                    } else {
                        mOperateType = OPERATE_TYPE_OPEAN_CCROOM;
                        ASK_SUCCESS_STR = "已为您打开冷藏室";
                        ASK_FAIL_STR = "冷藏室打开失败";
                        ccresult = ControlManager.getInstance().enableRoom(true, SerialInfo.ROOM_COLD_COLSET, mDeviceSetCallback);
                        if (!ccresult) {
                            talkText = "冷藏室打开失败";
                            startSpeak(talkText);
                        }
                    }
                } else if (judgeStr.equals("关闭")) {
                    if (!ControlManager.getInstance().getDataSendInfo().cold_closet_room_enable) {
                        talkText = "冷藏室已经处于关闭状态";
                        startSpeak(talkText);
                    } else {
                        mOperateType = OPERATE_TYPE_CLOSE_CCROOM;
                        ASK_SUCCESS_STR = "已为您关闭冷藏室";
                        ASK_FAIL_STR = "冷藏室关闭失败";
                        ccresult = ControlManager.getInstance().enableRoom(false, SerialInfo.ROOM_COLD_COLSET, mDeviceSetCallback);
                        if (!ccresult) {
                            talkText = "冷藏室关闭失败";
                            startSpeak(talkText);
                        }
                    }
                }
                return talkText;

            case LocalRecogVar.ID_ROOM_ENABLE://打开/关闭变温室
                if (DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V3) || DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V31)) {
                    return null;
                }
                log.d(TAG, "enable tc room ,str=" + judgeStr);
                if (judgeStr == null) {
                    Log.e(TAG, "enable cc room null");
                    return null;
                }

                boolean tcresult;
                if (judgeStr.equals("打开")) {
                    if (ControlManager.getInstance().getDataSendInfo().temp_changeable_room_room_enable) {
                        talkText = "变温室已经处于打开状态";
                        startSpeak(talkText);
                    } else {
                        mOperateType = OPERATE_TYPE_OPEN_TCROOM;
                        ASK_SUCCESS_STR = "已为您打开变温室";
                        ASK_FAIL_STR = "变温室打开失败";
                        tcresult = ControlManager.getInstance().enableRoom(true, SerialInfo.ROOM_CHANGEABLE_ROOM, mDeviceSetCallback);
                        if (!tcresult) {
                            talkText = "变温室打开失败";
                            startSpeak(talkText);
                        }
                    }
                } else if (judgeStr.equals("关闭")) {
                    if (!ControlManager.getInstance().getDataSendInfo().temp_changeable_room_room_enable) {
                        talkText = "变温室已经处于关闭状态";
                        startSpeak(talkText);
                    } else {
                        mOperateType = OPERATE_TYPE_CLOSE_TCROOM;
                        ASK_SUCCESS_STR = "已为您关闭变温室";
                        ASK_FAIL_STR = "变温室关闭失败";
                        tcresult = ControlManager.getInstance().enableRoom(false, SerialInfo.ROOM_CHANGEABLE_ROOM, mDeviceSetCallback);
                        if (tcresult) {
                            mOldScene = GlobalParams.getInstance().getSceneChoose();
                            GlobalParams.getInstance().setSceneChoose("");
                        } else {
                            talkText = "变温室关闭失败";
                            startSpeak(talkText);
                        }
                    }
                }

                return talkText;

            case LocalRecogVar.ID_CALL_XIAOXIAN1://唤醒过程中呼叫小鲜
            case LocalRecogVar.ID_CALL_XIAOXIAN2:
            case LocalRecogVar.ID_CALL_XIAOXIAN3:
                talkText = mContext.getResources().getString(R.string.text_wakeup_ask_repeat);
                startSpeak(talkText);
                return talkText;

            case LocalRecogVar.ID_ROOM_LENGCANG:
            case LocalRecogVar.ID_ROOM_LENGDONG:
            case LocalRecogVar.ID_ROOM_RUANLENGDONG:
            case LocalRecogVar.ID_ROOM_JIEDONG:
            case LocalRecogVar.ID_ROOM_ROULEI:
            case LocalRecogVar.ID_ROOM_GANHUO:
            case LocalRecogVar.ID_ROOM_SHUCAI:
            case LocalRecogVar.ID_ROOM_SHUIGUO:
            case LocalRecogVar.ID_ROOM_SHENCAI:
            case LocalRecogVar.ID_ROOM_JIDAN:
            case LocalRecogVar.ID_ROOM_BINGPI:
            case LocalRecogVar.ID_ROOM_YU:
            case LocalRecogVar.ID_ROOM_XIANPI:
            case LocalRecogVar.ID_ROOM_CHAYE:
            case LocalRecogVar.ID_ROOM_XUEGAO:
            case LocalRecogVar.ID_ROOM_MIANMO:
            case LocalRecogVar.ID_ROOM_MURU:
                if (DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V3) || DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V31)) {
                    return null;
                }
                log.d(TAG, "set room type=" + judgeStr);
                judgeStr = judgeStr.replace("区", "");
                int temp = 100;
                ArrayList<TCRoomScene> sceneAllList = RoomSceneManager.getInstance().getSceneAllList();
                for (int i = 0; i < sceneAllList.size(); i++) {
                    if (sceneAllList.get(i).name.equals(judgeStr)) {
                        temp = sceneAllList.get(i).value;
                        break;
                    }
                }
                if (temp == 100) {
                    talkText = "抱歉，没有找到对应场景";
                    startSpeak(talkText);
                    return talkText;
                }

                mOperateType = OPERATE_TYPE_SCENE_SET;
                ASK_SUCCESS_STR = "已为您把变温室设为" + judgeStr + "区";
                ASK_FAIL_STR = "变温室设置失败";
                boolean sceneResult = ControlManager.getInstance().setRoomTemp(temp, SerialInfo.ROOM_CHANGEABLE_ROOM, mDeviceSetCallback);
                if (sceneResult) {
                    mOldScene = GlobalParams.getInstance().getSceneChoose();
                    GlobalParams.getInstance().setSceneChoose(judgeStr);
                } else {
                    talkText = "变温室设置失败";
                    startSpeak(talkText);
                }
                return talkText;

            case LocalRecogVar.ID_CCROOM_TEMP_SET:
            case LocalRecogVar.ID_TCROOM_TEMP_SET:
            case LocalRecogVar.ID_FZROOM_TEMP_SET:

                int roomTemp = paserTempNumber(localRecogInfo);
                if (roomTemp >= 100) {
                    Log.e(TAG, "paserTempNumber fail!");
                    return null;
                }
                String roomStr = "";
                int roomType = LocalRecogVar.ID_CCROOM_TEMP_SET;
                if (id == LocalRecogVar.ID_CCROOM_TEMP_SET) {
                    roomStr = "冷藏室";
                    roomType = SerialInfo.ROOM_COLD_COLSET;
                    if (roomTemp < 2) {
                        talkText = "冷藏室不能设置低于2度";
                        startSpeak(talkText);
                        return talkText;
                    } else if (roomTemp > 8) {
                        talkText = "冷藏室不能设置高于8度";
                        startSpeak(talkText);
                        return talkText;
                    }
                    mOperateType = OPERATE_TYPE_CCROOM_TEMP_SET;
                } else if (id == LocalRecogVar.ID_TCROOM_TEMP_SET) {
                    if (DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V3) || DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V31)) {
                        return null;
                    }
                    roomStr = "变温室";
                    roomType = SerialInfo.ROOM_CHANGEABLE_ROOM;
                    if (roomTemp < -18) {
                        talkText = "变温室不能设置低于-18度";
                        startSpeak(talkText);
                        return talkText;
                    }
                    int maxTcTemp = 8;
                    if (DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V2)) {
                        maxTcTemp = -3;
                    }
                    if (roomTemp > maxTcTemp) {
                        talkText = "变温室不能设置高于" + maxTcTemp + "度";
                        startSpeak(talkText);
                        return talkText;
                    }
                    mOperateType = OPERATE_TYPE_TCROOM_TEMP_SET;
                } else if (id == LocalRecogVar.ID_FZROOM_TEMP_SET) {
                    roomStr = "冷冻室";
                    roomType = SerialInfo.ROOM_FREEZING_ROOM;
                    int minfzTemp = -25;
                    if (DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V2) || DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V3)
                            || DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V31)) {
                        minfzTemp = -24;
                    }
                    if (roomTemp < minfzTemp) {
                        talkText = "冷冻室不能设置低于" + minfzTemp + "度";
                        startSpeak(talkText);
                        return talkText;
                    }
                    int maxfzTemp = -15;
                    if (DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V2) || DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V3)
                            || DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V31)) {
                        maxfzTemp = -16;
                    }
                    if (roomTemp > maxfzTemp) {
                        talkText = "冷冻室不能设置高于" + maxfzTemp + "度";
                        startSpeak(talkText);
                        return talkText;
                    }
                    mOperateType = OPERATE_TYPE_FZROOM_TEMP_SET;
                }

                ASK_SUCCESS_STR = "已为您把" + roomStr + "设为" + roomTemp + "度";
                ASK_FAIL_STR = "抱歉，温度设置失败";
                boolean tempResult = ControlManager.getInstance().setRoomTemp(roomTemp, roomType, mDeviceSetCallback);
                if (tempResult) {
                    if (id == LocalRecogVar.ID_TCROOM_TEMP_SET) {
                        mOldScene = GlobalParams.getInstance().getSceneChoose();
                        GlobalParams.getInstance().setSceneChoose(GlobalParams.Value_Scene_ChooseName);
                    }
                } else {
                    talkText = "抱歉，温度设置失败";
                    startSpeak(talkText);
                }

                return talkText;

            case LocalRecogVar.ID_TIMER_ZHUDAN:
            case LocalRecogVar.ID_TIMER_ZHUZHOU:
            case LocalRecogVar.ID_TIMER_BAOTANG:
            case LocalRecogVar.ID_TIMER_MENGFAN:
            case LocalRecogVar.ID_TIMER_DUNYU:
            case LocalRecogVar.ID_TIMER_JIANYAO:
                if (GlobalParams.Clock_Start) {
                    talkText = "定时器已启动，请先关闭定时器";
                    startSpeak(talkText);
                    return talkText;
                }
                int mode = 0;
                switch (id) {
                    case LocalRecogVar.ID_TIMER_ZHUDAN:
                        mode = 1;
                        talkText = "已为您把定时器设置为蒸蛋模式";
                        break;
                    case LocalRecogVar.ID_TIMER_ZHUZHOU:
                        mode = 2;
                        talkText = "已为您把定时器设置为煮粥模式";
                        break;
                    case LocalRecogVar.ID_TIMER_BAOTANG:
                        mode = 3;
                        talkText = "已为您把定时器设置为煲汤模式";
                        break;
                    case LocalRecogVar.ID_TIMER_MENGFAN:
                        mode = 4;
                        talkText = "已为您把定时器设置为焖饭模式";
                        break;
                    case LocalRecogVar.ID_TIMER_DUNYU:
                        mode = 5;
                        talkText = "已为您把定时器设置为炖鱼模式";
                        break;
                    case LocalRecogVar.ID_TIMER_JIANYAO:
                        mode = 6;
                        talkText = "已为您把定时器设置为煎药模式";
                        break;
                }
//                Intent intentTimer = new Intent(mContext, Timer2Activity.class);
//                intentTimer.putExtra("mode",mode);
//                intentTimer.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                mContext.startActivity(intentTimer);

                startSpeak(talkText);
                removeTalkDialog();
                TimerApi.start(mContext, 0, mode);
                return talkText;


            case LocalRecogVar.ID_TIMER_MIN_SET://定时器分钟设置
                int minTime = paserMinNumber(localRecogInfo);
                if (minTime >= 10000) {
                    Log.e(TAG, "paserMinNumber fail");
                    return null;
                }
                if (GlobalParams.Clock_Start) {
                    talkText = "定时器已启动，请先关闭定时器";
                    startSpeak(talkText);
                    return talkText;
                }
                talkText = "已为您把定时器设置为" + minTime + "分钟";
                startSpeak(talkText);
                removeTalkDialog();
                TimerApi.start(mContext, minTime);
                return talkText;

            case LocalRecogVar.ID_TIMER_HOUR_SET://定时器小时设置
                int hourToMinTime = paserHourNumber(judgeStr);
                if (hourToMinTime >= 10000) {
                    Log.e(TAG, "paserMinNumber fail");
                    return null;
                }
                if (GlobalParams.Clock_Start) {
                    talkText = "定时器已启动，请先关闭定时器";
                    startSpeak(talkText);
                    return talkText;
                }
                talkText = "已为您把定时器设置为" + judgeStr;
                startSpeak(talkText);
                removeTalkDialog();
                TimerApi.start(mContext, hourToMinTime);
                return talkText;

            case LocalRecogVar.ID_TIMER_CLOSE://定时器关闭
            case LocalRecogVar.ID_TIMER_CANCEL:
                if (!GlobalParams.Clock_Start) {
                    talkText = "定时器已关闭";
                    startSpeak(talkText);
                    return talkText;
                }
                talkText = "已为您关闭定时器";
                startSpeak(talkText);
                removeTalkDialog();
                TimerApi.cancel(mContext);
                return talkText;

            case LocalRecogVar.ID_MUSIC_1://播放音乐
            case LocalRecogVar.ID_MUSIC_2:
            case LocalRecogVar.ID_MUSIC_3:
                talkText = "已为您打开音乐";
                startSpeak(talkText);
                removeTalkDialog();
                String packageName = "com.tencent.qqmusicpad";
                if (ApkUtil.checkApkExist(ViomiApplication.getContext(), packageName)) {
                    Log.e(TAG, "startOtherApp,packageName=" + packageName);
                    ApkUtil.startOtherApp(mContext, packageName, false);
                }
                return talkText;

            case LocalRecogVar.ID_MUSIC_STOP1://停止播放
            case LocalRecogVar.ID_MUSIC_STOP2:
                Intent intentStop = new Intent(BroadcastAction.ACTION_STOP_MUSIC);
                LocalBroadcastManager.getInstance(ViomiApplication.getContext()).sendBroadcast(intentStop);
                return null;

            case LocalRecogVar.ID_PHOTO_PLAY://电子相册
            case LocalRecogVar.ID_PHOTO_PLAY1:

                SharedPreferences sp = ViomiApplication.getContext().getSharedPreferences("screen_imgs", Context.MODE_PRIVATE);
                Set<String> screen_set = sp.getStringSet("screen_set", new HashSet<String>());
                if (screen_set == null || screen_set.size() == 0) {
                    talkText = "请先设置电子相册照片";
                    startSpeak(talkText);
                    removeTalkDialog();
                    Intent intentAlum = new Intent(mContext, AlbumActivity.class);
                    intentAlum.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(intentAlum);
                    return talkText;
                }
                talkText = "已为您打开电子相册";
                startSpeak(talkText);
                removeTalkDialog();
                Intent intent1 = new Intent(mContext, ScreenSaverActivity.class);
                intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent1);
                return talkText;

            case LocalRecogVar.ID_PHOTO_SET://设置电子相册
                talkText = "已为您跳转电子相册设置页面";
                startSpeak(talkText);
                removeTalkDialog();
                Intent intenXiangce = new Intent(mContext, AlbumActivity.class);
                intenXiangce.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intenXiangce);
                return talkText;

            case LocalRecogVar.ID_SERVICE_TEL1://售后电话
            case LocalRecogVar.ID_SERVICE_TEL2://售后电话
                talkText = "我们的售后电话是：4001002632";
                startSpeak(talkText);
                return talkText;

            case LocalRecogVar.ID_MENU_CHECK1://菜谱
            case LocalRecogVar.ID_MENU_CHECK2:
            case LocalRecogVar.ID_MENU_CHECK3://菜谱
            case LocalRecogVar.ID_MENU_CHECK4:
                talkText = "已为您跳转菜谱";
                startSpeak(talkText);
                removeTalkDialog();

//                Intent intentMenu = new Intent(mContext, CommonHeaderActivity.class);
//                intentMenu.putExtra("url", HttpConnect.COOKMENUURL);
//                intentMenu.putExtra("title", "健康菜谱");
//                intentMenu.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                mContext.startActivity(intentMenu);
                Intent intent = new Intent(mContext, RecipesHealthyActivity.class);
                mContext.startActivity(intent);
                return talkText;

            case LocalRecogVar.ID_FILM_PLAY1://播放视频
            case LocalRecogVar.ID_FILM_PLAY2:
            case LocalRecogVar.ID_FILM_PLAY3:
                talkText = "已为您打开影音娱乐";
                startSpeak(talkText);
                removeTalkDialog();
                String packageName1 = "com.qiyi.video.pad";
                if (ApkUtil.checkApkExist(ViomiApplication.getContext(), packageName1)) {
                    Log.e(TAG, "startOtherApp,packageName1=" + packageName1);
                    ApkUtil.startOtherApp(mContext, packageName1, false);
                }
                return talkText;

            default:
                break;

        }
        return null;

    }

    /***
     * g格式化输入命令的字符串 ，十转为10，俩转为2
     * @param oldStr
     * @return
     */
    private String listenStrFormat(String oldStr) {
        if (oldStr == null) {
            return null;
        }
        oldStr = oldStr.replace("俩", "2");
        int index = oldStr.indexOf("十");
        if (index <= 0) {
            return oldStr;
        } else {
            boolean preDigit = false, afterDigit = false;
            if (Character.isDigit(oldStr.charAt(index - 1))) {
                preDigit = true;
            }
            if (index < oldStr.length() && Character.isDigit(oldStr.charAt(index + 1))) {
                afterDigit = true;
            }
            if (preDigit && afterDigit) {
                oldStr = oldStr.substring(0, index) + oldStr.substring(index + 1);
            } else if (preDigit && (!afterDigit)) {
                oldStr = oldStr.replace("十", "0");
            } else if ((!preDigit) && afterDigit) {
                oldStr = oldStr.replace("十", "1");
            }
            return oldStr;
        }

    }

    private void setDeviceSetCallback() {
        if (mDeviceSetCallback == null) {
            mDeviceSetCallback = new AppCallback<String>() {
                @Override
                public void onSuccess(String data) {
                    Log.e(TAG, "setDeviceSetCallback success!data=" + data);
                    startSpeak(ASK_SUCCESS_STR);
                }

                @Override
                public void onFail(int errorCode, String msg) {
                    startSpeak(ASK_FAIL_STR);
                    if (mOperateType == OPERATE_TYPE_TCROOM_TEMP_SET || mOperateType == OPERATE_TYPE_CLOSE_TCROOM
                            || mOperateType == OPERATE_TYPE_SCENE_SET) {
                        GlobalParams.getInstance().setSceneChoose(mOldScene);
                    }
                }
            };
        }
    }

    /***
     * 时钟设置解析
     * @param hourStr
     * @return 10000表示解析错误
     */
    private int paserHourNumber(String hourStr) {
        switch (hourStr) {
            case "一个小时":
            case "一小时":
                return 60;
            case "半个小时":
            case "半小时":
                return 30;
            case "一个半小时":
                return 90;
            case "两个小时":
            case "两小时":
                return 120;
            case "两个半小时":
                return 150;
            case "三个小时":
            case "三小时":
                return 180;
            default:
                return 10000;
        }
    }

    /***
     * 分钟设置解析
     * @param localRecogInfo
     * @return 10000表示解析错误
     */
    private int paserMinNumber(LocalRecogInfo localRecogInfo) {
        boolean isMinus = false;
        String hundredStr = "";
        String hundredStrU = "";
        String tenStr = "";
        String numStr = "";
        String tenStrU = "";
        for (int i = 0; i < localRecogInfo.ws.size(); i++) {//判断id
            String slot = localRecogInfo.ws.get(i).slot;
            if (slot.equals(LocalRecogVar.TimerSetHundred)) {
                hundredStr = localRecogInfo.ws.get(i).cw.get(0).w;
            }
            if (slot.equals(LocalRecogVar.TimerSetHundredU)) {
                hundredStrU = localRecogInfo.ws.get(i).cw.get(0).w;
            }
            if (slot.equals(LocalRecogVar.TimerSetTen)) {
                tenStr = localRecogInfo.ws.get(i).cw.get(0).w;
            }
            if (slot.equals(LocalRecogVar.TimerSetNum)) {
                numStr = localRecogInfo.ws.get(i).cw.get(0).w;
            }
            if (slot.equals(LocalRecogVar.TimerSetTenU)) {
                tenStrU = localRecogInfo.ws.get(i).cw.get(0).w;
            }
        }
        boolean isHundredNone = false, isTenNone = false, isNumNone = false;
        hundredStr = hundredStr.replace("俩", "2");
        tenStr = tenStr.replace("俩", "2");
        numStr = numStr.replace("俩", "2");
        int hundred = 0;
        try {
            hundred = Integer.parseInt(hundredStr);
        } catch (Exception e) {
            isHundredNone = true;
        }
        int hundredU = 0;
        if ("百".equals(hundredStrU)) {
            hundredU = 1;
        }

        int ten = 0;
        try {
            ten = Integer.parseInt(tenStr);
        } catch (Exception e) {
            isTenNone = true;
        }
        int tenU = 0;
        if ("十".equals(tenStrU)) {
            tenU = 1;
        }

        int num = 0;
        try {
            num = Integer.parseInt(numStr);
        } catch (Exception e) {
            isNumNone = true;
        }
        int sum = 0;
        if (isTenNone && isNumNone && (tenU == 0)) {//都获取不到
            return 10000;
        } else if ((!isTenNone) && isNumNone && (tenU == 0)) {//只获取到hundred
            return ten;
        } else {
            if (tenU == 0 && (!isTenNone) && (!isNumNone) && num == 4) {//十识别到4处理
                num = 0;
                tenU = 1;
            }

            if (ten == 0) {
                sum = sum + 10 * tenU + num;
            } else {
                sum = sum + ten * 10 + num;
            }

            return sum;
        }
    }

    private int paserTempNumber(LocalRecogInfo localRecogInfo) {
        boolean isMinus = false;
        String tenStr = "";
        String numStr = "";
        String tenStrU = "";
        for (int i = 0; i < localRecogInfo.ws.size(); i++) {//判断id
            String slot = localRecogInfo.ws.get(i).slot;
            if (slot.equals(LocalRecogVar.RoomSetPre)) {
                isMinus = true;
            }
            if (slot.equals(LocalRecogVar.RoomSetTen)) {
                tenStr = localRecogInfo.ws.get(i).cw.get(0).w;
            }
            if (slot.equals(LocalRecogVar.RoomSetNum)) {
                numStr = localRecogInfo.ws.get(i).cw.get(0).w;
            }
            if (slot.equals(LocalRecogVar.RoomSetTenU)) {
                tenStrU = localRecogInfo.ws.get(i).cw.get(0).w;
            }
        }
        boolean isTenNone = false, isNumNone = false;
        tenStr = tenStr.replace("俩", "2");
        numStr = numStr.replace("俩", "2");
        int ten = 0;
        try {
            ten = Integer.parseInt(tenStr);
        } catch (Exception e) {
            isTenNone = true;
        }
        int tenU = 0;
        if ("十".equals(tenStrU)) {
            tenU = 1;
        }

        int num = 0;
        try {
            num = Integer.parseInt(numStr);
        } catch (Exception e) {
            isNumNone = true;
        }
        int sum;
        if (isTenNone && isNumNone && (tenU == 0)) {//都获取不到
            return 100;
        } else if ((!isTenNone) && isNumNone && (tenU == 0)) {//只获取到ten
            sum = ten;
            if (isMinus) {
                sum = 0 - sum;
            }
            return sum;
        } else {
            if (ten == 0) {
                sum = 10 * tenU + num;
            } else {
                sum = ten * 10 + num;
            }

            if (isMinus) {
                sum = 0 - sum;
            }
            return sum;
        }
    }


    /***
     *判断时间段，0;早上好，1中午好，2下午好；3晚上好
     * @return
     */
    private int getTime() {
        SimpleDateFormat df = new SimpleDateFormat("HH");
        String timeStr = df.format(new Date());
        int hour;
        try {
            hour = Integer.parseInt(timeStr);
        } catch (Exception e) {
            hour = -1;
            e.printStackTrace();
            return -1;
        }

        if ((hour >= 4) && (hour <= 11)) {
            return 0;
        } else if ((hour >= 12) && (hour <= 13)) {
            return 1;
        } else if ((hour >= 14) && (hour <= 17)) {
            return 2;
        } else if ((hour >= 18) || (hour <= 4)) {
            return 3;
        } else {
            return -1;
        }

    }

    /**
     * 开启/关闭语音
     *
     * @param enable
     */
    public void enableVoice(boolean enable) {
        if (enable) {
            isSpeaking = false;
            mIsWaking = false;
            VoiceWakeuper.getWakeuper().startListening(mWakeuperListener);
        } else {
            VoiceWakeuper.getWakeuper().stopListening();
            if (mTts != null && mTts.isSpeaking()) {
                mTts.stopSpeaking();
            }
        }
    }

    /***
     * 是否被占用
     * @return
     */
    public boolean isVoiceOccupy() {
        return mAudioManager.isMusicActive();
    }


}
