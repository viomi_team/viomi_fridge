package com.viomi.fridge.manager;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.util.Log;

import com.viomi.fridge.common.ViomiApplication;

/**
 * Created by young2 on 2017/5/22.
 */

public class BleManager {
    private final static String TAG="BleManager";

    private static BleManager INSTANCE=null;
    private  BluetoothManager mBluetoothManager;
    private BluetoothAdapter mBluetoothAdapter;


    public static BleManager getInstance(){
        synchronized (BleManager.class){
            if(INSTANCE==null){
                synchronized (BleManager.class){
                    if(INSTANCE==null){
                        INSTANCE=new BleManager();
                    }
                }
            }
        }
        return INSTANCE;
    }

    public BluetoothAdapter getBleAdapter(){
        return mBluetoothAdapter;
    }

    public BleManager(){
        bleInit();
    }

    /***
     *蓝牙初始化
     * @return
     */
    private boolean bleInit() {
        if (mBluetoothManager == null) {
            mBluetoothManager = (BluetoothManager) ViomiApplication.getContext().getSystemService(Context.BLUETOOTH_SERVICE);
            if (mBluetoothManager == null) {
                Log.e(TAG, "Unable to initialize BluetoothManager.");
                return false;
            }
        }
        mBluetoothAdapter = mBluetoothManager.getAdapter();
        if (mBluetoothAdapter == null) {
            Log.e(TAG, "Unable to obtain a BluetoothAdapter.");
            return false;
        }

        return true;
    }

    /***
     * 蓝牙是否开启
     * @return
     */
    public boolean isBleEnable(){
        return mBluetoothAdapter.isEnabled();
    }
}
