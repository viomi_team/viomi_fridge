package com.viomi.fridge.manager;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;

import com.umeng.analytics.MobclickAgent;
import com.viomi.fridge.common.GlobalParams;

import java.util.HashMap;

/**
 * 埋点统计类
 * Created by young2 on 2017/6/9.
 */

public class StatsManager {
    public static final String EVENT_ID_MODULE_ENTER = "module_enter";//模块进入

    public static final String EVENT_TYPE_PHOTO = "相册";
    public static final String EVENT_TYPE_FRIDGE = "控制";
    public static final String EVENT_TYPE_WEATHER = "天气";
    public static final String EVENT_TYPE_INFORM = "消息中心";
    public static final String EVENT_TYPE_FOOD = "食材管理";
    public static final String EVENT_TYPE_VMALL_MAIN = "商城首页";
    public static final String EVENT_TYPE_MENU = "菜谱";
    public static final String EVENT_TYPE_VIDEO = "视频";
    public static final String EVENT_TYPE_MUSIC = "音乐";
    public static final String EVENT_TYPE_VMALL_DETAIL = "商城详情";
    public static final String EVENT_TYPE_VOICE = "语音";
    public static final String EVENT_TYPE_TIMER = "定时器";
    public static final String EVENT_TYPE_VOLUME = "音量";
    public static final String EVENT_TYPE_BLE = "蓝牙";
    public static final String EVENT_TYPE_WIFI = "wifi";
    public static final String EVENT_TYPE_USER = "用户";
    public static final String EVENT_TYPE_SET = "设置";
    public static final String EVENT_TYPE_FLOAT_EXTEND = "悬浮展开";
    public static final String EVENT_TYPE_FLOAT_CLOSE = "悬浮展开";
    public static final String FLOAT_BUTTON_CLICK_ENENT = "悬浮框点击";

    /***
     * 埋点统计初始化
     * @param context
     */
    public static void init(Context context) {
        if (GlobalParams.STATS_ENABLE) {

            //   使用集成测试之后，所有测试数据不会进入应用正式的统计后台，只能在“管理--集成测试--实时日志”里查看
            //  MobclickAgent.setDebugMode(true);

            // SDK在统计Fragment时，需要关闭Activity自带的页面统计，
            // 然后在每个页面中重新集成页面统计的代码(包括调用了 onResume 和 onPause 的Activity)。
            MobclickAgent.openActivityDurationTrack(false);
            MobclickAgent.setScenarioType(context, MobclickAgent.EScenarioType.E_UM_NORMAL);
        }
    }

    /***
     * 页面访问开始
     */
    public static void recordActivityStart(Activity activity) {
        if (GlobalParams.STATS_ENABLE) {
            MobclickAgent.onResume(activity);
        }
    }

    /***
     * 页面访问结束
     */
    public static void recordActivityEnd(Activity activity) {
        if (GlobalParams.STATS_ENABLE) {
            MobclickAgent.onPause(activity);
        }
    }

    /***
     * 页面访问开始
     */
    public static void recordFragmentStart(Fragment fragment) {
        if (GlobalParams.STATS_ENABLE) {
            MobclickAgent.onPageStart(fragment.getClass().getSimpleName());
        }
    }

    /***
     * 页面访问结束
     */
    public static void recordFragmentPageEnd(Fragment fragment) {
        if (GlobalParams.STATS_ENABLE) {
            MobclickAgent.onPageEnd(fragment.getClass().getSimpleName());
        }
    }

    /***
     * 计数类型事件
     * @param context
     * @param eventId
     * @param type
     * @param attr
     */
    public static void recordCountEvent(Context context, String eventId, String type, String attr) {
        if (GlobalParams.STATS_ENABLE) {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("type", type);
            map.put("attr", attr);
            MobclickAgent.onEvent(context, eventId, map);
        }
    }

    /***
     * 计数类型事件
     * @param context
     * @param eventId
     * @param type
     * @param attr
     */
    public static void recordCalculateEvent(Context context, String eventId, String type, String attr, int digit) {
        if (GlobalParams.STATS_ENABLE) {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("type", type);
            map.put("attr", attr);
            MobclickAgent.onEventValue(context, eventId, map, digit);
        }
    }

    /***
     * 登陆账号统计
     * @param userId
     */
    public static void onProfileSignIn(String userId) {
        MobclickAgent.onProfileSignIn(userId);
    }

    public static final String USER_ID_VIOMI = "viomi";
    public static final String USER_ID_XIAOMI = "xiaomi";


    /***
     * 登陆账号统计
     * String type 账号类型
     * @param userId id
     */
    public static void onProfileSignIn(String type, String userId) {
        MobclickAgent.onProfileSignIn(type, userId);
    }


    /***
     * 账号注销
     */
    public static void onProfileSignOff() {
        MobclickAgent.onProfileSignOff();
    }


    /***
     * 上报自定义错误
     * @param context
     * @param error
     */
    private void reportError(Context context, String error) {
        MobclickAgent.reportError(context, error);
    }

}
