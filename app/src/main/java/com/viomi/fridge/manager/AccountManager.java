package com.viomi.fridge.manager;

import android.content.Context;
import android.util.Log;

import com.miot.common.people.People;
import com.miot.common.people.PeopleFactory;
import com.viomi.fridge.common.ViomiApplication;
import com.viomi.fridge.device.DeviceManager;
import com.viomi.fridge.model.bean.ViomiUser;
import com.viomi.fridge.util.FileUtil;
import com.viomi.fridge.util.log;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by young2 on 2016/9/7.
 */
public class AccountManager {
    public final static String ViomiUserFile="ViomiUser.dat";

    public static final int BIND_NOT_YET=0;//未绑定
    public static final int BIND_BY_VIOMI=1;//由云米app绑定
    public static final int BIND_BY_MIJIA=2;//由米家app绑定


    /***
     * 账号绑定的类型
     * @return
     */
    public static int getBindStatus(){
        if(DeviceManager.getInstance().isDeviceBind()){
            ViomiUser viomiUser=AccountManager.getViomiUser(ViomiApplication.getContext());
            if(viomiUser!=null){
                return BIND_BY_VIOMI;
            } else {
                return BIND_BY_MIJIA;
            }
        }else {
            return BIND_NOT_YET;
        }
    }


    public static void saveViomiUser(Context context, ViomiUser user){
        FileUtil.saveObject(context,ViomiUserFile,user);
    }

    /***
     * 获取云米app授权账号信息
     * @param context
     * @return
     */
    public static ViomiUser getViomiUser(Context context){
        ViomiUser user= (ViomiUser) FileUtil.getObject(context,ViomiUserFile);
        if(user!=null){
            user.setToken(formatNull(user.getToken()));
            user.setMiId(formatNull(user.getMiId()));
            user.setHeadImg(formatNull(user.getHeadImg()));
            user.setAccount(formatNull(user.getAccount()));
            user.setUserCode(formatNull(user.getUserCode()));
            user.setMobile(formatNull(user.getMobile()));
            user.setNickname(formatNull(user.getNickname()));
        }

        return user;
    }

    public static void deleteViomiUser(Context context){
        FileUtil.saveObject(context,ViomiUserFile,null);
    }

    public static ViomiUser parserViomiUser(String json){

        try {
            ViomiUser viomiUser=new ViomiUser();
            JSONObject jsonObject=new JSONObject(json);
            JSONObject xiaomiJson=jsonObject.getJSONObject("xiaomi");
            viomiUser.setMiId(xiaomiJson.getString("miId"));
            viomiUser.setType(xiaomiJson.getString("type"));
            viomiUser.setNickname(jsonObject.optString("nikeName"));
            viomiUser.setHeadImg(jsonObject.optString("headImg"));
            viomiUser.setAccount(jsonObject.getString("account"));
            viomiUser.setMobile(jsonObject.optString("mobile"));
            viomiUser.setCid(jsonObject.optInt("cid"));
            viomiUser.setGender(jsonObject.optInt("gender"));
            return viomiUser;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

    }
    public static People parserMiPeople(String json){

        try {
            ViomiUser viomiUser=new ViomiUser();
            JSONObject jsonObject=new JSONObject(json);
            JSONObject xiaomiJson=jsonObject.getJSONObject("xiaomi");

            String userId =xiaomiJson.getString("miId");
            String type =xiaomiJson.getString("type");
            String accessToken=xiaomiJson.getString("accessToken");
            long mExpiresIn=xiaomiJson.getLong("mExpiresIn");
            String macKey=xiaomiJson.getString("macKey");
            String macAlgorithm=xiaomiJson.getString("macAlgorithm");
//            Log.e("@@@@","accessToken="+accessToken+",userId="+userId+",mExpiresIn="+mExpiresIn+",macKey="
//            +macKey+",macAlgorithm="+macAlgorithm);
            if(type!=null&&((type.equals("ios"))||(type.equals("android_type")))){
                mExpiresIn=mExpiresIn-System.currentTimeMillis()/1000;
            }
           log.d("@@@@","expiresIn="+mExpiresIn);
            return PeopleFactory.createOauthPeople(accessToken, userId, mExpiresIn, macKey, macAlgorithm);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }



    private static String formatNull(String input){
        if(input==null||input.equals("null")){
            return null;
        }
        return input;
    }
}
