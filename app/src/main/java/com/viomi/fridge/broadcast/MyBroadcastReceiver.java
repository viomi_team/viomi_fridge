package com.viomi.fridge.broadcast;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.viomi.common.callback.AppCallback;
import com.viomi.fridge.broadcast.BroadcastAction;
import com.viomi.fridge.common.ViomiApplication;
import com.viomi.fridge.device.DeviceManager;
import com.viomi.fridge.manager.AccountManager;
import com.viomi.fridge.model.bean.ViomiUser;
import com.viomi.fridge.service.TimeSyncService;
import com.viomi.fridge.view.activity.WelcomeActivity;

import java.net.URL;
import java.net.URLConnection;


/**
 * Created by young2 on 2017/1/9.
 */

public class MyBroadcastReceiver extends android.content.BroadcastReceiver{
    private final static String TAG=MyBroadcastReceiver.class.getSimpleName();
    private static final String action_boot ="android.intent.action.BOOT_COMPLETED";
    private static final String action_replace ="android.intent.action.PACKAGE_REPLACED";

    @Override
    public void onReceive(Context context, Intent intent) {

        Log.i(TAG,"@@@@@@@@@@@@@@@@@@@@@@="+intent.getAction());
        if(intent.getAction().equals(action_boot)){
            Intent bootStartIntent = new Intent(context, WelcomeActivity.class);
            bootStartIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(bootStartIntent);
        }
        else if(intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION)){//wifi连接上与否

            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mobNetInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            NetworkInfo wifiNetInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            try{
                if ( !wifiNetInfo.isConnected()) {
                    Log.i(TAG,"wifi disconnect ");
                    // 无网操作
                } else {
                    Log.i(TAG,"wifi connect ");




                    // 有网操作
//                    ViomiUser viomiUser= AccountManager.getViomiUser(ViomiApplication.getContext());
//                    if(viomiUser!=null&&viomiUser.getMiId()!=null){
//                        DeviceManager.getInstance().stopBindDevice(ViomiApplication.getContext());
//                        DeviceManager.getInstance().startBindDevice( viomiUser.getMiId(), new AppCallback<String>() {
//                            @Override
//                            public void onSuccess(String data) {
//                                Log.i(TAG,"bind success!");
//                            }
//
//                            @Override
//                            public void onFail(int errorCode, String msg) {
//                                Log.e(TAG,"bind fail!errorCode="+errorCode+",msg="+msg);
//                            }
//                        });
//                    }

                    Intent locationIntent = new Intent(BroadcastAction.ACTION_LOCATION);
                    LocalBroadcastManager.getInstance(context).sendBroadcast(locationIntent);
                }
            }catch (Exception e){
                e.printStackTrace();
            }

//            Log.d(TAG,"network statue change");
//            NetworkInfo info = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
//            if(info.getState().equals(NetworkInfo.State.DISCONNECTED)){
//                Log.d(TAG,"wifi disconnect");
//            }
//            else if(info.getState().equals(NetworkInfo.State.CONNECTED)){
//                WifiManager wifiManager = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);
//                WifiInfo wifiInfo = wifiManager.getConnectionInfo();
//                Log.d(TAG,"wifi connect ,ssid=" + wifiInfo.getSSID());
//                Intent locationIntent = new Intent(BroadcastAction.ACTION_LOCATION);
//                LocalBroadcastManager.getInstance(context).sendBroadcast(locationIntent);
//            }
        }else if(intent.getAction().equals(action_replace)){//静默升级完成
//            Intent intent2 = new Intent(context, WelcomeActivity.class);
//            intent2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            context.startActivity(intent2);
        }

    }


}
