package com.viomi.fridge.view.widget;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.miot.common.people.People;
import com.viomi.common.module.okhttp.OkHttpClientManager;
import com.viomi.common.widget.SwitchButton;
import com.viomi.fridge.R;
import com.viomi.fridge.api.http.HttpConnect;
import com.viomi.fridge.util.JsonUitls;
import com.viomi.fridge.util.log;
import com.wx.wheelview.adapter.ArrayWheelAdapter;
import com.wx.wheelview.widget.WheelView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;

/**
 * 烟机设置 Dialog
 * Created by William on 2017/5/5.
 */

public class HoodSettingDialog extends BaseDialog {

    private static final String TAG = HoodSettingDialog.class.getSimpleName();
    private ProgressBar mProgressBar;
    private ScrollView mScrollView;
    private String deviceId, str;
    private int cruise_state, light_sync_state, minute, second;
    private People people;
    private final MyHandler mHandler = new MyHandler(this);
    private Context context;
    private SwitchButton btnCruise, btnLight;
    private TextView tvTime;
    private ExpandLayout mExpandLayout;
    private WheelView minWheelView, secWheelView;

    public HoodSettingDialog(Context context, String deviceId, People people) {
        super(context);
        this.context = context;
        this.deviceId = deviceId;
        this.people = people;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (this.getWindow() != null) {
            WindowManager.LayoutParams lp = this.getWindow().getAttributes();
            lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.gravity = Gravity.CENTER;
            this.getWindow().setAttributes(lp);
        }
        setCanceledOnTouchOutside(false);

        initView();

        initData();
    }

    @Override
    public void setView() {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_hood_setting);
    }

    private void initView() {
        mProgressBar = (ProgressBar) findViewById(R.id.hood_setting_progress);
        mScrollView = (ScrollView) findViewById(R.id.hood_setting_content);
        btnCruise = (SwitchButton) findViewById(R.id.hood_setting_boost);
        btnLight = (SwitchButton) findViewById(R.id.hood_setting_light);
        tvTime = (TextView) findViewById(R.id.hood_setting_time);
        minWheelView = (WheelView) findViewById(R.id.hood_setting_minute);
        secWheelView = (WheelView) findViewById(R.id.hood_setting_second);
        mExpandLayout = (ExpandLayout) findViewById(R.id.hood_setting_expand);
        mExpandLayout.initExpand(false);

        initWheelView();

        initListener();
    }

    private void initWheelView() {
        minWheelView.setWheelData(createMinute());
        minWheelView.setWheelAdapter(new ArrayWheelAdapter(context));
        minWheelView.setSkin(WheelView.Skin.None);  // 皮肤
        minWheelView.setLoop(true); // 循环滚动
        minWheelView.setWheelSize(5);
        WheelView.WheelViewStyle minStyle = new WheelView.WheelViewStyle();
        minStyle.selectedTextColor = Color.parseColor("#09A3A1");
        minStyle.textColor = Color.parseColor("#BFBFBF");
        minStyle.textSize = 20;
        minStyle.selectedTextSize = 30;
        minStyle.backgroundColor = Color.parseColor("#00ffffff");
        minStyle.holoBorderColor = Color.parseColor("#09A3A1");
        minStyle.holoBorderWidth = 1;
        minWheelView.setStyle(minStyle);
        minWheelView.setExtraText("分钟", Color.parseColor("#09A3A1"), 20, 60);

        secWheelView.setWheelData(createSecond());
        secWheelView.setWheelAdapter(new ArrayWheelAdapter(context));
        secWheelView.setSkin(WheelView.Skin.None);  // 皮肤
        secWheelView.setLoop(true); // 循环滚动
        secWheelView.setWheelSize(5);
        WheelView.WheelViewStyle secStyle = new WheelView.WheelViewStyle();
        secStyle.selectedTextColor = Color.parseColor("#09A3A1");
        secStyle.textColor = Color.parseColor("#BFBFBF");
        secStyle.textSize = 20;
        secStyle.selectedTextSize = 30;
        secStyle.backgroundColor = Color.parseColor("#00ffffff");
        secStyle.holoBorderColor = Color.parseColor("#dbdbdb");
        secWheelView.setStyle(secStyle);
        secWheelView.setExtraText("秒", Color.parseColor("#09A3A1"), 20, 60);
    }

    private void initListener() {
        // 关闭
        findViewById(R.id.hood_setting_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        // 设置时间
        tvTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mExpandLayout.toggleExpand();
            }
        });

        // 巡航增压设置
        btnCruise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cruise_state == 0) setCruise(1);
                else setCruise(0);
            }
        });
        btnCruise.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

            }
        });

        // 关机是否关灯
        btnLight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (light_sync_state == 0) setLight(1);
                else setLight(0);
            }
        });
        btnLight.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

            }
        });

        minWheelView.setOnWheelItemSelectedListener(new WheelView.OnWheelItemSelectedListener() {
            @Override
            public void onItemSelected(int position, Object o) {
                minute = Integer.parseInt(String.valueOf(o));
                minute = minute * 60;
            }
        });

        secWheelView.setOnWheelItemSelectedListener(new WheelView.OnWheelItemSelectedListener() {
            @Override
            public void onItemSelected(int position, Object o) {
                second = Integer.parseInt(String.valueOf(o));
            }
        });

        // 设置关机延时时间
        findViewById(R.id.hood_setting_set).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                str = tvTime.getText().toString();
                tvTime.setText("设置中...");
                mExpandLayout.toggleExpand();
                setDelay(minute + second);
            }
        });
    }

    private List<String> createMinute() {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < 3; i++) list.add("" + i);
        return list;
    }

    private List<String> createSecond() {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < 60; i++) list.add("" + i);
        return list;
    }

    private void initData() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "get_prop");
            jsonObject.put("did", deviceId);
            jsonObject.put("id", 7);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put("curise_state");
            jsonArray.put("poweroff_delaytime");
            jsonArray.put("light_sync_state");
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        HttpConnect.rpcDevice(people, deviceId, jsonObject.toString(), new OkHttpClientManager.ResultCallback<String>() {
            @Override
            public void onError(Call call, Exception e) {
                Message msg = mHandler.obtainMessage();
                msg.what = 0;
                mHandler.sendMessage(msg);
            }

            @Override
            public void onResponse(String response) {
                Message msg = mHandler.obtainMessage();
                msg.what = 1;
                msg.obj = response;
                mHandler.sendMessage(msg);
            }
        });
    }

    // 巡航增压开关
    private void setCruise(int cruise) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_curise");
            jsonObject.put("did", deviceId);
            jsonObject.put("id", 1);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(cruise);
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        HttpConnect.rpcDevice(people, deviceId, jsonObject.toString(), new OkHttpClientManager.ResultCallback<String>() {
            @Override
            public void onError(Call call, Exception e) {
                Message msg = mHandler.obtainMessage();
                msg.what = 2;
                mHandler.sendMessage(msg);
            }

            @Override
            public void onResponse(String response) {
                log.d(TAG, response);
                Message msg = mHandler.obtainMessage();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String code = jsonObject.optString("code");
                    if (code.equals("0")) msg.what = 3;
                    else msg.what = 2;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                msg.obj = response;
                mHandler.sendMessage(msg);
            }
        });
    }

    // 关机是否关灯
    private void setLight(int light) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_light_sync");
            jsonObject.put("did", deviceId);
            jsonObject.put("id", 1);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(light);
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        HttpConnect.rpcDevice(people, deviceId, jsonObject.toString(), new OkHttpClientManager.ResultCallback<String>() {
            @Override
            public void onError(Call call, Exception e) {
                Message msg = mHandler.obtainMessage();
                msg.what = 4;
                mHandler.sendMessage(msg);
            }

            @Override
            public void onResponse(String response) {
                log.d(TAG, response);
                Message msg = mHandler.obtainMessage();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String code = jsonObject.optString("code");
                    if (code.equals("0")) msg.what = 3;
                    else msg.what = 4;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                msg.obj = response;
                mHandler.sendMessage(msg);
            }
        });
    }

    // 设置延时时间
    private void setDelay(int time) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_poweroff_delaytime");
            jsonObject.put("did", deviceId);
            jsonObject.put("id", 1);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(time);
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        HttpConnect.rpcDevice(people, deviceId, jsonObject.toString(), new OkHttpClientManager.ResultCallback<String>() {
            @Override
            public void onError(Call call, Exception e) {
                Message msg = mHandler.obtainMessage();
                msg.what = 5;
                mHandler.sendMessage(msg);
            }

            @Override
            public void onResponse(String response) {
                log.d(TAG, response);
                Message msg = mHandler.obtainMessage();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String code = jsonObject.optString("code");
                    if (code.equals("0")) msg.what = 6;
                    else msg.what = 5;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                msg.obj = response;
                mHandler.sendMessage(msg);
            }
        });
    }

    private static class MyHandler extends Handler {

        WeakReference<HoodSettingDialog> weakReference;

        public MyHandler(HoodSettingDialog dialog) {
            this.weakReference = new WeakReference<>(dialog);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            HoodSettingDialog dialog = weakReference.get();
            switch (msg.what) {
                case 0: // 请求失败
                    Toast.makeText(dialog.context, dialog.context.getResources().getString(R.string.toast_http_result_error), Toast.LENGTH_SHORT).show();
                    log.myE(TAG, "request-------fail-------");
                    break;
                case 1:
                    String result = (String) msg.obj;
                    log.d(TAG, result);
                    try {
                        JSONObject json = new JSONObject(result);
                        String code = JsonUitls.getString(json, "code");
                        if ("0".equals(code)) {
                            JSONArray resultArray = JsonUitls.getJSONArray(json, "result");
                            dialog.mProgressBar.setVisibility(View.GONE);
                            dialog.mScrollView.setVisibility(View.VISIBLE);
                            dialog.cruise_state = resultArray.optInt(0);
                            int powerOff_DelayTime = resultArray.optInt(1);
                            dialog.light_sync_state = resultArray.optInt(2);

                            // 巡航增压
                            dialog.btnCruise.setChecked(dialog.cruise_state != 0);
                            // 延时关机
                            dialog.tvTime.setText(dialog.timeFormat(powerOff_DelayTime));
                            // 关机是否关灯
                            dialog.btnLight.setChecked(dialog.light_sync_state != 0);
                        } else {
                            Toast.makeText(dialog.context, "获取数据失败", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case 2: // 巡航设置失败
                    Toast.makeText(dialog.context, "设置失败", Toast.LENGTH_SHORT).show();
                    dialog.btnCruise.setChecked(!dialog.btnCruise.isChecked());
                    break;
                case 3: // 设置成功
                    dialog.cruise_state = dialog.btnCruise.isChecked() ? 1 : 0;
                    dialog.light_sync_state = dialog.btnLight.isChecked() ? 1 : 0;
                    Toast.makeText(dialog.context, "设置成功", Toast.LENGTH_SHORT).show();
                    break;
                case 4: // 关机灯光设置失败
                    Toast.makeText(dialog.context, "设置失败", Toast.LENGTH_SHORT).show();
                    dialog.btnLight.setChecked(!dialog.btnLight.isChecked());
                    break;
                case 5: // 延时设置失败
                    Toast.makeText(dialog.context, "设置失败", Toast.LENGTH_SHORT).show();
                    dialog.tvTime.setText(dialog.str);
                    break;
                case 6: // 延时设置成功
                    Toast.makeText(dialog.context, "设置成功", Toast.LENGTH_SHORT).show();
                    dialog.tvTime.setText(dialog.timeFormat(dialog.minute + dialog.second));
                    break;
            }
        }
    }

    private String timeFormat(int time) {
        String str;
        int minute = time / 60;
        int second = time % 60;
        if (time == 0) str = "请选择";
        else str = minute + "分钟" + second + "秒";
        return str;
    }

}
