package com.viomi.fridge.view.adapter.listener;

import android.view.View;

/**
 * Created by clevo on 2015/7/30.
 */
public interface RecycleItemClickListener {

    void onItemClick(View view, int position);
}
