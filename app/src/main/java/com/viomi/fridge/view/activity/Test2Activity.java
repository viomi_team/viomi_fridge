package com.viomi.fridge.view.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.Button;

import com.viomi.fridge.R;

public class Test2Activity extends Activity {
    private WebView webView;
    private Button btn;

    @SuppressLint("JavascriptInterface")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test2);
        webView = (WebView) findViewById(R.id.webview);
        btn = (Button) findViewById(R.id.btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                webView.post(new Runnable() {
                    @Override
                    public void run() {
                        webView.loadUrl("javascript:androidCallJs()");
                    }
                });
            }
        });
        initWebView();
    }

    @SuppressLint("JavascriptInterface")
    private void initWebView() {
//        webView.setWebChromeClient(new WebChromeClient());
        webView.getSettings().setJavaScriptEnabled(true);
//        webView.addJavascriptInterface(new JsInterface(), "wv");
        webView.addJavascriptInterface(this, "wv");
        webView.loadUrl("file:///android_asset/js_webView.html");
    }

    @JavascriptInterface
    public void sayHello(String msg) {
        Log.i("info", "=============msg:" + msg);
    }
//    class JsInterface {
//        public void sayHello(String msg) {
//            Log.i("info", "=============msg:" + msg);
//        }
//    }
}
