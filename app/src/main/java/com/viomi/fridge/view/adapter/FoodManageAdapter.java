package com.viomi.fridge.view.adapter;

import android.content.Context;
import android.support.annotation.IntDef;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.viomi.fridge.R;
import com.viomi.fridge.model.bean.FoodDetailData;
import com.viomi.fridge.util.ImgUtil;
import com.viomi.fridge.util.log;
import com.viomi.fridge.view.widget.RoundCornerImageView;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.List;

/**
 * 食材管理适配器
 * Created by William on 2017/6/22.
 */

public class FoodManageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = FoodManageAdapter.class.getSimpleName();
    private static final int VIEW_MAIN = 1;
    private static final int VIEW_NOT_MAIN = 2;
    public static final int NORMAL = 3;
    public static final int EDIT = 4;

    @IntDef({NORMAL, EDIT})
    @Retention(RetentionPolicy.SOURCE)
    public @interface Mode {
    }

    private int mMode = NORMAL;

    private List<FoodDetailData> list = new ArrayList<>();
    private Context context;
    private OnFoodClickListener onFoodClickListener;
    private boolean isMain;

    public FoodManageAdapter(Context context, List<FoodDetailData> list, boolean isMain) {
        this.context = context;
        this.list = list;
        this.isMain = isMain;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        if (viewType == VIEW_NOT_MAIN) {
            return new ItemHolder(LayoutInflater.from(context).inflate(R.layout.viewholder_food_manage, viewGroup, false));
        } else {
            return new MainHolder(LayoutInflater.from(context).inflate(R.layout.viewholder_food_manage_main, viewGroup, false));
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemHolder) {
            ItemHolder itemHolder = (ItemHolder) holder;
            setItemHolderData(itemHolder, position);
        } else {
            MainHolder mainHolder = (MainHolder) holder;
            setMainHolderData(mainHolder, position);
        }
    }

    @Override
    public int getItemCount() {
        if (list == null) list = new ArrayList<>();
        if (isMain && list.size() > 4) return 4;
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {
        int viewType;
        if (isMain) {
            viewType = VIEW_MAIN;
        } else {
            viewType = VIEW_NOT_MAIN;
        }
        return viewType;
    }

    // 食材管理页展示
    private void setItemHolderData(ItemHolder itemHolder, int position) {
        FoodDetailData data = list.get(position);
        // 食材名称
        itemHolder.textView.setText(data.name);

        // 进度计算
        int per;
        if (!data.deadLine.equals("可选填")) {
            long deadLine = Long.valueOf(data.deadLine) + 24L * 60L * 60L * 1000L;
            long current = System.currentTimeMillis();
            if (current >= deadLine) {
                per = 0;
                itemHolder.ivOutDate.setVisibility(View.VISIBLE);
                itemHolder.ivFade.setVisibility(View.VISIBLE);
            } else {
                itemHolder.ivOutDate.setVisibility(View.GONE);
                itemHolder.ivFade.setVisibility(View.GONE);

                // 计算进度
                long period = deadLine - current;
                int days = (int) (period / (24 * 60 * 60 * 1000));
                if (days >= 7) per = 100;   // 保质期 7 天以上
                else {
                    per = (int) ((days / 7.0) * 100);
                }
                if (per == 0) per = 10;
                log.d(TAG, per + "");
            }
        } else {
            itemHolder.ivOutDate.setVisibility(View.GONE);
            itemHolder.ivFade.setVisibility(View.GONE);
            per = 100;
        }

        // 进度显示
        itemHolder.progressBar.setProgress(per);

        if (per > 0 && per <= 25) {
            itemHolder.progressBar.setProgressDrawable(ContextCompat.getDrawable(context, R.drawable.food_progress_red));
        } else if (per > 25 && per <= 50) {
            itemHolder.progressBar.setProgressDrawable(ContextCompat.getDrawable(context, R.drawable.food_progress_orange));
        } else if (per > 50 && per <= 75) {
            itemHolder.progressBar.setProgressDrawable(ContextCompat.getDrawable(context, R.drawable.food_progress_yellow));
        } else {
            itemHolder.progressBar.setProgressDrawable(ContextCompat.getDrawable(context, R.drawable.food_progress_green));
        }

        // 显示图标
        ImgUtil.showImage("file:///" + data.imgPath, itemHolder.ivContent);

        if (mMode == NORMAL) itemHolder.ivSelect.setVisibility(View.INVISIBLE);
        else itemHolder.ivSelect.setVisibility(View.VISIBLE);

        if (data.getSelect())
            itemHolder.ivSelect.setImageResource(R.mipmap.icon_food_manage_selected);
        else itemHolder.ivSelect.setImageResource(R.mipmap.icon_food_manage_normal);

        itemHolder.relativeLayout.setOnClickListener(v -> {
            if (onFoodClickListener != null)
                onFoodClickListener.onChose(data, position, mMode);
        });
    }

    // 首页食材展示
    private void setMainHolderData(MainHolder holder, int position) {
        if (position == 3) {
            holder.imageView.setVisibility(View.INVISIBLE);
            holder.moreImageView.setVisibility(View.VISIBLE);
            holder.progressBar.setVisibility(View.INVISIBLE);
        } else {
            holder.progressBar.setVisibility(View.VISIBLE);
            holder.imageView.setVisibility(View.VISIBLE);
            holder.moreImageView.setVisibility(View.GONE);
            FoodDetailData data = list.get(position);

            // 显示图标
            ImgUtil.showImage("file:///" + data.imgPath, holder.imageView);
            // 食材名称
            holder.textView.setText(data.name);

            // 进度计算
            int per;
            if (!data.deadLine.equals("可选填")) {
                long deadLine = Long.valueOf(data.deadLine) + 24L * 60L * 60L * 1000L;
                long current = System.currentTimeMillis();
                if (current >= deadLine) {
                    per = 0;
                    holder.outDateImageView.setVisibility(View.VISIBLE);
                    holder.ivFade.setVisibility(View.VISIBLE);
                } else {
                    holder.outDateImageView.setVisibility(View.GONE);
                    holder.ivFade.setVisibility(View.GONE);
                    // 计算进度
                    long period = deadLine - current;
                    int days = (int) (period / (24 * 60 * 60 * 1000));
                    if (days >= 7) per = 100;   // 保质期 7 天以上
                    else {
                        per = (int) ((days / 7.0) * 100);
                    }
                    if (per == 0) per = 10;
                    log.d(TAG, per + "");
                }
            } else {
                holder.outDateImageView.setVisibility(View.GONE);
                holder.ivFade.setVisibility(View.GONE);
                per = 100;
            }

            // 进度显示
            holder.progressBar.setProgress(per);

            if (per > 0 && per <= 25) {
                holder.progressBar.setProgressDrawable(ContextCompat.getDrawable(context, R.drawable.food_progress_red));
            } else if (per > 25 && per <= 50) {
                holder.progressBar.setProgressDrawable(ContextCompat.getDrawable(context, R.drawable.food_progress_orange));
            } else if (per > 50 && per <= 75) {
                holder.progressBar.setProgressDrawable(ContextCompat.getDrawable(context, R.drawable.food_progress_yellow));
            } else {
                holder.progressBar.setProgressDrawable(ContextCompat.getDrawable(context, R.drawable.food_progress_green));
            }
        }
    }

    // 食材管理页 ViewHolder
    private class ItemHolder extends RecyclerView.ViewHolder {
        RoundCornerImageView ivContent, ivFade;
        ImageView ivOutDate, ivSelect;
        ProgressBar progressBar;
        TextView textView;
        RelativeLayout relativeLayout, rootLayout;

        ItemHolder(View itemView) {
            super(itemView);
            rootLayout = (RelativeLayout) itemView.findViewById(R.id.holder_food_root);
            relativeLayout = (RelativeLayout) itemView.findViewById(R.id.holder_food_layout);
            ivContent = (RoundCornerImageView) itemView.findViewById(R.id.holder_food_img);
            ivFade = (RoundCornerImageView) itemView.findViewById(R.id.holder_food_fade);
            ivOutDate = (ImageView) itemView.findViewById(R.id.holder_food_out_of_date);
            ivSelect = (ImageView) itemView.findViewById(R.id.holder_food_select);
            progressBar = (ProgressBar) itemView.findViewById(R.id.holder_food_progress);
            textView = (TextView) itemView.findViewById(R.id.holder_food_name);
        }
    }

    // 食材管理首页 ViewHolder
    private class MainHolder extends RecyclerView.ViewHolder {
        private ImageView imageView, ivFade;
        private ImageView moreImageView;
        private ImageView outDateImageView;
        private ProgressBar progressBar;
        private TextView textView;

        MainHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.holder_food_main_img);
            ivFade = (ImageView) itemView.findViewById(R.id.holder_food_main_fade);
            moreImageView = (ImageView) itemView.findViewById(R.id.holder_food_main_more);
            outDateImageView = (ImageView) itemView.findViewById(R.id.holder_food_main_out_of_date);
            progressBar = (ProgressBar) itemView.findViewById(R.id.holder_food_main_progress);
            textView = (TextView) itemView.findViewById(R.id.holder_food_main_name);
        }
    }

    public void setOnFoodClickListener(OnFoodClickListener onFoodClickListener) {
        this.onFoodClickListener = onFoodClickListener;
    }

    public interface OnFoodClickListener {
        void onChose(FoodDetailData data, int position, @Mode int mode);
    }

    public void setMode(@Mode int mode) {
        mMode = mode;
    }

}
