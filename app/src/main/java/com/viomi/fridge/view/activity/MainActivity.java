package com.viomi.fridge.view.activity;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.viomi.common.callback.AppCallback;
import com.viomi.fridge.R;
import com.viomi.fridge.albumhttp.AlbumActivity;
import com.viomi.fridge.albumhttp.CoreService;
import com.viomi.fridge.api.http.HttpConnect;
import com.viomi.fridge.broadcast.BroadcastAction;
import com.viomi.fridge.broadcast.ConnectionChangeReceiver;
import com.viomi.fridge.common.GlobalParams;
import com.viomi.fridge.common.ViomiApplication;
import com.viomi.fridge.device.DeviceConfig;
import com.viomi.fridge.manager.BleManager;
import com.viomi.fridge.manager.ControlManager;
import com.viomi.fridge.manager.RoomSceneManager;
import com.viomi.fridge.manager.VoiceManager;
import com.viomi.fridge.model.bean.GoodsEntity;
import com.viomi.fridge.model.bean.HomeWidgetBean;
import com.viomi.fridge.model.bean.TCRoomScene;
import com.viomi.fridge.model.bean.WebBaseData;
import com.viomi.fridge.service.BackgroudService;
import com.viomi.fridge.util.ApkUtil;
import com.viomi.fridge.util.JsonUitls;
import com.viomi.fridge.util.PhoneUtil;
import com.viomi.fridge.util.ResponseCode;
import com.viomi.fridge.util.WeatherIconUtil;
import com.viomi.fridge.util.log;
import com.viomi.fridge.view.widget.AutoScrollViewPager;
import com.viomi.fridge.view.widget.MyClockView;
import com.viomi.fridge.view.widget.SmallRadioView;
import com.viomi.fridge.view.widget.VoiceSettingDialog;
import com.viomi.fridge.wifimodel.WifiScanActivity;
import com.zhuge.analysis.stat.ZhugeSDK;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MainActivity extends BaseActivity {
    private final static String TAG = "home_page";
    private LocalBroadcastManager mBroadcastManager;
    private AutoScrollViewPager viewPager;
    private RelativeLayout fridge_control;
    private TextView tcroom_text;
    private RelativeLayout play_control;
    private RelativeLayout cook_menu;
    private RelativeLayout fridge_store;
    private SmallRadioView samllpoit;

    private TextView temp1;
    private TextView temp2;
    private TextView temp3;
    private MyHandler mHandler;
    private List<HomeWidgetBean> list1;
    private List<HomeWidgetBean> list2;
    private List<List<GoodsEntity>> list3;
    private SimpleDraweeView cook_menu_img;
    private AutoScrollViewPager goods_viewPager;
    private SmallRadioView goods_smallpoint;
    private TextView weather_text;
    private TextView location_text;
    private TextView fresh_text;
    private ImageView info_btn;
    private ImageView talk_btn;
    private ImageView timer_btn;
    private ImageView voice_btn;
    private ImageView network_btn;
    private ImageView setting_btn;
    private RefleshThread refleshThread;
    private int timeCount;
    private RelativeLayout weather_layout;
    private RelativeLayout loading_layout;
    private RelativeLayout volume_layout;
    private RelativeLayout volume_area_layout;
    private SeekBar soundBar;
    private ImageView mBleImageView;
    private AudioManager mAudioManager;
    private LinearLayout music;
    private LinearLayout video;
    private int mainVPsize;
    private int secondVPsize;
    private MyClockView myclockview;
    private ConnectionChangeReceiver mReceiver;

    private ImageView weather_icon;
    private static final int REQUEST_WEATHER_CODE = 1001;
    private static final int RESPONE_WEATHER_CODE = 1002;

    private String mVmallUrl = HttpConnect.STOREMAINURL;
    private BroadcastReceiver fileReceiver;
    private RelativeLayout qrcode_bg;
    private SimpleDraweeView qrcode_img;
    private Button qrcode_ok;
    private boolean testBoolean;
    private Intent albumServiceIntent;
    private Intent floatServiceIntent;
    private Intent backgroudServiceintent;
    private WifiManager wifiManager;
    private int[] wifiIcons;
    private BroadcastReceiver mReceiver3;
    private VoiceSettingDialog mVoiceSettingDialog;


    private static class MyHandler extends Handler {

        private WeakReference<MainActivity> weakReference;

        public MyHandler(MainActivity activity) {
            weakReference = new WeakReference<MainActivity>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            MainActivity mActivity = weakReference.get();
            switch (msg.what) {

                case 0: {//刷新页面，10
                    //刷新变温室
                    int t1 = ControlManager.getInstance().getDataSendInfo().cold_closet_temp_set;
                    String st1 = t1 == -50 ? "-" : t1 + "";
                    mActivity.temp1.setText(st1);
                    int t2 = ControlManager.getInstance().getDataSendInfo().temp_changeable_room_temp_set;
                    String st2 = t2 == -50 ? "-" : t2 + "";
                    mActivity.temp2.setText(st2);
                    String scene = GlobalParams.getInstance().getSceneChoose();
                    ArrayList<TCRoomScene> roomScenes = RoomSceneManager.getInstance().getSceneChooseList();
                    int temp = 100;
                    for (int i = 0; i < roomScenes.size(); i++) {
                        if (roomScenes.get(i).name.equals(scene)) {
                            temp = roomScenes.get(i).value;
                            break;
                        }
                    }
                    if ((!scene.equals(GlobalParams.Value_Scene_ChooseName)) && temp == ControlManager.getInstance().getDataSendInfo().temp_changeable_room_temp_set
                            && ControlManager.getInstance().getDataSendInfo().temp_changeable_room_room_enable) {
                        mActivity.tcroom_text.setText(scene);
                    } else {
                        mActivity.tcroom_text.setText(R.string.text_temp_changeable_room);
                    }
                    mActivity.temp3.setText(ControlManager.getInstance().getDataSendInfo().freezing_room_temp_set + "");
                    //刷新语音图标
                    if (GlobalParams.getInstance().isVoiceEnabe()) {
                        if (VoiceManager.getInstance().isVoiceOccupy()) {
                            mActivity.talk_btn.setImageResource(R.mipmap.vioce_occupy);
                        } else {
                            mActivity.talk_btn.setImageResource(R.mipmap.voice_enable);
                        }
                    } else {
                        mActivity.talk_btn.setImageResource(R.mipmap.voice_inenable);
                    }
                    mActivity.refreVoiceTalkView();
                    mActivity.mHandler.sendEmptyMessageDelayed(0, 10 * 1000);
                    break;
                }
                case 1: {
                    mActivity.loading_layout.setVisibility(View.GONE);
                    String result = (String) msg.obj;
                    log.myE(TAG + "ok", result + "");
                    mActivity.parseJson(result);
                    break;
                }
                case 2: {
                    mActivity.loading_layout.setVisibility(View.GONE);
                    String result = msg.obj.toString();
                    log.myE(TAG + "fail", result + "");
                    break;
                }

                case 3: {
//                    BDLocation bdLocation = (BDLocation) msg.obj;
//
//                    if (bdLocation != null) {
//                        String cityName = bdLocation.getCity() == null ? "" : ("null".equals(bdLocation.getCity()) ? "" : bdLocation.getCity());
//                        log.myE(TAG + "cityName=", cityName + ",cityCode=" + bdLocation.getCityCode() + ",-----------------------------------------------------华丽丽的--------------------------------------------------");
//                        mActivity.getCityWeather(cityName);
//                        String cityCode = bdLocation.getCityCode();
//                        if (cityCode == null) {
//                            cityCode = "";
//                        }
//
//                        GlobalParams.getInstance().setLocationCityCode(cityCode);
//                        GlobalParams.getInstance().setLocationCityName(cityName);
//                    }
                    break;
                }

                case 4: {
                    String result = (String) msg.obj;
                    mActivity.parseWeatherJson(result);
                    break;
                }

                case 5: {
                    if (mActivity.timeCount > 0) {

                        if (mActivity.timeCount % 24 == 0) {
                            mActivity.loadData();
                        }

                        if (mActivity.timeCount % 4 == 0) {
                            mActivity.loation();
                        }
                    }
                    mActivity.timeCount++;
                    break;
                }
            }
        }
    }

    public static class RefleshThread extends Thread {
        private WeakReference<MainActivity> weakReference;
        private MainActivity mActivity;

        public RefleshThread(MainActivity activity) {
            weakReference = new WeakReference<MainActivity>(activity);
            mActivity = weakReference.get();
        }

        @Override
        public void run() {
            while (!isInterrupted()) {
                mActivity.mHandler.sendEmptyMessage(5);
                SystemClock.sleep(60 * 60 * 1000);
            }
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        weather_layout = (RelativeLayout) findViewById(R.id.weather_layout);
        weather_icon = (ImageView) findViewById(R.id.weather_icon);
        weather_text = (TextView) findViewById(R.id.weather_text);
        location_text = (TextView) findViewById(R.id.location_text);
        fresh_text = (TextView) findViewById(R.id.fresh_text);

        info_btn = (ImageView) findViewById(R.id.info_btn);
        timer_btn = (ImageView) findViewById(R.id.timer_btn);
        talk_btn = (ImageView) findViewById(R.id.talk_btn);
        voice_btn = (ImageView) findViewById(R.id.voice_btn);
        network_btn = (ImageView) findViewById(R.id.network_btn);
        setting_btn = (ImageView) findViewById(R.id.setting_btn);

        viewPager = (AutoScrollViewPager) findViewById(R.id.viewPager);
        samllpoit = (SmallRadioView) findViewById(R.id.samllpoit);
        fridge_control = (RelativeLayout) findViewById(R.id.fridge_control);
        tcroom_text = (TextView) findViewById(R.id.tcroom_text);
        play_control = (RelativeLayout) findViewById(R.id.play_control);
        music = (LinearLayout) findViewById(R.id.music);
        video = (LinearLayout) findViewById(R.id.video);
        cook_menu = (RelativeLayout) findViewById(R.id.cook_menu);
        fridge_store = (RelativeLayout) findViewById(R.id.fridge_store);

        temp1 = (TextView) findViewById(R.id.temp1);
        temp2 = (TextView) findViewById(R.id.temp2);
        temp3 = (TextView) findViewById(R.id.temp3);

        cook_menu_img = (SimpleDraweeView) findViewById(R.id.cook_menu_img);
        goods_viewPager = (AutoScrollViewPager) findViewById(R.id.goods_viewPager);
        goods_smallpoint = (SmallRadioView) findViewById(R.id.goods_smallpoint);

        loading_layout = (RelativeLayout) findViewById(R.id.loading_layout);

        volume_layout = (RelativeLayout) findViewById(R.id.volume_layout);
        volume_area_layout = (RelativeLayout) findViewById(R.id.volume_area_layout);
        soundBar = (SeekBar) findViewById(R.id.soundBar);
        myclockview = (MyClockView) findViewById(R.id.myclockview);
//        mBleImageView= (ImageView) findViewById(R.id.icon_ble);

        qrcode_bg = (RelativeLayout) findViewById(R.id.qrcode_bg);
        qrcode_img = (SimpleDraweeView) findViewById(R.id.qrcode_img);
        qrcode_ok = (Button) findViewById(R.id.qrcode_ok);

        init();
        initListener();
        setdefuat();
        mHandler = new MyHandler(this);
        mHandler.sendEmptyMessage(0);
        refleshThread = new RefleshThread(this);
        refleshThread.start();
        registerReceiver();
        initService();
    }

    private void initService() {
        backgroudServiceintent = new Intent(this, BackgroudService.class);
        startService(backgroudServiceintent);
        albumServiceIntent = new Intent(this, CoreService.class);
        startService(albumServiceIntent);
//        floatServiceIntent = new Intent(this, FloatButtonService.class);
//        startService(floatServiceIntent);
    }

    //首次打开，无网络时的默认图
    private void setdefuat() {
        int[] mainVPimgs = new int[]{R.drawable.homepage_defuat_x5, R.drawable.homepage_defuat_v1};
        mainVPsize = mainVPimgs.length;
        List<SimpleDraweeView> imgList = new ArrayList<>();
        for (int i = 0; i < mainVPimgs.length * 2; i++) {
            SimpleDraweeView imageView = new SimpleDraweeView(this);
            imageView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setImageResource(mainVPimgs[i % mainVPimgs.length]);
            imgList.add(imageView);
        }
        viewPager.setCustomAdapterDatas(imgList);
        viewPager.startAutoScroll();
        samllpoit.setCount(mainVPsize);
        samllpoit.setIndex(0);


        int[][] goodsImgs = new int[][]{{R.drawable.g00_6999_v1, R.drawable.g01_4999_v1, R.drawable.g02_3999_c1}, {R.drawable.g10_2999_c1, R.drawable.g11_2999_s1, R.drawable.g12_2499_s1}};
        String[][] goodsNames = new String[][]{{"V1 尊享版", "V1 乐享版", "C1 厨下版"}, {"C1 厨上版", "S1 优享版", "S1 标准版"}};
        String[][] goodsPrice = new String[][]{{"¥6999", "¥4999", "¥3999"}, {"¥2999", "¥2999", "¥2499"}};

        secondVPsize = goodsImgs.length;
        List<LinearLayout> defuatList = new ArrayList<>();
        for (int i = 0; i < goodsImgs.length * 2; i++) {

            LinearLayout linearLayout = new LinearLayout(this);
            linearLayout.setLayoutParams(new LinearLayout.LayoutParams((int) PhoneUtil.pxTodip(this, 360), (int) PhoneUtil.pxTodip(this, 150)));
            linearLayout.setOrientation(LinearLayout.HORIZONTAL);
            for (int j = 0; j < goodsImgs[i % goodsImgs.length].length; j++) {
                View view = LayoutInflater.from(this).inflate(R.layout.goods_viewpager_item, null);
                SimpleDraweeView goods_img = (SimpleDraweeView) view.findViewById(R.id.goods_img);
                TextView name = (TextView) view.findViewById(R.id.name);
                TextView price = (TextView) view.findViewById(R.id.price);
                goods_img.setImageResource(goodsImgs[i % goodsImgs.length][j]);
                goods_img.setScaleType(ImageView.ScaleType.CENTER_CROP);
                name.setText(goodsNames[i % goodsImgs.length][j]);
                price.setText(goodsPrice[i % goodsImgs.length][j]);
                linearLayout.addView(view);
            }
            defuatList.add(linearLayout);
        }

        goods_viewPager.setCustomAdapterDatas(defuatList);
        goods_viewPager.startAutoScroll();
        goods_smallpoint.setCount(secondVPsize);
        goods_smallpoint.setIndex(0);
    }

    private void loadData() {
        loading_layout.setVisibility(View.VISIBLE);
        Map<String, String> map = new HashMap<>();
        map.put("layoutType", "1");
        HttpConnect.getRequestHandler(HttpConnect.HOME_PAGE_NEW, map, mHandler, 1, 2);
    }

    private void parseJson(String result) {

        list1 = new ArrayList<>();
        list2 = new ArrayList<>();
        list3 = new ArrayList<>();

        try {
            JSONObject json = new JSONObject(result);
            JSONObject mobBaseRes = JsonUitls.getJSONObject(json, "mobBaseRes");
            String code = JsonUitls.getString(mobBaseRes, "code");
            String desc = JsonUitls.getString(mobBaseRes, "desc");

            if (ResponseCode.isSuccess2(code, desc)) {

                JSONObject jsonResult = JsonUitls.getJSONObject(mobBaseRes, "result");
                JSONArray widgets = JsonUitls.getJSONArray(jsonResult, "widgets");

                for (int i = 0; i < widgets.length(); i++) {
                    JSONObject item = widgets.getJSONObject(i);
                    int widgetType = JsonUitls.getInt(item, "widgetType");

                    switch (widgetType) {
                        case 2: {
                            //主轮播图
                            parseBean1(list1, item);
                            break;
                        }
                        case 4: {
                            //菜谱
                            parseBean2(list2, item);
                            break;
                        }
                        case 5: {
                            //二级轮播图
                            parseBean3(list3, item);
                            break;
                        }
                        default:
                            break;
                    }
                }
                setData();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void parseBean1(List<HomeWidgetBean> list, JSONObject item) throws JSONException {
        JSONArray children = JsonUitls.getJSONArray(item, "children");
        for (int j = 0; j < children.length(); j++) {
            JSONObject data = children.getJSONObject(j);
            int idx = JsonUitls.getInt(data, "idx");
            JSONObject attrs = JsonUitls.getJSONObject(data, "attrs");
            String linkUrl = JsonUitls.getString(attrs, "link");
            String imageUrl = JsonUitls.getString(attrs, "img");
            String type = JsonUitls.getString(attrs, "type");
            list.add(new HomeWidgetBean(linkUrl, imageUrl, type, idx));
        }
        sortArray(list);
    }

    private List<HomeWidgetBean> sortArray(List<HomeWidgetBean> dataList) {
        for (int i = 0; i < dataList.size() - 1; i++) {
            for (int j = 0; j < dataList.size() - 1 - i; j++) {
                if (dataList.get(j).getIdx() > dataList.get(j + 1).getIdx()) {
                    HomeWidgetBean temp1 = dataList.get(j);
                    HomeWidgetBean temp2 = dataList.get(j + 1);
                    dataList.set(j, temp2);
                    dataList.set(j + 1, temp1);
                }
            }
        }
        return dataList;
    }

    private void parseBean2(List<HomeWidgetBean> list, JSONObject item) throws JSONException {
        JSONObject attrs = JsonUitls.getJSONObject(item, "attrs");
        String linkUrl = JsonUitls.getString(attrs, "link");
        String imageUrl = JsonUitls.getString(attrs, "img");
        if (!"-".equals(linkUrl)) {
            list.add(new HomeWidgetBean(linkUrl, imageUrl));
        }
    }

    private void parseBean3(List<List<GoodsEntity>> list, JSONObject item) throws JSONException {
        JSONArray children = JsonUitls.getJSONArray(item, "children");
        for (int i = 0; i < children.length(); i++) {
            JSONObject data = children.getJSONObject(i);
            JSONArray children_sec = JsonUitls.getJSONArray(data, "children");
            List<GoodsEntity> beanList = new ArrayList<>();
            for (int j = 0; j < children_sec.length(); j++) {
                JSONObject data2 = children_sec.getJSONObject(j);
                JSONObject attrs = JsonUitls.getJSONObject(data2, "attrs");
                String name = JsonUitls.getString(attrs, "name");
                String imageUrl = JsonUitls.getString(attrs, "img");
                double price = JsonUitls.getDouble(attrs, "prod_real_price");

                String sku_id = JsonUitls.getString(attrs, "sku_id");
                String spu_id = JsonUitls.getString(attrs, "spu_id");

                String targetId = "";
                int targetType = 0;
                if (!"-".equals(sku_id)) {
                    targetType = 2;
                    targetId = sku_id;
                } else {
                    if (!"-".equals(spu_id)) {
                        targetType = 1;
                        targetId = spu_id;
                    }
                }

                beanList.add(new GoodsEntity(name, imageUrl, price, targetId, targetType));
            }
            list.add(beanList);
        }
    }

    private void setData() {
        setViewPager(getTimes(list1));
        if (list2.size() > 0) {
            Uri uri = Uri.parse(list2.get(0).getImageUrl());
            cook_menu_img.setImageURI(uri);
            GlobalParams.Menu_Url = list2.get(0).getLinkUrl();
            cook_menu_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (list2.size() > 0) {
//                        Intent intent = new Intent(MainActivity.this, CookMenuActivity.class);
//                        intent.putExtra("url", list2.get(0).getLinkUrl());
//                        startActivity(intent);
                    } else {
                    }
                }
            });
        }

        setGoodsViewPager(getTimes(list3));
    }


    private int getTimes(List list) {
        int times = 0;
        switch (list.size()) {
            case 0:
                break;
            case 1:
                times = 4;
                break;
            case 2:
                times = 2;
                break;
            default:
                times = 1;
                break;
        }
        return times;
    }

    //设置主轮播图数据
    private void setViewPager(int times) {
        if (times == 0) {
            return;
        }

        mainVPsize = list1.size();
        List<SimpleDraweeView> imgList = new ArrayList<>();
        for (int i = 0; i < list1.size() * times; i++) {
            SimpleDraweeView imageView = new SimpleDraweeView(this);
            imageView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            Uri uri = Uri.parse(list1.get(i % list1.size()).getImageUrl());
            imageView.setImageURI(uri);
            final int finalI = i % list1.size();
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (list1.size() > 0) {

                        String type = list1.get(finalI).getType();

                        if (type != null) {

                            switch (type) {
                                case "normal":
                                    Intent intent = new Intent(MainActivity.this, VmallWebActivity.class);
                                    WebBaseData value = new WebBaseData();
                                    value.url = list1.get(finalI).getLinkUrl();
                                    intent.putExtra(WebBaseData.Intent_String, value);

                                    startActivity(intent);
                                    break;

                                case "webview":
//                                    Intent intent1 = new Intent(MainActivity.this, Web2Activity.class);
//                                    intent1.putExtra("url", list1.get(finalI).getLinkUrl());
//                                    startActivity(intent1);
                                    break;

                                case "popup":
                                    qrcode_bg.setVisibility(View.VISIBLE);
                                    qrcode_img.setImageURI(Uri.parse(list1.get(finalI).getLinkUrl()));
                                    break;
                            }

                        } else {
                            Intent intent = new Intent(MainActivity.this, VmallWebActivity.class);
                            WebBaseData value = new WebBaseData();
                            value.url = list1.get(finalI).getLinkUrl();
                            intent.putExtra(WebBaseData.Intent_String, value);
                            startActivity(intent);
                        }

                    }
                }
            });
            imgList.add(imageView);
        }

        viewPager.setCustomAdapterDatas(imgList);
        viewPager.startAutoScroll();
        samllpoit.setCount(list1.size());
        samllpoit.setIndex(0);
    }

    //设置二级轮播图数据
    private void setGoodsViewPager(int times) {
        if (times == 0) {
            return;
        }

        secondVPsize = list3.size();
        List<LinearLayout> goodsList = new ArrayList<>();

        for (int i = 0; i < list3.size() * times; i++) {

            LinearLayout linearLayout = new LinearLayout(this);
            linearLayout.setLayoutParams(new LinearLayout.LayoutParams((int) PhoneUtil.pxTodip(this, 360), (int) PhoneUtil.pxTodip(this, 150)));
            linearLayout.setOrientation(LinearLayout.HORIZONTAL);
            for (int j = 0; j < list3.get(i % list3.size()).size(); j++) {
                View view = LayoutInflater.from(this).inflate(R.layout.goods_viewpager_item, null);

                SimpleDraweeView goods_img = (SimpleDraweeView) view.findViewById(R.id.goods_img);
                TextView name = (TextView) view.findViewById(R.id.name);
                TextView price = (TextView) view.findViewById(R.id.price);

                Uri uri = Uri.parse(list3.get(i % list3.size()).get(j).getImageUrl());
                goods_img.setImageURI(uri);
                name.setText(list3.get(i % list3.size()).get(j).getName());
                price.setText("¥" + list3.get(i % list3.size()).get(j).getPrice());

                final int finalI = i % list3.size();
                final int finalJ = j;
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (list3.size() > 0) {
//                            ToastUtil.show("i=" + finalI + "  j=" + finalJ + "    TargetId=" + list3.get(finalI).get(finalJ).getTargetId());
                            Intent intent = new Intent(MainActivity.this, VmallWebActivity.class);
                            WebBaseData value = new WebBaseData();

                            switch (list3.get(finalI).get(finalJ).getTargetType()) {
                                case 1:
                                    value.url = mVmallUrl + "/index.html?page=detail&spuId=" + list3.get(finalI).get(finalJ).getTargetId();
                                    log.myE("url=", "-----------------------------------------------------------" + value.url);
                                    break;
                                case 2:
                                    value.url = mVmallUrl + "/index.html?page=detail&skuId=" + list3.get(finalI).get(finalJ).getTargetId();
                                    log.myE("url=", "-----------------------------------------------------------" + value.url);
                                    break;
                                default:
                                    break;
                            }
                            intent.putExtra(WebBaseData.Intent_String, value);

                            startActivity(intent);
                        } else {
                        }
                    }
                });
                linearLayout.addView(view);
            }
            goodsList.add(linearLayout);
        }
        goods_viewPager.setCustomAdapterDatas(goodsList);
        goods_viewPager.startAutoScroll();
        goods_smallpoint.setCount(list3.size());
        goods_smallpoint.setIndex(0);
    }

//    //定位回调
//    private static class LocationReceive implements BDLocationListener {
//        WeakReference<MainActivity> weakReference;
//
//        public LocationReceive(MainActivity activity) {
//            this.weakReference = new WeakReference<MainActivity>(activity);
//        }
//
//        @Override
//        public void onReceiveLocation(BDLocation bdLocation) {
//            MainActivity mActivity = weakReference.get();
//            log.myE("----BDLocationListener---", "-----------------onReceiveLocation------------------------------------1---------------------------------------");
//
//            mActivity.mLocClient.unRegisterLocationListener(mActivity.bdLocationListener);
//            mActivity.mLocClient.stop();
//            log.myE("----BDLocationListener---", "-----------------onReceiveLocation------------------------------------2---------------------------------------");
//
//            Message message = mActivity.mHandler.obtainMessage();
//            message.obj = bdLocation;
//            message.what = 3;
//            mActivity.mHandler.sendMessage(message);
//        }
//    }

    //定位 网络异常时无法获取定位
    private void loation() {
//        if (mLocClient != null) {
//            mLocClient.stop();
//        }
//        mLocClient = new LocationClient(getApplication());
//        bdLocationListener = new LocationReceive(this);
//        mLocClient.registerLocationListener(bdLocationListener);
//        LocationClientOption option = new LocationClientOption();
//        option.setOpenGps(true); // 打开gps
//        option.setCoorType("bd09ll"); // 设置坐标类型
//        option.setScanSpan(1000);
//        option.setIsNeedAddress(true);
//        mLocClient.setLocOption(option);
//        mLocClient.start();
    }


    //获取城市天气
    private void getCityWeather(String city) {

        SharedPreferences sp = getSharedPreferences("weather", MODE_PRIVATE);
        SharedPreferences.Editor edit = sp.edit();
        edit.putString("city_name", city);
        edit.commit();

        VoiceManager.getInstance().getWeather(city, new AppCallback<String>() {
            @Override
            public void onSuccess(String data) {
                log.myE("weather------------------------", data);
                Message msg = mHandler.obtainMessage();
                msg.what = 4;
                msg.obj = data;
                mHandler.sendMessage(msg);
            }

            @Override
            public void onFail(int errorCode, String msg) {
                log.myE("weather----------fail-----------", msg + "");
            }
        });

    }

    private void parseWeatherJson(String result) {
        try {
            JSONObject json = new JSONObject(result);
            JSONObject data = JsonUitls.getJSONObject(json, "data");
            JSONArray resultArray = JsonUitls.getJSONArray(data, "result");
            if (resultArray.length() > 0) {
                JSONObject todayData = resultArray.getJSONObject(0);

                String city = JsonUitls.getString(todayData, "city");
                String weather = JsonUitls.getString(todayData, "weather");
                String tempRange_s = JsonUitls.getString(todayData, "tempRange");
                String pm25 = JsonUitls.getString(todayData, "pm25");
                String airQuality = JsonUitls.getString(todayData, "airQuality");
                String wind = JsonUitls.getString(todayData, "wind");

                weather_text.setText(weather + " " + tempRange_s + " " + city);
                fresh_text.setText("PM2.5: " + pm25);
                WeatherIconUtil.setWeatherIcon(weather_icon, weather);
                String report = city + "," + weather + "," + tempRange_s + "," + wind + ",空气质量:" + airQuality + ",Pm2.5：" + pm25;
                GlobalParams.getInstance().setWeatherReport(report);
                GlobalParams.getInstance().setOutdoorTemp(tempRange_s);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void init() {


        //音量相关
        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        //最大音量
        int maxVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_SYSTEM);
        soundBar.setMax(maxVolume);

        wifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
        wifiManager.setWifiEnabled(true);
    }

    private void connectToStore(String url) {
        Intent intent = new Intent(MainActivity.this, VmallWebActivity.class);
        WebBaseData data = new WebBaseData();
        data.url = url;
        intent.putExtra(WebBaseData.Intent_String, data);
        startActivity(intent);
    }

    private void initListener() {

        weather_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, WeatherActivity.class);
                startActivityForResult(intent, REQUEST_WEATHER_CODE);
            }
        });


        info_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        timer_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Timer2Activity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        talk_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onShowVoiceSetDialog();
            }

        });

        voice_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //当前音量
                int currentVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_SYSTEM);
                soundBar.setProgress(currentVolume);
                volume_layout.setVisibility(View.VISIBLE);
                if (BleManager.getInstance().isBleEnable()) {
                    mBleImageView.setImageResource(R.mipmap.icon_ble_enable);
                } else {
                    mBleImageView.setImageResource(R.mipmap.icon_ble_inenable);
                }
            }
        });


        network_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
//                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(intent);
                Intent intent = new Intent(MainActivity.this, WifiScanActivity.class);
                startActivity(intent);
            }
        });

        setting_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, FridgeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });


        viewPager.setOnViewPageChangeListener(new AutoScrollViewPager.OnViewPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                samllpoit.setIndex(position % mainVPsize);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        goods_viewPager.setOnViewPageChangeListener(new AutoScrollViewPager.OnViewPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                goods_smallpoint.setIndex(position % secondVPsize);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        fridge_control.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this, FridgeActivity.class);
                startActivity(intent);
            }
        });

        music.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String packageName = "com.tencent.qqmusicpad";
                if (ApkUtil.checkApkExist(ViomiApplication.getContext(), packageName)) {
                    Log.e(TAG, "startOtherApp,packageName=" + packageName);
                    ApkUtil.startOtherApp(MainActivity.this, packageName, false);
                }
            }
        });

        video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String packageName = "com.qiyi.video.pad";
                if (ApkUtil.checkApkExist(ViomiApplication.getContext(), packageName)) {
                    Log.e(TAG, "startOtherApp,packageName1=" + packageName);
                    ApkUtil.startOtherApp(MainActivity.this, packageName, false);
                }
            }
        });

        fridge_store.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this, VmallWebActivity.class);
                WebBaseData data = new WebBaseData();
                data.url = mVmallUrl + "/index.html";
                intent.putExtra(WebBaseData.Intent_String, data);
                startActivity(intent);
            }
        });

        volume_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                volume_layout.setVisibility(View.GONE);
            }
        });

        volume_area_layout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });

        soundBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mAudioManager.setStreamVolume(AudioManager.STREAM_SYSTEM, progress, 0);
                mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, progress, 0);
                if (progress == 0) {
                    voice_btn.setImageResource(R.mipmap.icon_no_volume);
                } else {
                    voice_btn.setImageResource(R.mipmap.icon_volume);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        mBleImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Settings.ACTION_BLUETOOTH_SETTINGS);
                startActivity(intent);
            }
        });

        myclockview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(MainActivity.this, YMMusicService.class);
//                startService(intent);
//                Intent intent = new Intent(MainActivity.this, MusicPlayActivity.class);
//                startActivity(intent);

//                if (testBoolean) {
//                    SystemFileUtils.setHumanSensor(SystemFileUtils.HumanSensorType.close);
//                } else {
//                    SystemFileUtils.setHumanSensor(SystemFileUtils.HumanSensorType.open);
//                }
//                testBoolean = !testBoolean;

                Intent intent = new Intent(MainActivity.this, MainNewActivity.class);
                startActivity(intent);

            }
        });

        qrcode_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                qrcode_bg.setVisibility(View.GONE);
            }
        });

        qrcode_bg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                qrcode_bg.setVisibility(View.GONE);
            }
        });

        qrcode_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }

    private void registerReceiver() {
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        filter.addAction(Intent.ACTION_TIME_CHANGED);
        mReceiver = new ConnectionChangeReceiver() {
            @Override
            public void hasNoNetwork() {
                network_btn.setImageResource(R.drawable.wifi_iron0);
                log.myE("Receiver-wifi", "hasNoNetwork--------------------------------------------------------------");
            }

            @Override
            public void hasNetwork() {
                log.myE("Receiver-wifi", "hasNetwork----------------------------------------------------------------");
                loadData();
                loation();
            }
        };

        IntentFilter filter2 = new IntentFilter("viomi_file_completion");
        filter2.addAction(BroadcastAction.ACTION_WEATHER_GET);
        filter2.addAction(BroadcastAction.ACTION_PUSH_MESSAGE);
        fileReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equals("viomi_file_completion")) {
                    final Dialog albumDialog = new Dialog(MainActivity.this, R.style.fidge_dialog);
                    albumDialog.getWindow().setType((WindowManager.LayoutParams.TYPE_SYSTEM_ALERT));
                    View dialog_view = LayoutInflater.from(MainActivity.this).inflate(R.layout.album_dialog_layout, null);
                    albumDialog.setContentView(dialog_view);
                    TextView no_thanks = (TextView) dialog_view.findViewById(R.id.no_thanks);
                    TextView ok_do = (TextView) dialog_view.findViewById(R.id.ok_do);
                    no_thanks.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            albumDialog.dismiss();
                        }
                    });
                    ok_do.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            albumDialog.dismiss();
                            Intent intent1 = new Intent(MainActivity.this, AlbumActivity.class);
                            startActivity(intent1);
                        }
                    });
                    albumDialog.show();
                } else if (intent.getAction().equals(BroadcastAction.ACTION_WEATHER_GET)) {
                    String result = intent.getStringExtra("result");
                    if (mHandler != null) {
                        Message message = mHandler.obtainMessage();
                        message.obj = result;
                        message.what = 4;
                        mHandler.sendMessage(message);
                    }
                }
            }
        };

        wifiIcons = new int[]{R.drawable.wifi_iron1, R.drawable.wifi_iron2, R.drawable.wifi_iron3};

        IntentFilter filter3 = new IntentFilter(WifiManager.RSSI_CHANGED_ACTION);
        mReceiver3 = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(WifiManager.RSSI_CHANGED_ACTION)) {

                    WifiInfo info = wifiManager.getConnectionInfo();
                    if (info != null) {
                        network_btn.setImageResource(wifiIcons[WifiManager.calculateSignalLevel(info.getRssi(), wifiIcons.length)]);
                    } else {
                        network_btn.setImageResource(R.drawable.wifi_iron0);
                    }

                }
            }
        };

        registerReceiver(mReceiver, filter);
        registerReceiver(fileReceiver, filter2);
        registerReceiver(mReceiver3, filter3);
    }

    private void unregisterReceiver() {
        unregisterReceiver(mReceiver);
        unregisterReceiver(fileReceiver);
        unregisterReceiver(mReceiver3);
    }

    private float y;

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        int action = ev.getAction();

        switch (action) {
            case MotionEvent.ACTION_DOWN:
                y = ev.getY();
                break;
            case MotionEvent.ACTION_UP:
                float y1 = ev.getY();

                if (y1 - y > 300) {
                    loadData();
                    loation();
                }
                break;
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        log.myE("onActivityResult", requestCode + "-----------------------" + resultCode);
        if (requestCode == REQUEST_WEATHER_CODE && resultCode == RESPONE_WEATHER_CODE) {
            loation();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        String model = PhoneUtil.getDeviceModel();
        log.d(TAG, "model=" + model);
        if (model != null) {
            DeviceConfig.MODEL = model;
        }
        refreVoiceTalkView();
        //当前音量
        int currentVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_SYSTEM);
        soundBar.setProgress(currentVolume);
        if (currentVolume == 0) {
            voice_btn.setImageResource(R.mipmap.icon_no_volume);
        } else {
            voice_btn.setImageResource(R.mipmap.icon_volume);
        }

        if (BleManager.getInstance().isBleEnable()) {
            mBleImageView.setImageResource(R.mipmap.icon_ble_enable);
        } else {
            mBleImageView.setImageResource(R.mipmap.icon_ble_inenable);
        }
    }

    /***
     * 刷新语音命令图标
     */
    private void refreVoiceTalkView() {

        if (GlobalParams.getInstance().isVoiceEnabe()) {
            if (VoiceManager.getInstance().isVoiceOccupy()) {
                talk_btn.setImageResource(R.mipmap.vioce_occupy);
            } else {
                talk_btn.setImageResource(R.mipmap.voice_enable);
            }
        } else {
            talk_btn.setImageResource(R.mipmap.voice_inenable);
        }
    }

    /***
     * 语音设置dialog
     */
    private void onShowVoiceSetDialog() {
        if (mVoiceSettingDialog == null) {
            mVoiceSettingDialog = new VoiceSettingDialog(MainActivity.this, R.style.fidge_dialog);
            mVoiceSettingDialog.setCancelable(false);
        }
        mVoiceSettingDialog.show();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "onDestroy");
        ZhugeSDK.getInstance().flush(getApplicationContext());
        mHandler.removeCallbacksAndMessages(null);
        if (refleshThread != null) {
            refleshThread.interrupt();
        }
        unregisterReceiver();
        stopService(backgroudServiceintent);
        stopService(albumServiceIntent);
//        stopService(floatServiceIntent);
        System.exit(0);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return true;
    }
}
