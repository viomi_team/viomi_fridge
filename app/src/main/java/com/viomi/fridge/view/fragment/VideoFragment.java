package com.viomi.fridge.view.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.RelativeLayout;

import com.unilife.common.content.beans.iqiyi.IqiyiProgramInfo;
import com.unilife.common.content.beans.iqiyi.IqiyiProgramListData;
import com.unilife.common.utils.IqiyiUtils;
import com.unilife.content.logic.logic.IUMLogicListener;
import com.viomi.fridge.R;
import com.viomi.fridge.model.model.MyIqiyiVideo;
import com.viomi.fridge.util.ToastUtil;
import com.viomi.fridge.util.log;
import com.viomi.fridge.view.adapter.VideoGridAdapter;

import java.util.List;

/**
 * Created by Mocc on 2017/7/26
 */

public class VideoFragment extends Fragment {

    private String TAG = "VideoFragment";

    private GridView gridview;
    private VideoGridAdapter adapter;
    private List<IqiyiProgramInfo> dataList;
    private boolean isVisibleToUser;
    private boolean isprepare;
    private boolean hasloadfirst;
    private String m_token;
    private String categoryId;
    private RelativeLayout loading_layout;
    private RelativeLayout network_fail_layout;
    private MyIqiyiVideo instance;
    private int visibleLastIndex;
    private int currentPage = 0;
    private int totalPageNum;
    private int visibleItemCount;
    private Handler myhandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            currentPage++;
        }
    };


    public static synchronized VideoFragment getInstance(String m_token, String categoryId, String categoryName) {

        VideoFragment vf = new VideoFragment();
        Bundle bundle = new Bundle();
        bundle.putString("m_token", m_token);
        bundle.putString("categoryId", categoryId);
        bundle.putString("categoryName", categoryName);
        vf.setArguments(bundle);
        return vf;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        log.myE(TAG, "onCreateView");

        m_token = getArguments().getString("m_token");
        categoryId = getArguments().getString("categoryId");

        String categoryName = getArguments().getString("categoryName");
        TAG += categoryName;

        View view = inflater.inflate(R.layout.video_fragment_layout, null);
        gridview = (GridView) view.findViewById(R.id.gridview);
        loading_layout = (RelativeLayout) view.findViewById(R.id.loading_layout);
        network_fail_layout = (RelativeLayout) view.findViewById(R.id.network_fail_layout);

        instance = MyIqiyiVideo.getInstance();
        instance.setRequestTag(TAG);

        initListener();

        isprepare = true;
        //可见而又没加载，加载第一次
        if (isVisibleToUser && !hasloadfirst) {
            getData(m_token, 0, categoryId, currentPage, 24, true);
        }
        return view;
    }

    private void initListener() {

        gridview.setOnItemClickListener((p, v, pos, id) -> {

            ToastUtil.show("positon=" + pos);

        });

        loading_layout.setOnTouchListener((v, e) -> {
            return true;
        });

        network_fail_layout.setOnClickListener((v) -> {
            disReload();
            getData(m_token, 0, categoryId, currentPage, 24, true);
        });

        gridview.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

                int itemsLastIndex = adapter.getCount() - 1;
                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE && visibleLastIndex == itemsLastIndex && currentPage <= totalPageNum) {
                    loadmore();
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                VideoFragment.this.visibleItemCount = visibleItemCount;
                visibleLastIndex = firstVisibleItem + visibleItemCount - 1;
            }
        });

        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                IqiyiProgramInfo iqiyiProgramInfo = dataList.get(position);
                IqiyiUtils.forwardQYClient(iqiyiProgramInfo.getAlbumId(), iqiyiProgramInfo.getTvid(), getActivity());
            }
        });

    }

    //没有分页的
    private void loadmore() {
//        log.myE(TAG, "loadmore");
//        getData(m_token, 0, categoryId, currentPage, 24, false);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        this.isVisibleToUser = isVisibleToUser;
        log.myE(TAG, "isVisibleToUser=" + isVisibleToUser);

        //可见而又没加载并且准备好了，加载第一次
        if (isVisibleToUser && isprepare && !hasloadfirst) {
            getData(m_token, 0, categoryId, currentPage, 24, true);
        }
    }

    private void getData(String m_token, int order, String categoryId, int offset, int pageSize, boolean firstPage) {
        log.myE(TAG, "getData---categoryId=" + categoryId);

        showProgressBar();
        hasloadfirst = true;

        instance.getVideoByCatalog(m_token, 0, categoryId, offset, 96, new IUMLogicListener() {
            @Override
            public void onSuccess(Object data, long offset1, long total) {
                log.myE(TAG, "getData-onSuccess");
                disProgressBar();

                myhandler.sendEmptyMessage(0);

                log.myE(TAG, "offset=" + offset);

                IqiyiProgramListData iqiyiProgramListData = (IqiyiProgramListData) data;
                if (firstPage) {
                    totalPageNum = iqiyiProgramListData.getTotalNum().intValue() / 24;
                    log.myE(TAG, "totalPageNum=" + totalPageNum + "-----" + iqiyiProgramListData.getTotalNum().intValue());
                    dataList = iqiyiProgramListData.getDataList();
                    adapter = new VideoGridAdapter(dataList, getActivity());
                    gridview.setAdapter(adapter);
                } else {
                    dataList.addAll(iqiyiProgramListData.getDataList());
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onError(String msg) {
                log.myE(TAG, "getData-onError");
                disProgressBar();

                if (firstPage) {
                    showReload();
                } else {
                    ToastUtil.show("加载更多失败，请重试");
                }
            }
        });
    }


    private void showProgressBar() {
        loading_layout.setVisibility(View.VISIBLE);
    }

    private void disProgressBar() {
        loading_layout.setVisibility(View.GONE);
    }

    private void showReload() {
        network_fail_layout.setVisibility(View.VISIBLE);
    }

    private void disReload() {
        network_fail_layout.setVisibility(View.GONE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        instance.cancelRequest(TAG);
        if (dataList != null) {
            dataList.clear();
        }
    }
}
