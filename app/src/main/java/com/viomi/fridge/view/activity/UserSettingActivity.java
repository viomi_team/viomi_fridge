package com.viomi.fridge.view.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.miot.api.MiotManager;
import com.miot.common.exception.MiotException;
import com.viomi.common.callback.AppCallback;
import com.viomi.fridge.R;
import com.viomi.fridge.albumhttp.AlbumActivity;
import com.viomi.fridge.api.dao.InfoManager;
import com.viomi.fridge.broadcast.BroadcastAction;
import com.viomi.fridge.common.ViomiApplication;
import com.viomi.fridge.common.rxbus.BusEvent;
import com.viomi.fridge.common.rxbus.RxBus;
import com.viomi.fridge.device.AppConfig;
import com.viomi.fridge.device.DeviceConfig;
import com.viomi.fridge.device.DeviceManager;
import com.viomi.fridge.manager.AccountManager;
import com.viomi.fridge.manager.StatsManager;
import com.viomi.fridge.model.bean.ViomiUser;
import com.viomi.fridge.util.ToastUtil;
import com.viomi.fridge.util.ZhugeIoUtil;
import com.viomi.fridge.view.fragment.SettingAboutFragment;
import com.viomi.fridge.view.fragment.SettingCommonFragment;
import com.viomi.fridge.view.fragment.SettingDefaultFragment;
import com.viomi.fridge.view.fragment.SettingScreenFragment;
import com.viomi.fridge.view.fragment.SettingUserFragment;

public class UserSettingActivity extends BaseActivity {
    private ImageView imgBack;
    private TextView tvTitle;
    private RelativeLayout user_model;
    private SimpleDraweeView user_logo;
    private TextView user_name;
    private TextView user_id;
    private TextView login_btn;
    private TextView bind_btn;
    private RelativeLayout commom_model;
    private RelativeLayout screen_model;
    private RelativeLayout about_model;
    private RelativeLayout album_model;
    private RelativeLayout device_bind_layout;
    private RelativeLayout rl_file_manage;
    private TextView btn_restart;
    private RelativeLayout login_out_layout;
    private RelativeLayout unbind_layout;
    private TextView no_thanks,unbind_no_thanks;
    private TextView ok_do,unbind_ok_do;
    private RelativeLayout restart_layout;
    private TextView no_restart;
    private TextView yes_restart;
    private Fragment currentFragment;
    private boolean isLogined;
    private ImageView update_hint;
    private boolean isUpdate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_setting);
        initView();
        initListener();
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initUser();
    }

    private void initUser() {
        ViomiUser user = AccountManager.getViomiUser(this);
        if (user == null) {
            isLogined = false;
            login_btn.setText("登录");
        } else {
            login_btn.setText("退出登录");
            isLogined = true;
            showUserInfo();
        }
        if(DeviceManager.getInstance().isDeviceBind()){
            bind_btn.setText(R.string.text_device_already_bind);
        }else {
            bind_btn.setText(R.string.text_device_not_bind);
        }
    }

    private void showUserInfo() {
        ViomiUser user = AccountManager.getViomiUser(this);
        if (user == null) {
            return;
        }
        user_name.setText(user.getNickname());
        user_id.setVisibility(View.VISIBLE);
        user_id.setText(getString(R.string.title_viomi_account)+user.getAccount());
        Uri uri = Uri.parse(user.getHeadImg());
        user_logo.setImageURI(uri);
    }

    private void register() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BroadcastAction.ACTION_DEVICE_BIND);
        intentFilter.addAction(BroadcastAction.ACTION_DEVICE_UNBIND);
        intentFilter.addAction(BroadcastAction.ACTION_DEVICE_ALREADY_BIND);
        LocalBroadcastManager.getInstance(ViomiApplication.getContext()).registerReceiver(mBraodcastReceiver, intentFilter);
    }

    private void unregisterReceiver() {
        LocalBroadcastManager.getInstance(ViomiApplication.getContext()).unregisterReceiver(mBraodcastReceiver);
    }

    private BroadcastReceiver mBraodcastReceiver=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getAction()) {
                case BroadcastAction.ACTION_DEVICE_BIND:
                case BroadcastAction.ACTION_DEVICE_ALREADY_BIND:
                    if(bind_btn!=null) {
                        bind_btn.setText(R.string.text_device_already_bind);
                    }
                    break;

                case BroadcastAction.ACTION_DEVICE_UNBIND:
                    if(bind_btn!=null){
                        bind_btn.setText(R.string.text_device_not_bind);
                    }
                    break;

            }
        }
    };

    private void init() {
        register();
        currentFragment = SettingDefaultFragment.getInstance();
        getSupportFragmentManager().beginTransaction().add(R.id.lin, currentFragment).commit();

        Intent intent = getIntent();
        isUpdate = intent.getBooleanExtra("isUpdate", false);
        if (isUpdate) {
            update_hint.setVisibility(View.VISIBLE);
        }
    }

    private void initListener() {
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        user_model.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                modelSelect(SettingMode.USER);
            }
        });

        login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                modelSelect(SettingMode.USER);
                if (isLogined) {
                    login_out_layout.setVisibility(View.VISIBLE);
                } else {
                    Intent intent = new Intent(UserSettingActivity.this, ScanLoginActivity.class);
                    startActivity(intent);
                }
            }
        });

        device_bind_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(DeviceManager.getInstance().isDeviceBind()){
                    unbind_layout.setVisibility(View.VISIBLE);
                }else {
                    Intent intent = new Intent(UserSettingActivity.this, ScanMiBindActivity.class);
                    startActivity(intent);
                }

            }
        });

        commom_model.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                modelSelect(SettingMode.COMMOM);
            }
        });


        screen_model.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                modelSelect(SettingMode.SCREEN);
            }
        });

        about_model.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                modelSelect(SettingMode.ABOUT);
            }
        });


        album_model.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                modelSelect(SettingMode.ALBUM);
                Intent intent = new Intent(UserSettingActivity.this, AlbumActivity.class);
                startActivity(intent);
            }
        });

        rl_file_manage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                modelSelect(SettingMode.FILE_MANAGE);
                Intent intent = new Intent(mContext, FileManageActivity.class);
                startActivity(intent);
            }
        });

        btn_restart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                restart_layout.setVisibility(View.VISIBLE);
            }
        });

        login_out_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login_out_layout.setVisibility(View.GONE);
            }
        });

        no_thanks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login_out_layout.setVisibility(View.GONE);
            }
        });

        ok_do.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login_out_layout.setVisibility(View.GONE);
                login_btn.setText("登录");
                user_name.setText("未登录");
                user_id.setVisibility(View.GONE);
                Uri uri1 = Uri.parse("res:///" + R.drawable.dialog_write_bg);
                user_logo.setImageURI(uri1);
                isLogined = false;

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            MiotManager.getPeopleManager().deletePeople();
                        } catch (MiotException e) {
                            e.printStackTrace();
                        }
                        AccountManager.deleteViomiUser(UserSettingActivity.this);
                        InfoManager.getInstance().clearUserNewsRecord();
                        ZhugeIoUtil.setUserInfo(null);
                        StatsManager.onProfileSignOff();
                        Intent intentUserPush = new Intent(BroadcastAction.ACTION_REPORT_PUSH_USER);
                        sendBroadcast(intentUserPush);
                    }
                }).start();
                boolean result=DeviceManager.getInstance().unBindDevice(new AppCallback<String>() {
                    @Override
                    public void onSuccess(String data) {
                        RxBus.getInstance().post(BusEvent.MSG_LOGOUT_SUCCESS);
                    }

                    @Override
                    public void onFail(int errorCode, String msg) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Log.e(TAG,"rebind fail,msg="+msg);
                                ToastUtil.show(getString(R.string.text_device_unbind_fail));
                            }
                        });
                    }
                });
                if(!result){
                    Log.e(TAG,"rebind fail!");
                    ToastUtil.show(getString(R.string.text_device_unbind_fail));
                }
            }
        });


        restart_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                restart_layout.setVisibility(View.GONE);
            }
        });


        no_restart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                restart_layout.setVisibility(View.GONE);
            }
        });

        yes_restart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                restart_layout.setVisibility(View.GONE);
                Intent intent = new Intent("com.gmt.fridge.action.dameon.reboot");
                sendBroadcast(intent);
            }
        });

        unbind_no_thanks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                unbind_layout.setVisibility(View.GONE);
            }
        });

        unbind_ok_do.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                unbind_layout.setVisibility(View.GONE);
                login_out_layout.setVisibility(View.GONE);
                login_btn.setText("登录");
                user_name.setText("未登录");
                user_id.setVisibility(View.GONE);
                Uri uri1 = Uri.parse("res:///" + R.drawable.dialog_write_bg);
                user_logo.setImageURI(uri1);
                isLogined = false;

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            MiotManager.getPeopleManager().deletePeople();
                        } catch (MiotException e) {
                            e.printStackTrace();
                        }
                        AccountManager.deleteViomiUser(UserSettingActivity.this);
                        InfoManager.getInstance().clearUserNewsRecord();
                        ZhugeIoUtil.setUserInfo(null);
                        StatsManager.onProfileSignOff();
                        Intent intentUserPush = new Intent(BroadcastAction.ACTION_REPORT_PUSH_USER);
                        sendBroadcast(intentUserPush);
                    }
                }).start();
                boolean result=DeviceManager.getInstance().unBindDevice(new AppCallback<String>() {
                    @Override
                    public void onSuccess(String data) {

                    }

                    @Override
                    public void onFail(int errorCode, String msg) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Log.e(TAG,"rebind fail,msg="+msg);
                                ToastUtil.show(getString(R.string.text_device_unbind_fail));
                            }
                        });
                    }
                });
                if(!result){
                    Log.e(TAG,"rebind fail!");
                    ToastUtil.show(getString(R.string.text_device_unbind_fail));
                }
            }
        });
    }

    private enum SettingMode {
        USER, COMMOM, SCREEN, ABOUT, ALBUM,FILE_MANAGE;
    }

    private void modelSelect(SettingMode model) {

        user_model.setBackgroundColor(Color.parseColor("#ffffff"));
        commom_model.setBackgroundColor(Color.parseColor("#ffffff"));
        screen_model.setBackgroundColor(Color.parseColor("#ffffff"));
        about_model.setBackgroundColor(Color.parseColor("#ffffff"));
        album_model.setBackgroundColor(Color.parseColor("#ffffff"));
        rl_file_manage.setBackgroundColor(Color.parseColor("#ffffff"));

        Fragment fragment = null;

        switch (model) {
            case USER: {
                if(AppConfig.VIOMI_FRIDGE_V31.equals(DeviceConfig.MODEL)){
                    fragment = SettingDefaultFragment.getInstance();
                }else {
                    fragment = SettingUserFragment.getInstance();
                }
                break;
            }

            case COMMOM: {
                commom_model.setBackgroundColor(Color.parseColor("#0d000000"));
                fragment = SettingCommonFragment.getInstance();
                break;
            }

            case SCREEN: {
                screen_model.setBackgroundColor(Color.parseColor("#0d000000"));
                fragment = SettingScreenFragment.getInstance();
                break;
            }

            case ABOUT: {
                about_model.setBackgroundColor(Color.parseColor("#0d000000"));
                update_hint.setVisibility(View.GONE);
                fragment = SettingAboutFragment.getInstance(isUpdate);
                isUpdate=false;
                break;
            }

            case ALBUM: {
                fragment = SettingDefaultFragment.getInstance();
                break;
            }

            case FILE_MANAGE:{
                fragment = SettingDefaultFragment.getInstance();
                break;
            }

            default:
                fragment = SettingDefaultFragment.getInstance();
                break;
        }

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        if (currentFragment == null) {
            currentFragment = fragment;
            transaction.add(R.id.lin, currentFragment);
        } else {
            if (fragment.isAdded()) {
                transaction.hide(currentFragment).show(fragment);
            } else {
                transaction.hide(currentFragment).add(R.id.lin, fragment);
            }
        }

        transaction.commit();
        currentFragment = fragment;
    }

    private void initView() {

        imgBack = (ImageView) findViewById(R.id.imgBack);
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        tvTitle.setText("个人中心");
        user_model = (RelativeLayout) findViewById(R.id.user_model);
        user_logo = (SimpleDraweeView) findViewById(R.id.user_logo);
        user_name = (TextView) findViewById(R.id.user_name);
        user_id=(TextView) findViewById(R.id.user_id);
        login_btn = (TextView) findViewById(R.id.login_btn);
        bind_btn= (TextView) findViewById(R.id.bind_btn);

        commom_model = (RelativeLayout) findViewById(R.id.commom_model);
        screen_model = (RelativeLayout) findViewById(R.id.screen_model);
        about_model = (RelativeLayout) findViewById(R.id.about_model);
        update_hint = (ImageView) findViewById(R.id.update_hint);
        album_model = (RelativeLayout) findViewById(R.id.album_model);
        rl_file_manage = (RelativeLayout) findViewById(R.id.rl_file_manage);
        device_bind_layout=(RelativeLayout) findViewById(R.id.device_bind_layout);
        if(AppConfig.VIOMI_FRIDGE_V3.equals(DeviceConfig.MODEL)
                ||AppConfig.VIOMI_FRIDGE_V31.equals(DeviceConfig.MODEL)){
            device_bind_layout.setVisibility(View.VISIBLE);
        }else {
            device_bind_layout.setVisibility(View.GONE);
        }

        btn_restart = (TextView) findViewById(R.id.btn_restart);

        login_out_layout = (RelativeLayout) findViewById(R.id.login_out_layout);
        no_thanks = (TextView) findViewById(R.id.no_thanks);
        ok_do = (TextView) findViewById(R.id.ok_do);

        unbind_layout =(RelativeLayout) findViewById(R.id.unbind_layout);
        unbind_no_thanks = (TextView) findViewById(R.id.unbind_no_thanks);
        unbind_ok_do = (TextView) findViewById(R.id.unbind_ok_do);

        restart_layout = (RelativeLayout) findViewById(R.id.restart_layout);
        no_restart = (TextView) findViewById(R.id.no_restart);
        yes_restart = (TextView) findViewById(R.id.yes_restart);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver();
    }
}
