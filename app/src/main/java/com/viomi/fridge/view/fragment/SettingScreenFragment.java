package com.viomi.fridge.view.fragment;

import android.content.Context;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Spinner;

import com.viomi.common.widget.SwitchButton;
import com.viomi.fridge.R;
import com.viomi.fridge.common.GlobalParams;
import com.viomi.fridge.device.AppConfig;
import com.viomi.fridge.device.DeviceConfig;
import com.viomi.fridge.manager.VoiceManager;
import com.viomi.fridge.model.bean.ScreenSleepBean;
import com.viomi.fridge.util.SystemFileUtils;
import com.viomi.fridge.view.adapter.SleepTimeAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mocc on 2017/11/1
 */

public class SettingScreenFragment extends BaseFragment {

    private static volatile SettingScreenFragment fragment;
    private SwitchButton vioceBtn;
    private SwitchButton humanSensorBtn;
    private SeekBar lightBar;
    private SeekBar soundBar;
    private Spinner screen_sleep_spinner;
    private AudioManager mAudioManager;
    private List<ScreenSleepBean> screenSleepBeanList;

    public static SettingScreenFragment getInstance() {
        if (fragment == null) {
            synchronized (SettingScreenFragment.class) {
                if (fragment == null) {
                    fragment = new SettingScreenFragment();
                }
            }
        }
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.setting_screen_fragment_layout, null);
        vioceBtn = (SwitchButton) view.findViewById(R.id.vioceBtn);
        humanSensorBtn = (SwitchButton) view.findViewById(R.id.humanSensorBtn);
        lightBar = (SeekBar) view.findViewById(R.id.lightBar);
        soundBar = (SeekBar) view.findViewById(R.id.soundBar);
        screen_sleep_spinner = (Spinner) view.findViewById(R.id.screen_sleep_spinner);

        init();
        initListener();
        return view;
    }

    private void initListener() {

        vioceBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                GlobalParams.getInstance().setVoiceEnabe(b);
                VoiceManager.getInstance().enableVoice(b);
            }
        });

        humanSensorBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SystemFileUtils.HumanSensorType sensorType;
                SystemFileUtils.HumanSensorPathEnum pathEnum;
                if (isChecked) {
                    sensorType = SystemFileUtils.HumanSensorType.open;
                } else {
                    sensorType = SystemFileUtils.HumanSensorType.close;
                }

                switch (DeviceConfig.MODEL) {
                    // 小鲜互联 云米智能冰箱三门 绿联主控
                    case AppConfig.VIOMI_FRIDGE_V1:
                        pathEnum = SystemFileUtils.HumanSensorPathEnum.DOOR3;
                        break;

                    // 小鲜互联 云米智能冰箱四门 双鹿主控
                    case AppConfig.VIOMI_FRIDGE_V2:
                        pathEnum = SystemFileUtils.HumanSensorPathEnum.DOOR4;
                        break;

                    // 云米462大屏金属门冰箱
                    case AppConfig.VIOMI_FRIDGE_V3:
                    case AppConfig.VIOMI_FRIDGE_V31:
                        pathEnum = SystemFileUtils.HumanSensorPathEnum.DOOR462;
                        break;

                    // 云米455大屏玻璃门冰箱
                    case AppConfig.VIOMI_FRIDGE_V4:
                        pathEnum = SystemFileUtils.HumanSensorPathEnum.DOOR455;
                        break;
                    default:
                        pathEnum = SystemFileUtils.HumanSensorPathEnum.DOOR4;
                        break;
                }
                SystemFileUtils.setHumanSensor(sensorType, pathEnum);

                GlobalParams.getInstance().setHumanSensorSwitch(isChecked);
            }
        });

        soundBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mAudioManager.setStreamVolume(AudioManager.STREAM_SYSTEM, progress, 0);
                mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, progress, 0);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        lightBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                saveBrightness(getActivity(), progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        screen_sleep_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                setScreenSleepTime(screenSleepBeanList.get(position).getTime());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

    }

    //设置屏幕熄灭时间
    private boolean setScreenSleepTime(int time) {
        return Settings.System.putInt(getActivity().getContentResolver(), Settings.System.SCREEN_OFF_TIMEOUT, time);
    }

    private void saveBrightness(Context context, int brightness) {
        Uri uri = Settings.System.getUriFor(Settings.System.SCREEN_BRIGHTNESS);
        Settings.System.putInt(context.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS, brightness);
        context.getContentResolver().notifyChange(uri, null);
    }


    private void init() {
        //音量相关
        mAudioManager = (AudioManager)getActivity(). getSystemService(Context.AUDIO_SERVICE);
        //当前音量
        int currentVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_SYSTEM);
        //最大音量
        int maxVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_SYSTEM);
        soundBar.setMax(maxVolume);
        soundBar.setProgress(currentVolume);

        //亮度相关
        int systemBrightness = 0;
        try {
            systemBrightness = Settings.System.getInt(getActivity().getContentResolver(), Settings.System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        lightBar.setMax(255);
        lightBar.setProgress(systemBrightness);


        //屏幕休眠
        screenSleepBeanList = new ArrayList<>();
        screenSleepBeanList.add(new ScreenSleepBean(15 * 1000, "15秒", false));
        screenSleepBeanList.add(new ScreenSleepBean(30 * 1000, "30秒", false));
        screenSleepBeanList.add(new ScreenSleepBean(1 * 60 * 1000, "1分钟", false));
        screenSleepBeanList.add(new ScreenSleepBean(2 * 60 * 1000, "2分钟", false));
        screenSleepBeanList.add(new ScreenSleepBean(5 * 60 * 1000, "5分钟", false));
        screenSleepBeanList.add(new ScreenSleepBean(10 * 60 * 1000, "10分钟", false));
        screenSleepBeanList.add(new ScreenSleepBean(30 * 60 * 1000, "30分钟", false));
        screen_sleep_spinner.setAdapter(new SleepTimeAdapter(screenSleepBeanList, getActivity()));
        int screenTime;
        try {
            screenTime = Settings.System.getInt(getActivity().getContentResolver(), Settings.System.SCREEN_OFF_TIMEOUT);
            switch (screenTime) {
                case 15 * 1000: {
                    screen_sleep_spinner.setSelection(0);
                    break;
                }
                case 30 * 1000: {
                    screen_sleep_spinner.setSelection(1);
                    break;
                }
                case 1 * 60 * 1000: {
                    screen_sleep_spinner.setSelection(2);
                    break;
                }
                case 2 * 60 * 1000: {
                    screen_sleep_spinner.setSelection(3);
                    break;
                }
                case 5 * 60 * 1000: {
                    screen_sleep_spinner.setSelection(4);
                    break;
                }
                case 10 * 60 * 1000: {
                    screen_sleep_spinner.setSelection(5);
                    break;
                }
                case 30 * 60 * 1000: {
                    screen_sleep_spinner.setSelection(6);
                    break;
                }

                default: {
                    screen_sleep_spinner.setSelection(4);
                    break;
                }
            }
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }

        vioceBtn.setChecked(GlobalParams.getInstance().isVoiceEnabe());
        humanSensorBtn.setChecked(GlobalParams.getInstance().isHumanSensorSwitch());
    }
}
