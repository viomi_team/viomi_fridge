package com.viomi.fridge.view.adapter;

import android.app.Activity;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.viomi.fridge.R;
import com.viomi.fridge.api.dao.DeviceInfoMessage;
import com.viomi.fridge.api.dao.UserInfoMessage;
import com.viomi.fridge.model.bean.PushMsg;
import com.viomi.fridge.util.UMUtil;
import com.viomi.fridge.view.activity.FoodManageActivity;
import com.viomi.fridge.view.fragment.DeviceInfosFragment;
import com.viomi.fridge.view.fragment.UserInfosFragment;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by young2 on 2017/7/12.
 */

public class DeviceInfosAdapter extends RecyclerView.Adapter<DeviceInfosAdapter.MyViewHolder> {
    private Activity mContext;
    private DeviceInfosFragment mFragment;
    private List<DeviceInfoMessage> mDataList;
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM月dd日 HH:mm");

    public DeviceInfosAdapter(Activity context, Fragment fragment, List<DeviceInfoMessage> datas){
        mDataList=datas;
        mContext=context;
        mFragment= (DeviceInfosFragment) fragment;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(mContext).inflate(R.layout.item_info_user, parent, false);
        DeviceInfosAdapter.MyViewHolder holder = new DeviceInfosAdapter.MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        DeviceInfoMessage deviceInfoMessage=mDataList.get(position);
        if(PushMsg.PUSH_MESSAGE_TYPE_FOOD.equals(mDataList.get(position).getTopic())){
            holder.LogoImageView.setImageResource(R.mipmap.icon_info_food);
        }else {
            holder.LogoImageView.setImageResource(R.mipmap.icon_info_default);
        }

        if(mDataList.get(position).getTitle()!=null){
            holder.titleTextView.setText((deviceInfoMessage.getTitle()));
        }else {
            holder.titleTextView.setVisibility(View.GONE);
        }
        if(mDataList.get(position).getContent()!=null){
            holder.contentTextView.setText((deviceInfoMessage.getContent()));
        }else {
            holder.contentTextView.setVisibility(View.GONE);
        }
        if(deviceInfoMessage.isRead()){
            holder.newPointView.setVisibility(View.GONE);
        }else {
            holder.newPointView.setVisibility(View.VISIBLE);
        }
        if(position==mDataList.size()-1){
            holder.progressEndLineView.setVisibility(View.GONE);
        }else {
            holder.progressEndLineView.setVisibility(View.VISIBLE);
        }

        holder.timeTextVIew.setText(simpleDateFormat.format(new Date(deviceInfoMessage.getTime()*1000)));

        if(deviceInfoMessage.getLinkUrl()==null){
            holder.detailTextView.setVisibility(View.GONE);
        }else {
            holder.detailTextView.setVisibility(View.VISIBLE);
        }

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, FoodManageActivity.class);
                mContext.startActivity(intent);
            }
        });
        holder.deleteImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFragment.deleteInfo(mDataList.get(position).getInfoId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder
    {

        public View progressEndLineView;
        public SimpleDraweeView LogoImageView;
        public TextView titleTextView;
        public TextView contentTextView;
        public ImageView newPointView;
        public LinearLayout detailTextView;
        public ImageView deleteImageView;
        public TextView timeTextVIew;
        public CardView cardView;
        public MyViewHolder(View view)
        {
            super(view);
            progressEndLineView= (View) view.findViewById(R.id.step_line_end);
            LogoImageView= (SimpleDraweeView) view.findViewById(R.id.info_icon);
            titleTextView= (TextView) view.findViewById(R.id.info_title);
            contentTextView= (TextView) view.findViewById(R.id.info_desc);
            deleteImageView= (ImageView) view.findViewById(R.id.delete_view);
            detailTextView= (LinearLayout) view.findViewById(R.id.info_detail);
            newPointView= (ImageView) view.findViewById(R.id.red_point);
            timeTextVIew= (TextView) view.findViewById(R.id.time_text);
            cardView= (CardView) view.findViewById(R.id.cardview);
        }
    }
}
