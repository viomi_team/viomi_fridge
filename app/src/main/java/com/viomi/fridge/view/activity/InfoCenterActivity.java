package com.viomi.fridge.view.activity;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.viomi.fridge.R;
import com.viomi.fridge.api.dao.InfoManager;
import com.viomi.fridge.util.log;
import com.viomi.fridge.view.adapter.CommonAdapter;
import com.viomi.fridge.view.fragment.AdvertInfosFragment;
import com.viomi.fridge.view.fragment.DeviceInfosFragment;
import com.viomi.fridge.view.fragment.UserInfosFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by young2 on 2017/7/11.
 */

public class InfoCenterActivity  extends BaseActivity implements  AdvertInfosFragment.RefreshUnreadInfoListenr
,UserInfosFragment.RefreshUnreadInfoListenr,DeviceInfosFragment.RefreshUnreadInfoListenr{
    private static final String TAG=InfoCenterActivity.class.getSimpleName();
    private List<Fragment> mFragments = new ArrayList<Fragment>();
    private RadioButton mInfoAdvertButton,mInfoUserButton,mInfoDeviceButton;
    private TextView mNewInfoAdvert,mNewUserAdvert,mNewDeviceAdvert;
    private ViewPager mViewPager;
    private Fragment mAdvertInfoFragment,mUserInfoFragment,mDeviceInfoFragment;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        setContentView(R.layout.activity_info_center);
        init();
    }

    private void init(){
        mAdvertInfoFragment=new AdvertInfosFragment();
        mUserInfoFragment=new UserInfosFragment();
        mDeviceInfoFragment=new DeviceInfosFragment();

        mFragments.add(mAdvertInfoFragment);
        mFragments.add(mUserInfoFragment);
        mFragments.add(mDeviceInfoFragment);

        View backView = findViewById(R.id.icon_back);
        if(backView!=null) {
            backView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    finish();
                }
            });
        }

        ImageView button= (ImageView) findViewById(R.id.right_icon);
//        button.setBackgroundColor(getResources().getColor(R.color.transparent));
//        button.setVisibility(View.VISIBLE);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(InfoCenterActivity.this,InfoSetActivity.class);
                startActivity(intent);
            }
        });
        mInfoAdvertButton= (RadioButton) findViewById(R.id.button_info_advert);
        mInfoUserButton= (RadioButton) findViewById(R.id.button_info_user);
        mInfoDeviceButton= (RadioButton) findViewById(R.id.button_info_device);
        mInfoAdvertButton.setOnCheckedChangeListener(mCheckedChangeListener);
        mInfoUserButton.setOnCheckedChangeListener(mCheckedChangeListener);
        mInfoDeviceButton.setOnCheckedChangeListener(mCheckedChangeListener);

        mNewInfoAdvert= (TextView) findViewById(R.id.number_info_advert);
        mNewUserAdvert= (TextView) findViewById(R.id.number_info_user);
        mNewDeviceAdvert= (TextView) findViewById(R.id.number_info_device);
        mViewPager = (ViewPager) findViewById(R.id.viewpager);
        mViewPager.setOffscreenPageLimit(3);
        mViewPager.setAdapter(new CommonAdapter(getSupportFragmentManager(),mFragments));
        setPagerItem(0);
        Intent intent=getIntent();
        if(intent!=null){
            int advertCount=intent.getIntExtra("advert",0);
            int deviceCount=intent.getIntExtra("device",0);
            int userCount=intent.getIntExtra("user",0);
            refreshNewsCountView(advertCount,userCount,deviceCount);

            Bundle bundle = new Bundle();
            bundle.putInt("newsCount",advertCount);
            mAdvertInfoFragment.setArguments(bundle);

            Bundle bundle1 = new Bundle();
            bundle1.putInt("newsCount",userCount);
            mUserInfoFragment.setArguments(bundle1);

            Bundle bundle2 = new Bundle();
            bundle2.putInt("newsCount",deviceCount);
            mDeviceInfoFragment.setArguments(bundle2);
        }
    }

    private void setPagerItem(int index){
        switch (index){
            case 0:
                mViewPager.setCurrentItem(index);
                break;
            case 1:
                mViewPager.setCurrentItem(index);
                break;
            case 2:
                mViewPager.setCurrentItem(index);
                break;
        }
    }

     private CompoundButton.OnCheckedChangeListener mCheckedChangeListener=new CompoundButton.OnCheckedChangeListener() {

        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            if(b){
                switch(compoundButton.getId()){
                    case R.id.button_info_advert:
                        setPagerItem(0);
                        break;
                    case R.id.button_info_user:
                        setPagerItem(1);
                        break;
                    case R.id.button_info_device:
                        setPagerItem(2);
                        break;
                }
            }
        }
    };

    /***
     * 刷新新消息数量红点
     * @param advertCount
     * @param userCount
     * @param deviceCount
     */
    private void refreshNewsCountView(int advertCount,int userCount,int deviceCount){

        int recordCount= InfoManager.getInstance().getAdvertInfoRecordSize();
        log.d(TAG,"recordCount="+recordCount);
        advertCount+=InfoManager.getInstance().getAdvertNewsNumber();
        if(advertCount<=0||recordCount==0){//数据库记录是0，代表第一次获取，不显示最新数量
            mNewInfoAdvert.setVisibility(View.GONE);
        }else if(advertCount>=100){
            mNewInfoAdvert.setVisibility(View.VISIBLE);
            mNewInfoAdvert.setText("..");
        }else {
            mNewInfoAdvert.setVisibility(View.VISIBLE);
            mNewInfoAdvert.setText(""+advertCount);
        }

        recordCount=InfoManager.getInstance().getUserInfoRecordSize();
        log.d(TAG,"recordCount="+recordCount);
        userCount+=InfoManager.getInstance().getUserNewsNumber();
        if(userCount<=0||recordCount==0){
            mNewUserAdvert.setVisibility(View.GONE);
        }else if(userCount>=100){
            mNewUserAdvert.setVisibility(View.VISIBLE);
            mNewUserAdvert.setText("..");
        }else {
            mNewUserAdvert.setVisibility(View.VISIBLE);
            mNewUserAdvert.setText(""+userCount);
        }

        recordCount=InfoManager.getInstance().getDeviceInfoRecordSize();
        log.d(TAG,"recordCount="+recordCount);
        deviceCount+=InfoManager.getInstance().getDeviceNewsNumber();
        if(deviceCount<=0||recordCount==0){
            mNewDeviceAdvert.setVisibility(View.GONE);
        }else if(deviceCount>=100){
            mNewDeviceAdvert.setVisibility(View.VISIBLE);
            mNewDeviceAdvert.setText("..");
        }else {
            mNewDeviceAdvert.setVisibility(View.VISIBLE);
            mNewDeviceAdvert.setText(""+deviceCount);
        }
    }


    @Override
    public void refreshAdvertInfo(int count) {
        if(count==0){
            mNewInfoAdvert.setVisibility(View.GONE);
        }else if(count>=100){
            mNewInfoAdvert.setVisibility(View.VISIBLE);
            mNewInfoAdvert.setText("..");
        }else {
            mNewInfoAdvert.setVisibility(View.VISIBLE);
            mNewInfoAdvert.setText(""+count);
        }
    }

    @Override
    public void refreshUserInfo(int count) {
        mNewUserAdvert.setVisibility(View.GONE);
    }

    @Override
    public void refreshDeviceInfo(int count) {
        mNewDeviceAdvert.setVisibility(View.GONE);
    }
}
