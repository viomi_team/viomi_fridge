package com.viomi.fridge.view.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.viomi.fridge.R;
import com.viomi.fridge.model.bean.FileInfo;
import com.viomi.fridge.util.FileUtil;
import com.viomi.fridge.util.LogUtils;
import com.viomi.fridge.util.SDCardUtils;
import com.wx.wheelview.util.WheelUtils;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Ljh on 2017/11/17
 */
public class FileManageAdapter extends RecyclerView.Adapter<FileManageAdapter.MyViewHolder> implements View.OnClickListener {
    private static final String TAG = "FileManageAdapter";
    //
    private Context mContext;
    private List<FileInfo> datas = new ArrayList<>();
    private FileManageAdapterCallback mCallback;

    public FileManageAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public FileManageAdapter(Context mContext, List<FileInfo> datas) {
        this.mContext = mContext;
        this.datas = datas;
    }

    public List<FileInfo> getDatas() {
        return datas;
    }

    public void update(List<FileInfo> items) {
        this.datas = items;
        notifyDataSetChanged();
    }

    private FileInfo getItem(int position) {
        if (datas == null) {
            return null;
        }
        return datas.get(position);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = View.inflate(mContext, R.layout.item_file_manage, null);
        return new MyViewHolder(view, this);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        LogUtils.d(TAG + " the pos is:" + position);
        FileInfo bean = getItem(position);
        if (bean == null) {
            return;
        }
        holder.root.setTag(position);
        holder.tvName.setText(bean.getFileName());
        holder.tvName.setVisibility(bean.getFileType() == FileUtil.FileType.FILE_MUSIC ? View.VISIBLE : View.INVISIBLE);
        holder.imgSelect.setSelected(bean.isSelected());

        GradientDrawable drawable = (GradientDrawable) holder.tvSize.getBackground();
        int corner = WheelUtils.dip2px(mContext, 10);
        drawable.setCornerRadii(new float[]{0, 0, 0, 0, corner, corner, corner, corner});
        drawable.setColor(Color.parseColor("#66000000"));
        holder.tvSize.setBackground(drawable);
        holder.tvSize.setText(SDCardUtils.bit2KbMGB(bean.getFileSize()));

        if (bean.getBitmap(mContext) != null)
            holder.imgLogo.setImageBitmap(bean.getBitmap(mContext));
        else
            holder.imgLogo.setImageResource(R.drawable.logo_music1);
    }

    @Override
    public int getItemCount() {
        return datas.size();
    }

    @Override
    public void onClick(View v) {
        if (v == null) {
            return;
        }
        switch (v.getId()) {
            case R.id.root://选中
                if (mCallback != null)
                    mCallback.select(Integer.parseInt(v.getTag().toString()));
                break;
        }
    }

    public void setCallback(FileManageAdapterCallback callback) {
        mCallback = callback;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private RelativeLayout root;
        private ImageView imgLogo;
        private ImageView imgSelect;
        private TextView tvName, tvSize;
        public View.OnClickListener onClickListener;

        public void setOnClickListener(View.OnClickListener onClickListener) {
            this.onClickListener = onClickListener;
        }

        public MyViewHolder(View view, View.OnClickListener onClickListener) {
            super(view);
            root = (RelativeLayout) view.findViewById(R.id.root);
            imgLogo = (ImageView) view.findViewById(R.id.imgLogo);
            imgSelect = (ImageView) view.findViewById(R.id.imgSelect);
            tvName = (TextView) view.findViewById(R.id.tvName);
            tvSize = (TextView) view.findViewById(R.id.tvSize);
            root.setOnClickListener(onClickListener);
        }
    }

    public interface FileManageAdapterCallback {
        void select(int position);//
    }
}
