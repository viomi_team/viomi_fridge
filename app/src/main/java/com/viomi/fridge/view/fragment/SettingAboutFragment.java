package com.viomi.fridge.view.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.viomi.common.widget.SwitchButton;
import com.viomi.fridge.R;
import com.viomi.fridge.broadcast.BroadcastAction;
import com.viomi.fridge.common.GlobalParams;
import com.viomi.fridge.device.DeviceConfig;
import com.viomi.fridge.model.bean.MiIndentify;
import com.viomi.fridge.util.ApkUtil;
import com.viomi.fridge.util.PhoneUtil;
import com.viomi.fridge.util.ToolUtil;
import com.viomi.fridge.view.activity.FactoryTestActivity;
import com.viomi.fridge.view.activity.FeedbackActivity;
import com.viomi.fridge.view.activity.InstructionsActivity;
import com.viomi.fridge.view.activity.VersionManagerActivity;

/**
 * Created by Mocc on 2017/11/1
 */

public class SettingAboutFragment extends BaseFragment {

    private static volatile SettingAboutFragment fragment;
    private volatile SettingAboutFragment instance;
    private RelativeLayout version;
    private RelativeLayout debug_layout,upgrade_layout;
    private TextView version_new;
    private TextView version_text;
    private RelativeLayout system_info;
    private TextView info_text;
    private RelativeLayout instructions;
    private RelativeLayout feedback;

    private boolean mAppUpgradeFlag, mSystemUpgradeFlag;
    private LinearLayout hide_layout;
    private RelativeLayout test_model;
    private RelativeLayout system_set;
    private SwitchButton vmallHttpBtn,upgradeTestBtn;
    private int clickCount;
    private static boolean isUpdate;

//    public static SettingAboutFragment getInstance() {
//        if (fragment == null) {
//            synchronized (SettingAboutFragment.class) {
//                if (fragment == null) {
//                    fragment = new SettingAboutFragment();
//                }
//            }
//        }
//        return fragment;
//    }

    public static SettingAboutFragment getInstance(boolean isUpdate) {
        SettingAboutFragment.isUpdate = isUpdate;
        return new SettingAboutFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.setting_about_fragment_layout, null);

        version = (RelativeLayout) view.findViewById(R.id.version);
        version_new = (TextView) view.findViewById(R.id.version_new);
        version_text = (TextView) view.findViewById(R.id.version_text);

        system_info = (RelativeLayout) view.findViewById(R.id.system_info);
        info_text = (TextView) view.findViewById(R.id.info_text);

        instructions = (RelativeLayout) view.findViewById(R.id.instructions);
        feedback = (RelativeLayout) view.findViewById(R.id.feedback);

        debug_layout=(RelativeLayout) view.findViewById(R.id.debug_layout);
        upgrade_layout=(RelativeLayout) view.findViewById(R.id.upgrade_layout);
        if(!GlobalParams.HTTP_DEBUG){
            debug_layout.setVisibility(View.GONE);
            upgrade_layout.setVisibility(View.GONE);
        }

        hide_layout = (LinearLayout) view.findViewById(R.id.hide_layout);
        test_model = (RelativeLayout) view.findViewById(R.id.test_model);
        system_set = (RelativeLayout) view.findViewById(R.id.system_set);
        vmallHttpBtn = (SwitchButton) view.findViewById(R.id.VmallHttpBtn);
        upgradeTestBtn = (SwitchButton) view.findViewById(R.id.UpgradeTestBtn);
        initListener();
        init();
        return view;
    }

    private void init() {
        //版本相关
        mAppUpgradeFlag = getActivity().getIntent().getBooleanExtra("app_new", false);
        mSystemUpgradeFlag = getActivity().getIntent().getBooleanExtra("system_new", false);

        if (isUpdate) {
            mAppUpgradeFlag=isUpdate;
            version_new.setVisibility(View.VISIBLE);
        }

        version_text.setText("V" + ApkUtil.getVersionCode());

        //网络信息
        MiIndentify miIndentify = PhoneUtil.getMiIdentify();
        String mac = miIndentify.mac;
        String did = miIndentify.did;
        if (miIndentify.mac.equals(DeviceConfig.DefaultMac)) {
            mac = "error";
            did = "error";
        }else {
            if(!ToolUtil.isNumber(miIndentify.did)){
                did+="error";
            }
        }
        if (miIndentify.did.contains("34CE00")) {
            did = "error";
        }

        if(did==null||did.contains("error")){
            info_text.setTextColor(getResources().getColor(R.color.red));
        }
        info_text.setText("MAC:" + mac + ",DID=" + did);

        //商城调试
        vmallHttpBtn.setChecked(GlobalParams.getInstance().isVmallHttpDebug());
        upgradeTestBtn.setChecked(GlobalParams.getInstance().isUpgradeTestEnable());
    }

    private void initListener() {
        version.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), VersionManagerActivity.class);
                intent.putExtra("app_new", mAppUpgradeFlag);
                intent.putExtra("system_new", mSystemUpgradeFlag);
                startActivity(intent);
                version_new.setVisibility(View.GONE);
                getActivity().sendBroadcast(new Intent(BroadcastAction.ACTION_VERSION_UPDATE_CLICK));
            }
        });

        system_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickCount++;
                if (clickCount == 5) {
                    hide_layout.setVisibility(View.VISIBLE);
                }
            }
        });

        instructions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), InstructionsActivity.class);
                startActivity(intent);
            }
        });

        feedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), FeedbackActivity.class);
                startActivity(intent);
            }
        });

        test_model.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), FactoryTestActivity.class);
                startActivity(intent);
            }
        });

        system_set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Settings.ACTION_SETTINGS);
                startActivity(intent);
            }
        });

        vmallHttpBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                GlobalParams.getInstance().setVmallHttpDebug(b);
            }
        });

        upgradeTestBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                GlobalParams.getInstance().setUpgradeTestEnable(b);
            }
        });

    }
}
