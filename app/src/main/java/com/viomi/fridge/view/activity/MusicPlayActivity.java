package com.viomi.fridge.view.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.viomi.fridge.R;
import com.viomi.fridge.broadcast.BroadcastAction;
import com.viomi.fridge.manager.VoiceManager;
import com.viomi.fridge.util.log;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.Timer;
import java.util.TimerTask;

public class MusicPlayActivity extends BaseHandlerActivity {
    private final static String TAG=MusicPlayActivity.class.getSimpleName();
    private LocalBroadcastManager mBroadcastManager;
    private MediaPlayer mediaPlayer;

    private Button playButton;
    private SeekBar seekBar;
    private MyHandler mhandler;
    private ImageView rotate_cd;
    private TextView mTotalTextView;
    private TextView mExpendTextView;
    private int mMusicIndex;
    private String mPlayUrl;
    private Timer mTimer;
    private TimerTask mTimeTask;
    private final static int MSG_WHAT_PLAY=100;
    private TextView  mTitleText;
    private TextView mSubTitleText;
    public static  MusicPlayActivity   mInstance;


    public static class MyHandler extends Handler {
        private WeakReference<MusicPlayActivity> weakReference;

        public MyHandler(MusicPlayActivity activity) {
            weakReference = new WeakReference<MusicPlayActivity>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            MusicPlayActivity mActivity = weakReference.get();

            switch (msg.what) {
                case 0: {
                    int position = mActivity.mediaPlayer.getCurrentPosition();
                    int time = mActivity.mediaPlayer.getDuration();
                    int max = mActivity.seekBar.getMax();
                    int duration= position/1000;
                    String ten,ge;
                    if(duration/60>=10){
                        ten=""+duration/60;
                    }else {
                        ten="0"+duration/60;
                    }
                    if(duration%60>=10){
                        ge=""+duration%60;
                    }else {
                        ge="0"+duration%60;
                    }
                    String expendTime=ten+":"+ge;
                    mActivity.mExpendTextView.setText(expendTime);
                    mActivity.seekBar.setProgress(position * max / time);
                    if(position >=time){
                        mActivity.finish();
                        return;
                    }
                    mActivity.mhandler.sendEmptyMessageDelayed(0, 1000);
                    break;
                }

                case 1: {
                    mActivity.clickPlay();
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        log.d(TAG,"onCreate");
        mInstance=this;
        setContentView(R.layout.activity_music_play);
        mMusicIndex=getIntent().getIntExtra("music",0);
        mPlayUrl=getIntent().getStringExtra("playUrl");
        String mTitle=getIntent().getStringExtra("title");
        String mCategory=getIntent().getStringExtra("category");

        mTitleText= (TextView) findViewById(R.id.title_text);
        mSubTitleText= (TextView) findViewById(R.id.sub_title_text);
        if(mTitle!=null){
            mTitleText.setText(mTitle);
        }
        if(mCategory!=null){
            mSubTitleText.setText(mCategory);
        }
        mTotalTextView= (TextView) findViewById(R.id.total_text);
        mExpendTextView= (TextView) findViewById(R.id.expend_text);
        ImageView dismiss = (ImageView) findViewById(R.id.dismiss);
        ImageView forward = (ImageView) findViewById(R.id.forward);
        ImageView next = (ImageView) findViewById(R.id.next);
        playButton = (Button) findViewById(R.id.play);
        seekBar = (SeekBar) findViewById(R.id.seekBar);
        rotate_cd = (ImageView) findViewById(R.id.rotate_cd);

        mhandler = new MyHandler(this);
    //    mhandler.sendEmptyMessageDelayed(1, 3 * 1000);

        dismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaPlayer.stop();
                onBackPressed();
            }
        });

        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickPlay();
            }
        });

        seekBar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        if (mediaPlayer == null) {
            mediaPlayer = new MediaPlayer();
        }

        preparePlayMusic();
        register();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
       setIntent(intent);
        mMusicIndex=getIntent().getIntExtra("music",0);
        mPlayUrl=getIntent().getStringExtra("playUrl");
        String mTitle=getIntent().getStringExtra("title");
        String mCategory=getIntent().getStringExtra("category");
        if(mTitle!=null){
            mTitleText.setText(mTitle);
        }
        if(mCategory!=null){
            mSubTitleText.setText(mCategory);
        }
        playButton.setSelected(false);
        preparePlayMusic();

    }

    private void register() {
        mBroadcastManager = LocalBroadcastManager.getInstance(this);
        IntentFilter intentFilter = new IntentFilter(BroadcastAction.ACTION_STOP_MUSIC);
        mBroadcastManager.registerReceiver(myReceiver, intentFilter);
    }

    private BroadcastReceiver myReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getAction()) {
                case BroadcastAction.ACTION_STOP_MUSIC:
                    if(mediaPlayer!=null){
                        mediaPlayer.pause();
                    }
                    finish();
               //     VoiceManager.getInstance().startSpeak("停止播放音乐");
                    break;
            }

        }
    };

    private void unRegister() {
        mBroadcastManager.unregisterReceiver(myReceiver);
    }

    private void clickPlay() {
        if (playButton.isSelected()) {
            Log.i(TAG,"music pause!");
            mediaPlayer.pause();
            mhandler.removeMessages(0);
            rotate_cd.clearAnimation();
        } else {
            Log.i(TAG,"music play!");
            if(mPlayUrl==null){
                return;
            }
            mediaPlayer.start();
            mhandler.sendEmptyMessage(0);
            RotateAnimation ra = new RotateAnimation(0, 359, RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f);
            //设置旋转次数
            ra.setRepeatCount(RotateAnimation.INFINITE);
            //设置重复旋转的模式
            //ra.setRepeatMode(RotateAnimation.REVERSE);
            ra.setDuration(3000);
            ra.setInterpolator(new LinearInterpolator());
            rotate_cd.startAnimation(ra);
        }
        playButton.setSelected(!playButton.isSelected());
    }

    private void preparePlayMusic() {

        if(mPlayUrl==null){
            return;
        }
        //重置MediaPlayer
        mediaPlayer.reset();
        //设置播放数据源
        try {
            mediaPlayer.setDataSource(mPlayUrl);
            mediaPlayer.prepareAsync();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //设置是否循环播放
        mediaPlayer.setLooping(false);
        //准备播放
        mediaPlayer.setOnPreparedListener(mPreparedListener);

        //当音乐播放完成时调用
        mediaPlayer.setOnCompletionListener(mCompletionListener);
    }



    private MediaPlayer.OnCompletionListener mCompletionListener  = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mp) {
            Log.i(TAG,"onCompletion");
//            finish();
        }
    };

    private MediaPlayer.OnPreparedListener mPreparedListener = new MediaPlayer.OnPreparedListener() {
        @Override
        public void onPrepared(MediaPlayer mp) {
            log.d(TAG,"onPrepared");
            int duration= mp.getDuration()/1000;
            String ten,ge;
            if(duration/60>=10){
                ten=""+duration/60;
            }else {
                ten="0"+duration/60;
            }
            if(duration%60>=10){
                ge=""+duration%60;
            }else {
                ge="0"+duration%60;
            }
            String time=ten+":"+ge;
            mTotalTextView.setText(time);
            if(VoiceManager.getInstance().isSpeaking()){
                stopTimer();
                mTimer=new Timer();
                mTimeTask=new TimerTask() {
                    @Override
                    public void run() {
                        if(!VoiceManager.getInstance().isSpeaking()){
                            stopTimer();
                            if(mHandler!=null){
                               mHandler.sendEmptyMessage(MSG_WHAT_PLAY);
                            }
                        }
                    }
                };
                mTimer.schedule(mTimeTask,200,200);
            }else {
                clickPlay();
            }
        }
    };

    private void stopTimer(){
        if(mTimer!=null){
            mTimer.cancel();
            mTimer=null;
        }
        if(mTimeTask!=null){
            mTimeTask.cancel();
            mTimeTask=null;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mhandler != null) {
            mhandler.removeCallbacksAndMessages(null);
        }
        if (mediaPlayer != null) {
            mediaPlayer.release();
        }
        unRegister();
        stopTimer();
        mCompletionListener=null;
        mPreparedListener=null;
        mInstance=null;
    }

    @Override
    protected void processMsg(Message msg) {
        switch (msg.what){
            case MSG_WHAT_PLAY:
                clickPlay();
                break;
        }
    }
}
