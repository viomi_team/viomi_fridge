package com.viomi.fridge.view.widget;

import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;

import com.larswerkman.holocolorpicker.ColorPicker;
import com.viomi.fridge.R;
import com.viomi.fridge.util.log;

/**
 * 画笔编辑 Dialog
 * Created by William on 2017/6/21.
 */

public class PenEditDialog extends BaseDialog implements ColorPicker.OnColorChangedListener, SeekBar.OnSeekBarChangeListener {

    private static final String TAG = PenEditDialog.class.getSimpleName();
    private int size;   // 画笔大小缩放基准
    private int progress;
    private View view;
    private SeekBar seekBar;
    private ColorPicker colorPicker;
    private ImageView imageView;
    private PaletteView paletteView;

    public PenEditDialog(Context context, View view, SeekBar seekBar, ColorPicker colorPicker,
                         ImageView imageView, PaletteView paletteView, int size, int progress) {
        super(context);
        this.view = view;
        this.seekBar = seekBar;
        this.colorPicker = colorPicker;
        this.imageView = imageView;
        this.paletteView = paletteView;
        this.size = size;
        this.progress = progress;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = this.getWindow();
        if (window != null) {
            WindowManager.LayoutParams lp = window.getAttributes();
            lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(lp);
            window.setBackgroundDrawable(null);
            window.setDimAmount(0f);    // 背景无模糊
            window.setGravity(Gravity.CENTER);
            window.setWindowAnimations(R.style.popupWindow_anim_style);  // Dialog 动画
        }
        setCanceledOnTouchOutside(true);

        initView();
    }

    private void initView() {
        colorPicker.setOnColorChangedListener(this);
        seekBar.setOnSeekBarChangeListener(this);
        seekBar.setProgress(progress);
    }

    // 设置画笔大小
    private void setPenSize(int progress) {
        int calcProgress = progress > 1 ? progress : 1;
        int newSize = Math.round((size / 100f) * calcProgress);
        int offset = Math.round((size - newSize) / 2);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(newSize, newSize);
        lp.setMargins(offset, offset, offset, offset);
        imageView.setLayoutParams(lp);
        paletteView.setPenRawSize(newSize);
    }

    @Override
    public void setView() {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(view);
    }

    @Override
    public void onColorChanged(int color) {
        paletteView.setPenColor(color);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (seekBar.getId() == R.id.pen_size_seek) {     // 设置画笔大小
            setPenSize(progress);
            this.progress = progress;
            log.d(TAG, "current progress:" + this.progress);
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

}
