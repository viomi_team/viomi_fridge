package com.viomi.fridge.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.miot.api.MiotManager;
import com.miot.common.people.People;
import com.viomi.common.widget.SwitchButton;
import com.viomi.fridge.R;
import com.viomi.fridge.broadcast.BroadcastAction;
import com.viomi.fridge.manager.PushSetManager;
import com.viomi.fridge.model.bean.PushSetInfo;
import com.viomi.fridge.view.widget.BaseDialog;
import com.viomi.fridge.view.widget.TimePickerView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by young2 on 2017/6/16.
 */

public class InfoSetActivity extends BaseActivity implements View.OnClickListener{
    private SwitchButton mAdvertSwitchButton,mUserSwitchButton,mDeviceSwitchButton,mInfoTimeSwitchButton;
    private TextView mInfoTimeText;
    private TextView mTimeSeclectText ;
    private RelativeLayout mInfoTimeView;
    private boolean ignoreChange;
    private int mInfoStartTime,mInfoEndTime;
    private BaseDialog mMLAlertDialog;
    private int mStartTime,mEndTime;
    private static final String[] mDatas=new String[]{"0","1","2","3","4","5","6","7","8","9","10","11"
            ,"12","13","14","15","16","17","18","19","20","21","22","23"};
    private List<String> mDataListStart=new ArrayList<>();
    private List<String> mDataListEnd=new ArrayList<>();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        setContentView(R.layout.activity_info_setting);
        init();
    }

    private void init(){
        PushSetInfo pushSetInfo= PushSetManager.getPushSetInfo();
        mStartTime=pushSetInfo.getPushBlockTimeStart();
        mEndTime=pushSetInfo.getPushBlockTimeEnd();
        mAdvertSwitchButton= (SwitchButton) findViewById(R.id.advert_info_switch);
        mUserSwitchButton= (SwitchButton) findViewById(R.id.advert_user_switch);
        mDeviceSwitchButton= (SwitchButton) findViewById(R.id.advert_device_switch);
        mInfoTimeSwitchButton= (SwitchButton) findViewById(R.id.info_enable_switch);

        View backView = findViewById(R.id.icon_back);
        if(backView!=null) {
            backView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    finish();
                }
            });
        }
        mInfoTimeText= (TextView) findViewById(R.id.info_time_text);
        mInfoStartTime=pushSetInfo.getPushBlockTimeStart();
        mInfoEndTime=pushSetInfo.getPushBlockTimeEnd();
        for(int i=0;i<mDatas.length;i++){
            mDataListStart.add(mDatas[i]);
        }
        for(int i=0;i<mDatas.length;i++){
            mDataListEnd.add(mDatas[i]);
        }
        mInfoTimeText.setText(formatTime(mInfoStartTime)+"-"+formatTime(mInfoEndTime));
        mInfoTimeView= (RelativeLayout) findViewById(R.id.info_time_view);

        ignoreChange=true;
        if(pushSetInfo.isAdvertEnable()){
            mAdvertSwitchButton.setChecked(true);
        }else {
            mAdvertSwitchButton.setChecked(false);
        }
        ignoreChange=false;

        ignoreChange=true;
        if(pushSetInfo.isUserEnable()){
            mUserSwitchButton.setChecked(true);
        }else {
            mUserSwitchButton.setChecked(false);
        }
        ignoreChange=false;

        ignoreChange=true;
        if(pushSetInfo.isDeviceEnable()){
            mDeviceSwitchButton.setChecked(true);
        }else {
            mDeviceSwitchButton.setChecked(false);
        }
        ignoreChange=false;

        ignoreChange=true;
        if(pushSetInfo.isPushBlockEnable()){
            mInfoTimeView.setOnClickListener(this);
            mInfoTimeSwitchButton.setChecked(true);
        }else {
            mInfoTimeSwitchButton.setChecked(false);
            mInfoTimeView.setOnClickListener(null);
        }
        ignoreChange=false;


        mAdvertSwitchButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(!ignoreChange){
                    PushSetInfo pushSetInfo= PushSetManager.getPushSetInfo();
                    if(pushSetInfo.isAdvertEnable()!=isChecked){
                        pushSetInfo.setAdvertEnable(isChecked);
                        PushSetManager.savePushSetInfo(pushSetInfo);
                    }

                    Intent intentUserPush=new Intent(BroadcastAction.ACTION_REPORT_PUSH_ADVERT);
                    sendBroadcast(intentUserPush);
//                    GlobalParams.getInstance().setAdvertInfoEnable(isChecked);
               //     PushManagerPushManager.getInstance().advertPushSet();
                }
            }
        });

        mUserSwitchButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(!ignoreChange){
//                    GlobalParams.getInstance().setUserInfoEnable(isChecked);
                    PushSetInfo pushSetInfo= PushSetManager.getPushSetInfo();
                    if(pushSetInfo.isUserEnable()!=isChecked){
                        pushSetInfo.setUserEnable(isChecked);
                        PushSetManager.savePushSetInfo(pushSetInfo);
                    }
                    Intent intentUserPush=new Intent(BroadcastAction.ACTION_REPORT_PUSH_USER);
                    sendBroadcast(intentUserPush);
                }
            }
        });

        mDeviceSwitchButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(!ignoreChange){
                 //   GlobalParams.getInstance().setDeviceInfoEnable(isChecked);
                    PushSetInfo pushSetInfo= PushSetManager.getPushSetInfo();
                    if(pushSetInfo.isDeviceEnable()!=isChecked){
                        pushSetInfo.setDeviceEnable(isChecked);
                        PushSetManager.savePushSetInfo(pushSetInfo);
                    }

                    Intent intent=new Intent(BroadcastAction.ACTION_REPORT_PUSH_DEVICE);
                    String userId=null;
                    People mPeople = MiotManager.getPeople();
                    if(mPeople!=null){
                        userId=mPeople.getUserId();
                    }
                    intent.putExtra(BroadcastAction.EXTRA_PUSH,userId);
                    sendBroadcast(intent);
                }
            }
        });
        mInfoTimeSwitchButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(!ignoreChange){
                    PushSetInfo pushSetInfo= PushSetManager.getPushSetInfo();
                    if(pushSetInfo.isPushBlockEnable()!=isChecked){
                        pushSetInfo.setPushBlockEnable(isChecked);
                        PushSetManager.savePushSetInfo(pushSetInfo);
                    }
                   // GlobalParams.getInstance().setInfoTimeEnable(isChecked);
                    if(isChecked){
                        mInfoTimeView.setOnClickListener(InfoSetActivity.this);
                    }else {
                        mInfoTimeView.setOnClickListener(null);
                    }
                    Intent intentUserPush=new Intent(BroadcastAction.ACTION_REPORT_PUSH_SET_TIME);
                    sendBroadcast(intentUserPush);
                //    PushManager.getInstance().setAcceptTime(isChecked,GlobalParams.getInstance().getBlockInfoTimeStart(),GlobalParams.getInstance().getBlockInfoTimeEnd());
                }
            }
        });
    }

    private String formatTime(int hour){
        String result="";
      if(hour<10){
          result="0"+hour;
      }else {
          result=""+hour;
      }
        result+=":00";
       return result;
    }

    private void showTimerSet(){
        View view= LayoutInflater.from(this).inflate(R.layout.dialog_double_time_pick,null);
        TimePickerView time_picker_start= (TimePickerView) view.findViewById(R.id.time_picker_from);
        TimePickerView time_picker_end= (TimePickerView) view.findViewById(R.id.time_picker_to);
        mTimeSeclectText= (TextView) view.findViewById(R.id.time_seclect_text);
        PushSetInfo pushSetInfo= PushSetManager.getPushSetInfo();
        int startHour= pushSetInfo.getPushBlockTimeStart();
        int endHour=pushSetInfo.getPushBlockTimeEnd();
        mStartTime=startHour;
        mEndTime=endHour;
        mTimeSeclectText.setText(formatTime(startHour)+"-"+formatTime(endHour));
        time_picker_start.setData(mDataListStart);
        time_picker_end.setData(mDataListEnd);
        time_picker_start.setSelected(""+startHour);
        time_picker_end.setSelected(""+endHour);

        mMLAlertDialog=new BaseDialog(InfoSetActivity.this){
            @Override
            public void setView() {
                mMLAlertDialog.setContentView(view);
            }
        };
        mMLAlertDialog.show();

        TextView confirmButton= (TextView) view.findViewById(R.id.confirm_button);
        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mMLAlertDialog!=null){
                    mMLAlertDialog.dismiss();
                    mMLAlertDialog=null;
                }

                PushSetInfo pushSetInfo= PushSetManager.getPushSetInfo();
                pushSetInfo.setPushBlockTimeStart(mStartTime);
                pushSetInfo.setPushBlockTimeEnd(mEndTime);
                PushSetManager.savePushSetInfo(pushSetInfo);

                Intent intentUserPush=new Intent(BroadcastAction.ACTION_REPORT_PUSH_SET_TIME);
                sendBroadcast(intentUserPush);
                mInfoTimeText.setText(formatTime(mStartTime)+"-"+formatTime(mEndTime));

            }
        });
        time_picker_start.setOnSelectListener(new TimePickerView.onSelectListener() {
            @Override
            public void onSelect(String text) {
                int start=0;
                try {
                    start=Integer.parseInt(text);
                }catch (Exception e){
                    e.printStackTrace();
                }
                mStartTime=start;
             //   GlobalParams.getInstance().setBlockInfoTimeStart(start);
                mTimeSeclectText.setText(formatTime(start)+"-"+formatTime(mEndTime));
            }
        });
        time_picker_end.setOnSelectListener(new TimePickerView.onSelectListener() {
            @Override
            public void onSelect(String text) {
                int end=0;
                try {
                    end=Integer.parseInt(text);
                }catch (Exception e){
                    e.printStackTrace();
                }

                mEndTime=end;
               // GlobalParams.getInstance().setBlockInfoTimeEnd(end);
                mTimeSeclectText.setText(formatTime(mStartTime)+"-"+formatTime(end));
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mDataListStart=null;
        mDataListEnd=null;
        if(mMLAlertDialog!=null){
            mMLAlertDialog.dismiss();
            mMLAlertDialog=null;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.info_time_view:
                showTimerSet();
                break;
        }
    }
}
