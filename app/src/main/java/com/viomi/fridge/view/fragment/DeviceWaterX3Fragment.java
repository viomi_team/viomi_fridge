package com.viomi.fridge.view.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.miot.api.MiotManager;
import com.miot.common.people.People;
import com.viomi.common.module.okhttp.OkHttpClientManager;
import com.viomi.fridge.R;
import com.viomi.fridge.api.http.HttpConnect;
import com.viomi.fridge.device.AppConfig;
import com.viomi.fridge.manager.VoiceManager;
import com.viomi.fridge.model.bean.WaterPuriProp;
import com.viomi.fridge.model.parser.DeviceWaterX3Decode;
import com.viomi.fridge.util.log;
import com.viomi.fridge.view.widget.ExpandLayout;
import com.viomi.fridge.view.widget.WaterFilterDialog;
import com.viomi.fridge.view.widget.waveview.WaveHelper;
import com.viomi.fridge.view.widget.waveview.WaveView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.Call;

/**
 * X3，S1 优享版和S1 标准版净水器 Fragment
 * Created by William on 2017/4/14.
 */

public class DeviceWaterX3Fragment extends Fragment implements View.OnClickListener {

    private final static String TAG = DeviceWaterX3Fragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    private final static int MSG_WHAT_GET_PROP = 0;
    private final static int MSG_WHAT_GET_EXTRA_PROP = 1;
    private final static int MSG_WHAT_SET_PROP_RESPONSE = 2;
    private final static int MSG_WHAT_SET_PROP_FAIL = 3;
    private String did, mDeviceName, model;    // 设备Id，设备名称
    private View mView;
    private final MyHandler mHandler = new MyHandler(this);
    private People mPeople;
    private Timer mTimer;
    private TimerTask mTimeTask;
    private TextView tvOutTds, tvIntelLife, tvRoLife, tvPureLife, tvWaterTemp, tvUvState, tvWaterStatus;
    private TextView tvTempSetting, tvTempCustom, tvFlowSetting, tvFlowCustom1, tvFlowCustom2;
    private SeekBar tempBar, s_flow_bar, b_flow_bar;
    private float oneLife, twoLife, threeLife;
    private boolean mIsSetting = false;
    private WaveHelper mWaveHelper;
    private int skuid = 0;
    private boolean mIsVoiceOperate=false;//是否语音操作
    private int mMinTemp=-1;//最低可设置温度

    public static DeviceWaterX3Fragment newInstance(String param1, String param2, String param3) {
        DeviceWaterX3Fragment fragment = new DeviceWaterX3Fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        args.putString(ARG_PARAM3, param3);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            did = getArguments().getString(ARG_PARAM1);
            mDeviceName = getArguments().getString(ARG_PARAM2);
            model = getArguments().getString(ARG_PARAM3);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_connect_xthree, container, false);

        tvOutTds = (TextView) mView.findViewById(R.id.outTds);   // TDS
        tvIntelLife = (TextView) mView.findViewById(R.id.intelLife);   // 智能3合1滤芯
        tvRoLife = (TextView) mView.findViewById(R.id.roLife);   // RO反渗透滤芯
        tvPureLife = (TextView) mView.findViewById(R.id.pureLife);   // Pure Tank
        tvWaterTemp = (TextView) mView.findViewById(R.id.water_temp);      // 自来水水温
        tvUvState = (TextView) mView.findViewById(R.id.uv_state);  // UV杀菌
        tvWaterStatus = (TextView) mView.findViewById(R.id.make_status);  // 制水状态
        tvTempSetting = (TextView) mView.findViewById(R.id.temp_setting_tv1);   // 设置温度
        tvTempCustom = (TextView) mView.findViewById(R.id.temp_setting_tv2);   // 自定义温度
        tvFlowSetting = (TextView) mView.findViewById(R.id.flow_setting_tv1);  // 设置流量
        tvFlowCustom1 = (TextView) mView.findViewById(R.id.flow_setting_tv2);   // 自定义
        tvFlowCustom2 = (TextView) mView.findViewById(R.id.flow_setting_tv4);  // 自定义
        tempBar = (SeekBar) mView.findViewById(R.id.temp_bar);
        s_flow_bar = (SeekBar) mView.findViewById(R.id.s_flow_bar);
        b_flow_bar = (SeekBar) mView.findViewById(R.id.b_flow_bar);
        mView.findViewById(R.id.attr_three).setVisibility(View.VISIBLE);
        mView.findViewById(R.id.flow_setting_m_layout).setVisibility(View.GONE);

        TextView tvDeviceName = (TextView) mView.findViewById(R.id.device_name);
        tvDeviceName.setText(mDeviceName);

        init();
        initListener();
        startTimer();

        return mView;
    }

    @Override
    public void onPause() {
        super.onPause();
        mWaveHelper.cancel();
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onDetach() {
        super.onDetach();
        stopTimer();
    }

    public void onDestroy() {
        super.onDestroy();
        stopTimer();
        mHandler.removeCallbacksAndMessages(null);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (hidden) {
            stopTimer();
            mHandler.removeCallbacksAndMessages(null);
            mWaveHelper.cancel();
        }
    }

    private void init() {
        mPeople = MiotManager.getPeopleManager().getPeople();

        TextView tvFlow2Min = (TextView) mView.findViewById(R.id.low_big_cup);
        TextView tvFlow2Max = (TextView) mView.findViewById(R.id.high_big_cup);
        tvFlow2Min.setText("" + 310 + "ml");
        tvFlow2Max.setText("" + 1000 + "ml");

        WaveView waveView = (WaveView) mView.findViewById(R.id.wave_bg);
        waveView.setBorder(0, 0);
        waveView.setWaveColor(
                Color.parseColor("#FFFFFF"),
                Color.parseColor("#FFFFFF"));
        mWaveHelper = new WaveHelper(waveView);
        mWaveHelper.start();

        if (model.equals(AppConfig.YUNMI_WATERPURI_S1) || model.equals(AppConfig.YUNMI_WATERPURI_S2)) {
            mView.findViewById(R.id.water_temp_layout).setVisibility(View.GONE);
            mView.findViewById(R.id.water_flow_layout).setVisibility(View.GONE);
        }

        if (model.equals(AppConfig.YUNMI_WATERPURI_S2)) skuid = 20;
        else skuid = 30;

        tempBar.setMax(90 - 40);
        s_flow_bar.setMax(300 - 120);
        b_flow_bar.setMax(1000 - 120);
    }

    private void initListener() {
        final ExpandLayout tempSettingLayout = (ExpandLayout) mView.findViewById(R.id.temp_setting_layout);
        final ExpandLayout flowSettingLayout = (ExpandLayout) mView.findViewById(R.id.flow_setting_layout);
        final Button btnTemp = (Button) mView.findViewById(R.id.temp_setting);
        final Button btnFlow = (Button) mView.findViewById(R.id.flow_setting);

        tempSettingLayout.initExpand(false);
        flowSettingLayout.initExpand(false);

        mView.findViewById(R.id.temp_layout).setOnClickListener(v -> {
            btnTemp.setSelected(!btnTemp.isSelected());
            tempSettingLayout.toggleExpand();
        });

        mView.findViewById(R.id.flow_layout).setOnClickListener(v -> {
            btnFlow.setSelected(!btnFlow.isSelected());
            flowSettingLayout.toggleExpand();
        });

        tempBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                tvTempSetting.setText(40 + progress + "℃");
                tvTempCustom.setText(tempChose(40 + progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int temp = seekBar.getProgress() + 40;
                mIsSetting = true;
                setProp("set_tempe_setup", 1, temp);
            }
        });

        s_flow_bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                tvFlowSetting.setText(120 + progress + "ml");
                tvFlowCustom1.setText(120 + progress + "ml");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int temp = seekBar.getProgress() + 120;
                mIsSetting = true;
                setProp("set_flow_setup", 0, temp);
            }
        });

        b_flow_bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                tvFlowSetting.setText(310 + progress + "ml");
                tvFlowCustom2.setText(310 + progress + "ml");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int temp = seekBar.getProgress() + 310;
                mIsSetting = true;
                setProp("set_flow_setup", 1, temp);
            }
        });
    }

    private void startTimer() {
        stopTimer();
        int sleepTime = 5 * 1000;   // 执行间隔
        mTimer = new Timer();
        mTimeTask = new TimerTask() {
            @Override
            public void run() {
                getProp();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (!model.equals(AppConfig.YUNMI_WATERPURI_S1) && !model.equals(AppConfig.YUNMI_WATERPURI_S2))
                    getExtraProp();
            }
        };
        mTimer.schedule(mTimeTask, 0, sleepTime);
    }

    private void getProp() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "get_prop");
            jsonObject.put("did", did);
            jsonObject.put("id", 123);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        HttpConnect.rpcDevice(mPeople, did, jsonObject.toString(), new OkHttpClientManager.ResultCallback<String>() {
            @Override
            public void onError(Call call, Exception e) {
                log.myE(TAG, "getProp error,msg=" + e.getMessage());
            }

            @Override
            public void onResponse(String response) {
                log.d(TAG, "getProp=" + response);
                Message message = mHandler.obtainMessage();
                message.what = MSG_WHAT_GET_PROP;
                message.obj = response;
                mHandler.sendMessage(message);
            }
        });
    }

    private void getExtraProp() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "get_prop");
            jsonObject.put("did", did);
            jsonObject.put("id", 1);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put("setup_tempe");
            jsonArray.put("setup_flow");
            jsonArray.put("custom_tempe1");
            jsonArray.put("custom_flow0");
            jsonArray.put("custom_flow1");
            jsonArray.put("curr_tempe");
            jsonArray.put("curr_flow");
            jsonArray.put("min_set_tempe");
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        HttpConnect.rpcDevice(mPeople, did, jsonObject.toString(), new OkHttpClientManager.ResultCallback<String>() {
            @Override
            public void onError(Call call, Exception e) {
                log.myE(TAG, "getProp error,msg=" + e.getMessage());
            }

            @Override
            public void onResponse(String response) {
                log.d(TAG, "getPropX3=" + response);
                Message message = mHandler.obtainMessage();
                message.what = MSG_WHAT_GET_EXTRA_PROP;
                message.obj = response;
                mHandler.sendMessage(message);
            }
        });
    }

    private void setProp(String method, int index, int value) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", method);
            jsonObject.put("did", did);
            jsonObject.put("id", 12);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(index);
            jsonArray.put(value);
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        HttpConnect.rpcDevice(mPeople, did, jsonObject.toString(), new OkHttpClientManager.ResultCallback<String>() {
            @Override
            public void onError(Call call, Exception e) {
                log.myE(TAG, "setProp error,msg=" + e.getMessage());
                mHandler.sendEmptyMessage(MSG_WHAT_SET_PROP_FAIL);
                mIsSetting = false;
            }

            @Override
            public void onResponse(String response) {
                log.d(TAG, "setProp=" + response);
                Message message = mHandler.obtainMessage();
                message.what = MSG_WHAT_SET_PROP_RESPONSE;
                message.obj = response;
                mHandler.sendMessage(message);
                mIsSetting = false;
            }
        });
    }

    private void stopTimer() {
        if (mTimer != null) {
            mTimer.cancel();
            mTimer = null;
        }
        if (mTimeTask != null) {
            mTimeTask.cancel();
            mTimeTask = null;
        }
    }

    private String tempChose(int temp) {
        String str;
        switch (temp) {
            case 40:
                str = "40℃适合冲益生菌";
                break;
            case 50:
                str = "50℃适合冲奶/饮用";
                break;
            case 60:
                str = "60℃适合冲蜂蜜水";
                break;
            case 75:
                str = "75℃适合冲泡龙井";
                break;
            case 80:
                str = "80℃适合冲绿茶";
                break;
            case 85:
                str = "85℃适合冲泡大红袍/咖啡";
                break;
            case 90:
                str = "90℃适合冲泡铁观音";
                break;
            default:
                str = temp + "℃";
                break;
        }
        return str;
    }

    private void refreshPropView(WaterPuriProp waterPuriProp) {
        if (waterPuriProp.pTds < 100) {
            tvOutTds.setText(String.valueOf(waterPuriProp.pTds));
        }

        tvWaterTemp.setText("" + waterPuriProp.temp + "℃");
        if (waterPuriProp.uvStatus == 0) tvUvState.setText("空闲中");
        else tvUvState.setText("杀菌中");
        if (waterPuriProp.openStatus == 0) tvWaterStatus.setText("空闲中");
        else tvWaterStatus.setText("制水中");

        oneLife = waterPuriProp.oneLifeInt;
        twoLife = waterPuriProp.twoLifeInt;
        threeLife = waterPuriProp.threeLifeInt;

        tvIntelLife.setText("" + (int) oneLife + "%");
        tvRoLife.setText("" + (int) twoLife + "%");
        tvPureLife.setText("" + (int) threeLife + "%");

        mView.findViewById(R.id.intel_click).setOnClickListener(this);
        mView.findViewById(R.id.ro_click).setOnClickListener(this);
        mView.findViewById(R.id.pure_click).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        WaterFilterDialog waterFilterDialog = null;
        switch (v.getId()) {
            case R.id.intel_click:  // 智能3合1滤芯
                waterFilterDialog = new WaterFilterDialog(getActivity(), 5, oneLife, 21);
                break;
            case R.id.ro_click: // RO反渗透滤芯
                waterFilterDialog = new WaterFilterDialog(getActivity(), 6, twoLife, skuid);
                break;
            case R.id.pure_click:   // Pure Tank
                waterFilterDialog = new WaterFilterDialog(getActivity(), 7, threeLife, -1);
                break;
        }
        if (waterFilterDialog != null) waterFilterDialog.show();
    }

    private static class MyHandler extends Handler {
        WeakReference<DeviceWaterX3Fragment> weakReference;

        public MyHandler(DeviceWaterX3Fragment fragment) {
            this.weakReference = new WeakReference<>(fragment);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            DeviceWaterX3Fragment fragment = weakReference.get();
            switch (msg.what) {
                case MSG_WHAT_GET_PROP:
                    String result = (String) msg.obj;
                    if (result == null) return;
                    JSONObject jsonObject;
                    try {
                        jsonObject = new JSONObject(result);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        return;
                    }
                    WaterPuriProp waterPuriProp = DeviceWaterX3Decode.decode(jsonObject);
                    if (waterPuriProp == null) return;
                    fragment.refreshPropView(waterPuriProp);
                    break;
                case MSG_WHAT_GET_EXTRA_PROP:
                    String text = (String) msg.obj;
                    if (text == null) return;
                    JSONObject jsonObject1;
                    try {
                        jsonObject1 = new JSONObject(text);
                        JSONArray jsonArray = jsonObject1.getJSONArray("result");

                        int i = 0;
                        int setup_tempe;
                        int setup_flow;
                        int custom_tempe1;
                        int custom_flow0;
                        int custom_flow1;
                        int min_temp;

                        setup_tempe = jsonArray.optInt(i);
                        i++;
                        setup_flow = jsonArray.optInt(i);
                        i++;
                        custom_tempe1 = jsonArray.optInt(i);
                        i++;
                        custom_flow0 = jsonArray.optInt(i);
                        i++;
                        custom_flow1 = jsonArray.optInt(i);
                        i++;
                        i++;
                        i++;
                        min_temp = jsonArray.optInt(i);
                        fragment.mMinTemp=min_temp;

                        if (fragment.mIsSetting) return;

                        fragment.tvTempSetting.setText("" + setup_tempe + "℃");
                        fragment.tvTempCustom.setText(fragment.tempChose(custom_tempe1));

                        fragment.tvFlowSetting.setText("" + setup_flow + "ml");
                        fragment.tvFlowCustom1.setText("" + custom_flow0 + "ml");
                        fragment.tvFlowCustom2.setText("" + custom_flow1 + "ml");
                        fragment.tempBar.setProgress(custom_tempe1 - 40);
                        fragment.s_flow_bar.setProgress(custom_flow0 - 120);
                        fragment.b_flow_bar.setProgress(custom_flow1 - 310);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case MSG_WHAT_SET_PROP_FAIL:
                    Toast.makeText(fragment.getActivity(), "设置失败", Toast.LENGTH_SHORT).show();
                    if(fragment.mIsVoiceOperate){
                        VoiceManager.getInstance().startSpeak("设置失败，请重试");
                    }
                    fragment.mIsVoiceOperate=false;
                    break;
                case MSG_WHAT_SET_PROP_RESPONSE:
                    String response = (String) msg.obj;
                    if (response == null) return;
                    try {
                        JSONObject jsonObject2 = new JSONObject(response);
                        String message = jsonObject2.optString("message");
                        if (!"ok".equals(message)) {
                            Toast.makeText(fragment.getActivity(), "设置失败", Toast.LENGTH_SHORT).show();
                            if(fragment.mIsVoiceOperate){
                                VoiceManager.getInstance().startSpeak("设置失败，请重试");
                            }
                        }else {
                            if(fragment.mIsVoiceOperate){
                                VoiceManager.getInstance().startSpeak("已为您设置成功");
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }finally {
                        fragment.mIsVoiceOperate=false;
                    }
                    break;
            }
        }
    }

    /***
     * 语音控制
     * @param temp
     */
    public void tempVoiceControl(int temp){
        mIsVoiceOperate=true;
        setProp("set_tempe_setup", 1, temp);
    }

    /***
     * 获取最低可设置温度
     * @return
     */
    public int getMinTemp(){
        return mMinTemp;
    }

}
