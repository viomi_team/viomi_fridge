package com.viomi.fridge.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.viomi.fridge.R;
import com.viomi.fridge.model.bean.xunfei.XFMenuResp;
import com.viomi.fridge.model.bean.xunfei.XFNewsResp;
import com.viomi.fridge.util.log;

/**
 * Created by young2 on 2017/8/2.
 */

public class MenuShowActivity extends BaseActivity {
    private final static String TAG=MenuShowActivity.class.getSimpleName();
    private SimpleDraweeView mMenuLogo;
    private TextView mMenuTitle,mIngredient,mAccessory,mStepText;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        log.d(TAG,"onCreate");
        setContentView(R.layout.activity_menu_show);
        mMenuLogo= (SimpleDraweeView) findViewById(R.id.menu_logo);
        mMenuTitle= (TextView) findViewById(R.id.menu_title);
        mIngredient= (TextView) findViewById(R.id.menu_ingredient);
        mAccessory= (TextView) findViewById(R.id.menu_accessory);
        mStepText= (TextView) findViewById(R.id.menu_step);
        ImageView icon_back= (ImageView) findViewById(R.id.icon_back);
        icon_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private void init(){
        String  title= getIntent().getStringExtra("title");
        String ingredient=getIntent().getStringExtra("ingredient");
        String  accessory= getIntent().getStringExtra("accessory");
        String steps=getIntent().getStringExtra("steps");
        String imgUrl=getIntent().getStringExtra("imgUrl");

        mMenuLogo.setImageURI(imgUrl);

        mMenuTitle.setText(title);
        if(ingredient!=null&&ingredient.length()!=0){
            mIngredient.setText(ingredient);
        }else {
            mIngredient.setText("无");
        }
        if(accessory!=null&&accessory.length()!=0){
            mAccessory.setText(accessory);
        }else {
            mAccessory.setText("无");
        }
        if(steps!=null){
            String[] list= steps.split("。");
            String stepStr="";
            if(list==null||list.length<2){
                stepStr=steps;
            }else {
                for (int i=0;i<list.length;i++) {
                    stepStr+=list[i]+"\n";
                }
            }
            mStepText.setText(stepStr);
        }

    }



}
