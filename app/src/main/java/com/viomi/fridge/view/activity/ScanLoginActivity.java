package com.viomi.fridge.view.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.miot.api.MiotManager;
import com.miot.common.config.AppConfiguration;
import com.miot.common.exception.MiotException;
import com.miot.common.people.People;
import com.viomi.common.callback.AppCallback;
import com.viomi.common.util.GsonUtil;
import com.viomi.fridge.R;
import com.viomi.fridge.api.http.HttpConnect;
import com.viomi.fridge.api.http.VmallResponseCode;
import com.viomi.fridge.broadcast.BroadcastAction;
import com.viomi.fridge.common.GlobalParams;
import com.viomi.fridge.common.rxbus.BusEvent;
import com.viomi.fridge.common.rxbus.RxBus;
import com.viomi.fridge.device.AppConfig;
import com.viomi.fridge.device.DeviceManager;
import com.viomi.fridge.manager.AccountManager;
import com.viomi.fridge.manager.StatsManager;
import com.viomi.fridge.model.bean.QrCodeScanResult;
import com.viomi.fridge.model.bean.ViomiUser;
import com.viomi.fridge.util.JsonUitls;
import com.viomi.fridge.util.PhoneUtil;
import com.viomi.fridge.util.ResponseCode;
import com.viomi.fridge.util.ZhugeIoUtil;
import com.viomi.fridge.util.log;
import com.viomi.fridge.view.widget.BaseDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Call;

public class ScanLoginActivity extends BaseActivity {
    private final static String TAG = ScanLoginActivity.class.getSimpleName();

    private static final int MSG_WHAT_WAIT_FOR_SCAN_SUCCESS = 0;
    private static final int MSG_WHAT_WAIT_FOR_SCAN_FAIL = 1;
    private static final int MSG_WHAT_CHECK_LOGIN = 2;
    private static final int MSG_WHAT_CHECK_LOGIN_SUCCESS = 3;
    private static final int MSG_WHAT_CHECK_LOGIN_FAIL = 4;
    private static final int MSG_WHAT_LOGIN_FAIL = 6;
    private static final int MSG_WHAT_LOGIN_SUCCESS = 7;
    private static final int MSG_WHAT_LOGIN_TIMEOUT = 8;

    private MyHandler mHandler;
    private RelativeLayout loading_layout;
    private SimpleDraweeView qrcode_img;
    private CheckThread checkThread;
    private ImageView back_icon;
    private TextView how_to_use_view;
    private String mLoginType;
    private People mTempPeople;
    private ViomiUser mTempViomiUser;
    private AppCallback<String> mBindCallback;
    private ProgressDialog progressDialog = null;
    private String mClientId;
    private BaseDialog mMLAlertDialog;
    private Call mHttpCall;

    private static class MyHandler extends Handler {
        WeakReference<ScanLoginActivity> weakReference;

        public MyHandler(ScanLoginActivity activity) {
            this.weakReference = new WeakReference<ScanLoginActivity>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            ScanLoginActivity mActivity = weakReference.get();
            if (mActivity != null) {
                switch (msg.what) {
                    case MSG_WHAT_WAIT_FOR_SCAN_SUCCESS:
                        mActivity.loading_layout.setVisibility(View.GONE);
                        log.myE(TAG, "MSG_WHAT_WAIT_FOR_SCAN_SUCCESS,msg=" + msg.obj.toString());
                        mActivity.waitForScan((String) msg.obj);
                        break;

                    case MSG_WHAT_WAIT_FOR_SCAN_FAIL:
                        mActivity.qrcode_img.setVisibility(View.VISIBLE);
                        mActivity.qrcode_img.setImageResource(R.drawable.request_qrcode);
                        mActivity.loading_layout.setVisibility(View.GONE);
                        log.myE(TAG, "MSG_WHAT_WAIT_FOR_SCAN_FAIL,msg=" + msg.obj.toString());
                        Toast.makeText(mActivity, HttpConnect.ERROR_MESSAGE_HTTP_RESULT, Toast.LENGTH_SHORT).show();
                        break;

                    case MSG_WHAT_CHECK_LOGIN:
                        log.myE(TAG, "MSG_WHAT_CHECK_LOGIN @@@@@@@@@@@@@");
                        mActivity.checkLogin();
                        break;
                    case MSG_WHAT_CHECK_LOGIN_SUCCESS:
                        log.myE(TAG, "MSG_WHAT_CHECK_LOGIN_SUCCESS,msg=" + msg.obj.toString());
                        mActivity.parseJSON((String) msg.obj);
                        break;
                    case MSG_WHAT_CHECK_LOGIN_FAIL:
                        log.myE(TAG, "MSG_WHAT_CHECK_LOGIN_FAIL,msg=" + msg.obj.toString());
                        break;

                    case MSG_WHAT_LOGIN_FAIL:
                        if (mActivity.progressDialog != null) {
                            mActivity.progressDialog.dismiss();
                        }
                        String errorMsg = (String) msg.obj;
                        Toast.makeText(mActivity, mActivity.getString(R.string.toast_login_fail) + "!" + errorMsg, Toast.LENGTH_SHORT).show();
                        break;

                    case MSG_WHAT_LOGIN_SUCCESS:
                        log.myE(TAG, "MSG_WHAT_LOGIN_SUCCESS");
                        RxBus.getInstance().post(BusEvent.MSG_LOGIN_SUCCESS);
                        Toast.makeText(mActivity, mActivity.getString(R.string.toast_login_success), Toast.LENGTH_SHORT).show();
                        mActivity.finish();
                        break;

                    case MSG_WHAT_LOGIN_TIMEOUT:
                        Toast.makeText(mActivity, mActivity.getString(R.string.toast_login_time_out), Toast.LENGTH_SHORT).show();
                        mActivity.finish();
                        break;
                }
            }
        }
    }


    private static class CheckThread extends Thread {
        private WeakReference<ScanLoginActivity> weakReference;

        public CheckThread(ScanLoginActivity activity) {
            this.weakReference = new WeakReference<ScanLoginActivity>(activity);
        }

        @Override
        public void run() {
            while (!isInterrupted()) {
                ;
                ScanLoginActivity mActivity = weakReference.get();
                if (mActivity != null) {
                    mActivity.mHandler.sendEmptyMessage(MSG_WHAT_CHECK_LOGIN);
                    SystemClock.sleep(3000);
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_login);

        back_icon = (ImageView) findViewById(R.id.back_icon);
        loading_layout = (RelativeLayout) findViewById(R.id.loading_layout);
        qrcode_img = (SimpleDraweeView) findViewById(R.id.qrcode_img);
        how_to_use_view = (TextView) findViewById(R.id.how_to_use_view);
        mHandler = new MyHandler(this);
        requestQRImg();
        initListener();
    }


    public void showDownloadAppView() {
        final View view = LayoutInflater.from(ScanLoginActivity.this).inflate(R.layout.dialog_scan_qrcode, null);
        if (mMLAlertDialog == null) {
            mMLAlertDialog = new BaseDialog(ScanLoginActivity.this, R.style.fidge_dialog) {
                @Override
                public void setView() {
                    mMLAlertDialog.setContentView(view);
                }
            };
        }
        mMLAlertDialog.show();
        ImageView closeView = (ImageView) view.findViewById(R.id.colse_view);
        closeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMLAlertDialog.hideBar();
                mMLAlertDialog.dismiss();
                mMLAlertDialog = null;
            }
        });

    }


    //请求二维码
    private void requestQRImg() {
        loading_layout.setVisibility(View.VISIBLE);
        qrcode_img.setVisibility(View.INVISIBLE);
        Map<String, String> map = new HashMap<>();
        map.put("type", "1");
        mClientId = formatMac(PhoneUtil.getMiIdentify().mac) + System.currentTimeMillis() / 1000;
        map.put("clientID", mClientId);
        mHttpCall = HttpConnect.getRequestHandler(HttpConnect.qrcode_create_url, map, mHandler,
                MSG_WHAT_WAIT_FOR_SCAN_SUCCESS, MSG_WHAT_WAIT_FOR_SCAN_FAIL);
    }

    private String formatMac(String str) {
        if (str == null) {
            return "";
        }
        return str.replaceAll(":", "");
    }


    //显示二维码，等待登录
    private void waitForScan(String jsonStr) {
        try {
            JSONObject json = new JSONObject(jsonStr);
            JSONObject mobBaseRes = JsonUitls.getJSONObject(json, "mobBaseRes");
            String code = JsonUitls.getString(mobBaseRes, "code");
            String desc = JsonUitls.getString(mobBaseRes, "desc");
            if (ResponseCode.isSuccess(code, desc)) {
                String result = JsonUitls.getString(mobBaseRes, "result");
                Uri uri = Uri.parse(result);
                qrcode_img.setVisibility(View.VISIBLE);
                qrcode_img.setImageURI(uri);
            } else {
                //这里要处理重新获取二维码
                return;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //启动线程查询是否扫描成功
        if (checkThread != null) {
            checkThread.interrupt();
        }
        checkThread = new CheckThread(this);
        checkThread.start();
    }

    //查询是否登录成功
    private void checkLogin() {
        Map<String, String> paramsMap = new HashMap<>();
        paramsMap.put("clientID", mClientId);
        mHttpCall = HttpConnect.postRequestHandler(HttpConnect.check_login_status_url, paramsMap, mHandler, MSG_WHAT_CHECK_LOGIN_SUCCESS, MSG_WHAT_CHECK_LOGIN_FAIL);
    }

    private void parseJSON(String jsonStr) {
        log.d(TAG, "scan receve=" + jsonStr);

        try {
            QrCodeScanResult result = GsonUtil.parseJsonWithGson((String) jsonStr, QrCodeScanResult.class);
            if (result != null && result.mobBaseRes != null) {
                if (result.mobBaseRes.code == VmallResponseCode.SUCCESS && result.mobBaseRes.token != null
                        && result.mobBaseRes.appendAttr != null && result.mobBaseRes.loginData != null &&
                        result.mobBaseRes.loginData.userCode != null) {

                    ViomiUser viomiUser = AccountManager.parserViomiUser(result.mobBaseRes.appendAttr);
                    People people = AccountManager.parserMiPeople(result.mobBaseRes.appendAttr);

                    if (viomiUser != null && people != null) {
                        viomiUser.setToken(result.mobBaseRes.token);
                        viomiUser.setUserCode(result.mobBaseRes.loginData.userCode);
                        GlobalParams.getInstance().setUserId(result.mobBaseRes.loginData.userId);
                        log.d(TAG, "viomiUser success，token=" + viomiUser.getToken() + ",usercode=" + viomiUser.getUserCode());
                        if (checkThread != null) {
                            checkThread.interrupt();
                        }
                        mTempPeople = people;
                        mTempViomiUser = viomiUser;
                        if (mBindCallback == null) {
                            mBindCallback = new AppCallback<String>() {
                                @Override
                                public void onSuccess(String data) {
                                    Log.i(TAG, "bind success!");
//                                    miLogin(mTempPeople.getAccessToken(),expiresIn,mTempPeople.getMacKey(),mTempPeople.getMacAlgorithm());
                                    try {
                                        MiotManager.getPeopleManager().savePeople(mTempPeople);//保存小米帐号信息
                                    } catch (MiotException e) {
                                        Log.e(TAG, "savePeople fail!msg=" + e.getMessage());
                                        if (mHandler != null) {
                                            Message message = mHandler.obtainMessage();
                                            message.obj = "XiaomiAccountGetPeopleInfoTask  savePeople Fail！";
                                            message.arg1 = -101;
                                            message.what = MSG_WHAT_LOGIN_FAIL;
                                            mHandler.sendMessage(message);
                                        }
                                        return;
                                    }
                                    AccountManager.saveViomiUser(ScanLoginActivity.this, mTempViomiUser);//保存商城帐号信息
                                    log.d(TAG, "viomiUser save，token=" + mTempViomiUser.getToken() + ",usercode=" + mTempViomiUser.getUserCode());
                                    People people111 = MiotManager.getPeopleManager().getPeople();
                                    if (people111 != null) {
                                        log.d(TAG, "people=" + people111.toString());
                                    }
                                    if (mTempViomiUser.getType().equals("ios")) {
                                        GlobalParams.getInstance().setScanPhoneType(1);
                                    } else {
                                        GlobalParams.getInstance().setScanPhoneType(0);
                                    }

                                    AppConfiguration appConfig = new AppConfiguration();
                                    if (GlobalParams.getInstance().getScanPhoneType() == 0) {
                                        appConfig.setAppId(AppConfig.OAUTH_ANDROID_APP_ID);
                                        appConfig.setAppKey(AppConfig.OAUTH_ANDROID_APP_KEY);
                                    } else {
                                        appConfig.setAppId(AppConfig.OAUTH_IOS_APP_ID);
                                        appConfig.setAppKey(AppConfig.OAUTH_IOS_APP_KEY);
                                    }

                                    MiotManager.getInstance().setAppConfig(appConfig);

                                    ZhugeIoUtil.setUserInfo(mTempViomiUser);
                                    StatsManager.onProfileSignIn(StatsManager.USER_ID_VIOMI, mTempViomiUser.getAccount());
                                    StatsManager.onProfileSignIn(StatsManager.USER_ID_XIAOMI, mTempViomiUser.getMiId());
                                    GlobalParams.getInstance().setViomiLoginTime(System.currentTimeMillis() / 1000);
                                    ;
                                    Intent intentUserPush = new Intent(BroadcastAction.ACTION_REPORT_PUSH_USER);
                                    sendBroadcast(intentUserPush);
                                    if (checkThread != null) {
                                        checkThread.interrupt();
                                    }
                                    if (mHandler != null) {
                                        mHandler.sendEmptyMessage(MSG_WHAT_LOGIN_SUCCESS);
                                    }

                                }

                                @Override
                                public void onFail(int errorCode, String msg) {
                                    Log.e(TAG, "bind fail!errorCode=" + errorCode + ",msg=" + msg);
                                    if (mHandler != null) {
                                        Message message = mHandler.obtainMessage();
                                        message.obj = msg;
                                        message.arg1 = errorCode;
                                        message.what = MSG_WHAT_LOGIN_FAIL;
                                        mHandler.sendMessage(message);
                                    }
                                }
                            };
                        }
                        DeviceManager.getInstance().reBindDevice(viomiUser.getMiId(), mBindCallback);
                        progressDialog = (ProgressDialog) ProgressDialog.show(ScanLoginActivity.this, "请稍候...", "授权登录中...", true, true);
                        if (mHandler != null) {
                            mHandler.sendEmptyMessageDelayed(MSG_WHAT_LOGIN_TIMEOUT, 120000);
                        }
                    } else {
                        if (viomiUser == null) {
                            Log.e(TAG, "viomiUser null");
                        }
                        if (people == null) {
                            Log.e(TAG, "people null");
                        }
                    }
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "error msg=" + e.getMessage());
            e.printStackTrace();
        }

    }


    private void initListener() {

        back_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        how_to_use_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ScanLoginActivity.this, LoginScanGuideActivity.class));
            }
        });

        ((TextView) findViewById(R.id.download_app_view)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDownloadAppView();
            }
        });
        qrcode_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestQRImg();
            }
        });
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mMLAlertDialog != null) {
            mMLAlertDialog.dismiss();
            mMLAlertDialog = null;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
        if (checkThread != null) {
            checkThread.interrupt();
        }
        if (mHandler != null) {
            mHandler.weakReference.clear();
            mHandler.removeCallbacksAndMessages(null);
        }
        mBindCallback = null;
        if (mHttpCall != null) {
            mHttpCall.cancel();
        }
    }
}
