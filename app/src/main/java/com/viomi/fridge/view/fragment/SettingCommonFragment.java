package com.viomi.fridge.view.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.bigkoo.pickerview.TimePickerView;
import com.bigkoo.pickerview.listener.CustomListener;
import com.viomi.fridge.R;
import com.viomi.fridge.device.AppConfig;
import com.viomi.fridge.util.SDCardUtils;
import com.viomi.fridge.view.activity.BluetoothSetActivity;
import com.viomi.fridge.wifimodel.WifiScanActivity;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Mocc on 2017/11/1
 */

public class SettingCommonFragment extends BaseFragment {

    private static volatile SettingCommonFragment fragment;
    private RelativeLayout setting_wifi;
    private RelativeLayout setting_bluetooth;
    private RelativeLayout setting_time;
    private TextView memory_used;
    private TextView memory_total;
    private SeekBar memory_seekbar;

    private TimePickerView pvCustomTime;

    public static SettingCommonFragment getInstance() {
        if (fragment == null) {
            synchronized (SettingCommonFragment.class) {
                if (fragment == null) {
                    fragment = new SettingCommonFragment();
                }
            }
        }
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.setting_common_fragment_layout, null);

        setting_wifi = (RelativeLayout) view.findViewById(R.id.setting_wifi);
        setting_bluetooth = (RelativeLayout) view.findViewById(R.id.setting_bluetooth);
        setting_time = (RelativeLayout) view.findViewById(R.id.setting_time);
        memory_used = (TextView) view.findViewById(R.id.memory_used);
        memory_total = (TextView) view.findViewById(R.id.memory_total);
        memory_seekbar = (SeekBar) view.findViewById(R.id.memory_seekbar);

        memory_seekbar.setEnabled(false);

        initlistener();
        initCustomTimePicker();
        showMemory();
        return view;
    }

    private void showMemory() {
        //存储
        String totalMemory = String.format("%.2f", SDCardUtils.getSDCardTotalSize() / 1024f) + "GB";
        String usedMemory = String.format("%.2f", (SDCardUtils.getSDCardTotalSize()-SDCardUtils.getSDCardFreeSize() )/ 1024f) + "GB";
        memory_total.setText(totalMemory);
        memory_used.setText(usedMemory);
        int progress= (int) (((SDCardUtils.getSDCardTotalSize()-SDCardUtils.getSDCardFreeSize() +0d)/ SDCardUtils.getSDCardTotalSize())*100);
        memory_seekbar.setProgress(progress);
    }

    private void initlistener() {

        setting_wifi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), WifiScanActivity.class);
                startActivity(intent);
            }
        });

        //
        setting_bluetooth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, BluetoothSetActivity.class));
            }
        });

        setting_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pvCustomTime.show();
            }
        });
    }

    private void initCustomTimePicker() {
        // 注意：自定义布局中，id为 optionspicker 或者 timepicker 的布局以及其子控件必须要有，否则会报空指针
        // 具体可参考demo 里面的两个自定义布局
        //因为系统Calendar的月份是从0-11的,所以如果是调用Calendar的set方法来设置时间,月份的范围也要是从0-11
        //控制时间范围(如果不设置范围，则使用默认时间1900-2100年，此段代码可注释)
        Calendar selectedDate = Calendar.getInstance();//系统当前时间
        Calendar startDate = Calendar.getInstance();
        startDate.set(2017, 0, 1);
        Calendar endDate = Calendar.getInstance();
        endDate.set(2027, 2, 28);
        //时间选择器 ，自定义布局
        pvCustomTime = new TimePickerView.Builder(getActivity(), new TimePickerView.OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {//选中事件回调
                setSystemTime(date.getTime());
            }
        })
                .setDate(selectedDate)
                .setRangDate(startDate, endDate)
                .setLayoutRes(R.layout.system_time_setting_layout, new CustomListener() {

                    @Override
                    public void customLayout(View v) {
                        final TextView tvSubmit = (TextView) v.findViewById(R.id.yes_set);
                        TextView ivCancel = (TextView) v.findViewById(R.id.no_set);
                        tvSubmit.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                pvCustomTime.returnData();
                                getActivity().sendBroadcast(new Intent(AppConfig.ACTION_FOOD_MANAGE_UPDATE));
                            }
                        });
                        ivCancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                pvCustomTime.dismiss();
                            }
                        });
                    }
                })
                .setDividerColor(getResources().getColor(R.color.myTheme1))
                .build();
    }

    //设置系统时间
    private void setSystemTime(long time) {
        Intent intent = new Intent();
        intent.setAction("android.intent.action.USER_SET_TIME");
        intent.putExtra("time", time);
        getActivity().sendBroadcast(intent);
    }
}
