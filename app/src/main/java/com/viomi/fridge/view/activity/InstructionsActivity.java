package com.viomi.fridge.view.activity;

import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;

import com.viomi.fridge.R;
import com.viomi.fridge.api.http.HttpConnect;
import com.viomi.fridge.device.AppConfig;
import com.viomi.fridge.util.PhoneUtil;

import static com.viomi.fridge.R.id.webview;

public class InstructionsActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instructions);

        ImageView back = (ImageView) findViewById(R.id.back);
        WebView webView = (WebView) findViewById(webview);

        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);

        String url = null;
        String model = PhoneUtil.getDeviceModel();
        switch (model) {
            // 小鲜互联 云米智能冰箱三门 绿联主控
            case AppConfig.VIOMI_FRIDGE_V1:
                url = HttpConnect.INSTRUCTION;
                break;

            // 小鲜互联 云米智能冰箱四门 双鹿主控
            case AppConfig.VIOMI_FRIDGE_V2:
                url = HttpConnect.INSTRUCTION428;
                break;

            // 云米462大屏金属门冰箱
            case AppConfig.VIOMI_FRIDGE_V3:
            case AppConfig.VIOMI_FRIDGE_V31:
                url = HttpConnect.INSTRUCTION462;
                break;

            // 云米455大屏玻璃门冰箱
            case AppConfig.VIOMI_FRIDGE_V4:
                url = HttpConnect.INSTRUCTION455;
                break;
            default:
                url = HttpConnect.INSTRUCTION;
                break;
        }

        webView.loadUrl(url);

        back.setOnClickListener(v -> {
            onBackPressed();
        });
    }
}
