package com.viomi.fridge.view.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.lidroid.xutils.http.client.multipart.MultipartEntity;
import com.lidroid.xutils.http.client.multipart.content.FileBody;
import com.viomi.common.module.okhttp.OkHttpClientManager;
import com.viomi.fridge.R;
import com.viomi.fridge.api.http.HttpConnect;
import com.viomi.fridge.broadcast.BroadcastAction;
import com.viomi.fridge.common.ViomiApplication;
import com.viomi.fridge.device.AppConfig;
import com.viomi.fridge.model.bean.WebBaseData;
import com.viomi.fridge.service.BackgroudService;
import com.viomi.fridge.util.ApkUtil;
import com.viomi.fridge.util.FileUtil;
import com.viomi.fridge.util.OkHttpUtils;

import org.android.agoo.common.CallBack;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.util.EntityUtils;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import okhttp3.Call;

public class TestActivity extends Activity {

    private Intent backgroudServiceintent;
    private Button btn1, btn2, btn3;
    private int acount = 0;
    String savePath = "/sdcard/device/";//文件夹路径
    //    private static final String savePath = Environment.getExternalStorageState() + File.separator + "device";
    private static final String fileName = "temp.txt";
    private String mVmallUrl = HttpConnect.STOREMAINURL;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        btn1 = (Button) findViewById(R.id.btn_event1);
        btn2 = (Button) findViewById(R.id.btn_event2);
        btn3 = (Button) findViewById(R.id.btn_event3);
        OkHttpUtils.getInstance();
        initService();
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                String json = ApkUtil.getJson(TestActivity.this, "event1", "事件一");
//                FileUtil.writeTxtToFile(TestActivity.this, AppConfig.EventKey.MI_DEVICE_ID, "事件一");

//                acount++;
//                FileUtil.writeTxtToFile(TestActivity.this, AppConfig.EventKey.EVENT_ENTER_UNION, "事件" + acount);
                Intent intent = new Intent(TestActivity.this, VmallWebActivity.class);
                WebBaseData data = new WebBaseData();
                data.url = mVmallUrl + "/index.html";
                intent.putExtra(WebBaseData.Intent_String, data);
                startActivity(intent);
            }
        });

        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                String Path = filePath + filName;
//                File file = new File(Path);
//                if (file.exists()) {
//                    OkHttpUtils.getInstance().uploadFile(HttpConnect.UPLOAD_EVENT_FILE, file, new CallBack() {
//                        @Override
//                        public void onSuccess() {
//                            FileUtil.deleteChildFile(file);
//                        }
//                        @Override
//                        public void onFailure(String tips, String exception) {
//
//                        }
//                    });
//                }
            }
        });

        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                String Path = filePath + filName;
//                File file = new File(Path);
//                if (file.exists()) {
//                    String str = FileUtil.getFormatSize(file.length());
//                    Toast.makeText(TestActivity.this, str, Toast.LENGTH_LONG).show();
//                }
//                Intent intent = new Intent(BroadcastAction.ACTION_UPLOAD_EVENT);//出现任何一个门开报警，弹出报警界面
//                LocalBroadcastManager.getInstance(ViomiApplication.getContext()).sendBroadcast(intent);
            }
        });
    }

    private static String filePath = "/sdcard/device/";//文件夹路径
    private static final String filName = "temp.txt";

    private void initService() {
        backgroudServiceintent = new Intent(this, BackgroudService.class);
        startService(backgroudServiceintent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopService();
    }

    private void stopService() {
        stopService(backgroudServiceintent);
    }
}
