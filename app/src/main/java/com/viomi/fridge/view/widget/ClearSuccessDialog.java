package com.viomi.fridge.view.widget;

import android.content.Context;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.viomi.fridge.R;

/**
 * Created by Ljh on 2017/11/28
 */

public class ClearSuccessDialog extends BaseDialog implements View.OnClickListener{
    private Context mContext;
    private TextView tvInfo;
    boolean mCancelable = true;

    public ClearSuccessDialog(Context context) {
        super(context, R.style.DialogGrey);
        this.mContext = context;
    }

    public void setInfo(String info){
        tvInfo.setText(info);
    }

    @Override
    public void setView() {
        setContentView(R.layout.dialog_clear_success);
        tvInfo = (TextView) findViewById(R.id.tvInfo);
        RelativeLayout root = (RelativeLayout) findViewById(R.id.root);
        root.setOnClickListener(this);
    }

    @Override
    public void setCancelable(boolean flag) {
        mCancelable = flag;
        super.setCancelable(flag);
    }

    @Override
    public void onClick(View v) {
        if(mCancelable)
            this.cancel();
    }
}