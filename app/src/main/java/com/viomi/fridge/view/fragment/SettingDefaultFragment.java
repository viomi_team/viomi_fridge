package com.viomi.fridge.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.viomi.fridge.R;

/**
 * Created by Mocc on 2017/11/1
 */

public class SettingDefaultFragment extends BaseFragment {

    private static volatile SettingDefaultFragment fragment;

    public static SettingDefaultFragment getInstance() {
        if (fragment == null) {
            synchronized (SettingDefaultFragment.class) {
                if (fragment == null) {
                    fragment = new SettingDefaultFragment();
                }
            }
        }
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.setting_default_fragment_layout, null);
        return view;
    }
}
