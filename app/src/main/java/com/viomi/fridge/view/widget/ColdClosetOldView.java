package com.viomi.fridge.view.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.viomi.common.widget.SwitchButton;
import com.viomi.fridge.R;
import com.viomi.fridge.model.bean.SerialInfo;

/**
 * Created by young2 on 2017/1/3.
 */

public class ColdClosetOldView extends RelativeLayout {
    private OnTempChangeListener mTempChangeListener;
    private SeekBar mSeekBar;
    private TextView mTempView,mSetTempView,mStatusView;
    private SwitchButton mSwitchButton;
    private TextView mSwitchText;
    private int mSetTemp,mMinTemp,mMaxTemp,mCurrentTemp;
    private int mSeekbarLength=180;
    private int mModel;
    private boolean mIgnoreChange;

    public ColdClosetOldView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public ColdClosetOldView(Context context, AttributeSet attrs) {
        this(context,attrs,0);
    }

    public ColdClosetOldView(Context context) {
        this(context,null);
    }

    private void  init(Context context){
        View.inflate(context, R.layout.view_cold_closet_old,this);
        mMinTemp=2;
        mMaxTemp=8;
        mSeekbarLength=60;
        mSeekBar=(SeekBar)findViewById(R.id.seekbar);
        mTempView= (TextView) findViewById(R.id.temp);
        mSetTempView= (TextView) findViewById(R.id.set_temp_text);
        mStatusView= (TextView) findViewById(R.id.status);
        mSwitchButton= (SwitchButton) findViewById(R.id.switch_button);
        mSwitchText= (TextView) findViewById(R.id.switch_status);
        mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                int temp= (int) (((float)progress)*(mMaxTemp-mMinTemp)/mSeekbarLength+mMinTemp);
                mSetTempView.setText(temp+"℃");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                mSetTemp= (int) (((float)seekBar.getProgress())*(mMaxTemp-mMinTemp)/mSeekbarLength+mMinTemp);
                mSetTempView.setText(mSetTemp+"℃");
                if(mTempChangeListener!=null){
                    mTempChangeListener.onTempChange(mSetTemp);
                }
            }
        });
        mSwitchButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(mTempChangeListener!=null&&(!mIgnoreChange)){
                    if(!isChecked){
                        mSetTemp=SerialInfo.ROOM_CLOSE_TEMP;
                        mSeekBar.setEnabled(false);
                        mSwitchText.setText("off");
                        mSwitchText.setTextColor(getResources().getColor(R.color.switch_not_check_color));
                    }else {
                        mSetTemp=SerialInfo.COLD_COLSET_DEFAULT_TEMP;
                        mSeekBar.setEnabled(true);
                        mSwitchText.setText("on");
                        mSwitchText.setTextColor(getResources().getColor(R.color.switch_check_color));
                    }
                    mTempChangeListener.onTempChange(mSetTemp);
                }
            }
        });
    }


    /***
     * 初始化
     * @param setTemp
     * @param currentTemp
     */
    public void initData(int setTemp,int currentTemp,int mode){
        if(setTemp<mMinTemp){
            setTemp=mMinTemp;
        }else if(setTemp>mMaxTemp){
            setTemp=mMaxTemp;
        }
//        if(mode== SerialInfo.MODE_QUICK_COOL){
//            mStatusView.setVisibility(VISIBLE);
//        }else {
//            mStatusView.setVisibility(GONE);
//        }
        mSetTemp=setTemp;
        if(mSetTemp==SerialInfo.ROOM_CLOSE_TEMP){
            mIgnoreChange=true;
            mSwitchButton.setChecked(false);
            mIgnoreChange=false;
            mSeekBar.setEnabled(false);
            mSwitchText.setText("off");
            mSwitchText.setTextColor(getResources().getColor(R.color.switch_not_check_color));
        }else {
            mIgnoreChange=true;
            mSwitchButton.setChecked(true);
            mIgnoreChange=false;
            mSwitchText.setText("on");
            mSwitchText.setTextColor(getResources().getColor(R.color.switch_check_color));
            mSetTempView.setText(mSetTemp+"℃");
            int progress=(setTemp-mMinTemp)*mSeekbarLength/(mMaxTemp-mMinTemp);
            mSeekBar.setEnabled(true);
            mSeekBar.setProgress(progress);
        }
        mCurrentTemp=currentTemp;
        mTempView.setText(currentTemp+"℃");

    }

    /***
     * 设置温度
     * @param setTemp
     * @param currentTemp
     */
    public void setTemp(int setTemp,int currentTemp,int mode){
        if(setTemp<mMinTemp){
            setTemp=mMinTemp;
        }else if(setTemp>mMaxTemp){
            setTemp=mMaxTemp;
        }

        if(mModel!=mode){
            mModel=mode;
//            if(mode== SerialInfo.MODE_QUICK_COOL){
//                mStatusView.setVisibility(VISIBLE);
//            }else {
//                mStatusView.setVisibility(GONE);
//            }
        }

        if(mSetTemp!=setTemp){
            mSetTemp=setTemp;
            if(mSetTemp==SerialInfo.ROOM_CLOSE_TEMP){
                mIgnoreChange=true;
                mSwitchButton.setChecked(false);
                mIgnoreChange=false;
                mSeekBar.setEnabled(false);
                mSwitchText.setText("off");
                mSwitchText.setTextColor(getResources().getColor(R.color.switch_not_check_color));
            }else {
                mIgnoreChange=true;
                mSwitchButton.setChecked(true);
                mIgnoreChange=false;
                mSwitchText.setText("on");
                mSwitchText.setTextColor(getResources().getColor(R.color.switch_check_color));
                mSetTempView.setText(mSetTemp+"℃");
                int progress=(setTemp-mMinTemp)*mSeekbarLength/(mMaxTemp-mMinTemp);
                mSeekBar.setEnabled(true);
                mSeekBar.setProgress(progress);
            }
        }
        if(mCurrentTemp!=currentTemp){
            mCurrentTemp=currentTemp;
            mTempView.setText(currentTemp+"℃");
        }
    }


    /***
     * 滑动修改温度监听
     * @param listener
     */
    public void setOnTempChangeListener(OnTempChangeListener listener){
        mTempChangeListener=listener;
    }

    public interface OnTempChangeListener {
        void onTempChange(int temp);
    }

    public void  close(){
        mTempChangeListener=null;
    }
}
