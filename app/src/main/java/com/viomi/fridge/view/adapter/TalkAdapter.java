package com.viomi.fridge.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.viomi.fridge.R;
import com.viomi.fridge.model.bean.TalkInfo;

import java.util.List;

/**
 * Created by young2 on 2017/5/5.
 */

public class TalkAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private List<TalkInfo> mList;

    public TalkAdapter(List<TalkInfo> list, Context context){
        mList=list;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_talk, null);
            holder = new ViewHolder();
            holder.listenVoiceView = (TextView) convertView.findViewById(R.id.listen_voice_text);
            holder.talkVoiceView = (TextView) convertView.findViewById(R.id.talk_voice_text);
            holder.icon_talk=(ImageView) convertView.findViewById(R.id.icon_talk);
            holder.icon_listen=(ImageView) convertView.findViewById(R.id.icon_listen);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
       TalkInfo talkInfo= mList.get(position);
        if(talkInfo.talkType==TalkInfo.TYPE_LISTEN){
            holder.listenVoiceView.setText(talkInfo.msg);
            holder.talkVoiceView.setVisibility(View.GONE);
            holder.listenVoiceView.setVisibility(View.VISIBLE);
            holder.icon_talk.setVisibility(View.GONE);
            holder.icon_listen.setVisibility(View.VISIBLE);
        }else {
            holder.talkVoiceView.setText(talkInfo.msg);
            holder.talkVoiceView.setVisibility(View.VISIBLE);
            holder.icon_talk.setVisibility(View.VISIBLE);
            holder.listenVoiceView.setVisibility(View.GONE);
            holder.icon_listen.setVisibility(View.GONE);
        }
        return convertView;
    }

    class ViewHolder{
        ImageView icon_talk;
        ImageView icon_listen;
        TextView listenVoiceView;
        TextView talkVoiceView;
    }
}
