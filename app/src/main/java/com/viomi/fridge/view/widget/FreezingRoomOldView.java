package com.viomi.fridge.view.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.viomi.fridge.R;
import com.viomi.fridge.model.bean.SerialInfo;

/**
 * Created by young2 on 2017/1/3.
 */

public class FreezingRoomOldView extends RelativeLayout {
    private OnTempChangeListener mTempChangeListener;
    private SeekBar mSeekBar;
    private TextView mTempView,mSetTempView,mStatusView;
    private int mSetTemp,mMinTemp,mMaxTemp,mCurrentTemp;
    private int mSeekbarLength=180;
    private int mModel;

    public FreezingRoomOldView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public FreezingRoomOldView(Context context, AttributeSet attrs) {
        this(context,attrs,0);
    }

    public FreezingRoomOldView(Context context) {
        super(context,null);
    }

    private void  init(Context context){

        mMinTemp=-25;
        mMaxTemp=-15;
        mSeekbarLength=100;
        View.inflate(context, R.layout.view_freezing_room_old,this);
        mSeekBar=(SeekBar)findViewById(R.id.seekbar);
        mTempView= (TextView) findViewById(R.id.temp);
        mSetTempView= (TextView) findViewById(R.id.set_temp_text);
        mStatusView= (TextView) findViewById(R.id.status);
        mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                int temp= (int) (((float)progress)*(mMaxTemp-mMinTemp)/mSeekbarLength+mMinTemp);
                 mSetTempView.setText(temp+"℃");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                mSetTemp= (int) (((float)seekBar.getProgress())*(mMaxTemp-mMinTemp)/mSeekbarLength+mMinTemp);
                mSetTempView.setText(mSetTemp+"℃");
                if(mTempChangeListener!=null){
                    mTempChangeListener.onTempChange(mSetTemp);
                }
            }
        });
    }

    /***
     * 初始化
     * @param setTemp
     * @param currentTemp
     */
    public void initData(int setTemp,int currentTemp,int mode){
        if(setTemp<mMinTemp){
            setTemp=mMinTemp;
        }else if(setTemp>mMaxTemp){
            setTemp=mMaxTemp;
        }
//        if(mode== SerialInfo.MODE_QUICK_FREEZE){
//            mStatusView.setVisibility(VISIBLE);
//        }else {
//            mStatusView.setVisibility(GONE);
//        }
        mSetTemp=setTemp;
        int progress=(setTemp-mMinTemp)*mSeekbarLength/(mMaxTemp-mMinTemp);
        mSetTempView.setText(mSetTemp+"℃");
        mSeekBar.setProgress(progress);
        mCurrentTemp=currentTemp;
        mTempView.setText(currentTemp+"℃");

    }

    /***
     * 设置温度
     * @param setTemp
     * @param currentTemp
     */
    public void setTemp(int setTemp,int currentTemp,int mode){
        if(setTemp<mMinTemp){
            setTemp=mMinTemp;
        }else if(setTemp>mMaxTemp){
            setTemp=mMaxTemp;
        }

        if(mModel!=mode){
            mModel=mode;
//            if(mode== SerialInfo.MODE_QUICK_FREEZE){
//                mStatusView.setVisibility(VISIBLE);
//            }else {
//                mStatusView.setVisibility(GONE);
//            }
        }

        if(mSetTemp!=setTemp){
            mSetTemp=setTemp;
            mSetTempView.setText(mSetTemp+"℃");
            int progress=(setTemp-mMinTemp)*mSeekbarLength/(mMaxTemp-mMinTemp);
            mSeekBar.setProgress(progress);
        }
        if(mCurrentTemp!=currentTemp){
            mCurrentTemp=currentTemp;
            mTempView.setText(currentTemp+"℃");
        }


    }

    /***
     * 滑动修改温度监听
     * @param listener
     */
    public void setOnTempChangeListener(OnTempChangeListener listener){
        mTempChangeListener=listener;
    }

    public interface OnTempChangeListener {
        void onTempChange(int temp);
    }

    public void  close(){
        mTempChangeListener=null;
    }
}
