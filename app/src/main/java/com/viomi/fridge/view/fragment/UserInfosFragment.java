package com.viomi.fridge.view.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;


import com.viomi.common.callback.AppCallback;
import com.viomi.fridge.R;
import com.viomi.fridge.api.dao.InfoManager;
import com.viomi.fridge.api.dao.UserInfoMessage;
import com.viomi.fridge.api.http.HttpConnect;
import com.viomi.fridge.common.GlobalParams;
import com.viomi.fridge.device.AppConfig;
import com.viomi.fridge.device.DeviceConfig;
import com.viomi.fridge.manager.AccountManager;
import com.viomi.fridge.model.bean.ViomiUser;
import com.viomi.fridge.util.log;
import com.viomi.fridge.view.adapter.UserInfosAdapter;
import com.viomi.fridge.view.widget.BaseDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * Created by young2 on 2017/6/14.
 */

public class UserInfosFragment extends BaseHandlerFragment {
    private final static String TAG= UserInfosFragment.class.getSimpleName();
    private View mFragment;
    private RecyclerView mListView;
    private UserInfosAdapter mUserInfosAdapter;
    private LinearLayoutManager mLayoutManager;
    private List<UserInfoMessage> mDataList;
    private ProgressBar mLoadingView;
    private LinearLayout mEmptyImageView;
    private boolean mIsViewInitiated=false;
    private BaseDialog mMLAlertDialog;
    private RefreshUnreadInfoListenr mRefreshUnreadInfoListenr;
    private static final int MSG_WHAT_INIT_DATA=1;
    private   int mNewsCount;

    public interface RefreshUnreadInfoListenr{
        void refreshUserInfo(int count);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mRefreshUnreadInfoListenr= (RefreshUnreadInfoListenr) context;
        }catch (ClassCastException e){
            throw new ClassCastException(context.toString()+"must implement mRefreshUnreadInfoView");
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mRefreshUnreadInfoListenr= (RefreshUnreadInfoListenr) activity;
        }catch (ClassCastException e){
            throw new ClassCastException(activity.toString()+"must implement mRefreshUnreadInfoView");
        }
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mainView = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_user_info, null, false);
        mFragment=mainView;
        return mainView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
    }

    private void init(){
        mIsViewInitiated=true;
        mNewsCount=getArguments().getInt("newsCount");
        mListView= (RecyclerView) mFragment.findViewById(R.id.listview);
        mDataList=new ArrayList<>();
        mLayoutManager=new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false);
        mListView.setLayoutManager(mLayoutManager);
        mUserInfosAdapter =new UserInfosAdapter(getActivity(),this,mDataList);
        mListView.setAdapter(mUserInfosAdapter);
        mListView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                super.getItemOffsets(outRect, view, parent, state);
            }
        });
        mEmptyImageView= (LinearLayout) mFragment.findViewById(R.id.info_empty_view);
        mLoadingView= (ProgressBar) mFragment.findViewById(R.id.loading_view);

    }

    public void refreshRecordsReadStatus(){
        List<UserInfoMessage> list= InfoManager.getInstance().getUserNews();
        if(list!=null&&list.size()>0){
            for (int i=0;i<list.size();i++){
                UserInfoMessage info=list.get(i);
                InfoManager.getInstance().readUserInfo(info);
            }
        }
    }

    /***
     * 删除消息
     * @param infoId
     */
    public void deleteInfo(final int infoId){
        final View view=LayoutInflater.from(getActivity()).inflate(R.layout.dialog_confirm,null);
        mMLAlertDialog=new BaseDialog(getActivity()){
            @Override
            public void setView() {
                mMLAlertDialog.setContentView(view);
            }
        };
        mMLAlertDialog.show();
        TextView titleView= (TextView) view.findViewById(R.id.message_text);
        TextView confirmView= (TextView) view.findViewById(R.id.confirm_button);
        TextView cancelView= (TextView) view.findViewById(R.id.cancel_button);
        titleView.setText(getString(R.string.title_is_info_delete));
        cancelView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMLAlertDialog.hideBar();
                mMLAlertDialog.dismiss();
                mMLAlertDialog=null;
            }
        });
        confirmView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMLAlertDialog.dismiss();
                mMLAlertDialog=null;
                UserInfoMessage info= InfoManager.getInstance().getUserInfoById(infoId);
                InfoManager.getInstance().deleteUserInfo(info);
                dataInit();
            }
        });
    }

    /***
     * s刷新显示数据
     */
    public void dataInit(){
        mLoadingView.setVisibility(View.GONE);
        List<UserInfoMessage>  dataList= InfoManager.getInstance().getUserInfoRecords();
        if(dataList==null||dataList.size()<=0){
            mEmptyImageView.setVisibility(View.VISIBLE);
            mListView.setVisibility(View.GONE);
        }else {
            mEmptyImageView.setVisibility(View.GONE);
            mListView.setVisibility(View.VISIBLE);
            mDataList.clear();
            Collections.reverse(dataList);
            mDataList.addAll(dataList);
            mUserInfosAdapter.notifyDataSetChanged();
        }

    }



    /***
     * 加载新消息
     */
    private synchronized void loadNewInfo(){
        ViomiUser viomiUser= AccountManager.getViomiUser(getActivity());
        if(viomiUser==null){
            return;
        }
        int lastUserId= InfoManager.getInstance().getLastUserInfoId();
        if(!AppConfig.VIOMI_FRIDGE_V31.equals(DeviceConfig.MODEL)){
            HttpConnect.getUserInfos(viomiUser.getAccount(),lastUserId, new AppCallback<String>() {
                @Override
                public void onSuccess(String data) {
                    if(data!=null){
                        try {
                            long startTime= GlobalParams.getInstance().getViomiLoginTime();
                            JSONArray jsonArray=new JSONArray(data);
                            if(jsonArray!=null&&jsonArray.length()>0){
                                for (int i=jsonArray.length()-1;i>=0;i--){
                                    JSONObject jsonObject=  jsonArray.getJSONObject(i);
                                    UserInfoMessage info=new UserInfoMessage();
                                    info.setInfoId(jsonObject.getInt("id"));
                                    info.setTopic(jsonObject.getString("topic"));
                                    info.setTime(jsonObject.getLong("time")/1000);
                                    info.setTitle(formatStrNull(jsonObject.optString("title")));
                                    info.setContent(formatStrNull(jsonObject.optString("content")));
                                    info.setImgUrl(formatStrNull(jsonObject.optString("imgurl")));
                                    info.setLinkUrl(formatStrNull(jsonObject.optString("accessurl")));
                                    info.setDelete(false);
                                    if(startTime>info.getTime()){
                                        info.setRead(true);
                                    }else {
                                        info.setRead(false);
                                    }
                                    InfoManager.getInstance().addUserInfoRecord(info);
                                }
                            }
                        } catch (JSONException e) {
                            Log.e(TAG,"getUserInfos parse fail!msg="+e.getMessage());
                            e.printStackTrace();
                        }
                    }
                    if (mHandler!=null){
                        mHandler.sendEmptyMessage(MSG_WHAT_INIT_DATA);
                    }
                }

                @Override
                public void onFail(int errorCode, String msg) {
                    Log.e(TAG,"loadNewInfo fail!msg="+msg);
                    if (mHandler!=null){
                        mHandler.sendEmptyMessage(MSG_WHAT_INIT_DATA);
                    }
                }
            });
        }

    }

    private String formatStrNull(String str){
        if(str==null||str.length()==0||str.equals("null")){
            return null;
        }
        return str;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        log.d(TAG,"---setUserVisibleHint,isVisibleToUser="+isVisibleToUser);
        if(mIsViewInitiated&&isVisibleToUser){
            if(mNewsCount>0){
                mLoadingView.setVisibility(View.VISIBLE);
                mEmptyImageView.setVisibility(View.GONE);
                mListView.setVisibility(View.GONE);
                loadNewInfo();
            }else {
                if (mHandler!=null){
                    mHandler.sendEmptyMessage(MSG_WHAT_INIT_DATA);
                }
            }
            if(mRefreshUnreadInfoListenr!=null){
                mRefreshUnreadInfoListenr.refreshUserInfo(0);
            }
        }
    }

    @Override
    protected void processMsg(Message msg) {
        switch (msg.what){
            case MSG_WHAT_INIT_DATA:
                refreshRecordsReadStatus();
                dataInit();
                break;
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if(mMLAlertDialog!=null){
            mMLAlertDialog.dismiss();
            mMLAlertDialog=null;
        }
    }


}
