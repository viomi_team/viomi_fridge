package com.viomi.fridge.view.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.miot.api.MiotManager;
import com.miot.common.people.People;
import com.viomi.common.module.okhttp.OkHttpClientManager;
import com.viomi.fridge.R;
import com.viomi.fridge.api.http.HttpConnect;
import com.viomi.fridge.device.AppConfig;
import com.viomi.fridge.model.bean.WaterPuriProp;
import com.viomi.fridge.model.parser.DeviceGetPropDecode;
import com.viomi.fridge.util.log;
import com.viomi.fridge.view.widget.WaterFilterDialog;
import com.viomi.fridge.view.widget.waveview.WaveHelper;
import com.viomi.fridge.view.widget.waveview.WaveView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.Call;

/**
 * 无设置量净水器 Fragment
 * Created by William on 2017/4/10.
 */

public class DeviceWaterPuriFierFragment extends Fragment implements View.OnClickListener {

    private final static String TAG = DeviceWaterPuriFierFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    private final static int MSG_WHAT_GET_PROP = 0;
    private final static int MSG_WHAT_GET_EXTRA_PROP = 1;
    private String did, mDeviceName, model;    // 设备Id，设备名称
    private float oneLife, twoLife, threeLife, fourLife;
    private int skuid = 0;
    private TextView tvOutTds;
    private TextView tvPpLife, tvC1Life, tvRoLife, tvC2Life, tvWaterTemp, tvUvState, tvWaterPress, tvElecval;
    private People mPeople;
    private Timer mTimer;
    private TimerTask mTimeTask;
    private View mView;
    private WaveHelper mWaveHelper;
    private final MyHandler mHandler = new MyHandler(this);

    public static DeviceWaterPuriFierFragment newInstance(String param1, String param2, String param3) {
        DeviceWaterPuriFierFragment fragment = new DeviceWaterPuriFierFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        args.putString(ARG_PARAM3, param3);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            did = getArguments().getString(ARG_PARAM1);
            mDeviceName = getArguments().getString(ARG_PARAM2);
            model = getArguments().getString(ARG_PARAM3);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_connect_vtwo, container, false);

        tvOutTds = (TextView) mView.findViewById(R.id.outTds);   // TDS
        tvPpLife = (TextView) mView.findViewById(R.id.ppLife);   // PP棉滤芯
        tvC1Life = (TextView) mView.findViewById(R.id.c1Life);   // 前置活性炭滤芯
        tvRoLife = (TextView) mView.findViewById(R.id.roLife);   // RO反渗透滤芯
        tvC2Life = (TextView) mView.findViewById(R.id.c2Life);   // 后置活性炭滤芯
        tvWaterTemp = (TextView) mView.findViewById(R.id.water_temp);      // 自来水水温
        tvUvState = (TextView) mView.findViewById(R.id.uv_state);  // UV杀菌
        tvWaterPress = (TextView) mView.findViewById(R.id.press);  // 水压
        tvElecval = (TextView) mView.findViewById(R.id.elecval_state);    // 智能三通阀

        switch (model) {
            case AppConfig.YUNMI_WATERPURI_V1:
            case AppConfig.YUNMI_WATERPURI_V2:
                mView.findViewById(R.id.attr_one).setVisibility(View.VISIBLE);
                mView.findViewById(R.id.attr_two).setVisibility(View.VISIBLE);
                break;
            case AppConfig.YUNMI_WATERPURI_C1:
            case AppConfig.YUNMI_WATERPURI_C2:
                mView.findViewById(R.id.attr_one).setVisibility(View.VISIBLE);
                mView.findViewById(R.id.attr_two).setVisibility(View.GONE);
                break;
            default:
                mView.findViewById(R.id.attr_one).setVisibility(View.GONE);
                mView.findViewById(R.id.attr_two).setVisibility(View.GONE);
                break;
        }

        if (model.equals(AppConfig.YUNMI_WATERPURI_V2)) skuid = 19;
        else skuid = 18;

        TextView tvDeviceName = (TextView) mView.findViewById(R.id.device_name);
        tvDeviceName.setText(mDeviceName);

        init();
        startTimer();

        return mView;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        stopTimer();
    }

    public void onDestroy() {
        super.onDestroy();
        stopTimer();
        mHandler.removeCallbacksAndMessages(null);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (hidden) {
            stopTimer();
            mHandler.removeCallbacksAndMessages(null);
            mWaveHelper.cancel();
        }
    }

    private void init() {
        mPeople = MiotManager.getPeopleManager().getPeople();

        WaveView waveView = (WaveView) mView.findViewById(R.id.wave_bg);
        waveView.setBorder(0, 0);
        waveView.setWaveColor(
                Color.parseColor("#FFFFFF"),
                Color.parseColor("#FFFFFF"));
        mWaveHelper = new WaveHelper(waveView);
        mWaveHelper.start();
    }

    private void startTimer() {
        stopTimer();
        int sleepTime = 5 * 1000;   // 执行间隔
        mTimer = new Timer();
        mTimeTask = new TimerTask() {
            @Override
            public void run() {
                getProp();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (model.equals(AppConfig.YUNMI_WATERPURI_V1) || model.equals(AppConfig.YUNMI_WATERPURI_V2)
                        || model.equals(AppConfig.YUNMI_WATERPURI_C1) || model.equals(AppConfig.YUNMI_WATERPURI_C2))
                    getExtraProp();
            }
        };
        mTimer.schedule(mTimeTask, 0, sleepTime);
    }

    private void getProp() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "get_prop");
            jsonObject.put("did", did);
            jsonObject.put("id", 123);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        HttpConnect.rpcDevice(mPeople, did, jsonObject.toString(), new OkHttpClientManager.ResultCallback<String>() {
            @Override
            public void onError(Call call, Exception e) {
                Log.e(TAG, "getProp error,msg=" + e.getMessage());
            }

            @Override
            public void onResponse(String response) {
                log.d(TAG, "getProp=" + response);
                Message message = mHandler.obtainMessage();
                message.what = MSG_WHAT_GET_PROP;
                message.obj = response;
                mHandler.sendMessage(message);
            }
        });
    }

    private void getExtraProp() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "get_prop");
            jsonObject.put("did", did);
            jsonObject.put("id", 1);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put("temperature");
            jsonArray.put("uv_state");
            jsonArray.put("press");
            jsonArray.put("elecval_state");
            jsonObject.put("params", jsonArray);
        } catch (JSONException e1) {
            e1.printStackTrace();
        }

        HttpConnect.rpcDevice(mPeople, did, jsonObject.toString(), new OkHttpClientManager.ResultCallback<String>() {
            @Override
            public void onError(Call call, Exception e) {
                Log.e(TAG, "getProp error,msg=" + e.getMessage());
            }

            @Override
            public void onResponse(String response) {
                log.d(TAG, "getPropX5=" + response);
                Message message = mHandler.obtainMessage();
                message.what = MSG_WHAT_GET_EXTRA_PROP;
                message.obj = response;
                mHandler.sendMessage(message);
            }
        });
    }

    private void refreshPropView(WaterPuriProp waterPuriProp) {
        if (waterPuriProp.pTds < 100) {
            tvOutTds.setText(String.valueOf(waterPuriProp.pTds));
        }

        oneLife = waterPuriProp.oneLifeInt;
        twoLife = waterPuriProp.twoLifeInt;
        threeLife = waterPuriProp.threeLifeInt;
        fourLife = waterPuriProp.fourLifeInt;

        tvPpLife.setText("" + (int) waterPuriProp.oneLifeInt + "%");
        tvC1Life.setText("" + (int) waterPuriProp.twoLifeInt + "%");
        tvRoLife.setText("" + (int) waterPuriProp.threeLifeInt + "%");
        tvC2Life.setText("" + (int) waterPuriProp.fourLifeInt + "%");

        mView.findViewById(R.id.pp_click).setOnClickListener(this);
        mView.findViewById(R.id.cl_click).setOnClickListener(this);
        mView.findViewById(R.id.ro_click).setOnClickListener(this);
        mView.findViewById(R.id.c2_click).setOnClickListener(this);
    }

    private void stopTimer() {
        if (mTimer != null) {
            mTimer.cancel();
            mTimer = null;
        }
        if (mTimeTask != null) {
            mTimeTask.cancel();
            mTimeTask = null;
        }
    }

    @Override
    public void onClick(View v) {
        WaterFilterDialog waterFilterDialog = null;
        switch (v.getId()) {
            case R.id.pp_click: // PP棉滤芯
                waterFilterDialog = new WaterFilterDialog(getActivity(), 1, oneLife, 24);
                break;
            case R.id.cl_click: // 前置活性炭滤芯
                waterFilterDialog = new WaterFilterDialog(getActivity(), 2, twoLife, 16);
                break;
            case R.id.ro_click: // RO反渗透滤芯
                waterFilterDialog = new WaterFilterDialog(getActivity(), 3, threeLife, skuid);
                break;
            case R.id.c2_click: // 后置活性炭滤芯
                waterFilterDialog = new WaterFilterDialog(getActivity(), 4, fourLife, 17);
                break;
            default:
                break;
        }
        if (waterFilterDialog != null)
            waterFilterDialog.show();
    }

    private static class MyHandler extends Handler {
        WeakReference<DeviceWaterPuriFierFragment> weakReference;

        public MyHandler(DeviceWaterPuriFierFragment fragment) {
            this.weakReference = new WeakReference<>(fragment);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            DeviceWaterPuriFierFragment fragment = weakReference.get();
            switch (msg.what) {
                case MSG_WHAT_GET_PROP:     // 请求成功返回
                    String prop = (String) msg.obj;
                    if (prop == null) {
                        return;
                    }
                    JSONObject jsonObject;
                    try {
                        jsonObject = new JSONObject(prop);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        return;
                    }
                    WaterPuriProp waterPuriProp = DeviceGetPropDecode.decode(jsonObject, false);
                    if (waterPuriProp == null) {
                        return;
                    }
                    fragment.refreshPropView(waterPuriProp);
                    break;
                case MSG_WHAT_GET_EXTRA_PROP:
                    String text = (String) msg.obj;
                    if (text == null) {
                        return;
                    }
                    Log.d(TAG, "get_x5=" + text);
                    try {
                        JSONObject jsonObject1 = new JSONObject(text);
                        JSONArray jsonArray = jsonObject1.getJSONArray("result");
                        int i = 0;
                        int temp = jsonArray.optInt(i);
                        i++;
                        int uv_state = jsonArray.optInt(i);
                        i++;
                        fragment.tvWaterTemp.setText("" + temp + "℃");
                        if (uv_state == 0) {
                            fragment.tvUvState.setText("空闲中");
                        } else {
                            fragment.tvUvState.setText("杀菌中");
                        }

                        if (fragment.model.equals(AppConfig.YUNMI_WATERPURI_V1) || fragment.model.equals(AppConfig.YUNMI_WATERPURI_V2)) {
                            int press = jsonArray.optInt(i);
                            i++;
                            int elecval_state = jsonArray.optInt(i);
                            fragment.tvWaterPress.setText("" + press / 1000.0 + "Mpa");
                            if (elecval_state == 0) {
                                fragment.tvElecval.setText("关闭");
                            } else {
                                fragment.tvElecval.setText("开启");
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }

}
