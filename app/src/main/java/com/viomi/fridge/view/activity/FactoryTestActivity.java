package com.viomi.fridge.view.activity;

import android.bluetooth.BluetoothHeadset;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.viomi.common.widget.SwitchButton;
import com.viomi.fridge.R;
import com.viomi.fridge.common.GlobalParams;
import com.viomi.fridge.device.AppConfig;
import com.viomi.fridge.device.DeviceConfig;
import com.viomi.fridge.manager.SerialManager;
import com.viomi.fridge.manager.ControlManager;
import com.viomi.fridge.model.bean.DeviceError;
import com.viomi.fridge.model.bean.DeviceParamsGet;
import com.viomi.fridge.model.bean.FridgeTime;
import com.viomi.fridge.model.bean.SerialInfo;

import java.util.Timer;
import java.util.TimerTask;

import static com.viomi.fridge.R.id.model_text;

/**
 * Created by young2 on 2017/1/7.
 */

public class FactoryTestActivity extends BaseActivity {
    private static final String TAG=FactoryTestActivity.class.getSimpleName();
    private TextView mCommodityInspectionTextView;
    private Button mCommodityIinspectionButton,mForcedFrostButton,mQuickFreezeButton,mRCFForcedStartButton,mRollingOverModeButton;
    private static final int CODE_SELF_INSPECTION=0;
    private Handler mHandler = new Handler();
    private FreshThread mViewThread=null;
    private  boolean isForcedFrosting,isQuickFreezing,isRcfForcedStart,isOverRollingCloseMode;
    private SwitchButton mTimeCutButton;
    private TextView mSerialPresureButton;
    private boolean mSerialPresureFlag=false;
    private TimerTask mTimerTask;
    private Timer mTimer;
    private TextView current_temp_text;
    private TextView mRollingOverStatusText;
    private LinearLayout rolling_over_layout;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_factory_test);
        init();
    }

    private void  init(){
        mSerialPresureButton= (TextView) findViewById(R.id.serial_presure_button);
        Button self_inspection_button= (Button) findViewById(R.id.self_inspection_button);
        mCommodityIinspectionButton= (Button) findViewById(R.id.commodity_inspection_button);
        mForcedFrostButton=(Button) findViewById(R.id.forced_frost_button);
        mQuickFreezeButton=(Button) findViewById(R.id.quick_freeze_button);
        mRCFForcedStartButton= (Button) findViewById(R.id.rcf_forced_start_button);
        rolling_over_layout= (LinearLayout) findViewById(R.id.rolling_over_layout);
        mRollingOverModeButton= (Button) findViewById(R.id.rolling_over_mode_button);
        mRollingOverStatusText= (TextView) findViewById(R.id.rolling_over_status);
        mCommodityInspectionTextView= (TextView) findViewById(R.id.commodity_inspection_text);
        mTimeCutButton= (SwitchButton) findViewById(R.id.switch_button );
         current_temp_text= (TextView) findViewById(R.id.current_temp_text);

        if(DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V4)||DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V3)
                ||DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V31)){
            rolling_over_layout.setVisibility(View.VISIBLE);
        }else {
           // rolling_over_layout.setVisibility(View.GONE);
        }
        setCurrentTemp();
        ImageView leftIcon= (ImageView) findViewById(R.id.leftIcon);
        self_inspection_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setClassName("com.mediatek.factorymode", "com.mediatek.factorymode.FactoryMode");
                startActivityForResult(intent,CODE_SELF_INSPECTION);
                SerialManager.getInstance().close();
            }
        });
        mCommodityIinspectionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V2)){
                        boolean result =SerialManager.getInstance().commodityInspection(0);
                        if(result){
                            mCommodityIinspectionButton.setText("商检已启动");
                            mCommodityIinspectionButton.setEnabled(false);
                        }else {
                            Toast.makeText(FactoryTestActivity.this,"商检启动失败",Toast.LENGTH_SHORT).show();
                        }
                }else
                {
                    if(isCommodityInspectionTimeOut()&&(!ControlManager.getInstance().isCommodityInspectionRunning())){
                        v.setEnabled(false);
                        mCommodityInspectionTextView.setText("启动商检时间已过");
                    }else {
                        if(ControlManager.getInstance().isCommodityInspectionRunning()){
                            ControlManager.getInstance().stopCommodityInspection();
                            mCommodityIinspectionButton.setText("启动商检");
                            mCommodityInspectionTextView.setText("");
                        }else {
                            if(ControlManager.getInstance().getDataReceiveInfo().error==0){
                                ControlManager.getInstance().startCommodityInspection();
                                mCommodityIinspectionButton.setText("取消商检");

                            }else {
                                Toast.makeText(FactoryTestActivity.this,"发生故障，不能进入商检",Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }

            }
        });
        mQuickFreezeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isQuickFreezing){
                    ControlManager.getInstance().enableQuickFreeze(false,null);
                    mQuickFreezeButton.setText("速冻");
                    isQuickFreezing=false;
                }else {
                    ControlManager.getInstance().enableQuickFreeze(true,null);
                    mQuickFreezeButton.setText("取消速冻");
                    isQuickFreezing=true;
                }

            }
        });
        mForcedFrostButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isForcedFrosting){
                    ControlManager.getInstance().enableForcedFrost(false);
                    mForcedFrostButton.setText("强制化霜");
                    isForcedFrosting=false;
                }else {
                    ControlManager.getInstance().enableForcedFrost(true);
                    mForcedFrostButton.setText("取消强制化霜");
                    isForcedFrosting=true;
                }

            }
        });
        mRCFForcedStartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V2)||DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V3)
                        ||DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V31)){
                    Toast.makeText(FactoryTestActivity.this,"该冰箱型号无此功能",Toast.LENGTH_SHORT).show();
                    return;
                }
                if(isRcfForcedStart){
                    ControlManager.getInstance().enableRCFForcedStart(false);
                    mRCFForcedStartButton.setText("强制不停机");
                    isRcfForcedStart=false;
                }else {
                    ControlManager.getInstance().enableRCFForcedStart(true);
                    mRCFForcedStartButton.setText("取消强制不停机");
                    isRcfForcedStart=true;
                }
            }
        });
        mRollingOverModeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isOverRollingCloseMode){
                    ControlManager.getInstance().enableRollingOverColseMode(false,null);
                    mRollingOverModeButton.setText("防凝露关闭模式已退出，点击进入");
                    isOverRollingCloseMode=false;
                }else {
                    ControlManager.getInstance().enableRollingOverColseMode(true,null);
                    mRollingOverModeButton.setText("防凝露关闭模式已进入，点击退出");
                    isOverRollingCloseMode=true;
                }
            }
        });

        leftIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        if(DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V1)||DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V4)) {
            if(isCommodityInspectionTimeOut()&&(!ControlManager.getInstance().isCommodityInspectionRunning())){
                mCommodityIinspectionButton.setEnabled(false);
                mCommodityInspectionTextView.setText("启动商检时间已过");
            }else {
                if(ControlManager.getInstance().isCommodityInspectionRunning()){
                    mCommodityIinspectionButton.setText("取消商检");
                }else {
                    mCommodityIinspectionButton.setText("启动商检");
                }
            }
        }else if(DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V2)){
            mCommodityIinspectionButton.setEnabled(true);
        }else if(DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V3)||DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V31)){
            mCommodityIinspectionButton.setEnabled(false);
        }

        if(ControlManager.getInstance().isQuickFreezing()){
            isQuickFreezing=true;
            mQuickFreezeButton.setText("取消速冻");
        }else {
            isQuickFreezing=false;
            mQuickFreezeButton.setText("速冻");
        }
        if(ControlManager.getInstance().isForcedFrost()){
            isForcedFrosting=true;
            mForcedFrostButton.setText("取消强制化霜");
        }else {
            isForcedFrosting=false;
            mForcedFrostButton.setText("强制化霜");
        }
        if(ControlManager.getInstance().isRCFForcedStart()){
            isRcfForcedStart=true;
            mRCFForcedStartButton.setText("取消强制不停机");
        }else {
            isRcfForcedStart=false;
            mRCFForcedStartButton.setText("强制不停机");
        }

        if(ControlManager.getInstance().isRollingOverColseMode()){
            isOverRollingCloseMode=true;
            mRollingOverModeButton.setText("防凝露关闭模式已进入，点击退出");
        }else {
            isOverRollingCloseMode=false;
            mRollingOverModeButton.setText("防凝露关闭模式已退出，点击进入");
        }

        mTimeCutButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                GlobalParams.getInstance().setTestCutTimeEnable(b);
                ControlManager.getInstance().enableTimeCut(b);
            }
        });
         mTimeCutButton.setChecked(GlobalParams.getInstance().isTestCutTimeEnable());

        TextView modelText= (TextView) findViewById(model_text);
        if(DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V1)){
            modelText.setText("型号：云米智能冰箱ilve语音板");
        }else if(DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V2)){
            modelText.setText("型号：智能冰箱iLive四门语音版");
        }else if(DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V3)){
            modelText.setText("型号：云米互联网法式冰箱（462）");
        }else if(DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V31)){
            modelText.setText("型号：云米京东定制互联网冰箱（法式462）");
        }else if(DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V4)){
             modelText.setText("型号：云米455大屏玻璃门冰箱");
        }

        mSerialPresureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                            if(!mSerialPresureFlag){
                                mSerialPresureButton.setText("取消压力测试");
                                mSerialPresureFlag=true;
                                stopTimer();
                                mTimer=new Timer();
                                mTimerTask=new TimerTask() {
                                    @Override
                                    public void run() {
                                        ControlManager.getInstance().setRoomTemp(3, SerialInfo.ROOM_COLD_COLSET,null);
                                    }
                                };
                                mTimer.schedule(mTimerTask,1000,500);
                            }else {
                                mSerialPresureButton.setText("串口压力测试");
                                mSerialPresureFlag=false;
                                stopTimer();
                            }
            }
        });
        mViewThread=new FreshThread();
        mHandler.postDelayed(mViewThread,1000);

    }

    /***  * A、通电后的前30min，关闭变温室，冷藏室设置为2℃，速冻模式。
     B、通电30—60min，关闭冷藏室，打开变温室，变温室设置为-7℃，速冻模式。
     C、通电60—90min钟，关闭变温室，关闭冷藏室，速冻模式。
     D、商检模式下，不影响故障报警功能。
     ***/
    class FreshThread implements Runnable{
        @Override
        public void run() {
            try {
                if(ControlManager.getInstance().isCommodityInspectionRunning()){
                    int time= ControlManager.getInstance().getCommodityInspectionTime();
                    if(DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V1)){
                        if(time<20){
                            mCommodityInspectionTextView.setText("商检计时："+time+"分钟，操作：关闭变温室，冷藏室设置为2℃，冷冻室设置-25℃");
                        }else  if(time<40){
                            mCommodityInspectionTextView.setText("商检计时："+time+"分钟，操作：关闭冷藏室，打开变温室，变温室设置为-7℃，冷冻室设置-25℃");
                        }else  if(time<45){
                            mCommodityInspectionTextView.setText("商检计时："+time+"分钟，操作：关闭变温室，关闭冷藏室，冷冻室设置-25℃");
                        }else {
                            mCommodityInspectionTextView.setText("");
                            mCommodityIinspectionButton.setText("商检完成");
                        }
                    }else if(DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V4)){
                        if(time<30){
                            mCommodityInspectionTextView.setText("商检计时："+time+"分钟，操作：冷藏室设置为2℃，冷冻室设置-25℃");
                        }else  if(time<45){
                            mCommodityInspectionTextView.setText("商检计时："+time+"分钟，操作：关闭冷藏室，，冷冻室设置-25℃");
                        }else {
                            mCommodityInspectionTextView.setText("");
                            mCommodityIinspectionButton.setText("商检完成");
                        }
                    }
                }else {
                    mCommodityInspectionTextView.setText("");
                }

                setCurrentTemp();

                if(ControlManager.getInstance().isRollingOver()){
                    mRollingOverStatusText.setText("工作中");
                }else {
                    mRollingOverStatusText.setText("已关闭");
                }

            }catch (Exception e){
                Log.e(TAG,"serial read fail!msg: "+e.getMessage());
                e.printStackTrace();
            }
            mHandler.postDelayed(this,1000);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode==CODE_SELF_INSPECTION){
            Log.e(TAG,"self inspection return");
                SerialManager.getInstance().open(DeviceConfig.MODEL);

        }
    }

    private void setCurrentTemp(){
        DeviceParamsGet deviceParamsGet=ControlManager.getInstance().getDataReceiveInfo();

        String text="";
        if(DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V3)||DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V31)
                ||DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V4)){
             text="实际温度(℃)       冷藏室："+deviceParamsGet.cold_closet_temp_real
                    +"；      冷冻室："+deviceParamsGet.freezing_room_temp_real
                    +"；      环境温度："+deviceParamsGet.indoor_temp+"；      化霜传感器温度："+deviceParamsGet.fz_evaporator_temp;
            if(DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V4)){
                text+="；      冷冻加热器状态：" ;
                if(deviceParamsGet.fz_evaporator_heat){
                    text+="开启";
                }else {
                    text+="关闭";
                }
            }
        }else {
             text="实际温度(℃)       冷藏室："+deviceParamsGet.cold_closet_temp_real
                    +"；      变温室："+deviceParamsGet.temp_changeable_room_temp_real+"；      冷冻室："+deviceParamsGet.freezing_room_temp_real
                    +"；      环境温度："+deviceParamsGet.indoor_temp+"；      化霜传感器温度："+deviceParamsGet.fz_evaporator_temp;
            if(DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V1)){
                text+="；      冷冻加热器状态：" ;
                if(deviceParamsGet.fz_evaporator_heat){
                    text+="开启";
                }else {
                    text+="关闭";
                }
            }
        }

        current_temp_text.setText(text);
    }

    /***
     * 是否已超过商检启动时间，开机5分钟内启动
     * @return
     */
    private boolean isCommodityInspectionTimeOut(){
//        long currentTime=System.currentTimeMillis()/1000;
//        long time=currentTime- GlobalParams.getInstance().getAppStartTime();
//        if(time> FridgeTime.CommodityInspectionTimeOut){
//            return true;
//        }
        return false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mHandler!=null){
            mHandler.removeCallbacks(mViewThread);
            mHandler=null;
        }
        stopTimer();
    }

    private void stopTimer(){
        if(mTimer!=null){
            mTimer.cancel();
            mTimer=null;
        }
        if(mTimerTask!=null){
            mTimerTask.cancel();
            mTimerTask=null;
        }
    }

}
