package com.viomi.fridge.view.fragment;

import android.os.Bundle;
import android.os.Message;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.miot.api.MiotManager;
import com.miot.common.people.People;
import com.viomi.common.module.okhttp.OkHttpClientManager;
import com.viomi.fridge.R;
import com.viomi.fridge.api.http.HttpConnect;
import com.viomi.fridge.model.bean.DeviceErrorData;
import com.viomi.fridge.util.ToolUtil;
import com.viomi.fridge.util.log;
import com.viomi.fridge.view.adapter.DeviceErrorAdapter;
import com.viomi.fridge.view.widget.DeviceErrorDialog;
import com.viomi.fridge.view.widget.ExpandLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;

/**
 * 即热饮水吧 Fragment
 * Created by William on 2017/12/6.
 */

public class DeviceHeatKettleFragment extends BaseHandlerFragment implements View.OnClickListener, AdapterView.OnItemClickListener {
    private static final String PARAM_DID = "did";
    private static final String PARAM_NAME = "name";
    private View mView;
    private TextView mNameTextView, mTempTextView, mTempUnitTextView, mTDSTextView, mStoreTextView, mWashTextView, mMinSetTempTextView, mSetTempTextView,
            mTipTextView, mModeTextView, mErrorTextView;
    private ListView mListView;
    private List<DeviceErrorData> mList = new ArrayList<>();
    private DeviceErrorAdapter mAdapter;
    private SeekBar mSeekBar;
    private ExpandLayout mExpandLayout;
    private ImageView mImageView;
    private String did;// 设备 id
    private int mPeriod = 5000, mMinTemp, lastSetTemp;// 轮询周期, 最低设置温度
    private boolean mIsSetting = false;
    private People mPeople;
    private static final int MSG_DEVICE_GET_PROP = 1;// Get Prop 请求
    private static final int MSG_DEVICE_GET_PROP_FAIL = 2;// Get Prop 失败
    private static final int MSG_DEVICE_GET_PROP_SUCCESS = 3;// Get Prop 成功
    private static final int MSG_DEVICE_SET_TEMP = 4;// 设置水温
    private static final int MSG_DEVICE_SET_TEMP_FAIL = 5;// 设置水温失败
    private static final int MSG_DEVICE_SET_TEMP_SUCCESS = 6;// 设置水温成功

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_device_heat_kettle, container, false);

        initView();

        initData();

        return mView;
    }

    public static DeviceHeatKettleFragment newInstance(String did, String name) {
        DeviceHeatKettleFragment heatKettleFragment = new DeviceHeatKettleFragment();
        Bundle bundle = new Bundle();
        bundle.putString(PARAM_DID, did);
        bundle.putString(PARAM_NAME, name);
        heatKettleFragment.setArguments(bundle);
        return heatKettleFragment;
    }

    @Override
    protected void processMsg(Message msg) {
        super.processMsg(msg);
        switch (msg.what) {
            case MSG_DEVICE_GET_PROP: // Get Prop 请求
                getProp();
                break;
            case MSG_DEVICE_GET_PROP_FAIL: // Get Prop 失败
                if (mHandler != null)
                    mHandler.sendEmptyMessageDelayed(MSG_DEVICE_GET_PROP, mPeriod);
                break;
            case MSG_DEVICE_GET_PROP_SUCCESS: // Get Prop 成功
                String result = (String) msg.obj;
                refreshUI(result);
                break;
            case MSG_DEVICE_SET_TEMP: // 设置水温
                int temp = (int) msg.obj;
                setTemp(temp);
                break;
            case MSG_DEVICE_SET_TEMP_FAIL: // 设置水温失败
                mIsSetting = false;
                mSeekBar.setProgress(lastSetTemp - mMinTemp);
                break;
            case MSG_DEVICE_SET_TEMP_SUCCESS: // 设置水温成功
                mIsSetting = false;
                break;
        }
    }

    private void initView() {
        mNameTextView = (TextView) mView.findViewById(R.id.heat_kettle_name);
        mTempTextView = (TextView) mView.findViewById(R.id.heat_kettle_temp);
        mTempUnitTextView = (TextView) mView.findViewById(R.id.heat_kettle_temp_unit);
        mTDSTextView = (TextView) mView.findViewById(R.id.heat_kettle_tds_value);
        mStoreTextView = (TextView) mView.findViewById(R.id.heat_kettle_store_value);
        mWashTextView = (TextView) mView.findViewById(R.id.heat_kettle_wash_value);
        mMinSetTempTextView = (TextView) mView.findViewById(R.id.heat_kettle_min_temp);
        mSetTempTextView = (TextView) mView.findViewById(R.id.heat_kettle_set_temp);
        mTipTextView = (TextView) mView.findViewById(R.id.heat_kettle_temp_tip);
        mModeTextView = (TextView) mView.findViewById(R.id.heat_kettle_temp_status);
        mErrorTextView = (TextView) mView.findViewById(R.id.heat_kettle_error);
        mExpandLayout = (ExpandLayout) mView.findViewById(R.id.heat_kettle_temp_expand_layout);
        mImageView = (ImageView) mView.findViewById(R.id.heat_kettle_arrow);
        mSeekBar = (SeekBar) mView.findViewById(R.id.heat_kettle_temp_seek_bar);
        mListView = (ListView) mView.findViewById(R.id.heat_kettle_error_list);

        mView.findViewById(R.id.heat_kettle_set_temp_layout).setOnClickListener(this);
        mErrorTextView.setOnClickListener(this);
        mListView.setOnItemClickListener(this);

        mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if ((90 - mMinTemp) > 0) {
                    int setTemp = mMinTemp + progress;
                    mTipTextView.setText(tempChose(setTemp));
                    String str = setTemp + getActivity().getResources().getString(R.string.device_heat_kettle_temp_unit);
                    mSetTempTextView.setText(str);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                mIsSetting = true;
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (mHandler != null)
                    Message.obtain(mHandler, MSG_DEVICE_SET_TEMP, seekBar.getProgress() + mMinTemp).sendToTarget();
            }
        });

        mAdapter = new DeviceErrorAdapter(getActivity(), mList);
        mListView.setAdapter(mAdapter);
        ToolUtil.setListViewHeightBasedOnChildren(mListView);
        mExpandLayout.initExpand(false);
        mSeekBar.setEnabled(false);
    }

    private void initData() {
        if (getArguments() != null) {
            String name = getArguments().getString(PARAM_NAME, "");
            did = getArguments().getString(PARAM_DID, "");
            mNameTextView.setText(name);
        }
        mPeople = MiotManager.getPeopleManager().getPeople();
        if (mPeople == null) mPeople = new People("", "");
        if (mHandler != null) Message.obtain(mHandler, MSG_DEVICE_GET_PROP).sendToTarget();
    }

    private void getProp() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "get_prop");
            jsonObject.put("did", did);
            jsonObject.put("id", 1);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put("setup_tempe");
            jsonArray.put("tds");
            jsonArray.put("water_remain_time");
            jsonArray.put("flush_time");
            jsonArray.put("custom_tempe1");
            jsonArray.put("min_set_tempe");
            jsonArray.put("work_mode");
            jsonArray.put("run_status");
            jsonObject.put("params", jsonArray);

            HttpConnect.rpcDevice(mPeople, did, jsonObject.toString(), new OkHttpClientManager.ResultCallback<String>() {
                @Override
                public void onError(Call call, Exception e) {
                    log.d(TAG, call.toString());
                    if (mHandler != null)
                        Message.obtain(mHandler, MSG_DEVICE_GET_PROP_FAIL).sendToTarget();
                }

                @Override
                public void onResponse(String response) {
                    log.d(TAG, response);
                    if (mHandler != null)
                        Message.obtain(mHandler, MSG_DEVICE_GET_PROP_SUCCESS, response).sendToTarget();
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
            log.d(TAG, e.toString());
        }
    }

    private void setTemp(int temp) {
        JSONObject jsonObject = new JSONObject();
        try {
            log.d(TAG, "set temp" + temp);
            jsonObject.put("method", "set_tempe_setup");
            jsonObject.put("did", did);
            jsonObject.put("id", 2);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(1);
            jsonArray.put(temp);
            jsonObject.put("params", jsonArray);

            HttpConnect.rpcDevice(mPeople, did, jsonObject.toString(), new OkHttpClientManager.ResultCallback<String>() {
                @Override
                public void onError(Call call, Exception e) {
                    log.d(TAG, e.toString());
                    if (mHandler != null)
                        Message.obtain(mHandler, MSG_DEVICE_SET_TEMP_FAIL).sendToTarget();
                }

                @Override
                public void onResponse(String response) {
                    log.d(TAG, response);
                    if (ToolUtil.setPropertyJsonParse(response)) {
                        if (mHandler != null)
                            mHandler.sendEmptyMessageDelayed(MSG_DEVICE_SET_TEMP_SUCCESS, 2000);
                    } else {
                        if (mHandler != null)
                            Message.obtain(mHandler, MSG_DEVICE_SET_TEMP_FAIL).sendToTarget();
                    }
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
            mIsSetting = false;
        }
    }

    private void refreshUI(String result) {
        JSONArray jsonArray = ToolUtil.getPropJsonParse(result);
        if (jsonArray != null) {    // 解析成功
            // 设置温度
            String temp = jsonArray.optString(0);
            mTempTextView.setText(temp);
            if (mTempUnitTextView.getVisibility() != View.VISIBLE)
                mTempUnitTextView.setVisibility(View.VISIBLE);
            // TDS 值
            String tds = jsonArray.optString(1);
            mTDSTextView.setText(tds);
            // 存水提醒
            String store = jsonArray.optString(2);
            store = store + getActivity().getResources().getString(R.string.device_heat_kettle_store_unit);
            mStoreTextView.setText(store);
            // 清洗提醒
            String wash = String.valueOf(jsonArray.optInt(3) / 24);
            wash = wash + getActivity().getResources().getString(R.string.device_heat_kettle_wash_unit);
            mWashTextView.setText(wash);
            // 温水键温度设置
            int setTemp = jsonArray.optInt(4);
            mMinTemp = jsonArray.optInt(5);
            if ((90 - mMinTemp) <= 0) {  // 设置温度返回有误
                mMinSetTempTextView.setText(getActivity().getResources().getString(R.string.device_heat_kettle_error));
                mSeekBar.setEnabled(false);
            } else {
                String str = mMinTemp + getActivity().getResources().getString(R.string.device_heat_kettle_temp_unit);
                mMinSetTempTextView.setText(str);
                mSeekBar.setMax(90 - mMinTemp);
                mSeekBar.setEnabled(true);
            }
            if (!mIsSetting && (90 - mMinTemp) > 0) {
                lastSetTemp = setTemp;
                mSeekBar.setProgress(setTemp - mMinTemp);
            }
            // 工作模式
            int mode = jsonArray.optInt(6);
            switch (mode) {
                case 0:
                    mModeTextView.setText(getActivity().getResources().getString(R.string.device_pl_machine_temp_normal));
                    break;
                case 1:
                    mModeTextView.setText(getActivity().getResources().getString(R.string.device_pl_machine_temp_warm));
                    break;
                case 2:
                    mModeTextView.setText(getActivity().getResources().getString(R.string.device_pl_machine_temp_hot));
                    break;
                default:
                    mModeTextView.setText(getActivity().getResources().getString(R.string.device_heat_kettle_temp_no_select));
                    break;
            }
            // 故障
            int error = jsonArray.optInt(7);
            byte[] statusByte = ToolUtil.longToByte(error);
            int count = 0;
            for (int i = 21; i < 25; i++) {
                if (statusByte[i] == 1) {
                    count++;
                }
            }
            if (count > 0) {
                mErrorTextView.setText(String.format(getActivity().getResources().getString(R.string.device_heat_kettle_error_more), count));
                mErrorTextView.setVisibility(View.VISIBLE);
                addErrorList(statusByte);
            } else {
                mErrorTextView.setVisibility(View.GONE);
                mListView.setVisibility(View.GONE);
            }
        }
        if (mHandler != null) mHandler.sendEmptyMessageDelayed(MSG_DEVICE_GET_PROP, mPeriod);
    }

    private void addErrorList(byte[] statusByte) {
        mList.clear();
        if (statusByte[21] == 1) {
            DeviceErrorData errorData = new DeviceErrorData();
            errorData.title = getActivity().getResources().getString(R.string.device_heat_kettle_error_21);
            errorData.detail = getActivity().getResources().getString(R.string.device_heat_kettle_error_21_detail);
            mList.add(errorData);
        }
        if (statusByte[22] == 1) {
            DeviceErrorData errorData = new DeviceErrorData();
            errorData.title = getActivity().getResources().getString(R.string.device_heat_kettle_error_22);
            errorData.detail = getActivity().getResources().getString(R.string.device_heat_kettle_error_22_detail);
            mList.add(errorData);
        }
        if (statusByte[23] == 1) {
            DeviceErrorData errorData = new DeviceErrorData();
            errorData.title = getActivity().getResources().getString(R.string.device_heat_kettle_error_23);
            errorData.detail = getActivity().getResources().getString(R.string.device_heat_kettle_error_23_detail);
            mList.add(errorData);
        }
        if (statusByte[24] == 1) {
            DeviceErrorData errorData = new DeviceErrorData();
            errorData.title = getActivity().getResources().getString(R.string.device_heat_kettle_error_24);
            errorData.detail = getActivity().getResources().getString(R.string.device_heat_kettle_error_24_detail);
            mList.add(errorData);
        }
        mAdapter.notifyDataSetChanged();
        ToolUtil.setListViewHeightBasedOnChildren(mListView);
    }

    private String tempChose(int temp) {
        String str;
        switch (temp) {
            case 40:
                str = getResources().getString(R.string.device_heat_kettle_temp_tip_40);
                break;
            case 50:
                str = getResources().getString(R.string.device_heat_kettle_temp_tip_50);
                break;
            case 60:
                str = getResources().getString(R.string.device_heat_kettle_temp_tip_60);
                break;
            case 75:
                str = getResources().getString(R.string.device_heat_kettle_temp_tip_75);
                break;
            case 80:
                str = getResources().getString(R.string.device_heat_kettle_temp_tip_80);
                break;
            case 85:
                str = getResources().getString(R.string.device_heat_kettle_temp_tip_85);
                break;
            case 90:
                str = getResources().getString(R.string.device_heat_kettle_temp_tip_90);
                break;
            default:
                str = "";
                break;
        }
        return str;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.heat_kettle_set_temp_layout: // 展开和收缩
                mExpandLayout.toggleExpand();
                mImageView.setSelected(!mImageView.isSelected());
                break;
            case R.id.heat_kettle_error: // 故障
                if (mListView.getVisibility() == View.VISIBLE) mListView.setVisibility(View.GONE);
                else mListView.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        DeviceErrorData data = mList.get(position);
        if (data.detail == null || data.detail.equals("")) return;
        DeviceErrorDialog dialog = new DeviceErrorDialog(getActivity(), data.title, data.detail);
        dialog.show();
    }
}
