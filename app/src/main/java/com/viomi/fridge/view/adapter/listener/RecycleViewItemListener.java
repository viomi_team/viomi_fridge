package com.viomi.fridge.view.adapter.listener;

/**
 * Created by GMARUnity on 2017/2/3.
 */
public interface RecycleViewItemListener {

    void onItemClick(int postion);
    boolean onItemLongClick(int position);
}
