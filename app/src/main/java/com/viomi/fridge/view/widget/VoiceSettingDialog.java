package com.viomi.fridge.view.widget;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.miot.common.abstractdevice.AbstractDevice;
import com.viomi.common.widget.SwitchButton;
import com.viomi.fridge.R;
import com.viomi.fridge.broadcast.BroadcastAction;
import com.viomi.fridge.common.GlobalParams;
import com.viomi.fridge.common.ViomiApplication;
import com.viomi.fridge.device.AppConfig;
import com.viomi.fridge.manager.VoiceManager;
import com.viomi.fridge.model.bean.DeviceInfo;
import com.viomi.fridge.util.ApkUtil;
import com.viomi.fridge.util.FileUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by young2 on 2017/3/24.
 */

public class VoiceSettingDialog extends BaseDialog {
    private Context mContext;
    private OnMoreCMDClickListener mOnMoreCMDClickListener;
    private OnWakeupClickListener mOnWakeupClickListener;
    private TextView mVoiceStateView;
    private LinearLayout mOccupyMusicView;

    private SwitchButton mVoiceSwitchButton;

    public interface OnMoreCMDClickListener {
        void onClick();
    }

    public void setOnMoreCMDClickListener(OnMoreCMDClickListener listener) {
        this.mOnMoreCMDClickListener = listener;
    }

    public interface OnWakeupClickListener {
        void onClick();
    }

    public void setOnWakeupClickListener(OnWakeupClickListener listener) {
        this.mOnWakeupClickListener = listener;
    }


    protected VoiceSettingDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        mContext = context;
    }

    public VoiceSettingDialog(Context context, int themeResId) {
        super(context, themeResId);
        mContext = context;
    }

    public VoiceSettingDialog(Context context) {
        super(context);
        mContext = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mVoiceSwitchButton = (SwitchButton) findViewById(R.id.voiceBtn);
        mVoiceStateView = (TextView) findViewById(R.id.voice_enable_view);
        mOccupyMusicView = (LinearLayout) findViewById(R.id.music_occupy_view);
        ImageView music_occupy_app = (ImageView) findViewById(R.id.music_occupy_app);
        TextView manualWakeupButton = (TextView) findViewById(R.id.wakpup_button);
        LinearLayout moreVoiceCMDView = (LinearLayout) findViewById(R.id.more_voice_cmd_view);

        ImageView closeView = (ImageView) findViewById(R.id.icon_colse);
        closeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        manualWakeupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FileUtil.writeTxtToFile(mContext, AppConfig.EventKey.EVENT_WAKE_UP_VOICE, "wake_up");
                VoiceManager.getInstance().enableVoice(true);
                GlobalParams.getInstance().setVoiceEnabe(true);
                VoiceManager.getInstance().wakeupProcess();
                dismiss();
            }
        });

        moreVoiceCMDView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FileUtil.writeTxtToFile(mContext, AppConfig.EventKey.EVENT_FIND_VOICE_CMD, "cmd");
                Intent intent = new Intent(BroadcastAction.ACTION_VOICE_INTRODUCE_VISIBLE);
                LocalBroadcastManager.getInstance(ViomiApplication.getContext()).sendBroadcast(intent);
                dismiss();
            }
        });

        music_occupy_app.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
//                killMusicApp();
//                mOccupyMusicView.setVisibility(View.GONE);
                dismiss();
                String packageName = "com.tencent.qqmusicpad";
                if (ApkUtil.checkApkExist(ViomiApplication.getContext(), packageName)) {
                    Log.e("VoiceSettingDialog", "startOtherApp,packageName1=" + packageName);
                    ApkUtil.startOtherApp(mContext, packageName, false);
                }
            }
        });

        mVoiceSwitchButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                GlobalParams.getInstance().setVoiceEnabe(b);
                VoiceManager.getInstance().enableVoice(b);
                upLoadEvent(b);
                if (b) {
                    mVoiceStateView.setText(mContext.getString(R.string.text_voice_enable));
                } else {
                    mVoiceStateView.setText(mContext.getString(R.string.text_voice_inenable));
                }
            }
        });

        if (GlobalParams.getInstance().isVoiceEnabe()) {
            mVoiceSwitchButton.setChecked(true);
            mVoiceStateView.setText(mContext.getString(R.string.text_voice_enable));
        } else {
            mVoiceSwitchButton.setChecked(false);
            mVoiceStateView.setText(mContext.getString(R.string.text_voice_inenable));
        }
        if (VoiceManager.getInstance().isVoiceOccupy()) {
            mOccupyMusicView.setVisibility(View.VISIBLE);
        } else {
            mOccupyMusicView.setVisibility(View.GONE);
        }
    }

    private void upLoadEvent(boolean flag) {
        String state = "";
        if (flag) {
            state = "1";
        } else {
            state = "0";
        }
        JSONObject object = new JSONObject();
        try {
            object.put("state", state);
        } catch (JSONException e) {
            e.printStackTrace();
            FileUtil.writeTxtToFile(mContext, AppConfig.EventKey.EVENT_STATUS_VOICE, object.toString());
        }
    }

    @Override
    public void setView() {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_voice_setting);
        hideBar();
    }


    @Override
    public void show() {
        super.show();
        FileUtil.writeTxtToFile(mContext, AppConfig.EventKey.EVENT_ENTER_VOICE, null);
        if (GlobalParams.getInstance().isVoiceEnabe()) {
            mVoiceSwitchButton.setChecked(true);
            mVoiceStateView.setText(mContext.getString(R.string.text_voice_enable));
        } else {
            mVoiceSwitchButton.setChecked(false);
            mVoiceStateView.setText(mContext.getString(R.string.text_voice_inenable));
        }
        if (VoiceManager.getInstance().isVoiceOccupy()) {
            mOccupyMusicView.setVisibility(View.VISIBLE);
        } else {
            mOccupyMusicView.setVisibility(View.GONE);
        }
    }


    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        FileUtil.writeTxtToFile(mContext, AppConfig.EventKey.EVENT_EXIT_VOICE, null);
    }

    /***
     * 杀死正在运行的音乐播放软件
     */
    private void killMusicApp() {
        String musicPackageName = "com.tencent.qqmusicpad";
        String qiyiPackageName = "com.qiyi.video.pad";
        ActivityManager activityManager = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
        // 获取所有正在运行的app
        List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
        // 遍历app，获取应用名称或者包名
        for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
            if (appProcess.processName.equals(musicPackageName)) {
                //   android.os.Process.killProcess(appProcess.pid);
                activityManager.killBackgroundProcesses(musicPackageName);
            }
            if (appProcess.processName.equals(qiyiPackageName)) {
                //   android.os.Process.killProcess(appProcess.pid);
                activityManager.killBackgroundProcesses(qiyiPackageName);

            }
        }
    }

    private void killApp(ActivityManager activityManager, String packageName) {

//        Method method = null;
//        try {
//            method = Class.forName("android.app.ActivityManager").getMethod("forceStopPackage", String.class);
//            method.invoke(activityManager, packageName);
//        } catch (NoSuchMethodException e) {
//            e.printStackTrace();
//        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
//        } catch (IllegalAccessException e) {
//            e.printStackTrace();
//        } catch (InvocationTargetException e) {
//            e.printStackTrace();
//        }
    }

}
