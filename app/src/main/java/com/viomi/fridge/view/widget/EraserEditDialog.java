package com.viomi.fridge.view.widget;

import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;

import com.viomi.fridge.R;
import com.viomi.fridge.util.log;

/**
 * 橡皮擦编辑 Dialog
 * Created by William on 2017/6/21.
 */

public class EraserEditDialog extends BaseDialog implements SeekBar.OnSeekBarChangeListener {

    private static final String TAG = EraserEditDialog.class.getSimpleName();
    private View view;
    private SeekBar seekBar;
    private ImageView imageView;
    private PaletteView paletteView;
    private int size, progress;

    public EraserEditDialog(Context context, View view, SeekBar seekBar, ImageView imageView,
                            PaletteView paletteView, int size, int progress) {
        super(context);
        this.view = view;
        this.seekBar = seekBar;
        this.imageView = imageView;
        this.paletteView = paletteView;
        this.size = size;
        this.progress = progress;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = this.getWindow();
        if (window != null) {
            WindowManager.LayoutParams lp = window.getAttributes();
            lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(lp);
            window.setBackgroundDrawable(null);
            window.setDimAmount(0f);    // 背景无模糊
            window.setGravity(Gravity.CENTER);
            window.setWindowAnimations(R.style.popupWindow_anim_style);  // Dialog 动画
        }
        setCanceledOnTouchOutside(true);

        initView();
    }

    private void initView() {
        seekBar.setOnSeekBarChangeListener(this);
        seekBar.setProgress(progress);
    }

    private void setEraserSize(int progress) {
        int calcProgress = progress > 1 ? progress : 1;
        int newSize = Math.round((size / 100f) * calcProgress);
        int offset = Math.round((size - newSize) / 2);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(newSize, newSize);
        lp.setMargins(offset, offset, offset, offset);
        imageView.setLayoutParams(lp);
        paletteView.setEraserSize(newSize);
    }

    @Override
    public void setView() {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(view);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        setEraserSize(progress);
        this.progress = progress;
        log.d(TAG, "current progress:" + this.progress);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

}
