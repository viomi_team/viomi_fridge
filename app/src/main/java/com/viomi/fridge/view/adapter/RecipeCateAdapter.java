package com.viomi.fridge.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.viomi.fridge.R;
import com.viomi.fridge.model.bean.recipe.RecipeDetail;
import com.viomi.fridge.util.ImgUtil;

public class RecipeCateAdapter extends BaseRecycleViewAdapter {

    private Context mContext;
    private OnItemClickListener clickListener;

    public RecipeCateAdapter(Context mContext, OnItemClickListener clickListener) {
        this.mContext = mContext;
        this.clickListener = clickListener;
    }

    @Override
    public void onBindItemHolder(RecyclerView.ViewHolder holder, int position) {
        final ViewHolder holder1 = (ViewHolder) holder;
        if (datas != null && position < datas.size()) {
            RecipeDetail detail = (RecipeDetail) datas.get(position);
            if (detail != null) {
                RecipeDetail.recipe recipe = detail.getRecipe();
                if (recipe != null) {
                    ((ViewHolder) holder).tv_name.setText(detail.getName());
                    ((ViewHolder) holder).tv_descrip.setText(recipe.getSumary());
                    ImgUtil.showDefinedImage(recipe.getImg(), ((ViewHolder) holder).iv_icon, R.drawable.zf_default_load_image);
                }
            }
            ((ViewHolder) holder).item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (clickListener != null) {
                        clickListener.ItemClick(detail);
                    }
                }
            });
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_recipe_cate, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return datas == null ? 0 : datas.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_name;
        TextView tv_descrip;
        ImageView iv_icon;
        RelativeLayout item;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_descrip = (TextView) itemView.findViewById(R.id.tv_descrip);
            tv_name = (TextView) itemView.findViewById(R.id.tv_name);
            iv_icon = (ImageView) itemView.findViewById(R.id.iv_icon);
            item = (RelativeLayout) itemView.findViewById(R.id.list_item);
        }
    }

    public interface OnItemClickListener {
        void ItemClick(RecipeDetail entity);
    }
}
