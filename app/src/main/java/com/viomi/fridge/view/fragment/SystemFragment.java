package com.viomi.fridge.view.fragment;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.viomi.common.widget.SwitchButton;
import com.viomi.fridge.R;
import com.viomi.fridge.util.ApkUtil;
import com.viomi.fridge.util.SDCardUtils;
import com.viomi.fridge.view.activity.VersionManagerActivity;

/**
 * Created by young2 on 2017/1/6.
 */

public class SystemFragment extends BaseFragment {

    private SwitchButton voiceBtn;
    private SeekBar soundBar;
    private SeekBar lightBar;
    private Button clear_cache;
    private TextView valumeText;
    private AudioManager mAudioManager;
    private RelativeLayout restart;
    private RelativeLayout version;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mainView = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_system, null, false);
        voiceBtn = (SwitchButton) mainView.findViewById(R.id.voiceBtn);
        soundBar = (SeekBar) mainView.findViewById(R.id.soundBar);
        lightBar = (SeekBar) mainView.findViewById(R.id.lightBar);
        clear_cache = (Button) mainView.findViewById(R.id.clear_cache);
        valumeText = (TextView) mainView.findViewById(R.id.valumeText);
        restart = (RelativeLayout) mainView.findViewById(R.id.restart);
        version = (RelativeLayout) mainView.findViewById(R.id.version);
        TextView version_text = (TextView) mainView.findViewById(R.id.version_text);
        version_text.setText("V" + ApkUtil.getVersionCode());
        initListener();
        init();
        return mainView;
    }

    private void init() {

        //音量相关
        mAudioManager = (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);
        //当前音量
        int currentVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_SYSTEM);
        //最大音量
        int maxVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_SYSTEM);
        soundBar.setMax(maxVolume);
        soundBar.setProgress(currentVolume);

        //亮度相关
        int systemBrightness = 0;
        try {
            systemBrightness = Settings.System.getInt(getActivity().getContentResolver(), Settings.System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        lightBar.setMax(255);
        lightBar.setProgress(systemBrightness);


        String ram = "可用：" + String.format("%.2f", SDCardUtils.getSDCardFreeSize() / 1024f) + "G   总容量：" + String.format("%.2f", SDCardUtils.getSDCardTotalSize() / 1024f) + "G";
        valumeText.setText(ram);

        version.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), VersionManagerActivity.class);
                startActivity(intent);
            }
        });

    }


    private void initListener() {

        soundBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mAudioManager.setStreamVolume(AudioManager.STREAM_SYSTEM, progress, 0);
                mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, progress, 0);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        lightBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                saveBrightness(getActivity(), progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        clear_cache.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        restart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }

    private void saveBrightness(Context context, int brightness) {
        Uri uri = Settings.System.getUriFor(Settings.System.SCREEN_BRIGHTNESS);
        Settings.System.putInt(context.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS, brightness);
        context.getContentResolver().notifyChange(uri, null);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
