package com.viomi.fridge.view.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.viomi.common.module.okhttp.OkHttpClientManager;
import com.viomi.fridge.R;
import com.viomi.fridge.broadcast.BroadcastAction;
import com.viomi.fridge.common.GlobalParams;
import com.viomi.fridge.manager.UpgradeManager;
import com.viomi.fridge.model.bean.AppUpgradeMsg;
import com.viomi.fridge.model.bean.SystemUpgradeMsg;
import com.viomi.fridge.util.ApkUtil;
import com.viomi.fridge.util.PhoneUtil;
import com.viomi.fridge.util.log;
import com.viomi.fridge.view.widget.ProgressWheel;

import okhttp3.Call;

/**
 * Created by young2 on 2017/2/21.
 */

public class UpgradeActivity extends BaseActivity {
    private final  static String TAG=UpgradeActivity.class.getSimpleName();
    private final int TYPE_UPGRADE_APP=0;
    private final int TYPE_UPGRADE_SYSTEM=1;
    private final int TYPE_UPGRADE_MCU=2;
    private final int TYPE_UPGRADE_PLUG=3;
    private int mUpgradeType;
    private LocalBroadcastManager mBroadcastManager;
    private ProgressWheel mProgressWheel;
    private RelativeLayout mVersionMsgView;
    private TextView mUpgradingTipView;
    private TextView mOldVersionView,mNewVersionView,mDescView;
    private ImageView mStatusView;
    private Button mUpgradeButton;
    private AppUpgradeMsg mAppUpgradeMsg;
    private SystemUpgradeMsg mSystemUpgradeMsg;
    private Call mAppCall,mSystemCall;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upgrade);
        init();
    }

    private void init(){
        mProgressWheel= (ProgressWheel) findViewById(R.id.progress_bar);
        mVersionMsgView= (RelativeLayout) findViewById(R.id.version_message);
        mUpgradingTipView= (TextView) findViewById(R.id.upgrading_tip);
        mOldVersionView= (TextView) findViewById(R.id.old_version);
        mNewVersionView= (TextView) findViewById(R.id.new_version);
        mDescView= (TextView) findViewById(R.id.descrition);
        mUpgradeButton= (Button) findViewById(R.id.upgrade_button);
        mStatusView= (ImageView) findViewById(R.id.status_view);
        ImageView leftIcon= (ImageView) findViewById(R.id.leftIcon);
        leftIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onBack()){

                }else {
                    finish();
                }
            }
        });
        register();
        mUpgradeType=getIntent().getIntExtra("type",TYPE_UPGRADE_APP);
        switch (mUpgradeType){
            case TYPE_UPGRADE_APP:
               UpgradeManager.getInstance().checkAppUpgrade(UpgradeActivity.this,UpgradeManager.OPERATE_MANUAL);
                break;
            case TYPE_UPGRADE_SYSTEM:
                UpgradeManager.getInstance().checkSystemUpgrade(UpgradeActivity.this,UpgradeManager.OPERATE_MANUAL);
                break;
            case TYPE_UPGRADE_MCU:
                break;
            case TYPE_UPGRADE_PLUG:
                break;
        };

        mUpgradeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (mUpgradeType){
                    case TYPE_UPGRADE_APP:
                        mAppCall=UpgradeManager.getInstance().upgradeApp(mAppUpgradeMsg,UpgradeManager.OPERATE_MANUAL);
                        mVersionMsgView.setVisibility(View.GONE);
                        mUpgradingTipView.setVisibility(View.VISIBLE);
                        mUpgradingTipView.setText(R.string.text_version_upgrading);
                        mProgressWheel.setProgress(0);
                        mProgressWheel.setVisibility(View.VISIBLE);
                        mUpgradeButton.setVisibility(View.GONE);
                        break;
                    case TYPE_UPGRADE_SYSTEM:
                        mSystemCall=UpgradeManager.getInstance().upgradeSystem(mSystemUpgradeMsg,UpgradeManager.OPERATE_MANUAL);
                        mVersionMsgView.setVisibility(View.GONE);
                        mUpgradingTipView.setVisibility(View.VISIBLE);
                        mUpgradingTipView.setText(R.string.text_version_upgrading);
                        mProgressWheel.setProgress(0);
                        mProgressWheel.setVisibility(View.VISIBLE);
                        mUpgradeButton.setVisibility(View.GONE);
                        break;
                    case TYPE_UPGRADE_MCU:
                        break;
                    case TYPE_UPGRADE_PLUG:
                        break;
                };
            }
        });
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
//        if (GlobalParams.HTTP_DEBUG)
//            Beta.checkUpgrade(false, false);//检测是否有新版本
    }

    private void register() {
        mBroadcastManager = LocalBroadcastManager.getInstance(this);
        IntentFilter intentFilter = new IntentFilter(BroadcastAction.ACTION_APP_UPGRADE_CHECK);
        intentFilter.addAction(BroadcastAction.ACTION_APP_DOWNLOAD_FINISH);
        intentFilter.addAction(BroadcastAction.ACTION_APP_DOWNLOAD_PROGRESS);
        intentFilter.addAction(BroadcastAction.ACTION_START_APP_UPGRADE);

        intentFilter.addAction(BroadcastAction.ACTION_SYSTEM_UPGRADE_CHECK);
        intentFilter.addAction(BroadcastAction.ACTION_SYSTEM_DOWNLOAD_FINISH);
        intentFilter.addAction(BroadcastAction.ACTION_SYSTEM_DOWNLOAD_PROGRESS);
        intentFilter.addAction(BroadcastAction.ACTION_START_SYSTEM_UPGRADE);
        mBroadcastManager.registerReceiver(myReceiver, intentFilter);
    }

    private BroadcastReceiver myReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            log.d(TAG, "onReceive,action=" + intent.getAction());
            switch (intent.getAction()) {
                case BroadcastAction.ACTION_APP_UPGRADE_CHECK:
                    mUpgradingTipView.setVisibility(View.GONE);
                    String result=intent.getStringExtra("need_upgrade");
                    switch (result){
                        case UpgradeManager.NeedUpgradeYes:
                           AppUpgradeMsg appUpgradeMsg= intent.getParcelableExtra("data");
                            mAppUpgradeMsg=appUpgradeMsg;
                            if(appUpgradeMsg==null){
                                mStatusView.setImageResource(R.mipmap.update_img_fail);
                                mVersionMsgView.setVisibility(View.VISIBLE);
                                mDescView.setText(R.string.text_version_message_error);
                            }else {
                                mStatusView.setImageResource(R.mipmap.update_img_update);
                                mVersionMsgView.setVisibility(View.VISIBLE);
                                mOldVersionView.setText(getResources().getString(R.string.text_current_version)+ ApkUtil.getVersionCode());
                                mNewVersionView.setText(getResources().getString(R.string.text_current_version)+ appUpgradeMsg.versionCode);
                                mDescView.setText(getResources().getString(R.string.text_cloud_version_desc)+appUpgradeMsg.upgradeDescription);
                                mUpgradeButton.setVisibility(View.VISIBLE);
                            }
                            break;
                        case UpgradeManager.NeedUpgradeNo:
                            mStatusView.setImageResource(R.mipmap.update_img_latest);
                            mVersionMsgView.setVisibility(View.VISIBLE);
                            mOldVersionView.setText(getResources().getString(R.string.text_current_version)+ ApkUtil.getVersionCode());
                            mDescView.setText(R.string.text_already_latest_version);
                            break;
                        case UpgradeManager.NeedUpgradeError:
                            mStatusView.setImageResource(R.mipmap.update_img_fail);
                            mVersionMsgView.setVisibility(View.VISIBLE);
                            mDescView.setText(R.string.text_upgrade_message_fail);
                         default:
                            break;
                    }

                    break;


                case BroadcastAction.ACTION_SYSTEM_UPGRADE_CHECK:
                    mUpgradingTipView.setVisibility(View.GONE);
                    String systemResult=intent.getStringExtra("need_upgrade");
                    switch (systemResult){
                        case UpgradeManager.NeedUpgradeYes:
                            SystemUpgradeMsg systemUpgradeMsg= intent.getParcelableExtra("data");
                            mSystemUpgradeMsg=systemUpgradeMsg;
                            if(systemUpgradeMsg==null){
                                mStatusView.setImageResource(R.mipmap.update_img_fail);
                                mVersionMsgView.setVisibility(View.VISIBLE);
                                mDescView.setText(R.string.text_version_message_error);
                            }else {
                                mStatusView.setImageResource(R.mipmap.update_img_update);
                                mVersionMsgView.setVisibility(View.VISIBLE);
                                mOldVersionView.setText(getResources().getString(R.string.text_current_version)+ PhoneUtil.getSystemVersionCode());
                                mNewVersionView.setText(getResources().getString(R.string.text_current_version)+ systemUpgradeMsg.versionCode);
                                mDescView.setText(getResources().getString(R.string.text_cloud_version_desc)+systemUpgradeMsg.upgradeDescription);
                                mUpgradeButton.setVisibility(View.VISIBLE);
                            }
                            break;
                        case UpgradeManager.NeedUpgradeNo:
                            mStatusView.setImageResource(R.mipmap.update_img_latest);
                            mVersionMsgView.setVisibility(View.VISIBLE);
                            mOldVersionView.setText(getResources().getString(R.string.text_current_version)+PhoneUtil.getSystemVersionCode());
                            mDescView.setText(R.string.text_already_latest_version);
                            break;
                        case UpgradeManager.NeedUpgradeError:
                            mStatusView.setImageResource(R.mipmap.update_img_fail);
                            mVersionMsgView.setVisibility(View.VISIBLE);
                            mDescView.setText(R.string.text_upgrade_message_fail);
                        default:
                            break;
                    }
                    break;

                case BroadcastAction.ACTION_APP_DOWNLOAD_PROGRESS:
                case BroadcastAction.ACTION_SYSTEM_DOWNLOAD_PROGRESS:
                    int progress=intent.getIntExtra("progress",0);
                    log.d(TAG, "ACTION_APP_DOWNLOAD_PROGRESS,progress=" + progress);
                    mProgressWheel.setProgress(progress);
                    break;

                case BroadcastAction.ACTION_APP_DOWNLOAD_FINISH:
                    String appDownloadResult=intent.getStringExtra("result");
                    if(UpgradeManager.UpgradeSuccess.equals(appDownloadResult)){
                        mStatusView.setImageResource(R.mipmap.update_img_success);
                        mVersionMsgView.setVisibility(View.VISIBLE);
                        mUpgradingTipView.setVisibility(View.GONE);
                        mProgressWheel.setVisibility(View.INVISIBLE);
                        mNewVersionView.setVisibility(View.INVISIBLE);
                        mOldVersionView.setVisibility(View.INVISIBLE);
                        mDescView.setText(R.string.text_app_upgrade_success);
                    }else {
                        mStatusView.setImageResource(R.mipmap.update_img_fail);
                        mVersionMsgView.setVisibility(View.VISIBLE);
                        mUpgradingTipView.setVisibility(View.GONE);
                        mProgressWheel.setVisibility(View.INVISIBLE);
                        mNewVersionView.setVisibility(View.INVISIBLE);
                        mOldVersionView.setVisibility(View.INVISIBLE);
                        mDescView.setText(R.string.text_upgrade_download_fail);
                    }

                    if(mAppCall!=null){
                        mAppCall.cancel();
                        mAppCall=null;
                     }
                    break;

                case BroadcastAction.ACTION_SYSTEM_DOWNLOAD_FINISH:
                    String systemDownloadResult=intent.getStringExtra("result");
                    if(UpgradeManager.UpgradeSuccess.equals(systemDownloadResult)){
                        mStatusView.setImageResource(R.mipmap.update_img_success);
                        mVersionMsgView.setVisibility(View.VISIBLE);
                        mUpgradingTipView.setVisibility(View.GONE);
                        mProgressWheel.setVisibility(View.INVISIBLE);
                        mNewVersionView.setVisibility(View.INVISIBLE);
                        mOldVersionView.setVisibility(View.INVISIBLE);
                        mDescView.setText(R.string.text_system_upgrade_success);
                    }else {
                        mStatusView.setImageResource(R.mipmap.update_img_fail);
                        mVersionMsgView.setVisibility(View.VISIBLE);
                        mUpgradingTipView.setVisibility(View.GONE);
                        mProgressWheel.setVisibility(View.INVISIBLE);
                        mNewVersionView.setVisibility(View.INVISIBLE);
                        mOldVersionView.setVisibility(View.INVISIBLE);
                        mDescView.setText(R.string.text_upgrade_download_fail);
                    }
                    if(mSystemCall!=null){
                        mSystemCall.cancel();
                        mSystemCall=null;
                    }
                    break;


            }

        }
    };

    private void unRegister() {
        mBroadcastManager.unregisterReceiver(myReceiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unRegister();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode==event.KEYCODE_BACK){
            if(onBack()){
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    private boolean onBack(){
        if(mSystemCall!=null||mAppCall!=null){
            new AlertDialog.Builder(UpgradeActivity.this).setTitle("升级中")
                    .setMessage("是否退出升级").setNegativeButton("继续升级",null)
                    .setPositiveButton("坚决退出", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            if(mSystemCall!=null){
                                mSystemCall.cancel();
                                mSystemCall=null;
                            }
                            if(mAppCall!=null){
                                mAppCall.cancel();
                                mAppCall=null;
                            }
                            OkHttpClientManager.cancelAll();
                            finish();
                        }
                    }).show();
            {
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
                int uiFlags = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                        | View.SYSTEM_UI_FLAG_FULLSCREEN; // hide status bar

                if (Build.VERSION.SDK_INT >= 19) {
                    uiFlags |= 0x00001000;    //SYSTEM_UI_FLAG_IMMERSIVE_STICKY: hide navigation bars - compatibility: building API level is lower thatn 19, use magic number directly for higher API target level
                } else {
                    uiFlags |= View.SYSTEM_UI_FLAG_LOW_PROFILE;
                }
                try{
                    getWindow().getDecorView().setSystemUiVisibility(uiFlags);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
            return true;
        }else {
            return false;
        }
    }
}
