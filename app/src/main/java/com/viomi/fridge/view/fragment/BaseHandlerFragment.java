package com.viomi.fridge.view.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;

import java.lang.ref.WeakReference;

/**
 * Created by young2 on 2017/2/9.
 */

public abstract class BaseHandlerFragment extends BaseFragment {
    protected Handler mHandler;

    protected static class BaseHandler extends Handler {
        WeakReference<BaseHandlerFragment> weakReference;

        BaseHandler(BaseHandlerFragment fragment) {
            this.weakReference = new WeakReference<>(fragment);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (this.weakReference != null) {
                BaseHandlerFragment fragment = this.weakReference.get();
                if (fragment != null && !fragment.isRemoving()) {
                    fragment.processMsg(msg);
                }
            }
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mHandler = new BaseHandler(this);
    }

    public void onDestroy() {
        super.onDestroy();
        if (mHandler != null) {
            mHandler.removeCallbacksAndMessages(null);
            mHandler = null;
        }
    }

    protected void processMsg(Message msg) {

    }
}
