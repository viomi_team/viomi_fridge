package com.viomi.fridge.view.adapter;

import android.app.Activity;

import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.viomi.fridge.R;
import com.viomi.fridge.api.dao.AdvertInfoMessage;
import com.viomi.fridge.api.dao.InfoManager;
import com.viomi.fridge.util.ToastUtil;
import com.viomi.fridge.util.UMUtil;
import com.viomi.fridge.view.fragment.AdvertInfosFragment;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by young2 on 2017/7/12.
 */

public class AdvertInfosAdapter extends RecyclerView.Adapter<AdvertInfosAdapter.MyViewHolder> {
    private Activity mContext;
    private AdvertInfosFragment mFragment;
    private List<AdvertInfoMessage> mDataList;
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM月dd日 HH:mm");

    public AdvertInfosAdapter(Activity context, Fragment fragment, List<AdvertInfoMessage> datas){
        mDataList=datas;
        mContext=context;
        mFragment= (AdvertInfosFragment) fragment;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(mContext).inflate(R.layout.item_info_advert, parent, false);
        AdvertInfosAdapter.MyViewHolder holder = new AdvertInfosAdapter.MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        AdvertInfoMessage advertInfoMessage=mDataList.get(position);
        if(advertInfoMessage.getImgUrl()!=null){
            Uri uri = Uri.parse(advertInfoMessage.getImgUrl());
            holder.LogoImageView.setImageURI(uri);
            holder.LogoImageView.setVisibility(View.VISIBLE);
        }else {
            holder.LogoImageView.setVisibility(View.GONE);
        }
        if(mDataList.get(position).getTitle()!=null){
            holder.titleTextView.setText((advertInfoMessage.getTitle()));
        }else {
            holder.titleTextView.setVisibility(View.GONE);
        }
        if(mDataList.get(position).getContent()!=null){
            holder.contentTextView.setText((advertInfoMessage.getContent()));
        }else {
            holder.contentTextView.setVisibility(View.GONE);
        }
        if(advertInfoMessage.isRead()){
            holder.newPointView.setVisibility(View.GONE);
        }else {
            holder.newPointView.setVisibility(View.VISIBLE);
        }
        if(position==mDataList.size()-1){
            holder.progressEndLineView.setVisibility(View.GONE);
        }else {
            holder.progressEndLineView.setVisibility(View.VISIBLE);
        }

        holder.timeTextVIew.setText(simpleDateFormat.format(new Date(advertInfoMessage.getTime()*1000)));

        if(advertInfoMessage.getLinkUrl()==null){
            holder.detailTextView.setVisibility(View.GONE);
        }else {
            holder.detailTextView.setVisibility(View.VISIBLE);
        }

        holder.detailTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InfoManager.getInstance().readAdvertInfo(mDataList.get(position));
                mFragment.dataInit();
                String url=mDataList.get(position).getLinkUrl();
                if(url==null||(!url.startsWith("http"))){
                    Log.e("advert info","link error,url="+url);
                    ToastUtil.show(mContext.getString(R.string.toast_link_url_error));
                }
                UMUtil.onCommonWebPageJump(mContext,mDataList.get(position).getLinkUrl());
            }
        });
        holder.deleteImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFragment.deleteInfo(mDataList.get(position).getInfoId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder
    {

        public View progressEndLineView;
        public SimpleDraweeView LogoImageView;
        public TextView titleTextView;
        public TextView contentTextView;
        public ImageView newPointView;
        public LinearLayout detailTextView;
        public ImageView deleteImageView;
        public TextView timeTextVIew;
        public MyViewHolder(View view)
        {
            super(view);
            progressEndLineView= (View) view.findViewById(R.id.step_line_end);
            LogoImageView= (SimpleDraweeView) view.findViewById(R.id.info_icon);
            titleTextView= (TextView) view.findViewById(R.id.info_title);
            contentTextView= (TextView) view.findViewById(R.id.info_desc);
            deleteImageView= (ImageView) view.findViewById(R.id.delete_view);
            detailTextView= (LinearLayout) view.findViewById(R.id.info_detail);
            newPointView= (ImageView) view.findViewById(R.id.red_point);
            timeTextVIew= (TextView) view.findViewById(R.id.time_text);
        }
    }
}
