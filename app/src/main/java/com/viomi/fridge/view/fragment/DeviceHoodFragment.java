package com.viomi.fridge.view.fragment;

import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.miot.api.MiotManager;
import com.miot.common.people.People;
import com.viomi.common.module.okhttp.OkHttpClientManager;
import com.viomi.fridge.R;
import com.viomi.fridge.api.http.HttpConnect;
import com.viomi.fridge.util.JsonUitls;
import com.viomi.fridge.util.PhoneUtil;
import com.viomi.fridge.util.ToolUtil;
import com.viomi.fridge.util.log;
import com.viomi.fridge.view.widget.BubbleView;
import com.viomi.fridge.view.widget.CircleProgressView;
import com.viomi.fridge.view.widget.HoodPMDialog;
import com.viomi.fridge.view.widget.HoodSettingDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.Call;

/**
 * 烟机 Fragment
 * Created by William on 2017/4/14.
 */


public class DeviceHoodFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG = DeviceHoodFragment.class.getSimpleName();
    private String mParam1, mParam2, airState, delayTime, aqi_thd, aqi_hour, aqi_min, aqi_time;
    private Button btn1, btn2, btn3, btn4, btn5;
    private LinearLayout llHoodStatus, llPMLayout;
    private BubbleView mBubbleView;
    private People mPeople;
    private TextView stove_status1, stove_status2, tvMode, tvTime, tvCount, tvPower, tvLow, tvMiddle, tvHigh, tvLight, tvStokeStatus, tvBattery, tvSetting,
            tvUsing1, tvUsing2, tvPurify, tvAir, tvPMSetting;
    private View mView;
    private ImageView light_status, stoke1_img, stoke2_img, ivFan, ivOff, ivBattery, ivFanOut;
    private final MyHandler mHandler = new MyHandler(this);
    private RotateAnimation ra;
    private boolean isSetting = false, isDelay = false;
    private ClipDrawable mClipDrawable;
    private CircleProgressView mCircleProgressView;


    public static DeviceHoodFragment newInstance(String param1, String param2) {
        DeviceHoodFragment fragment = new DeviceHoodFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            log.myE("mParam1mParam2", mParam1 + mParam2 + "");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_device_hood, container, false);

        btn1 = (Button) view.findViewById(R.id.btn1);
        btn2 = (Button) view.findViewById(R.id.btn2);
        btn3 = (Button) view.findViewById(R.id.btn3);
        btn4 = (Button) view.findViewById(R.id.btn4);
        btn5 = (Button) view.findViewById(R.id.btn5);

        mView = view.findViewById(R.id.pm_line);
        tvMode = (TextView) view.findViewById(R.id.mode_text);
        tvTime = (TextView) view.findViewById(R.id.running_time);
        tvCount = (TextView) view.findViewById(R.id.count_value);
        tvPower = (TextView) view.findViewById(R.id.tv1);
        tvLow = (TextView) view.findViewById(R.id.tv2);
        tvMiddle = (TextView) view.findViewById(R.id.tv3);
        tvHigh = (TextView) view.findViewById(R.id.tv4);
        tvLight = (TextView) view.findViewById(R.id.tv5);
        tvStokeStatus = (TextView) view.findViewById(R.id.hood_status);
        tvBattery = (TextView) view.findViewById(R.id.stoke_per);
        tvSetting = (TextView) view.findViewById(R.id.hood_setting);
        tvPurify = (TextView) view.findViewById(R.id.purify_value);
        tvAir = (TextView) view.findViewById(R.id.air_quality);
        tvPMSetting = (TextView) view.findViewById(R.id.pm_setting);
        light_status = (ImageView) view.findViewById(R.id.light_status);
        ivFan = (ImageView) view.findViewById(R.id.fan);
        ivOff = (ImageView) view.findViewById(R.id.hood_off_img);
        ivFanOut = (ImageView) view.findViewById(R.id.fan_out);
        llPMLayout = (LinearLayout) view.findViewById(R.id.pm_layout);
        llHoodStatus = (LinearLayout) view.findViewById(R.id.hood_status_layout);
        mBubbleView = (BubbleView) view.findViewById(R.id.hood_bubble);
        mCircleProgressView = (CircleProgressView) view.findViewById(R.id.hood_delay_progress);

        tvUsing1 = (TextView) view.findViewById(R.id.stove_using1);
        tvUsing2 = (TextView) view.findViewById(R.id.stove_using2);
        stove_status1 = (TextView) view.findViewById(R.id.stove_status1);
        stove_status2 = (TextView) view.findViewById(R.id.stove_status2);
        stoke1_img = (ImageView) view.findViewById(R.id.stoke1_img);
        stoke2_img = (ImageView) view.findViewById(R.id.stoke2_img);

        ivBattery = (ImageView) view.findViewById(R.id.hood_battery_icon);
        LayerDrawable layerDrawable = (LayerDrawable) ivBattery.getDrawable();
        mClipDrawable = (ClipDrawable) layerDrawable.findDrawableByLayerId(R.id.battery_full);

        TextView device_name = (TextView) view.findViewById(R.id.device_name);
        device_name.setText(mParam2);

        mPeople = MiotManager.getPeopleManager().getPeople();
        if (mPeople == null) {
            mPeople = new People("", "");
        }

        mHandler.sendEmptyMessage(0);

        initListener();

        initCircleProgress();

        return view;
    }

    private void initListener() {
        // 电源
        btn1.setOnClickListener(v -> {
            isSetting = true;
            if (btn1.isSelected() && !isDelay && !"0".equals(delayTime) && !"error".equals(delayTime))
                setPower("1");
            else if (isDelay || btn1.isSelected()) {
                setPower("0");
//                    tvMode.setText("油烟机已关闭");
//                    ivOff.setVisibility(View.VISIBLE);
//                    ivFan.setVisibility(View.GONE);
//                    rotate_fan.clearAnimation();
//                    mBubbleView.setStop(true);
//                    btn2.setSelected(false);
//                    tvLow.setSelected(false);
//                    btn3.setSelected(false);
//                    tvMiddle.setSelected(false);
//                    btn4.setSelected(false);
//                    tvHigh.setSelected(false);
//                    btn2.setEnabled(false);
//                    btn3.setEnabled(false);
//                    btn4.setEnabled(false);
            } else {
                setPower("2");
            }
            if (isDelay) {
                mHandler.removeCallbacks(mRunnable);
                btn1.setSelected(false);
                tvPower.setSelected(false);
            } else {
                btn1.setSelected(!btn1.isSelected());
                tvPower.setSelected(!tvPower.isSelected());
            }
        });

        // 低档排烟
        btn2.setOnClickListener(v -> {
            isSetting = true;
            if (btn1.isSelected()) {
                if (btn2.isSelected()) {
//                        tvMode.setText("空闲中");
//                        rotate_fan.clearAnimation();
//                        mBubbleView.setStop(true);
                    setWindLevel("0");
                } else {
//                        startAnim(3000, 30);
                    setWindLevel("1");
                }
                btn2.setSelected(!btn2.isSelected());
                btn3.setSelected(false);
                btn4.setSelected(false);
                tvLow.setSelected(!tvLow.isSelected());
                tvMiddle.setSelected(false);
                tvHigh.setSelected(false);
//                    ivOff.setVisibility(View.GONE);
//                    tvMode.setText("低档排烟模式");
            }
        });

        // 高档排烟
        btn3.setOnClickListener(v -> {
            isSetting = true;
            if (btn1.isSelected()) {
                if (btn3.isSelected()) {
//                        tvMode.setText("空闲中");
//                        rotate_fan.clearAnimation();
//                        mBubbleView.setStop(true);
                    setWindLevel("0");
                } else {
//                        startAnim(2000, 20);
                    setWindLevel("16");
                }
                btn2.setSelected(false);
                btn3.setSelected(!btn3.isSelected());
                btn4.setSelected(false);
                tvLow.setSelected(false);
                tvMiddle.setSelected(!tvMiddle.isSelected());
                tvHigh.setSelected(false);
//                    ivOff.setVisibility(View.GONE);
//                    tvMode.setText("高档排烟模式");
            }
        });

        // 爆炒排烟
        btn4.setOnClickListener(v -> {
            isSetting = true;
            if (btn1.isSelected()) {
                if (btn4.isSelected()) {
//                        tvMode.setText("空闲中");
//                        rotate_fan.clearAnimation();
//                        mBubbleView.setStop(true);
                    setWindLevel("0");
                } else {
//                        startAnim(1000, 10);
                    setWindLevel("4");
                }
                btn2.setSelected(false);
                btn3.setSelected(false);
                btn4.setSelected(!btn4.isSelected());
                tvLow.setSelected(false);
                tvMiddle.setSelected(false);
                tvHigh.setSelected(!tvHigh.isSelected());
//                    ivOff.setVisibility(View.GONE);
//                    tvMode.setText("爆炒排烟模式");
            }
        });

        // 灯光
        btn5.setOnClickListener(v -> {
            isSetting = true;
            if (btn5.isSelected()) {
                setLight("0");
//                    light_status.setVisibility(View.GONE);
            } else {
                setLight("1");
//                    light_status.setVisibility(View.VISIBLE);
            }
            tvLight.setSelected(!tvLight.isSelected());
            btn5.setSelected(!btn5.isSelected());
        });

        // 设置
        tvSetting.setOnClickListener(v -> {
            HoodSettingDialog hoodSettingDialog = new HoodSettingDialog(getActivity(), mParam1, mPeople);
            hoodSettingDialog.show();
        });

        // PM 2.5
        tvPMSetting.setOnClickListener(v -> {
            HoodPMDialog hoodPMDialog = new HoodPMDialog(getActivity(), mParam1, mPeople, airState, aqi_thd, aqi_hour, aqi_min, aqi_time);
            hoodPMDialog.show();
        });
    }

    private void initCircleProgress() {
        mCircleProgressView.setRoundColor(ContextCompat.getColor(getActivity(), R.color.fade_green));
        mCircleProgressView.setStrokeWidth(4);
        mCircleProgressView.setText("");
    }

    // 获取设备信息
    private void getDeviceInfo() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "get_prop");
            jsonObject.put("did", mParam1);
            jsonObject.put("id", 1);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put("run_time");
            jsonArray.put("power_state");
            jsonArray.put("wind_state");
            jsonArray.put("light_state");
            jsonArray.put("link_state");
            jsonArray.put("stove1_data");
            jsonArray.put("stove2_data");
            jsonArray.put("pm2_5");
            jsonArray.put("battary_life");
            jsonArray.put("poweroff_delaytime");
            jsonArray.put("aqi_state");
            jsonArray.put("aqi_thd");
            jsonArray.put("aqi_hour");
            jsonArray.put("aqi_min");
            jsonArray.put("aqi_time");
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        HttpConnect.rpcDevice(mPeople, mParam1, jsonObject.toString(), new OkHttpClientManager.ResultCallback<String>() {
            @Override
            public void onError(Call call, Exception e) {
                Message msg = mHandler.obtainMessage();
                msg.what = 2;
                mHandler.sendMessage(msg);
            }

            @Override
            public void onResponse(String response) {
                Message msg = mHandler.obtainMessage();
                msg.what = 1;
                msg.obj = response;
                mHandler.sendMessage(msg);
            }
        });
    }

    // 获取设备数据
    private void getDeviceData() {
        long time_end = System.currentTimeMillis() / 1000;
        long time_start = time_end - 365 * 24 * 60 * 60;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("did", mParam1);
            jsonObject.put("type", "store");
            jsonObject.put("key", "life_record");
            jsonObject.put("time_start", time_start);
            jsonObject.put("time_end", time_end);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        HttpConnect.deviceData(mPeople, jsonObject.toString(), new OkHttpClientManager.ResultCallback<String>() {
            @Override
            public void onError(Call call, Exception e) {
                Message message = mHandler.obtainMessage();
                message.what = 2;
                mHandler.sendMessage(message);
            }

            @Override
            public void onResponse(String response) {
                Message message = mHandler.obtainMessage();
                message.what = 8;
                message.obj = response;
                mHandler.sendMessage(message);
            }
        });
    }

    // 设置电源
    private void setPower(String status) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_power");
            jsonObject.put("did", mParam1);
            jsonObject.put("id", 1);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(status);
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        HttpConnect.rpcDevice(mPeople, mParam1, jsonObject.toString(), new OkHttpClientManager.ResultCallback<String>() {
            @Override
            public void onError(Call call, Exception e) {
                isSetting = false;
                Message msg = mHandler.obtainMessage();
                msg.what = 2;
                mHandler.sendMessage(msg);
            }

            @Override
            public void onResponse(String response) {
                isSetting = false;
                Message msg = mHandler.obtainMessage();
                msg.what = 3;
                msg.obj = response;
                mHandler.sendMessage(msg);
            }
        });
    }

    // 设置风速
    private void setWindLevel(String status) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_wind");
            jsonObject.put("did", mParam1);
            jsonObject.put("id", 1);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(status);
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        HttpConnect.rpcDevice(mPeople, mParam1, jsonObject.toString(), new OkHttpClientManager.ResultCallback<String>() {
            @Override
            public void onError(Call call, Exception e) {
                isSetting = false;
                Message msg = mHandler.obtainMessage();
                msg.what = 2;
                mHandler.sendMessage(msg);
            }

            @Override
            public void onResponse(String response) {
                isSetting = false;
                Message msg = mHandler.obtainMessage();
                msg.what = 5;
                msg.obj = response;
                mHandler.sendMessage(msg);
            }
        });
    }

    // 设置灯光
    private void setLight(String status) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_light");
            jsonObject.put("did", mParam1);
            jsonObject.put("id", 1);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(status);
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        HttpConnect.rpcDevice(mPeople, mParam1, jsonObject.toString(), new OkHttpClientManager.ResultCallback<String>() {
            @Override
            public void onError(Call call, Exception e) {
                isSetting = false;
                Message msg = mHandler.obtainMessage();
                msg.what = 2;
                mHandler.sendMessage(msg);
            }

            @Override
            public void onResponse(String response) {
                isSetting = false;
                Message msg = mHandler.obtainMessage();
                msg.what = 7;
                msg.obj = response;
                mHandler.sendMessage(msg);
            }
        });
    }

    private void parseJson(String result) {
        try {
            JSONObject json = new JSONObject(result);
            String code = JsonUitls.getString(json, "code");
            if ("0".equals(code)) {
                JSONArray resultArray = JsonUitls.getJSONArray(json, "result");

                llHoodStatus.setVisibility(View.VISIBLE);
                // 烟灶联动与火力
                String link_state = resultArray.optString(4);
                String stove1_data = resultArray.optString(5);
                String stove2_data = resultArray.optString(6);
                switch (link_state) {
                    case "0":
                        tvStokeStatus.setText("烟灶未联动");
                        ivBattery.setVisibility(View.GONE);
                        tvBattery.setVisibility(View.GONE);
                        tvUsing1.setVisibility(View.GONE);
                        tvUsing2.setVisibility(View.GONE);
                        stove_status1.setText("燃气灶未连接");
                        stove_status2.setText("燃气灶未连接");
                        stoke1_img.setImageResource(R.mipmap.icon_hood_non_link);
                        stoke2_img.setImageResource(R.mipmap.icon_hood_non_link);
                        break;
                    case "1":
                        tvStokeStatus.setText("烟灶未联动");
                        ivBattery.setVisibility(View.GONE);
                        tvBattery.setVisibility(View.GONE);
                        tvUsing1.setVisibility(View.GONE);
                        tvUsing2.setVisibility(View.GONE);
                        stove_status1.setText("燃气灶未连接");
                        stove_status2.setText("燃气灶未连接");
                        stoke1_img.setImageResource(R.mipmap.icon_hood_non_link);
                        stoke2_img.setImageResource(R.mipmap.icon_hood_non_link);
                        break;
                    default:
                        tvStokeStatus.setText("烟灶联动中");
                        ivBattery.setVisibility(View.VISIBLE);
                        tvBattery.setVisibility(View.VISIBLE);

                        switch (stove1_data) {
                            case "0":
                                stove_status1.setText("燃气灶已关闭");
                                tvUsing1.setVisibility(View.GONE);
                                stoke1_img.setImageResource(R.mipmap.icon_hood_off);
                                break;
                            case "1":
                                stove_status1.setText("小火");
                                tvUsing1.setVisibility(View.VISIBLE);
                                stoke1_img.setImageResource(R.mipmap.icon_hood_low_fire);
                                break;
                            case "2":
                                stove_status1.setText("大火");
                                tvUsing1.setVisibility(View.VISIBLE);
                                stoke1_img.setImageResource(R.mipmap.icon_hood_high_fire);
                                break;
                        }

                        switch (stove2_data) {
                            case "0":
                                stove_status2.setText("燃气灶已关闭");
                                tvUsing2.setVisibility(View.GONE);
                                stoke2_img.setImageResource(R.mipmap.icon_hood_off);
                                break;
                            case "1":
                                stove_status2.setText("小火");
                                tvUsing2.setVisibility(View.VISIBLE);
                                stoke2_img.setImageResource(R.mipmap.icon_hood_low_fire);
                                break;
                            case "2":
                                stove_status2.setText("大火");
                                tvUsing2.setVisibility(View.VISIBLE);
                                stoke2_img.setImageResource(R.mipmap.icon_hood_high_fire);
                                break;
                        }
                        break;
                }

                // 电池
                String battery = resultArray.optString(8);
                if (!battery.equals("error")) {
                    tvBattery.setText("" + Integer.parseInt(battery) + "%");
                    mClipDrawable.setLevel(calculateLevel(Integer.parseInt(battery)));
                }

                // 延时时间
                delayTime = resultArray.optString(9);

                if (isSetting) return;
                // 电源状态
                String powerStatus = resultArray.optString(1);
                if (powerStatus.equals("0")) {
                    tvMode.setText("油烟机已关闭");
                    ivFanOut.setVisibility(View.VISIBLE);
                    ivFan.clearAnimation();
                    mBubbleView.setStop(true);
                    ivOff.setVisibility(View.VISIBLE);
                    ivFan.setVisibility(View.GONE);
                    mCircleProgressView.setVisibility(View.GONE);
                    if (mCircleProgressView != null) mCircleProgressView.cancelAnimate();
                    mHandler.removeCallbacks(mRunnable);
                    isDelay = false;
                    btn1.setSelected(false);
                    tvPower.setSelected(false);
                    btn2.setSelected(false);
                    tvLow.setSelected(false);
                    btn3.setSelected(false);
                    tvMiddle.setSelected(false);
                    btn4.setSelected(false);
                    tvHigh.setSelected(false);
                    btn2.setEnabled(false);
                    btn3.setEnabled(false);
                    btn4.setEnabled(false);
                } else {
                    ivOff.setVisibility(View.GONE);
                    if (powerStatus.equals("1")) {  // 待机
                        ivFan.setVisibility(View.GONE);
                        ivFanOut.setVisibility(View.GONE);
                        ivFan.clearAnimation();
                        if (mCircleProgressView.getVisibility() == View.GONE) {
                            mCircleProgressView.setVisibility(View.VISIBLE);
                            if (delayTime.equals("0") || delayTime.equals("error"))
                                delayTime = "120";
                            mCircleProgressView.setPeriod(Integer.parseInt(delayTime) * 10);
                            mCircleProgressView.runAnimate(100);
                            tvMode.setText("延时关机中");
                            isDelay = true;
                            mHandler.post(mRunnable);
                        }
                    } else {
                        isDelay = false;
                        ivFanOut.setVisibility(View.VISIBLE);
                        mHandler.removeCallbacks(mRunnable);
                        mCircleProgressView.setVisibility(View.GONE);
                        if (mCircleProgressView != null) mCircleProgressView.cancelAnimate();
                        btn1.setSelected(true);
                        tvPower.setSelected(true);
                        ivFan.setVisibility(View.VISIBLE);
                    }
                    btn2.setEnabled(true);
                    btn3.setEnabled(true);
                    btn4.setEnabled(true);
                    // 风挡
                    String windLevel = resultArray.optString(2);
                    switch (windLevel) {
                        // 关闭
                        case "0":
                            if (mCircleProgressView.getVisibility() == View.GONE)
                                tvMode.setText("空闲中");
                            ivFan.clearAnimation();
                            mBubbleView.setStop(true);
                            btn2.setSelected(false);
                            tvLow.setSelected(false);
                            btn3.setSelected(false);
                            tvMiddle.setSelected(false);
                            btn4.setSelected(false);
                            tvHigh.setSelected(false);
                            break;
                        // 低档
                        case "1":
                            if (mCircleProgressView.getVisibility() == View.GONE)
                                tvMode.setText("低档排烟模式");
                            startAnim(3000, 30);
                            btn2.setSelected(true);
                            tvLow.setSelected(true);
                            btn3.setSelected(false);
                            tvMiddle.setSelected(false);
                            btn4.setSelected(false);
                            tvHigh.setSelected(false);
                            break;
                        // 爆炒
                        case "4": {
                            if (mCircleProgressView.getVisibility() == View.GONE)
                                tvMode.setText("爆炒排烟模式");
                            startAnim(1000, 10);
                            btn2.setSelected(false);
                            tvLow.setSelected(false);
                            btn3.setSelected(false);
                            tvMiddle.setSelected(false);
                            btn4.setSelected(true);
                            tvHigh.setSelected(true);
                        }
                        break;
                        // 高档
                        case "16": {
                            if (mCircleProgressView.getVisibility() == View.GONE)
                                tvMode.setText("高档排烟模式");
                            startAnim(2000, 20);
                            btn2.setSelected(false);
                            tvLow.setSelected(false);
                            btn3.setSelected(true);
                            tvMiddle.setSelected(true);
                            btn4.setSelected(false);
                            tvHigh.setSelected(false);
                        }
                        break;
                    }
                }

                // 灯光
                String lightStatus = resultArray.optString(3);
                switch (lightStatus) {
                    case "0":
                        btn5.setSelected(false);
                        tvLight.setSelected(false);
                        light_status.setVisibility(View.GONE);
                        break;
                    case "1":
                        btn5.setSelected(true);
                        tvLight.setSelected(true);
                        light_status.setVisibility(View.VISIBLE);
                        break;
                }

                // PM 2.5
                String pm2_5 = resultArray.optString(7);
                airState = resultArray.optString(10);
                if (!pm2_5.equals("error")) {
                    mView.setVisibility(View.VISIBLE);
                    llPMLayout.setVisibility(View.VISIBLE);
                    tvAir.setText(setAirQuality(pm2_5));
                    if (!airState.equals("error")) {
                        aqi_thd = resultArray.optString(11);
                        aqi_hour = resultArray.optString(12);
                        aqi_min = resultArray.optString(13);
                        aqi_time = resultArray.optString(14);
                        tvPMSetting.setVisibility(View.VISIBLE);
                    }
                } else {
                    mView.setVisibility(View.GONE);
                    llPMLayout.setVisibility(View.GONE);
                    tvPMSetting.setVisibility(View.GONE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // 空气质量
    private String setAirQuality(String pm2_5) {
        String str = "--";
        switch (pm2_5) {
            case "1":
                str = "优";
                break;
            case "2":
                str = "良";
                break;
            case "3":
                str = "轻度污染";
                break;
            case "4":
                str = "中度污染";
                break;
            case "5":
                str = "重度污染";
                break;
            case "6":
                str = "严重污染";
                break;
        }
        return str;
    }

    private boolean isOffLine(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            String code = jsonObject.optString("code");
            if (code.equals("-2")) {
                llHoodStatus.setVisibility(View.GONE);
                ivFan.setVisibility(View.GONE);
                light_status.setVisibility(View.GONE);
                tvMode.setText("油烟机已关闭");
                ivOff.setVisibility(View.VISIBLE);
                tvTime.setText("--");
                tvCount.setText("--");
                btn1.setSelected(false);
                tvPower.setSelected(false);
                btn2.setSelected(false);
                tvLow.setSelected(false);
                btn3.setSelected(false);
                tvMiddle.setSelected(false);
                btn4.setSelected(false);
                tvHigh.setSelected(false);
                btn5.setSelected(false);
                tvLight.setSelected(false);
                btn2.setEnabled(false);
                btn3.setEnabled(false);
                btn4.setEnabled(false);
                return true;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }

    private void startAnim(int d, int time) {
        if (mCircleProgressView.getVisibility() == View.GONE) {
            if (ivFan.getAnimation() == null) {
                ra = new RotateAnimation(0, -359, RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f);
                ra.setRepeatCount(RotateAnimation.INFINITE);    // 循环
                ra.setInterpolator(new LinearInterpolator());   // 匀速
                ivFan.startAnimation(ra);
            }
            ra.setDuration(d); // 持续时间
        }
        mBubbleView.setStop(false);
        mBubbleView.setTime(time);
    }

    private int calculateLevel(int progress) {
        int leftOffset = PhoneUtil.dipToPx(getActivity(), 2);
        int powerLength = PhoneUtil.dipToPx(getActivity(), 26.5f);// 40 px in hdpi
        int totalLength = PhoneUtil.dipToPx(getActivity(), 32.5f);// 49 px in hdpi
        return (leftOffset + powerLength * progress / 100) * 10000 / totalLength;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mHandler.removeCallbacksAndMessages(null);
        ivFan.clearAnimation();
        mBubbleView.recycle();
        mBubbleView.setStop(true);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (hidden) {
            mHandler.removeCallbacksAndMessages(null);
            ivFan.clearAnimation();
            mBubbleView.setStop(true);
        }
    }

    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            btn1.setSelected(!btn1.isSelected());
            tvPower.setSelected(!tvPower.isSelected());
            mHandler.postDelayed(mRunnable, 500);
        }
    };

    private static class MyHandler extends Handler {
        WeakReference<DeviceHoodFragment> weakReference;

        public MyHandler(DeviceHoodFragment fragment) {
            this.weakReference = new WeakReference<>(fragment);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            DeviceHoodFragment fragment = weakReference.get();
            String result;
            switch (msg.what) {
                case 0:
                    fragment.getDeviceInfo();
                    fragment.getDeviceData();
                    fragment.mHandler.sendEmptyMessageDelayed(0, 3000);
                    break;
                case 1:     // get_prop
                    result = (String) msg.obj;
                    if (!fragment.isOffLine(result))
                        fragment.parseJson(result);
                    log.d(TAG + "：getProp：", result + "");
                    break;
                case 2:     // 网络请求失败
                    try {
                        Toast.makeText(fragment.getActivity(), fragment.getActivity().getResources().getString(R.string.toast_http_result_error),
                                Toast.LENGTH_SHORT).show();
                    }catch (NullPointerException e){
                        e.printStackTrace();
                    }
                    break;
                case 3:     // 设置电源
                    result = (String) msg.obj;
                    if (fragment.isOffLine(result))
                        Toast.makeText(fragment.getActivity(), "设置失败，设备已离线", Toast.LENGTH_SHORT).show();
                    log.d(TAG + "：setPower：", result + "");
                    break;
                case 4:
                    Toast.makeText(fragment.getActivity(), "设置失败", Toast.LENGTH_SHORT).show();
                    break;
                case 5:     // 设置风速
                    result = (String) msg.obj;
                    if (fragment.isOffLine(result))
                        Toast.makeText(fragment.getActivity(), "设置失败，设备已离线", Toast.LENGTH_SHORT).show();
                    log.d(TAG + "：setWind：", result + "");
                    break;
                case 6:
//                    result = (String) msg.obj;
//                    log.myE("getprop----------------------------------fail-------------------------------", result + "");
                    break;
                case 7:     // 设置灯光
                    result = (String) msg.obj;
                    if (fragment.isOffLine(result))
                        Toast.makeText(fragment.getActivity(), "设置失败，设备已离线", Toast.LENGTH_SHORT).show();
                    log.d(TAG + "：setLight：", result + "");
                    break;
                case 8:     // 设备统计数据
                    result = (String) msg.obj;
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.optString("code").equals("0")) {
                            JSONArray jsonArray = JsonUitls.getJSONArray(jsonObject, "result");
                            if (jsonArray.length() == 0) return;
                            JSONObject data = jsonArray.optJSONObject(0);
                            String value = data.optString("value");
                            if (value.equals("")) return;
                            Pattern pattern = Pattern.compile("\\d+");
                            Matcher matcher = pattern.matcher(value);
                            matcher.find();
                            int count = Integer.parseInt(matcher.group(0));
                            matcher.find();
                            int time = Integer.parseInt(matcher.group(0));
                            matcher.find();
                            int purify = Integer.parseInt(matcher.group(0));

                            fragment.tvCount.setText("" + count + "");
                            fragment.tvTime.setText(Html.fromHtml(ToolUtil.timeFormat(time)));
                            fragment.tvPurify.setText("" + purify + "");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    log.d(TAG + "：device---data：", result + "");
                    break;
            }
        }
    }

}
