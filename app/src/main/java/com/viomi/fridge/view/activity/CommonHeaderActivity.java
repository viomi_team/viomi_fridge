package com.viomi.fridge.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.viomi.fridge.R;
import com.viomi.fridge.device.AppConfig;
import com.viomi.fridge.util.FileUtil;
import com.viomi.fridge.view.web.activity.BrowserActivity;

public class CommonHeaderActivity extends WebActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTitle = "水质地图";
        FileUtil.writeTxtToFile(CommonHeaderActivity.this, AppConfig.EventKey.EVENT_ENTER_WATER_MAP, null);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        FileUtil.writeTxtToFile(CommonHeaderActivity.this, AppConfig.EventKey.EVENT_EXIT_WATER_MAP, null);
    }
}
