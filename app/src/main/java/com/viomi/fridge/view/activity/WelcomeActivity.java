package com.viomi.fridge.view.activity;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.viomi.common.module.permission.EasyPermission;
import com.viomi.common.module.permission.PermissionCode;
import com.viomi.fridge.R;
import com.viomi.fridge.common.GlobalParams;
import com.viomi.fridge.manager.H5UrlManager;
import com.viomi.fridge.util.ApkUtil;
import com.viomi.fridge.util.log;

import java.util.List;

/**
 * Created by young2 on 2016/12/17.
 */

public class WelcomeActivity extends BaseHandlerActivity implements EasyPermission.PermissionCallback{
    private final static String TAG=WelcomeActivity.class.getSimpleName();
    private static final byte MSG_WHAT_LOAD= 1;
    private int TIME_LENGTH_MIN=0;
    private static final String[] mPermissions = new String[] {
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e(TAG,"onCreate");
        setContentView(R.layout.activity_welcome);
        init();

    }


    private void init(){
        GlobalParams.getInstance().setTestCutTimeEnable(false);
        mHandler.sendEmptyMessageDelayed(MSG_WHAT_LOAD,TIME_LENGTH_MIN);
        //requestPermission();
    }


    @Override
    protected void processMsg(Message msg) {
        switch (msg.what){
            case MSG_WHAT_LOAD:
                entryApp();
                Log.e(TAG,"start app");
                startActivity(new Intent(WelcomeActivity.this,MainNewV2Activity.class));
                finish();
                break;
        }
    }


    private void requestPermission(){
        EasyPermission.with(WelcomeActivity.this)
                .addRequestCode(PermissionCode.REQUEST_LOCATION)
                .permissions(mPermissions)
                .request();
    }

    @Override
    public void onPermissionGranted(int requestCode, List<String> perms) {

    }

    @Override
    public void onPermissionDenied(int requestCode, List<String> perms) {
        EasyPermission.checkDeniedPermissionsNeverAskAgain(this, getString(R.string.text_please_open_location_permission), perms,false);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void entryApp(){
        int versionCode= ApkUtil.getVersionCode();
        if(versionCode>GlobalParams.getInstance().getAppVersionCode()){//升级完，第一次进入新版本
            H5UrlManager.clearH5Files();
            //DeviceParamsManager.deleteParamsSet(ViomiApplication.getContext());
        }
        GlobalParams.getInstance().setAppVersionCode(versionCode);
        vmallH5Process();
    }

    /***
     * 处理商城h5文件
     */
    private void vmallH5Process(){

        if(!H5UrlManager.vmallH5Process()){
            Log.e(TAG,"vmallH5Process result false!!!");
        }
        GlobalParams.Version_Vmall_H5= H5UrlManager.getH5Version();
        GlobalParams.getInstance().setVmallH5Version(GlobalParams.Version_Vmall_H5);
        log.d(TAG,"Version_Vmall_H5="+GlobalParams.Version_Vmall_H5);
    }

}
