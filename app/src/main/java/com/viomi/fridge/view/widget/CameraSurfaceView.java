package com.viomi.fridge.view.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.SurfaceView;

/**
 * 摄像头
 * Created by William on 2017/11/6.
 */

public class CameraSurfaceView extends SurfaceView {

    private final static float[] Ratio = new float[]{16, 9};

    public CameraSurfaceView(Context context) {
        super(context);
    }

    public CameraSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int originalWidth = MeasureSpec.getSize(widthMeasureSpec);
        int originalHeight = (int) (originalWidth * Ratio[1] / Ratio[0]);

        super.onMeasure(
                MeasureSpec.makeMeasureSpec(originalWidth, MeasureSpec.EXACTLY),
                MeasureSpec.makeMeasureSpec(originalHeight, MeasureSpec.EXACTLY));
    }

}
