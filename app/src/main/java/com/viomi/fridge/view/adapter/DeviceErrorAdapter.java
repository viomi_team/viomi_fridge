package com.viomi.fridge.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.viomi.fridge.R;
import com.viomi.fridge.model.bean.DeviceErrorData;

import java.util.ArrayList;
import java.util.List;

/**
 * 设备故障列表适配器
 * Created by William on 2017/12/13.
 */

public class DeviceErrorAdapter extends BaseAdapter {
    private Context mContext;
    private List<DeviceErrorData> mList;

    public DeviceErrorAdapter(Context context, List<DeviceErrorData> list) {
        mContext = context;
        mList = list;
        if (mList == null) mList = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(mContext).inflate(R.layout.viewholder_device_error, parent, false);
            viewHolder.titleTextView = (TextView) convertView.findViewById(R.id.holder_device_error_title);
            convertView.setTag(viewHolder);
        } else viewHolder = (ViewHolder) convertView.getTag();

        DeviceErrorData data = mList.get(position);
        String str = String.format(mContext.getResources().getString(R.string.device_heat_kettle_error_num), position + 1) + data.title;
        viewHolder.titleTextView.setText(str);

        return convertView;
    }

    private class ViewHolder {
        private TextView titleTextView;
    }
}
