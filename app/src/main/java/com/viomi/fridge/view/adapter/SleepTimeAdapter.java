package com.viomi.fridge.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RadioButton;
import android.widget.TextView;

import com.viomi.fridge.R;
import com.viomi.fridge.model.bean.ScreenSleepBean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by viomi on 2017/3/21.
 */

public class SleepTimeAdapter extends BaseAdapter {

    private List<ScreenSleepBean> list = new ArrayList();
    private Context context;
    private LayoutInflater inflater;

    public SleepTimeAdapter(List<ScreenSleepBean> list, Context context) {
        this.list = list;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
         ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.screen_sleep_time_item_layout, null);
            holder = new ViewHolder();
            holder.sleep_time = (TextView) convertView.findViewById(R.id.sleep_time);
            holder.select_btn = (RadioButton) convertView.findViewById(R.id.select_btn);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.sleep_time.setText(list.get(position).getTimeText());
        holder.select_btn.setChecked(list.get(position).isSelect());
        return convertView;
    }

    class ViewHolder{
        TextView sleep_time;
        RadioButton select_btn;
    }
}
