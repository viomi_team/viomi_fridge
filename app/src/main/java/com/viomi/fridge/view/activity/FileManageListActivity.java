package com.viomi.fridge.view.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.viomi.fridge.R;
import com.viomi.fridge.model.bean.FileInfo;
import com.viomi.fridge.util.FileOperationHelper;
import com.viomi.fridge.util.FileUtil;
import com.viomi.fridge.util.SDCardUtils;
import com.viomi.fridge.view.adapter.FileManageAdapter;
import com.viomi.fridge.view.widget.ClearIngDialog;
import com.viomi.fridge.view.widget.ClearSuccessDialog;
import com.viomi.fridge.view.widget.RecyclerSpace;
import com.viomi.fridge.view.widget.WrapContentGridLayoutManager;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ljh on 2017/11/17.
 */

public class FileManageListActivity extends BaseActivity implements FileManageAdapter.FileManageAdapterCallback, FileOperationHelper.IOperationProgressListener {
    private ImageView imgBack;
    private TextView tvTitle;
    private RecyclerView recyclerView;
    private TextView tvSelectAll;
    private TextView tvClearInfo;
    private TextView tvClear;
    private ClearIngDialog mClearIngDialog;
    private ClearSuccessDialog mClearSuccessDialog;
    //参数
    boolean cleared = false;
    long clearSize = 0;
    FileManageAdapter mFileManageAdapter;
    List<FileInfo> mFileInfos = new ArrayList<>();
    ArrayList<FileInfo> selectFiles = new ArrayList<>();
    FileUtil.FileType fileType = FileUtil.FileType.FILE_MUSIC;
    private FileOperationHelper mFileOperationHelper;


    public static void acitonStartForResult(Context context, FileUtil.FileType fileType, int reqCode) {
        Intent intent = new Intent(context, FileManageListActivity.class);
        intent.putExtra("fileType", fileType);
        ((Activity) context).startActivityForResult(intent, reqCode);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file_manage_list);
        initView();
        initListener();
        init();
    }

    private void initView() {
        mClearIngDialog = new ClearIngDialog(mContext);
        mClearSuccessDialog = new ClearSuccessDialog(mContext);
        //
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        tvSelectAll = (TextView) findViewById(R.id.tvSelectAll);
        tvClearInfo = (TextView) findViewById(R.id.tvClearInfo);
        tvClear = (TextView) findViewById(R.id.tvClear);
        //
        mFileManageAdapter = new FileManageAdapter(mContext, mFileInfos);
        mFileManageAdapter.setCallback(this);

        WrapContentGridLayoutManager gridLayoutManager = new WrapContentGridLayoutManager(this, 6);
        RecyclerSpace wideSpace = new RecyclerSpace(2, 0, 0);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.addItemDecoration(wideSpace);
        recyclerView.setAdapter(mFileManageAdapter);
    }

    private void initListener() {
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cleared)
                    setResult(Activity.RESULT_OK);
                finish();
            }
        });
        tvClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(mContext)
                        .setMessage("清理后不可恢复，是否确认清理？")
                        .setNegativeButton("否", null)
                        .setPositiveButton("是", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                mClearIngDialog.show();
                                selectFiles.clear();
                                //删除文件
                                for (FileInfo temp : mFileInfos) {
                                    if (temp.isSelected()) {
                                        selectFiles.add(temp);
                                    }
                                }
                                mFileOperationHelper.Delete(selectFiles);
                            }
                        }).show();
            }
        });

        tvSelectAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearSize = 0;
                v.setSelected(!v.isSelected());
                for (FileInfo temp : mFileInfos) {
                    temp.setSelected(v.isSelected());
                    if (v.isSelected())
                        clearSize += temp.getFileSize();
                }
                showClearInfo();
                mFileManageAdapter.notifyDataSetChanged();
            }
        });
    }

    private void init() {
        fileType = (FileUtil.FileType) getIntent().getSerializableExtra("fileType");
        if (fileType == FileUtil.FileType.FILE_MUSIC) {
            tvTitle.setText(R.string.music);
            mFileInfos.addAll(FileUtil.getFileInfoList(mContext, FileUtil.FileType.FILE_MUSIC));
        } else if (fileType == FileUtil.FileType.FILE_VIDEO) {
            tvTitle.setText(R.string.video);
            mFileInfos.addAll(FileUtil.getFileInfoList(mContext, FileUtil.FileType.FILE_VIDEO));
        } else if (fileType == FileUtil.FileType.FILE_PIC) {
            tvTitle.setText(R.string.pic);
            mFileInfos.addAll(FileUtil.getFileInfoList(mContext, FileUtil.FileType.FILE_PIC));
        }
        mFileManageAdapter.notifyDataSetChanged();

        mFileOperationHelper = new FileOperationHelper(this, mContext);
    }

    @Override
    public void select(int position) {//TODO LJH
        mFileInfos.get(position).setSelected(!mFileInfos.get(position).isSelected());
        if (mFileInfos.get(position).isSelected())
            clearSize += mFileInfos.get(position).getFileSize();
        else
            clearSize -= mFileInfos.get(position).getFileSize();
        showClearInfo();
        mFileManageAdapter.notifyDataSetChanged();
        tvSelectAll.setSelected(isSelectAll());
    }

    public boolean isSelectAll() {
        boolean selectAll = true;
        for (FileInfo temp : mFileInfos) {
            if (temp.isSelected() == false) {
                selectAll = false;
                break;
            }
        }
        return selectAll;
    }

    public void showClearInfo() {
        tvClearInfo.setText(clearSize > 0 ? "可节省" + SDCardUtils.bit2KbMGB(clearSize)
                + ",清理后可用" + SDCardUtils.bit2KbMGB(SDCardUtils.getSDCardFreeSizeBit() + clearSize) : "");
        tvClearInfo.setVisibility(clearSize > 0 ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public void onFinish() {
        new Thread(() -> {
            SystemClock.sleep(3000);
            ((Activity) mContext).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    for (FileInfo select : selectFiles) {
                        for (FileInfo temp : mFileInfos) {
                            if (select.getFileId() == temp.getFileId()) {
                                mFileInfos.remove(temp);
                                break;
                            }
                        }
                    }
                    cleared = true;
                    tvClearInfo.setVisibility(View.INVISIBLE);
                    mFileManageAdapter.notifyDataSetChanged();
                    mClearSuccessDialog.show();
                    mClearSuccessDialog.setInfo("恭喜您，节省" + SDCardUtils.bit2KbMGB(clearSize) + "空间");
                    mClearIngDialog.cancel();
                }
            });
        }).start();
    }

    @Override
    public void onFileChanged(String path) {
        if (path == null)
            return;
        final File f = new File(path);
        final Intent intent;
        if (f.isDirectory()) {
            intent = new Intent("action.intent.action_file_update_broadast");
        } else {
            intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            intent.setData(Uri.fromFile(new File(path)));

        }
        mContext.sendBroadcast(intent);
    }
}
