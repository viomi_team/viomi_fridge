package com.viomi.fridge.view.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothA2dp;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothHeadset;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.unilife.common.utils.StringUtils;
import com.viomi.fridge.R;
import com.viomi.fridge.device.AppConfig;
import com.viomi.fridge.device.DeviceConfig;
import com.viomi.fridge.manager.BleManager;
import com.viomi.fridge.model.bean.BleDevice;
import com.viomi.fridge.util.BleUtils;
import com.viomi.fridge.util.LogUtils;
import com.viomi.fridge.view.adapter.BleAdapter;
import com.viomi.fridge.view.widget.EditNameDialog;
import com.viomi.fridge.view.widget.UnScrollListView;
import com.wx.wheelview.util.WheelUtils;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by Ljh on 2017/10/20.
 */

public class BluetoothSetFragment extends BaseFragment {
    //UI
    private ImageView imgSwitch;
    private TextView tvDeName;
    private TextView tvBleIntro;
    private LinearLayout llDevPaired;
    private UnScrollListView lvDevPaird;
    private LinearLayout llDevScan;
    private UnScrollListView lvDevScan;
    private TextView tvRefresh;
    private EditNameDialog mEditNameDialog;
    //
    private BluetoothAdapter mBluetoothAdapter;
    BleAdapter mPairAdapter, mScanAdapter;
    List<BleDevice> mPairDevices = new ArrayList<>();
    List<BleDevice> mScanDevices = new ArrayList<>();
    private boolean supportBlePhone = true;//是否支持蓝牙电话
    /**
     * 蓝牙音频传输协议
     */
    private BluetoothA2dp a2dp;
    private BluetoothHeadset headset;
    /**
     * 需要连接的蓝牙设备
     */
    private BluetoothDevice curBlooth;//当前连接的蓝牙耳机、音箱
    private BluetoothDevice will2Blooth;
    private String curPhoneAddr = "";//当前连接的
    private String will2Phone = "";//将要连接的手机
    //
    private static final String FIND_PHONE = "com.viomi.fridge.curPhone";//查询当前连接的手机
    private static final String FIND_PHONE_RESULT = "com.viomi.fridge.curPhoneResult";//查询结果

    private static final String CONNECT_TO_PHONE = "com.viomi.fridge.connectTo";//与手机连接或断开连接
    //
    private static final int REQ_ENABLE_BT = 1000;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View mainView = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_bluetooth_set, null, false);
        //
        imgSwitch = (ImageView) mainView.findViewById(R.id.imgSwitch);
        tvDeName = (TextView) mainView.findViewById(R.id.tvDeName);
        tvBleIntro = (TextView) mainView.findViewById(R.id.tvBleIntro);
        llDevPaired = (LinearLayout) mainView.findViewById(R.id.llDevPaired);
        lvDevPaird = (UnScrollListView) mainView.findViewById(R.id.lvDevPaird);
        llDevScan = (LinearLayout) mainView.findViewById(R.id.llDevScan);
        lvDevScan = (UnScrollListView) mainView.findViewById(R.id.lvDevScan);
        tvRefresh = (TextView) mainView.findViewById(R.id.tvRefresh);
        mEditNameDialog = new EditNameDialog(mContext, new EditNameDialog.EditNameDialogCallBack() {
            @Override
            public void btnRightClick(String info) {
                if (mBluetoothAdapter.isDiscovering()) {
                    mBluetoothAdapter.cancelDiscovery();
                }
                if (mBluetoothAdapter.setName(info))
                    tvDeName.setText(info);
            }
        });
        //
        GradientDrawable drawable = (GradientDrawable) tvRefresh.getBackground();
        int px = WheelUtils.dip2px(mContext, 100);
        drawable.setCornerRadii(new float[]{px, px, px, px, px, px, px, px});
        tvRefresh.setBackground(drawable);
        initListener();
        init();
        //
        return mainView;
    }

    public void findCurPhone() {//查找连接的手机
        Intent intent = new Intent(FIND_PHONE);
        mContext.sendBroadcast(intent);
        LogUtils.w(TAG, "sendBroadcast " + intent.getAction());
    }

    public void switchConnectPhone(String bleAddr, boolean connected) {//改变对端手机连接状态,true为进行连接，false为断开
        Intent intent = new Intent(CONNECT_TO_PHONE);
        intent.putExtra("bleAddr", bleAddr);
        intent.putExtra("status", connected ? "1" : "0");
        LogUtils.w(TAG, "sendBroadcast " + intent.getAction() + " the bleAddr:" + bleAddr + " switch to:" + (connected ? "1" : "0"));
        mContext.sendBroadcast(intent);
    }

    private void initListener() {
        imgSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.isSelected()) {
                    new AlertDialog.Builder(mContext).setTitle("要关闭蓝牙吗？")
                            .setMessage("此操作将会断开所有蓝牙连接")
                            .setNegativeButton("取消", null)
                            .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    initShow(false);
                                    mBluetoothAdapter.disable();
                                    curPhoneAddr = will2Phone = "";
                                    curBlooth = will2Blooth = null;
                                }
                            }).show();
                } else {
                    Intent mIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(mIntent, REQ_ENABLE_BT);

                    Intent discoverable = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);//请求搜被索
                    discoverable.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION,120);//可被搜索120s
                    startActivity(discoverable);//开始
                }
            }
        });
        tvDeName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {//
                mEditNameDialog.show(mBluetoothAdapter.getName());
            }
        });
        tvRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvRefresh.setText("扫描中…");
                mScanDevices.clear();
                mScanAdapter.notifyDataSetChanged();
                if (mBluetoothAdapter.isDiscovering()) {
                    mBluetoothAdapter.cancelDiscovery();
                }
                //开始搜索蓝牙设备,搜索到的蓝牙设备通过广播返回
                mBluetoothAdapter.startDiscovery();
            }
        });
        lvDevPaird.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                if (mBluetoothAdapter.isDiscovering()) {
                    mBluetoothAdapter.cancelDiscovery();//
                }
                if (mPairDevices.get(position).isConnected()) {//切换为断开
                    new AlertDialog.Builder(mContext).setTitle("要断开连接吗？")
                            .setMessage("此操作将会断开蓝牙设备的连接")
                            .setNegativeButton("取消", null)
                            .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (mPairDevices.get(position).getBluetoothDevice().getBluetoothClass().getMajorDeviceClass() == BluetoothClass.Device.Major.PHONE) {
                                        switchConnectPhone(mPairDevices.get(position).getBluetoothDevice().getAddress(), false);
                                    } else {
                                        try {
                                            if (a2dp != null)
                                                a2dp.getClass()
                                                        .getMethod("disconnect", BluetoothDevice.class)
                                                        .invoke(a2dp, mPairDevices.get(position).getBluetoothDevice());
                                            if (headset != null)
                                                headset.getClass()
                                                        .getMethod("disconnect", BluetoothDevice.class)
                                                        .invoke(headset, mPairDevices.get(position).getBluetoothDevice());
                                        } catch (IllegalAccessException e) {
                                            e.printStackTrace();
                                        } catch (InvocationTargetException e) {
                                            e.printStackTrace();
                                        } catch (NoSuchMethodException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }).show();
                } else {//进行连接
                    closeProfile();
                    //
                    if (mPairDevices.get(position).getBluetoothDevice().getBluetoothClass().getMajorDeviceClass() == BluetoothClass.Device.Major.PHONE) {
                        showLoading();
                        will2Phone = mPairDevices.get(position).getBluetoothDevice().getAddress();
                        if (curPhoneAddr.length() > 0)
                            switchConnectPhone(curPhoneAddr, false);
                        else
                            switchConnectPhone(will2Phone, true);
                    } else {//连接耳机
                        showLoading();
                        will2Blooth = mPairDevices.get(position).getBluetoothDevice();
                        if (curPhoneAddr.length() > 0)//断开HFP的手机连接
                            switchConnectPhone(curPhoneAddr, false);
                        contectBlueDevices();
                    }
                }
            }
        });
        lvDevPaird.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {//取消连接与绑定
                if (mBluetoothAdapter.isDiscovering()) {
                    mBluetoothAdapter.cancelDiscovery();
                }

                new AlertDialog.Builder(mContext).setTitle("要删除该设备吗？")
                        .setMessage("此操作将会取消与该设备的配对")
                        .setNegativeButton("取消", null)
                        .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                BleUtils.unpairDevice(mPairDevices.get(position).getBluetoothDevice());//取消配对
                            }
                        }).show();
                return true;
            }
        });
        lvDevScan.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {//配对并连接
                if (mBluetoothAdapter.isDiscovering()) {
                    mBluetoothAdapter.cancelDiscovery();
                }
                showLoading();
                new BleUtils(mScanDevices.get(position).getBluetoothDevice()).doPair();
            }
        });
    }

    private void init() {
        if (DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V3) ||DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V31)
                || DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V4)) {//支持蓝牙电话
            supportBlePhone = true;
            tvBleIntro.setText("开启蓝牙可用于接听电话或连接音箱播放音乐\n\n使用冰箱接听电话：请保持您的手提电话蓝牙功能处于开启状态，用蓝牙连接云米冰箱，即可用冰箱接听电话\n\n连接音箱播放音乐：请保持您的音箱设备蓝牙功能处于开启状态，用冰箱蓝牙功能连接您的音箱设备，即可用音箱播放冰箱音乐");
        } else {
            supportBlePhone = false;
            tvBleIntro.setText("开启蓝牙可用于连接音箱播放音乐\n\n连接音箱播放音乐：请保持您的音箱设备蓝牙功能处于开启状态，用冰箱蓝牙功能连接您的音箱设备，即可用音箱播放冰箱音乐");
        }
        //
        mBluetoothAdapter = BleManager.getInstance().getBleAdapter();
        tvDeName.setText(mBluetoothAdapter.getName());
        //
        mPairAdapter = new BleAdapter(mContext, mPairDevices);
        lvDevPaird.setAdapter(mPairAdapter);
        mScanAdapter = new BleAdapter(mContext, mScanDevices);
        lvDevScan.setAdapter(mScanAdapter);
        initBle();
    }

    public void initBle() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        intentFilter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        intentFilter.addAction(BluetoothA2dp.ACTION_CONNECTION_STATE_CHANGED);
        intentFilter.addAction(BluetoothA2dp.ACTION_PLAYING_STATE_CHANGED);
        intentFilter.addAction(BluetoothDevice.ACTION_FOUND);
        intentFilter.addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED);

        intentFilter.addAction(BluetoothHeadset.ACTION_CONNECTION_STATE_CHANGED);

        intentFilter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        intentFilter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
        intentFilter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED);
        if (supportBlePhone) {
            intentFilter.addAction("android.bluetooth.headsetclient.profile.action.CONNECTION_STATE_CHANGED");
            intentFilter.addAction(FIND_PHONE_RESULT);
        }
        mContext.registerReceiver(this.mReceiver, intentFilter);
        //
        initShow(mBluetoothAdapter.isEnabled());
        if (mBluetoothAdapter.isEnabled()) {
            if (supportBlePhone)
                findCurPhone();
            else
                refreshBoundedDev();//
            contectBlueDevices();//开启协议通道
            tvRefresh.performClick();
        }
    }

    public void initShow(boolean enable) {
        imgSwitch.setSelected(enable);
        tvDeName.setClickable(enable);
        llDevPaired.setVisibility(enable ? View.VISIBLE : View.GONE);
        llDevScan.setVisibility(enable ? View.VISIBLE : View.GONE);
    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context paramAnonymousContext, Intent intent) {
            String action = intent.getAction();
            if (FIND_PHONE_RESULT.equals(action)) {//查询结果
                LogUtils.w(TAG, "the action is:" + action);
                if (!StringUtils.isEmpty(intent.getStringExtra("bleAddr"))) {
                    curPhoneAddr = intent.getStringExtra("bleAddr");
                    LogUtils.w(TAG, "the action is:" + action + " the bleAddr is:" + curPhoneAddr);
                    refreshBoundedDev();
                    hideLoading();
                }
            } else {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                if (device != null)
                    LogUtils.w(TAG, "the action is:" + action + " the bleAddr is:" + device.getAddress());
                //
                if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                    LogUtils.w(TAG, action + device.getName() + " the boundstatus:" + device.getBondState() + " the type is:" + device.getBluetoothClass()
                            .getMajorDeviceClass());
                    if (device.getBondState() == BluetoothDevice.BOND_NONE
                            && ((device.getBluetoothClass().getMajorDeviceClass() == BluetoothClass.Device.Major.PHONE && supportBlePhone)
                            || device.getBluetoothClass().getMajorDeviceClass() == BluetoothClass.Device.Major.AUDIO_VIDEO)) {
                        //是手机或音箱类设备
                        mScanDevices.add(new BleDevice(device));
                        mScanAdapter.notifyDataSetChanged();
                    }
                } else if (BluetoothDevice.ACTION_BOND_STATE_CHANGED.equals(action)) {
                    switch (device.getBondState()) {
                        case BluetoothDevice.BOND_BONDING:
                            LogUtils.w(TAG, "正在配对......");
                            break;
                        case BluetoothDevice.BOND_BONDED:
                            LogUtils.w(TAG, "完成配对");
                            refreshBoundedDev();
                            if (device.getBluetoothClass().getMajorDeviceClass() == BluetoothClass.Device.Major.PHONE) {
                                if (supportBlePhone) {
                                    closeProfile();
                                    will2Phone = device.getAddress();
                                    if (curPhoneAddr.length() > 0)
                                        switchConnectPhone(curPhoneAddr, false);
                                    else
                                        switchConnectPhone(will2Phone, true);
                                }
                            } else {
                                if (curPhoneAddr.length() > 0)
                                    switchConnectPhone(curPhoneAddr, false);
                                will2Blooth = device;
                                contectBlueDevices();
                            }
                            for (BleDevice dev : mScanDevices) {
                                if (dev.getBluetoothDevice().getAddress().equals(device.getAddress())) {
                                    mScanDevices.remove(dev);
                                    break;
                                }
                            }
                            mScanAdapter.notifyDataSetChanged();
                            //hideLoading();
                            break;
                        case BluetoothDevice.BOND_NONE:
                            LogUtils.w(TAG, "取消配对");
                            refreshBoundedDev();
                            hideLoading();
                            break;
                        default:
                            break;
                    }
                } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                    tvRefresh.setText("刷新");
                    //hideLoading();
                } else if (BluetoothA2dp.ACTION_CONNECTION_STATE_CHANGED.equals(action)) {
                    switch (intent.getIntExtra(BluetoothA2dp.EXTRA_STATE, -1)) {
                        case BluetoothA2dp.STATE_CONNECTING:
                            showLoading();
                            LogUtils.w(TAG, "device: " + device.getName() + " BluetoothA2dp connecting");
                            break;
                        case BluetoothA2dp.STATE_CONNECTED:
                            LogUtils.w(TAG, "device: " + device.getName() + " BluetoothA2dp connected");
                            curBlooth = device;
                            will2Blooth = null;
                            refreshBoundedDev();
                            //连接成功，开始播放
                            hideLoading();
                            break;
                        case BluetoothA2dp.STATE_DISCONNECTING:
                            LogUtils.w(TAG, "device: " + device.getName() + " BluetoothA2dp disconnecting");
                            break;
                        case BluetoothA2dp.STATE_DISCONNECTED://A2DP断开连接
                            LogUtils.w(TAG, "device: " + device.getName() + " BluetoothA2dp disconnected");
                            if (curBlooth != null && will2Blooth == null && curBlooth.getAddress().equals(device.getAddress())) {//已连接的设备切换为断开
                                curBlooth = null;
                                refreshBoundedDev();
                                hideLoading();
                            } else if (curBlooth != null && (will2Phone.length() > 0 || will2Blooth != null) && curBlooth.getAddress().equals(device.getAddress())) {//连接其他设备
                                curBlooth = null;
                            } else if (will2Blooth != null && will2Blooth.getAddress().equals(device.getAddress())) {//连接失败
                                will2Blooth = null;
                                refreshBoundedDev();
                                hideLoading();
                            } else
                                hideLoading();
                            break;
                        default:
                            break;
                    }
                } else if (BluetoothA2dp.ACTION_PLAYING_STATE_CHANGED.equals(action)) {//播放状态变化
                    //int state = intent.getIntExtra(BluetoothA2dp.EXTRA_STATE, -1);
                } else if (BluetoothDevice.ACTION_ACL_CONNECTED.equals(action)) {//蓝牙已连接
                    LogUtils.w(TAG, device.getName() + "   ACTION_ACL_CONNECTED");
                } else if (BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED.equals(action)) {//正在断开蓝牙连接
                    LogUtils.w(TAG, device.getName() + "   ACTION_ACL_DISCONNECT_REQUESTED");
                } else if (BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(action)) {//蓝牙连接已断开
                    LogUtils.w(TAG, device.getName() + "   ACTION_ACL_DISCONNECTED");
                } else if (BluetoothHeadset.ACTION_CONNECTION_STATE_CHANGED.equals(action)) {
                    switch (intent.getIntExtra(BluetoothHeadset.EXTRA_STATE, -1)) {
                        case BluetoothHeadset.STATE_CONNECTING:
                            LogUtils.w(TAG, "device: " + device.getName() + "    BluetoothHeadset connecting");
                            break;
                        case BluetoothHeadset.STATE_CONNECTED:
                            LogUtils.w(TAG, "device: " + device.getName() + "    BluetoothHeadset connected");
                            break;
                        case BluetoothHeadset.STATE_DISCONNECTING:
                            LogUtils.w(TAG, "device: " + device.getName() + "    BluetoothHeadset disconnecting");
                            break;
                        case BluetoothHeadset.STATE_DISCONNECTED:
                            if (curBlooth == null && will2Blooth != null) {//无连接的情况下进行连接，失败
                                will2Blooth = null;
                                refreshBoundedDev();
                                hideLoading();
                            }
                            LogUtils.w(TAG, "device: " + device.getName() + "    BluetoothHeadset disconnected");
                            break;
                        default:
                            break;
                    }
                } else if ("android.bluetooth.headsetclient.profile.action.CONNECTION_STATE_CHANGED".equals(action)) {
                    LogUtils.w(TAG, "BluetoothHeadsetClient CONNECTION_STATE_CHANGED");
                    switch (intent.getIntExtra(BluetoothProfile.EXTRA_STATE, -1)) {
                        case BluetoothProfile.STATE_CONNECTING:
                            showLoading();
                            LogUtils.w(TAG, "device: " + device.getName() + "    BluetoothHeadsetClient connecting");
                            break;
                        case BluetoothProfile.STATE_CONNECTED:
                            LogUtils.w(TAG, "device: " + device.getName() + "    BluetoothHeadsetClient connected");
                            curPhoneAddr = device.getAddress();
                            will2Phone = "";
                            refreshBoundedDev();
                            hideLoading();
                            break;
                        case BluetoothProfile.STATE_DISCONNECTING:
                            LogUtils.w(TAG, "device: " + device.getName() + "    BluetoothHeadsetClient disconnecting");
                            break;
                        case BluetoothProfile.STATE_DISCONNECTED:
                            LogUtils.w(TAG, "device: " + device.getName() + "    BluetoothHeadsetClient disconnected");
                            if (curPhoneAddr.length() > 0 && will2Phone.length() == 0 && curPhoneAddr.equals(device.getAddress())) {//已连接的设备切换为断开
                                curPhoneAddr = "";
                                //refreshBoundedDev();
                                hideLoading();
                            } else if (curPhoneAddr.length() > 0 && (will2Phone.length() > 0 || will2Blooth != null) && curPhoneAddr.equals(device.getAddress
                                    ())) {//连接其他设备
                                curPhoneAddr = "";
                                if (will2Phone.length() > 0)
                                    switchConnectPhone(will2Phone, true);
                            } else if (will2Phone.length() > 0 && will2Phone.equals(device.getAddress())) {//连接失败
                                will2Phone = "";
                                //refreshBoundedDev();
                                hideLoading();
                            } else
                                hideLoading();
                            refreshBoundedDev();
                            break;
                        default:
                            break;
                    }
                }
            }
        }
    };

    public void refreshBoundedDev() {
        if (mBluetoothAdapter.isEnabled()) {
            mPairDevices.clear();
            Set<BluetoothDevice> temDevices = mBluetoothAdapter.getBondedDevices();
            for (BluetoothDevice device : temDevices) {
                BleDevice temp = new BleDevice(device);
                if (device.getBluetoothClass().getMajorDeviceClass() == BluetoothClass.Device.Major.AUDIO_VIDEO) {
                    if (a2dp != null) {
                        if (a2dp.getConnectionState(temp.getBluetoothDevice()) == BluetoothProfile.STATE_CONNECTED) {
                            if (curBlooth == null)
                                curBlooth = temp.getBluetoothDevice();
                            temp.setConnected(true);
                        } else
                            temp.setConnected(false);
                    }
                    mPairDevices.add(temp);
                } else if (supportBlePhone && device.getBluetoothClass().getMajorDeviceClass() == BluetoothClass.Device.Major.PHONE) {
                    if (curPhoneAddr.equals(temp.getBluetoothDevice().getAddress()))
                        temp.setConnected(true);
                    mPairDevices.add(temp);
                }
            }
            mPairAdapter.notifyDataSetChanged();
        }
    }

    private void closeProfile() {
        try {
            //curBlooth = null;//
            if (a2dp != null)
                a2dp.getClass()
                        .getMethod("disconnect", BluetoothDevice.class)
                        .invoke(a2dp, curBlooth);
            if (headset != null)
                headset.getClass()
                        .getMethod("disconnect", BluetoothDevice.class)
                        .invoke(headset, curBlooth);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        if (a2dp != null)
            mBluetoothAdapter.closeProfileProxy(BluetoothProfile.A2DP, a2dp);
        if (headset != null)
            mBluetoothAdapter.closeProfileProxy(BluetoothProfile.HEADSET, headset);
    }

    /**
     * 连接蓝牙
     */
    private void contectBlueDevices() {
        closeProfile();
        /**使用A2DP协议连接设备*/
        mBluetoothAdapter.getProfileProxy(mContext, mProfileServiceListener, BluetoothProfile.A2DP);//音箱连接
        mBluetoothAdapter.getProfileProxy(mContext, mProfileServiceListener, BluetoothProfile.HEADSET);
    }

    /**
     * 连接蓝牙设备
     */
    private BluetoothProfile.ServiceListener mProfileServiceListener = new BluetoothProfile.ServiceListener() {
        @Override
        public void onServiceDisconnected(int profile) {
            if (profile == BluetoothProfile.HEADSET) {
                LogUtils.w(TAG, "HEADSET onServiceDisconnected");
                headset = null;
            } else if (profile == BluetoothProfile.A2DP) {
                LogUtils.w(TAG, "A2DP onServiceDisconnected");
                a2dp = null;
            }
        }

        @Override
        public void onServiceConnected(int profile, BluetoothProfile proxy) {
            try {
                if (profile == BluetoothProfile.HEADSET) {
                    LogUtils.w(TAG, "headset onServiceConnected");
                    headset = (BluetoothHeadset) proxy;
                    if (will2Blooth != null && headset.getConnectionState(will2Blooth) == BluetoothProfile.STATE_DISCONNECTED) {
                        headset.getClass()
                                .getMethod("connect", BluetoothDevice.class)
                                .invoke(headset, will2Blooth);
                    }
                } else if (profile == BluetoothProfile.A2DP) {
                    LogUtils.w(TAG, "A2DP onServiceConnected");
                    /**使用A2DP的协议连接蓝牙设备*/
                    a2dp = (BluetoothA2dp) proxy;
                    refreshBoundedDev();
                    if (will2Blooth != null && a2dp.getConnectionState(will2Blooth) == BluetoothProfile.STATE_DISCONNECTED) {
                        a2dp.getClass()
                                .getMethod("connect", BluetoothDevice.class)
                                .invoke(a2dp, will2Blooth);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_ENABLE_BT && resultCode == Activity.RESULT_OK) {//开启蓝牙
            curPhoneAddr = will2Phone = "";
            curBlooth = will2Blooth = null;
            initShow(true);
            contectBlueDevices();
            refreshBoundedDev();//TODO
            if (supportBlePhone)
                findCurPhone();
            tvRefresh.performClick();
        } else {
            imgSwitch.setSelected(false);
        }
    }

    @Override
    public void onDestroyView() {
        mContext.unregisterReceiver(mReceiver);
        if (a2dp != null) {
            mBluetoothAdapter.closeProfileProxy(BluetoothProfile.A2DP, a2dp);
            a2dp = null;
        }
        if (headset != null) {
            mBluetoothAdapter.closeProfileProxy(BluetoothProfile.HEADSET, headset);
            headset = null;
        }
        if (mProfileServiceListener != null) {
            mProfileServiceListener = null;
        }
        super.onDestroyView();
    }
}