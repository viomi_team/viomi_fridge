package com.viomi.fridge.view.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.viomi.fridge.manager.StatsManager;
import com.viomi.fridge.view.widget.Loading;

/**
 * Created by young2 on 2017/1/3.
 */

public class BaseFragment extends Fragment {
    public final String TAG = this.getClass().getSimpleName();
    private Loading loading;
    protected Context mContext;

    @Override
    public void onResume() {
        super.onResume();
        StatsManager.recordFragmentStart(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        StatsManager.recordFragmentPageEnd(this);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = getActivity();
    }

    public void initLoading() {
        if (loading == null) {
            loading = new Loading(mContext);
        }
    }

    public void showLoading() {
        if (getActivity().isFinishing())
            return;
        initLoading();
        loading.setCancelable(true);
        if (!loading.isShowing()) {
            loading.show();
        }
    }

    public void showUnCancelLoading() {
        if (getActivity().isFinishing())
            return;
        initLoading();
        loading.setCancelable(false);
        if (!loading.isShowing()) {
            loading.show();
        }
    }

    public void hideLoading() {
        if (loading != null && loading.isShowing())
            loading.cancel();
    }

    public boolean getLoadingStatus() {
        initLoading();
        return loading.isShowing();
    }

    public void openActivity(Class<?> clazz) {
        openActivity(clazz, null);
    }

    public void openActivity(Class<?> clazz, Bundle bundle) {
        Intent intent = new Intent(getActivity(), clazz);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
    }

    public void openActvityForResult(Class<?> clazz, int requestCode) {
        Intent intent = new Intent(getActivity(), clazz);
        startActivityForResult(intent, requestCode);
    }

    public void openActvityForResult(Class<?> clazz, int requestCode, Bundle bundle) {
        Intent intent = new Intent(getActivity(), clazz);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivityForResult(intent, requestCode);
    }
}
