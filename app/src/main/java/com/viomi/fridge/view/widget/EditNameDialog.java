package com.viomi.fridge.view.widget;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.viomi.fridge.R;
import com.viomi.fridge.util.ToastUtil;

/**
 * Created by Ljh on 2017/11/30
 */

public class EditNameDialog extends BaseDialog implements View.OnClickListener {
    private Context mContext;
    private TextView tvTitle, tvBtnLeft, tvBtnRight;
    private EditText edInfo;
    EditNameDialogCallBack mEditNameDialogCallBack;

    public EditNameDialog(Context context) {
        super(context, R.style.DialogGrey);
        this.mContext = context;
    }

    public EditNameDialog(Context context, EditNameDialogCallBack callBack) {
        super(context, R.style.DialogGrey);
        this.mContext = context;
        //
        this.mEditNameDialogCallBack = callBack;
    }

    public void setTitle(String title) {
        tvTitle.setText(title);
    }

    @Override
    public void setView() {
        setContentView(R.layout.dialog_edit_name);
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        tvBtnLeft = (TextView) findViewById(R.id.tvBtnLeft);
        tvBtnRight = (TextView) findViewById(R.id.tvBtnRight);
        tvBtnLeft.setOnClickListener(this);
        tvBtnRight.setOnClickListener(this);
        edInfo = (EditText) findViewById(R.id.edInfo);
        this.setCancelable(false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvBtnLeft:
                this.cancel();
                break;
            case R.id.tvBtnRight:
                if (TextUtils.isEmpty(edInfo.getText()))
                    ToastUtil.show("请输入蓝牙名称");
                else {
                    if (mEditNameDialogCallBack != null)
                        mEditNameDialogCallBack.btnRightClick(edInfo.getText().toString().trim());
                    this.cancel();
                }
                break;
        }
    }

    public void show(String info){
        show();
        edInfo.setText(info);
        GradientDrawable drawable = (GradientDrawable) edInfo.getBackground();
        drawable.setColor(Color.parseColor("#f0f0f0"));
        edInfo.setBackground(drawable);
    }

    public interface EditNameDialogCallBack {
        //void btnLeftClick();
        void btnRightClick(String info);
    }
}