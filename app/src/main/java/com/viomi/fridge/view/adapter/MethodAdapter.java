package com.viomi.fridge.view.adapter;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.viomi.fridge.R;
import com.viomi.fridge.model.bean.recipe.Step;
import com.viomi.fridge.util.ImgUtil;
import com.viomi.fridge.view.widget.RoundImageView;


public class MethodAdapter extends ABaseAdapter<Step> {

    public MethodAdapter(Context context) {
        super(context);
    }

    @Override
    protected View setConvertView(int position, Step entity, View convertView) {

        ViewHolder holder = null;
        if (convertView == null) {
            convertView = View.inflate(getContext(), R.layout.item_method_view, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (entity != null) {
//            holder.index.setText("" +( position+1));
            holder.method.setText(entity.getStep());
            ImgUtil.showDefinedImage(entity.getImg(),holder.imageView,R.drawable.zf_default_message_image);
        }
        return convertView;
    }

    private class ViewHolder {
        ImageView imageView;
        TextView index;
        TextView method;

        public ViewHolder(View view) {
            this.imageView = (ImageView) view.findViewById(R.id.iv_img);
            this.method = (TextView) view.findViewById(R.id.tv_method);
//            this.index = (TextView) view.findViewById(R.id.tv_index);
        }
    }
}
