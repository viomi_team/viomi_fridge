package com.viomi.fridge.view.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.viomi.fridge.R;
import com.viomi.fridge.device.DeviceImageManager;
import com.viomi.fridge.model.bean.DeviceInfo;

import java.util.List;

/**
 * 智能互联列表适配器
 */

public class DeviceListAdapter extends BaseAdapter {
    private Context mContext;
    private List<DeviceInfo> mDevices;

    public DeviceListAdapter(Context context, List<DeviceInfo> devices) {
        this.mContext = context;
        this.mDevices = devices;
    }

    @Override
    public int getCount() {
        return mDevices.size();
    }

    @Override
    public Object getItem(int i) {
        return mDevices.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        SmartItemHolder holder;
        if (view == null) {
            view = View.inflate(mContext, R.layout.item_device, null);
            holder = new SmartItemHolder();
            holder.logoView = (ImageView) view.findViewById(R.id.id_logo);
            holder.nameView = (TextView) view.findViewById(R.id.id_name);
            holder.stateView = (TextView) view.findViewById(R.id.id_state);
            view.setTag(holder);
        } else holder = (SmartItemHolder) view.getTag();
        // 设备类型分类
        DeviceInfo info = mDevices.get(i);
        if (info.getAbstractDevice() != null) { // 小米设备
            holder.nameView.setText(info.getAbstractDevice().getName());// 设备名称
            if (info.getAbstractDevice().getDevice().isOnline()) {   // 设备在线
                holder.stateView.setText(info.getAbstractDevice().getDeviceId());
                holder.nameView.setTextColor(ContextCompat.getColor(mContext, R.color.main_color));
            } else {
                String str = info.getAbstractDevice().getDeviceId() + " " + mContext.getResources().getString(R.string.device_offline);
                holder.stateView.setText(str);
                holder.nameView.setTextColor(ContextCompat.getColor(mContext, R.color.item_desc_color));
            }
            holder.logoView.setImageResource(DeviceImageManager.getIconByModel(info.getAbstractDevice().getDevice().getDeviceModel()));
        } else if (info.getCameraInfo() != null) {  // 绿联 IPC 设备
            holder.nameView.setText(info.getCameraInfo().getName());// 设备名称
            if (info.getCameraInfo().isOnline()) {  // 设备在线
                holder.stateView.setText(info.getCameraInfo().getSrcId());
                holder.nameView.setTextColor(ContextCompat.getColor(mContext, R.color.main_color));
            } else {
                String str = info.getCameraInfo().getSrcId() + " " + mContext.getResources().getString(R.string.device_offline);
                holder.stateView.setText(str);
                holder.nameView.setTextColor(ContextCompat.getColor(mContext, R.color.item_desc_color));
            }
            holder.logoView.setImageResource(R.drawable.icon_device_list_camera);
        }
        return view;
    }

    private class SmartItemHolder {
        private ImageView logoView;
        private TextView nameView;
        private TextView stateView;
    }
}
