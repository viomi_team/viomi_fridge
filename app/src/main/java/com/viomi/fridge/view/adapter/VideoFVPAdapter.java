package com.viomi.fridge.view.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

import com.unilife.common.content.beans.iqiyi.IqiyiCategoryInfo;
import com.viomi.fridge.view.fragment.VideoFragment;

import java.util.List;

/**
 * Created by Mocc on 2017/7/26
 */

public class VideoFVPAdapter extends FragmentPagerAdapter {

    private List<VideoFragment> flist;
    private List<IqiyiCategoryInfo> infos;

    public VideoFVPAdapter(FragmentManager fm, List<VideoFragment> flist, List<IqiyiCategoryInfo> infos) {
        super(fm);
        this.flist = flist;
        this.infos = infos;
    }

    @Override
    public Fragment getItem(int position) {
        return flist.get(position);
    }


    @Override
    public int getCount() {
        return flist.size();
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
//        super.destroyItem(container, position, object);
//        log.myE("VideoFVPAdapter","destroyItem="+position);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return infos.get(position).getCategoryName();
    }
}
