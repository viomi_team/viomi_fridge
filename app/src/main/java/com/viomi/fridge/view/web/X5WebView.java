package com.viomi.fridge.view.web;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;

import com.tencent.smtt.export.external.interfaces.GeolocationPermissionsCallback;
import com.tencent.smtt.export.external.interfaces.JsResult;
import com.tencent.smtt.export.external.interfaces.SslError;
import com.tencent.smtt.export.external.interfaces.SslErrorHandler;
import com.tencent.smtt.export.external.interfaces.WebResourceRequest;
import com.tencent.smtt.export.external.interfaces.WebResourceResponse;
import com.tencent.smtt.sdk.QbSdk;
import com.tencent.smtt.sdk.WebChromeClient;
import com.tencent.smtt.sdk.WebSettings;
import com.tencent.smtt.sdk.WebSettings.LayoutAlgorithm;
import com.tencent.smtt.sdk.WebView;
import com.tencent.smtt.sdk.WebViewClient;
import com.viomi.fridge.R;
import com.viomi.fridge.common.ViomiApplication;
import com.viomi.fridge.preference.ManagePreference;
import com.viomi.fridge.util.LogUtils;
import com.viomi.fridge.view.widget.BaseAlertDialog;

/**
 * 腾讯浏览服务 WebView
 * Created by William on 2018/7/30.
 */
public class X5WebView extends WebView {
    private static final String TAG = X5WebView.class.getSimpleName();
    private OnWebViewListener onWebViewListener;

    @SuppressLint("SetJavaScriptEnabled")
    public X5WebView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView webView, String url) {
                LogUtils.d(TAG, "shouldOverrideUrlLoading");
                return false;
            }

            @Override
            public WebResourceResponse shouldInterceptRequest(WebView webView, WebResourceRequest webResourceRequest) {
                LogUtils.d(TAG, "shouldInterceptRequest");
                return super.shouldInterceptRequest(webView, webResourceRequest);
            }

            @Override
            public void onPageStarted(WebView webView, String url, Bitmap bitmap) {
                super.onPageStarted(webView, url, bitmap);
                LogUtils.d(TAG, "onPageStarted = " + url);
                if (onWebViewListener != null) onWebViewListener.onPageStarted(url);
            }

            @Override
            public void onPageFinished(WebView webView, String url) {
                super.onPageFinished(webView, url);
                LogUtils.d(TAG, "onPageFinished = " + url);
                if (onWebViewListener != null) onWebViewListener.onPageFinished(url);
            }

            @Override
            public void onReceivedError(WebView webView, int i, String s, String s1) {
                super.onReceivedError(webView, i, s, s1);
                LogUtils.e(TAG, "onReceivedError，code = " + i + "message = " + s + "，" + s1);
                if (onWebViewListener != null) onWebViewListener.onReceivedError();
            }

            @Override
            public void onReceivedSslError(WebView webView, SslErrorHandler sslErrorHandler, SslError sslError) {
                super.onReceivedSslError(webView, sslErrorHandler, sslError);
                LogUtils.e(TAG, "onReceivedSslError");
            }
        });

        this.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onReceivedTitle(WebView webView, String title) {
                super.onReceivedTitle(webView, title);
                LogUtils.d(TAG, "onReceivedTitle : " + title);
                if (onWebViewListener != null) onWebViewListener.onReceivedTitle(title);
            }

            @Override
            public void onReceivedIcon(WebView webView, Bitmap bitmap) {
                super.onReceivedIcon(webView, bitmap);
                LogUtils.d(TAG, "onReceivedIcon");
            }

            @Override
            public void onProgressChanged(WebView webView, int progress) {
                super.onProgressChanged(webView, progress);
                LogUtils.d(TAG, "onProgressChanged：" + progress);
                if (onWebViewListener != null)
                    onWebViewListener.onProgressChanged(progress);
            }

            @Override
            public boolean onJsConfirm(WebView arg0, String arg1, String arg2, JsResult arg3) {
                // 处理 javascript 中的 confirm
                LogUtils.d(TAG, "onJsConfirm");
                return super.onJsConfirm(arg0, arg1, arg2, arg3);
            }

            @Override
            public boolean onJsAlert(WebView arg0, String url, String message, JsResult result) {
                // 处理 javascript 中的 alert
                LogUtils.d(TAG, "onJsAlert");
                BaseAlertDialog dialog = new BaseAlertDialog(context, message, getResources().getString(R.string.cancel), getResources().getString(R.string.confirm));
                dialog.show();
                dialog.setOnLeftClickListener(() -> {
                    dialog.dismiss();
                    result.cancel();
                });
                dialog.setOnRightClickListener(() -> {
                    dialog.dismiss();
                    result.cancel();
                });
                return true;
            }

            @Override
            public void onGeolocationPermissionsShowPrompt(String s, GeolocationPermissionsCallback geolocationPermissionsCallback) {
                // 处理定位权限请求
                geolocationPermissionsCallback.invoke(s, true, false);
                LogUtils.d(TAG, "onGeolocationPermissionsShowPrompt");
                super.onGeolocationPermissionsShowPrompt(s, geolocationPermissionsCallback);
            }
        });
        // this.setWebChromeClient(chromeClient);
        // WebStorage webStorage = WebStorage.getInstance();
        initWebViewSettings();
        this.getView().setClickable(true);
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void initWebViewSettings() {
        WebSettings webSetting = this.getSettings();
        webSetting.setJavaScriptEnabled(true);// 是否允许执行 JavaScript 脚本
        webSetting.setJavaScriptCanOpenWindowsAutomatically(true);
        webSetting.setAllowFileAccess(true);// 允许访问文件
        webSetting.setLayoutAlgorithm(LayoutAlgorithm.NARROW_COLUMNS);// 设置 WebView 底层布局算法
        webSetting.setSupportZoom(false);// 支持缩放
        webSetting.setBuiltInZoomControls(false);// 设置 WebView 是否使用其内置的变焦机制
        webSetting.setUseWideViewPort(true);// 当该属性被设置为 false 时，加载页面的宽度总是适应 WebView 控件宽度
        webSetting.setSupportMultipleWindows(false);// 是否支持多屏窗口
        webSetting.setLoadWithOverviewMode(true);// 是否使用预览模式加载界面
        webSetting.setAppCacheEnabled(true);// 设置 Application 缓存 API 是否开启
        // webSetting.setDatabaseEnabled(true);// 启用数据库
        webSetting.setDomStorageEnabled(true);// 设置是否开启 DOM 存储 API 权限
        webSetting.setGeolocationEnabled(true);// 是否开启定位功能
        webSetting.setAppCacheMaxSize(Long.MAX_VALUE);// 缓存最大值
        // webSetting.setPageCacheCapacity(IX5WebSettings.DEFAULT_CACHE_CAPACITY);
        webSetting.setPluginState(WebSettings.PluginState.ON_DEMAND);
        // webSetting.setRenderPriority(WebSettings.RenderPriority.HIGH);
        webSetting.setCacheMode(WebSettings.LOAD_NO_CACHE);
        webSetting.setAppCachePath(ViomiApplication.getContext().getDir("viomi", 0).getPath());// 缓存路径
        webSetting.setDatabasePath(ViomiApplication.getContext().getDir("viomi", 0).getPath());// 缓存路径
        webSetting.setGeolocationDatabasePath(ViomiApplication.getContext().getDir("viomi", 0).getPath());// 地理位置缓存路径
        webSetting.setLoadsImagesAutomatically(true);// 是否自动加载图片资源
//        webSettings.setAllowContentAccess(true);
//        webSettings.setSavePassword(false);
//        webSettings.setSaveFormData(false);
//        webSettings.setAllowUniversalAccessFromFileURLs(true);// 设置 WebView 运行中的脚本可以是否访问任何原始起点内容

        // this.getSettingsExtension().setPageCacheCapacity(IX5WebSettings.DEFAULT_CACHE_CAPACITY);//extension
        // settings 的设计
    }

    @Override
    protected boolean drawChild(Canvas canvas, View child, long drawingTime) {
        boolean ret = super.drawChild(canvas, child, drawingTime);
        canvas.save();
        Paint paint = new Paint();
        paint.setColor(0x7fff0000);
        paint.setTextSize(24.f);
        paint.setAntiAlias(true);
        if (ManagePreference.getInstance().getDebug()) { // 调试环境下显示内核信息
            if (getX5WebViewExtension() != null) {
                canvas.drawText(this.getContext().getPackageName() + "-pid:"
                        + android.os.Process.myPid(), 10, 50, paint);
                canvas.drawText(
                        "X5  Core:" + QbSdk.getTbsVersion(this.getContext()), 10,
                        100, paint);
            } else {
                canvas.drawText(this.getContext().getPackageName() + "-pid:"
                        + android.os.Process.myPid(), 10, 50, paint);
                canvas.drawText("Sys Core", 10, 100, paint);
            }
            canvas.drawText(Build.MANUFACTURER, 10, 150, paint);
            canvas.drawText(Build.MODEL, 10, 200, paint);
            canvas.restore();
        }
        return ret;
    }

    public X5WebView(Context arg0) {
        super(arg0);
    }

    public interface OnWebViewListener {
        void onReceivedTitle(String title);

        void onProgressChanged(int progress);

        void onPageStarted(String url);

        void onPageFinished(String url);

        void onReceivedError();
    }

    public void setOnPageFinishListener(OnWebViewListener onWebViewListener) {
        this.onWebViewListener = onWebViewListener;
    }
}
