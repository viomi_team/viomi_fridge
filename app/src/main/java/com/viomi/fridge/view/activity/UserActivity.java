package com.viomi.fridge.view.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.miot.api.MiotManager;
import com.miot.common.exception.MiotException;
import com.viomi.common.callback.AppCallback;
import com.viomi.fridge.R;
import com.viomi.fridge.api.dao.InfoManager;
import com.viomi.fridge.api.http.HttpConnect;
import com.viomi.fridge.broadcast.BroadcastAction;
import com.viomi.fridge.common.ViomiApplication;
import com.viomi.fridge.device.DeviceManager;
import com.viomi.fridge.manager.AccountManager;
import com.viomi.fridge.manager.StatsManager;
import com.viomi.fridge.model.bean.ViomiUser;
import com.viomi.fridge.model.bean.WebBaseData;
import com.viomi.fridge.util.ToastUtil;
import com.viomi.fridge.util.ZhugeIoUtil;

public class UserActivity extends BaseHandlerActivity {
    private final static String TAG = UserActivity.class.getSimpleName();
    private ImageView back_icon;
    private SimpleDraweeView user_logo;
    private TextView user_name;
    private TextView login_btn;
    private RelativeLayout my_order;
    private TextView e_mail;
    private TextView bind_phone;
    private RelativeLayout my_address;
    private RelativeLayout coupon;
    private RelativeLayout suggestion;
    private final static int MSG_WHAT_LOGOUT_FAIL = 1;

    private boolean isLogined;
    private RelativeLayout dialog_layout;
    private TextView no_thanks;
    private TextView ok_do;
    private String mVmallUrl = HttpConnect.STOREMAINURL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        back_icon = (ImageView) findViewById(R.id.back_icon);
        user_logo = (SimpleDraweeView) findViewById(R.id.user_logo);
        user_name = (TextView) findViewById(R.id.user_name);
        login_btn = (TextView) findViewById(R.id.login_btn);
        my_order = (RelativeLayout) findViewById(R.id.my_order);
        e_mail = (TextView) findViewById(R.id.e_mail);
        bind_phone = (TextView) findViewById(R.id.bind_phone);
        my_address = (RelativeLayout) findViewById(R.id.my_address);
        coupon = (RelativeLayout) findViewById(R.id.coupon);
        suggestion = (RelativeLayout) findViewById(R.id.suggestion);

        dialog_layout = (RelativeLayout) findViewById(R.id.dialog_layout);
        no_thanks = (TextView) findViewById(R.id.no_thanks);
        ok_do = (TextView) findViewById(R.id.ok_do);

        initListener();
    }

    @Override
    protected void onResume() {
        super.onResume();
        init();
    }

    private void init() {
        ViomiUser user = AccountManager.getViomiUser(this);
        if (user == null) {
            isLogined = false;
            login_btn.setText("登录");
        } else {
            login_btn.setText("退出登录");
            isLogined = true;

            showUserInfo();
        }
    }

    private void showUserInfo() {
        ViomiUser user = AccountManager.getViomiUser(this);
        if (user == null) {
            return;
        }
        user_name.setText(user.getNickname());
        Uri uri = Uri.parse(user.getHeadImg());
        user_logo.setImageURI(uri);
        e_mail.setText("-");
        bind_phone.setText(user.getMobile());
    }

    private void initListener() {
        back_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isLogined) {
                    dialog_layout.setVisibility(View.VISIBLE);
                } else {
                    Intent intent = new Intent(UserActivity.this, ScanLoginActivity.class);
                    startActivity(intent);
                }
            }
        });

        my_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                jumpH5("/index.html?page=orderlist");
            }
        });

        my_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                jumpH5("/index.html?page=addr");
            }
        });

        coupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                jumpH5("/index.html?page=coupon");
            }
        });

        suggestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        dialog_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_layout.setVisibility(View.GONE);
            }
        });

        no_thanks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_layout.setVisibility(View.GONE);
            }
        });

        ok_do.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_layout.setVisibility(View.GONE);
                login_btn.setText("登录");
                user_name.setText("未登录");
                Uri uri1 = Uri.parse("res:///" + R.drawable.dialog_write_bg);
                user_logo.setImageURI(uri1);
                isLogined = false;

//                    DeviceManager.getInstance().bind(ViomiApplication.getContext(),"0", new AppCallback<String>() {
//                        @Override
//                        public void onSuccess(String data) {
//                            Log.i(TAG,"bind success!");
//                            AccountManager.deleteViomiUser(UserActivity.this);
//                            boolean result = DeviceManager.getInstance().unbind(ViomiApplication.getContext());
//                            if(!result){
//                               if (mHandler!=null){
//                                   mHandler.sendEmptyMessage(MSG_WHAT_LOGOUT_FAIL);
//                               }
//                                return;
//                            }
//                        }
//
//                        @Override
//                        public void onFail(int errorCode, String msg) {
//                            Log.e(TAG,"bind fail!errorCode="+errorCode+",msg="+msg);
//                            if (mHandler!=null){
//                                mHandler.sendEmptyMessage(MSG_WHAT_LOGOUT_FAIL);
//                            }
//                        }
//                    });
                try {
                    MiotManager.getPeopleManager().deletePeople();
                } catch (MiotException e) {
                    e.printStackTrace();
                }
                AccountManager.deleteViomiUser(UserActivity.this);
                InfoManager.getInstance().clearUserNewsRecord();
                ZhugeIoUtil.setUserInfo(null);
                StatsManager.onProfileSignOff();
                Intent intentUserPush = new Intent(BroadcastAction.ACTION_REPORT_PUSH_USER);
                sendBroadcast(intentUserPush);
                DeviceManager.getInstance().unBindDevice(new AppCallback<String>() {
                    @Override
                    public void onSuccess(String data) {

                    }

                    @Override
                    public void onFail(int errorCode, String msg) {

                    }
                });
            }
        });

    }

    private void jumpH5(String url) {
        if (isLogined) {
            Intent intent = new Intent(UserActivity.this, VmallWebActivity.class);
            WebBaseData data = new WebBaseData();
            data.url = mVmallUrl + url;
            intent.putExtra(WebBaseData.Intent_String, data);
            startActivity(intent);
        } else {
            ToastUtil.show("请先登录！");
            Intent intent = new Intent(UserActivity.this, ScanLoginActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.in_activity_back, R.anim.out_activity_back);
    }

    @Override
    protected void processMsg(Message msg) {
        switch (msg.what) {
            case MSG_WHAT_LOGOUT_FAIL:
                Toast.makeText(UserActivity.this, getString(R.string.toast_log_out_fail), Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
