package com.viomi.fridge.view.widget;

import android.app.TimePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.miot.common.people.People;
import com.viomi.common.module.okhttp.OkHttpClientManager;
import com.viomi.common.widget.SwitchButton;
import com.viomi.fridge.R;
import com.viomi.fridge.api.http.HttpConnect;
import com.viomi.fridge.util.JsonUitls;
import com.viomi.fridge.util.log;
import com.wx.wheelview.adapter.ArrayWheelAdapter;
import com.wx.wheelview.widget.WheelView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;

/**
 * 烟机换风设置 Dialog
 * Created by William on 2017/6/6.
 */

public class HoodPMDialog extends BaseDialog implements SwitchButton.OnCheckedChangeListener, View.OnClickListener,
        SeekBar.OnSeekBarChangeListener, WheelView.OnWheelItemSelectedListener {

    private final static String TAG = HoodPMDialog.class.getSimpleName();
    private String isOn, deviceId, aqi_thd, aqi_hour, aqi_min, aqi_time;
    private People people;
    private ScrollView mScrollView;
    private SwitchButton mSwitchButton;
    private SeekBar mSeekBar;
    private ExpandLayout mExpandLayout;
    private TextView tvValue, tvOpenTime, tvWindTime;
    private int isEnable = 0, type = 0, pmValue, minute;
    private String openTime, windTime, mHour, mMinute;
    private Context context;
    private final MyHandler mHandler = new MyHandler(this);


    public HoodPMDialog(Context context, String deviceId, People people, String isOn,
                        String aqi_thd, String aqi_hour, String aqi_min, String aqi_time) {
        super(context);
        this.context = context;
        this.deviceId = deviceId;
        this.people = people;
        this.isOn = isOn;
        this.aqi_thd = aqi_thd;
        this.aqi_hour = aqi_hour;
        this.aqi_min = aqi_min;
        this.aqi_time = aqi_time;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (this.getWindow() != null) {
            WindowManager.LayoutParams lp = this.getWindow().getAttributes();
            lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.height = WindowManager.LayoutParams.MATCH_PARENT;
            lp.gravity = Gravity.CENTER;
            this.getWindow().setAttributes(lp);
        }
        setCanceledOnTouchOutside(false);

        initView();
    }

    @Override
    public void setView() {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_hood_pm);
    }

    private void initView() {
        mExpandLayout = (ExpandLayout) findViewById(R.id.pm_expand);
        mSwitchButton = (SwitchButton) findViewById(R.id.pm_wind_switch);
        mScrollView = (ScrollView) findViewById(R.id.pm_content);
        mSeekBar = (SeekBar) findViewById(R.id.pm_setting);
        WheelView wheelView = (WheelView) findViewById(R.id.pm_wind_setting);
        tvValue = (TextView) findViewById(R.id.pm_value);
        tvOpenTime = (TextView) findViewById(R.id.pm_open_time);
        tvWindTime = (TextView) findViewById(R.id.pm_wind_time);

        wheelView.setWheelData(createTime());
        wheelView.setWheelAdapter(new ArrayWheelAdapter(context));
        wheelView.setSkin(WheelView.Skin.None);  // 皮肤
        wheelView.setLoop(true); // 循环滚动
        wheelView.setWheelSize(5);
        WheelView.WheelViewStyle minStyle = new WheelView.WheelViewStyle();
        minStyle.selectedTextColor = Color.parseColor("#09A3A1");
        minStyle.textColor = Color.parseColor("#BFBFBF");
        minStyle.textSize = 20;
        minStyle.selectedTextSize = 30;
        minStyle.backgroundColor = Color.parseColor("#00ffffff");
        minStyle.holoBorderColor = Color.parseColor("#09A3A1");
        minStyle.holoBorderWidth = 1;
        wheelView.setStyle(minStyle);
        wheelView.setExtraText("分钟", Color.parseColor("#09A3A1"), 20, 60);

        if (!isOn.equals("0")) {  // 开启
            mSwitchButton.setChecked(true);
            isEnable = 1;
            mScrollView.setVisibility(View.VISIBLE);
        } else {   // 关闭
            mSwitchButton.setChecked(false);
            isEnable = 0;
            mScrollView.setVisibility(View.INVISIBLE);
        }

        if (aqi_thd.equals("error")) pmValue = 100;
        else pmValue = Integer.valueOf(aqi_thd);
        mSeekBar.setProgress(pmValue - 100);
        tvValue.setText(String.valueOf(pmValue));

        String hour = aqi_hour.equals("error") ? "00" : aqi_hour;
        String min = aqi_min.equals("error") ? "00" : aqi_min;
        openTime = hour + ":" + min;

        if (aqi_time.equals("error")) windTime = "0分钟";
        else windTime = aqi_time + "分钟";

        tvOpenTime.setText(openTime);
        tvWindTime.setText(windTime);

        mExpandLayout.initExpand(false);
        findViewById(R.id.hood_pm_close).setOnClickListener(this);
        findViewById(R.id.pm_wind_set).setOnClickListener(this);
        findViewById(R.id.pm_wind_time).setOnClickListener(this);
        tvOpenTime.setOnClickListener(this);
        wheelView.setOnWheelItemSelectedListener(this);
        mSwitchButton.setOnCheckedChangeListener(this);
        mSeekBar.setOnSeekBarChangeListener(this);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            mScrollView.setVisibility(View.VISIBLE);
            isEnable = 1;
        } else {
            mScrollView.setVisibility(View.INVISIBLE);
            isEnable = 0;
        }
        type = 1;
        setApi();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.hood_pm_close:    // 关闭
                dismiss();
                break;
            case R.id.pm_open_time: // 开启时间
                TimePickerDialog timePickerDialog = new TimePickerDialog(context,
                        (view, hourOfDay, minute) -> {
                            if (hourOfDay < 10) mHour = "0" + hourOfDay;
                            else mHour = "" + hourOfDay;
                            if (minute < 10) mMinute = "0" + minute;
                            else mMinute = "" + minute;
                            tvOpenTime.setText(mHour + ":" + mMinute);
                            setApi();
                        }, 12, 0, true);
                timePickerDialog.show();
                type = 3;
                break;
            case R.id.pm_wind_set:  // 设置
                mExpandLayout.toggleExpand();
                tvWindTime.setText("" + minute + "分钟");
                type = 4;
                setApi();
                break;
            case R.id.pm_wind_time:
                mExpandLayout.toggleExpand();
                break;
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        tvValue.setText(100 + progress + "");
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        setApi();
        type = 2;
    }

    // 设置
    private void setApi() {
        JSONObject jsonObject = new JSONObject();
        String[] time = tvOpenTime.getText().toString().split(":");
        String[] wind = tvWindTime.getText().toString().split("分钟");
        try {
            jsonObject.put("method", "set_aqi");
            jsonObject.put("did", deviceId);
            jsonObject.put("id", 123);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(isEnable);
            jsonArray.put(mSeekBar.getProgress() + 100);
            jsonArray.put(time[0]);
            jsonArray.put(time[1]);
            jsonArray.put(wind[0]);
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        HttpConnect.rpcDevice(people, deviceId, jsonObject.toString(), new OkHttpClientManager.ResultCallback<String>() {
            @Override
            public void onError(Call call, Exception e) {
                Message msg = mHandler.obtainMessage();
                msg.what = 0;
                mHandler.sendMessage(msg);
            }

            @Override
            public void onResponse(String response) {
                Message msg = mHandler.obtainMessage();
                msg.obj = response;
                msg.what = 1;
                mHandler.sendMessage(msg);
            }
        });
    }

    private List<String> createTime() {
        List<String> list = new ArrayList<>();
        list.add("5");
        list.add("10");
        list.add("15");
        list.add("30");
        list.add("45");
        list.add("60");
        list.add("120");
        return list;
    }

    // 设置失败返回上一次值
    private void setDefault() {
        if (type == 1) {    // 换风开关
            mSwitchButton.setChecked(!mSwitchButton.isChecked());
            isEnable = mSwitchButton.isChecked() ? 1 : 0;
            mScrollView.setVisibility(mScrollView.getVisibility() == View.VISIBLE ? View.INVISIBLE : View.VISIBLE);
        } else if (type == 2) {     // 设置 PM2.5 值
            mSeekBar.setProgress(pmValue - 100);
            tvValue.setText(100 + mSeekBar.getProgress() + "");
        } else if (type == 3) {     // 开启时间
            tvOpenTime.setText(openTime);
        } else if (type == 4) {     // 换风时间
            tvWindTime.setText(windTime);
        }
    }

    @Override
    public void onItemSelected(int position, Object o) {
        minute = Integer.parseInt(String.valueOf(o));
    }

    private static class MyHandler extends Handler {

        WeakReference<HoodPMDialog> weakReference;

        public MyHandler(HoodPMDialog dialog) {
            this.weakReference = new WeakReference<>(dialog);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            HoodPMDialog dialog = weakReference.get();
            switch (msg.what) {
                case 0: // 失败
                    dialog.setDefault();
                    Toast.makeText(dialog.context, dialog.context.getResources().getString(R.string.toast_http_result_error), Toast.LENGTH_SHORT).show();
                    log.myE(TAG, "request-------fail-------");
                    break;
                case 1: // 成功
                    String result = (String) msg.obj;
                    log.d(TAG, result);
                    try {
                        JSONObject json = new JSONObject(result);
                        String code = JsonUitls.getString(json, "code");
                        if ("0".equals(code)) {
                            dialog.pmValue = dialog.mSeekBar.getProgress() + 100;
                            dialog.openTime = dialog.tvOpenTime.getText().toString();
                            dialog.windTime = dialog.tvWindTime.getText().toString();
                            Toast.makeText(dialog.context, "设置成功", Toast.LENGTH_SHORT).show();
                        } else {
                            dialog.setDefault();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }

}
