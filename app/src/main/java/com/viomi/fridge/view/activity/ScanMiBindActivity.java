
package com.viomi.fridge.view.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.viomi.fridge.R;
import com.viomi.fridge.broadcast.BroadcastAction;
import com.viomi.fridge.common.ViomiApplication;
import com.viomi.fridge.device.DeviceManager;
import com.viomi.fridge.util.ToastUtil;
import com.xiaomi.miot.host.manager.MiotHostManager;
import com.xiaomi.miot.typedef.error.MiotError;
import com.xiaomi.miot.typedef.listener.BindKeyListener;

import java.util.HashMap;
import java.util.Map;

public class ScanMiBindActivity extends BaseHandlerActivity{
    private final static String TAG = ScanMiBindActivity.class.getSimpleName();
    private SimpleDraweeView qrcode_img;
    private RelativeLayout loading_layout;

    private final static int MSG_WHAT_BIND_FAIL=100;
    private final static int MSG_WHAT_BIND_ALREADY=101;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_mi_bind);
        init();
        register();
    }


    private void init(){
        loading_layout = (RelativeLayout) findViewById(R.id.loading_layout);
        ImageView back_icon = (ImageView) findViewById(R.id.back_icon);
        back_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        qrcode_img = (SimpleDraweeView) findViewById(R.id.qrcode_img);
        requestQRImg();

        qrcode_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestQRImg();
            }
        });
    }


    private void register() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BroadcastAction.ACTION_DEVICE_BIND);
        intentFilter.addAction(BroadcastAction.ACTION_DEVICE_UNBIND);
        LocalBroadcastManager.getInstance(ViomiApplication.getContext()).registerReceiver(mBraodcastReceiver, intentFilter);
    }

    private void unregisterReceiver() {
        LocalBroadcastManager.getInstance(ViomiApplication.getContext()).unregisterReceiver(mBraodcastReceiver);
    }

    private BroadcastReceiver mBraodcastReceiver=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getAction()) {
                case BroadcastAction.ACTION_DEVICE_BIND:
                    ToastUtil.show(getString(R.string.text_device_bind_success));
                    finish();
                    break;
            }
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver();
    }

    @Override
    protected void processMsg(Message msg) {
        switch (msg.what){
            case MSG_WHAT_BIND_FAIL:
                ToastUtil.show(getString(R.string.toast_operate_fail)+"!"+msg.obj);
                loading_layout.setVisibility(View.INVISIBLE);
                qrcode_img.setImageResource(R.drawable.request_qrcode);
                break;

            case MSG_WHAT_BIND_ALREADY:
                DeviceManager.getInstance().setDeviceBindFlag(true);
                Intent intent=new Intent(BroadcastAction.ACTION_DEVICE_ALREADY_BIND);
                LocalBroadcastManager.getInstance(ViomiApplication.getContext()).sendBroadcast(intent);
                ToastUtil.show(getString(R.string.text_device_bind_already));
                finish();
                break;
        }
    }

    //请求二维码
    private void requestQRImg() {
        loading_layout.setVisibility(View.VISIBLE);
        try {
            Log.i(TAG, "GetBindKey start!");
            MiotHostManager.getInstance().getBindKey(new BindKeyListener() {
                @Override
                public void onSucceed(final String key, int expire) {
                    String bindKey = key;
                    Log.e(TAG, "GetBindKey successed!");
                    Log.e(TAG, "bindkey: " + key);
                    Log.e(TAG, "expire: " + expire);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            qrcode_img.setImageBitmap(generateBitmap(key,300,300));
                            loading_layout.setVisibility(View.INVISIBLE);
                        }
                    });
                }

                @Override
                public void onFailed(MiotError error) {
                    Log.e(TAG, "getBindKey failed: " + error.toString());
                    if(error.getCode()==-6){
                        if(mHandler!=null){
                            Message message=mHandler.obtainMessage();
                            message.obj=error.toString();
                            message.what=MSG_WHAT_BIND_ALREADY;
                            mHandler.sendMessage(message);
                        }
                    }else {
                        if(mHandler!=null){
                            Message message=mHandler.obtainMessage();
                            message.obj=error.toString();
                            message.what=MSG_WHAT_BIND_FAIL;
                            mHandler.sendMessage(message);
                        }
                    }
                }
            });
        } catch (com.xiaomi.miot.typedef.exception.MiotException e) {
            Log.e(TAG, "getBindKey error: " + e.getMessage());
            e.printStackTrace();
            if(mHandler!=null){
                Message message=mHandler.obtainMessage();
                message.obj= e.getMessage();
                message.what=MSG_WHAT_BIND_FAIL;
                mHandler.sendMessage(message);
            }
        }
    }

    /***
     * 生成二维码
     * @param content
     * @param width
     * @param height
     * @return
     */
    private Bitmap generateBitmap(String content, int width, int height) {
        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        Map<EncodeHintType, String> hints = new HashMap<>();
        hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
        try {
            BitMatrix encode = qrCodeWriter.encode(content, BarcodeFormat.QR_CODE, width, height, hints);
            int[] pixels = new int[width * height];
            for (int i = 0; i < height; i++) {
                for (int j = 0; j < width; j++) {
                    if (encode.get(j, i)) {
                        pixels[i * width + j] = 0x00000000;
                    } else {
                        pixels[i * width + j] = 0xffffffff;
                    }
                }
            }
            return Bitmap.createBitmap(pixels, 0, width, width, height, Bitmap.Config.RGB_565);
        } catch (WriterException e) {
            e.printStackTrace();
        }
        return null;
    }

}
