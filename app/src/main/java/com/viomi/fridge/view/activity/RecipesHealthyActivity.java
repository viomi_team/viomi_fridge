package com.viomi.fridge.view.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.viomi.fridge.R;
import com.viomi.fridge.device.AppConfig;
import com.viomi.fridge.model.bean.recipe.RecipeCate;
import com.viomi.fridge.model.bean.recipe.RecipeDetail;
import com.viomi.fridge.model.bean.recipe.Secondlevel;
import com.viomi.fridge.model.bean.recipe.categoryInfo;
import com.viomi.fridge.mvp.presenter.RecipePresenter;
import com.viomi.fridge.mvp.presenter.RecipeSelectedPresenter;
import com.viomi.fridge.mvp.view.RecipeCateView;
import com.viomi.fridge.mvp.view.RecipeSelectedView;
import com.viomi.fridge.util.FileUtil;
import com.viomi.fridge.util.JsonUitls;
import com.viomi.fridge.view.adapter.RecipeAdapter;
import com.viomi.fridge.view.fragment.MyFragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecipesHealthyActivity extends BaseActivity implements RecipeSelectedView, View.OnClickListener {

    @BindView(R.id.back_icon)
    public ImageView back_icon;

    @BindView(R.id.search_icon)
    public ImageView search_icon;

    @BindView(R.id.title_name)
    public TextView title_name;

    @BindView(R.id.tv_dishes)
    public TextView tv_dishes;

    @BindView(R.id.tv_art)
    public TextView tv_art;

    @BindView(R.id.tv_kind)
    public TextView tv_kind;

    @BindView(R.id.tv_crowd)
    public TextView tv_crowd;

    @BindView(R.id.tv_frush)
    public TextView tv_frush;

    @BindView(R.id.tv_function)
    public TextView tv_function;

    @BindView(R.id.ll_refresh)
    public LinearLayout ll_refresh;

    @BindView(R.id.iv_refresh)
    public ImageView iv_refresh;

    @BindView(R.id.gridview)
    public GridView gridview;


    private ArrayList<RecipeDetail> details = new ArrayList<>();
    private TextView[] textViews = null;

    private RecipeAdapter recipeAdapter;
    //    RecipePresenter presenter = null;
    RecipeSelectedPresenter presenter = null;

    private Animation circle_anim;
    private HashMap<Integer, String[]> map;
    private int key = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipes_healthy);
        ButterKnife.bind(this);
        initdata();
    }

    private void initdata() {
        initGridView();
        FileUtil.writeTxtToFile(mContext, AppConfig.EventKey.EVENT_ENTER_RECEIPE, null);
        map = new HashMap<>();
        circle_anim = AnimationUtils.loadAnimation(mContext, R.anim.image_refresh_rotate);
        LinearInterpolator interpolator = new LinearInterpolator();  //设置匀速旋转，在xml文件中设置会出现卡顿
        circle_anim.setInterpolator(interpolator);

        textViews = new TextView[]{tv_dishes, tv_art, tv_kind, tv_crowd, tv_function};
        for (int i = 0; i < textViews.length; i++) {
            textViews[i].setOnClickListener(this);
        }
//        presenter = new RecipePresenter(this);
        presenter = new RecipeSelectedPresenter(this);
        title_name.setText("健康菜谱");
        back_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        search_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openActivity(RecipeSearchActivity.class);
            }
        });
        ll_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                refreshData();
            }
        });

        initJson();
        refreshData();
    }

    private void initGridView() {
        recipeAdapter = new RecipeAdapter(mContext);
        gridview.setAdapter(recipeAdapter);
        gridview.setOnItemClickListener(new ItemClickListener());
//        recipeAdapter.initData(getlist());
    }


    private void initJson() {
        initMap(JsonString.recipe1);
        initMap(JsonString.recipe2);
        initMap(JsonString.recipe3);
        initMap(JsonString.recipe4);
        initMap(JsonString.recipe5);
    }

    private void initMap(String str) {
        Secondlevel secondlevel = JsonUitls.parseSecondlevel(str);
        if (secondlevel != null) {
            ArrayList<categoryInfo> cates = secondlevel.getChilds();
            if (cates != null && cates.size() != 0) {
                String[] strings = new String[cates.size()];
                for (int i = 0; i < cates.size(); i++) {
                    strings[i] = cates.get(i).getCtgId();
                }
                map.put(key, strings);
                key++;
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        FileUtil.writeTxtToFile(mContext, AppConfig.EventKey.EVENT_EXIT_RECEIPE, null);
        map.clear();
        map = null;
    }

    private void refreshData() {
        details.clear();
        iv_refresh.startAnimation(circle_anim);
        loadDatas();
    }

    private void loadDatas() {
        if (map != null) {
            Random random = new Random();
            String[] strings = map.get(random.nextInt(map.size()));
            String cateid = strings[random.nextInt(strings.length)];
            if (TextUtils.isEmpty(cateid)) {
//                Log.i("info","==============cateid为null");
                return;
            }
            HashMap<String, String> map = new HashMap<>();
            map.put("key", getString(R.string.app_key));
            map.put("cid", cateid);
            map.put("page", "1");
            map.put("size", "10");
            presenter.loadData(map);
        }
    }

    private ArrayList<RecipeDetail> getlist() {
        ArrayList<RecipeDetail> list = new ArrayList<>();

        RecipeDetail detail1 = new RecipeDetail();
        detail1.setName("红烧肉翅");
        detail1.setMenuId("00100010070000017731");
        detail1.setThumbnail("http://f2.mob.com/null/2015/08/19/1439945057496.jpg");

        RecipeDetail detail2 = new RecipeDetail();
        detail2.setName("红烧肉翅");
        detail2.setMenuId("00100010070000017731");
        detail2.setThumbnail("http://f2.mob.com/null/2015/08/19/1439941503983.jpg");


        RecipeDetail detail3 = new RecipeDetail();
        detail3.setName("红烧肉翅");
        detail3.setMenuId("00100010070000017731");
        detail3.setThumbnail("http://f2.mob.com/null/2015/08/19/1439945057496.jpg");


        RecipeDetail detail4 = new RecipeDetail();
        detail4.setName("红烧肉翅");
        detail4.setMenuId("00100010070000017731");
        detail4.setThumbnail("http://f2.mob.com/null/2015/08/19/1439941503983.jpg");


        RecipeDetail detail5 = new RecipeDetail();
        detail5.setName("红烧肉翅");
        detail5.setMenuId("00100010070000017731");
        detail5.setThumbnail("http://f2.mob.com/null/2015/08/19/1439941503983.jpg");

        list.add(detail1);
        list.add(detail2);
        list.add(detail3);
        list.add(detail4);
        list.add(detail5);
        return list;
    }

//    private void getCategory() {
//        HashMap<String, String> map = new HashMap<>();
//        map.put("key", getString(R.string.app_key));
//        presenter.getCategory(map);
//    }

    @Override
    public void onClick(View view) {
        Bundle bundle = new Bundle();
        String name="";
        switch (view.getId()) {
            case R.id.tv_dishes:
                name= getString(R.string.recipe_dishes);
                bundle.putString("json", JsonString.recipe1);
                bundle.putString("title", getString(R.string.recipe_dishes));
                break;
            case R.id.tv_art:
                name= getString(R.string.recipe_art);
                bundle.putString("json", JsonString.recipe2);
                bundle.putString("title", getString(R.string.recipe_art));
                break;
            case R.id.tv_kind:
                name= getString(R.string.recipe_kind);
                bundle.putString("json", JsonString.recipe3);
                bundle.putString("title", getString(R.string.recipe_kind));
                break;
            case R.id.tv_crowd:
                name= getString(R.string.recipe_crowd);
                bundle.putString("json", JsonString.recipe4);
                bundle.putString("title", getString(R.string.recipe_crowd));
                break;
            case R.id.tv_function:
                name= getString(R.string.recipe_function);
                bundle.putString("json", JsonString.recipe5);
                bundle.putString("title", getString(R.string.recipe_function));
                break;
        }
        JSONObject object=new JSONObject();
        try {
            object.put("name",name);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        FileUtil.writeTxtToFile(mContext, AppConfig.EventKey.EVENT_ENTER_RECEIPE_CATE, object.toString());
        openActivity(RecipesSeriesActivity.class, bundle);
//          String ctgId= (String) view.getTag();
////        if (!TextUtils.isEmpty(ctgId)){
////            showToast("ctgId:"+ctgId,false);
////        }
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void loadData(RecipeDetail data) {
        bindData(data);
    }

    @Override
    public void onError(String error) {
        showToast(error, false);
        loadDatas();
    }


    private void bindData(RecipeDetail data) {
        if (!isContain(data)) {
            details.add(data);
//            showToast("details.size():"+details.size(),false);
//            Log.i("info","==============details.size():"+details.size());
            if (details.size() == 5) {
                iv_refresh.clearAnimation();
                recipeAdapter.initData(details);
            }else {
                loadDatas();
            }
        }else {
//            showToast("出现重复data",false);
//            Log.i("info","==============出现重复data");
            loadDatas();
        }
    }

    private boolean isContain(RecipeDetail data) {
        boolean flag = false;
        if (details.size() != 0) {
            for (int i = 0; i < details.size(); i++) {
                RecipeDetail recipeDetail = details.get(i);
                if (recipeDetail.getMenuId().equals(data.getMenuId())) {
                    flag = true;
                }
            }
        }
        return flag;
    }


    private class ItemClickListener implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            RecipeDetail detail = (RecipeDetail) adapterView.getItemAtPosition(i);
            if (detail != null) {
                Bundle bundle = new Bundle();
                bundle.putString("MenuId", detail.getMenuId());
                openActivity(RecipesDetailsActivity.class, bundle);
            }
        }
    }
//    @Override
//    public void showProgress() {
//        showLoading();
//    }
//
//    @Override
//    public void hideProgress() {
//        hideLoading();
//    }
//
//    @Override
//    public void loadData(RecipeCate data) {
//        bindata(data);
//    }
//
//    @Override
//    public void onError(String error) {
//        showToast(error, false);
//    }
//
//    private void bindata(RecipeCate data) {
//        if (data != null) {
//            ArrayList<Secondlevel> leves = data.getChilds();
//            for (int i = 0; i < leves.size(); i++) {
//                String ctgId = leves.get(i).getCategoryInfo().getCtgId();
//                textViews[i].setTag(ctgId);
//            }
//        }
//    }
}
