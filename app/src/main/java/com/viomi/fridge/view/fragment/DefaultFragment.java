package com.viomi.fridge.view.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.viomi.fridge.R;
import com.viomi.fridge.device.AppConfig;

/**
 * 智能互联默认 Fragment
 * Created by William on 2017/5/9.
 */
public class DefaultFragment extends Fragment {

    private static final String ARG_PARAM1 = "param";
    private boolean isDefault = false;
    private TextView tvChose;
    private LinearLayout mLinearLayout;

    public static DefaultFragment newInstance(boolean param) {
        DefaultFragment fragment = new DefaultFragment();
        Bundle args = new Bundle();
        args.putBoolean(ARG_PARAM1, param);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            isDefault = getArguments().getBoolean(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_default, container, false);

        mLinearLayout = (LinearLayout) view.findViewById(R.id.default_no_device);
        tvChose = (TextView) view.findViewById(R.id.default_chose);
        TextView tvDefault = (TextView) view.findViewById(R.id.default_text);

        if (isDefault) {
            mLinearLayout.setVisibility(View.GONE);
            tvDefault.setVisibility(View.VISIBLE);
        }

        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(getActivity().getApplicationContext());
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(AppConfig.ACTION_HAVE_DEVICE);
        localBroadcastManager.registerReceiver(mReceiver, intentFilter);

        return view;
    }

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getAction()) {
                case AppConfig.ACTION_HAVE_DEVICE:
                    if (!isDefault) {
                        mLinearLayout.setVisibility(View.GONE);
                        tvChose.setVisibility(View.VISIBLE);
                    }
                    break;
                case AppConfig.ACTION_HAVE_NO_DEVICE:
                    if (!isDefault) {
                        mLinearLayout.setVisibility(View.VISIBLE);
                        tvChose.setVisibility(View.GONE);
                    }
                    break;
            }
        }
    };

}
