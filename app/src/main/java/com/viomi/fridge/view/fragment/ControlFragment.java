package com.viomi.fridge.view.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.viomi.fridge.R;
import com.viomi.fridge.manager.ControlViewManager;
import com.viomi.fridge.manager.ControlManager;
import com.viomi.fridge.model.bean.DataReceiveInfo;
import com.viomi.fridge.model.bean.SerialInfo;
import com.viomi.fridge.model.parser.SerialReceiveParser;
import com.viomi.fridge.util.log;
import com.viomi.fridge.view.widget.ColdClosetOldView;
import com.viomi.fridge.view.widget.FreezingRoomOldView;
import com.viomi.fridge.view.widget.TempChangeableRoomOldView;

/**
 * Created by young2 on 2016/12/31.
 */

public class ControlFragment extends BaseFragment {
    private final static String TAG=ControlFragment.class.getSimpleName();
    private Activity mActivity;
    private FreezingRoomOldView mFreezingRoomView;
    private TempChangeableRoomOldView mTempChangeableRoomView;
    private ColdClosetOldView mColdClosetView;
    private ImageView mSmatrModelView,mHolidayModelView,mOneKeyCleanView;
    private ControlViewManager mControlViewManager;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mainView = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_control, null, false);
        return mainView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
    }

    private void init(){
        mActivity=getActivity();
//        DataReceiveInfo dataReceiveInfo=ControlManager.getInstance().getDataReceiveInfo();
        DataReceiveInfo dataReceiveInfo;
        mFreezingRoomView= (FreezingRoomOldView)getActivity().findViewById(R.id.freezing_room_layout);
      //  mFreezingRoomView.initData(dataReceiveInfo.freezing_room_temp_set,dataReceiveInfo.freezing_room_temp_real,dataReceiveInfo.mode);
        mTempChangeableRoomView= (TempChangeableRoomOldView) mActivity.findViewById(R.id.temp_changeable_room_layout);
      //  mTempChangeableRoomView.initData(dataReceiveInfo.temp_changeable_room_temp_set,dataReceiveInfo.temp_changeable_room_temp_real);
        mColdClosetView= (ColdClosetOldView) mActivity.findViewById(R.id.cold_closet_layout);
      //  mColdClosetView.initData(dataReceiveInfo.cold_closet_temp_set,dataReceiveInfo.cold_closet_temp_real,dataReceiveInfo.mode);
        mSmatrModelView= (ImageView) mActivity.findViewById(R.id.smart_button);
        mHolidayModelView= (ImageView) mActivity.findViewById(R.id.holiday_button);
        mOneKeyCleanView= (ImageView) mActivity.findViewById(R.id.one_key_clean_button);

        mColdClosetView.setOnTempChangeListener(new ColdClosetOldView.OnTempChangeListener() {
            @Override
            public void onTempChange(int temp) {
                Log.i(TAG,"mColdClosetView onTempChange,temp="+temp);
                if(ControlManager.getInstance().isCommodityInspectionRunning()){
                    Toast.makeText(mActivity,R.string.toast_commodity_inspection_running,Toast.LENGTH_SHORT).show();
                    return;
                }
                mControlViewManager.paramsChange();
                boolean result=ControlManager.getInstance().setRoomTemp(temp,SerialInfo.ROOM_COLD_COLSET,null);
                if(!result){
                    Toast.makeText(getActivity(),getString(R.string.toast_set_fail),Toast.LENGTH_SHORT).show();
                }
            }
        });

        mTempChangeableRoomView.setOnTempChangeListener(new TempChangeableRoomOldView.OnTempChangeListener() {
            @Override
            public void onTempChange(int temp) {
                Log.i(TAG,"mTempChangeableRoomView onTempChange,temp="+temp);
                if(ControlManager.getInstance().isCommodityInspectionRunning()){
                    Toast.makeText(mActivity,R.string.toast_commodity_inspection_running,Toast.LENGTH_SHORT).show();
                    return;
                }
                mControlViewManager.paramsChange();
                boolean result=ControlManager.getInstance().setRoomTemp(temp,SerialInfo.ROOM_CHANGEABLE_ROOM,null);
                if(!result){
                    Toast.makeText(getActivity(),getString(R.string.toast_set_fail),Toast.LENGTH_SHORT).show();
                }
            }
        });

        mFreezingRoomView.setOnTempChangeListener(new FreezingRoomOldView.OnTempChangeListener() {
            @Override
            public void onTempChange(int temp) {
                Log.i(TAG,"mFreezingRoomView onTempChange,temp="+temp);
                if(ControlManager.getInstance().isCommodityInspectionRunning()){
                    Toast.makeText(mActivity,R.string.toast_commodity_inspection_running,Toast.LENGTH_SHORT).show();
                    return;
                }
                mControlViewManager.paramsChange();
                boolean result=ControlManager.getInstance().setRoomTemp(temp,SerialInfo.ROOM_FREEZING_ROOM,null);
                if(!result){
                    Toast.makeText(getActivity(),getString(R.string.toast_set_fail),Toast.LENGTH_SHORT).show();
                }
            }
        });
        mSmatrModelView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ControlManager.getInstance().isCommodityInspectionRunning()){
                    Toast.makeText(mActivity,R.string.toast_commodity_inspection_running,Toast.LENGTH_SHORT).show();
                    return;
                }
                mSmatrModelView.setEnabled(false);
                mHolidayModelView.setEnabled(true);
                mControlViewManager.paramsChange();
                ControlManager.getInstance().enableSmartMode(true);

            }
        });
        mHolidayModelView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ControlManager.getInstance().isCommodityInspectionRunning()){
                    Toast.makeText(mActivity,R.string.toast_commodity_inspection_running,Toast.LENGTH_SHORT).show();
                    return;
                }
                mSmatrModelView.setEnabled(true);
                mHolidayModelView.setEnabled(false);
                mControlViewManager.paramsChange();
                boolean result=ControlManager.getInstance().enableHolidayMode(true,null);
                if(!result){
                    Toast.makeText(getActivity(),getString(R.string.toast_set_fail),Toast.LENGTH_SHORT).show();
                }
            }
        });
        mOneKeyCleanView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ControlManager.getInstance().isCommodityInspectionRunning()){
                    Toast.makeText(mActivity,R.string.toast_commodity_inspection_running,Toast.LENGTH_SHORT).show();
                    return;
                }
                mOneKeyCleanView.setEnabled(false);
                mControlViewManager.paramsChange();
                ControlManager.getInstance().enableOneKeyClean(true);

            }
        });

        mControlViewManager=new ControlViewManager();
        mControlViewManager.init(this);
    }

    /***
     * 刷新界面
     * @param info
     */
    public void refreshView(DataReceiveInfo info){
        if(info==null){
            return;
        }
        if(mControlViewManager.isSetting()){
            log.d(TAG,"is settting");
            return;
        }
        Log.i(TAG,"mode="+info.mode);
        mFreezingRoomView.setTemp(info.freezing_room_temp_set,info.freezing_room_temp_real,info.mode);
        mColdClosetView.setTemp(info.cold_closet_temp_set,info.cold_closet_temp_real,info.mode);
        mTempChangeableRoomView.setTemp(info.temp_changeable_room_temp_set,info.temp_changeable_room_temp_real);
        if(info.mode== SerialInfo.MODE_SMART){
            mSmatrModelView.setEnabled(false);
            mHolidayModelView.setEnabled(true);
        }else if(info.mode== SerialInfo.MODE_HOLIDAY){
            mSmatrModelView.setEnabled(true);
            mHolidayModelView.setEnabled(false);
        }else {
            mSmatrModelView.setEnabled(true);
            mHolidayModelView.setEnabled(true);
        }
        if(SerialReceiveParser.isOneKeyCleanWorking(info.status)){
            mOneKeyCleanView.setEnabled(false);
        }else {
            mOneKeyCleanView.setEnabled(true);
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(mControlViewManager!=null){
            mControlViewManager.close();
            mControlViewManager=null;
        }
        mFreezingRoomView.close();
        mColdClosetView.close();
        mTempChangeableRoomView.close();
    }
}
