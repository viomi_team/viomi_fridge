package com.viomi.fridge.view.adapter;

import android.net.Uri;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.io.File;
import java.util.List;

/**
 * Created by viomi on 2017/2/15.
 */

public class ScreenVPAdapter extends PagerAdapter {

    private List<ImageView> list;
    private List<File> filelist;

    public ScreenVPAdapter(List<ImageView> list, List<File> filelist) {
        this.list = list;
        this.filelist = filelist;
    }

    @Override
    public int getCount() {
        return Integer.MAX_VALUE;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view==object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ImageView imageView = list.get(position % list.size());

        if (filelist.size()>0) {
            imageView.setImageURI(Uri.fromFile(filelist.get(position % filelist.size())));
        }

        container.addView(imageView);
        return imageView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        list.get(position%list.size()).setImageURI(null);
        container.removeView(list.get(position%list.size()));
    }
}
