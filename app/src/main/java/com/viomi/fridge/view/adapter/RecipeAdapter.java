package com.viomi.fridge.view.adapter;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.viomi.fridge.R;
import com.viomi.fridge.model.bean.recipe.RecipeDetail;
import com.viomi.fridge.util.ImageLoaderUtils;
import com.viomi.fridge.util.ImgUtil;
import com.viomi.fridge.util.PhoneUtil;
import com.viomi.fridge.view.widget.RoundImageView;

public class RecipeAdapter extends ABaseAdapter<RecipeDetail> {
    private Context context;
    private int width;
    public RecipeAdapter(Context context) {
        super(context);
        this.context = context;
        width= (PhoneUtil.getScreenWidth(context)-PhoneUtil.dipToPx(context,160))/5;
    }

    @Override
    protected View setConvertView(int position, RecipeDetail entity, View convertView) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = View.inflate(getContext(), R.layout.item_recipe_view, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (entity!=null){
            holder.name.setText(entity.getName());
            holder.imageView.getLayoutParams().width=width;
            holder.imageView.getLayoutParams().height=width;
            ImgUtil.showDefinedImage(entity.getThumbnail(),holder.imageView,R.drawable.zf_default_message_image);
        }
        return convertView;
    }

    private class ViewHolder {
        RoundImageView imageView;
        TextView name;
        public ViewHolder(View view) {
            this.imageView = (RoundImageView) view.findViewById(R.id.iv_icon);
            this.name = (TextView) view.findViewById(R.id.tv_name);
        }
    }
}
