package com.viomi.fridge.view.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bigkoo.pickerview.TimePickerView;
import com.bigkoo.pickerview.listener.CustomListener;
import com.viomi.common.widget.SwitchButton;
import com.viomi.fridge.R;
import com.viomi.fridge.albumhttp.AlbumActivity;
import com.viomi.fridge.common.GlobalParams;
import com.viomi.fridge.device.AppConfig;
import com.viomi.fridge.device.DeviceConfig;
import com.viomi.fridge.manager.AccountManager;
import com.viomi.fridge.manager.VoiceManager;
import com.viomi.fridge.model.bean.MiIndentify;
import com.viomi.fridge.model.bean.ScreenSleepBean;
import com.viomi.fridge.model.bean.ViomiUser;
import com.viomi.fridge.util.ApkUtil;
import com.viomi.fridge.util.PhoneUtil;
import com.viomi.fridge.util.SDCardUtils;
import com.viomi.fridge.util.SystemFileUtils;
import com.viomi.fridge.util.log;
import com.viomi.fridge.view.adapter.SleepTimeAdapter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class SystemActivity extends BaseActivity {

    private SwitchButton mVmallDebugSwitchBtn, mVmallSwitchBtn;
    private EditText mVmallEditView;
    private Button mVmallSaveButton;
    private SwitchButton voiceBtn;
    private SeekBar soundBar;
    private SeekBar lightBar;
    private Button clear_cache;
    private TextView valumeText;
    private AudioManager mAudioManager;
    private RelativeLayout restart;
    private RelativeLayout version;
    private RelativeLayout homePage_config;
    private RelativeLayout e_album;
    private RelativeLayout instructions;
    private RelativeLayout version_update;
    private RelativeLayout test_model;
    private RelativeLayout system_info;
    private TextView version_text;
    private ImageView back_icon;
    private int clickCount;
    private LinearLayout hide_layout;
    private boolean mIgnoreSwitchChange;
    private RelativeLayout dialog_layout;
    private TextView no_thanks;
    private TextView ok_do;
    private LinearLayout content_layout;
    private RelativeLayout set_screen_sleep;
    private Spinner screen_sleep_spinner;
    private List<ScreenSleepBean> screenSleepBeanList;
    private boolean mAppUpgradeFlag, mSystemUpgradeFlag;
    private RelativeLayout date_and_time;
    private TimePickerView pvCustomTime;
    private SwitchButton humanSensorBtn;
    private RelativeLayout feedback;
    private ImageView mMusicPlayView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_system);
        mAppUpgradeFlag = getIntent().getBooleanExtra("app_new", false);
        mSystemUpgradeFlag = getIntent().getBooleanExtra("system_new", false);
        mVmallSwitchBtn = (SwitchButton) findViewById(R.id.VmallHttpBtn);
        mVmallDebugSwitchBtn = (SwitchButton) findViewById(R.id.VmallDebugBtn);
        mVmallEditView = (EditText) findViewById(R.id.h5_url);
        mVmallSaveButton = (Button) findViewById(R.id.h5_url_button);
        back_icon = (ImageView) findViewById(R.id.back_icon);
        voiceBtn = (SwitchButton) findViewById(R.id.voiceBtn);
        mMusicPlayView = (ImageView) findViewById(R.id.music_play_view);
        humanSensorBtn = (SwitchButton) findViewById(R.id.humanSensorBtn);
        //音量调节
        soundBar = (SeekBar) findViewById(R.id.soundBar);
        //屏幕亮度
        lightBar = (SeekBar) findViewById(R.id.lightBar);
        //电子相册
        e_album = (RelativeLayout) findViewById(R.id.e_album);
//        //首页配置
//        homePage_config = (RelativeLayout) findViewById(R.id.homePage_config);
        //存储空间
        clear_cache = (Button) findViewById(R.id.clear_cache);
        valumeText = (TextView) findViewById(R.id.valumeText);
        //当前版本
        version = (RelativeLayout) findViewById(R.id.version);
        version_text = (TextView) findViewById(R.id.version_text);
        TextView user_text = (TextView) findViewById(R.id.user_text);
        ViomiUser viomiUser = AccountManager.getViomiUser(SystemActivity.this);
        if (viomiUser != null) {
            user_text.setText(GlobalParams.getInstance().getUserId() + "," + viomiUser.getUserCode() + "," + viomiUser.getToken());
        }
        TextView version_new = (TextView) findViewById(R.id.version_new);
        if (mAppUpgradeFlag || mSystemUpgradeFlag) {
            version_new.setVisibility(View.VISIBLE);
        }


        system_info = (RelativeLayout) findViewById(R.id.system_info);
//        使用说明
        instructions = (RelativeLayout) findViewById(R.id.instructions);
        //重启系统
        restart = (RelativeLayout) findViewById(R.id.restart);
        dialog_layout = (RelativeLayout) findViewById(R.id.dialog_layout);
        content_layout = (LinearLayout) findViewById(R.id.content_layout);
        no_thanks = (TextView) findViewById(R.id.no_thanks);
        ok_do = (TextView) findViewById(R.id.ok_do);
        //开发者模式隐藏内容
        hide_layout = (LinearLayout) findViewById(R.id.hide_layout);
        //版本升级
        version_update = (RelativeLayout) findViewById(R.id.version_update);
        //测试模式
        test_model = (RelativeLayout) findViewById(R.id.test_model);
        //屏幕待机时间
        set_screen_sleep = (RelativeLayout) findViewById(R.id.set_screen_sleep);
        screen_sleep_spinner = (Spinner) findViewById(R.id.screen_sleep_spinner);
        //设置时间和日期
        date_and_time = (RelativeLayout) findViewById(R.id.date_and_time);

        RelativeLayout vmall_http_set = (RelativeLayout) findViewById(R.id.vmall_http_set);
        if (GlobalParams.HTTP_DEBUG) {
            vmall_http_set.setVisibility(View.VISIBLE);
        } else {
            vmall_http_set.setVisibility(View.GONE);
        }
        //意见反馈入口
        feedback = (RelativeLayout) findViewById(R.id.feedback);

        version_text.setText("V" + ApkUtil.getVersionCode());
        mVmallEditView.setText(GlobalParams.getInstance().getVmallDebugUrl());

        TextView info_text = (TextView) findViewById(R.id.info_text);

        MiIndentify miIndentify = PhoneUtil.getMiIdentify();
        String mac = miIndentify.mac;
        String did = miIndentify.did;
        if (miIndentify.mac.equals(DeviceConfig.DefaultMac)) {
            mac = "error";
            did = "error";
        }
        if (miIndentify.did.contains("34CE00")) {//预防mac烧到did
            did = "error";
        }
        info_text.setText("MAC:" + mac + ",DID=" + did);
        initListener();
        init();
    }


    private void init() {
        //音量相关
        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        //当前音量
        int currentVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_SYSTEM);
        //最大音量
        int maxVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_SYSTEM);
        soundBar.setMax(maxVolume);
        soundBar.setProgress(currentVolume);

        //亮度相关
        int systemBrightness = 0;
        try {
            systemBrightness = Settings.System.getInt(getContentResolver(), Settings.System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        lightBar.setMax(255);
        lightBar.setProgress(systemBrightness);

        //存储
        String ram = "可用：" + String.format("%.2f", SDCardUtils.getSDCardFreeSize() / 1024f) + "G";
        //+ "G   总容量：" + String.format("%.2f", SDCardUtils.getSDCardTotalSize() / 1024f) + "G";
        valumeText.setText(ram);

        //屏幕休眠
        screenSleepBeanList = new ArrayList<>();
        screenSleepBeanList.add(new ScreenSleepBean(15 * 1000, "15秒", false));
        screenSleepBeanList.add(new ScreenSleepBean(30 * 1000, "30秒", false));
        screenSleepBeanList.add(new ScreenSleepBean(1 * 60 * 1000, "1分钟", false));
        screenSleepBeanList.add(new ScreenSleepBean(2 * 60 * 1000, "2分钟", false));
        screenSleepBeanList.add(new ScreenSleepBean(5 * 60 * 1000, "5分钟", false));
        screenSleepBeanList.add(new ScreenSleepBean(10 * 60 * 1000, "10分钟", false));
        screenSleepBeanList.add(new ScreenSleepBean(30 * 60 * 1000, "30分钟", false));
        screen_sleep_spinner.setAdapter(new SleepTimeAdapter(screenSleepBeanList, this));
        int screenTime;
        try {
            screenTime = Settings.System.getInt(getContentResolver(), Settings.System.SCREEN_OFF_TIMEOUT);
            switch (screenTime) {
                case 15 * 1000: {
                    screen_sleep_spinner.setSelection(0);
                    break;
                }
                case 30 * 1000: {
                    screen_sleep_spinner.setSelection(1);
                    break;
                }
                case 1 * 60 * 1000: {
                    screen_sleep_spinner.setSelection(2);
                    break;
                }
                case 2 * 60 * 1000: {
                    screen_sleep_spinner.setSelection(3);
                    break;
                }
                case 5 * 60 * 1000: {
                    screen_sleep_spinner.setSelection(4);
                    break;
                }
                case 10 * 60 * 1000: {
                    screen_sleep_spinner.setSelection(5);
                    break;
                }
                case 30 * 60 * 1000: {
                    screen_sleep_spinner.setSelection(6);
                    break;
                }

                default: {
                    screen_sleep_spinner.setSelection(4);
                    break;
                }
            }
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }

        initCustomTimePicker();

        voiceBtn.setChecked(GlobalParams.getInstance().isVoiceEnabe());
        humanSensorBtn.setChecked(GlobalParams.getInstance().isHumanSensorSwitch());
    }


    private void initCustomTimePicker() {
        // 注意：自定义布局中，id为 optionspicker 或者 timepicker 的布局以及其子控件必须要有，否则会报空指针
        // 具体可参考demo 里面的两个自定义布局
        //因为系统Calendar的月份是从0-11的,所以如果是调用Calendar的set方法来设置时间,月份的范围也要是从0-11
        //控制时间范围(如果不设置范围，则使用默认时间1900-2100年，此段代码可注释)
        Calendar selectedDate = Calendar.getInstance();//系统当前时间
        Calendar startDate = Calendar.getInstance();
        startDate.set(2017, 0, 1);
        Calendar endDate = Calendar.getInstance();
        endDate.set(2027, 2, 28);
        //时间选择器 ，自定义布局
        pvCustomTime = new TimePickerView.Builder(this, new TimePickerView.OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {//选中事件回调
                setSystemTime(date.getTime());
            }
        })
                /*.setType(TimePickerView.Type.ALL)//default is all
                .setCancelText("Cancel")
                .setSubmitText("Sure")
                .setContentSize(18)
                .setTitleSize(20)
                .setTitleText("Title")
                .isCyclic(true)// default is false
                .setTitleColor(Color.BLACK)
               /*.setDividerColor(Color.WHITE)//设置分割线的颜色
                .setTextColorCenter(Color.LTGRAY)//设置选中项的颜色
                .setLineSpacingMultiplier(1.6f)//设置两横线之间的间隔倍数
                .setTitleBgColor(Color.DKGRAY)//标题背景颜色 Night mode
                .setBgColor(Color.BLACK)//滚轮背景颜色 Night mode
                .setSubmitColor(Color.WHITE)
                .setCancelColor(Color.WHITE)*/
               /*.gravity(Gravity.RIGHT)// default is center*/
                .setDate(selectedDate)
                .setRangDate(startDate, endDate)
                .setLayoutRes(R.layout.system_time_setting_layout, new CustomListener() {

                    @Override
                    public void customLayout(View v) {
                        final TextView tvSubmit = (TextView) v.findViewById(R.id.yes_set);
                        TextView ivCancel = (TextView) v.findViewById(R.id.no_set);
                        tvSubmit.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                pvCustomTime.returnData();
                                sendBroadcast(new Intent(AppConfig.ACTION_FOOD_MANAGE_UPDATE));
                            }
                        });
                        ivCancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                pvCustomTime.dismiss();
                            }
                        });
                    }
                })
                .setDividerColor(getResources().getColor(R.color.myTheme1))
                .build();
    }

    //设置系统时间
    private void setSystemTime(long time) {
//        Intent intent = new Intent(Settings.ACTION_DATE_SETTINGS);
//        startActivity(intent);
        Intent intent = new Intent();
        intent.setAction("android.intent.action.USER_SET_TIME");
        intent.putExtra("time", time);
        sendBroadcast(intent);
    }

    //设置屏幕熄灭时间
    private boolean setScreenSleepTime(int time) {
        return Settings.System.putInt(getContentResolver(), Settings.System.SCREEN_OFF_TIMEOUT, time);
    }


    //在这里设置点击事件
    private void initListener() {

        back_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        soundBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mAudioManager.setStreamVolume(AudioManager.STREAM_SYSTEM, progress, 0);
                mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, progress, 0);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        lightBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                saveBrightness(SystemActivity.this, progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        e_album.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SystemActivity.this, AlbumActivity.class);
                startActivity(intent);
            }
        });

        clear_cache.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        version.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                clickCount++;
//                if (clickCount == 5) {
//                    hide_layout.setVisibility(View.VISIBLE);
//                }
                Intent intent = new Intent(SystemActivity.this, VersionManagerActivity.class);
                intent.putExtra("app_new", mAppUpgradeFlag);
                intent.putExtra("system_new", mSystemUpgradeFlag);
                startActivity(intent);
            }
        });

        system_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickCount++;
                if (clickCount == 5) {
                    hide_layout.setVisibility(View.VISIBLE);
                }
            }
        });

//        instructions.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//            }
//        });

        restart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_layout.setVisibility(View.VISIBLE);
            }
        });

        version_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SystemActivity.this, VersionManagerActivity.class);
                startActivity(intent);
            }
        });

        test_model.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SystemActivity.this, FactoryTestActivity.class);
                startActivity(intent);
            }
        });

        RelativeLayout system_set = (RelativeLayout) findViewById(R.id.system_set);
        system_set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Settings.ACTION_SETTINGS);
                startActivity(intent);
            }
        });

        voiceBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                GlobalParams.getInstance().setVoiceEnabe(b);
                VoiceManager.getInstance().enableVoice(b);
            }
        });

        humanSensorBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SystemFileUtils.HumanSensorType sensorType;
                SystemFileUtils.HumanSensorPathEnum pathEnum;
                if (isChecked) {
                    sensorType = SystemFileUtils.HumanSensorType.open;
                } else {
                    sensorType = SystemFileUtils.HumanSensorType.close;
                }

                switch (DeviceConfig.MODEL) {
                    // 小鲜互联 云米智能冰箱三门 绿联主控
                    case AppConfig.VIOMI_FRIDGE_V1:
                        pathEnum = SystemFileUtils.HumanSensorPathEnum.DOOR3;
                        break;

                    // 小鲜互联 云米智能冰箱四门 双鹿主控
                    case AppConfig.VIOMI_FRIDGE_V2:
                        pathEnum = SystemFileUtils.HumanSensorPathEnum.DOOR4;
                        break;

                    // 云米462大屏金属门冰箱
                    case AppConfig.VIOMI_FRIDGE_V3:
                    case AppConfig.VIOMI_FRIDGE_V31:
                        pathEnum = SystemFileUtils.HumanSensorPathEnum.DOOR462;
                        break;

                    // 云米455大屏玻璃门冰箱
                    case AppConfig.VIOMI_FRIDGE_V4:
                        pathEnum = SystemFileUtils.HumanSensorPathEnum.DOOR455;
                        break;
                    default:
                        pathEnum = SystemFileUtils.HumanSensorPathEnum.DOOR4;
                        break;
                }
                SystemFileUtils.setHumanSensor(sensorType, pathEnum);

                GlobalParams.getInstance().setHumanSensorSwitch(isChecked);
            }
        });

        mVmallSwitchBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                GlobalParams.getInstance().setVmallHttpDebug(b);
            }
        });
        mVmallDebugSwitchBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                GlobalParams.getInstance().setVmallDebug(b);
            }
        });

        mVmallDebugSwitchBtn.setChecked(GlobalParams.getInstance().isVmallDebug());
        mVmallSwitchBtn.setChecked(GlobalParams.getInstance().isVmallHttpDebug());

        dialog_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_layout.setVisibility(View.GONE);
            }
        });

        content_layout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });

        no_thanks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_layout.setVisibility(View.GONE);
            }
        });

        ok_do.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent("com.gmt.fridge.action.dameon.reboot");
                sendBroadcast(intent);
            }
        });
        mVmallSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GlobalParams.getInstance().setVmallDebugUrl(mVmallEditView.getText().toString());
                Toast.makeText(SystemActivity.this, "保存成功", Toast.LENGTH_SHORT).show();
            }
        });

        screen_sleep_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                setScreenSleepTime(screenSleepBeanList.get(position).getTime());
                log.myE("onItemSelected", "------------------------------------setScreenSleepTime-----------------------------");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        date_and_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pvCustomTime.show();
            }
        });

        instructions.setOnClickListener(v -> {
            Intent intent = new Intent(this, InstructionsActivity.class);
            startActivity(intent);
        });

        feedback.setOnClickListener((view) -> {
            Intent intent = new Intent(this, FeedbackActivity.class);
            startActivity(intent);
        });

    }

    @Nullable
    @Override
    protected Dialog onCreateDialog(int id, Bundle args) {
        return super.onCreateDialog(id, args);
    }

    private void saveBrightness(Context context, int brightness) {
        Uri uri = Settings.System.getUriFor(Settings.System.SCREEN_BRIGHTNESS);
        Settings.System.putInt(context.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS, brightness);
        context.getContentResolver().notifyChange(uri, null);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.in_activity_back, R.anim.out_activity_back);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mAudioManager.isMusicActive()) {
            mMusicPlayView.setVisibility(View.GONE);
        } else {
            mMusicPlayView.setVisibility(View.GONE);
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
