package com.viomi.fridge.view.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.unilife.UMToken;
import com.unilife.common.content.beans.iqiyi.IqiyiProgramInfo;
import com.unilife.common.content.beans.iqiyi.IqiyiProgramListData;
import com.unilife.common.utils.IqiyiUtils;
import com.unilife.content.logic.logic.IUMLogicListener;
import com.unilife.content.logic.models.iqiyi.UMIqiyiVideo;
import com.viomi.fridge.R;
import com.viomi.fridge.util.ToastUtil;
import com.viomi.fridge.util.log;
import com.viomi.fridge.view.adapter.VideoGridAdapter;
import com.viomi.fridge.view.adapter.VideoSearchHisAdapter;

import java.util.List;
import java.util.Set;

public class VideoSearchActivity extends BaseActivity {

    private final static String TAG = "VideoSearchActivity";
    private ImageView btn_back;
    private EditText search_txt;
    private TextView search_btn;
    private LinearLayout history_layout;
    private GridView his_gridview;
    private RelativeLayout result_layout;
    private GridView result_gridview;
    private String m_token;
    private SharedPreferences sp;

    private VideoSearchHisAdapter hisAdapter;
    private Set<String> historyList;
    private RelativeLayout loading_layout;
    private List<IqiyiProgramInfo> dataList;
    private String[] checkhisArry;
    private ImageView clear_input;
    private TextView clear_history;
    private RelativeLayout header;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_search);

        header = (RelativeLayout) findViewById(R.id.header);
        btn_back = (ImageView) findViewById(R.id.btn_back);
        search_txt = (EditText) findViewById(R.id.search_txt);
        search_btn = (TextView) findViewById(R.id.search_btn);
        history_layout = (LinearLayout) findViewById(R.id.history_layout);
        his_gridview = (GridView) findViewById(R.id.his_gridview);
        result_layout = (RelativeLayout) findViewById(R.id.result_layout);
        result_gridview = (GridView) findViewById(R.id.result_gridview);
        loading_layout = (RelativeLayout) findViewById(R.id.loading_layout);
        clear_input = (ImageView) findViewById(R.id.clear_input);
        clear_history = (TextView) findViewById(R.id.clear_history);
        clear_history.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);

        sp = getSharedPreferences("video_history", MODE_PRIVATE);

        initListener();
        getUMToken(null);
        showHistory();
        String vioce_control_keyword = getIntent().getStringExtra("vioce_control_keyword");
        if (vioce_control_keyword != null && vioce_control_keyword.length() != 0) {
            setSearch(vioce_control_keyword);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        String vioce_control_keyword = getIntent().getStringExtra("vioce_control_keyword");
        if (vioce_control_keyword != null && vioce_control_keyword.length() != 0) {
            setSearch(vioce_control_keyword);
        }
    }


    //语音对接-搜索视频，传入关键字
    private void setSearch(String text) {
        search_txt.setText(text);
        preSearch();
    }

    private String[] checkHistory() {
        String history_txt = sp.getString("video_history_txt", "");
        if (history_txt.length() == 0) {
            return new String[0];
        }
        String[] hisArray = history_txt.split("split_tag_ofvk");
        return hisArray;
    }

    private void addHistory(String keyword) {
        String history_txt = sp.getString("video_history_txt", "");
        String new_history_txt = keyword + "split_tag_ofvk" + history_txt;

        String[] hisArray = new_history_txt.split("split_tag_ofvk");

        if (hisArray.length > 20) {
            new_history_txt = "";
            for (int i = 0; i < 20; i++) {
                new_history_txt = new_history_txt + hisArray[i] + "split_tag_ofvk";
            }
        }

        SharedPreferences.Editor edit = sp.edit();
        edit.putString("video_history_txt", new_history_txt);
        edit.commit();
    }

    private void deleteHistory() {
        String new_history_txt = "";
        SharedPreferences.Editor edit = sp.edit();
        edit.putString("video_history_txt", new_history_txt);
        edit.commit();
    }

    private void showHistory() {
        checkhisArry = checkHistory();
        hisAdapter = new VideoSearchHisAdapter(this, checkhisArry);
        his_gridview.setAdapter(hisAdapter);
    }

    private void disInputShowResult() {
        history_layout.setVisibility(View.GONE);
        result_layout.setVisibility(View.VISIBLE);
        hideKeyBoard();
    }

    private void hideKeyBoard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
        }
    }

    private void disResultShowInput() {
        history_layout.setVisibility(View.VISIBLE);
        result_layout.setVisibility(View.GONE);
    }

    private void showProgressBar() {
        loading_layout.setVisibility(View.VISIBLE);
    }

    private void disProgressBar() {
        loading_layout.setVisibility(View.GONE);
    }

    private void initListener() {
        header.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideKeyBoard();
                return false;
            }
        });

        history_layout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideKeyBoard();
                return false;
            }
        });

        btn_back.setOnClickListener((v) -> {
            onBackPressed();
        });

        search_btn.setOnClickListener(v -> {
            preSearch();
        });

        search_txt.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                disResultShowInput();
                return false;
            }
        });

        search_txt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s!=null&&s.length()>0) {
                    clear_input.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        search_txt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (event!=null) {
                    preSearch();
                    log.myE(TAG,"setOnEditorActionListener------");
                }
                return false;
            }
        });

        his_gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                setSearch(checkhisArry[position]);
            }
        });

        result_gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                IqiyiProgramInfo iqiyiProgramInfo = dataList.get(position);
                IqiyiUtils.forwardQYClient(iqiyiProgramInfo.getAlbumId(), iqiyiProgramInfo.getTvid(), VideoSearchActivity.this);
            }
        });

        loading_layout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });

        clear_input.setOnClickListener(v -> {
            search_txt.setText("");
            clear_input.setVisibility(View.GONE);
        });

        clear_history.setOnClickListener(v -> {
            deleteHistory();
            showHistory();
        });
    }


    private void preSearch() {
        String keyword = search_txt.getText().toString();
        if (keyword.length() == 0) {
            ToastUtil.show("请输入搜索关键字！");
            return;
        }

        addHistory(keyword);
        showHistory();
        search(keyword);
        disInputShowResult();
    }

    private void getUMToken(String keyword) {
        UMToken.getInstance().getToken("unilife_standard_api_yunmi", "unilife_standard_api_yunmi_123456", new UMToken.IUMTokenListener() {
            @Override
            public void onSuccess(String token) {
                log.myE(TAG, "" + token + "69kl");
                m_token = token;

                if (keyword != null && keyword.length() != 0) {
                    search(keyword);
                }
            }

            @Override
            public void onFail(String msg) {
                log.myE(TAG, "getUMToken-fail");
                disProgressBar();

                if (keyword != null && keyword.length() != 0) {
                    ToastUtil.show("网络连接失败，请重新搜索！");
                }
            }
        });
    }

    private void search(String keyword) {

        showProgressBar();

        if (m_token == null) {
            getUMToken(keyword);
            return;
        }

        UMIqiyiVideo.getInstance().getVideoBySearch(m_token, keyword, 0, 96, new IUMLogicListener() {
            @Override
            public void onSuccess(Object data, long offset, long total) {
                disProgressBar();

                IqiyiProgramListData iqiyiProgramListData = (IqiyiProgramListData) data;
                dataList = iqiyiProgramListData.getDataList();
                VideoGridAdapter adapter = new VideoGridAdapter(dataList, VideoSearchActivity.this);
                result_gridview.setAdapter(adapter);
            }

            @Override
            public void onError(String msg) {
                disProgressBar();
                ToastUtil.show("网络连接失败，请重新搜索！");
            }
        });
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        super.dispatchTouchEvent(ev);
        hideNavigationBar();
        return super.dispatchTouchEvent(ev);
    }

}
