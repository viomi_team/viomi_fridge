package com.viomi.fridge.view.adapter;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.unilife.common.content.beans.iqiyi.IqiyiProgramInfo;
import com.viomi.fridge.R;

import java.util.List;

/**
 * Created by Mocc on 2017/7/28
 */

public class VideoGridAdapter extends BaseAdapter {

    private List<IqiyiProgramInfo> dataList;
    private Context context;
    private LayoutInflater inflater;

    public VideoGridAdapter(List<IqiyiProgramInfo> dataList, Context context) {
        this.dataList = dataList;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Object getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.video_gridview_item_layout, null);
            holder = new ViewHolder();
            holder.img = (SimpleDraweeView) convertView.findViewById(R.id.img);
            holder.play_times = (TextView) convertView.findViewById(R.id.play_times);
            holder.update_time = (TextView) convertView.findViewById(R.id.update_time);
            holder.title = (TextView) convertView.findViewById(R.id.title);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        IqiyiProgramInfo info = dataList.get(position);

        if (info.getPicHorizontalHigh() != null && !"".equals(info.getPicHorizontalHigh())) {
            holder.img.setImageURI(info.getPicHorizontalHigh());
        } else {
            if (info.getPicHorizontalLow() != null) {
                holder.img.setImageURI(Uri.parse(info.getPicHorizontalLow()));
            } else {
                holder.img.setImageURI(Uri.parse(""));
            }
        }

        String times = info.getPlayTimes() + "";
        times = times.equals("") ? "0" : times;
        times = times.equals("null") ? "0" : times;
        holder.play_times.setText("播放" + times + "次");

        String uploadDate = info.getUploadDate() + "";
        uploadDate = uploadDate.equals("null") ? "" : uploadDate;
        uploadDate = uploadDate.equals("0") ? "" : uploadDate;
        holder.update_time.setText(uploadDate);

        String title = info.getTitle() + "";
        title = title.equals("null") ? "" : title;
        holder.title.setText(title);

        return convertView;
    }

    static class ViewHolder {
        SimpleDraweeView img;
        TextView play_times, update_time, title;
    }
}
