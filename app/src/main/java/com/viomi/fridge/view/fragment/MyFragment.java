package com.viomi.fridge.view.fragment;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.github.jdsjlzx.interfaces.OnLoadMoreListener;
import com.github.jdsjlzx.interfaces.OnRefreshListener;
import com.github.jdsjlzx.recyclerview.LRecyclerView;
import com.github.jdsjlzx.recyclerview.LRecyclerViewAdapter;
import com.github.jdsjlzx.recyclerview.ProgressStyle;
import com.viomi.fridge.R;
import com.viomi.fridge.model.bean.recipe.RecipeDetail;
import com.viomi.fridge.model.bean.recipe.RecipeSeries;
import com.viomi.fridge.model.bean.recipe.Step;
import com.viomi.fridge.mvp.presenter.RecipeSeriesPresenter;
import com.viomi.fridge.mvp.view.RecipeSeriesView;
import com.viomi.fridge.util.ToastUtil;
import com.viomi.fridge.util.ToolUtil;
import com.viomi.fridge.view.activity.RecipesDetailsActivity;
import com.viomi.fridge.view.adapter.RecipeCateAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.viomi.fridge.util.ApkUtil.getPackageName;


public class MyFragment extends BaseFragment implements RecipeSeriesView {
    private static final String TAG = MyFragment.class.getSimpleName();
    private static final int PAGE_NUM = 20;
    private int page = 1;
    private boolean isFirst = true;
    private Button btnRefresh;
    private String cateid;
    private View view;
    private LRecyclerView recyclerView;
    protected LRecyclerViewAdapter mLRecyclerViewAdapter = null;
    private TextView tv_empty;

    //    private View empty_view;
    private RelativeLayout rl_nodata_view, rl_no_view;
    private SimpleDraweeView sImg;
    DraweeController draweeController;

    private RecipeCateAdapter adapter;
    private List<RecipeDetail> dataList;
    RecipeSeriesPresenter presenter = null;

    public static MyFragment newInstance(String title) {
        MyFragment fragment = new MyFragment();
        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putString("cateid", title);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new RecipeSeriesPresenter(this);
        // 这里我只是简单的用num区别标签，其实具体应用中可以使用真实的fragment对象来作为叶片
        cateid = getArguments() != null ? getArguments().getString("cateid") : "";
    }

    /**
     * 为Fragment加载布局时调用
     **/
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        if (view == null) {
//            empty_view=inflater.inflate(R.layout.loading,null);
            view = inflater.inflate(R.layout.fragment_pager_list, null);
//            sImg = (SimpleDraweeView) empty_view.findViewById(R.id.sImg);
//            draweeController = Fresco.newDraweeControllerBuilder()
//                    .setAutoPlayAnimations(true)
//                    .setUri(Uri.parse("res://" + getPackageName() + "/" + R.drawable.loading_g))
//                    .build();
//            sImg.setController(draweeController);
            initViews(view);
        }
        return view;
    }

    private void initViews(View view) {
        rl_nodata_view = (RelativeLayout) view.findViewById(R.id.rl_no_data);
        rl_no_view = (RelativeLayout) view.findViewById(R.id.rl_no);
        sImg = (SimpleDraweeView) view.findViewById(R.id.sImg);
        draweeController = Fresco.newDraweeControllerBuilder()
                .setAutoPlayAnimations(true)
                .setUri(Uri.parse("res://" + getPackageName() + "/" + R.drawable.loading_g))
                .build();
        sImg.setController(draweeController);
//        tv_empty = (TextView) view.findViewById(R.id.tv_empty);
        recyclerView = (LRecyclerView) view.findViewById(R.id.list);
        recyclerView.setEmptyView(rl_nodata_view);
        // recyclerView.setPullRefreshEnabled(false);
        recyclerView.setHeaderViewColor(R.color.material_dark, R.color.material, android.R.color.white);
        recyclerView.setRefreshProgressStyle(ProgressStyle.Pacman); //设置下拉刷新Progress的样式
        recyclerView.setFooterViewHint("拼命加载中", "我是有底线的", "网络不给力啊，点击再试一次吧");
        ((DefaultItemAnimator) recyclerView.getItemAnimator()).setSupportsChangeAnimations(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        adapter = new RecipeCateAdapter(getActivity(), new itemClickListener());
        mLRecyclerViewAdapter = new LRecyclerViewAdapter(adapter);
        recyclerView.setAdapter(mLRecyclerViewAdapter);
        recyclerView.setLoadMoreEnabled(true);
        recyclerView.refreshComplete(10);
        recyclerView.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        recyclerView.refreshComplete(10);
                        page = 1;
                        isFirst = true;
                        if (dataList.size() > 0) {
                            dataList.clear();
                        }
                        requestMusices();
                    }
                }, 1000);
            }
        });
        recyclerView.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        requestMore();
                    }
                }, 1000);
            }
        });
        requestMusices();
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void loadData(RecipeSeries data) {
        if (isFirst) {
            initDatas(data);
        } else {
            bindDatas(data);
        }
    }

    @Override
    public void onError(String error) {
        ToastUtil.show(error);
    }

    private class itemClickListener implements RecipeCateAdapter.OnItemClickListener {
        @Override
        public void ItemClick(RecipeDetail entity) {
            if (entity != null) {
                String menuid = entity.getMenuId();
                if (!TextUtils.isEmpty(menuid)) {
                    Bundle bundle = new Bundle();
                    bundle.putString("MenuId", menuid);
                    openActivity(RecipesDetailsActivity.class, bundle);
                }
            }
        }
    }

    private void requestMusices() {
        if (TextUtils.isEmpty(cateid)) {
            return;
        }
        HashMap<String, String> map = new HashMap<>();
        map.put("key", getString(R.string.app_key));
        map.put("cid", cateid);
        map.put("page", "" + page);
        map.put("size", "" + PAGE_NUM);
        presenter.getRecipeSeries(map);
    }

    private int total = 0;

    private void initDatas(RecipeSeries recipeSeries) {
        if (recipeSeries != null) {
            ArrayList<RecipeDetail> list = ToolUtil.getList(recipeSeries.getList());
            int size = list.size();
            if (dataList == null) {
                dataList = new ArrayList<>();
            }
            if (list != null && size != 0) {
                total += size;
//                if (size == PAGE_NUM) {
//                    page++;
//                    recyclerView.setNoMore(false);
//                } else {
//                    recyclerView.setNoMore(true);
//                }
                page++;
                dataList.addAll(list);
                if (total < 8) {
                    requestMusices();
                    return;
                }
//                recyclerView.setNoMore(false);
//                if (dataList.size() > 0) {
//                    dataList.clear();
//                }
                recyclerView.setNoMore(false);
                total = 0;
                isFirst = false;
                adapter.setDataList(dataList);
            } else {
                if (dataList.size() != 0) {
                    recyclerView.setNoMore(false);
                    total = 0;
                    isFirst = false;
                    adapter.setDataList(dataList);
                }else {
                    recyclerView.setNoMore(true);
                    sImg.setVisibility(View.GONE);
                    rl_no_view.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    private void requestMore() {
        if (TextUtils.isEmpty(cateid)) {
            return;
        }
        HashMap<String, String> map = new HashMap<>();
        map.put("key", getString(R.string.app_key));
        map.put("cid", cateid);
        map.put("page", "" + page);
        map.put("size", "" + PAGE_NUM);
        presenter.getRecipeSeries(map);
    }

    private void bindDatas(RecipeSeries baseOnline) {
        if (baseOnline != null) {
            ArrayList<RecipeDetail> list = ToolUtil.getList(baseOnline.getList());
            int size = list.size();
            if (list != null && size != 0) {
                total += size;
                if (dataList == null) {
                    dataList = new ArrayList<>();
                }
//                if (size == PAGE_NUM) {
//                    recyclerView.setNoMore(false);
//                    page++;
//                } else {
//                    recyclerView.setNoMore(true);
//                }
                page++;
                dataList.addAll(list);
                if (total < 8) {
                    requestMore();
                    return;
                }
                recyclerView.setNoMore(false);
                total = 0;
                adapter.setDataList(dataList);
            } else {
                recyclerView.setNoMore(true);
            }
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ((ViewGroup) view.getParent()).removeView(view);
    }
}