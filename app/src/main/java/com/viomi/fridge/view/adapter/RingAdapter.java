package com.viomi.fridge.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.viomi.fridge.R;
import com.viomi.fridge.model.bean.RingBean;

import java.util.List;

/**
 * Created by viomi on 2017/2/27.
 */

public class RingAdapter extends BaseAdapter {

    private List<RingBean> list;
    private Context context;
    private LayoutInflater inflater;

    public RingAdapter(List<RingBean> list, Context context) {
        this.list = list;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = inflater.inflate(R.layout.ring_select_item_layout, null);
        TextView name = (TextView) convertView.findViewById(R.id.name);
        ImageView iv = (ImageView) convertView.findViewById(R.id.iv);
        name.setText(list.get(position).getName());
        if (list.get(position).isSelect()) {
            iv.setImageResource(R.drawable.ring_selected);
        } else {
            iv.setImageResource(R.drawable.ring_unselected);
        }
        return convertView;
    }
}
