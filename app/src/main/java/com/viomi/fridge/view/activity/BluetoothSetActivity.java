package com.viomi.fridge.view.activity;


import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.viomi.fridge.R;
import com.viomi.fridge.view.fragment.BluetoothSetFragment;

/**
 * Created by Ljh on 2017/10/23
 */

public class BluetoothSetActivity extends BaseActivity {

    private ImageView imgBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_head_container);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        ((TextView)findViewById(R.id.tvTitle)).setText(R.string.set);
        initListener();
        //
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        BluetoothSetFragment bluetoothSetFragment = new BluetoothSetFragment();
        transaction.add(R.id.container, bluetoothSetFragment);
        transaction.show(bluetoothSetFragment).commitAllowingStateLoss();
    }

    private void initListener() {
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
