package com.viomi.fridge.view.activity;

import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationListener;
import com.facebook.drawee.view.SimpleDraweeView;
import com.miot.api.MiotManager;
import com.viomi.fridge.R;
import com.viomi.fridge.albumhttp.AlbumActivity;
import com.viomi.fridge.albumhttp.CoreService;
import com.viomi.fridge.api.http.HttpConnect;
import com.viomi.fridge.broadcast.BroadcastAction;
import com.viomi.fridge.broadcast.ConnectionChangeReceiver;
import com.viomi.fridge.common.GlobalParams;
import com.viomi.fridge.common.ViomiApplication;
import com.viomi.fridge.device.AppConfig;
import com.viomi.fridge.device.AppConstants;
import com.viomi.fridge.device.DeviceConfig;
import com.viomi.fridge.manager.AccountManager;
import com.viomi.fridge.manager.BleManager;
import com.viomi.fridge.manager.StatsManager;
import com.viomi.fridge.manager.UpgradeManager;
import com.viomi.fridge.model.bean.FoodDetailData;
import com.viomi.fridge.model.bean.GoodsEntity;
import com.viomi.fridge.model.bean.HomeWidgetBean;
import com.viomi.fridge.model.bean.ViomiUser;
import com.viomi.fridge.model.bean.WebBaseData;
import com.viomi.fridge.mvp.bean.FridgeTempBean;
import com.viomi.fridge.mvp.bean.WeatherBean;
import com.viomi.fridge.mvp.presenter.NewMainPresenter;
import com.viomi.fridge.mvp.view.NewMain;
import com.viomi.fridge.service.BackgroudService;
import com.viomi.fridge.service.FloatButtonService;
import com.viomi.fridge.service.FoodManageService;
import com.viomi.fridge.service.InfoPushService;
import com.viomi.fridge.util.ApkUtil;
import com.viomi.fridge.util.FileUtil;
import com.viomi.fridge.util.OkHttpUtils;
import com.viomi.fridge.util.PhoneUtil;
import com.viomi.fridge.util.SystemFileUtils;
import com.viomi.fridge.util.ToastUtil;
import com.viomi.fridge.util.ToolUtil;
import com.viomi.fridge.util.WeatherIconUtil;
import com.viomi.fridge.util.log;
import com.viomi.fridge.view.adapter.FoodManageAdapter;
import com.viomi.fridge.view.web.activity.BrowserActivity;
import com.viomi.fridge.view.widget.AutoScrollViewPager;
import com.viomi.fridge.view.widget.AutoScrollViewPagerIMG;
import com.viomi.fridge.view.widget.MyClockView;
import com.viomi.fridge.view.widget.RecyclerSpace;
import com.viomi.fridge.view.widget.SmallRadioView;
import com.viomi.fridge.view.widget.VoiceSettingDialog;
import com.viomi.fridge.view.widget.WrapContentGridLayoutManager;
import com.viomi.fridge.wifimodel.WifiScanActivity;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import viomi.com.gdloc.MyLocationUitl;


public class MainNewV2Activity extends BaseActivity implements NewMain {

    private MyClockView myclockview;
    private RelativeLayout weather;
    private ImageView weather_icon;
    private TextView weather_pm25;
    private TextView weather_temp;
    private ImageView store_btn;
    private ImageView talk_btn;
    private ImageView timer_btn;
    private ImageView voice_btn;
    private ImageView network_btn;
    private SimpleDraweeView user_btn;
    private LinearLayout fridge_control;
    private TextView fridge_temp1;
    private TextView fridge_tempName1;
    private TextView fridge_temp2;
    private TextView fridge_tempName2;
    private TextView fridge_temp3;
    private TextView fridge_tempName3;
    private View fridge_temp_line1;
    private View fridge_temp_line2;
    private RelativeLayout album_layout;
    private AutoScrollViewPagerIMG album_vp;
    private FrameLayout mInfoView;
    private TextView mInfoTips;
    private RelativeLayout food_manage;
    private TextView mNoFood;
    private RecyclerView mRecyclerView;
    private LinearLayout water_map;
    private LinearLayout music;
    private LinearLayout video;
    private LinearLayout cook_menu;
    private LinearLayout intelligence_link;
    private SmallRadioView goods_smallpoint;
    private AutoScrollViewPager goods_viewPager;
    private RelativeLayout loading_layout;
    private RelativeLayout volume_layout;
    private RelativeLayout volume_area_layout;
    private SeekBar soundBar;
    private RelativeLayout qrcode_bg;
    private SimpleDraweeView qrcode_img;
    private Button qrcode_ok;
    private LinearLayout connect_hint_layout;
    private TextView go_connect;
    private TextView connect_know_it;
    private AudioManager mAudioManager;
    private WifiManager wifiManager;

    private final static String TAG = "MainNewV2Activity";
    private String mVmallUrl = HttpConnect.STOREMAINURL;
    private int mAdvertNewsCount, mUserNewsCount, mDeviceNewsCount;
    private VoiceSettingDialog mVoiceSettingDialog;

    private static final int REQUEST_WEATHER_CODE = 1001;
    private static final int RESPONE_WEATHER_CODE = 1002;

    private int secondVPsize;

    private NewMainPresenter presenter;

    private ConnectionChangeReceiver mReceiver;
    private BroadcastReceiver fileReceiver;
    private BroadcastReceiver mReceiver3;
    private BroadcastReceiver volumeReceiver;
    private BroadcastReceiver albumUpdateReceiver;
    private BroadcastReceiver mFoodManageReceiver;
    private BroadcastReceiver mLocationReceiver;
    private int[] wifiIcons;

    private List<FoodDetailData> mList;
    private FoodManageAdapter mAdapter;

    private Intent backgroudServiceintent;
    private Intent albumServiceIntent;
    private Intent floatServiceIntent;
    private Intent foodServiceIntent;
    private Intent pushServiceintent;
    private Intent deamonServiceintent;
    private LinearLayout fridge_temp_room2;
    private boolean isConnectNetwork;
    private RelativeLayout store_more;
    private ImageView mJingdongMallView;
    private BroadcastReceiver blueToothReceiver;
    private boolean isBlueToothConnected;
    private RelativeLayout main_layout;
    private int guideNo;
    private BroadcastReceiver updateReceiver;
    private boolean isUpdate;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_new_v2);
        initView();
        init();
        initListener();
        setdefault();
        registerReceiver();
        initService();
        initBlueToothState();
        initPresenter();
    }

    private void initPresenter() {
        presenter = new NewMainPresenter();
        presenter.attacthView(this);
        presenter.setEventCallback(new EventCallback() {
            @Override
            public void onCallback() {
                log.myE(TAG, "eventcallback");
                //check update per 4hr
                UpgradeManager.getInstance().checkAppUpgrade(MainNewV2Activity.this, UpgradeManager.OPERATE_MANUAL);
            }
        });
        presenter.refresh10Seconds();
    }

    private void initBlueToothState() {
        BluetoothAdapter ba = BluetoothAdapter.getDefaultAdapter();
        if (ba != null) {
            if (ba.isEnabled()) {
                int a2dp = ba.getProfileConnectionState(BluetoothProfile.A2DP);              //可操控蓝牙设备，如带播放暂停功能的蓝牙耳机
                int headset = ba.getProfileConnectionState(BluetoothProfile.HEADSET);        //蓝牙头戴式耳机，支持语音输入输出
                int health = ba.getProfileConnectionState(BluetoothProfile.HEALTH);          //蓝牙穿戴式设备

                //查看是否蓝牙是否连接到三种设备的一种，以此来判断是否处于连接状态还是打开并没有连接的状态
                int flag = -1;
                if (a2dp == BluetoothProfile.STATE_CONNECTED) {
                    flag = a2dp;
                } else if (headset == BluetoothProfile.STATE_CONNECTED) {
                    flag = headset;
                } else if (health == BluetoothProfile.STATE_CONNECTED) {
                    flag = health;
                }
                //说明连接上了三种设备的一种
                if (flag != -1) {
                    log.myE("蓝牙", "蓝牙连接 flag=" + flag);
                    isBlueToothConnected = true;
                } else {
                    log.myE("蓝牙", "蓝牙断开 flag=" + flag);
                }
            } else {
                log.myE("蓝牙", "蓝牙断开 unable");
            }
        } else {
            log.myE("蓝牙", "蓝牙异常");
        }
    }

    private void init() {
        OkHttpUtils.getInstance();
        String model = PhoneUtil.getDeviceModel();

        if (model != null) {
            DeviceConfig.MODEL = model;
        }

        //每次系统重启要写入红外感应
        //462/455没有变温室，隐藏
        SystemFileUtils.HumanSensorType sensorType;
        SystemFileUtils.HumanSensorPathEnum pathEnum;
        boolean humanSensorSwitch = GlobalParams.getInstance().isHumanSensorSwitch();
        if (humanSensorSwitch) {
            sensorType = SystemFileUtils.HumanSensorType.open;
        } else {
            sensorType = SystemFileUtils.HumanSensorType.close;
        }

        switch (DeviceConfig.MODEL) {
            // 小鲜互联 云米智能冰箱三门 绿联主控
            case AppConfig.VIOMI_FRIDGE_V1:
                pathEnum = SystemFileUtils.HumanSensorPathEnum.DOOR3;
                break;

            // 小鲜互联 云米智能冰箱四门 双鹿主控
            case AppConfig.VIOMI_FRIDGE_V2:
                pathEnum = SystemFileUtils.HumanSensorPathEnum.DOOR4;
                break;

            // 云米462大屏金属门冰箱
            case AppConfig.VIOMI_FRIDGE_V3:
            case AppConfig.VIOMI_FRIDGE_V31:
                pathEnum = SystemFileUtils.HumanSensorPathEnum.DOOR462;
                fridge_temp_room2.setVisibility(View.GONE);
                fridge_temp_line2.setVisibility(View.GONE);
                break;

            // 云米455大屏玻璃门冰箱
            case AppConfig.VIOMI_FRIDGE_V4:
                pathEnum = SystemFileUtils.HumanSensorPathEnum.DOOR455;
                fridge_temp_room2.setVisibility(View.GONE);
                fridge_temp_line2.setVisibility(View.GONE);
                break;
            default:
                pathEnum = SystemFileUtils.HumanSensorPathEnum.DOOR4;
                break;
        }
        SystemFileUtils.setHumanSensor(sensorType, pathEnum);
        GlobalParams.getInstance().setHumanSensorSwitch(humanSensorSwitch);

        //音量相关
        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        //最大音量
        int maxVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_SYSTEM);
        soundBar.setMax(maxVolume);

        //开机自动连wifi
        wifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
        wifiManager.setWifiEnabled(true);

        //加载相册
        updateAlbum();

        //食材管理的
        WrapContentGridLayoutManager gridLayoutManager = new WrapContentGridLayoutManager(this, 2);
        mRecyclerView.setLayoutManager(gridLayoutManager);
        RecyclerSpace space = new RecyclerSpace(4, 0, 0);   // 设置分割线
        mRecyclerView.addItemDecoration(space);
        mList = new ArrayList<>();
        mAdapter = new FoodManageAdapter(this, mList, true);
        mRecyclerView.setAdapter(mAdapter);

        //操作引导 每次安装新版本展示
        int versionCode = ApkUtil.getVersionCode();
        if (versionCode != GlobalParams.getInstance().getGuideVersion()) {
            showGuide();
            GlobalParams.getInstance().setGuideVersion(versionCode);
        }

    }

    private void registerReceiver() {
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        filter.addAction(Intent.ACTION_TIME_CHANGED);
        mReceiver = new ConnectionChangeReceiver() {
            @Override
            public void hasNoNetwork() {
                network_btn.setImageResource(R.drawable.main_icon_wifi_0_v2);
                isConnectNetwork = false;
            }

            @Override
            public void hasNetwork() {
                log.myE(TAG, "hasNetwork");
                isConnectNetwork = true;
                presenter.loadData();
                loationWeather();
            }
        };

        IntentFilter filter2 = new IntentFilter("viomi_file_completion");
        fileReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals("viomi_file_completion")) {
                    final Dialog albumDialog = new Dialog(MainNewV2Activity.this, R.style.fidge_dialog);
                    albumDialog.getWindow().setType((WindowManager.LayoutParams.TYPE_SYSTEM_ALERT));
                    View dialog_view = LayoutInflater.from(MainNewV2Activity.this).inflate(R.layout.album_dialog_layout, null);
                    albumDialog.setContentView(dialog_view);
                    TextView no_thanks = (TextView) dialog_view.findViewById(R.id.no_thanks);
                    TextView ok_do = (TextView) dialog_view.findViewById(R.id.ok_do);
                    no_thanks.setOnClickListener(v -> albumDialog.dismiss());
                    ok_do.setOnClickListener(v -> {
                        albumDialog.dismiss();
                        Intent intent1 = new Intent(MainNewV2Activity.this, AlbumActivity.class);
                        startActivity(intent1);
                    });
                    albumDialog.show();
                }
            }
        };

        wifiIcons = new int[]{R.drawable.main_icon_wifi_1_v2, R.drawable.main_icon_wifi_2_v2, R.drawable.main_icon_wifi_3_v2};
        IntentFilter filter3 = new IntentFilter(WifiManager.RSSI_CHANGED_ACTION);
        mReceiver3 = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(WifiManager.RSSI_CHANGED_ACTION)) {
                    WifiInfo info = wifiManager.getConnectionInfo();
                    if (info != null && info.getBSSID() != null) {
                        network_btn.setImageResource(wifiIcons[WifiManager.calculateSignalLevel(info.getRssi(), wifiIcons.length)]);
                        isConnectNetwork = true;
                    } else {
                        network_btn.setImageResource(R.drawable.main_icon_wifi_0_v2);
                        isConnectNetwork = false;
                    }

                }
            }
        };

        IntentFilter filter4 = new IntentFilter("android.media.VOLUME_CHANGED_ACTION");
        volumeReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals("android.media.VOLUME_CHANGED_ACTION")) {
                    int currentVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
                    setSoundBarValue(currentVolume);
                }
            }
        };

        IntentFilter filter5 = new IntentFilter("UPDATE_ALBUM");
        albumUpdateReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals("UPDATE_ALBUM")) {
                    updateAlbum();
                }
            }
        };

        /*
         * modify by William
         */
        IntentFilter filter6 = new IntentFilter();
        filter6.addAction(Intent.ACTION_TIME_TICK);
        filter6.addAction(AppConfig.ACTION_FOOD_MANAGE_SEND_DATA);
        mFoodManageReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(Intent.ACTION_TIME_TICK)) {
                    if (ToolUtil.isServiceWork(MainNewV2Activity.this, "com.viomi.fridge.service.FoodManageService")) {
                        log.d(TAG, "FoodManageService is Running");
                    } else {
                        startService(new Intent(MainNewV2Activity.this, FoodManageService.class));
                        log.d(TAG, "FoodManageService is not Running");
                    }
                } else if (intent.getAction().equals(AppConfig.ACTION_FOOD_MANAGE_SEND_DATA)) {    // 更新食材
                    log.d(TAG, "get data success");
                    List<FoodDetailData> data = (List<FoodDetailData>) intent.getSerializableExtra("food_list");
                    updateFood(data);
                }
            }
        };

        IntentFilter locationFilter = new IntentFilter();
        locationFilter.addAction(BroadcastAction.ACTION_WEATHER_GET);
        locationFilter.addAction(BroadcastAction.ACTION_PUSH_MESSAGE);
        mLocationReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(BroadcastAction.ACTION_WEATHER_GET)) {
                    String result = intent.getStringExtra("result");
                    presenter.referWeather(result);
                } else if (intent.getAction().equals(BroadcastAction.ACTION_PUSH_MESSAGE)) {
                    presenter.refershNewInfo();
                }
            }
        };
        LocalBroadcastManager.getInstance(ViomiApplication.getContext()).registerReceiver(mLocationReceiver, locationFilter);

        IntentFilter filter7 = new IntentFilter();
        filter7.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
        filter7.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        blueToothReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (BluetoothDevice.ACTION_ACL_CONNECTED.equals(intent.getAction())) {
                    BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    if (device.getBluetoothClass().getMajorDeviceClass() != BluetoothClass.Device.Major.PHONE) {
                        log.myE("蓝牙", "蓝牙连接");
                        isBlueToothConnected = true;
                        updateHeaderIcon_voice(false, isBlueToothConnected);
                    }
                } else if (BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(intent.getAction())) {
                    BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    if (device.getBluetoothClass().getMajorDeviceClass() != BluetoothClass.Device.Major.PHONE) {
                        log.myE("蓝牙", "蓝牙断开");
                        isBlueToothConnected = false;
                        int currentVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_SYSTEM);
                        updateHeaderIcon_voice(currentVolume == 0, isBlueToothConnected);
                    }
                }
            }
        };

        IntentFilter filter8 = new IntentFilter();
        filter8.addAction(BroadcastAction.ACTION_APP_UPGRADE_CHECK);
        filter8.addAction(BroadcastAction.ACTION_VERSION_UPDATE_CLICK);
        updateReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (BroadcastAction.ACTION_APP_UPGRADE_CHECK.equals(intent.getAction())) {
                    log.myE(TAG, "ACTION_APP_UPGRADE");
                    updateIcon_update(true);
                }

                if (BroadcastAction.ACTION_VERSION_UPDATE_CLICK.equals(intent.getAction())) {
                    log.myE(TAG, "ACTION_APP_UPGRADE_CLICK");
                    updateIcon_update(false);
                }
            }
        };

        registerReceiver(mReceiver, filter);
        registerReceiver(fileReceiver, filter2);
        registerReceiver(mReceiver3, filter3);
        registerReceiver(volumeReceiver, filter4);
        registerReceiver(albumUpdateReceiver, filter5);
        registerReceiver(mFoodManageReceiver, filter6);
        registerReceiver(blueToothReceiver, filter7);
        registerReceiver(updateReceiver, filter8);
    }

    private void unregisterReceiver() {
        unregisterReceiver(mReceiver);
        unregisterReceiver(fileReceiver);
        unregisterReceiver(mReceiver3);
        unregisterReceiver(volumeReceiver);
        unregisterReceiver(albumUpdateReceiver);
        unregisterReceiver(mFoodManageReceiver);
        unregisterReceiver(blueToothReceiver);
        unregisterReceiver(updateReceiver);
        LocalBroadcastManager.getInstance(ViomiApplication.getContext()).unregisterReceiver(mLocationReceiver);
    }

    private void initService() {
        backgroudServiceintent = new Intent(this, BackgroudService.class);
        startService(backgroudServiceintent);

        albumServiceIntent = new Intent(this, CoreService.class);
        startService(albumServiceIntent);

        floatServiceIntent = new Intent(this, FloatButtonService.class);

        foodServiceIntent = new Intent(MainNewV2Activity.this, FoodManageService.class);
        startService(foodServiceIntent);

        pushServiceintent = new Intent(MainNewV2Activity.this, InfoPushService.class);
        startService(pushServiceintent);

//        deamonServiceintent = new Intent(MainNewV2Activity.this, JobDaemonService.class);
//        startService(deamonServiceintent);
    }

    private void stopService() {
        stopService(backgroudServiceintent);
        stopService(albumServiceIntent);
        stopService(foodServiceIntent);
        stopService(pushServiceintent);
    }


    private void initListener() {
        myclockview.setOnClickListener(v -> {
//            showGuide();
        });

        mJingdongMallView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FileUtil.writeTxtToFile(mContext, AppConfig.EventKey.EVENT_MALL, null);
                Intent intent = new Intent();
                intent.setAction("android.intent.action.jd.smart.fridge.coolbuy.main");
                intent.addCategory(Intent.CATEGORY_DEFAULT);
                startActivity(intent);
            }
        });

        store_btn.setOnClickListener((view) -> {
            if (AppConfig.VIOMI_FRIDGE_V31.equals(DeviceConfig.MODEL)) {
                Intent intent = new Intent();
                intent.setAction("android.intent.action.jd.smart.fridge.coolbuy.main");
                intent.addCategory(Intent.CATEGORY_DEFAULT);
                startActivity(intent);
            } else {
                StatsManager.recordCountEvent(this, StatsManager.EVENT_ID_MODULE_ENTER, StatsManager.EVENT_TYPE_VMALL_MAIN, "vamll_icon");
                if (isConnectNetwork) {
                    Intent intent = new Intent(MainNewV2Activity.this, VmallWebActivity.class);
                    WebBaseData data = new WebBaseData();
                    data.url = mVmallUrl + "/index.html";
                    intent.putExtra("isMall",true);
                    intent.putExtra(WebBaseData.Intent_String, data);
                    startActivity(intent);
//                    Intent intent = new Intent(MainNewV2Activity.this, BrowserActivity.class);
//                    String url = mVmallUrl + "/index.html";
//                    intent.putExtra(AppConstants.WEB_URL, url);
//                    startActivity(intent);
                } else {
                    showConnectHint();
                }
            }
        });

        mInfoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StatsManager.recordCountEvent(MainNewV2Activity.this, StatsManager.EVENT_ID_MODULE_ENTER, StatsManager.EVENT_TYPE_INFORM, "");
                Intent intent = new Intent(MainNewV2Activity.this, InfoCenterActivity.class);
                intent.putExtra("advert", mAdvertNewsCount);
                intent.putExtra("device", mDeviceNewsCount);
                intent.putExtra("user", mUserNewsCount);
                startActivity(intent);
            }
        });

        talk_btn.setOnClickListener((view) -> {
            StatsManager.recordCountEvent(MainNewV2Activity.this, StatsManager.EVENT_ID_MODULE_ENTER, StatsManager.EVENT_TYPE_VOICE, "");
            onShowVoiceSetDialog();
        });

        timer_btn.setOnClickListener((view) -> {
            StatsManager.recordCountEvent(MainNewV2Activity.this, StatsManager.EVENT_ID_MODULE_ENTER, StatsManager.EVENT_TYPE_TIMER, "");
            Intent intent = new Intent(MainNewV2Activity.this, Timer2Activity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        });

        voice_btn.setOnClickListener((view) -> {

            StatsManager.recordCountEvent(MainNewV2Activity.this, StatsManager.EVENT_ID_MODULE_ENTER, StatsManager.EVENT_TYPE_VOLUME, "");
            volume_layout.setVisibility(View.VISIBLE);
            int currentVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_SYSTEM);
            setSoundBarValue(currentVolume);
            updateIcon_blueTooth(BleManager.getInstance().isBleEnable());
        });

        volume_layout.setOnClickListener((view) -> {
            volume_layout.setVisibility(View.GONE);
        });

        volume_area_layout.setOnTouchListener((v, event) -> {
            return true;
        });

        soundBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mAudioManager.setStreamVolume(AudioManager.STREAM_SYSTEM, progress, 0);
                mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, progress, 0);
                updateHeaderIcon_voice(progress == 0, isBlueToothConnected);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        network_btn.setOnClickListener((view) -> {
            StatsManager.recordCountEvent(MainNewV2Activity.this, StatsManager.EVENT_ID_MODULE_ENTER, StatsManager.EVENT_TYPE_WIFI, "");
            Intent intent = new Intent(MainNewV2Activity.this, WifiScanActivity.class);
            startActivity(intent);
        });

        user_btn.setOnClickListener((view) -> {
            StatsManager.recordCountEvent(MainNewV2Activity.this, StatsManager.EVENT_ID_MODULE_ENTER, StatsManager.EVENT_TYPE_USER, "");
//            Intent intent = new Intent(MainNewV2Activity.this, UserActivity.class);
//            startActivity(intent);
            Intent intent = new Intent(this, UserSettingActivity.class);
            intent.putExtra("isUpdate", isUpdate);
            startActivity(intent);
        });

        fridge_control.setOnClickListener((view) -> {
            StatsManager.recordCountEvent(MainNewV2Activity.this, StatsManager.EVENT_ID_MODULE_ENTER, StatsManager.EVENT_TYPE_FRIDGE, "");
            Intent intent = new Intent(MainNewV2Activity.this, FridgeActivity.class);
            startActivity(intent);
        });

        album_layout.setOnClickListener((view) -> {
            StatsManager.recordCountEvent(MainNewV2Activity.this, StatsManager.EVENT_ID_MODULE_ENTER, StatsManager.EVENT_TYPE_PHOTO, "");
            Intent intent = new Intent(MainNewV2Activity.this, AlbumActivity.class);
            startActivity(intent);
        });

        weather.setOnClickListener((view) -> {
            StatsManager.recordCountEvent(MainNewV2Activity.this, StatsManager.EVENT_ID_MODULE_ENTER, StatsManager.EVENT_TYPE_WEATHER, "");
            Intent intent = new Intent(MainNewV2Activity.this, WeatherActivity.class);
            startActivityForResult(intent, REQUEST_WEATHER_CODE);
        });


        water_map.setOnClickListener(v -> {
            if (isConnectNetwork) {
                Intent intent = new Intent(MainNewV2Activity.this, CommonHeaderActivity.class);
                WebBaseData data = new WebBaseData();
                data.url = HttpConnect.WATERMAPURL;
                intent.putExtra(WebBaseData.Intent_String, data);
                startActivity(intent);
//                intent.putExtra("url", HttpConnect.WATERMAPURL);
//                intent.putExtra("title", "水质地图");
//                startActivity(intent);
//                Intent intent = new Intent(MainNewV2Activity.this, BrowserActivity.class);
//                intent.putExtra(AppConstants.WEB_URL, HttpConnect.WATERMAPURL);
//                startActivity(intent);
            } else {
                showConnectHint();
            }
        });

        cook_menu.setOnClickListener(v -> {
            if (isConnectNetwork) {
//                Intent intent = new Intent(MainNewV2Activity.this, CommonHeaderActivity.class);
//                intent.putExtra("url", HttpConnect.COOKMENUURL);
//                intent.putExtra("title", "健康菜谱");
//                startActivity(intent);
                Intent intent = new Intent(MainNewV2Activity.this, RecipesHealthyActivity.class);
                startActivity(intent);
            } else {
                showConnectHint();
            }
        });

        intelligence_link.setOnClickListener(v -> {
            if (MiotManager.getPeopleManager().getPeople() == null) {
                Toast.makeText(this, "请先扫描登录帐号体验", Toast.LENGTH_SHORT).show();
                return;
            }
            Intent intentConnect = new Intent(this, ConnectUnionActivity.class);
            startActivity(intentConnect);

//            Intent intent = new Intent(MainNewV2Activity.this, CommonHeaderActivity.class);
//                intent.putExtra("url", HttpConnect.COOKMENUURL);
//                intent.putExtra("title", "健康菜谱");
//                startActivity(intent);
        });

        music.setOnClickListener((view) -> {
            FileUtil.writeTxtToFile(mContext, AppConfig.EventKey.EVENT_ENTER_MUSIC, null);
//            StatsManager.recordCountEvent(MainNewV2Activity.this, StatsManager.EVENT_ID_MODULE_ENTER, StatsManager.EVENT_TYPE_MUSIC, "");
            String packageName = "com.tencent.qqmusicpad";
            if (ApkUtil.checkApkExist(ViomiApplication.getContext(), packageName)) {
                Log.e(TAG, "startOtherApp,packageName=" + packageName);
                boolean result = ApkUtil.startOtherApp(MainNewV2Activity.this, packageName, false);
                if (!result) {
                    ToastUtil.show("应用暂未安装");
                }
            }
        });

        video.setOnClickListener((view) -> {
            FileUtil.writeTxtToFile(mContext, AppConfig.EventKey.EVENT_ENTER_VIDEO, null);
//            StatsManager.recordCountEvent(MainNewV2Activity.this, StatsManager.EVENT_ID_MODULE_ENTER, StatsManager.EVENT_TYPE_VIDEO, "");
            String packageName = "com.qiyi.video.pad";
            if (ApkUtil.checkApkExist(ViomiApplication.getContext(), packageName)) {
                Log.e(TAG, "startOtherApp,packageName1=" + packageName);
                ApkUtil.startOtherApp(MainNewV2Activity.this, packageName, false);
            }
//            Intent intent = new Intent(MainNewV2Activity.this, VideoUMActivity.class);
//            startActivity(intent);
        });

        store_more.setOnClickListener(v -> {
            if (isConnectNetwork) {
                Intent intent = new Intent(MainNewV2Activity.this, VmallWebActivity.class);
                WebBaseData data = new WebBaseData();
                data.url = mVmallUrl + "/index.html";
                intent.putExtra("isMall",true);
                intent.putExtra(WebBaseData.Intent_String, data);
                startActivity(intent);
//                Intent intent = new Intent(MainNewV2Activity.this, BrowserActivity.class);
//                String url = mVmallUrl + "/index.html";
//                intent.putExtra(AppConstants.WEB_URL, url);
//                startActivity(intent);
            } else {
                showConnectHint();
            }
        });

        goods_viewPager.setOnViewPageChangeListener(new AutoScrollViewPager.OnViewPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                goods_smallpoint.setIndex(position % secondVPsize);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        food_manage.setOnClickListener(v -> {
            StatsManager.recordCountEvent(MainNewV2Activity.this, StatsManager.EVENT_ID_MODULE_ENTER, StatsManager.EVENT_TYPE_FOOD, "");
            Intent intent = new Intent(MainNewV2Activity.this, FoodManageActivity.class);
            startActivity(intent);
        });

        go_connect.setOnClickListener(v -> {
            disConnectHint();
            Intent intent = new Intent(this, WifiScanActivity.class);
            startActivity(intent);
        });

        connect_know_it.setOnClickListener(v -> {
            disConnectHint();
        });
    }

    private void showGuide() {
        ImageView imageView = new ImageView(this);
        imageView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        imageView.setImageResource(R.drawable.guide1);
        main_layout.addView(imageView);

        imageView.setOnClickListener(view -> {
            guideNo++;
            switch (guideNo) {
                case 1:
                    imageView.setImageResource(R.drawable.guide2);
                    break;
                case 2:
                    imageView.setImageResource(R.drawable.guide3);
                    break;
                case 3:
                    imageView.setImageResource(R.drawable.guide4);
                    break;
                case 4:
                    imageView.setImageResource(R.drawable.guide5);
                    break;
                case 5:
                    imageView.setImageResource(R.drawable.guide6);
                    break;
                case 6:
                    guideNo = 0;
                    main_layout.removeView(imageView);
                    break;
            }
        });
    }

    private void initView() {
        mContext = MainNewV2Activity.this;
        main_layout = (RelativeLayout) findViewById(R.id.main_layout);

        //首页头部
        myclockview = (MyClockView) findViewById(R.id.myclockview);

        weather = (RelativeLayout) findViewById(R.id.weather);
        weather_icon = (ImageView) findViewById(R.id.weather_icon);
        weather_pm25 = (TextView) findViewById(R.id.weather_pm25);
        weather_temp = (TextView) findViewById(R.id.weather_temp);

        store_btn = (ImageView) findViewById(R.id.store_btn);
        talk_btn = (ImageView) findViewById(R.id.talk_btn);
        timer_btn = (ImageView) findViewById(R.id.timer_btn);
        voice_btn = (ImageView) findViewById(R.id.voice_btn);
        network_btn = (ImageView) findViewById(R.id.network_btn);
        user_btn = (SimpleDraweeView) findViewById(R.id.user_btn);

        //首页中间
        fridge_control = (LinearLayout) findViewById(R.id.fridge_control);
        fridge_temp1 = (TextView) findViewById(R.id.fridge_temp1);
        fridge_tempName1 = (TextView) findViewById(R.id.fridge_tempName1);
        fridge_temp_room2 = (LinearLayout) findViewById(R.id.fridge_temp_room2);
        fridge_temp2 = (TextView) findViewById(R.id.fridge_temp2);
        fridge_tempName2 = (TextView) findViewById(R.id.fridge_tempName2);
        fridge_temp3 = (TextView) findViewById(R.id.fridge_temp3);
        fridge_tempName3 = (TextView) findViewById(R.id.fridge_tempName3);
        fridge_temp_line1 = findViewById(R.id.fridge_temp_line1);
        fridge_temp_line2 = findViewById(R.id.fridge_temp_line2);

        album_layout = (RelativeLayout) findViewById(R.id.album_layout);
        album_vp = (AutoScrollViewPagerIMG) findViewById(R.id.album_vp);

        mInfoView = (FrameLayout) findViewById(R.id.info_view);
        mInfoTips = (TextView) findViewById(R.id.info_tips);

        food_manage = (RelativeLayout) findViewById(R.id.food_manage);
        mNoFood = (TextView) findViewById(R.id.food_manage_no_content);
        mRecyclerView = (RecyclerView) findViewById(R.id.food_manage_content);

        //首页底部
        water_map = (LinearLayout) findViewById(R.id.water_map);
        music = (LinearLayout) findViewById(R.id.music);
        video = (LinearLayout) findViewById(R.id.video);
        cook_menu = (LinearLayout) findViewById(R.id.cook_menu);
        intelligence_link = (LinearLayout) findViewById(R.id.intelligence_link);

        store_more = (RelativeLayout) findViewById(R.id.store_more);
        goods_smallpoint = (SmallRadioView) findViewById(R.id.goods_smallpoint);
        goods_viewPager = (AutoScrollViewPager) findViewById(R.id.goods_viewPager);
        mJingdongMallView = (ImageView) findViewById(R.id.jingdongmall);

        //loading
        loading_layout = (RelativeLayout) findViewById(R.id.loading_layout);

        //音量
        volume_layout = (RelativeLayout) findViewById(R.id.volume_layout);
        volume_area_layout = (RelativeLayout) findViewById(R.id.volume_area_layout);
        soundBar = (SeekBar) findViewById(R.id.soundBar);

        //扫二维码活动
        qrcode_bg = (RelativeLayout) findViewById(R.id.qrcode_bg);
        qrcode_img = (SimpleDraweeView) findViewById(R.id.qrcode_img);
        qrcode_ok = (Button) findViewById(R.id.qrcode_ok);

        //联网提示
        connect_hint_layout = (LinearLayout) findViewById(R.id.connect_hint_layout);
        go_connect = (TextView) findViewById(R.id.go_connect);
        connect_know_it = (TextView) findViewById(R.id.connect_know_it);

        if (AppConfig.VIOMI_FRIDGE_V31.equals(DeviceConfig.MODEL)) {
            store_more.setVisibility(View.GONE);
            goods_viewPager.setVisibility(View.GONE);
            mJingdongMallView.setVisibility(View.VISIBLE);
        } else {
            store_more.setVisibility(View.VISIBLE);
            goods_viewPager.setVisibility(View.VISIBLE);
            mJingdongMallView.setVisibility(View.GONE);
        }
    }

    /**
     * 更新食材管理数据
     */
    private void updateFood(List<FoodDetailData> data) {
        if (data.size() == 0) {
            mNoFood.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
        } else {
            mList.clear();
            mList.addAll(data);
            mNoFood.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
            if (mList != null) {
                Collections.sort(mList, (o1, o2) -> {
                    if (o1.deadLine.equals("可选填")) return 0;
                    else if (o2.deadLine.equals("可选填")) return -1;
                    else if (Long.valueOf(o1.deadLine) < Long.valueOf(o2.deadLine)) return -1;
                    return 0;
                });
            }
            mAdapter.notifyDataSetChanged();
        }
    }

    private void onShowVoiceSetDialog() {
        if (mVoiceSettingDialog == null) {
            mVoiceSettingDialog = new VoiceSettingDialog(MainNewV2Activity.this, R.style.fidge_dialog);
            mVoiceSettingDialog.setCancelable(false);
        }
        mVoiceSettingDialog.show();
    }

    private void setdefault() {
        int[][] goodsImgs = new int[][]{{R.drawable.g00_6999_v1, R.drawable.g01_4999_v1, R.drawable.g02_3999_c1}, {R.drawable.g10_2999_c1, R.drawable.g11_2999_s1, R.drawable.g12_2499_s1}};
        String[][] goodsNames = new String[][]{{"V1 尊享版", "V1 乐享版", "C1 厨下版"}, {"C1 厨上版", "S1 优享版", "S1 标准版"}};
        String[][] goodsPrice = new String[][]{{"¥6999", "¥5999", "¥4499"}, {"¥3499", "¥2999", "¥2499"}};
        secondVPsize = goodsImgs.length;
        List<LinearLayout> defuatList = new ArrayList<>();
        for (int i = 0; i < goodsImgs.length * 2; i++) {
            LinearLayout linearLayout = new LinearLayout(this);
            linearLayout.setLayoutParams(new LinearLayout.LayoutParams((int) PhoneUtil.pxTodip(this, 314), (int) PhoneUtil.pxTodip(this, 114)));
            linearLayout.setOrientation(LinearLayout.HORIZONTAL);
            for (int j = 0; j < goodsImgs[i % goodsImgs.length].length; j++) {
                View view = LayoutInflater.from(this).inflate(R.layout.goods_viewpager_item, null);
                SimpleDraweeView goods_img = (SimpleDraweeView) view.findViewById(R.id.goods_img);
                goods_img.setOnClickListener(v -> {
                    showConnectHint();
                });
                TextView name = (TextView) view.findViewById(R.id.name);
                TextView price = (TextView) view.findViewById(R.id.price);
                goods_img.setImageResource(goodsImgs[i % goodsImgs.length][j]);
                goods_img.setScaleType(ImageView.ScaleType.CENTER_CROP);
                name.setText(goodsNames[i % goodsImgs.length][j]);
                price.setText(goodsPrice[i % goodsImgs.length][j]);
                linearLayout.addView(view);
            }
            defuatList.add(linearLayout);
        }
        goods_viewPager.setCustomAdapterDatas(defuatList);
        goods_viewPager.startAutoScroll();
        goods_smallpoint.setCount(secondVPsize);
        goods_smallpoint.setIndex(0);
    }

    @Override
    public void showProgressbar() {
        loading_layout.setVisibility(View.VISIBLE);
    }

    @Override
    public void dismissProgressbar() {
        loading_layout.setVisibility(View.GONE);
    }

    @Override
    public void updateHeaderIcon_talk(int resId) {
        talk_btn.setImageResource(resId);
    }

    @Override
    public void updateHeaderIcon_voice(boolean isSilent, boolean isBTConnected) {

        if (isBTConnected) {
            voice_btn.setImageResource(R.drawable.main_icon_volume_2_v2);
        } else {
            if (isSilent) {
                voice_btn.setImageResource(R.drawable.main_icon_volume_0_v2);
            } else {
                voice_btn.setImageResource(R.drawable.main_icon_volume_1_v2);
            }
        }
    }


    @Override
    public void updateIcon_blueTooth(boolean isopen) {

    }

    @Override
    public void updateIcon_user() {
        switch (AccountManager.getBindStatus()) {
            case AccountManager.BIND_NOT_YET: {
                log.myE(TAG, "BIND_NOT_YET");
                Uri uri = Uri.parse("res:///" + R.drawable.main_icon_user_v2);
                user_btn.setImageURI(uri);
                break;
            }
            case AccountManager.BIND_BY_VIOMI: {
                log.myE(TAG, "BIND_BY_VIOMI");
                Uri uridf = Uri.parse("res:///" + R.drawable.main_icon_user_ym_v2);
                user_btn.setImageURI(uridf);

                ViomiUser user = AccountManager.getViomiUser(this);
                if (user != null) {
                    Uri uri = Uri.parse(user.getHeadImg());
                    user_btn.setImageURI(uri);
                }
                break;
            }
            case AccountManager.BIND_BY_MIJIA: {
                log.myE(TAG, "BIND_BY_MIJIA");
                Uri uri = Uri.parse("res:///" + R.drawable.main_icon_user_mj_v2);
                user_btn.setImageURI(uri);
                break;
            }
            default:
                log.myE(TAG, "BIND_default");
                Uri uri = Uri.parse("res:///" + R.drawable.main_icon_user_v2);
                user_btn.setImageURI(uri);
                break;
        }
    }

    @Override
    public void updateIcon_update(boolean isReadyUpdate) {
        isUpdate = isReadyUpdate;
        if (isReadyUpdate) {
            user_btn.setBackgroundResource(R.drawable.update_red_hint);
        } else {
            user_btn.setBackgroundResource(0);
        }
    }

    @Override
    public void setSoundBarValue(int value) {
        soundBar.setProgress(value);
    }


    @Override
    public void updateFridgeStatus(FridgeTempBean bean) {
        fridge_temp1.setText(bean.getTemp1());
        fridge_temp2.setText(bean.getTemp2());
        fridge_tempName2.setText(bean.getTemp2_model());
        fridge_temp3.setText(bean.getTemp3());
    }

    @Override
    public void updateAlbum() {
        File saveDir = new File(Environment.getExternalStorageDirectory(), "viomi_album_show");
        List<Uri> ablumlist = new ArrayList<>();
        if (saveDir.exists()) {
            File[] files = saveDir.listFiles();
            for (int i = 0; i < files.length; i++) {
                ablumlist.add(Uri.fromFile(files[i]));
            }
        }

        if (ablumlist.size() == 0) {
            List<Integer> defualtList = new ArrayList<>();
            defualtList.add(R.drawable.iv1);
            defualtList.add(R.drawable.iv2);
            defualtList.add(R.drawable.iv3);
            defualtList.add(R.drawable.iv4);
            defualtList.add(R.drawable.iv5);
            defualtList.add(R.drawable.iv6);
            album_vp.setDefualtAdapterDatas(defualtList);
            album_vp.startAutoScroll();
            return;
        }
        album_vp.setCustomAdapterDatas(ablumlist);
        album_vp.startAutoScroll();
    }

    @Override
    public void updateMessage() {

    }

    @Override
    public void updateFoodList() {

    }

    @Override
    public void updateWeather(WeatherBean weather) {
        WeatherIconUtil.setWeatherIcon(weather_icon, weather.getWeather());
        weather_temp.setText(weather.getTempRange_s());
        weather_pm25.setText("PM2.5指数：" + weather.getPm25());
    }

    @Override
    public void updateFoodMenu(List<HomeWidgetBean> cooklist) {
        if (cooklist.size() > 0) {
            Uri uri = Uri.parse(cooklist.get(0).getImageUrl());
        }
    }

    @Override
    public void updateAdsRecommend(List<List<GoodsEntity>> adslist, int times) {
        if (times == 0) {
            return;
        }

        List<LinearLayout> goodsList = new ArrayList<>();

        secondVPsize = adslist.size();

        for (int i = 0; i < adslist.size() * times; i++) {

            LinearLayout linearLayout = new LinearLayout(this);
            linearLayout.setLayoutParams(new LinearLayout.LayoutParams((int) PhoneUtil.pxTodip(this, 314), (int) PhoneUtil.pxTodip(this, 144)));
            linearLayout.setOrientation(LinearLayout.HORIZONTAL);
            for (int j = 0; j < adslist.get(i % adslist.size()).size(); j++) {
                View view = LayoutInflater.from(this).inflate(R.layout.goods_viewpager_item, null);

                SimpleDraweeView goods_img = (SimpleDraweeView) view.findViewById(R.id.goods_img);
                TextView name = (TextView) view.findViewById(R.id.name);
                TextView price = (TextView) view.findViewById(R.id.price);

                Uri uri = Uri.parse(adslist.get(i % adslist.size()).get(j).getImageUrl());
                goods_img.setImageURI(uri);
                name.setText(adslist.get(i % adslist.size()).get(j).getName());
                price.setText("¥" + adslist.get(i % adslist.size()).get(j).getPrice());

                final int finalI = i % adslist.size();
                final int finalJ = j;
                view.setOnClickListener(v -> {
                    if (isConnectNetwork) {
                        if (adslist.size() > 0) {
                            Intent intent = new Intent(MainNewV2Activity.this, VmallWebActivity.class);
                            WebBaseData value = new WebBaseData();

                            switch (adslist.get(finalI).get(finalJ).getTargetType()) {
                                case 1:
                                    value.url = mVmallUrl + "/index.html?page=detail&spuId=" + adslist.get(finalI).get(finalJ).getTargetId();
                                    log.myE(TAG, "" + value.url);
                                    break;
                                case 2:
                                    value.url = mVmallUrl + "/index.html?page=detail&skuId=" + adslist.get(finalI).get(finalJ).getTargetId();
                                    log.myE(TAG, "" + value.url);
                                    break;
                                default:
                                    break;
                            }
                            StatsManager.recordCountEvent(MainNewV2Activity.this, StatsManager.EVENT_ID_MODULE_ENTER, StatsManager.EVENT_TYPE_VMALL_DETAIL, "vmall_detail_connect");
                            intent.putExtra(WebBaseData.Intent_String, value);
                            startActivity(intent);
                        }

                    } else {
                        showConnectHint();
                        StatsManager.recordCountEvent(MainNewV2Activity.this, StatsManager.EVENT_ID_MODULE_ENTER, StatsManager.EVENT_TYPE_VMALL_DETAIL, "vmall_detail_disconnect");
                    }
                });
                linearLayout.addView(view);
            }
            goodsList.add(linearLayout);
        }
        goods_viewPager.setCustomAdapterDatas(goodsList);
        goods_viewPager.startAutoScroll();
        goods_smallpoint.setCount(adslist.size());
        goods_smallpoint.setIndex(0);
    }

    @Override
    public void loationWeather() {
        MyLocationUitl.getInstance().startloc(getApplicationContext(), new AMapLocationListener() {
            @Override
            public void onLocationChanged(AMapLocation aMapLocation) {
                String cityName = aMapLocation.getCity() == null ? "" : ("null".equals(aMapLocation.getCity()) ? "" : aMapLocation.getCity());
                String cityCode = aMapLocation.getCityCode() == null ? "" : ("null".equals(aMapLocation.getCityCode()) ? "" : aMapLocation.getCityCode());

                GlobalParams.getInstance().setLocationCityCode(cityCode);
                GlobalParams.getInstance().setLocationCityName(cityName);
                log.myE(TAG, "onReceiveLocation---*---" + aMapLocation + ",cityName=" + cityName);

                SharedPreferences sp = getSharedPreferences("weather", MODE_PRIVATE);
                SharedPreferences.Editor edit = sp.edit();
                edit.putString("city_name", cityName);
                edit.commit();
                presenter.locationToWeather(cityName);
            }
        });
    }

    @Override
    public void refershInfoNew(String tips, int advertNews, int userNews, int deviceNews) {
        mAdvertNewsCount = advertNews;
        mUserNewsCount = userNews;
        mDeviceNewsCount = deviceNews;
        mInfoTips.setText(tips);
    }

    @Override
    public void showConnectHint() {
        connect_hint_layout.setVisibility(View.VISIBLE);
    }

    @Override
    public void disConnectHint() {
        connect_hint_layout.setVisibility(View.GONE);
    }

    private float y;

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        int action = ev.getAction();

        switch (action) {
            case MotionEvent.ACTION_DOWN:
                y = ev.getY();
                break;
            case MotionEvent.ACTION_UP:
                float y1 = ev.getY();

                if (y1 - y > 300) {
                    log.myE(TAG, "reflesh");
                    presenter.loadData();
                    loationWeather();
                    presenter.refershNewInfo();
                }
                break;
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_WEATHER_CODE && resultCode == RESPONE_WEATHER_CODE) {
            loationWeather();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mList == null || mList.size() == 0)
            sendBroadcast(new Intent(AppConfig.ACTION_FOOD_MANAGE_UPDATE));
    }

    @Override
    protected void onResume() {
        super.onResume();

        Intent jingdongintent = new Intent(BroadcastAction.ACTION_JINGDONG);
        sendBroadcast(jingdongintent);

        stopService(floatServiceIntent);

        presenter.refreshTalkStatus();
        presenter.refershNewInfo();
        String model = PhoneUtil.getDeviceModel();
        if (model != null) {
            DeviceConfig.MODEL = model;
        }

        int currentVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        setSoundBarValue(currentVolume);
        updateHeaderIcon_voice(currentVolume == 0, isBlueToothConnected);
        updateIcon_blueTooth(BleManager.getInstance().isBleEnable());
        updateIcon_user();
    }

    @Override
    protected void onPause() {
        super.onPause();
        startService(floatServiceIntent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detacthView();
        unregisterReceiver();
        stopService();
        MyLocationUitl.getInstance().stopLoc();
    }
}
