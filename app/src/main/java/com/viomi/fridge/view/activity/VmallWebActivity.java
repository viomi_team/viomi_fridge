package com.viomi.fridge.view.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.amap.api.location.AMapLocationClient;
import com.miot.api.MiotManager;
import com.miot.common.people.People;
import com.tencent.smtt.sdk.CookieSyncManager;
import com.viomi.fridge.R;
import com.viomi.fridge.common.GlobalParams;
import com.viomi.fridge.common.ViomiApplication;
import com.viomi.fridge.common.rxbus.BusEvent;
import com.viomi.fridge.common.rxbus.RxBus;
import com.viomi.fridge.device.AppConfig;
import com.viomi.fridge.device.AppConstants;
import com.viomi.fridge.manager.AccountManager;
import com.viomi.fridge.model.bean.QRCodeBase;
import com.viomi.fridge.model.bean.ViomiUser;
import com.viomi.fridge.model.bean.WebBaseData;
import com.viomi.fridge.util.FileUtil;
import com.viomi.fridge.util.LogUtils;
import com.viomi.fridge.util.ToastUtil;
import com.viomi.fridge.util.log;
import com.viomi.fridge.view.web.X5WebView;
import com.viomi.fridge.view.web.activity.BaseActivity;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.OnClick;
import rx.Subscription;

/**
 * 云米商城网页 Activity
 * Created by young2 on 2017/2/21.
 */
@SuppressLint("Registered")
public class VmallWebActivity extends WebActivity {
    private final static String TAG = "VmallWebActivity";
    private Object mJavaScriptInterface = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mJavaScriptInterface = new VmallWebActivity.VMallJavaScriptInterface();
        mWebView.addJavascriptInterface(mJavaScriptInterface, "H5ToNative");
    }

    private class VMallJavaScriptInterface {
        /***
         * 事件
         */
        @JavascriptInterface
        public void uploadBuryData(String key, String value) {
            if (!TextUtils.isEmpty(key) &&
                    !TextUtils.isEmpty(value)) {
                FileUtil.writeTxtToFile(VmallWebActivity.this, key, value);
            }
        }

        /**
         * 清除帐号信息
         */
        @JavascriptInterface
        public void onClearAcount() {
            LogUtils.d(TAG, "onClearAcount");
            AccountManager.deleteViomiUser(ViomiApplication.getContext());
        }

        /**
         * 打开新 H5 页面
         *
         * @param title 新页面标题
         * @param url   新页面链接
         */
        @JavascriptInterface
        public void onWebPageJump(String title, String url) {
            LogUtils.d(TAG, "onWebPageJump");
            Intent intent = new Intent(VmallWebActivity.this, VmallWebActivity.class);
            WebBaseData model = new WebBaseData();
            model.url = url;
            model.name = title;
            intent.putExtra(WebBaseData.Intent_String, model);
            startActivity(intent);
        }

        /**
         * 关闭当前页面，返回上一页面
         */
        @JavascriptInterface
        public void onWebPageReturn() {
            LogUtils.d(TAG, "onWebPageReturn");
            finish();
        }

        /**
         * 跳转到登陆页面
         */
        @JavascriptInterface
        public void onLoginPageJump() {
            LogUtils.d(TAG, "onLoginPageJump");
            Intent intent = new Intent(VmallWebActivity.this, ScanLoginActivity.class);
            startActivity(intent);
        }

        /**
         * 获取城市名称
         */
        @JavascriptInterface
        public String getCityName() {
            LogUtils.d(TAG, "getCityName");
            return GlobalParams.getInstance().getLocationCityName();
        }

        /**
         * 获取城市编码
         */
        @JavascriptInterface
        public String getCityCode() {
            LogUtils.d(TAG, "getCityCode");
            return GlobalParams.getInstance().getLocationCityCode();
        }

        /**
         * 获取用户信息
         * return 未登录，返回 null;已登陆返回 json 字符串
         */
        @JavascriptInterface
        public String getUserInfo() {
            LogUtils.d(TAG, "getUserInfo");
            ViomiUser viomiUser = AccountManager.getViomiUser(VmallWebActivity.this);
            if (viomiUser == null) {
                return null;
            }
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("account", viomiUser.getAccount());
                jsonObject.put("userCode", viomiUser.getUserCode());
                jsonObject.put("token", viomiUser.getToken());
                jsonObject.put("cid", viomiUser.getCid());
                log.myE(TAG, "调了getUserInfo-viomiUser---------" + jsonObject.toString());
                return jsonObject.toString();
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
        }

        // 编辑器关闭时调用，隐藏系统底部导航栏
        @JavascriptInterface
        public void hideBottomNavigation() {
            LogUtils.d(TAG, "hideBottomNavigation");
            runOnUiThread(() -> {
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
                int uiFlags = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                        | View.SYSTEM_UI_FLAG_FULLSCREEN; // hide status bar

                if (Build.VERSION.SDK_INT >= 19) {
                    uiFlags |= 0x00001000;    //SYSTEM_UI_FLAG_IMMERSIVE_STICKY: hide navigation bars - compatibility: building API level is lower thatn 19, use magic number directly for higher API target level
                } else {
                    uiFlags |= View.SYSTEM_UI_FLAG_LOW_PROFILE;
                }
                try {
                    getWindow().getDecorView().setSystemUiVisibility(uiFlags);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        }

        // 有数据更新是清除本地缓存，H5 调用
        @JavascriptInterface
        public void clearNativeCache() {
            LogUtils.d(TAG, "clearNativeCache");
//            runOnUiThread(() -> {
//                LogUtils.d(TAG, "clearNativeCache");
//                mWebView.clearCache(true);
//            });
            mWebView.clearCache(true);
        }

        // H5 加载失败后调用
        @JavascriptInterface
        public void loadFail() {
            LogUtils.d(TAG, "loadFail");
            runOnUiThread(() -> LogUtils.d(TAG, "loadFail"));
        }

        // 是否显示商城标题栏
        @JavascriptInterface
        public boolean isH5TilteBarShow() {
            return false;
        }
    }
}
