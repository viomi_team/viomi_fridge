package com.viomi.fridge.view.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.viomi.fridge.R;
import com.viomi.fridge.model.bean.recipe.RecipeSeries;
import com.viomi.fridge.model.bean.recipe.Secondlevel;
import com.viomi.fridge.model.bean.recipe.categoryInfo;
import com.viomi.fridge.mvp.presenter.RecipeSeriesPresenter;
import com.viomi.fridge.mvp.view.RecipeSeriesView;
import com.viomi.fridge.view.adapter.ViewPagerFragmentAdapter;
import com.viomi.fridge.view.fragment.MyFragment;
import com.viomi.fridge.view.widget.MyTabLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecipesSeriesActivity extends BaseActivity {

    @BindView(R.id.back_icon)
    public ImageView back_icon;
    @BindView(R.id.title_name)
    public TextView title_name;

    @BindView(R.id.tabLayout)
    public MyTabLayout tabLayout;

    @BindView(R.id.viewPager)
    public ViewPager viewPager;

    private ViewPagerFragmentAdapter viewPagerFragmentAdapter;
    private List<CharSequence> listTitle;
    private List<Fragment> listData;
    private String json, title;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_series);
        ButterKnife.bind(this);
        initViews();
    }

    private void initViews() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            json = bundle.getString("json", "");
            title = bundle.getString("title", "");
        }
//        presenter = new RecipeSeriesPresenter(this);
        if (!TextUtils.isEmpty(title)) {
            title_name.setText(title);
        }

        viewPagerFragmentAdapter = new ViewPagerFragmentAdapter(getSupportFragmentManager(), listData, listTitle);
        viewPager.setAdapter(viewPagerFragmentAdapter);
        tabLayout.setupWithViewPager(viewPager);

        back_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        bindDatas();
    }

    private void bindDatas() {
        if (!TextUtils.isEmpty(json)) {
            listTitle = new ArrayList<>();
            listData = new ArrayList<>();
            Secondlevel secondlevel = parseSecondlevel(json);
            if (secondlevel != null) {
                ArrayList<categoryInfo> cates = secondlevel.getChilds();
                if (cates != null && cates.size() != 0) {
                    for (categoryInfo entity : cates) {
                        listTitle.add(entity.getName());
                        listData.add(MyFragment.newInstance(entity.getCtgId()));
                    }

                    //刷新页面
                    if (viewPagerFragmentAdapter != null) {
                        viewPagerFragmentAdapter.setListTitle(listTitle);
                        viewPagerFragmentAdapter.setListData(listData);
                        viewPagerFragmentAdapter.notifyDataSetChanged();
                    }
                }
            }
        }
    }

    private Secondlevel parseSecondlevel(String json) {
        Secondlevel secondlevel = null;
        try {
            secondlevel = new Secondlevel();
            JSONObject object = new JSONObject(json);
            categoryInfo level = parseCategory(object.getJSONObject("categoryInfo"));
            JSONArray array = object.getJSONArray("childs");
            secondlevel.setCategoryInfo(level);

            ArrayList<categoryInfo> list1 = new ArrayList<>();
            for (int j = 0; j < array.length(); j++) {
                JSONObject object2 = (JSONObject) array.get(j);
                categoryInfo categoryInfo1 = parseCategory(object2.getJSONObject("categoryInfo"));
                list1.add(categoryInfo1);
            }
            secondlevel.setChilds(list1);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return secondlevel;
    }

    private categoryInfo parseCategory(JSONObject object) {
        String json = object.toString();
        return JSON.parseObject(json, categoryInfo.class);
    }
//    private void getCategory() {
//        HashMap<String, String> map = new HashMap<>();
//        map.put("key", getString(R.string.app_key));
//        map.put("cid", "0010001007");
//        map.put("page", "1");
//        map.put("size", "10");
//        presenter.getRecipeSeries(map);
//    }
}
