package com.viomi.fridge.view.activity;

import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.viomi.fridge.R;
import com.viomi.fridge.common_api.TimerApi;
import com.viomi.fridge.device.AppConfig;
import com.viomi.fridge.model.bean.RingBean;
import com.viomi.fridge.broadcast.TimerBroadcastReceiver;
import com.viomi.fridge.service.TimerService;
import com.viomi.fridge.util.FileUtil;
import com.viomi.fridge.util.log;
import com.viomi.fridge.view.adapter.RingAdapter;
import com.wx.wheelview.adapter.ArrayWheelAdapter;
import com.wx.wheelview.widget.WheelView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class Timer2Activity extends BaseActivity implements TimerApi.TimerCallback {

    private ImageView rightIcon;
    private WheelView hourWheelView;
    private WheelView minuteWheelView;
    private Button cancel;
    private Button alarm_ring;
    private Button start_timing;
    private TextView timing_text;
    private int hourStr;
    private int minStr;
    private int timer;
    private boolean isPuase;
    private boolean isTimerContinue;
    private Button model1;
    private Button model2;
    private Button model3;
    private Button model4;
    private Button model5;
    private Button model6;
    private RelativeLayout ring_select_layout;
    private TextView no;
    private TextView yes;
    private RelativeLayout ring_bg_layout;
    private ListView ringlist;
    private List<RingBean> list;
    private RingAdapter adapter;
    private SharedPreferences sp;
    private Intent serviceIntent;
    private ServiceConnection connection;
    private TimerService timerService;
    private TimerBroadcastReceiver receiver;
    private int currentTimer;
    private boolean isBindSuccess;
    private TimerApi timerApi;
    private MyHandler myHandler;
    private int voiceSetTime;
    private int voiceSetMode;

    private static class MyHandler extends Handler {

        private Timer2Activity mActivity;
        private WeakReference<Timer2Activity> weakReference;

        private MyHandler(Timer2Activity mActivity) {
            this.weakReference = new WeakReference<Timer2Activity>(mActivity);
            this.mActivity = weakReference.get();
        }

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    mActivity.timerStart();
                    break;
                case 2:
                    mActivity.timerStart(mActivity.voiceSetTime);
                    break;
                case 3:
                    mActivity.timerStart(mActivity.voiceSetTime, mActivity.voiceSetMode);
                    break;
                case 4:
                    mActivity.timerPause();
                    break;
                case 5:
                    mActivity.timerContinue();
                    break;
                case 6:
                    mActivity.timerCancel();
                    break;
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timer2);
        hourWheelView = (WheelView) findViewById(R.id.hour_wheel);
        minuteWheelView = (WheelView) findViewById(R.id.minute_wheel);
        timing_text = (TextView) findViewById(R.id.timing_text);

        cancel = (Button) findViewById(R.id.cancel);
        alarm_ring = (Button) findViewById(R.id.alarm_ring);
        start_timing = (Button) findViewById(R.id.start_timing);
        rightIcon = (ImageView) findViewById(R.id.RightIcon);

        model1 = (Button) findViewById(R.id.model1);
        model2 = (Button) findViewById(R.id.model2);
        model3 = (Button) findViewById(R.id.model3);
        model4 = (Button) findViewById(R.id.model4);
        model5 = (Button) findViewById(R.id.model5);
        model6 = (Button) findViewById(R.id.model6);

        ring_select_layout = (RelativeLayout) findViewById(R.id.ring_select_layout);
        ring_bg_layout = (RelativeLayout) findViewById(R.id.ring_bg_layout);
        ringlist = (ListView) findViewById(R.id.ringlist);
        no = (TextView) findViewById(R.id.no);
        yes = (TextView) findViewById(R.id.yes);

        Intent intent = getIntent();
        int voiceStartMode = intent.getIntExtra("startMode", -1);
        voiceSetTime = intent.getIntExtra("time", -1);
        voiceSetMode = intent.getIntExtra("mode", -1);

        myHandler = new MyHandler(this);
        Message msg = myHandler.obtainMessage();
        myHandler.sendEmptyMessageDelayed(voiceStartMode, 1000);
        FileUtil.writeTxtToFile(mContext, AppConfig.EventKey.EVENT_ENTER_TIMER, null);
        initWheelView();
        initlistener();
        init();
        initService();
        initBroadcast();

        timerApi = TimerApi.getInstance();
    }

    @Override
    protected void onResume() {
        super.onResume();
        timerApi.setCallback(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        timerApi.removeCallback();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //取消绑定服务
        unBindService();
        unregisterReceiver();
        FileUtil.writeTxtToFile(mContext, AppConfig.EventKey.EVENT_EXIT_TIMER, null);
        myHandler.removeCallbacksAndMessages(null);
    }

    private void initBroadcast() {
        IntentFilter filter = new IntentFilter();
        filter.addAction("timerservice_timerbroadcast");
        receiver = new TimerBroadcastReceiver() {
            @Override
            public void onReceiveCurrenttime(Intent intent) {
                currentTimer = intent.getIntExtra("currentTimer", 0);
                log.myE("TimerBroadcastReceiver", currentTimer + "");

                timing_text.setVisibility(View.VISIBLE);
                String s = secToTime(currentTimer);
                timing_text.setText(s);
                start_timing.setText("暂停");
                isPuase = true;

                if (currentTimer == 0) {
                    isPuase = false;
                    isTimerContinue = false;
                    start_timing.setText("时间到！");
                    start_timing.setEnabled(false);
                }
            }
        };
        registerReceiver(receiver, filter);
    }

    private void unregisterReceiver() {
        unregisterReceiver(receiver);
    }

    private void initService() {
        serviceIntent = new Intent(Timer2Activity.this, TimerService.class);
        connection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                timerService = ((TimerService.MyIBinder) service).getTimerService();
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                isBindSuccess = false;
            }
        };
        startService(serviceIntent);
        //绑定服务，销毁时需要解绑
        isBindSuccess = bindService(serviceIntent, connection, Service.BIND_AUTO_CREATE);
    }

    private void unBindService() {
        unbindService(connection);
    }

    private void init() {
        sp = getSharedPreferences("ring", MODE_PRIVATE);
        int selectPosition = sp.getInt("ringmodel", 0);

        String[] ringNames = new String[]{"铃声1", "铃声2", "铃声3", "铃声4", "铃声5"};
        list = new ArrayList<>();
        for (int i = 0; i < ringNames.length; i++) {
            RingBean bean;
            if (i == selectPosition) {
                bean = new RingBean(ringNames[i], true);
            } else {
                bean = new RingBean(ringNames[i], false);
            }
            list.add(bean);
        }
        adapter = new RingAdapter(list, this);
        ringlist.setAdapter(adapter);
    }

    private void initWheelView() {
        hourWheelView.setWheelData(createHours());
        hourWheelView.setWheelAdapter(new ArrayWheelAdapter(this));
        hourWheelView.setSkin(WheelView.Skin.None);
        hourWheelView.setLoop(true);
        hourWheelView.setWheelSize(5);
        WheelView.WheelViewStyle style = new WheelView.WheelViewStyle();
        style.selectedTextColor = Color.parseColor("#FDFDFD");
        style.textColor = Color.parseColor("#f1f1f1");
        style.textSize = 30;
        style.selectedTextSize = 40;
        style.backgroundColor = Color.parseColor("#00ffffff");
        style.holoBorderColor = Color.parseColor("#dbdbdb");
        hourWheelView.setStyle(style);
        hourWheelView.setExtraText("小时", Color.parseColor("#FFFFFF"), 20, 80);


        minuteWheelView.setWheelAdapter(new ArrayWheelAdapter(this));
        minuteWheelView.setSkin(WheelView.Skin.None);
        minuteWheelView.setWheelData(createMinutes());
        minuteWheelView.setLoop(true);
        minuteWheelView.setWheelSize(5);
        WheelView.WheelViewStyle style2 = new WheelView.WheelViewStyle();
        style2.selectedTextColor = Color.parseColor("#FDFDFD");
        style2.textColor = Color.parseColor("#f1f1f1");
        style2.textSize = 30;
        style2.selectedTextSize = 40;
        style2.backgroundColor = Color.parseColor("#00ffffff");
        style2.holoBorderColor = Color.parseColor("#dbdbdb");
        minuteWheelView.setStyle(style2);
        minuteWheelView.setExtraText("分钟", Color.parseColor("#FFFFFF"), 20, 80);

        hourWheelView.setOnWheelItemSelectedListener(new WheelView.OnWheelItemSelectedListener() {
            @Override
            public void onItemSelected(int position, Object o) {
                hourStr = Integer.parseInt((String) o);
                log.myE("onItemSelected", "" + position);
            }
        });
        minuteWheelView.setOnWheelItemSelectedListener(new WheelView.OnWheelItemSelectedListener() {
            @Override
            public void onItemSelected(int position, Object o) {
                minStr = Integer.parseInt((String) o);
            }
        });
    }


    private ArrayList<String> createHours() {
        ArrayList<String> list = new ArrayList<String>();
        for (int i = 0; i < 24; i++) {
            if (i < 10) {
                list.add("0" + i);
            } else {
                list.add("" + i);
            }
        }
        return list;
    }

    private ArrayList<String> createMinutes() {
        ArrayList<String> list = new ArrayList<String>();
        for (int i = 0; i < 60; i++) {
            if (i < 10) {
                list.add("0" + i);
            } else {
                list.add("" + i);
            }
        }
        return list;
    }


    private void setmodelstatus(String color, int bgDrawable, Button selectBtn, boolean isSelect, int status) {

        if (isSelect) {
            model1.setSelected(false);
            model2.setSelected(false);
            model3.setSelected(false);
            model4.setSelected(false);
            model5.setSelected(false);
            model6.setSelected(false);
            selectBtn.setSelected(true);

            switch (status) {
                case 1:
                    hourWheelView.setSelection(0);
                    minuteWheelView.setSelection(15);
                    break;
                case 2:
                    hourWheelView.setSelection(0);
                    minuteWheelView.setSelection(30);
                    break;
                case 3:
                    hourWheelView.setSelection(1);
                    minuteWheelView.setSelection(30);
                    break;
                case 4:
                    hourWheelView.setSelection(0);
                    minuteWheelView.setSelection(45);
                    break;
                case 5:
                    hourWheelView.setSelection(0);
                    minuteWheelView.setSelection(20);
                    break;
                case 6:
                    hourWheelView.setSelection(0);
                    minuteWheelView.setSelection(20);
                    break;
            }
        }
    }

    private void setSingleBtn(Button btn, int status) {

        if (btn.isSelected()) {
            setmodelstatus("#0bc3cd", R.drawable.timer_model_btn1, btn, false, status);
            btn.setSelected(false);
        } else {
            setmodelstatus("#999999", R.drawable.timer_model_btn2, btn, true, status);
        }
    }


    private void initlistener() {

        model1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSingleBtn(model1, 1);
            }
        });

        model2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSingleBtn(model2, 2);
            }
        });

        model3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSingleBtn(model3, 3);
            }
        });

        model4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSingleBtn(model4, 4);
            }
        });


        model5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSingleBtn(model5, 5);
            }
        });

        model6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSingleBtn(model6, 6);
            }
        });


        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timerCancel();
            }
        });

        alarm_ring.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ring_bg_layout.setVisibility(View.VISIBLE);
            }
        });

        start_timing.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                startTimerClick();
            }
        });


        rightIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        ring_bg_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ring_bg_layout.setVisibility(View.GONE);
            }
        });

        ring_select_layout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ring_bg_layout.setVisibility(View.GONE);
            }
        });

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ring_bg_layout.setVisibility(View.GONE);
                timerService.saveVoiceSetting();
            }
        });

        ringlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                for (int i = 0; i < list.size(); i++) {
                    list.get(i).setSelect(false);
                    if (i == position) {
                        list.get(i).setSelect(true);
                        timerService.playVioce(position);
                    }
                }
                adapter.notifyDataSetChanged();
            }
        });
    }


    private void startTimerClick() {
        if (isPuase) {
            timerPause();
        } else {
            if (isTimerContinue) {
                timerContinue();
            } else {
                timerStart();
            }
        }
    }


    //私有开始
    private void timerStart() {
        if (!isPuase) {
            if (!isTimerContinue) {
                timer = hourStr * 60 * 60 + minStr * 60;
                startOrContinue();
            }
        }
    }

    //私有开始
    private void timerStart(int time) {
        if (!isPuase) {
            if (!isTimerContinue) {
                timer = time * 60;
                startOrContinue();
            }
        }
    }

    //私有开始
    private void timerStart(int time, int mode) {
        if (mode == -1) {
            timerStart(time);
            return;
        }
        switch (mode) {
            case 1:
                setSingleBtn(model1, 1);
                time = 15;
                break;
            case 2:
                setSingleBtn(model2, 2);
                time = 30;
                break;
            case 3:
                setSingleBtn(model3, 3);
                time = 90;
                break;
            case 4:
                setSingleBtn(model4, 4);
                time = 45;
                break;
            case 5:
                setSingleBtn(model5, 5);
                time = 20;
                break;
            case 6:
                setSingleBtn(model6, 6);
                time = 20;
                break;
        }
        timerStart(time);
    }


    //私有继续
    private void timerContinue() {
        if (!isPuase) {
            if (isTimerContinue) {
                timer = currentTimer;

                startOrContinue();
            }
        }
    }

    //私有暂停
    private void timerPause() {
        if (isPuase) {
            isPuase = !isPuase;
            isTimerContinue = true;
            start_timing.setText("继续");
            timerService.puaseTimer();
        }
    }

    //私有取消
    private void timerCancel() {
        timerService.cancelTimer();
        start_timing.setEnabled(true);
        timing_text.setVisibility(View.GONE);
        start_timing.setText("开始");
        isPuase = false;
        isTimerContinue = false;
        FileUtil.writeTxtToFile(mContext, AppConfig.EventKey.EVENT_CANCEL_TIMER, null);
    }


    private void startOrContinue() {
        if (timer == 0) {
            Toast.makeText(Timer2Activity.this, "请设置时间！", Toast.LENGTH_SHORT).show();
            return;
        }
        FileUtil.writeTxtToFile(mContext, AppConfig.EventKey.EVENT_START_TIMER, null);
        isPuase = !isPuase;
        start_timing.setText("暂停");
        timerService.startTimer(timer);
    }


    private String secToTime(int time) {
        String timeStr = null;
        int hour = 0;
        int minute = 0;
        int second = 0;
        if (time <= 0)
            return "00:00";
        else {
            minute = time / 60;
            if (minute < 60) {
                second = time % 60;
                timeStr = unitFormat(minute) + ":" + unitFormat(second);
            } else {
                hour = minute / 60;
                if (hour > 99)
                    return "99:59:59";
                minute = minute % 60;
                second = time - hour * 3600 - minute * 60;
                timeStr = unitFormat(hour) + ":" + unitFormat(minute) + ":" + unitFormat(second);
            }
        }
        return timeStr;
    }

    public static String unitFormat(int i) {
        String retStr = null;
        if (i >= 0 && i < 10)
            retStr = "0" + Integer.toString(i);
        else
            retStr = "" + i;
        return retStr;
    }


    @Override
    public void start() {
        timerStart();
    }

    @Override
    public void start(int time) {
        timerStart(time);
    }

    @Override
    public void start(int time, int mode) {
        timerStart(time, mode);
    }

    @Override
    public void pause() {
        timerPause();
    }

    @Override
    public void continue_t() {
        timerContinue();
    }

    @Override
    public void cancel() {
        timerCancel();
    }
}
