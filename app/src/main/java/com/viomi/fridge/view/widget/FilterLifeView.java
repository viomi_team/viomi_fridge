package com.viomi.fridge.view.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

import com.viomi.fridge.R;

/**
 * Created by young2 on 2017/1/3.
 */

public class FilterLifeView extends RelativeLayout{

    public FilterLifeView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public FilterLifeView(Context context, AttributeSet attrs) {
       this(context,null,0);
    }

    public FilterLifeView(Context context) {
        this(context,null);
    }

    private void  init(Context context){
        View.inflate(context, R.layout.view_filter_life,this);
    }


}
