package com.viomi.fridge.view.activity;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationListener;
import com.facebook.drawee.view.SimpleDraweeView;
import com.viomi.fridge.R;
import com.viomi.fridge.albumhttp.AlbumActivity;
import com.viomi.fridge.albumhttp.CoreService;
import com.viomi.fridge.api.http.HttpConnect;
import com.viomi.fridge.broadcast.BroadcastAction;
import com.viomi.fridge.broadcast.ConnectionChangeReceiver;
import com.viomi.fridge.common.GlobalParams;
import com.viomi.fridge.common.ViomiApplication;
import com.viomi.fridge.device.AppConfig;
import com.viomi.fridge.device.DeviceConfig;
import com.viomi.fridge.manager.BleManager;
import com.viomi.fridge.manager.StatsManager;
import com.viomi.fridge.model.bean.FoodDetailData;
import com.viomi.fridge.model.bean.GoodsEntity;
import com.viomi.fridge.model.bean.HomeWidgetBean;
import com.viomi.fridge.model.bean.WebBaseData;
import com.viomi.fridge.mvp.bean.FridgeTempBean;
import com.viomi.fridge.mvp.bean.WeatherBean;
import com.viomi.fridge.mvp.presenter.NewMainPresenter;
import com.viomi.fridge.mvp.view.NewMain;
import com.viomi.fridge.service.BackgroudService;
import com.viomi.fridge.service.FoodManageService;
import com.viomi.fridge.service.InfoPushService;
import com.viomi.fridge.util.ApkUtil;
import com.viomi.fridge.util.PhoneUtil;
import com.viomi.fridge.util.SystemFileUtils;
import com.viomi.fridge.util.ToolUtil;
import com.viomi.fridge.util.WeatherIconUtil;
import com.viomi.fridge.util.log;
import com.viomi.fridge.view.adapter.FoodManageAdapter;
import com.viomi.fridge.view.widget.AutoScrollViewPager;
import com.viomi.fridge.view.widget.AutoScrollViewPagerIMG;
import com.viomi.fridge.view.widget.MyClockView;
import com.viomi.fridge.view.widget.RecyclerSpace;
import com.viomi.fridge.view.widget.SmallRadioView;
import com.viomi.fridge.view.widget.VoiceSettingDialog;
import com.viomi.fridge.view.widget.WrapContentGridLayoutManager;
import com.viomi.fridge.wifimodel.WifiScanActivity;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import viomi.com.gdloc.MyLocationUitl;

public class MainNewActivity extends BaseActivity implements NewMain {
    private NewMainPresenter presenter;
    private final static String TAG = "MainNewActivity";
    private String mVmallUrl = HttpConnect.STOREMAINURL;
    private CardView mInfoView;
    private TextView mInfoTips;
    private MyClockView myclockview;
    private ImageView store_btn;
    private ImageView talk_btn;
    private ImageView timer_btn;
    private ImageView voice_btn;
    private ImageView network_btn;
    private ImageView user_btn;
    private LinearLayout fridge_control;
    private TextView fridge_temp1;
    private TextView fridge_temp2;
    private TextView fridge_temp3;
    private TextView mNoFood;
    private RelativeLayout weather;
    private ImageView weather_icon;
    private TextView weather_staus;
    private TextView weather_temp;
    private TextView weather_pm25;
    private TextView weather_location;
    private LinearLayout music;
    private LinearLayout video;
    private RelativeLayout cook_menu;
    private SimpleDraweeView cook_menu_img;
    private SmallRadioView goods_smallpoint;
    private TextView store_more;
    private AutoScrollViewPager goods_viewPager;
    private RelativeLayout loading_layout;
    private RelativeLayout volume_layout;
    private RelativeLayout volume_area_layout;
    private RelativeLayout food_manage;
    private ImageView icon_ble;
    private SeekBar soundBar;
    private RelativeLayout qrcode_bg;
    private SimpleDraweeView qrcode_img;
    private Button qrcode_ok;
    private RecyclerView mRecyclerView;
    private FoodManageAdapter mAdapter;
    private int secondVPsize;
    private WifiManager wifiManager;
    private ConnectionChangeReceiver mReceiver;
    private BroadcastReceiver fileReceiver;
    private int[] wifiIcons;
    private List<FoodDetailData> mList = new ArrayList<>();
    private BroadcastReceiver mReceiver3;
    private Intent albumServiceIntent;
    private Intent floatServiceIntent;
    private Intent backgroudServiceintent;
    private Intent pushServiceintent;
    private static final int REQUEST_WEATHER_CODE = 1001;
    private static final int RESPONE_WEATHER_CODE = 1002;
    private final static int ALBUMREQUESTCODE = 1003;
    private final static int ALBUMREPONSECODE = 1004;
    private TextView fridge_temp2_model;
    private VoiceSettingDialog mVoiceSettingDialog;
    private AudioManager mAudioManager;
    private BroadcastReceiver volumeReceiver;
    private RelativeLayout album_layout;
    private SimpleDraweeView album_iv;
    private AutoScrollViewPagerIMG album_vp;
    private BroadcastReceiver albumUpdateReceiver;
    private BroadcastReceiver mFoodManageReceiver;
    private BroadcastReceiver mLocationReceiver;
    private Intent foodServiceIntent;
    private int mAdvertNewsCount, mUserNewsCount, mDeviceNewsCount;
    private RelativeLayout main_view;
    private LinearLayout connect_hint_layout;
    private TextView go_connect;
    private TextView connect_know_it;
    private LinearLayout fridge_change_room;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_new);

        initView();
        init();
        initListener();
        setdefault();
        registerReceiver();
        initService();

        presenter = new NewMainPresenter();
        presenter.attacthView(this);
        presenter.refresh10Seconds();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mList == null || mList.size() == 0)
            sendBroadcast(new Intent(AppConfig.ACTION_FOOD_MANAGE_UPDATE));
    }


    private void init() {
        String model = PhoneUtil.getDeviceModel();
        log.d(TAG, "model=" + model);
        if (model != null) {
            DeviceConfig.MODEL = model;
        }

        //每次系统重启要写入红外感应
        SystemFileUtils.HumanSensorType sensorType;
        SystemFileUtils.HumanSensorPathEnum pathEnum;
        boolean humanSensorSwitch = GlobalParams.getInstance().isHumanSensorSwitch();
        if (humanSensorSwitch) {
            sensorType = SystemFileUtils.HumanSensorType.open;
        } else {
            sensorType = SystemFileUtils.HumanSensorType.close;
        }

        switch (DeviceConfig.MODEL) {
            // 小鲜互联 云米智能冰箱三门 绿联主控
            case AppConfig.VIOMI_FRIDGE_V1:
                pathEnum = SystemFileUtils.HumanSensorPathEnum.DOOR3;
                break;

            // 小鲜互联 云米智能冰箱四门 双鹿主控
            case AppConfig.VIOMI_FRIDGE_V2:
                pathEnum = SystemFileUtils.HumanSensorPathEnum.DOOR4;
                break;

            // 云米462大屏金属门冰箱
            //隐藏变温室
            case AppConfig.VIOMI_FRIDGE_V3:
            case AppConfig.VIOMI_FRIDGE_V31:
                pathEnum = SystemFileUtils.HumanSensorPathEnum.DOOR462;
                fridge_change_room.setVisibility(View.GONE);
                break;

            // 云米455大屏玻璃门冰箱
            //隐藏变温室
            case AppConfig.VIOMI_FRIDGE_V4:
                pathEnum = SystemFileUtils.HumanSensorPathEnum.DOOR455;
                fridge_change_room.setVisibility(View.GONE);
                break;
            default:
                pathEnum = SystemFileUtils.HumanSensorPathEnum.DOOR4;
                break;
        }
        SystemFileUtils.setHumanSensor(sensorType, pathEnum);
        GlobalParams.getInstance().setHumanSensorSwitch(humanSensorSwitch);

        //音量相关
        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        //最大音量
        int maxVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_SYSTEM);
        soundBar.setMax(maxVolume);

        wifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
        wifiManager.setWifiEnabled(true);

        updateAlbum();
    }


    //定位 网络异常时无法获取定位
    @Override
    public void loationWeather() {

        MyLocationUitl.getInstance().startloc(getApplicationContext(), new AMapLocationListener() {
            @Override
            public void onLocationChanged(AMapLocation aMapLocation) {
                String cityName = aMapLocation.getCity() == null ? "" : ("null".equals(aMapLocation.getCity()) ? "" : aMapLocation.getCity());
                String cityCode = aMapLocation.getCityCode() == null ? "" : ("null".equals(aMapLocation.getCityCode()) ? "" : aMapLocation.getCityCode());

                GlobalParams.getInstance().setLocationCityCode(cityCode);
                GlobalParams.getInstance().setLocationCityName(cityName);
                log.myE(TAG, "onReceiveLocation---*---" + aMapLocation + ",cityName=" + cityName);

                SharedPreferences sp = getSharedPreferences("weather", MODE_PRIVATE);
                SharedPreferences.Editor edit = sp.edit();
                edit.putString("city_name", cityName);
                edit.commit();
                presenter.locationToWeather(cityName);
            }
        });
    }

    @Override
    public void refershInfoNew(String tips, int advertNews, int userNews, int deviceNews) {
        mAdvertNewsCount = advertNews;
        mUserNewsCount = userNews;
        mDeviceNewsCount = deviceNews;
        mInfoTips.setText(tips);
    }

    @Override
    public void showConnectHint() {
        connect_hint_layout.setVisibility(View.VISIBLE);
    }

    @Override
    public void disConnectHint() {
        connect_hint_layout.setVisibility(View.GONE);
    }

    private void registerReceiver() {
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        filter.addAction(Intent.ACTION_TIME_CHANGED);
        mReceiver = new ConnectionChangeReceiver() {
            @Override
            public void hasNoNetwork() {
                network_btn.setImageResource(R.drawable.wifi_iron0);
            }

            @Override
            public void hasNetwork() {
                log.myE(TAG, "hasNetwork");
                presenter.loadData();
                loationWeather();
            }
        };

        IntentFilter filter2 = new IntentFilter("viomi_file_completion");
        fileReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals("viomi_file_completion")) {
                    final Dialog albumDialog = new Dialog(MainNewActivity.this, R.style.fidge_dialog);
                    albumDialog.getWindow().setType((WindowManager.LayoutParams.TYPE_SYSTEM_ALERT));
                    View dialog_view = LayoutInflater.from(MainNewActivity.this).inflate(R.layout.album_dialog_layout, null);
                    albumDialog.setContentView(dialog_view);
                    TextView no_thanks = (TextView) dialog_view.findViewById(R.id.no_thanks);
                    TextView ok_do = (TextView) dialog_view.findViewById(R.id.ok_do);
                    no_thanks.setOnClickListener(v -> albumDialog.dismiss());
                    ok_do.setOnClickListener(v -> {
                        albumDialog.dismiss();
                        Intent intent1 = new Intent(MainNewActivity.this, AlbumActivity.class);
                        startActivity(intent1);
                    });
                    albumDialog.show();
                }
            }
        };

        wifiIcons = new int[]{R.drawable.wifi_iron1, R.drawable.wifi_iron2, R.drawable.wifi_iron3};
        IntentFilter filter3 = new IntentFilter(WifiManager.RSSI_CHANGED_ACTION);
        mReceiver3 = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(WifiManager.RSSI_CHANGED_ACTION)) {
                    WifiInfo info = wifiManager.getConnectionInfo();
                    if (info != null) {
                        network_btn.setImageResource(wifiIcons[WifiManager.calculateSignalLevel(info.getRssi(), wifiIcons.length)]);
                    } else {
                        network_btn.setImageResource(R.drawable.wifi_iron0);
                    }

                }
            }
        };

        IntentFilter filter4 = new IntentFilter("android.media.VOLUME_CHANGED_ACTION");
        volumeReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals("android.media.VOLUME_CHANGED_ACTION")) {
                    int currentVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
                    setSoundBarValue(currentVolume);
                }
            }
        };

        IntentFilter filter5 = new IntentFilter("UPDATE_ALBUM");
        albumUpdateReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals("UPDATE_ALBUM")) {
                    updateAlbum();
                }
            }
        };

        /*
         * modify by William
         */
        IntentFilter filter6 = new IntentFilter();
        filter6.addAction(Intent.ACTION_TIME_TICK);
        filter6.addAction(AppConfig.ACTION_FOOD_MANAGE_SEND_DATA);
        mFoodManageReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(Intent.ACTION_TIME_TICK)) {
                    if (ToolUtil.isServiceWork(MainNewActivity.this, "com.viomi.fridge.service.FoodManageService")) {
                        log.d(TAG, "FoodManageService is Running");
                    } else {
                        startService(new Intent(MainNewActivity.this, FoodManageService.class));
                        log.d(TAG, "FoodManageService is not Running");
                    }
                } else if (intent.getAction().equals(AppConfig.ACTION_FOOD_MANAGE_SEND_DATA)) {    // 更新食材
                    log.d(TAG, "get data success");
                    List<FoodDetailData> data = (List<FoodDetailData>) intent.getSerializableExtra("food_list");
                    updateFood(data);
                }
            }
        };

        IntentFilter locationFilter = new IntentFilter();
        locationFilter.addAction(BroadcastAction.ACTION_WEATHER_GET);
        locationFilter.addAction(BroadcastAction.ACTION_PUSH_MESSAGE);
        mLocationReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(BroadcastAction.ACTION_WEATHER_GET)) {
                    String result = intent.getStringExtra("result");
                    presenter.referWeather(result);
                } else if (intent.getAction().equals(BroadcastAction.ACTION_PUSH_MESSAGE)) {
                    presenter.refershNewInfo();
                }
            }
        };
        LocalBroadcastManager.getInstance(ViomiApplication.getContext()).registerReceiver(mLocationReceiver, locationFilter);

        registerReceiver(mReceiver, filter);
        registerReceiver(fileReceiver, filter2);
        registerReceiver(mReceiver3, filter3);
        registerReceiver(volumeReceiver, filter4);
        registerReceiver(albumUpdateReceiver, filter5);
        registerReceiver(mFoodManageReceiver, filter6);

    }


    private void unregisterReceiver() {
        unregisterReceiver(mReceiver);
        unregisterReceiver(fileReceiver);
        unregisterReceiver(mReceiver3);
        unregisterReceiver(volumeReceiver);
        unregisterReceiver(albumUpdateReceiver);
        unregisterReceiver(mFoodManageReceiver);
        LocalBroadcastManager.getInstance(ViomiApplication.getContext()).unregisterReceiver(mLocationReceiver);
    }

    private void initService() {
        backgroudServiceintent = new Intent(this, BackgroudService.class);
        startService(backgroudServiceintent);

        albumServiceIntent = new Intent(this, CoreService.class);
        startService(albumServiceIntent);

//        floatServiceIntent = new Intent(this, FloatButtonService.class);

        foodServiceIntent = new Intent(MainNewActivity.this, FoodManageService.class);
        startService(foodServiceIntent);

        pushServiceintent = new Intent(MainNewActivity.this, InfoPushService.class);
        startService(pushServiceintent);
    }

    private void stopService() {
        stopService(backgroudServiceintent);
        stopService(albumServiceIntent);
        stopService(foodServiceIntent);
        stopService(pushServiceintent);
    }

    private void initView() {

        main_view = (RelativeLayout) findViewById(R.id.main_view);

        myclockview = (MyClockView) findViewById(R.id.myclockview);
        store_btn = (ImageView) findViewById(R.id.store_btn);
        talk_btn = (ImageView) findViewById(R.id.talk_btn);
        timer_btn = (ImageView) findViewById(R.id.timer_btn);
        voice_btn = (ImageView) findViewById(R.id.voice_btn);
        network_btn = (ImageView) findViewById(R.id.network_btn);
        user_btn = (ImageView) findViewById(R.id.user_btn);

        fridge_control = (LinearLayout) findViewById(R.id.fridge_control);
        fridge_temp1 = (TextView) findViewById(R.id.fridge_temp1);
        fridge_temp2 = (TextView) findViewById(R.id.fridge_temp2);
        fridge_temp2_model = (TextView) findViewById(R.id.fridge_temp2_model);
        fridge_temp3 = (TextView) findViewById(R.id.fridge_temp3);
        fridge_change_room = (LinearLayout) findViewById(R.id.fridge_change_room);

        album_layout = (RelativeLayout) findViewById(R.id.album_layout);
        album_vp = (AutoScrollViewPagerIMG) findViewById(R.id.album_vp);


        weather = (RelativeLayout) findViewById(R.id.weather);
        weather_icon = (ImageView) findViewById(R.id.weather_icon);
        weather_staus = (TextView) findViewById(R.id.weather_staus);
        weather_temp = (TextView) findViewById(R.id.weather_temp);
        weather_pm25 = (TextView) findViewById(R.id.weather_pm25);
        weather_location = (TextView) findViewById(R.id.weather_location);

        music = (LinearLayout) findViewById(R.id.music);
        video = (LinearLayout) findViewById(R.id.video);

        cook_menu = (RelativeLayout) findViewById(R.id.cook_menu);
        cook_menu_img = (SimpleDraweeView) findViewById(R.id.cook_menu_img);

        goods_smallpoint = (SmallRadioView) findViewById(R.id.goods_smallpoint);
        store_more = (TextView) findViewById(R.id.store_more);
        goods_viewPager = (AutoScrollViewPager) findViewById(R.id.goods_viewPager);

        loading_layout = (RelativeLayout) findViewById(R.id.loading_layout);

        volume_layout = (RelativeLayout) findViewById(R.id.volume_layout);
        volume_area_layout = (RelativeLayout) findViewById(R.id.volume_area_layout);
//        icon_ble = (ImageView) findViewById(R.id.icon_ble);
        soundBar = (SeekBar) findViewById(R.id.soundBar);

        qrcode_bg = (RelativeLayout) findViewById(R.id.qrcode_bg);
        qrcode_img = (SimpleDraweeView) findViewById(R.id.qrcode_img);
        qrcode_ok = (Button) findViewById(R.id.qrcode_ok);

        mInfoView = (CardView) findViewById(R.id.info_view);
        mInfoTips = (TextView) findViewById(R.id.info_tips);

        food_manage = (RelativeLayout) findViewById(R.id.food_manage);
        mNoFood = (TextView) findViewById(R.id.food_manage_no_content);
        mRecyclerView = (RecyclerView) findViewById(R.id.food_manage_content);
        WrapContentGridLayoutManager gridLayoutManager = new WrapContentGridLayoutManager(this, 2);
        mRecyclerView.setLayoutManager(gridLayoutManager);
        RecyclerSpace space = new RecyclerSpace(4, 0, 0);   // 设置分割线
        mRecyclerView.addItemDecoration(space);
        mAdapter = new FoodManageAdapter(this, mList, true);
        mRecyclerView.setAdapter(mAdapter);

        connect_hint_layout = (LinearLayout) findViewById(R.id.connect_hint_layout);
        go_connect = (TextView) findViewById(R.id.go_connect);
        connect_know_it = (TextView) findViewById(R.id.connect_know_it);
    }

    /**
     * 更新食材管理数据
     */
    private void updateFood(List<FoodDetailData> data) {
        if (data.size() == 0) {
            mNoFood.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
        } else {
            mList.clear();
            mList.addAll(data);
            mNoFood.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
            if (mList != null) {
                Collections.sort(mList, (o1, o2) -> {
                    if (o1.deadLine.equals("可选填")) return 0;
                    else if (o2.deadLine.equals("可选填")) return -1;
                    else if (Long.valueOf(o1.deadLine) < Long.valueOf(o2.deadLine)) return -1;
                    return 0;
                });
            }
            mAdapter.notifyDataSetChanged();
        }
    }


    private void initListener() {
        myclockview.setOnClickListener((view) -> {
            Intent intent = new Intent(MainNewActivity.this, MainNewV2Activity.class);
            startActivity(intent);
        });

        store_btn.setOnClickListener((view) -> {

            StatsManager.recordCountEvent(this, StatsManager.EVENT_ID_MODULE_ENTER, StatsManager.EVENT_TYPE_VMALL_MAIN, "vamll_icon");
            Intent intent = new Intent(MainNewActivity.this, VmallWebActivity.class);
            WebBaseData data = new WebBaseData();
            data.url = mVmallUrl + "/index.html";
            intent.putExtra(WebBaseData.Intent_String, data);
            startActivity(intent);
        });

        mInfoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StatsManager.recordCountEvent(MainNewActivity.this, StatsManager.EVENT_ID_MODULE_ENTER, StatsManager.EVENT_TYPE_INFORM, "");
                Intent intent = new Intent(MainNewActivity.this, InfoCenterActivity.class);
                intent.putExtra("advert", mAdvertNewsCount);
                intent.putExtra("device", mDeviceNewsCount);
                intent.putExtra("user", mUserNewsCount);
                startActivity(intent);
            }
        });

        talk_btn.setOnClickListener((view) -> {
//            VoiceManager.getInstance().textUnderstandertTest("我要看西游记");
//            Intent intent=new Intent(MainNewActivity.this,MusicPlayActivity.class);
//            startActivity(intent);
//            int room=0;
//            String name="苹果";
//            DeviceInfoMessage deviceInfoMessage = new DeviceInfoMessage();
//            String roomStr = "";
//            if (room == 0) {
//                roomStr = ViomiApplication.getContext().getString(R.string.text_cold_closet);
//            } else if (room == 1) {
//                roomStr = ViomiApplication.getContext().getString(R.string.text_temp_changeable_room);
//            } else if (room == 2) {
//                roomStr = ViomiApplication.getContext().getString(R.string.text_freezing_room);
//            }
//            deviceInfoMessage.setTitle(name + ViomiApplication.getContext().getString(R.string.text_food_expend));
//            deviceInfoMessage.setContent(ViomiApplication.getContext().getString(R.string.text_food_expend_desc) + roomStr);
//            deviceInfoMessage.setInfoId(InfoManager.getInstance().getLastDeviceInfoId() + 1);
//            deviceInfoMessage.setTopic(PushMsg.PUSH_MESSAGE_TYPE_FOOD);
//            deviceInfoMessage.setTime(System.currentTimeMillis() / 1000);
//            deviceInfoMessage.setDelete(false);
//            deviceInfoMessage.setRead(false);
//            InfoManager.getInstance().addDeviceInfoRecord(deviceInfoMessage);
//
//            PushMsg pushMsg = new PushMsg();
//            pushMsg.title = name + ViomiApplication.getContext().getString(R.string.text_food_expend);
//            pushMsg.content = ViomiApplication.getContext().getString(R.string.text_food_expend_info_desc);
//            pushMsg.type=PushMsg.PUSH_MESSAGE_TYPE_FOOD;
//            PushManager.getInstance().pushNotificationToStatusBar(pushMsg);
            StatsManager.recordCountEvent(MainNewActivity.this, StatsManager.EVENT_ID_MODULE_ENTER, StatsManager.EVENT_TYPE_VOICE, "");
            onShowVoiceSetDialog();
        });

        timer_btn.setOnClickListener((view) -> {
            StatsManager.recordCountEvent(MainNewActivity.this, StatsManager.EVENT_ID_MODULE_ENTER, StatsManager.EVENT_TYPE_TIMER, "");
            Intent intent = new Intent(MainNewActivity.this, Timer2Activity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        });

        voice_btn.setOnClickListener((view) -> {
            StatsManager.recordCountEvent(MainNewActivity.this, StatsManager.EVENT_ID_MODULE_ENTER, StatsManager.EVENT_TYPE_VOLUME, "");
            volume_layout.setVisibility(View.VISIBLE);
            int currentVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_SYSTEM);
            setSoundBarValue(currentVolume);
            updateIcon_blueTooth(BleManager.getInstance().isBleEnable());
        });

        icon_ble.setOnClickListener((view) -> {
            StatsManager.recordCountEvent(MainNewActivity.this, StatsManager.EVENT_ID_MODULE_ENTER, StatsManager.EVENT_TYPE_BLE, "");
            Intent intent = new Intent(Settings.ACTION_BLUETOOTH_SETTINGS);
            startActivity(intent);
        });

        volume_layout.setOnClickListener((view) -> {
            volume_layout.setVisibility(View.GONE);

        });

        volume_area_layout.setOnTouchListener((v, event) -> {
            return true;
        });

        soundBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mAudioManager.setStreamVolume(AudioManager.STREAM_SYSTEM, progress, 0);
                mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, progress, 0);
                updateHeaderIcon_voice(progress == 0,false);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        network_btn.setOnClickListener((view) -> {
            StatsManager.recordCountEvent(MainNewActivity.this, StatsManager.EVENT_ID_MODULE_ENTER, StatsManager.EVENT_TYPE_WIFI, "");
            Intent intent = new Intent(MainNewActivity.this, WifiScanActivity.class);
            startActivity(intent);
        });

        user_btn.setOnClickListener((view) -> {
            StatsManager.recordCountEvent(MainNewActivity.this, StatsManager.EVENT_ID_MODULE_ENTER, StatsManager.EVENT_TYPE_USER, "");
            Intent intent = new Intent(MainNewActivity.this, UserActivity.class);
            startActivity(intent);
        });

        fridge_control.setOnClickListener((view) -> {
            StatsManager.recordCountEvent(MainNewActivity.this, StatsManager.EVENT_ID_MODULE_ENTER, StatsManager.EVENT_TYPE_FRIDGE, "");
            Intent intent = new Intent(MainNewActivity.this, FridgeActivity.class);
            startActivity(intent);
        });

        album_layout.setOnClickListener((view) -> {
            StatsManager.recordCountEvent(MainNewActivity.this, StatsManager.EVENT_ID_MODULE_ENTER, StatsManager.EVENT_TYPE_PHOTO, "");
            Intent intent = new Intent(MainNewActivity.this, AlbumActivity.class);
            startActivity(intent);
        });

        weather.setOnClickListener((view) -> {
            StatsManager.recordCountEvent(MainNewActivity.this, StatsManager.EVENT_ID_MODULE_ENTER, StatsManager.EVENT_TYPE_WEATHER, "");
            Intent intent = new Intent(MainNewActivity.this, WeatherActivity.class);
            startActivityForResult(intent, REQUEST_WEATHER_CODE);
        });


        music.setOnClickListener((view) -> {
            StatsManager.recordCountEvent(MainNewActivity.this, StatsManager.EVENT_ID_MODULE_ENTER, StatsManager.EVENT_TYPE_MUSIC, "");
            String packageName = "com.tencent.qqmusicpad";
            if (ApkUtil.checkApkExist(ViomiApplication.getContext(), packageName)) {
                Log.e(TAG, "startOtherApp,packageName=" + packageName);
                ApkUtil.startOtherApp(MainNewActivity.this, packageName, false);
            }
        });

        video.setOnClickListener((view) -> {
            StatsManager.recordCountEvent(MainNewActivity.this, StatsManager.EVENT_ID_MODULE_ENTER, StatsManager.EVENT_TYPE_VIDEO, "");
            String packageName = "com.qiyi.video.pad";
            if (ApkUtil.checkApkExist(ViomiApplication.getContext(), packageName)) {
                Log.e(TAG, "startOtherApp,packageName1=" + packageName);
                ApkUtil.startOtherApp(MainNewActivity.this, packageName, false);
            }
//            Intent intent = new Intent(MainNewActivity.this, VideoUMActivity.class);
//            startActivity(intent);
        });

        cook_menu_img.setOnClickListener(v -> {
            showConnectHint();
        });

        store_more.setOnClickListener((view) -> {
            StatsManager.recordCountEvent(MainNewActivity.this, StatsManager.EVENT_ID_MODULE_ENTER, StatsManager.EVENT_TYPE_VMALL_MAIN, "vmall_more");
            Intent intent = new Intent(MainNewActivity.this, VmallWebActivity.class);
            WebBaseData data = new WebBaseData();
            data.url = mVmallUrl + "/index.html";
            intent.putExtra(WebBaseData.Intent_String, data);
            startActivity(intent);

        });


        goods_viewPager.setOnViewPageChangeListener(new AutoScrollViewPager.OnViewPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                goods_smallpoint.setIndex(position % secondVPsize);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        food_manage.setOnClickListener(v -> {
            StatsManager.recordCountEvent(MainNewActivity.this, StatsManager.EVENT_ID_MODULE_ENTER, StatsManager.EVENT_TYPE_FOOD, "");
            Intent intent = new Intent(MainNewActivity.this, FoodManageActivity.class);
            startActivity(intent);
        });

        go_connect.setOnClickListener(v -> {
            disConnectHint();
            Intent intent = new Intent(this, WifiScanActivity.class);
            startActivity(intent);
        });

        connect_know_it.setOnClickListener(v -> {
            disConnectHint();
        });
    }


    private void onShowVoiceSetDialog() {
        if (mVoiceSettingDialog == null) {
            mVoiceSettingDialog = new VoiceSettingDialog(MainNewActivity.this, R.style.fidge_dialog);
            mVoiceSettingDialog.setCancelable(false);
        }
        mVoiceSettingDialog.show();
    }

    private float y;

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        int action = ev.getAction();

        switch (action) {
            case MotionEvent.ACTION_DOWN:
                y = ev.getY();
                break;
            case MotionEvent.ACTION_UP:
                float y1 = ev.getY();

                if (y1 - y > 300) {
                    log.myE(TAG, "reflesh");
                    presenter.loadData();
                    loationWeather();
                    presenter.refershNewInfo();
                }
                break;
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_WEATHER_CODE && resultCode == RESPONE_WEATHER_CODE) {
            loationWeather();
        }
    }

    @Override
    protected void onResume() {
//        stopService(floatServiceIntent);
        log.myE(TAG, "-------------------onResume");
        super.onResume();
        presenter.refreshTalkStatus();
        presenter.refershNewInfo();
        String model = PhoneUtil.getDeviceModel();
        if (model != null) {
            DeviceConfig.MODEL = model;
        }

        int currentVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        setSoundBarValue(currentVolume);
        updateHeaderIcon_voice(currentVolume == 0,false);
        updateIcon_blueTooth(BleManager.getInstance().isBleEnable());

    }

    @Override
    protected void onPause() {
//        startService(floatServiceIntent);
        log.myE(TAG, "-------------------onPause");
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detacthView();
        unregisterReceiver();
        stopService();
        MyLocationUitl.getInstance().stopLoc();
    }

    @Override
    public void showProgressbar() {
        loading_layout.setVisibility(View.VISIBLE);
    }

    @Override
    public void dismissProgressbar() {
        loading_layout.setVisibility(View.GONE);
    }

    @Override
    public void updateHeaderIcon_talk(int resId) {
        talk_btn.setImageResource(resId);
    }

    @Override
    public void updateHeaderIcon_voice(boolean isSilent, boolean isBTConnected) {
        if (isSilent) {
            voice_btn.setImageResource(R.mipmap.icon_no_volume);
        } else {
            voice_btn.setImageResource(R.mipmap.icon_volume);
        }
    }

    @Override
    public void updateIcon_blueTooth(boolean isopen) {
        if (isopen) {
            icon_ble.setImageResource(R.mipmap.icon_ble_enable);
        } else {
            icon_ble.setImageResource(R.mipmap.icon_ble_inenable);
        }
    }

    @Override
    public void updateIcon_user() {

    }

    @Override
    public void updateIcon_update(boolean isReadyUpdate) {

    }

    @Override
    public void setSoundBarValue(int value) {
        soundBar.setProgress(value);
    }

    @Override
    public void updateFridgeStatus(FridgeTempBean bean) {
        fridge_temp1.setText(bean.getTemp1());
        fridge_temp2.setText(bean.getTemp2());
        fridge_temp2_model.setText(bean.getTemp2_model());
        fridge_temp3.setText(bean.getTemp3());
    }

    @Override
    public void updateAlbum() {
        File saveDir = new File(Environment.getExternalStorageDirectory(), "viomi_album_show");
        List<Uri> ablumlist = new ArrayList<>();
        if (saveDir.exists()) {
            File[] files = saveDir.listFiles();
            for (int i = 0; i < files.length; i++) {
                ablumlist.add(Uri.fromFile(files[i]));
            }
        }

        if (ablumlist.size() == 0) {
            List<Integer> defualtList = new ArrayList<>();
            defualtList.add(R.drawable.iv1);
            defualtList.add(R.drawable.iv2);
            defualtList.add(R.drawable.iv3);
            defualtList.add(R.drawable.iv4);
            defualtList.add(R.drawable.iv5);
            defualtList.add(R.drawable.iv6);
            album_vp.setDefualtAdapterDatas(defualtList);
            album_vp.startAutoScroll();
            return;
        }
        album_vp.setCustomAdapterDatas(ablumlist);
        album_vp.startAutoScroll();
    }

    @Override
    public void updateMessage() {

    }

    @Override
    public void updateFoodList() {

    }

    @Override
    public void updateWeather(WeatherBean weather) {
        WeatherIconUtil.setWeatherIcon2(weather_icon, weather.getWeather());
        weather_staus.setText(weather.getWeather());
        weather_temp.setText(weather.getTempRange_s());
        weather_pm25.setText(weather.getPm25());
        weather_location.setText(weather.getCity());
    }


    @Override
    public void updateFoodMenu(List<HomeWidgetBean> cooklist) {
        if (cooklist.size() > 0) {
            Uri uri = Uri.parse(cooklist.get(0).getImageUrl());
            cook_menu_img.setImageURI(uri);
            GlobalParams.Menu_Url = cooklist.get(0).getLinkUrl();
            cook_menu_img.setOnClickListener(v -> {
                if (cooklist.size() > 0) {
                    StatsManager.recordCountEvent(MainNewActivity.this, StatsManager.EVENT_ID_MODULE_ENTER, StatsManager.EVENT_TYPE_MENU, "nemu_connect");
//                    Intent intent = new Intent(MainNewActivity.this, CookMenuActivity.class);
//                    intent.putExtra("url", cooklist.get(0).getLinkUrl());
//                    startActivity(intent);
                } else {
                    StatsManager.recordCountEvent(MainNewActivity.this, StatsManager.EVENT_ID_MODULE_ENTER, StatsManager.EVENT_TYPE_MENU, "menu_disconnect");
                }
            });
        }
    }

    @Override
    public void updateAdsRecommend(List<List<GoodsEntity>> adslist, int times) {
        if (times == 0) {
            return;
        }

        List<LinearLayout> goodsList = new ArrayList<>();

        secondVPsize = adslist.size();

        for (int i = 0; i < adslist.size() * times; i++) {

            LinearLayout linearLayout = new LinearLayout(this);
            linearLayout.setLayoutParams(new LinearLayout.LayoutParams((int) PhoneUtil.pxTodip(this, 360), (int) PhoneUtil.pxTodip(this, 150)));
            linearLayout.setOrientation(LinearLayout.HORIZONTAL);
            for (int j = 0; j < adslist.get(i % adslist.size()).size(); j++) {
                View view = LayoutInflater.from(this).inflate(R.layout.goods_viewpager_item, null);

                SimpleDraweeView goods_img = (SimpleDraweeView) view.findViewById(R.id.goods_img);
                TextView name = (TextView) view.findViewById(R.id.name);
                TextView price = (TextView) view.findViewById(R.id.price);

                Uri uri = Uri.parse(adslist.get(i % adslist.size()).get(j).getImageUrl());
                goods_img.setImageURI(uri);
                name.setText(adslist.get(i % adslist.size()).get(j).getName());
                price.setText("¥" + adslist.get(i % adslist.size()).get(j).getPrice());

                final int finalI = i % adslist.size();
                final int finalJ = j;
                view.setOnClickListener(v -> {
                    if (adslist.size() > 0) {
                        Intent intent = new Intent(MainNewActivity.this, VmallWebActivity.class);
                        WebBaseData value = new WebBaseData();

                        switch (adslist.get(finalI).get(finalJ).getTargetType()) {
                            case 1:
                                value.url = mVmallUrl + "/index.html?page=detail&spuId=" + adslist.get(finalI).get(finalJ).getTargetId();
                                log.myE(TAG, "" + value.url);
                                break;
                            case 2:
                                value.url = mVmallUrl + "/index.html?page=detail&skuId=" + adslist.get(finalI).get(finalJ).getTargetId();
                                log.myE(TAG, "" + value.url);
                                break;
                            default:
                                break;
                        }
                        StatsManager.recordCountEvent(MainNewActivity.this, StatsManager.EVENT_ID_MODULE_ENTER, StatsManager.EVENT_TYPE_VMALL_DETAIL, "vmall_detail_connect");
                        intent.putExtra(WebBaseData.Intent_String, value);
                        startActivity(intent);
                    } else {
                        StatsManager.recordCountEvent(MainNewActivity.this, StatsManager.EVENT_ID_MODULE_ENTER, StatsManager.EVENT_TYPE_VMALL_DETAIL, "vmall_detail_disconnect");
                    }
                });
                linearLayout.addView(view);
            }
            goodsList.add(linearLayout);
        }
        goods_viewPager.setCustomAdapterDatas(goodsList);
        goods_viewPager.startAutoScroll();
        goods_smallpoint.setCount(adslist.size());
        goods_smallpoint.setIndex(0);
    }

    private void setdefault() {
        int[][] goodsImgs = new int[][]{{R.drawable.g00_6999_v1, R.drawable.g01_4999_v1, R.drawable.g02_3999_c1}, {R.drawable.g10_2999_c1, R.drawable.g11_2999_s1, R.drawable.g12_2499_s1}};
        String[][] goodsNames = new String[][]{{"V1 尊享版", "V1 乐享版", "C1 厨下版"}, {"C1 厨上版", "S1 优享版", "S1 标准版"}};
        String[][] goodsPrice = new String[][]{{"¥6999", "¥4999", "¥3999"}, {"¥2999", "¥2999", "¥2499"}};

        secondVPsize = goodsImgs.length;
        List<LinearLayout> defuatList = new ArrayList<>();
        for (int i = 0; i < goodsImgs.length * 2; i++) {
            LinearLayout linearLayout = new LinearLayout(this);
            linearLayout.setLayoutParams(new LinearLayout.LayoutParams((int) PhoneUtil.pxTodip(this, 360), (int) PhoneUtil.pxTodip(this, 150)));
            linearLayout.setOrientation(LinearLayout.HORIZONTAL);
            for (int j = 0; j < goodsImgs[i % goodsImgs.length].length; j++) {
                View view = LayoutInflater.from(this).inflate(R.layout.goods_viewpager_item, null);
                SimpleDraweeView goods_img = (SimpleDraweeView) view.findViewById(R.id.goods_img);
                goods_img.setOnClickListener(v -> {
                    showConnectHint();
                });
                TextView name = (TextView) view.findViewById(R.id.name);
                TextView price = (TextView) view.findViewById(R.id.price);
                goods_img.setImageResource(goodsImgs[i % goodsImgs.length][j]);
                goods_img.setScaleType(ImageView.ScaleType.CENTER_CROP);
                name.setText(goodsNames[i % goodsImgs.length][j]);
                price.setText(goodsPrice[i % goodsImgs.length][j]);
                linearLayout.addView(view);
            }
            defuatList.add(linearLayout);
        }

        goods_viewPager.setCustomAdapterDatas(defuatList);
        goods_viewPager.startAutoScroll();
        goods_smallpoint.setCount(secondVPsize);
        goods_smallpoint.setIndex(0);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == event.KEYCODE_BACK) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
