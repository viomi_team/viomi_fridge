package com.viomi.fridge.view.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.widget.ImageView;

import com.miot.api.MiotManager;
import com.miot.common.people.People;
import com.viomi.fridge.common.GlobalParams;
import com.viomi.fridge.common.ViomiApplication;
import com.viomi.fridge.manager.AccountManager;
import com.viomi.fridge.model.bean.ViomiUser;
import com.viomi.fridge.model.bean.WebBaseData;
import com.viomi.fridge.util.LogUtils;
import com.viomi.fridge.util.log;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by young2 on 2017/2/21.
 */

public class CommonWebActivity extends WebActivity {
    private final static String TAG = CommonWebActivity.class.getSimpleName();
    private Object mJavaScriptInterface = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mJavaScriptInterface = new VmallJavaScriptInterface();
        mWebView.addJavascriptInterface(mJavaScriptInterface, "H5ToNative");
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        mJavaScriptInterface = null;
    }

    private class VmallJavaScriptInterface {
        /***
         * 清除帐号信息
         */
        @JavascriptInterface
        public void onClearAcount() {
            AccountManager.deleteViomiUser(ViomiApplication.getContext());
        }

        /***
         * 打开新H5页面
         * @param title 新页面标题
         * @param url  新页面链接
         */
        @JavascriptInterface
        public void onWebPageJump(String title, String url) {
            log.myE(TAG, "onWebPageJump");
            Intent intent = new Intent(CommonWebActivity.this, CommonWebActivity.class);
            WebBaseData model = new WebBaseData();
            model.url = url;
            model.name = title;
            intent.putExtra(WebBaseData.Intent_String, model);
            startActivity(intent);
        }

        /***
         * 关闭当前页面，返回上一页面
         */
        @JavascriptInterface
        public void onWebPageReturn() {
            finish();
        }

        /***
         * 跳转到登陆页面
         */
        @JavascriptInterface
        public void onLoginPageJump() {
            Intent intent = new Intent(CommonWebActivity.this, ScanLoginActivity.class);
            startActivity(intent);
        }

        /***
         * 获取城市名称
         */
        @JavascriptInterface
        public String getCityName() {
            return GlobalParams.getInstance().getLocationCityName();
        }

        /***
         * 获取城市编码
         */
        @JavascriptInterface
        public String getCityCode() {
            return GlobalParams.getInstance().getLocationCityCode();
        }

        /***
         * 获取用户信息
         * return 未登录，返回null;已登陆返回json字符串
         */
        @JavascriptInterface
        public String getUserInfo() {
            ViomiUser viomiUser = AccountManager.getViomiUser(CommonWebActivity.this);
            if (viomiUser == null) {
                return null;
            }
            String miid=null;
            People mPeople = MiotManager.getPeople();
            if(mPeople!=null){
                miid=mPeople.getUserId();
            }
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("account", viomiUser.getAccount());
                jsonObject.put("userCode", viomiUser.getUserCode());
                jsonObject.put("token", viomiUser.getToken());
                jsonObject.put("cid", viomiUser.getCid());
                jsonObject.put("miid", miid);
                return jsonObject.toString();
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
        }

        //编辑器关闭时调用，隐藏系统底部导航栏
        @JavascriptInterface
        public void hideBottomNavigation() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                    getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
                    int uiFlags = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                            | View.SYSTEM_UI_FLAG_FULLSCREEN; // hide status bar

                    if (Build.VERSION.SDK_INT >= 19) {
                        uiFlags |= 0x00001000;    //SYSTEM_UI_FLAG_IMMERSIVE_STICKY: hide navigation bars - compatibility: building API level is lower thatn 19, use magic number directly for higher API target level
                    } else {
                        uiFlags |= View.SYSTEM_UI_FLAG_LOW_PROFILE;
                    }
                    try {
                        getWindow().getDecorView().setSystemUiVisibility(uiFlags);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    log.myE(TAG, "调了hideBottomNavigation");
                }
            });
        }

        //有数据更新是清除本地缓存，H5调用
        @JavascriptInterface
        public void clearNativeCache() {
            log.myE(TAG, "调了clear()");
            mWebView.clearCache(true);
        }

        //H5加载失败后调用
        @JavascriptInterface
        public void loadFail() {
            LogUtils.d(TAG, "loadFail");
            runOnUiThread(() -> LogUtils.d(TAG, "loadFail"));
        }
    }
}
