package com.viomi.fridge.view.fragment;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.miot.api.MiotManager;
import com.miot.common.people.People;
import com.viomi.common.module.okhttp.OkHttpClientManager;
import com.viomi.fridge.R;
import com.viomi.fridge.api.http.HttpConnect;
import com.viomi.fridge.manager.VoiceManager;
import com.viomi.fridge.model.bean.WaterPuriProp;
import com.viomi.fridge.model.parser.DeviceGetPropDecode;
import com.viomi.fridge.util.log;
import com.viomi.fridge.view.widget.ExpandLayout;
import com.viomi.fridge.view.widget.WaterFilterDialog;
import com.viomi.fridge.view.widget.waveview.WaveHelper;
import com.viomi.fridge.view.widget.waveview.WaveView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.Call;

/**
 * X5 净水器 Fragment
 * Created by William on 2017/4/14.
 */

public class DeviceWaterPuriFragment extends Fragment implements View.OnClickListener {

    private final static String TAG = DeviceWaterPuriFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private OnFragmentInteractionListener mListener;
    private RelativeLayout temp_layout;
    private Button temp_setting;
    private ExpandLayout temp_setting_layout, flow_setting_layout;
    private TextView temp_setting_tv1, temp_setting_tv2, flow_setting_tv1, flow_setting_tv2, flow_setting_tv3, flow_setting_tv4;
    private SeekBar temp_bar;
    private RelativeLayout flow_layout;
    private Button flow_setting;
    private SeekBar s_flow_bar, m_flow_bar, b_flow_bar;
    private Timer mTimer;
    private TimerTask mTimeTask;
    private People mPeople;
    private String did, mDeviceName;
    private final MyHandler mHandler = new MyHandler(this);
    private boolean mIsSetting = false;
    private View mView;
    private float oneLife, twoLife, threeLife, fourLife;
    private TextView mOutTdsView, ppLifeView, c1LifeView, c2LifeView, roLifeView, mWaterTemp, mUvState;
    private WaveHelper mWaveHelper;

    private int MIN_TEMP_BAR = 40;
    private int MAX_TEMP_BAR = 90;
    private int MIN_FLOW_BAR1 = 120;
    private int MAX_FLOW_BAR1 = 300;
    private int MIN_FLOW_BAR2 = 310;
    private int MAX_FLOW_BAR2 = 450;
    private int MIN_FLOW_BAR3 = 460;
    private int MAX_FLOW_BAR3 = 2000;

    private final static int MSG_WHAT_GET_PROP = 0;
    private final static int MSG_WHAT_GET_X5_PROP = 1;
    private final static int MSG_WHAT_SET_PROP_RESPONSE = 2;
    private final static int MSG_WHAT_SET_PROP_FAIL = 3;

    private boolean mIsVoiceOperate=false;//是否语音操作

    public static DeviceWaterPuriFragment newInstance(String param1, String param2) {
        DeviceWaterPuriFragment fragment = new DeviceWaterPuriFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            String param1 = getArguments().getString(ARG_PARAM1);
            String param2 = getArguments().getString(ARG_PARAM2);
            did = param1;
            mDeviceName = param2;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_connect_xfive, container, false);

        temp_layout = (RelativeLayout) mView.findViewById(R.id.temp_layout);
        temp_setting = (Button) mView.findViewById(R.id.temp_setting);
        temp_setting_layout = (ExpandLayout) mView.findViewById(R.id.temp_setting_layout);
        temp_setting_tv1 = (TextView) mView.findViewById(R.id.temp_setting_tv1);
        temp_setting_tv2 = (TextView) mView.findViewById(R.id.temp_setting_tv2);
        temp_bar = (SeekBar) mView.findViewById(R.id.temp_bar);

        flow_layout = (RelativeLayout) mView.findViewById(R.id.flow_layout);
        flow_setting = (Button) mView.findViewById(R.id.flow_setting);
        flow_setting_layout = (ExpandLayout) mView.findViewById(R.id.flow_setting_layout);
        flow_setting_tv1 = (TextView) mView.findViewById(R.id.flow_setting_tv1);
        flow_setting_tv2 = (TextView) mView.findViewById(R.id.flow_setting_tv2);
        flow_setting_tv3 = (TextView) mView.findViewById(R.id.flow_setting_tv3);
        flow_setting_tv4 = (TextView) mView.findViewById(R.id.flow_setting_tv4);
        s_flow_bar = (SeekBar) mView.findViewById(R.id.s_flow_bar);
        m_flow_bar = (SeekBar) mView.findViewById(R.id.m_flow_bar);
        b_flow_bar = (SeekBar) mView.findViewById(R.id.b_flow_bar);

        mOutTdsView = (TextView) mView.findViewById(R.id.outTds);
        ppLifeView = (TextView) mView.findViewById(R.id.ppLife);
        c1LifeView = (TextView) mView.findViewById(R.id.c1Life);
        roLifeView = (TextView) mView.findViewById(R.id.roLife);
        c2LifeView = (TextView) mView.findViewById(R.id.c2Life);
        mWaterTemp = (TextView) mView.findViewById(R.id.water_temp);
        mUvState = (TextView) mView.findViewById(R.id.uv_state);
//        mPressView = (TextView) mView.findViewById(R.id.press);
//        mElecavlStateView = (TextView) mView.findViewById(R.id.elecval_state);

        temp_setting_layout.initExpand(false);
        flow_setting_layout.initExpand(false);

        TextView mDeviceNameView = (TextView) mView.findViewById(R.id.device_name);
        mDeviceNameView.setText(mDeviceName);
        initListener();
        init();
        startTimer();

        return mView;
    }

    // 设置默认值，可删除
    private void init() {
        mPeople = MiotManager.getPeopleManager().getPeople();
        temp_bar.setMax(MAX_TEMP_BAR - MIN_TEMP_BAR);
        s_flow_bar.setMax(MAX_FLOW_BAR1 - MIN_FLOW_BAR1);
        m_flow_bar.setMax(MAX_FLOW_BAR2 - MIN_FLOW_BAR2);
        b_flow_bar.setMax(MAX_FLOW_BAR3 - MIN_FLOW_BAR3);

        WaveView waveView = (WaveView) mView.findViewById(R.id.wave_bg);
        waveView.setBorder(0, 0);
        waveView.setWaveColor(
                Color.parseColor("#FFFFFF"),
                Color.parseColor("#FFFFFF"));
        mWaveHelper = new WaveHelper(waveView);
        mWaveHelper.start();
//        temp_bar.setProgress(25);
//        temp_setting_tv1.setText("--℃");
//        temp_setting_tv2.setText("--℃");
//
//        s_flow_bar.setProgress(80);
//        flow_setting_tv1.setText("--ml");
//        flow_setting_tv2.setText("--ml");
    }

    //监听点击事件
    private void initListener() {
        temp_layout.setOnClickListener(v -> {
            temp_setting_layout.toggleExpand();
//                if (temp_setting.isSelected()) {
//                    temp_setting_layout.setVisibility(View.GONE);
//                } else {
//                    temp_setting_layout.setVisibility(View.VISIBLE);
//                }
            temp_setting.setSelected(!temp_setting.isSelected());
        });

        flow_layout.setOnClickListener(v -> {
            flow_setting_layout.toggleExpand();
//                if (flow_setting.isSelected()) {
//                    flow_setting_layout.setVisibility(View.GONE);
//                } else {
//                    flow_setting_layout.setVisibility(View.VISIBLE);
//                }
            flow_setting.setSelected(!flow_setting.isSelected());
        });

        temp_setting.setOnClickListener(v -> {
            temp_setting_layout.toggleExpand();
//                if (temp_setting.isSelected()) {
//                    temp_setting_layout.setVisibility(View.GONE);
//                } else {
//                    temp_setting_layout.setVisibility(View.VISIBLE);
//                }
            temp_setting.setSelected(!temp_setting.isSelected());
        });

        flow_setting.setOnClickListener(v -> {
            flow_setting_layout.toggleExpand();
            if (flow_setting.isSelected()) {
                flow_setting_layout.setVisibility(View.GONE);
            } else {
                flow_setting_layout.setVisibility(View.VISIBLE);
            }
            flow_setting.setSelected(!flow_setting.isSelected());
        });

        temp_bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                temp_setting_tv1.setText(40 + progress + "℃");
                temp_setting_tv2.setText(tempChose(40 + progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int temp = seekBar.getProgress() + MIN_TEMP_BAR;
                mIsSetting = true;
                setProp("set_tempe_setup", 1, temp);

            }
        });

        s_flow_bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                flow_setting_tv1.setText(120 + progress + "ml");
                flow_setting_tv2.setText(120 + progress + "ml");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int temp = seekBar.getProgress() + MIN_FLOW_BAR1;
                mIsSetting = true;
                setProp("set_flow_setup", 0, temp);
            }
        });

        m_flow_bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                flow_setting_tv1.setText(310 + progress + "ml");
                flow_setting_tv3.setText(310 + progress + "ml");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int temp = seekBar.getProgress() + MIN_FLOW_BAR2;
                mIsSetting = true;
                setProp("set_flow_setup", 1, temp);
            }
        });

        b_flow_bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                flow_setting_tv1.setText(460 + progress + "ml");
                flow_setting_tv4.setText(460 + progress + "ml");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int temp = seekBar.getProgress() + MIN_FLOW_BAR3;
                mIsSetting = true;
                setProp("set_flow_setup", 2, temp);
            }
        });
    }

    private String tempChose(int temp) {
        String str;
        switch (temp) {
            case 40:
                str = "40℃适合冲益生菌";
                break;
            case 50:
                str = "50℃适合冲奶/饮用";
                break;
            case 60:
                str = "60℃适合冲蜂蜜水";
                break;
            case 75:
                str = "75℃适合冲泡龙井";
                break;
            case 80:
                str = "80℃适合冲绿茶";
                break;
            case 85:
                str = "85℃适合冲泡大红袍/咖啡";
                break;
            case 90:
                str = "90℃适合冲泡铁观音";
                break;
            default:
                str = temp + "℃";
                break;
        }
        return str;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            // throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        stopTimer();
    }

    @Override
    public void onClick(View v) {
        WaterFilterDialog waterFilterDialog = null;
        switch (v.getId()) {
            case R.id.pp_click: // PP棉滤芯
                waterFilterDialog = new WaterFilterDialog(getActivity(), 1, oneLife, 24);
                break;
            case R.id.cl_click: // 前置活性炭滤芯
                waterFilterDialog = new WaterFilterDialog(getActivity(), 2, twoLife, 16);
                break;
            case R.id.ro_click: // RO反渗透滤芯
                waterFilterDialog = new WaterFilterDialog(getActivity(), 3, threeLife, 18);
                break;
            case R.id.c2_click: // 后置活性炭滤芯
                waterFilterDialog = new WaterFilterDialog(getActivity(), 4, fourLife, 17);
                break;
            default:
                break;
        }
        if (waterFilterDialog != null)
            waterFilterDialog.show();
    }


    /***
     * 语音控制
     * @param temp
     */
    public void tempVoiceControl(int temp){
        mIsVoiceOperate=true;
        setProp("set_tempe_setup", 1, temp);
    }

    private static class MyHandler extends Handler {
        WeakReference<DeviceWaterPuriFragment> weakReference;

        public MyHandler(DeviceWaterPuriFragment fragment) {
            this.weakReference = new WeakReference<>(fragment);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            DeviceWaterPuriFragment fragment = weakReference.get();
            switch (msg.what) {
                case MSG_WHAT_GET_PROP:
                    String prop = (String) msg.obj;
                    if (prop == null) {
                        return;
                    }
                    JSONObject jsonObject;
                    try {
                        jsonObject = new JSONObject(prop);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        return;
                    }
                    WaterPuriProp waterPuriProp = DeviceGetPropDecode.decode(jsonObject, false);
                    if (waterPuriProp == null) {
                        return;
                    }
                    fragment.refreshPropView(waterPuriProp);
                    break;
                case MSG_WHAT_GET_X5_PROP:
                    String text = (String) msg.obj;
                    if (text == null) {
                        return;
                    }
                    log.d(TAG, "get_x5=" + text);
                    try {
                        JSONObject jsonObject1 = new JSONObject(text);
                        JSONArray jsonArray = jsonObject1.getJSONArray("result");
                        int i = 0;
                        // int press=jsonArray.getInt(i);
                        int temp = jsonArray.getInt(i);
                        i++;
                        int uv_state = jsonArray.getInt(i);
                        // int elecval_state=jsonArray.getInt(i);
                        int setup_tempe = 0;
                        int setup_flow = 0;
                        int custom_tempe1 = 0;
                        int custom_flow0 = 0;
                        int custom_flow1 = 0;
                        int custom_flow2 = 0;
                        try {
                            i++;
                            setup_tempe = jsonArray.getInt(i);
                            i++;
                            setup_flow = jsonArray.getInt(i);
                            i++;
                            custom_tempe1 = jsonArray.getInt(i);
                            i++;
                            custom_flow0 = jsonArray.getInt(i);
                            i++;
                            custom_flow1 = jsonArray.getInt(i);
                            i++;
                            custom_flow2 = jsonArray.getInt(i);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        fragment.mWaterTemp.setText("" + temp + "℃");
//                        mPressView.setText(""+press/1000.0+"Mpa");
                        if (uv_state == 0) {
                            fragment.mUvState.setText("空闲中");
                        } else {
                            fragment.mUvState.setText("杀菌中");
                        }

//                        if(elecval_state==0){
//                           mElecavlStateView.setText("关闭");
//                        }else {
//                            mElecavlStateView.setText("开启");
//                        }

                        if (fragment.mIsSetting) {
                            return;
                        }
                        fragment.temp_setting_tv1.setText("" + setup_tempe + "℃");
                        fragment.temp_setting_tv2.setText(fragment.tempChose(custom_tempe1));
                        fragment.flow_setting_tv1.setText("" + setup_flow + "ml");
                        fragment.flow_setting_tv2.setText("" + custom_flow0 + "ml");
                        fragment.flow_setting_tv3.setText("" + custom_flow1 + "ml");
                        fragment.flow_setting_tv4.setText("" + custom_flow2 + "ml");

                        fragment.temp_bar.setProgress(custom_tempe1 - fragment.MIN_TEMP_BAR);
                        fragment.s_flow_bar.setProgress(custom_flow0 - fragment.MIN_FLOW_BAR1);
                        fragment.m_flow_bar.setProgress(custom_flow1 - fragment.MIN_FLOW_BAR2);
                        fragment.b_flow_bar.setProgress(custom_flow2 - fragment.MIN_FLOW_BAR3);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

                case MSG_WHAT_SET_PROP_RESPONSE:
                    String response = (String) msg.obj;
                    if (response == null) {
                        return;
                    }
                    try {
                        JSONObject jsonObject2 = new JSONObject(response);
                        String message = jsonObject2.getString("message");
                        if (!"ok".equals(message)) {
                            Toast.makeText(fragment.getActivity(), "设置失败", Toast.LENGTH_SHORT).show();
                            if(fragment.mIsVoiceOperate){
                                VoiceManager.getInstance().startSpeak("设置失败，请重试");
                            }
                        }else {
                            if(fragment.mIsVoiceOperate){
                                VoiceManager.getInstance().startSpeak("已为您设置成功");
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }finally {
                        fragment.mIsVoiceOperate=false;
                    }
                    break;

                case MSG_WHAT_SET_PROP_FAIL:
                    Toast.makeText(fragment.getActivity(), "设置失败", Toast.LENGTH_SHORT).show();
                    if(fragment.mIsVoiceOperate){
                        VoiceManager.getInstance().startSpeak("设置失败，请重试");
                    }
                    fragment.mIsVoiceOperate=false;
                    break;
            }
        }
    }


    private interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    private void startTimer() {
        int sleepTime = 5 * 1000;
        stopTimer();
        mTimer = new Timer();
        mTimeTask = new TimerTask() {
            @Override
            public void run() {
                getProp();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                getPropX5();
            }
        };
        mTimer.schedule(mTimeTask, 0, sleepTime);
    }

    private void stopTimer() {
        if (mTimer != null) {
            mTimer.cancel();
            mTimer = null;
        }
        if (mTimeTask != null) {
            mTimeTask.cancel();
            mTimeTask = null;
        }
    }

    private void refreshPropView(WaterPuriProp waterPuriProp) {
        if (waterPuriProp.pTds < 100) {
            mOutTdsView.setText(String.valueOf(waterPuriProp.pTds));
        }

        oneLife = waterPuriProp.oneLifeInt;
        twoLife = waterPuriProp.twoLifeInt;
        threeLife = waterPuriProp.threeLifeInt;
        fourLife = waterPuriProp.fourLifeInt;

        ppLifeView.setText("" + (int) waterPuriProp.oneLifeInt + "%");
        c1LifeView.setText("" + (int) waterPuriProp.twoLifeInt + "%");
        roLifeView.setText("" + (int) waterPuriProp.threeLifeInt + "%");
        c2LifeView.setText("" + (int) waterPuriProp.fourLifeInt + "%");

        mView.findViewById(R.id.pp_click).setOnClickListener(this);
        mView.findViewById(R.id.cl_click).setOnClickListener(this);
        mView.findViewById(R.id.ro_click).setOnClickListener(this);
        mView.findViewById(R.id.c2_click).setOnClickListener(this);
    }

    //getProp={"code":0,"message":"ok","result":[127,4,200,819,200,911,910,3395,910,4163,3600,4320,3600,8640,7200,17280,3600,8640,0,1,0,1,0]}
    private void getProp() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "get_prop");
            jsonObject.put("did", did);
            jsonObject.put("id", 123);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        HttpConnect.rpcDevice(mPeople, did, jsonObject.toString(), new OkHttpClientManager.ResultCallback<String>() {
            @Override
            public void onError(Call call, Exception e) {
                log.myE(TAG, "getProp error,msg=" + e.getMessage());
            }

            @Override
            public void onResponse(String response) {
                log.d(TAG, "getProp=" + response);
                Message message = mHandler.obtainMessage();
                message.what = MSG_WHAT_GET_PROP;
                message.obj = response;
                mHandler.sendMessage(message);
            }
        });
    }

    private void getPropX5() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "get_prop");
            jsonObject.put("did", did);
            jsonObject.put("id", 1);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put("temperature");
            jsonArray.put("uv_state");
            jsonArray.put("setup_tempe");
            jsonArray.put("setup_flow");
            jsonArray.put("custom_tempe1");
            jsonArray.put("custom_flow0");
            jsonArray.put("custom_flow1");
            jsonArray.put("custom_flow2");
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        HttpConnect.rpcDevice(mPeople, did, jsonObject.toString(), new OkHttpClientManager.ResultCallback<String>() {
            @Override
            public void onError(Call call, Exception e) {
                log.myE(TAG, "getProp error,msg=" + e.getMessage());
            }

            @Override
            public void onResponse(String response) {
                log.d(TAG, "getPropX5=" + response);
                Message message = mHandler.obtainMessage();
                message.what = MSG_WHAT_GET_X5_PROP;
                message.obj = response;
                mHandler.sendMessage(message);
            }
        });
    }

    private void setProp(String method, int index, int value) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", method);
            jsonObject.put("did", did);
            jsonObject.put("id", 12);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(index);
            jsonArray.put(value);
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        HttpConnect.rpcDevice(mPeople, did, jsonObject.toString(), new OkHttpClientManager.ResultCallback<String>() {
            @Override
            public void onError(Call call, Exception e) {
                log.myE(TAG, "setProp error,msg=" + e.getMessage());
                mHandler.sendEmptyMessage(MSG_WHAT_SET_PROP_FAIL);
                mIsSetting = false;
            }

            @Override
            public void onResponse(String response) {
                log.d(TAG, "setProp=" + response);
                Message message = mHandler.obtainMessage();
                message.what = MSG_WHAT_SET_PROP_RESPONSE;
                message.obj = response;
                mHandler.sendMessage(message);
                mIsSetting = false;
            }
        });
    }

    public void onDestroy() {
        super.onDestroy();
        stopTimer();
        mHandler.removeCallbacksAndMessages(null);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (hidden) {
            stopTimer();
            mHandler.removeCallbacksAndMessages(null);
            mWaveHelper.cancel();
        }
    }

}
