package com.viomi.fridge.view.activity;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.WindowManager;
import android.webkit.GeolocationPermissions;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amap.api.location.AMapLocationClient;
import com.viomi.fridge.R;
import com.viomi.fridge.model.bean.WebBaseData;
import com.viomi.fridge.util.ToastUtil;
import com.viomi.fridge.util.log;

import java.util.Timer;
import java.util.TimerTask;

@SuppressLint("SetJavaScriptEnabled")
public class WebActivity2 extends BaseHandlerActivity {
    private static final String TAG = "WebActivity";
    protected WebView mWebView;
    private WebBaseData mData;
    private ImageView mBackIcon;
    public TextView mTitleText;
    public RelativeLayout mTitleView;
    protected boolean isWebLoadFinish;
    private RelativeLayout network_fail_layout;

    private AMapLocationClient mLocationClientSingle;
    private LinearLayout mLoadingView;
    private TimerTask mTimerTask;
    private Timer mTimer;
    private static final int MSG_WHAT_TIMEOUT=1;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);
        setContentView(R.layout.activity_webview);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE
                | WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        init();
        initListener();
    }

    public void onResume() {
        super.onResume();
        if (mWebView != null)
            network_fail_layout.setVisibility(View.GONE);
        mWebView.onResume();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopTimer();
        if (null != mLocationClientSingle) {
            mLocationClientSingle.stopAssistantLocation();
            mLocationClientSingle.onDestroy();
        }
        if (mWebView != null) {
            ViewParent parent = mWebView.getParent();
            if (parent != null) {
                ((ViewGroup) parent).removeView(mWebView);
            }
            mWebView.stopLoading();
            WebSettings webSettings = mWebView.getSettings();
            // 退出时调用此方法，移除绑定的服务，否则某些特定系统会报错
            webSettings.setAllowFileAccess(true);// 允许访问文件
            webSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);// 设置 WebView 底层布局算法
            webSettings.setSupportZoom(false);// 禁止手势缩放
            webSettings.setBuiltInZoomControls(false);// 设置 WebView 是否使用其内置的变焦机制
            webSettings.setUseWideViewPort(false);// 当该属性被设置为 false 时，加载页面的宽度总是适应 WebView 控件宽度
            webSettings.setSupportMultipleWindows(false);// 是否支持多屏窗口
            webSettings.setLoadWithOverviewMode(true);// 是否使用预览模式加载界面
            webSettings.setAppCacheEnabled(true);// 设置 Application 缓存 API 是否开启
            webSettings.setDatabaseEnabled(true);// 启用数据库
            webSettings.setDomStorageEnabled(true);// 设置是否开启 DOM 存储 API 权限
            webSettings.setJavaScriptEnabled(true);// 是否允许执行 JavaScript 脚本
            webSettings.setGeolocationEnabled(true);// 是否开启定位功能
            webSettings.setAppCacheMaxSize(Long.MAX_VALUE);// 缓存最大值
            webSettings.setAppCachePath(this.getDir("viomi", 0).getPath());// 缓存路径
            webSettings.setDatabasePath(this.getDir("viomi", 0).getPath());// 缓存路径
            webSettings.setGeolocationDatabasePath(this.getDir("viomi", 0).getPath());// 地理位置缓存路径       webSettings.setAllowContentAccess(true);
            webSettings.setLoadsImagesAutomatically(true);// 是否自动加载图片资源
            webSettings.setPluginState(WebSettings.PluginState.ON_DEMAND);
            webSettings.setRenderPriority(WebSettings.RenderPriority.HIGH);
            webSettings.setSavePassword(false);
            webSettings.setSaveFormData(false);
            webSettings.setAllowUniversalAccessFromFileURLs(true);// 设置 WebView 运行中的脚本可以是否访问任何原始起点内容
            mWebView.clearView();
            mWebView.removeAllViews();

            try {
                mWebView.destroy();
            } catch (Throwable ex) {
            }
        }
    }

    @Override
    protected void processMsg(Message msg) {
        switch (msg.what){
            case MSG_WHAT_TIMEOUT:
                loadUrlFail();
                break;
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        mData = intent.getExtras().getParcelable(WebBaseData.Intent_String);
        if (mData != null && mData.url != null) {
            String url = mData.url;
            log.myE(TAG, "loadUrl=" + url);
            mWebView.loadUrl(url);
        }
    }


    protected void init() {
        mTitleView = (RelativeLayout) findViewById(R.id.title_view);
        mTitleText = (TextView) findViewById(R.id.tvTitle);
        mTitleText.setText("云米商城");
        mBackIcon = (ImageView) findViewById(R.id.imgBack);
        network_fail_layout = (RelativeLayout) findViewById(R.id.network_fail_layout);
        mWebView = (WebView) findViewById(R.id.id_webview);
        mLoadingView = (LinearLayout) findViewById(R.id.loading_view);

        Intent intent = getIntent();
        mData = intent.getExtras().getParcelable(WebBaseData.Intent_String);

        if (mData == null || mData.url == null) {
            finish();
            ToastUtil.show("网页链接错误！url=" + mData.url);
        }
        //
        startAssistLocation();
     //   initSonicWeb(mData.url);
        WebSettings wSet = mWebView.getSettings();
        // add java script interface
        // note:if api level lower than 17(android 4.2), addJavascriptInterface has security
        // issue, please use x5 or see https://developer.android.com/reference/android/webkit/
        // WebView.html#addJavascriptInterface(java.lang.Object, java.lang.String)
        wSet.setJavaScriptEnabled(true);
        mWebView.removeJavascriptInterface("searchBoxJavaBridge_");
       // intent.putExtra(SonicJavaScriptInterface.PARAM_LOAD_URL_TIME, System.currentTimeMillis());
     //   mWebView.addJavascriptInterface(new SonicJavaScriptInterface(sonicSessionClient, intent), "sonic");

        // init webview settings
        wSet.setAllowContentAccess(true);
        wSet.setAppCacheEnabled(true);
        wSet.setSavePassword(false);
        wSet.setSaveFormData(false);
        wSet.setLoadWithOverviewMode(true);
        wSet.setUseWideViewPort(true);
        setWebviewLocation(mWebView, wSet);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            wSet.setAllowUniversalAccessFromFileURLs(true);//允许跨域
        }

        mWebView.setWebViewClient(new MyWebViewClient());
        mWebView.setWebChromeClient(new MyWebChromeClient());

        if (mData != null && mData.url != null) {
            String url = mData.url;
            log.myE(TAG, "loadUrl=" + url);
            mWebView.loadUrl(url);
        }

    }

    private void setWebviewLocation(WebView webView, WebSettings webSettings) {
        if (webView == null || webSettings == null) {
            return;
        }
        //启用数据库
        webSettings.setDatabaseEnabled(true);
        //启用地理定位
        webSettings.setGeolocationEnabled(true);
        //设置定位的数据库路径
        String dir = this.getApplicationContext().getDir("database", Context.MODE_PRIVATE).getPath();
        webSettings.setGeolocationDatabasePath(dir);
        //开启DomStorage缓存
        webSettings.setDomStorageEnabled(true);

    }


    private void initListener() {
        mBackIcon.setOnClickListener(v -> {
            onBackPressed();
        });
    }

    public void loadUrlFail() {
        network_fail_layout.setVisibility(View.VISIBLE);
        mTitleView.setVisibility(View.VISIBLE);
    }

    private class MyWebViewClient extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            stopTimer();
            mTimer = new Timer();
            mTimerTask = new TimerTask() {
                @Override
                public void run() {
                    stopTimer();
                    if (mHandler != null) {
                        mHandler.sendEmptyMessage(MSG_WHAT_TIMEOUT);
                    }
                }
            };
            mTimer.schedule(mTimerTask, 30000);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            log.myE(TAG, "onPageFinished=" + url);
            stopTimer();
            mLoadingView.setVisibility(View.GONE);
        }

        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            log.myE(TAG, "onReceivedError");
            stopTimer();
            loadUrlFail();
        }

        @TargetApi(21)
        @Override
        public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {
            return shouldInterceptRequest(view, request.getUrl().toString());
        }

        @Override
        public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
            return null;
        }

        //public boolean shouldOverrideUrlLoading(WebView view, String url) {
        //    view.loadUrl(url);
        //    log.myE(TAG, "shouldOverrideUrlLoading");
        //    return true;
        //}
    }

    private class MyWebChromeClient extends WebChromeClient {


        @Override
        public void onReceivedTitle(WebView view, String title) {
            log.myE(TAG, "onReceivedTitle=" + title + "---view.getTitle()=" + view.getTitle());
            mTitleText.setText(title);
        }

        public void onReceivedIcon(WebView view, Bitmap icon) {
            super.onReceivedIcon(view, icon);
        }

        // 处理javascript中的alert
        public boolean onJsAlert(WebView view, String url, String message, final JsResult result) {
            return true;
        }

        // 处理javascript中的confirm
        public boolean onJsConfirm(WebView view, String url, String message, final JsResult result) {
            return true;
        }

        // 处理定位权限请求
        @Override
        public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback) {
            callback.invoke(origin, true, false);
            super.onGeolocationPermissionsShowPrompt(origin, callback);
        }
        @Override
        // 设置网页加载的进度条
        public void onProgressChanged(WebView view, int newProgress) {
            //    WebActivity.this.getWindow().setFeatureInt(Window.FEATURE_PROGRESS, newProgress * 100);
            Log.d(TAG, "onProgressChanged: " + newProgress);
            super.onProgressChanged(view, newProgress);
        }
    }

    /**
     * 启动H5辅助定位
     */
    private void startAssistLocation() {
        if (null == mLocationClientSingle) {
            mLocationClientSingle = new AMapLocationClient(this.getApplicationContext());
        }
        mLocationClientSingle.startAssistantLocation();
    }

    private void stopTimer() {
        if (mTimer != null) {
            mTimer.cancel();
            mTimer = null;
        }
        if (mTimerTask != null) {
            mTimerTask.cancel();
            mTimerTask = null;
        }
    }
}
