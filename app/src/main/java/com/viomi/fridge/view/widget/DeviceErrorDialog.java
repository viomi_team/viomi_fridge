package com.viomi.fridge.view.widget;

import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.viomi.fridge.R;

/**
 * 设备故障详情 Dialog
 * Created by William on 2017/12/13.
 */

public class DeviceErrorDialog extends BaseDialog {
    private String mTitle, mDetail;// 故障标题，故障详情

    public DeviceErrorDialog(Context context, String title, String detail) {
        super(context);
        mTitle = title;
        mDetail = detail;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (this.getWindow() != null) {
            WindowManager.LayoutParams lp = this.getWindow().getAttributes();
            lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.gravity = Gravity.CENTER;
            this.getWindow().setAttributes(lp);
            this.getWindow().setBackgroundDrawable(null);
        }
        setCanceledOnTouchOutside(false);

        TextView titleTextView = (TextView) findViewById(R.id.device_error_title);
        TextView detailTextView = (TextView) findViewById(R.id.device_error_detail);
        titleTextView.setText(mTitle);
        detailTextView.setText(mDetail);
        findViewById(R.id.device_error_close).setOnClickListener(v -> dismiss());
    }

    @Override
    public void setView() {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_device_error);
    }
}
