package com.viomi.fridge.view.activity;
import java.io.IOException;

import android.app.Activity;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.viomi.fridge.R;

public class VolumeSettingActivity extends BaseActivity {

    private SeekBar MusicSeekBar;
    private int volume = -1;
    private int  Musicmaxvolume=-1;
    private TextView textView;
    private Button btn;

    private MediaPlayer mMediaPlayer;
    private AudioManager audioManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_volume_settings);
        ImageView rightIcon= (ImageView) findViewById(R.id.RightIcon);
        rightIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        /** 创建MediaPlayer对象 **/
        mMediaPlayer = new MediaPlayer();
        audioManager = (AudioManager)getSystemService(Context.AUDIO_SERVICE);

        MusicSeekBar = (SeekBar)findViewById(R.id.music_seekbar);
        volume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        Musicmaxvolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        MusicSeekBar.setMax(Musicmaxvolume);
        MusicSeekBar.setProgress(volume);
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,volume,AudioManager.ADJUST_SAME);

        MusicSeekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener(){
            //第一个时OnStartTrackingTouch,在进度开始改变时执行
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            //第二个方法onProgressChanged是当进度发生改变时执行
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {

            }
            //第三个是onStopTrackingTouch,在停止拖动时执行
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int i= seekBar.getProgress();
                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,i,AudioManager.ADJUST_SAME);
                try {
                    MusicPlay();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        });

    }


    private void MusicPlay() throws IOException
    {
        mMediaPlayer.reset();
        Uri alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
        try{
            mMediaPlayer.setDataSource(this,alert);
        }catch(Exception e){
            e.printStackTrace();
        }
        //final AudioManager  audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        if(audioManager.getStreamVolume(AudioManager.STREAM_MUSIC)!=0){
            mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            try {
                mMediaPlayer.prepare();
            } catch (Exception e) {
                e.printStackTrace();
            }
            mMediaPlayer.start();

        }

    }


    @Override
    protected void onDestroy() {
        if(mMediaPlayer!=null){
            mMediaPlayer.stop();
            mMediaPlayer=null;
        }
        super.onDestroy();
    }

}

