package com.viomi.fridge.view.adapter;

import android.app.Activity;

import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.viomi.fridge.R;
import com.viomi.fridge.api.dao.AdvertInfoMessage;
import com.viomi.fridge.api.dao.InfoManager;
import com.viomi.fridge.api.dao.UserInfoMessage;
import com.viomi.fridge.util.UMUtil;
import com.viomi.fridge.view.fragment.AdvertInfosFragment;
import com.viomi.fridge.view.fragment.UserInfosFragment;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by young2 on 2017/7/12.
 */

public class UserInfosAdapter extends RecyclerView.Adapter<UserInfosAdapter.MyViewHolder> {
    private Activity mContext;
    private UserInfosFragment mFragment;
    private List<UserInfoMessage> mDataList;
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM月dd日 HH:mm");

    public UserInfosAdapter(Activity context, Fragment fragment, List<UserInfoMessage> datas){
        mDataList=datas;
        mContext=context;
        mFragment= (UserInfosFragment) fragment;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(mContext).inflate(R.layout.item_info_user, parent, false);
        UserInfosAdapter.MyViewHolder holder = new UserInfosAdapter.MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        UserInfoMessage userInfoMessage=mDataList.get(position);
        switch (mDataList.get(position).getTopic()){
            case "delivery":
                holder.LogoImageView.setImageResource(R.mipmap.icon_info_deliver);
                break;
            case "comment":
                holder.LogoImageView.setImageResource(R.mipmap.icon_info_comment);
                break;
            case "evaluation_notify":
                holder.LogoImageView.setImageResource(R.mipmap.icon_info_evaluation);
                break;
            case "customer_service":
                holder.LogoImageView.setImageResource(R.mipmap.icon_info_service);
                break;
            case "payment_notify":
                holder.LogoImageView.setImageResource(R.mipmap.icon_info_wait_play);
                break;
        }
        if(mDataList.get(position).getTitle()!=null){
            holder.titleTextView.setText((userInfoMessage.getTitle()));
        }else {
            holder.titleTextView.setVisibility(View.GONE);
        }
        if(mDataList.get(position).getContent()!=null){
            holder.contentTextView.setText((userInfoMessage.getContent()));
        }else {
            holder.contentTextView.setVisibility(View.GONE);
        }
        if(userInfoMessage.isRead()){
            holder.newPointView.setVisibility(View.GONE);
        }else {
            holder.newPointView.setVisibility(View.VISIBLE);
        }
        if(position==mDataList.size()-1){
            holder.progressEndLineView.setVisibility(View.GONE);
        }else {
            holder.progressEndLineView.setVisibility(View.VISIBLE);
        }

        holder.timeTextVIew.setText(simpleDateFormat.format(new Date(userInfoMessage.getTime()*1000)));

        if(userInfoMessage.getLinkUrl()==null){
            holder.detailTextView.setVisibility(View.GONE);
        }else {
            holder.detailTextView.setVisibility(View.GONE);
        }

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url=mDataList.get(position).getLinkUrl();
                if(url!=null&&url.length()>0){
                    UMUtil.onWebPageJump(mContext,mContext.getString(R.string.title_order_detail),url,false);
                }
            }
        });
        holder.deleteImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFragment.deleteInfo(mDataList.get(position).getInfoId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder
    {

        public View progressEndLineView;
        public SimpleDraweeView LogoImageView;
        public TextView titleTextView;
        public TextView contentTextView;
        public ImageView newPointView;
        public LinearLayout detailTextView;
        public ImageView deleteImageView;
        public TextView timeTextVIew;
        public CardView cardView;
        public MyViewHolder(View view)
        {
            super(view);
            progressEndLineView= (View) view.findViewById(R.id.step_line_end);
            LogoImageView= (SimpleDraweeView) view.findViewById(R.id.info_icon);
            titleTextView= (TextView) view.findViewById(R.id.info_title);
            contentTextView= (TextView) view.findViewById(R.id.info_desc);
            deleteImageView= (ImageView) view.findViewById(R.id.delete_view);
            detailTextView= (LinearLayout) view.findViewById(R.id.info_detail);
            newPointView= (ImageView) view.findViewById(R.id.red_point);
            timeTextVIew= (TextView) view.findViewById(R.id.time_text);
            cardView= (CardView) view.findViewById(R.id.cardview);
        }
    }
}
