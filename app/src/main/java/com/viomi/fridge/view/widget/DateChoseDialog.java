package com.viomi.fridge.view.widget;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.viomi.fridge.R;
import com.wx.wheelview.adapter.ArrayWheelAdapter;
import com.wx.wheelview.widget.WheelView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 日期选择 Dialog
 * Created by William on 2017/7/10.
 */

public class DateChoseDialog extends BaseDialog implements View.OnClickListener {

    //    private static final String TAG = DateChoseDialog.class.getSimpleName();
    private WheelView mYearWheelView, mMonthWheelView, mDayWheelView;
    private Context context;
    private int selectedYear, selectedMonth, selectedDay;
    private OnDateSelectedListener onDateSelectedListener;
    private String period;
    private List<Integer> dayList = new ArrayList<>();

    public DateChoseDialog(Context context, String period) {
        super(context);
        this.context = context;
        this.period = period;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = this.getWindow();
        if (window != null) {
            WindowManager.LayoutParams lp = window.getAttributes();
            lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(lp);
            window.setBackgroundDrawable(null);
            window.setGravity(Gravity.CENTER);
        }
        setCanceledOnTouchOutside(false);

        initView();

        initData();
    }

    private void initView() {
        mYearWheelView = (WheelView) findViewById(R.id.year_wheel);
        mMonthWheelView = (WheelView) findViewById(R.id.month_wheel);
        mDayWheelView = (WheelView) findViewById(R.id.day_wheel);

        findViewById(R.id.date_chose_cancel).setOnClickListener(this);
        findViewById(R.id.date_chose_confirm).setOnClickListener(this);
    }

    private void initData() {
        // 获取当前日期
        Calendar calendar = Calendar.getInstance();
        if (period != null) {
            long deadline = Long.valueOf(period);
            calendar.setTime(new Date(deadline));
        }
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        // WheelView Style
        WheelView.WheelViewStyle wheelStyle = new WheelView.WheelViewStyle();
        wheelStyle.selectedTextColor = Color.parseColor("#09A3A1");
        wheelStyle.textColor = Color.parseColor("#BFBFBF");
        wheelStyle.textSize = 20;
        wheelStyle.selectedTextSize = 30;
        wheelStyle.backgroundColor = Color.parseColor("#00ffffff");
        wheelStyle.holoBorderColor = Color.parseColor("#09A3A1");
        wheelStyle.holoBorderWidth = 1;

        List<Integer> yearLList = new ArrayList<>();    // 年份集合
        List<Integer> monthList = new ArrayList<>(1);   // 月份集合
        for (int i = 1900; i < 2101; i++) yearLList.add(i);
        for (int i = 1; i < 13; i++) monthList.add(i);

        // 年
        mYearWheelView.setWheelData(yearLList);
        mYearWheelView.setWheelAdapter(new ArrayWheelAdapter(context));
        mYearWheelView.setSkin(WheelView.Skin.None);  // 皮肤
        mYearWheelView.setLoop(true); // 循环滚动
        mYearWheelView.setWheelSize(5);
        mYearWheelView.setStyle(wheelStyle);
        mYearWheelView.setExtraText("年", 0xFF09A3A1, 20, 60);

        // 月
        mMonthWheelView.setWheelData(monthList);
        mMonthWheelView.setWheelAdapter(new ArrayWheelAdapter(context));
        mMonthWheelView.setSkin(WheelView.Skin.None);
        mMonthWheelView.setLoop(true);  // 循环滚动
        mMonthWheelView.setWheelSize(5);
        mMonthWheelView.setStyle(wheelStyle);
        mMonthWheelView.setExtraText("月", 0xFF09A3A1, 20, 60);

        // 日
        mDayWheelView.setWheelData(setDays(year, month));
        mDayWheelView.setWheelAdapter(new ArrayWheelAdapter(context));
        mDayWheelView.setSkin(WheelView.Skin.None);
        mDayWheelView.setLoop(true);    // 循环滚动
        mDayWheelView.setWheelSize(5);
        mDayWheelView.setStyle(wheelStyle);
        mDayWheelView.setExtraText("日", 0xFF09A3A1, 20, 60);

        // 初始化
        mYearWheelView.setSelection(yearLList.indexOf(year));
        mMonthWheelView.setSelection(monthList.indexOf(month));
        mDayWheelView.setSelection(dayList.indexOf(day));

        mYearWheelView.setOnWheelItemSelectedListener((position, o) -> {
            selectedYear = (int) o;
            mDayWheelView.setWheelData(setDays(selectedYear, selectedMonth));
//            mMonthWheelView.setWheelData(setMonthData(year, selectedYear, month));
        });

        mMonthWheelView.setOnWheelItemSelectedListener((position, o) -> {
            selectedMonth = (int) o;
            mDayWheelView.setWheelData(setDays(selectedYear, selectedMonth));
        });

        mDayWheelView.setOnWheelItemSelectedListener((position, o) -> selectedDay = (int) o);
    }

//    private List<Integer> setMonthData(int currentYear, int selectedYear, int month) {
//        monthList.clear();
//        if (selectedYear == currentYear)
//            for (int i = month; i < 13; i++) monthList.add(i);
//        else for (int i = 1; i < 13; i++) monthList.add(i);
//        return monthList;
//    }

    /**
     * 根据年 月 获取对应的月份 天数
     */
    private List<Integer> setDays(int selectedYear, int selectedMonth) {
        dayList.clear();
        Calendar a = Calendar.getInstance();
        a.set(Calendar.YEAR, selectedYear);
        a.set(Calendar.MONTH, selectedMonth - 1);
        a.set(Calendar.DATE, 1);
        a.roll(Calendar.DATE, -1);
        int maxDate = a.get(Calendar.DATE);
        for (int i = 1; i <= maxDate; i++) dayList.add(i);
//        if (currentYear == selectedYear && currentMonth == selectedMonth) {
//            Calendar a = Calendar.getInstance();
//            a.set(Calendar.YEAR, currentYear);
//            a.set(Calendar.MONTH, currentMonth - 1);
//            a.set(Calendar.DATE, 1);
//            a.roll(Calendar.DATE, -1);
//            int maxDate = a.get(Calendar.DATE);
//            for (int i = day; i <= maxDate; i++) dayList.add(i);
//        } else {
//            Calendar a = Calendar.getInstance();
//            a.set(Calendar.YEAR, selectedYear);
//            a.set(Calendar.MONTH, selectedMonth - 1);
//            a.set(Calendar.DATE, 1);
//            a.roll(Calendar.DATE, -1);
//            int maxDate = a.get(Calendar.DATE);
//            for (int i = 1; i <= maxDate; i++) dayList.add(i);
//        }
        return dayList;
    }

    @Override
    public void setView() {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_date_chose);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.date_chose_cancel:    // 取消
                dismiss();
                break;
            case R.id.date_chose_confirm:   // 确认
                if (onDateSelectedListener != null)
                    onDateSelectedListener.onDateSelected(selectedYear, selectedMonth, selectedDay);
                dismiss();
                break;
        }
    }

    public interface OnDateSelectedListener {
        void onDateSelected(int year, int month, int day);
    }

    public void setOnDateSelectedListener(OnDateSelectedListener onDateSelectedListener) {
        this.onDateSelectedListener = onDateSelectedListener;
    }

}
