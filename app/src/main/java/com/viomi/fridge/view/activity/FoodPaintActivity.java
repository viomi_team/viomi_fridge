package com.viomi.fridge.view.activity;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.larswerkman.holocolorpicker.ColorPicker;
import com.larswerkman.holocolorpicker.OpacityBar;
import com.larswerkman.holocolorpicker.ValueBar;
import com.viomi.fridge.R;
import com.viomi.fridge.device.AppConfig;
import com.viomi.fridge.device.DeviceConfig;
import com.viomi.fridge.manager.ControlManager;
import com.viomi.fridge.model.bean.FoodDetailData;
import com.viomi.fridge.util.FileUtil;
import com.viomi.fridge.util.log;
import com.viomi.fridge.view.widget.DateChoseDialog;
import com.viomi.fridge.view.widget.EraserEditDialog;
import com.viomi.fridge.view.widget.KeyboardStatusListener;
import com.viomi.fridge.view.widget.MyProgressDialog;
import com.viomi.fridge.view.widget.PaletteView;
import com.viomi.fridge.view.widget.PenEditDialog;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.lang.ref.WeakReference;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

/**
 * 食材管理绘图 Activity
 * Created by William on 2017/6/16.
 */

public class FoodPaintActivity extends BaseHandlerActivity implements ViewTreeObserver.OnGlobalLayoutListener, View.OnClickListener,
        PaletteView.DrawCallback, PaletteView.Callback, DateChoseDialog.OnDateSelectedListener, TextWatcher, KeyboardStatusListener.SoftKeyboardStateListener {

    private static final String TAG = FoodPaintActivity.class.getSimpleName();
    private static final String Path = Environment.getExternalStorageDirectory().toString() + "/viomi/";    // 保存目录
    private static final String FILE_NAME = Path + "food_manage.xml";    // 保存的文件名
    private TextView tvChill, tvChange, tvFreeze, tvGuarantee;  // 冷藏区、变温区和冷冻区选择，保质期
    private LinearLayout mLinearLayout;
    private EditText mEditText;     // 名称输入
    private ImageView ivPreview, ivUndo, ivRedo, ivPen, ivEraser;   // 绘图预览，撤销，恢复，画笔，橡皮擦
    private View lineOne, lineTwo;
    private PaletteView mPaletteView;   // 画板
    private PenEditDialog mPenEditDialog;   // 画笔工具 Dialog
    private EraserEditDialog mEraserEditDialog; // 橡皮擦 Dialog
    private MyProgressDialog mProgressDialog;   // 加载 Dialog
    private MyRunnable mRunnable;   // 保存文件线程
    private LoadRunnable mLoadRunnable = new LoadRunnable(this);
    private FoodDetailData mData = new FoodDetailData();
    private List<FoodDetailData> foodMsgList = new ArrayList<>();
    private int type = 0;   // 0:冷藏室；1:变温室；2:冷冻室
    private int size;   // 画笔大小缩放基准
    private static final int MSG_SHOW_PEN_EDIT = 1; // 显示画笔编辑
    private static final int MSG_SHOW_ERASER_EDIT = 2;  // 显示橡皮擦编辑
    private static final int MSG_SAVE_IMG_FAIL = 3; // 保存图片失败
    private static final int MSG_SAVE_FILE_FAIL = 4;    // 保存文件失败
    private static final int MSG_SAVE_FILE_SUCCESS = 5; // 保存文件成功

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_paint);

        initView();

        initData();

        initPenDialog();

        initEraserDialog();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mLinearLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
        mPaletteView.recycle();
        if (mPenEditDialog != null) {
            mPenEditDialog.dismiss();
            mPenEditDialog = null;
        }
        if (mEraserEditDialog != null) {
            mEraserEditDialog.dismiss();
            mEraserEditDialog = null;
        }
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
        if (mRunnable != null) {
            mRunnable = null;
        }
        if (mLoadRunnable != null) mLoadRunnable = null;
    }

    private void initView() {
        mLinearLayout = (LinearLayout) findViewById(R.id.food_paint_root);
        mPaletteView = (PaletteView) findViewById(R.id.food_paint_palette);
        mEditText = (EditText) findViewById(R.id.food_paint_name);
        ivPreview = (ImageView) findViewById(R.id.food_paint_preview);
        tvChill = (TextView) findViewById(R.id.food_paint_chill);
        tvChange = (TextView) findViewById(R.id.food_paint_change);
        tvFreeze = (TextView) findViewById(R.id.food_paint_freeze);
        tvGuarantee = (TextView) findViewById(R.id.food_paint_guarantee);
        lineOne = findViewById(R.id.food_paint_line1);
        lineTwo = findViewById(R.id.food_paint_line2);
        ivUndo = (ImageView) findViewById(R.id.food_paint_undo);
        ivRedo = (ImageView) findViewById(R.id.food_paint_redo);
        ivPen = (ImageView) findViewById(R.id.food_paint_pen);
        ivEraser = (ImageView) findViewById(R.id.food_paint_eraser);

        ivUndo.setEnabled(false);
        ivRedo.setEnabled(false);

        mEditText.addTextChangedListener(this);
        mPaletteView.setOnDrawCallback(this);
        mPaletteView.setCallback(this);
        tvGuarantee.setOnClickListener(this);
        ivUndo.setOnClickListener(this);
        ivRedo.setOnClickListener(this);
        ivPen.setOnClickListener(this);
        ivEraser.setOnClickListener(this);
        tvChill.setOnClickListener(this);
        tvChange.setOnClickListener(this);
        tvFreeze.setOnClickListener(this);
        findViewById(R.id.food_paint_save).setOnClickListener(this);
        findViewById(R.id.food_paint_delete).setOnClickListener(this);
        findViewById(R.id.food_paint_close).setOnClickListener(this);
        mLinearLayout.getViewTreeObserver().addOnGlobalLayoutListener(this);

        KeyboardStatusListener keyboardStatusListener = new KeyboardStatusListener(findViewById(R.id.food_paint_root));
        keyboardStatusListener.addSoftKeyboardStateListener(this);

        // 两门
        if (DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V3) || DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V31)
                || DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V4)) {
            tvChange.setVisibility(View.GONE);
            lineOne.setVisibility(View.GONE);
            lineTwo.setVisibility(View.GONE);
        }
    }

    private void initData() {
        // 画笔宽度缩放基准参数
        Drawable circleDrawable = ContextCompat.getDrawable(FoodPaintActivity.this, R.drawable.paint_pen_size_base);
        assert circleDrawable != null;
        size = circleDrawable.getIntrinsicWidth();

        type = getIntent().getIntExtra("type", 0);
        switch (type) {
            case 0: // 冷藏室
                tvChill.setBackgroundColor(0xFFFFFFFF);
                tvChill.setTextColor(0xFF4ADAB0);
                lineOne.setVisibility(View.INVISIBLE);
                break;
            case 1: // 变温室
                tvChange.setBackgroundColor(0xFFFFFFFF);
                tvChange.setTextColor(0xFF4ADAB0);
                lineOne.setVisibility(View.INVISIBLE);
                lineTwo.setVisibility(View.INVISIBLE);
                break;
            case 2:    // 冷冻室
                tvFreeze.setBackgroundColor(0xFFFFFFFF);
                tvFreeze.setTextColor(0xFF4ADAB0);
                lineTwo.setVisibility(View.INVISIBLE);
                break;
        }
        mHandler.post(mLoadRunnable);
    }

    private void initPenDialog() {
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_pen_edit_layout, mLinearLayout, false);
        ColorPicker colorPicker = (ColorPicker) view.findViewById(R.id.pen_color_picker);
        ImageView imageView = (ImageView) view.findViewById(R.id.pen_size);
        SeekBar seekBar = (SeekBar) view.findViewById(R.id.pen_size_seek);
        ValueBar valueBar = (ValueBar) view.findViewById(R.id.pen_value);
        OpacityBar opacityBar = (OpacityBar) view.findViewById(R.id.pen_opacity);
        colorPicker.addOpacityBar(opacityBar);
        colorPicker.addValueBar(valueBar);
        colorPicker.setShowOldCenterColor(false);   // 关闭显示上次颜色

        mPaletteView.setPenRawSize(Math.round((size / 100f) * 40)); // 初始化画笔粗细

        mPenEditDialog = new PenEditDialog(FoodPaintActivity.this, view, seekBar, colorPicker,
                imageView, mPaletteView, size, 40);
    }

    private void initEraserDialog() {
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_eraser_edit_layout, mLinearLayout, false);
        ImageView imageView = (ImageView) view.findViewById(R.id.eraser_size);
        SeekBar seekBar = (SeekBar) view.findViewById(R.id.eraser_size_seek);

        mPaletteView.setEraserSize(Math.round((size / 100f) * 40)); // 初始化橡皮擦粗细

        mEraserEditDialog = new EraserEditDialog(FoodPaintActivity.this, view, seekBar, imageView, mPaletteView, size, 40);
    }

    private void setGuaranteePeriod() {
        DateChoseDialog dateChoseDialog = new DateChoseDialog(FoodPaintActivity.this, null);
        dateChoseDialog.setOnDateSelectedListener(this);
        dateChoseDialog.show();
    }

    private void showPenEdit() {
        if (mPenEditDialog != null) {
            mPenEditDialog.show();
        } else initPenDialog();
    }

    private void showEraserEdit() {
        if (mEraserEditDialog != null) {
            mEraserEditDialog.show();
        } else initEraserDialog();
    }

    // 保存食材信息到文件
    private void save() {
        if (mProgressDialog == null) mProgressDialog = new MyProgressDialog(FoodPaintActivity.this);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.setMessage("正在保存,请稍候...");
        mProgressDialog.show();
        if (mRunnable == null) mRunnable = new MyRunnable(FoodPaintActivity.this);
        mHandler.post(mRunnable);
    }

    // 保存绘图
    private String saveImage(Bitmap bmp, int quality, String name) {
        if (bmp == null) return null;

        File dir = new File(Path);
        boolean isExist = dir.exists() || dir.mkdirs();

        if (isExist) {
            String fileName = name + ".jpg";
            File file = new File(dir, fileName);
            FileOutputStream fos = null;
            try {
                fos = new FileOutputStream(file);
                bmp.compress(Bitmap.CompressFormat.JPEG, quality, fos);
                fos.flush();
                return Path + fileName;
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (fos != null) {
                    try {
                        fos.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return null;
    }

    // 创建并保存数据到 xml 文件
    private void createXml(String addTime, String imgPath) {
        mData.type = String.valueOf(type);   // 存放冰箱位置
        mData.name = mEditText.getText().toString().trim().equals("") ? "未命名" : mEditText.getText().toString().trim();  // 食材名称
        mData.addTime = addTime;     // 食材添加时间
        mData.imgPath = imgPath;     // 食材绘图保存目录
        mData.isSelected = false;
        mData.isPush = "false";
        String time = tvGuarantee.getText().toString();

        try {
            // 食材到期时间
            if (time.equals("可选填")) mData.deadLine = "可选填";
            else {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                Date date = new Date(System.currentTimeMillis());
                long start = (simpleDateFormat.parse(simpleDateFormat.format(date))).getTime();
                long period = System.currentTimeMillis() - start;

                DateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                Date date1 = df.parse(time);
                mData.deadLine = String.valueOf((date1.getTime() + period));
            }

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.newDocument();

            // 创建根节点
            Element eleRoot = doc.createElement("root");
            eleRoot.setAttribute("author", "viomi");
            eleRoot.setAttribute("date", new Date().toString());
            doc.appendChild(eleRoot);

            // 创建食材根节点
            Element eleFood = doc.createElement("food");
            eleRoot.appendChild(eleFood);

            // 食材存放类型节点
            Element eleType = doc.createElement("type");
            Node nodeType = doc.createTextNode(mData.type);
            eleType.appendChild(nodeType);
            eleFood.appendChild(eleType);

            // 食材名称节点
            Element eleName = doc.createElement("name");
            Node nodeName = doc.createTextNode(mData.name);
            eleName.appendChild(nodeName);
            eleFood.appendChild(eleName);

            // 食材添加时间节点
            Element eleTime = doc.createElement("time");
            Node nodeTime = doc.createTextNode(mData.addTime);
            eleTime.appendChild(nodeTime);
            eleFood.appendChild(eleTime);

            // 食材绘图保存路径
            Element elePath = doc.createElement("path");
            Node nodePath = doc.createTextNode(mData.imgPath);
            elePath.appendChild(nodePath);
            eleFood.appendChild(elePath);

            // 食材到期时间
            Element eleDeadLine = doc.createElement("deadline");
            Node nodeDeadLine = doc.createTextNode(mData.deadLine);
            eleDeadLine.appendChild(nodeDeadLine);
            eleFood.appendChild(eleDeadLine);

            // 默认未推送
            Element elePush = doc.createElement("push");
            Node nodePush = doc.createTextNode(mData.isPush);
            elePush.appendChild(nodePush);
            eleFood.appendChild(elePush);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource domSource = new DOMSource(doc);
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");    // 编码方式
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");    // 是否缩进

            File dir = new File(Path);
            boolean isExist = dir.exists() || dir.mkdirs();
            if (isExist) {
                File file = new File(FILE_NAME);
                boolean isCreate = file.createNewFile();
                if (isCreate) {
                    FileOutputStream fileOutputStream = new FileOutputStream(file);
                    PrintWriter printWriter = new PrintWriter(fileOutputStream);
                    StreamResult streamResult = new StreamResult(printWriter);
                    transformer.transform(domSource, streamResult);
                    fileOutputStream.flush();
                    fileOutputStream.close();
                    mHandler.sendEmptyMessage(MSG_SAVE_FILE_SUCCESS);
                }
            }
        } catch (TransformerFactoryConfigurationError | Exception e) {
            e.printStackTrace();
            log.d(TAG, e.toString());
            mHandler.sendEmptyMessage(MSG_SAVE_FILE_FAIL);
        }
    }

    // 在原文件添加数据
    private void modifyXml(String addTime, String imgPath) {
        mData.type = String.valueOf(type);   // 冰箱存放位置
        mData.name = mEditText.getText().toString().trim().equals("") ? "未命名" : mEditText.getText().toString().trim();  // 食材名称
        mData.addTime = addTime;     // 食材添加时间
        mData.imgPath = imgPath;     // 食材绘图保存目录
        mData.isSelected = false;
        mData.isPush = "false";
        String time = tvGuarantee.getText().toString();
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        try {
            // 食材到期时间
            if (time.equals("可选填")) mData.deadLine = "可选填";
            else {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                Date date = new Date(System.currentTimeMillis());
                long start = (simpleDateFormat.parse(simpleDateFormat.format(date))).getTime();
                long period = System.currentTimeMillis() - start;

                DateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                Date date1 = df.parse(time);
                mData.deadLine = String.valueOf((date1.getTime() + period));
            }

            DocumentBuilder db = dbFactory.newDocumentBuilder();
            Document doc = db.parse(new File(FILE_NAME));
            Element root = (Element) doc.getFirstChild();

            // 创建食材根节点
            Element eleFood = doc.createElement("food");
            root.appendChild(eleFood);

            // 食材存放类型节点
            Element eleType = doc.createElement("type");
            Node nodeType = doc.createTextNode(mData.type);
            eleType.appendChild(nodeType);
            eleFood.appendChild(eleType);

            // 食材名称节点
            Element eleName = doc.createElement("name");
            Node nodeName = doc.createTextNode(mData.name);
            eleName.appendChild(nodeName);
            eleFood.appendChild(eleName);

            // 食材添加时间节点
            Element eleTime = doc.createElement("time");
            Node nodeTime = doc.createTextNode(mData.addTime);
            eleTime.appendChild(nodeTime);
            eleFood.appendChild(eleTime);

            // 食材绘图保存路径
            Element elePath = doc.createElement("path");
            Node nodePath = doc.createTextNode(mData.imgPath);
            elePath.appendChild(nodePath);
            eleFood.appendChild(elePath);

            // 食材到期时间
            Element eleDeadLine = doc.createElement("deadline");
            Node nodeDeadLine = doc.createTextNode(mData.deadLine);
            eleDeadLine.appendChild(nodeDeadLine);
            eleFood.appendChild(eleDeadLine);

            // 默认未推送
            Element elePush = doc.createElement("push");
            Node nodePush = doc.createTextNode(mData.isPush);
            elePush.appendChild(nodePush);
            eleFood.appendChild(elePush);

            // 因为 dom 是将一棵树整个拷进内存操作, 上面操作的只是内存中的 dom, 并没有存到硬盘中去。
            // 下面就是将内存中的 dom 树存入硬盘中
            // 第一步，用 TransformerFactory 的工厂方法得到一个 tfFactory 对象
            TransformerFactory tfFactory = TransformerFactory.newInstance();
            // 第二步，用 tfFactory 对象的 newTransformer 得到一个 Transformer 对象 tf
            Transformer tf = tfFactory.newTransformer();
            // 第三步，用 tf 对象的 transform 方法保存
            tf.setOutputProperty(OutputKeys.ENCODING, "UTF-8");    // 编码方式
            tf.setOutputProperty(OutputKeys.INDENT, "yes");    // 是否缩进
            tf.transform(new DOMSource(doc), new StreamResult(new File(FILE_NAME)));
            mHandler.sendEmptyMessage(MSG_SAVE_FILE_SUCCESS);
        } catch (ParserConfigurationException | TransformerException | IOException | SAXException | ParseException e) {
            e.printStackTrace();
            log.d(TAG, e.toString());
            mHandler.sendEmptyMessage(MSG_SAVE_FILE_FAIL);
        }
    }

    // 读取食材默认保质期
    private void loadFoodData() {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();

            // 读取文件流
            InputStream stream = getResources().getAssets().open("food_manage_data.xml");
            Document dom = builder.parse(stream);
            dom.normalize();

            Element root = dom.getDocumentElement();
            NodeList items = root.getElementsByTagName("food");   // 查找所有 food 节点

            for (int i = 0; i < items.getLength(); i++) {
                Element item = (Element) items.item(i);
                FoodDetailData data = new FoodDetailData();
                data.no_filter = item.getAttribute("no_filter");
                data.filter = item.getAttribute("filter");
                data.name = item.getFirstChild().getNodeValue();
                foodMsgList.add(data);
            }
//            for (int i = 0; i < foodMsgList.size(); i++) {
//                log.d(TAG, "name:" + foodMsgList.get(i).name + "，filter:" + foodMsgList.get(i).filter + "，no_filter:" + foodMsgList.get(i).no_filter);
//            }
        } catch (ParserConfigurationException | IOException | SAXException e) {
            e.printStackTrace();
        }
    }

    // 根据输入名称匹配保质期
    private String matchPeriod(String name) {
        log.d(TAG, "Filter least：" + ControlManager.getInstance().getFilterLifeUsePercent());
        long current = System.currentTimeMillis();
        long deadline = 0;
        if (name.contains("鱼") || name.contains("虾") || name.contains("蟹") || name.contains("蚌")) {
            if (ControlManager.getInstance().getFilterLifeUsePercent() >= 100) {     // 滤芯用完
                if (type == 0) {     // 冷藏室
                    deadline = current + 24L * 60L * 60L * 1000L;
                } else if (type == 2) {      // 冷冻室
                    deadline = current + 24L * 60L * 60L * 1000L * 89L;
                }
            } else {
                if (type == 0) {     // 冷藏室
                    deadline = current + 24L * 60L * 60L * 1000L * 4L;
                } else if (type == 2) {      // 冷冻室
                    deadline = current + 24L * 60L * 60L * 1000L * 179L;
                }
            }
        } else if (name.contains("蛋")) {
            if (type == 0) {     // 冷藏室
                deadline = current + 24L * 60L * 60L * 1000L * 20L;
            }
        } else if (name.contains("肉")) {
            if (ControlManager.getInstance().getFilterLifeUsePercent() >= 100) {     // 滤芯用完
                if (type == 0) {     // 冷藏室
                    deadline = current + 24L * 60L * 60L * 1000L;
                } else if (type == 2) {      // 冷冻室
                    deadline = current + 24L * 60L * 60L * 1000L * 89L;
                }
            } else {
                if (type == 0) {     // 冷藏室
                    deadline = current + 24L * 60L * 60L * 1000L * 4L;
                } else if (type == 2) {      // 冷冻室
                    deadline = current + 24L * 60L * 60L * 1000L * 179L;
                }
            }
        } else {
            if (type == 0) {    // 冷藏室
                for (int i = 0; i < foodMsgList.size(); i++) {
                    if (foodMsgList.get(i).name.equals(name)) {
                        long day;
                        if (ControlManager.getInstance().getFilterLifeUsePercent() >= 100) {    // 滤芯用完
                            day = Long.valueOf(foodMsgList.get(i).no_filter);
                        } else {    // 滤芯未用完
                            day = Long.valueOf(foodMsgList.get(i).filter);
                        }
                        deadline = current + 24L * 60L * 60L * 1000L * (day - 1L);
                        break;
                    }
                }
            }
        }
        String time = "";
        if (deadline != 0) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            time = sdf.format(new Date(deadline));
        }
        return time;
    }

    private void setTime() {
        String str = matchPeriod(mEditText.getText().toString().trim());
        if (!str.equals("")) {
            tvGuarantee.setText(str);
            tvGuarantee.setTextColor(0xFF333333);
        } else {
            tvGuarantee.setText("可选填");
            tvGuarantee.setTextColor(0xFFE1E1E1);
        }
    }

    @Override
    protected void processMsg(Message msg) {
        switch (msg.what) {
            case MSG_SHOW_PEN_EDIT:     // 显示画笔编辑
                showPenEdit();
                break;
            case MSG_SHOW_ERASER_EDIT:  // 显示橡皮擦编辑
                showEraserEdit();
                break;
            case MSG_SAVE_IMG_FAIL:  // 保存绘图失败
                if (mProgressDialog != null) mProgressDialog.dismiss();
                Toast.makeText(FoodPaintActivity.this, "保存绘图失败", Toast.LENGTH_SHORT).show();
                break;
            case MSG_SAVE_FILE_FAIL:    // 保存失败
                if (mProgressDialog != null) mProgressDialog.dismiss();
                Toast.makeText(FoodPaintActivity.this, "保存食材失败", Toast.LENGTH_SHORT).show();
                break;
            case MSG_SAVE_FILE_SUCCESS: // 保存成功
                if (mProgressDialog != null) mProgressDialog.dismiss();
                Toast.makeText(FoodPaintActivity.this, "保存食材成功", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent();
                intent.putExtra("new_food", mData);
                setResult(1011, intent);
                finish();
                break;
        }
    }

    @Override
    public void onGlobalLayout() {
        hideNavigationBar();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.food_paint_close: // 关闭
                finish();
                break;
            case R.id.food_paint_chill: // 冷藏室
                type = 0;
                setTime();
                tvChill.setBackgroundColor(0xFFFFFFFF);
                tvChill.setTextColor(0xFF4ADAB0);
                tvChange.setBackgroundColor(0xFF4ADAB0);
                tvChange.setTextColor(0xFFFFFFFF);
                tvFreeze.setBackgroundColor(0xFF4ADAB0);
                tvFreeze.setTextColor(0xFFFFFFFF);
                if (!DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V3) && !DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V31) && !DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V4)) {
                    lineOne.setVisibility(View.INVISIBLE);
                    lineTwo.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.food_paint_change: // 变温室
                type = 1;
                setTime();
                tvChill.setBackgroundColor(0xFF4ADAB0);
                tvChill.setTextColor(0xFFFFFFFF);
                tvChange.setBackgroundColor(0xFFFFFFFF);
                tvChange.setTextColor(0xFF4ADAB0);
                tvFreeze.setBackgroundColor(0xFF4ADAB0);
                tvFreeze.setTextColor(0xFFFFFFFF);
                if (!DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V3) && !DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V31)
                        && !DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V4)) {
                    lineOne.setVisibility(View.INVISIBLE);
                    lineTwo.setVisibility(View.INVISIBLE);
                }
                break;
            case R.id.food_paint_freeze:    // 冷冻室
                type = 2;
                setTime();
                tvChill.setBackgroundColor(0xFF4ADAB0);
                tvChill.setTextColor(0xFFFFFFFF);
                tvChange.setBackgroundColor(0xFF4ADAB0);
                tvChange.setTextColor(0xFFFFFFFF);
                tvFreeze.setBackgroundColor(0xFFFFFFFF);
                tvFreeze.setTextColor(0xFF4ADAB0);
                if (!DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V3) && !DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V31)
                        && !DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V4)) {
                    lineOne.setVisibility(View.VISIBLE);
                    lineTwo.setVisibility(View.INVISIBLE);
                }
                break;
            case R.id.food_paint_guarantee: // 保质期
                setGuaranteePeriod();
                break;
            case R.id.food_paint_undo:  // 撤销
                mPaletteView.undo();
                break;
            case R.id.food_paint_redo:  // 恢复
                mPaletteView.redo();
                break;
            case R.id.food_paint_pen:   // 画笔
                if (mPaletteView.getMode() == PaletteView.DRAW) {
                    mHandler.sendEmptyMessage(MSG_SHOW_PEN_EDIT);
                } else {
                    ivPen.setImageResource(R.mipmap.icon_food_paint_pen_active);
                    ivEraser.setImageResource(R.mipmap.icon_food_paint_eraser_normal);
                    mPaletteView.setMode(PaletteView.DRAW);
                }
                break;
            case R.id.food_paint_eraser:    // 橡皮擦
                if (mPaletteView.getMode() == PaletteView.ERASER) {
                    mHandler.sendEmptyMessage(MSG_SHOW_ERASER_EDIT);
                } else {
                    ivPen.setImageResource(R.mipmap.icon_food_paint_pen_normal);
                    ivEraser.setImageResource(R.mipmap.icon_food_paint_eraser_active);
                    mPaletteView.setMode(PaletteView.ERASER);
                }
                break;
            case R.id.food_paint_delete:    // 删除全部
                new AlertDialog.Builder(FoodPaintActivity.this)
                        .setMessage("确定删除所有绘图?")
                        .setPositiveButton("确定", (dialog, which) -> mPaletteView.clear())
                        .setNegativeButton("取消", (dialog, which) -> dialog.dismiss())
                        .setCancelable(false)
                        .show();
                break;
            case R.id.food_paint_save:  // 保存
                save();
                break;
        }
    }

    @Override
    public void onDrawChange(Bitmap bitmap) {
        ivPreview.setImageBitmap(bitmap);
    }

    @Override
    public void onUndoRedoStatusChanged() {
        if (mPaletteView.canUndo()) {
            ivUndo.setEnabled(true);
            ivUndo.setImageResource(R.mipmap.icon_food_paint_undo_active);
        } else {
            ivUndo.setEnabled(false);
            ivUndo.setImageResource(R.mipmap.icon_food_paint_undo_normal);
        }
        if (mPaletteView.canRedo()) {
            ivRedo.setEnabled(true);
            ivRedo.setImageResource(R.mipmap.icon_food_paint_redo_active);
        } else {
            ivRedo.setEnabled(false);
            ivRedo.setImageResource(R.mipmap.icon_food_paint_redo_normal);
        }
    }

    @Override
    public void onDateSelected(int year, int month, int day) {
        String mon = month < 10 ? "0" + month : String.valueOf(month);
        String Day = day < 10 ? "0" + day : String.valueOf(day);
        String date = year + "-" + mon + "-" + Day;
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Calendar calendar = Calendar.getInstance();
        String currentDate = calendar.get(Calendar.YEAR) + "-" + (calendar.get(Calendar.MONTH) + 1) + "-" + calendar.get(Calendar.DAY_OF_MONTH);
        try {
            Date dt1 = df.parse(date);
            Date dt2 = df.parse(currentDate);    // 获取当前时间
//            if (dt1.getTime() >= dt2.getTime()) {
            tvGuarantee.setText(year + "-" + mon + "-" + Day);
            tvGuarantee.setTextColor(0xFF333333);
//            } else {
//                Toast.makeText(FoodPaintActivity.this, "保质期至少为1天", Toast.LENGTH_SHORT).show();
//            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        setTime();
    }

    @Override
    public void onSoftKeyboardOpened(int keyboardHeightInPx) {
        mEditText.setCursorVisible(true);   // 显示光标
        if (mEditText.getText().toString().trim().equals("未命名"))
            mEditText.setText("");
        log.d(TAG, "Keyboard open success");
    }

    @Override
    public void onSoftKeyboardClosed() {
        mEditText.setCursorVisible(false);  // 隐藏光标
        if (mEditText.getText().toString().trim().equals(""))
            mEditText.setText("未命名");
        log.d(TAG, "Keyboard close success");
    }

    private static class MyRunnable implements Runnable {

        private WeakReference<FoodPaintActivity> weakReference;
        private FoodPaintActivity activity;

        MyRunnable(FoodPaintActivity activity) {
            this.weakReference = new WeakReference<>(activity);
            this.activity = this.weakReference.get();
        }

        @Override
        public void run() {
            String time = String.valueOf(System.currentTimeMillis());
            Bitmap bm = activity.mPaletteView.buildBitmap();
            String savePath = null;
            if (bm != null) savePath = activity.saveImage(bm, 80, time);
            else activity.mHandler.sendEmptyMessage(MSG_SAVE_IMG_FAIL);  // 图像保存失败
            if (savePath != null) {
                File file = new File(FILE_NAME);
                if (!file.exists()) activity.createXml(time, savePath); // 不存在则创建文件
                else activity.modifyXml(time, savePath);    // 在原文件修改
            } else {
                activity.mHandler.sendEmptyMessage(MSG_SAVE_IMG_FAIL);  // 图像保存失败
            }
        }
    }

    private static class LoadRunnable implements Runnable {

        private WeakReference<FoodPaintActivity> weakReference;
        private FoodPaintActivity activity;

        LoadRunnable(FoodPaintActivity activity) {
            this.weakReference = new WeakReference<>(activity);
            this.activity = this.weakReference.get();
        }

        @Override
        public void run() {
            if (activity != null) activity.loadFoodData();
        }
    }

}
