package com.viomi.fridge.view.widget;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.net.Uri;
import android.os.SystemClock;
import android.view.View;
import android.widget.RelativeLayout;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.viomi.fridge.R;

import static com.viomi.fridge.util.ApkUtil.getPackageName;

/**
 * Created by Ljh on 2017/11/28
 */

public class ClearIngDialog extends BaseDialog implements View.OnClickListener {
    private Context mContext;
    boolean mCancelable = true;
    Dialog dd = this;

    public ClearIngDialog(Context context) {
        super(context, R.style.DialogGrey);
        this.mContext = context;
    }

    @Override
    public void setView() {
        setContentView(R.layout.dialog_clearing);
        RelativeLayout root = (RelativeLayout) findViewById(R.id.root);
        root.setOnClickListener(this);
        SimpleDraweeView sImg = (SimpleDraweeView) findViewById(R.id.sImg);
        DraweeController draweeController = Fresco.newDraweeControllerBuilder()
                .setAutoPlayAnimations(true)
                .setUri(Uri.parse("res://" + getPackageName() + "/" + R.drawable.clear_g))
                .build();
        sImg.setController(draweeController);
        setCancelable(false);
    }

    @Override
    public void setCancelable(boolean flag) {
        mCancelable = flag;
        super.setCancelable(flag);
    }

    @Override
    public void onClick(View v) {
        if (mCancelable)
            this.cancel();
    }

    public void cancelDelay() {
        new Thread(() -> {
            SystemClock.sleep(3000);
            ((Activity) mContext).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    cancel();
                }
            });
        }).start();
    }
}