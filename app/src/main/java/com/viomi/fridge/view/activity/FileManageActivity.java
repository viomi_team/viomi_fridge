package com.viomi.fridge.view.activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.viomi.fridge.R;
import com.viomi.fridge.model.bean.FileInfo;
import com.viomi.fridge.util.FileUtil;
import com.viomi.fridge.util.LogUtils;
import com.viomi.fridge.util.SDCardUtils;
import com.viomi.fridge.util.ToastUtil;
import com.viomi.fridge.view.widget.ClearIngDialog;
import com.viomi.fridge.view.widget.ClearSuccessDialog;

import java.io.File;
import java.util.List;

/**
 * Created by Ljh on 2017/11/17.
 */

public class FileManageActivity extends BaseActivity {
    private ImageView imgBack;
    private TextView tvTitle;
    private TextView tvFree;
    private TextView tvTotal;
    private TextView tvClear;
    private LinearLayout llVideo;
    private TextView tvVideoInfo;
    private LinearLayout llPic;
    private TextView tvPicInfo;
    private LinearLayout llMusic;
    private TextView tvMusicInfo;
    private RelativeLayout rlMemory;
    private ClearIngDialog mClearIngDialog;
    private ClearSuccessDialog mClearSuccessDialog;
    //参数
    private long musicSize = 0;
    private long picSize = 0;
    private long videoSize = 0;
    private long totalMemory = 0, freeMemory = 0;
    //
    private static final int REQ_CLEAR = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file_manage);
        initView();
        initListener();
        init();
    }

    private void initView() {
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        tvFree = (TextView) findViewById(R.id.tvFree);
        tvTotal = (TextView) findViewById(R.id.tvTotal);
        tvClear = (TextView) findViewById(R.id.tvClear);
        llVideo = (LinearLayout) findViewById(R.id.llVideo);
        tvVideoInfo = (TextView) findViewById(R.id.tvVideoInfo);
        llPic = (LinearLayout) findViewById(R.id.llPic);
        tvPicInfo = (TextView) findViewById(R.id.tvPicInfo);
        llMusic = (LinearLayout) findViewById(R.id.llMusic);
        tvMusicInfo = (TextView) findViewById(R.id.tvMusicInfo);
        rlMemory = (RelativeLayout) findViewById(R.id.rlMemory);
        tvTitle.setText(R.string.file_manage);
        mClearIngDialog = new ClearIngDialog(mContext);
        mClearSuccessDialog = new ClearSuccessDialog(mContext);
    }

    private void init() {
        initSize();
    }

    public void initSize() {
        totalMemory = SDCardUtils.getSDCardTotalSize();
        freeMemory = SDCardUtils.getSDCardFreeSizeBit();
        if (freeMemory / 1024 / 1024 > totalMemory * 0.6) {
            rlMemory.setBackgroundResource(R.drawable.storage_green);
        } else if (freeMemory / 1024 / 1024 > totalMemory * 0.2) {
            rlMemory.setBackgroundResource(R.drawable.storage_yellow);
        } else
            rlMemory.setBackgroundResource(R.drawable.storage_red);
        tvTotal.setText("共" + SDCardUtils.bit2KbMGB(SDCardUtils.getSDCardTotalSize() * 1024 * 1024));
        tvFree.setText(SDCardUtils.bit2KbMGB(freeMemory));
        tvMusicInfo.setText(SDCardUtils.bit2KbMGB(musicSize = FileUtil.queryFileSize(mContext, FileUtil.FileType.FILE_MUSIC)));
        tvPicInfo.setText(SDCardUtils.bit2KbMGB(picSize = FileUtil.queryFileSize(mContext, FileUtil.FileType.FILE_PIC)));
        tvVideoInfo.setText(SDCardUtils.bit2KbMGB(videoSize = FileUtil.queryFileSize(mContext, FileUtil.FileType.FILE_VIDEO)));
    }

    private void initListener() {
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        tvClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {//清理缓存
                mClearIngDialog.show();
                //删除文件和清理缓存
                new AsyncTask<Void, Void, Boolean>() {
                    @Override
                    protected Boolean doInBackground(Void... params) {
                        //垃圾文件
                        List<FileInfo> mTempFiles = FileUtil.getFileInfoList(mContext, FileUtil.FileType.FILE_GARBAGE);
                        for (FileInfo temp : mTempFiles) {
                            FileUtil.deleteAll(new File(temp.getAbsolutePath()));
                        }
                        //缓存
                        FileUtil.cleanAppsCache(mContext);
                        //删除爱奇艺缓存视频和QQ音乐的缓存文件
                        FileUtil.deleteChildFile(new File("/storage/sdcard0/Android/data/com.qiyi.video.pad/files"));
                        FileUtil.deleteChildFile(new File("/storage/sdcard0/Android/data/com.tencent.qqmusicpad/files"));
                        //通知系统重新进行媒体扫描
                        Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                        intent.setData(Uri.fromFile(new File(Environment.getExternalStorageDirectory().getAbsolutePath())));
                        sendBroadcast(intent);
                        return true;
                    }

                    @Override
                    protected void onPostExecute(Boolean result) {
                        super.onPostExecute(result);
                        LogUtils.d(TAG, " the result is:" + result);
                        long preFreeMemory = freeMemory;
                        initSize();
                        if (freeMemory - preFreeMemory > 1024) {
                            new Thread(() -> {
                                SystemClock.sleep(3000);
                                ((Activity) mContext).runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        mClearIngDialog.cancel();
                                        mClearSuccessDialog.show();
                                        mClearSuccessDialog.setInfo("恭喜您，节省" + SDCardUtils.bit2KbMGB(freeMemory - preFreeMemory) + "空间");
                                    }
                                });
                            }).start();
                        } else {
                            mClearIngDialog.cancel();
                            ToastUtil.show("无可清理的垃圾");
                        }
                    }
                }.execute();
            }
        });

        llMusic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (musicSize > 0)
                    FileManageListActivity.acitonStartForResult(mContext, FileUtil.FileType.FILE_MUSIC, REQ_CLEAR);
            }
        });

        llPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (picSize > 0)
                    FileManageListActivity.acitonStartForResult(mContext, FileUtil.FileType.FILE_PIC, REQ_CLEAR);
            }
        });

        llVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (videoSize > 0)
                    FileManageListActivity.acitonStartForResult(mContext, FileUtil.FileType.FILE_VIDEO, REQ_CLEAR);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK)
            initSize();
    }
}
