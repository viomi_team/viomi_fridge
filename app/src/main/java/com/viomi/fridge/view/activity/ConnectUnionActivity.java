package com.viomi.fridge.view.activity;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.PixelFormat;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.miot.api.MiotManager;
import com.miot.common.abstractdevice.AbstractDevice;
import com.miot.common.people.People;
import com.v2.clsdk.model.CameraInfo;
import com.viomi.fridge.R;
import com.viomi.fridge.broadcast.BroadcastAction;
import com.viomi.fridge.device.AppConfig;
import com.viomi.fridge.device.MiDeviceManager;
import com.viomi.fridge.manager.CameraListManager;
import com.viomi.fridge.manager.VoiceManager;
import com.viomi.fridge.model.bean.DeviceInfo;
import com.viomi.fridge.tasks.CameraLoginTask;
import com.viomi.fridge.tasks.GetCameraListTask;
import com.viomi.fridge.util.FileUtil;
import com.viomi.fridge.util.SDKInstance;
import com.viomi.fridge.util.log;
import com.viomi.fridge.view.adapter.DeviceListAdapter;
import com.viomi.fridge.view.fragment.DefaultFragment;
import com.viomi.fridge.view.fragment.DeviceCameraFragment;
import com.viomi.fridge.view.fragment.DeviceHeatKettleFragment;
import com.viomi.fridge.view.fragment.DeviceHoodFragment;
import com.viomi.fridge.view.fragment.DevicePLMachineFragment;
import com.viomi.fridge.view.fragment.DeviceWaterPuriFierFragment;
import com.viomi.fridge.view.fragment.DeviceWaterPuriFragment;
import com.viomi.fridge.view.fragment.DeviceWaterX3Fragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.viomi.fridge.R.id.lin;

/**
 * 智能互联 Activity
 * Created by young2 on 2017/2/24.
 */

public class ConnectUnionActivity extends BaseHandlerActivity {
    private final static String TAG = ConnectUnionActivity.class.getSimpleName();
    private DeviceListAdapter mAdapter;// 列表适配器
    private List<DeviceInfo> mDevices = new ArrayList<>();
    private LocalBroadcastManager mBroadcastManager;
    private static final int REFRESH_COMPLETE = 1;
    private static final int REFRESH_VIEW = 2;
    private static final int REFRESH_COMPLETE_SMART = 3;
    private static final int ACCESS_FINE_PERMISSION_NO_ENABLE = 4;
    //    private static final int LOGIN_SUCCESS = 5;
//    private static final int GET_PEOPLE_SUCCESS = 6;
//    private static final int LOGIN_FAIL = 7;
    public static final int IPC_LOGIN_SUCCESS = 8;// 摄像头登录成功
    public static final int IPC_LOGIN_FAIL = 9;// 摄像头登录失败
    public static final int IPC_GET_DEVICE_SUCCESS = 10;// 摄像头列表获取成功
    public static final int IPC_GET_DEVICE_FAIL = 11;// 摄像头列表获取失败
    private Fragment currentFragment, defaultFragment;// 当前和默认 Fragment
    private RelativeLayout mRelativeLayout;// 刷新
    private ProgressBar mProgressBar;// 加载进度
    private boolean mIsVoiceEntry;// 是否语音进入
    private boolean mIsFoundDeviceSuccess = false;// 是否找到设备
    private DeviceInfo mCurrentDevice;// 当前设备
    private int count = 0, curPosition = -1;
    private CameraLoginTask mCameraLoginTask;
    private GetCameraListTask mGetCameraListTask;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connect_union);
        getWindow().setFormat(PixelFormat.TRANSLUCENT);
        init();
    }

    private void init() {
        FileUtil.writeTxtToFile(mContext, AppConfig.EventKey.EVENT_ENTER_UNION, null);
        final ListView listView = (ListView) findViewById(R.id.list_device);
        ImageView backIcon = (ImageView) findViewById(R.id.leftIcon);
        mProgressBar = (ProgressBar) findViewById(R.id.refresh_progress);
        mRelativeLayout = (RelativeLayout) findViewById(R.id.refresh_view);
        mAdapter = new DeviceListAdapter(this, mDevices);
        listView.setAdapter(mAdapter);
        listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);// 单选模式
        mBroadcastManager = LocalBroadcastManager.getInstance(this);

        listView.setOnItemClickListener((adapterView, view, position, l) -> {
            if (curPosition != position) {
                curPosition = position;
                DeviceInfo deviceInfo = mDevices.get(position);
                fragmentControl(deviceInfo);
            }
        });

        registerReceiver();
        // IPC SDK 初始化
        SDKInstance.init(this);
        SDKInstance.getInstance().enableDebugLog(true);// 调试日志
        // 获取设备列表
        if (mHandler != null)
            Message.obtain(mHandler, REFRESH_VIEW).sendToTarget();
        // IPC 登录
//        CameraLoginTask cameraLoginTask = new CameraLoginTask(mHandler);
//        cameraLoginTask.execute("13790455320", "zxc138ASD139");
//        mCameraLoginTask = cameraLoginTask;

        // 返回
        backIcon.setOnClickListener(view -> finish());
        // 刷新
        mRelativeLayout.setOnClickListener(view -> {
            if (mHandler != null) {
                mProgressBar.setVisibility(View.VISIBLE);
                Message.obtain(mHandler, REFRESH_VIEW).sendToTarget();
            }
        });
        // Fragment 管理
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        defaultFragment = DefaultFragment.newInstance(false);
        fragmentTransaction.add(lin, defaultFragment);
        fragmentTransaction.commit();
        currentFragment = defaultFragment;
    }

    private void upLoadEvent(DeviceInfo info) {
        AbstractDevice device = info.getAbstractDevice();
        if (device != null) {
            String model = device.getDeviceModel();
            String did = device.getDeviceId();
            String name = device.getDevice().getName();
            JSONObject object = new JSONObject();
            try {
                object.put("model", model);
                object.put("did", did);
                object.put("name", name);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            FileUtil.writeTxtToFile(mContext, AppConfig.EventKey.EVENT_FIND_UNION, object.toString());
        }
    }

    private void fragmentControl(DeviceInfo deviceInfo) {
        mCurrentDevice = deviceInfo;
        upLoadEvent(deviceInfo);
        Fragment fragment;
        DeviceWaterPuriFragment xFiveFragment;
        DeviceWaterPuriFierFragment vTwoFragment;
        DeviceWaterX3Fragment xThreeFragment;
        DeviceHoodFragment hoodFragment;// 烟机 Fragment
        DeviceCameraFragment cameraFragment;// 摄像头 Fragment
        DeviceHeatKettleFragment heatKettleFragment;// 即热饮水吧 Fragment
        DevicePLMachineFragment plMachineFragment;// 管线机 Fragment
        String model, did = "";
        if (deviceInfo.getAbstractDevice() != null) {  // 小米设备
            model = deviceInfo.getAbstractDevice().getDeviceModel();
            did = deviceInfo.getAbstractDevice().getDeviceId();
        } else model = "camera";
        // 选择
        switch (model) {
            case AppConfig.YUNMI_WATERPURI_X3: // 即热直饮净水器 X3 (100G)
            case AppConfig.YUNMI_WATERPURI_S1: // S1 优享版 智能 3 合 1 系列
            case AppConfig.YUNMI_WATERPURI_S2: // S1 标准版 智能 3 合 1 系列
                xThreeFragment = DeviceWaterX3Fragment.newInstance(did, deviceInfo.getAbstractDevice().getName(), model);
                fragment = xThreeFragment;
                break;
            case AppConfig.YUNMI_WATERPURI_X5: // 即热直饮净水器 X5 (400G)
                xFiveFragment = DeviceWaterPuriFragment.newInstance(did, deviceInfo.getAbstractDevice().getName());
                fragment = xFiveFragment;
                break;
            case AppConfig.YUNMI_WATERPURI_V1: // V1 乐享版 人工智能系列
            case AppConfig.YUNMI_WATERPURI_V2: // V1 尊享版 人工智能系列
            case AppConfig.YUNMI_WATERPURI_C1: // C1 厨上版 新鲜水系列
            case AppConfig.YUNMI_WATERPURI_C2: // C1 厨下版 新鲜水系列
            case AppConfig.YUNMI_WATERPURIFIER_V1: // 小米净水器厨上版
            case AppConfig.YUNMI_WATERPURIFIER_V2: // 小米净水器厨上版
            case AppConfig.YUNMI_WATERPURIFIER_V3: // 小米净水器厨下
            case AppConfig.YUNMI_WATERPURI_LX2: // 小米净水器厨上版
            case AppConfig.YUNMI_WATERPURI_LX3: // 小米净水器厨下版
                vTwoFragment = DeviceWaterPuriFierFragment.newInstance(did, deviceInfo.getAbstractDevice().getName(), model);
                fragment = vTwoFragment;
                break;
            case AppConfig.VIOMI_HOOD_T8: // 标准型 T 型机
            case AppConfig.VIOMI_HOOD_A6: // 标准型塔型机
            case AppConfig.VIOMI_HOOD_C6: // 经济型侧吸机
            case AppConfig.VIOMI_HOOD_A7: // 尊贵型侧吸机
            case AppConfig.VIOMI_HOOD_A5: // 标准型 T 型机
            case AppConfig.VIOMI_HOOD_A4: // 经济型侧吸机
            case AppConfig.VIOMI_HOOD_C1: // 智能油烟机 Cross
            case AppConfig.VIOMI_HOOD_H1: // 智能油烟机 hurri 标准
            case AppConfig.VIOMI_HOOD_H2: // 智能油烟机 hurri 尊享
                hoodFragment = DeviceHoodFragment.newInstance(did, deviceInfo.getAbstractDevice().getName());
                fragment = hoodFragment;
                break;
            case AppConfig.YUNMI_KETTLE_R1: // 云米智能即热饮水吧（MINI）
                heatKettleFragment = DeviceHeatKettleFragment.newInstance(did, deviceInfo.getAbstractDevice().getName());
                fragment = heatKettleFragment;
                break;
            case AppConfig.YUNMI_PL_MACHINE_MG2: // 云米 MG2 型即热管线机
                plMachineFragment = DevicePLMachineFragment.newInstance(did, deviceInfo.getAbstractDevice().getName());
                fragment = plMachineFragment;
                break;
            case "camera": // IPC
                cameraFragment = DeviceCameraFragment.newInstance(deviceInfo.getCameraInfo().getSrcId());
                fragment = cameraFragment;
                break;
            default:
                defaultFragment = DefaultFragment.newInstance(true);
                fragment = defaultFragment;
                break;
        }
        // Fragment 管理
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.remove(currentFragment).add(lin, fragment);
        fragmentTransaction.commit();
        currentFragment = fragment;
    }

    @Override
    protected void processMsg(Message msg) {
        switch (msg.what) {
            case REFRESH_VIEW:  // 开始扫描设备
                log.d(TAG, "receive REFRESH_VIEW");
                mIsFoundDeviceSuccess = false;
                refreshView();
                break;
            case REFRESH_COMPLETE_SMART:    // 找到设备
                mIsFoundDeviceSuccess = true;
                onRefreshDevice();
                break;
            case REFRESH_COMPLETE:  //
                mProgressBar.setVisibility(View.GONE);
                mAdapter.notifyDataSetChanged();
                break;
            case ACCESS_FINE_PERMISSION_NO_ENABLE:
                mRelativeLayout.setEnabled(true);
                mProgressBar.setVisibility(View.GONE);
                mAdapter.notifyDataSetChanged();
                log.myE(TAG, "receive ACCESS_FINE_PERMISSION_NO_ENABLE");
                break;
//            case LOGIN_SUCCESS: // 登陆成功
//                break;
//            case GET_PEOPLE_SUCCESS: // 获取用户信息成功
//                Log.d(TAG, "receive GET_PEOPLE_SUCCESS");
//                People mPeople = MiotManager.getPeople();
//                if (mPeople != null) {
//                    mUserText.setText("" + "用户ID:" + mPeople.getUserId());
//                } else {
//                    mUserText.setText("登录互联帐号");
//                    mUserText.setTextColor(ContextCompat.getColor(ConnectUnionActivity.this, R.color.gray));
//                }
//                refreshView();
//                break;
//            case LOGIN_FAIL:
//                Toast.makeText(ConnectUnionActivity.this, R.string.toast_login_fail, Toast.LENGTH_SHORT).show();
//                break;
            case IPC_LOGIN_SUCCESS: // IPC 登录成功
                GetCameraListTask getCameraListTask = new GetCameraListTask(mHandler);
                getCameraListTask.execute();  // 获取设备列表
                mGetCameraListTask = getCameraListTask;
                break;
            case IPC_LOGIN_FAIL:    // IPC 登录失败
                if (count < 4) {
                    SDKInstance.getInstance().logout();
                    count++;
                    CameraLoginTask cameraLoginTask = new CameraLoginTask(mHandler);
                    cameraLoginTask.execute("13790455320", "zxc138ASD139");
                    mCameraLoginTask = cameraLoginTask;
                } else {
                    count = 0;
                    mRelativeLayout.setEnabled(true);
                    mProgressBar.setVisibility(View.GONE);
                }
                break;
            case IPC_GET_DEVICE_SUCCESS:    // 设备获取成功
                mDevices.clear();
                for (CameraInfo cameraInfo : CameraListManager.getInstance().getCameraList()) {
                    DeviceInfo info = new DeviceInfo();
                    info.setCameraInfo(cameraInfo);
                    mDevices.add(info);
                }
                if (mHandler != null) mHandler.sendEmptyMessage(REFRESH_VIEW);
                break;
            case IPC_GET_DEVICE_FAIL:   // 设备获取失败
                mRelativeLayout.setEnabled(true);
                mProgressBar.setVisibility(View.GONE);
                break;
        }
    }

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction() != null) {
                log.d(TAG, "action: " + intent.getAction());
                switch (intent.getAction()) {
                    case AppConfig.ACTION_DISCOVERY_DEVICE_SUCCEED:
                        if (mHandler != null) {
                            mHandler.sendEmptyMessageDelayed(REFRESH_COMPLETE_SMART, 1000);
                        }
                        break;
                    case AppConfig.ACTION_DISCOVERY_DEVICE_FAILED:
                        if (intent.getIntExtra("errorCode", 0) == 1015) {   // 没权限导致扫描错误
                            if (mHandler != null) {
                                mHandler.sendEmptyMessageDelayed(ACCESS_FINE_PERMISSION_NO_ENABLE, 1000);
                            }
                        }
                        break;
                    case BroadcastAction.ACTION_DEVICE_CHOOSE:
                        if (!mIsFoundDeviceSuccess) {
                            VoiceManager.getInstance().startSpeak("暂未搜到设备，请稍候");
                            return;
                        }
                        if (mDevices.size() == 0) {
                            VoiceManager.getInstance().startSpeak("请您先用手机绑定智能设备");
                            return;
                        }
                        int index = intent.getIntExtra(BroadcastAction.EXTRA_DATA, 0);
                        if (index > mDevices.size()) {
                            VoiceManager.getInstance().startSpeak("您只有" + mDevices.size() + "台智能设备，请重新选择");
                            return;
                        }
                        index -= 1;
                        DeviceInfo deviceInfo = mDevices.get(index);
                        String name = "";
                        if (deviceInfo.getAbstractDevice() != null)
                            name = deviceInfo.getAbstractDevice().getName();
                        else if (deviceInfo.getCameraInfo() != null)
                            name = deviceInfo.getCameraInfo().getName();
                        VoiceManager.getInstance().startSpeak("为您选择设备：" + name);
                        fragmentControl(deviceInfo);
                        break;
                    case BroadcastAction.ACTION_WATERPURI_TEMP:
                        if (mCurrentDevice == null) {
                            VoiceManager.getInstance().startSpeak("请先选择设备");
                            return;
                        }
                        int temp_set = intent.getIntExtra(BroadcastAction.EXTRA_DATA, 0);
                        String model = "";
                        if (mCurrentDevice.getAbstractDevice() != null)
                            model = mCurrentDevice.getAbstractDevice().getDeviceModel();
                        switch (model) {
                            case AppConfig.YUNMI_WATERPURI_X3:  // 即热直饮净水器X3 (100G)
                                int minTemp = ((DeviceWaterX3Fragment) currentFragment).getMinTemp();
                                if (minTemp < 0) {
                                    VoiceManager.getInstance().startSpeak("数据刷新中，等稍候再试");
                                    return;
                                }
                                if (temp_set < minTemp) {
                                    VoiceManager.getInstance().startSpeak("温水模式不能设置低于" + minTemp + "度");
                                    return;
                                } else if (temp_set > 90) {
                                    VoiceManager.getInstance().startSpeak("温水模式不能设置高于90度");
                                    return;
                                }
                                ((DeviceWaterX3Fragment) currentFragment).tempVoiceControl(temp_set);
                                break;
                            case AppConfig.YUNMI_WATERPURI_X5:  // 即热直饮净水器X5 (100G)
                                if (temp_set < 40) {
                                    VoiceManager.getInstance().startSpeak("温水模式不能设置低于40度");
                                    return;
                                } else if (temp_set > 90) {
                                    VoiceManager.getInstance().startSpeak("温水模式不能设置高于90度");
                                    return;
                                }
                                ((DeviceWaterPuriFragment) currentFragment).tempVoiceControl(temp_set);
                                break;
                        }
                        break;
                }
            }
        }
    };

    private void registerReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(AppConfig.ACTION_BIND_SERVICE_SUCCEED);
        filter.addAction(AppConfig.ACTION_BIND_SERVICE_FAILED);
        filter.addAction(AppConfig.ACTION_DISCOVERY_DEVICE_SUCCEED);
        filter.addAction(AppConfig.ACTION_DISCOVERY_DEVICE_FAILED);
        filter.addAction(AppConfig.ACTION_DEVICE_FOUND);
        filter.addAction(AppConfig.ACTION_DEVICE_LOST);
        filter.addAction(AppConfig.ACTION_DEVICE_UPDATE);
        filter.addAction(BroadcastAction.ACTION_DEVICE_CHOOSE);
        filter.addAction(BroadcastAction.ACTION_WATERPURI_TEMP);
        mBroadcastManager.registerReceiver(mReceiver, filter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        FileUtil.writeTxtToFile(mContext, AppConfig.EventKey.EVENT_EXIT_UNION, null);
        mBroadcastManager.unregisterReceiver(mReceiver);
        SDKInstance.getInstance().logout(); // 退出登录
        if (mCameraLoginTask != null && mCameraLoginTask.getStatus() == AsyncTask.Status.RUNNING) {
            mCameraLoginTask.cancel(true);
            mCameraLoginTask = null;
        }
        if (mGetCameraListTask != null && mGetCameraListTask.getStatus() == AsyncTask.Status.RUNNING) {
            mGetCameraListTask.cancel(true);
            mGetCameraListTask = null;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
//        isLoginClick = false;
//        People mPeople = MiotManager.getPeople();
//        if (mPeople != null) {
//            mUserText.setText("" + "用户ID:" + mPeople.getUserId());
//        } else {
//            mUserText.setText("登录互联帐号");
//            mUserText.setTextColor(ContextCompat.getColor(ConnectUnionActivity.this, R.color.gray));
//        }
        mIsVoiceEntry = getIntent().getBooleanExtra("voice_entry", false);
    }

    /***
     * 开始搜索设备
     */
    private void refreshView() {
        People mPeople = MiotManager.getPeople();
        if (mPeople != null) {
            deviceScan();
        } else {
            Log.e(TAG, "not login!");
            MiDeviceManager.getInstance().clearDevices();
            mHandler.sendEmptyMessageDelayed(REFRESH_COMPLETE, 200);
        }
    }

    private void deviceScan() {
        Log.i(TAG, "deviceScan");
        MiDeviceManager.getInstance().getWanDeviceList();
    }

    /***
     * 刷新设备
     */
    private void onRefreshDevice() {
        // 小米设备集合
        List<AbstractDevice> wanDevices = MiDeviceManager.getInstance().getWanDevices();
        if (wanDevices == null) {
            Log.e(TAG, "onRefreshDevice device null");
            mProgressBar.setVisibility(View.GONE);
            mAdapter.notifyDataSetChanged();
            return;
        }
        mDevices.clear();
        for (AbstractDevice abstractDevice : wanDevices) {
            DeviceInfo info = new DeviceInfo();
            info.setAbstractDevice(abstractDevice);
            mDevices.add(info);
        }
        mAdapter.notifyDataSetChanged();
        mProgressBar.setVisibility(View.GONE);

        if (mDevices.size() > 0) {
            if (mIsVoiceEntry) {
                VoiceManager.getInstance().startSpeak("请问您选择第几台智能设备？");
            }
            mBroadcastManager.sendBroadcast(new Intent(AppConfig.ACTION_HAVE_DEVICE));
        } else {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            defaultFragment = DefaultFragment.newInstance(false);
            if (currentFragment == null) fragmentTransaction.add(lin, defaultFragment);
            else fragmentTransaction.hide(currentFragment).add(lin, defaultFragment);
            fragmentTransaction.commit();
            currentFragment = defaultFragment;
            mBroadcastManager.sendBroadcast(new Intent(AppConfig.ACTION_HAVE_NO_DEVICE));
        }
    }
}