package com.viomi.fridge.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.unilife.UMToken;
import com.unilife.common.content.beans.iqiyi.IqiyiCategoryInfo;
import com.unilife.common.content.beans.iqiyi.ResponseIqiyiCategroy;
import com.unilife.content.logic.logic.IUMLogicListener;
import com.unilife.content.logic.models.iqiyi.UMIqiyiCatalog;
import com.viomi.fridge.R;
import com.viomi.fridge.util.log;
import com.viomi.fridge.view.adapter.VideoFVPAdapter;
import com.viomi.fridge.view.fragment.VideoFragment;

import java.util.ArrayList;
import java.util.List;

public class VideoUMActivity extends BaseActivity {

    private final static String TAG = "VideoUMActivity";
    private ImageView btn_back;
    private TabLayout tab;
    private RelativeLayout search;
    private TextView search_txt;
    private ViewPager tab_vp;
    private RelativeLayout loading_layout;
    private RelativeLayout network_fail_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_um);

        initView();
        initListener();
        getUMToken();

    }


    private void initView() {
        btn_back = (ImageView) findViewById(R.id.btn_back);
        tab = (TabLayout) findViewById(R.id.tab);
        search = (RelativeLayout) findViewById(R.id.search);
        search_txt = (TextView) findViewById(R.id.search_txt);
        tab_vp = (ViewPager) findViewById(R.id.tab_vp);
        loading_layout = (RelativeLayout) findViewById(R.id.loading_layout);
        network_fail_layout = (RelativeLayout) findViewById(R.id.network_fail_layout);
    }

    private void initListener() {
        btn_back.setOnClickListener((v) -> {
            onBackPressed();
        });

        search.setOnClickListener((v) -> {
            Intent intent = new Intent(this, VideoSearchActivity.class);
            startActivity(intent);
        });

        network_fail_layout.setOnClickListener((v)->{
            disReload();
            getUMToken();
        });

    }

    private void showProgressBar() {
        loading_layout.setVisibility(View.VISIBLE);
    }

    private void disProgressBar() {
        loading_layout.setVisibility(View.GONE);
    }

    private void showReload() {
        network_fail_layout.setVisibility(View.VISIBLE);
    }

    private void disReload() {
        network_fail_layout.setVisibility(View.GONE);
    }

    private void getUMToken() {
        showProgressBar();

        UMToken.getInstance().getToken("unilife_standard_api_yunmi", "unilife_standard_api_yunmi_123456", new UMToken.IUMTokenListener() {
            @Override
            public void onSuccess(String token) {
                log.myE(TAG, "" + token + "69kl");
                getIQYCatalog(token);
            }

            @Override
            public void onFail(String msg) {
                log.myE(TAG, "getUMToken-fail");
                disProgressBar();
                showReload();
            }
        });
    }

    private void getIQYCatalog(String m_token) {
        UMIqiyiCatalog.getInstance().setRequestTag("viomi");
        UMIqiyiCatalog.getInstance().getIqiyiCatalog(m_token, new IUMLogicListener() {
            @Override
            public void onSuccess(Object data, long offset, long total) {

                disProgressBar();

                ResponseIqiyiCategroy responseIqiyiCategroy = (ResponseIqiyiCategroy) data;
                List<IqiyiCategoryInfo> infos = responseIqiyiCategroy.getInfos();
                List<VideoFragment> vlist = new ArrayList();

                log.myE(TAG, "size-----" + infos.size());

                for (int i = 0; i < infos.size(); i++) {
                    vlist.add(VideoFragment.getInstance(m_token,infos.get(i).getCategoryId(), infos.get(i).getCategoryName()));
                }
                VideoFVPAdapter adapter = new VideoFVPAdapter(getSupportFragmentManager(), vlist, infos);
                tab_vp.setAdapter(adapter);
                tab.setupWithViewPager(tab_vp);

            }

            @Override
            public void onError(String msg) {
                log.myE(TAG, "getIQYCatalog-fail");
                disProgressBar();
                showReload();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        UMIqiyiCatalog.getInstance().cancelRequest("UMFreeBuyHttpDao");
//        UMIqiyiCatalog.getInstance().cancelRequest("UMModel");
//        UMIqiyiCatalog.getInstance().cancelRequest("viomi");
    }
}
