package com.viomi.fridge.view.web.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.amap.api.location.AMapLocationClient;
import com.miot.api.MiotManager;
import com.miot.common.people.People;
import com.tencent.smtt.sdk.CookieSyncManager;
import com.viomi.fridge.R;
import com.viomi.fridge.common.GlobalParams;
import com.viomi.fridge.common.ViomiApplication;
import com.viomi.fridge.common.rxbus.BusEvent;
import com.viomi.fridge.common.rxbus.RxBus;
import com.viomi.fridge.device.AppConfig;
import com.viomi.fridge.device.AppConstants;
import com.viomi.fridge.manager.AccountManager;
import com.viomi.fridge.model.bean.QRCodeBase;
import com.viomi.fridge.util.FileUtil;
import com.viomi.fridge.util.LogUtils;
import com.viomi.fridge.util.RxSchedulerUtil;
import com.viomi.fridge.util.ToolUtil;
import com.viomi.fridge.view.activity.ScanLoginActivity;
import com.viomi.fridge.view.activity.UserSettingActivity;
import com.viomi.fridge.view.activity.VmallWebActivity;
import com.viomi.fridge.view.web.X5WebView;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.OnClick;
import rx.Subscription;

/**
 * 云米商城网页 Activity
 * Created by young2 on 2017/2/21.
 */
public class BrowserActivity extends BaseActivity implements X5WebView.OnWebViewListener {
    private final static String TAG = BrowserActivity.class.getSimpleName();
    private String mUrl;// 链接
    private boolean mIsError = false;// 是否网络错误
    private AMapLocationClient mLocationClientSingle;// 高德定位
    private X5WebView mWebView;// 浏览器
    private VMallJavaScriptInterface mJavaScriptInterface;// JS 接口
    private Subscription mSubscription;// 消息订阅
//    private LoginQRCodeDialog mLoginQRCodeDialog;// 登录对话框

    @BindView(R.id.browser_fail_layout)
    RelativeLayout mRelativeLayout;// 网络错误布局

    @BindView(R.id.browser_web)
    ViewGroup mViewGroup;// 浏览器

    @BindView(R.id.browser_progress)
    ProgressBar mProgressBar;// 进度条

    @BindView(R.id.title_bar_close)
    ImageView mCloseImageView;// 关闭

    @Override
    public void onCreate(Bundle savedInstanceState) {
        mTitle = "";
        layoutId = R.layout.activity_browser;
        super.onCreate(savedInstanceState);
        startAssistLocation();
        mUrl = getIntent().getStringExtra(AppConstants.WEB_URL);
        mWebView = new X5WebView(this, null);
        mViewGroup.addView(mWebView, new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));

        mJavaScriptInterface = new VMallJavaScriptInterface();
        mWebView.addJavascriptInterface(mJavaScriptInterface, "H5ToNative");
        mWebView.setOnPageFinishListener(this);

        if (mUrl == null) mWebView.loadUrl(AppConstants.URL_VMALL_RELEASE);
        else mWebView.loadUrl(mUrl);
        // Cookie 同步
        CookieSyncManager.createInstance(this);
        CookieSyncManager.getInstance().sync();

        mSubscription = RxBus.getInstance().subscribe(busEvent -> {
            switch (busEvent.getMsgId()) {
                case BusEvent.MSG_LOGIN_SUCCESS: // 登录成功
                    mWebView.reload();
                    break;
                case BusEvent.MSG_LOGOUT_SUCCESS: // 注销成功
                    mWebView.reload();
                    break;
            }
        });

        if (mBackImageView != null) mBackImageView.setOnClickListener(v -> {
            if (mWebView != null && mWebView.canGoBack()) mWebView.goBack();
            else finish();
        });
        FileUtil.writeTxtToFile(BrowserActivity.this, AppConfig.EventKey.EVENT_ENTER_WATER_MAP, null);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mJavaScriptInterface = null;
        if (mSubscription != null) {
            mSubscription.unsubscribe();
            mSubscription = null;
        }
//        if (mLoginQRCodeDialog != null && mLoginQRCodeDialog.isAdded()) {
//            mLoginQRCodeDialog.dismiss();
//            mLoginQRCodeDialog = null;
//        }
        if (mWebView != null) {
            mWebView.stopLoading();
            if (mViewGroup != null) mViewGroup.removeAllViews();
            mWebView.removeAllViews();
            mWebView.destroy();
            mWebView = null;
        }
        RxBus.getInstance().post(BusEvent.MSG_START_STROLL);
        FileUtil.writeTxtToFile(BrowserActivity.this, AppConfig.EventKey.EVENT_EXIT_WATER_MAP, null);
    }

    @OnClick(R.id.title_bar_close)
    public void close() { // 关闭
        finish();
    }

    @OnClick(R.id.browser_error_retry)
    public void retry() { // 重试
        mWebView.reload();
    }

    /**
     * 启动 H5 辅助定位
     */
    private void startAssistLocation() {
        if (mLocationClientSingle == null) {
            mLocationClientSingle = new AMapLocationClient(this.getApplicationContext());
        }
        mLocationClientSingle.startAssistantLocation();
    }

    @Override
    public void onReceivedTitle(String title) {
        if (mTitleTextView != null) mTitleTextView.setText(title);
        if (mWebView.getX5WebViewExtension() == null && title.equals("找不到网页")) { // 系统内核
            mIsError = true;
            mRelativeLayout.setVisibility(View.VISIBLE);
            mWebView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onProgressChanged(int progress) {
        mProgressBar.setVisibility(View.VISIBLE);
        mProgressBar.setProgress(progress);
        // 优化用户体验
        if (progress >= 80) mProgressBar.setVisibility(View.GONE);
        if (progress >= 30 && !mIsError) {
            if (mRelativeLayout.getVisibility() == View.VISIBLE)
                mRelativeLayout.setVisibility(View.GONE);
            if (mWebView.getVisibility() == View.GONE) mWebView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onPageStarted(String url) {
        mIsError = false;
        mProgressBar.setVisibility(View.VISIBLE);
        if (!url.contains("mi-ae.net")) {
            mWebView.stopLoading();
            mWebView.loadUrl(mUrl);
        }
    }

    @Override
    public void onPageFinished(String url) {
        mUrl = url;
        mProgressBar.setVisibility(View.GONE);
        if (mWebView.canGoBack()) mCloseImageView.setVisibility(View.VISIBLE);
        else mCloseImageView.setVisibility(View.GONE);
    }

    @Override
    public void onReceivedError() {
        if (mWebView.getX5WebViewExtension() == null) { // 系统内核
            mIsError = true;
            mRelativeLayout.setVisibility(View.VISIBLE);
            mWebView.setVisibility(View.GONE);
        }
    }

    private class VMallJavaScriptInterface {
        /**
         * 清除帐号信息
         */
        @JavascriptInterface
        public void onClearAcount() {
            LogUtils.d(TAG, "onClearAcount");
            AccountManager.deleteViomiUser(ViomiApplication.getContext());
//            ManageRepository.getInstance().logout()
//                    .compose(RxSchedulerUtil.SchedulersTransformer1())
//                    .subscribe(aBoolean -> {
//                        if (aBoolean) {
//                            ToolUtil.saveObject(ViomiApplication.getContext(), AppConstants.USER_INFO_FILE, null);
//                            mWebView.reload();
//                        }
//                    });
        }

        /**
         * 打开新 H5 页面
         *
         * @param title 新页面标题
         * @param url   新页面链接
         */
        @JavascriptInterface
        public void onWebPageJump(String title, String url) {
            LogUtils.d(TAG, "onWebPageJump");
            runOnUiThread(() -> {
                Intent intent = new Intent(BrowserActivity.this, BrowserActivity.class);
                intent.putExtra("url", url);
                startActivity(intent);
            });
        }

        /**
         * 关闭当前页面，返回上一页面
         */
        @JavascriptInterface
        public void onWebPageReturn() {
            LogUtils.d(TAG, "onWebPageReturn");
            runOnUiThread(BrowserActivity.this::finish);
        }

        /**
         * 跳转到登陆页面
         */
        @JavascriptInterface
        public void onLoginPageJump() {
            LogUtils.d(TAG, "onLoginPageJump");
            runOnUiThread(() -> {
                Intent intent = new Intent(BrowserActivity.this, ScanLoginActivity.class);
                startActivity(intent);
//                if (mLoginQRCodeDialog == null) mLoginQRCodeDialog = new LoginQRCodeDialog();
//                if (mLoginQRCodeDialog.isAdded()) return;
//                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
//                mLoginQRCodeDialog.show(fragmentTransaction, TAG);
            });
        }

        /**
         * 获取城市名称
         */
        @JavascriptInterface
        public String getCityName() {
            LogUtils.d(TAG, "getCityName");
//            return FridgePreference.getInstance().getCity();
            return GlobalParams.getInstance().getLocationCityName();
        }

        /**
         * 获取城市编码
         */
        @JavascriptInterface
        public String getCityCode() {
            LogUtils.d(TAG, "getCityCode");
//            return FridgePreference.getInstance().getCityCode();
            return GlobalParams.getInstance().getLocationCityCode();
        }

        /**
         * 获取用户信息
         * return 未登录，返回 null;已登陆返回 json 字符串
         */
        @JavascriptInterface
        public String getUserInfo() {
            LogUtils.d(TAG, "getUserInfo");
            QRCodeBase qrCodeBase = (QRCodeBase) FileUtil.getObject(BrowserActivity.this, "ViomiUser.dat");
            if (qrCodeBase == null) {
                return null;
            }
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("account", qrCodeBase.getLoginQRCode().getUserInfo().getAccount());
                jsonObject.put("userCode", qrCodeBase.getLoginQRCode().getUserInfo().getUserCode());
                jsonObject.put("token", qrCodeBase.getLoginQRCode().getUserInfo().getToken());
                jsonObject.put("cid", qrCodeBase.getLoginQRCode().getUserInfo().getCid());
                People mPeople = MiotManager.getPeople();
                if (mPeople != null && !TextUtils.isEmpty(mPeople.getUserId())) {
                    jsonObject.put("miid", mPeople.getUserId());
                }
                return jsonObject.toString();
            } catch (JSONException e) {
                LogUtils.d(TAG, e.getMessage());
                e.printStackTrace();
                return null;
            }
        }

        // 编辑器关闭时调用，隐藏系统底部导航栏
        @JavascriptInterface
        public void hideBottomNavigation() {
            LogUtils.d(TAG, "hideBottomNavigation");
            runOnUiThread(() -> {
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
                int uiFlags = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                        | View.SYSTEM_UI_FLAG_FULLSCREEN; // hide status bar

                if (Build.VERSION.SDK_INT >= 19) {
                    uiFlags |= 0x00001000;    //SYSTEM_UI_FLAG_IMMERSIVE_STICKY: hide navigation bars - compatibility: building API level is lower thatn 19, use magic number directly for higher API target level
                } else {
                    uiFlags |= View.SYSTEM_UI_FLAG_LOW_PROFILE;
                }
                try {
                    getWindow().getDecorView().setSystemUiVisibility(uiFlags);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        }

        // 有数据更新是清除本地缓存，H5 调用
        @JavascriptInterface
        public void clearNativeCache() {
            LogUtils.d(TAG, "clearNativeCache");
            runOnUiThread(() -> {
                LogUtils.d(TAG, "clearNativeCache");
                mWebView.clearCache(true);
            });
        }

        // H5 加载失败后调用
        @JavascriptInterface
        public void loadFail() {
            LogUtils.d(TAG, "loadFail");
            runOnUiThread(() -> LogUtils.d(TAG, "loadFail"));
        }

        // 是否显示商城标题栏
        @JavascriptInterface
        public boolean isH5TilteBarShow() {
            return false;
        }
    }
}
