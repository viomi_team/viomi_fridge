package com.viomi.fridge.view.adapter;

import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.viomi.fridge.R;
import com.viomi.fridge.model.bean.BleDevice;
import com.viomi.fridge.util.LogUtils;

import java.util.List;

/**
 * Created by Ljh on 2017/10/23
 */
public class BleAdapter extends BaseAdapter {
    private Context context;
    private List<BleDevice> list;

    public BleAdapter(Context context, List<BleDevice> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public Object getItem(int position) {
        return list == null ? 0 : list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = View.inflate(context, R.layout.item_bluetooth, null);
            holder = new ViewHolder();
            holder.tvName = (TextView) convertView.findViewById(R.id.tvName);
            holder.tvStatus = (TextView) convertView.findViewById(R.id.tvStatus);
            holder.imgType = (ImageView) convertView.findViewById(R.id.imgType);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        //
        holder.tvName.setText(list.get(position).getBluetoothDevice().getName());
        if (list.get(position).getBluetoothDevice().getBondState() == BluetoothDevice.BOND_BONDED) {//绑定了的设备
            holder.tvStatus.setText(list.get(position).isConnected() ? "已连接" : "未连接");
        } else {
            holder.tvStatus.setText("");
        }
        LogUtils.w(context.getClass().getSimpleName(), "the position is:" + position + " the name is:" + list.get(position).getBluetoothDevice().getName() +
                "   " + list.get(position)
                .getBluetoothDevice().getBluetoothClass().getMajorDeviceClass
                        ());
        if (list.get(position).getBluetoothDevice().getBluetoothClass().getMajorDeviceClass() == BluetoothClass.Device.Major.PHONE)
            holder.imgType.setImageResource(R.drawable.phone);
        else
            holder.imgType.setImageResource(R.drawable.earphone);

        return convertView;
    }

    class ViewHolder {
        TextView tvName, tvStatus;
        ImageView imgType;
    }
}
