package com.viomi.fridge.view.activity;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.viomi.fridge.R;
import com.viomi.fridge.api.http.HttpConnect;
import com.viomi.fridge.common_api.HttpApi;
import com.viomi.fridge.manager.SerialManager;
import com.viomi.fridge.util.ToastUtil;
import com.viomi.fridge.util.log;

import java.util.HashMap;
import java.util.Map;

public class FeedbackActivity extends BaseActivity {

    public static final int INT = 14;
    private ImageView back_icon;
    private EditText phone_num;
    private EditText feedback_txt;
    private Button submit;
    private Handler mhandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    loading_layout.setVisibility(View.GONE);
                    ToastUtil.show("提交成功！");
                    log.myE("test0", "" + msg.obj);
                    finish();
                    break;
                case 1:
                    loading_layout.setVisibility(View.GONE);
                    ToastUtil.show("提交失败！");
                    log.myE("test1", "" + msg.obj);
                    break;
            }
        }
    };
    private RelativeLayout loading_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);

        back_icon = (ImageView) findViewById(R.id.back_icon);
        phone_num = (EditText) findViewById(R.id.phone_num);
        feedback_txt = (EditText) findViewById(R.id.feedback_txt);
        submit = (Button) findViewById(R.id.submit);
        loading_layout = (RelativeLayout) findViewById(R.id.loading_layout);

        initListener();
    }


    private void initListener() {
        back_icon.setOnClickListener((v -> {
            onBackPressed();
        }));

        phone_num.setOnTouchListener(onTouchListener);
        feedback_txt.setOnTouchListener(onTouchListener);

        submit.setOnClickListener((view) -> {
            submitFeedback();
        });


    }

    private View.OnTouchListener onTouchListener = (v, e) -> {
        new Thread(
                () -> {
                    SystemClock.sleep(100);
                    runOnUiThread(
                            () -> {
                                hideNavigationBar();
                            }
                    );
                }
        ).start();
        return false;
    };


    private void submitFeedback() {

        String numString = phone_num.getText().toString();
        String feedbackString = feedback_txt.getText().toString();

        if (numString.length() != 11) {
            ToastUtil.show("请输入11位手机号码");
            return;
        }

        if (feedbackString.length() < 6) {
            ToastUtil.show("反馈内容不能少于6个字");
            return;
        }


        Map<String, String> map = new HashMap<>();
        map.put("feedback", feedbackString);
        map.put("sourceChannel", "9");
        map.put("appVersion", getVersion());
        map.put("contactWay", "" + numString);

        loading_layout.setVisibility(View.VISIBLE);
        HttpApi.postRequestHandler(HttpConnect.FEEDBACK, map, mhandler, 0, 1);
    }


    public String getVersion() {
        try {
            PackageManager manager = this.getPackageManager();
            PackageInfo info = manager.getPackageInfo(this.getPackageName(), 0);
            String version = info.versionName;
            return "" + version;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

}
