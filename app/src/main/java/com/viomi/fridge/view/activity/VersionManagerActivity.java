package com.viomi.fridge.view.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.os.PowerManager;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.viomi.common.callback.AppCallback;
import com.viomi.fridge.R;
import com.viomi.fridge.common.GlobalParams;
import com.viomi.fridge.device.AppConfig;
import com.viomi.fridge.device.DeviceConfig;
import com.viomi.fridge.manager.ControlManager;
import com.viomi.fridge.manager.H5UrlManager;
import com.viomi.fridge.manager.UpgradeManager;
import com.viomi.fridge.util.ApkUtil;
import com.viomi.fridge.util.PhoneUtil;
import com.viomi.fridge.util.log;
import com.viomi.fridge.view.activity.BaseActivity;

import okhttp3.Call;

/**
 * Created by young2 on 2017/2/21.
 */

public class VersionManagerActivity extends BaseHandlerActivity {

    private final  static String TAG=VersionManagerActivity.class.getSimpleName();
    private ProgressDialog progressDialog;
    private TextView mVmallVersion;
    private boolean mAppUpgradeFlag,mSystemUpgradeFlag;
    private Call mCall;

//    private PowerManager.WakeLock mWakeLock;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_version_manager);
        init();
    }

    private void init(){
        mAppUpgradeFlag=getIntent().getBooleanExtra("app_new",false);
        mSystemUpgradeFlag=getIntent().getBooleanExtra("system_new",false);

        TextView system_version_new= (TextView) findViewById(R.id.system_version_new);
        TextView app_version_new= (TextView) findViewById(R.id.app_version_new);
        if(mAppUpgradeFlag){
            app_version_new.setVisibility(View.VISIBLE);
        }
        if(mSystemUpgradeFlag){
            system_version_new.setVisibility(View.VISIBLE);
        }
        RelativeLayout appVersionView= (RelativeLayout) findViewById(R.id.app_version_view);
        RelativeLayout systemVersionView= (RelativeLayout) findViewById(R.id.system_version_view);
        RelativeLayout vmallVersionView= (RelativeLayout) findViewById(R.id.vmall_version_view);
        RelativeLayout mcuVersionView= (RelativeLayout) findViewById(R.id.mcu_version_view);
        if(DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V2)||DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V3)
                ||DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V31)){
            mcuVersionView.setVisibility(View.GONE);
        }

        TextView appVersion= (TextView) findViewById(R.id.app_version);
        mVmallVersion= (TextView) findViewById(R.id.vmall_version);
        mVmallVersion.setText( "V"+H5UrlManager.getH5Version());
        ImageView leftIcon= (ImageView) findViewById(R.id.leftIcon);
        leftIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        appVersion.setText("V"+ApkUtil.getVersionCode());
        TextView systemVersion= (TextView) findViewById(R.id.sys_version);
        systemVersion.setText( PhoneUtil.getSystemVersionCodeStr());
        TextView mcuVersion= (TextView) findViewById(R.id.mcu_version);
        mcuVersion.setText("V"+ControlManager.getInstance().getDataReceiveInfo().model+ControlManager.getInstance().getDataReceiveInfo().version);
        appVersionView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(VersionManagerActivity.this,UpgradeActivity.class);
                intent.putExtra("type",0);
                startActivity(intent);
            }
        });
        systemVersionView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent=new Intent(VersionManagerActivity.this,UpgradeActivity.class);
//                intent.putExtra("type",1);
//                startActivity(intent);
            }
        });
        vmallVersionView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                updateVmall();
            }
        });
        mcuVersionView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Toast.makeText(VersionManagerActivity.this,"固件升级",Toast.LENGTH_SHORT).show();
//                UpgradeManager.getInstance().checkMcuUpgrade(VersionManagerActivity.this,UpgradeManager.OPERATE_AUTO);
            }
        });

//        PowerManager pm = (PowerManager)getSystemService(Context.POWER_SERVICE);//得到电源管理器对象
//        mWakeLock = pm.newWakeLock(PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.FULL_WAKE_LOCK, "TEST");
//        mWakeLock.acquire();

    }


    private void updateVmall() {
        if(progressDialog!=null){
            progressDialog.show(this, "提示", "正在升级中，请稍侯",false, true);
        }

        if(mCall!=null){
            mCall.cancel();
            mCall=null;
        }
        mCall= H5UrlManager.getInstance().updateVmallH5Zip(new AppCallback<String>() {
            @Override
            public void onSuccess(String data) {
                if(progressDialog!=null){
                    progressDialog.dismiss();
                    progressDialog=null;
                }
                log.d(TAG, "updateVmallH5Zip response， msg:" + data);
                vmallH5Process();
                mVmallVersion.setText( "V"+H5UrlManager.getH5Version());
                if(mHandler!=null){
                    mHandler.sendEmptyMessage(100);
                }
            }

            @Override
            public void onFail(int errorCode, String msg) {
                if(progressDialog!=null){
                    progressDialog.dismiss();
                    progressDialog=null;
                }
                Log.d(TAG, "updateVmallH5Zip fail，errorCode:" + errorCode + ",msg:" + msg);
            }
        });
    }

    /***
     * 处理商城h5文件
     */
    private void vmallH5Process(){

        if(!H5UrlManager.vmallH5Process()){
            Log.e(TAG,"vmallH5Process result false!!!");
        }
        GlobalParams.Version_Vmall_H5= H5UrlManager.getH5Version();
        GlobalParams.getInstance().setVmallH5Version(GlobalParams.Version_Vmall_H5);
        log.d(TAG,"Version_Vmall_H5="+GlobalParams.Version_Vmall_H5);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(progressDialog!=null){
            progressDialog.dismiss();
            progressDialog=null;
        }
//        if(mWakeLock!=null){
//            mWakeLock.release();
//        }

    }

    @Override
    protected void processMsg(Message msg) {
        switch (msg.what){
            case 100:
                Toast.makeText(VersionManagerActivity.this,"版本已更新",Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
