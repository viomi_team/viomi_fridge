package com.viomi.fridge.view.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.viomi.fridge.R;


public class SmallRadioView extends LinearLayout {
    private int leftMargin, rightMargin;
    private int checked, unchecked, count;
    private LayoutParams layoutParams;
    private Context context;
    private int index = 0;

    public SmallRadioView(Context context) {
        this(context, null);
    }

    public SmallRadioView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SmallRadioView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        pareeAttr(attrs);
        initView();
    }

    private void pareeAttr(AttributeSet attrs) {
        TypedArray typedArray = getResources().obtainAttributes(attrs, R.styleable.SmallRadioView);
        count = typedArray.getInteger(R.styleable.SmallRadioView_count, 0);
        checked = typedArray.getResourceId(R.styleable.SmallRadioView_checked, 0);
        unchecked = typedArray.getResourceId(R.styleable.SmallRadioView_unchecked, 0);
        leftMargin = (int) typedArray.getDimension(R.styleable.SmallRadioView_leftMargin, 0);
        rightMargin = (int) typedArray.getDimension(R.styleable.SmallRadioView_rightMargin, 0);
        typedArray.recycle();
    }

    private void initView() {
        layoutParams =
                new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.leftMargin = this.leftMargin;
        layoutParams.rightMargin = this.rightMargin;
        setGravity(Gravity.CENTER);
        if (count > 0) {
            this.removeAllViews();
            for (int i = 0; i < count; i++) {
                ImageView iv = new ImageView(context);
                if (i == index) {
                    iv.setImageResource(checked);
                    iv.setTag('t');
                } else {
                    iv.setImageResource(unchecked);
                    iv.setTag("f");
                }
                iv.setLayoutParams(layoutParams);
                this.addView(iv);
            }
        }
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
        initView();
    }

    public void setIndex(int index) {
        this.index = index;
        initView();
    }
}
