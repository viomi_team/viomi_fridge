package com.viomi.fridge.view.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.google.gson.JsonArray;
import com.viomi.fridge.R;
import com.viomi.fridge.device.AppConfig;
import com.viomi.fridge.model.bean.recipe.RecipeDetail;
import com.viomi.fridge.model.bean.recipe.RecipeModel;
import com.viomi.fridge.model.bean.recipe.Step;
import com.viomi.fridge.model.bean.xunfei.XFMenuResp;
import com.viomi.fridge.mvp.presenter.RecipeDetailsPresenter;
import com.viomi.fridge.mvp.view.RecipeDetailsView;
import com.viomi.fridge.util.FileUtil;
import com.viomi.fridge.util.ImgUtil;
import com.viomi.fridge.util.JsonUitls;
import com.viomi.fridge.util.ToolUtil;
import com.viomi.fridge.view.adapter.MethodAdapter;
import com.viomi.fridge.view.widget.CollapsibleTextView;
import com.viomi.fridge.view.widget.ExpandTextView;
import com.viomi.fridge.view.widget.MyListView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecipesDetailsActivity extends BaseActivity implements RecipeDetailsView {

    @BindView(R.id.back_icon)
    public ImageView back_icon;
    @BindView(R.id.title_name)
    public TextView title_name;

    @BindView(R.id.iv_img)
    public ImageView iv_img;

    @BindView(R.id.tv_name)
    public TextView tv_name;

    @BindView(R.id.tv_descrip)
    public ExpandTextView tv_descrip;

//    @BindView(R.id.tv_sumary)
//    public TextView tv_sumary;

    @BindView(R.id.tv_ingredients)
    public TextView tv_ingredients;

    @BindView(R.id.ll_methods)
    public LinearLayout ll_methods;

    @BindView(R.id.ll_ingredients)
    public LinearLayout ll_ingredients;

    @BindView(R.id.lv_methods)
    public MyListView lv_methods;

    private MethodAdapter adapter;

    RecipeDetailsPresenter presenter = null;
    private String id;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_details);
        ButterKnife.bind(this);
        initdata();
    }

    private void initdata() {
        adapter = new MethodAdapter(mContext);
        lv_methods.setAdapter(adapter);
        lv_methods.setFocusable(false);
        presenter = new RecipeDetailsPresenter(this);
        title_name.setText("菜谱详情");
        back_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        getCategory();
    }

    private void getCategory() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            id = bundle.getString("MenuId", "");
        }
        if (!TextUtils.isEmpty(id)) {
            HashMap<String, String> map = new HashMap<>();
            map.put("key", getString(R.string.app_key));
            map.put("id", id);
            presenter.getDetails(map);
        } else {
            String json = bundle.getString("json", "");
            if (!TextUtils.isEmpty(json)) {
                RecipeModel result = JSON.parseObject(json, RecipeModel.class);
                if (result != null) {
                    bindDatas(result);
                }
            }
//            result= (XFMenuResp.Result) getIntent().getExtras().getSerializable("obj");
//            if (result!=null){
//                bindDatas();
//            }
        }
    }

    private void bindDatas(RecipeModel recipeModel) {
        String img = recipeModel.imgUrl;
        if (!TextUtils.isEmpty(img)) {
            ImgUtil.showDefinedImage(img, iv_img, R.drawable.zf_default_message_image);
        }
//        tv_descrip.setDesc(recipeModel.tag, TextView.BufferType.NORMAL);
        if (!TextUtils.isEmpty(recipeModel.tag)) {
            tv_descrip.setText(ToolUtil.formatUrlString(recipeModel.tag));
        }
        tv_descrip.setVisibility(TextUtils.isEmpty(recipeModel.tag) ? View.GONE : View.VISIBLE);


        tv_name.setText(recipeModel.title);
        JSONObject object=new JSONObject();
        try {
            object.put("name",recipeModel.title);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        FileUtil.writeTxtToFile(mContext, AppConfig.EventKey.EVENT_WATCH_RECEIPE, object.toString());
        String str = recipeModel.ingredient;
        if (!TextUtils.isEmpty(str) && !str.equals("-")) {
            String ingredients = getString(str);
            tv_ingredients.setText(ingredients);
        } else {
            ll_ingredients.setVisibility(View.GONE);
        }
        ArrayList<Step> steps = changList(recipeModel.getStepsWithImg());
        if (steps != null && steps.size() != 0) {
            adapter.initData(steps);
        } else {
            ll_methods.setVisibility(View.GONE);
        }
    }


    private ArrayList<Step> changList(List<RecipeModel.StepMethod> stepMethods) {
        ArrayList<Step> steps = new ArrayList<>();
        for (int i = 0; i < stepMethods.size(); i++) {
            RecipeModel.StepMethod stepMethod = stepMethods.get(i);
            Step step = new Step();
            step.setStep(stepMethod.getContent());
            step.setImg(stepMethod.getImage());
            steps.add(step);
        }
        return steps;
    }

    @Override
    public void showProgress() {
        showLoading();
    }

    @Override
    public void hideProgress() {
        hideLoading();
    }

    @Override
    public void loadData(RecipeDetail data) {
        if (data != null) {
            RecipeDetail.recipe recipe = data.getRecipe();
            if (recipe != null) {
                String img = recipe.getImg();
                if (!TextUtils.isEmpty(img)) {
                    ImgUtil.showDefinedImage(img, iv_img, R.drawable.zf_default_message_image);
                }

//                tv_descrip.setText(recipe.getSumary());
//                tv_descrip.setDesc(recipe.getSumary(), TextView.BufferType.NORMAL);

                String content = recipe.getSumary();
                if (!TextUtils.isEmpty(content)) {
                    tv_descrip.setText(ToolUtil.formatUrlString(content));
                }
                tv_descrip.setVisibility(TextUtils.isEmpty(content) ? View.GONE : View.VISIBLE);

                tv_name.setText(data.getName());
                JSONObject object=new JSONObject();
                try {
                    object.put("name",data.getName());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                FileUtil.writeTxtToFile(mContext, AppConfig.EventKey.EVENT_WATCH_RECEIPE, object.toString());
                String str = recipe.getIngredients();
                if (!TextUtils.isEmpty(str) && !str.equals("-")) {
                    String ingredients = getString(str);
                    tv_ingredients.setText(ingredients);
                } else {
                    ll_ingredients.setVisibility(View.GONE);
                }
//                ArrayList<Step> steps = recipe.getMethod();
                ArrayList<Step> steps = recipe.getSteps();
                if (steps != null && steps.size() != 0) {
                    adapter.initData(steps);
                } else {
                    ll_methods.setVisibility(View.GONE);
                }
            }
        }
    }

    @Override
    public void onError(String error) {
        showToast(error, false);
    }


    private String getString(String Str) {
        String str = Str.substring(2, Str.length() - 2);
        if (str.contains(",")) {
            StringBuilder sb = new StringBuilder();
            String[] strings = str.split(",");
            for (int i = 0; i < strings.length; i++) {
                sb.append(strings[i])
                        .append("\n");
            }
            return sb.toString();
        } else {
            return str;
        }
    }
}
