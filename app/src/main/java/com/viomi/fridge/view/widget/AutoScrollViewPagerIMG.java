package com.viomi.fridge.view.widget;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.viomi.fridge.albumhttp.AlbumActivity;

import java.util.ArrayList;
import java.util.List;


public class AutoScrollViewPagerIMG extends ViewPager {

    private long delayTime;
    private static final long DEFAULT_DELAY_TIME = 4000;
    private List<Uri> mDatas;
    private List<Integer> mDefaultDatas;
    private MyHandler handler;
    private OnViewPageChangeListener onViewPageChangeListener;
    private CustomViewPagerAdapter adapter;
    private static final int MSG_AUTO_SCROLL = 0;
    private List<ImageView> imgList;



    private class MyHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_AUTO_SCROLL:
                    setCurrentItem(getCurrentItem() + 1);
                    sendEmptyMessageDelayed(MSG_AUTO_SCROLL, delayTime);
                    break;
            }
        }
    }

    public interface OnViewPageChangeListener {
        void onPageScrolled(int position, float positionOffset, int positionOffsetPixels);

        void onPageSelected(int position);

        void onPageScrollStateChanged(int state);
    }


    public AutoScrollViewPagerIMG(Context context) {
        this(context, null);
    }

    public AutoScrollViewPagerIMG(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {
        handler = new MyHandler();
        imgList = new ArrayList<>();

        for (int i = 0; i < 4; i++) {
            ImageView imageView = new ImageView(context);
            imageView.setLayoutParams(new ViewGroup.LayoutParams(this.getWidth(), this.getHeight()));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setOnClickListener((view) -> {
                Intent intent = new Intent(context, AlbumActivity.class);
                context.startActivity(intent);
            });
            imgList.add(imageView);
        }
    }


    public void setCustomAdapterDatas(List<Uri> datas) {
        this.mDatas = datas;
        adapter = new CustomViewPagerAdapter(datas);
        this.setAdapter(adapter);
        setListener();
    }

    public void setDefualtAdapterDatas(List<Integer> mDefaultDatas) {
       this. mDefaultDatas = mDefaultDatas;
        DefaultViewPagerAdapter defaultAdapter = new DefaultViewPagerAdapter(mDefaultDatas);
        this.setAdapter(defaultAdapter);
        setDefaultListener();
    }

    public void setOnViewPageChangeListener(OnViewPageChangeListener onViewPageChangeListener) {
        this.onViewPageChangeListener = onViewPageChangeListener;
    }

    private void setListener() {
        if (onViewPageChangeListener != null) {
            this.addOnPageChangeListener(new OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    onViewPageChangeListener.onPageScrolled(position % mDatas.size(), positionOffset, positionOffsetPixels);
                }

                @Override
                public void onPageSelected(int position) {
                    onViewPageChangeListener.onPageSelected(position % mDatas.size());
                }

                @Override
                public void onPageScrollStateChanged(int state) {
                    onViewPageChangeListener.onPageScrollStateChanged(state);
                }
            });
        }
    }

    private void setDefaultListener() {
        if (onViewPageChangeListener != null) {
            this.addOnPageChangeListener(new OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    onViewPageChangeListener.onPageScrolled(position % mDefaultDatas.size(), positionOffset, positionOffsetPixels);
                }

                @Override
                public void onPageSelected(int position) {
                    onViewPageChangeListener.onPageSelected(position % mDefaultDatas.size());
                }

                @Override
                public void onPageScrollStateChanged(int state) {
                    onViewPageChangeListener.onPageScrollStateChanged(state);
                }
            });
        }
    }


    public void setDelayTime(long delayTime) {
        this.delayTime = delayTime;
    }


    public void startAutoScroll() {
        if (delayTime != 0) {
            startAutoScroll(delayTime);
        } else {
            startAutoScroll(DEFAULT_DELAY_TIME);
        }
    }

    /**
     * 开始划
     */
    public void startAutoScroll(long delayTime) {
            this.delayTime = delayTime;
            handler.removeMessages(MSG_AUTO_SCROLL);
            handler.sendEmptyMessageDelayed(MSG_AUTO_SCROLL, delayTime);
    }

    /**
     * 停止划
     */
    public void stopAutoScroll() {
        handler.removeMessages(MSG_AUTO_SCROLL);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case MotionEvent.ACTION_UP:
                startAutoScroll();
                break;
            default:
                stopAutoScroll();
                break;
        }
        return super.onTouchEvent(ev);
    }

    private class CustomViewPagerAdapter extends PagerAdapter {

        private List<Uri> list;

        public CustomViewPagerAdapter(List<Uri> list) {
            this.list = list;
        }

        @Override
        public int getCount() {
            return Integer.MAX_VALUE;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            ImageView imageView = imgList.get(position % imgList.size());

            imageView.setImageURI(list.get(position % list.size()));

            container.addView(imageView);
            return imageView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView(imgList.get(position % imgList.size()));
        }
    }

    private class DefaultViewPagerAdapter extends PagerAdapter {

        private List<Integer> list;

        public DefaultViewPagerAdapter(List<Integer> list) {
            this.list = list;
        }

        @Override
        public int getCount() {
            return Integer.MAX_VALUE;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            ImageView imageView = imgList.get(position % imgList.size());
            imageView.setImageResource(list.get(position % list.size()));
            container.addView(imageView);
            return imageView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView(imgList.get(position % imgList.size()));
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        handler.removeCallbacksAndMessages(null);
    }

}
