package com.viomi.fridge.view.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.viomi.fridge.R;
import com.viomi.fridge.api.http.HttpConnect;
import com.viomi.fridge.manager.AccountManager;
import com.viomi.fridge.model.bean.WebBaseData;
import com.viomi.fridge.util.ToastUtil;
import com.viomi.fridge.view.activity.ScanLoginActivity;
import com.viomi.fridge.view.activity.VmallWebActivity;

/**
 * Created by Mocc on 2017/11/1
 */

public class SettingUserFragment extends BaseFragment {


    private static volatile SettingUserFragment fragment;
    private RelativeLayout my_order;
    private RelativeLayout my_address;
    private RelativeLayout coupon;
    private String mVmallUrl = HttpConnect.STOREMAINURL;

    public static SettingUserFragment getInstance() {
        if (fragment == null) {
            synchronized (SettingUserFragment.class) {
                if (fragment == null) {
                    fragment = new SettingUserFragment();
                }
            }
        }
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.setting_user_fragment_layout, null);
        my_order = (RelativeLayout) view.findViewById(R.id.my_order);
        my_address = (RelativeLayout) view.findViewById(R.id.my_address);
        coupon = (RelativeLayout) view.findViewById(R.id.coupon);

        initListener();
        return view;
    }

    private void initListener() {
        my_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                jumpH5("/index.html?page=orderlist");
            }
        });

        my_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                jumpH5("/index.html?page=addr");
            }
        });

        coupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                jumpH5("/index.html?page=coupon");
            }
        });
    }

    private void jumpH5(String url) {
        if ( AccountManager.getViomiUser(getActivity())!=null) {
            Intent intent = new Intent(getActivity(), VmallWebActivity.class);
            WebBaseData data = new WebBaseData();
            data.url = mVmallUrl + url;
            intent.putExtra(WebBaseData.Intent_String, data);
            startActivity(intent);
        } else {
            ToastUtil.show("请先登录！");
            Intent intent = new Intent(getActivity(), ScanLoginActivity.class);
            startActivity(intent);
        }
    }

}
