package com.viomi.fridge.view.widget;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.viomi.fridge.R;
import com.viomi.fridge.api.http.HttpConnect;
import com.viomi.fridge.model.bean.WebBaseData;
import com.viomi.fridge.view.activity.VmallWebActivity;

/**
 * 净水机滤芯状态
 * Created by William on 2017/4/11.
 */

public class WaterFilterDialog extends BaseDialog implements View.OnClickListener {

    private int num;
    private Context mContext;
    private float per;
    private int skuId;
    private CircleProgressView mCircleOne, mCircleTwo, mCircleThree, mCircleFour;

    public WaterFilterDialog(Context context, int num, float per, int skuId) {
        super(context);
        mContext = context;
        this.num = num;
        this.per = per;
        this.skuId = skuId;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (this.getWindow() != null) {
            WindowManager.LayoutParams lp = this.getWindow().getAttributes();
            lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.gravity = Gravity.CENTER;
            this.getWindow().setAttributes(lp);
        }
        setCanceledOnTouchOutside(false);

        initView();
    }

    private void initView() {
        mCircleOne = (CircleProgressView) findViewById(R.id.water_filter_one);
        mCircleTwo = (CircleProgressView) findViewById(R.id.water_filter_two);
        mCircleThree = (CircleProgressView) findViewById(R.id.water_filter_three);
        mCircleFour = (CircleProgressView) findViewById(R.id.water_filter_four);
        TextView tvRemain = (TextView) findViewById(R.id.water_filter_remain);
        TextView tvPer = (TextView) findViewById(R.id.water_filter_per);
        TextView tvTitle = (TextView) findViewById(R.id.water_filter_title);
        TextView tvDesc1 = (TextView) findViewById(R.id.water_filter_desc1);
        TextView tvDesc2 = (TextView) findViewById(R.id.water_filter_desc2);
        findViewById(R.id.water_filter_close).setOnClickListener(this);
        findViewById(R.id.water_filter_buy).setOnClickListener(this);

        mCircleOne.setText("1");
        mCircleTwo.setText("2");
        mCircleThree.setText("3");
        mCircleFour.setText("4");
        tvPer.setText("" + (int) per + "%");

        switch (num) {
            case 1:
                tvRemain.setText(mContext.getResources().getString(R.string.water_filter_one_remain));
                tvTitle.setText(mContext.getResources().getString(R.string.water_filter_one_title));
                tvDesc1.setText(mContext.getResources().getString(R.string.water_filter_one_desc1));
                tvDesc2.setText(mContext.getResources().getString(R.string.water_filter_one_desc2));
                mCircleOne.runAnimate(per);
                break;
            case 2:
                tvRemain.setText(mContext.getResources().getString(R.string.water_filter_two_remain));
                tvTitle.setText(mContext.getResources().getString(R.string.water_filter_two_title));
                tvDesc1.setText(mContext.getResources().getString(R.string.water_filter_two_desc1));
                tvDesc2.setText(mContext.getResources().getString(R.string.water_filter_two_desc2));
                mCircleTwo.runAnimate(per);
                break;
            case 3:
                tvRemain.setText(mContext.getResources().getString(R.string.water_filter_three_remain));
                tvTitle.setText(mContext.getResources().getString(R.string.water_filter_three_title));
                tvDesc1.setText(mContext.getResources().getString(R.string.water_filter_three_desc1));
                tvDesc2.setText(mContext.getResources().getString(R.string.water_filter_three_desc2));
                mCircleThree.runAnimate(per);
                break;
            case 4:
                tvRemain.setText(mContext.getResources().getString(R.string.water_filter_four_remain));
                tvTitle.setText(mContext.getResources().getString(R.string.water_filter_four_title));
                tvDesc1.setText(mContext.getResources().getString(R.string.water_filter_four_desc1));
                tvDesc2.setText(mContext.getResources().getString(R.string.water_filter_four_desc2));
                mCircleFour.runAnimate(per);
                break;
            case 5:
                tvRemain.setText(mContext.getResources().getString(R.string.water_filter_x3_one_remain));
                tvTitle.setText(mContext.getResources().getString(R.string.water_filter_x3_one_title));
                tvDesc1.setText(mContext.getResources().getString(R.string.water_filter_x3_one_desc1));
                tvDesc2.setText("");
                mCircleOne.setVisibility(View.GONE);
                mCircleTwo.setText("1");
                mCircleThree.setText("2");
                mCircleFour.setText("3");
                mCircleTwo.runAnimate(per);
                break;
            case 6:
                tvRemain.setText(mContext.getResources().getString(R.string.water_filter_three_remain));
                tvTitle.setText(mContext.getResources().getString(R.string.water_filter_three_title));
                tvDesc1.setText(mContext.getResources().getString(R.string.water_filter_three_desc1));
                tvDesc2.setText(mContext.getResources().getString(R.string.water_filter_three_desc2));
                mCircleOne.setVisibility(View.GONE);
                mCircleTwo.setText("1");
                mCircleThree.setText("2");
                mCircleFour.setText("3");
                mCircleThree.runAnimate(per);
                break;
            case 7:
                tvRemain.setText(mContext.getResources().getString(R.string.water_filter_x3_three_remain));
                tvTitle.setText(mContext.getResources().getString(R.string.water_filter_x3_three_title));
                tvDesc1.setText(mContext.getResources().getString(R.string.water_filter_x3_three_desc1));
                tvDesc2.setText(mContext.getResources().getString(R.string.water_filter_x3_three_desc2));
                mCircleOne.setVisibility(View.GONE);
                mCircleTwo.setText("1");
                mCircleThree.setText("2");
                mCircleFour.setText("3");
                mCircleFour.runAnimate(per);
                break;
            default:
                break;
        }
    }

    @Override
    public void setView() {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_water_filter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.water_filter_close:   // 关闭
                dismiss();
                break;
            case R.id.water_filter_buy:     // 购买滤芯
                Intent intent = new Intent(mContext, VmallWebActivity.class);
                WebBaseData webBaseData = new WebBaseData();
                webBaseData.url = HttpConnect.STOREMAINURL + "/index.html?page=detail&backToNative=1&skuId=" + skuId;
                intent.putExtra(WebBaseData.Intent_String, webBaseData);
                mContext.startActivity(intent);
                break;
        }
    }

    @Override
    public void setOnDismissListener(@Nullable OnDismissListener listener) {
        super.setOnDismissListener(listener);
        if (mCircleOne != null) mCircleOne.cancelAnimate();
        if (mCircleTwo != null) mCircleTwo.cancelAnimate();
        if (mCircleThree != null) mCircleThree.cancelAnimate();
        if (mCircleFour != null) mCircleFour.cancelAnimate();
    }

}
