package com.viomi.fridge.view.activity;

public class JsonString {

    public static final String Opp = "{\n" +
            "  \"requestId\": \"string\",\n" +
            "  \"appVersion\": \"string\",\n" +
            "  \"systemVersion\": \"string\",\n" +
            "  \"time\": \"long\",\n" +
            "  \"name\": \"string\",\n" +
            "  \"did\": \"string\",\n" +
            "  \"model\": \"string\",\n" +
            "  \"ip\": \"string\",\n" +
            "  \"mac\": \"string\",\n" +
            "  \"data\": {\n" +
            "    \"key\": \"string\",\n" +
            "    \"screen\": \"string\",\n" +
            "    \"ssid\": \"string\",\n" +
            "    \"value\": \"string\"\n" +
            "  }\n" +
            "}";

    public static final String recipe1 = "{\n" +
            "        \"categoryInfo\": {\n" +
            "          \"ctgId\": \"0010001002\",\n" +
            "          \"name\": \"按菜品选择菜谱\",\n" +
            "          \"parentId\": \"0010001001\"\n" +
            "        },\n" +
            "        \"childs\": [\n" +
            "          {\n" +
            "            \"categoryInfo\": {\n" +
            "              \"ctgId\": \"0010001007\",\n" +
            "              \"name\": \"荤菜\",\n" +
            "              \"parentId\": \"0010001002\"\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            \"categoryInfo\": {\n" +
            "              \"ctgId\": \"0010001008\",\n" +
            "              \"name\": \"素菜\",\n" +
            "              \"parentId\": \"0010001002\"\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            \"categoryInfo\": {\n" +
            "              \"ctgId\": \"0010001009\",\n" +
            "              \"name\": \"汤粥\",\n" +
            "              \"parentId\": \"0010001002\"\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            \"categoryInfo\": {\n" +
            "              \"ctgId\": \"0010001010\",\n" +
            "              \"name\": \"西点\",\n" +
            "              \"parentId\": \"0010001002\"\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            \"categoryInfo\": {\n" +
            "              \"ctgId\": \"0010001011\",\n" +
            "              \"name\": \"主食\",\n" +
            "              \"parentId\": \"0010001002\"\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            \"categoryInfo\": {\n" +
            "              \"ctgId\": \"0010001012\",\n" +
            "              \"name\": \"饮品\",\n" +
            "              \"parentId\": \"0010001002\"\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            \"categoryInfo\": {\n" +
            "              \"ctgId\": \"0010001013\",\n" +
            "              \"name\": \"便当\",\n" +
            "              \"parentId\": \"0010001002\"\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            \"categoryInfo\": {\n" +
            "              \"ctgId\": \"0010001014\",\n" +
            "              \"name\": \"小吃\",\n" +
            "              \"parentId\": \"0010001002\"\n" +
            "            }\n" +
            "          }\n" +
            "        ]\n" +
            "      }";
    public static final String recipe2 = "{\n" +
            "        \"categoryInfo\": {\n" +
            "          \"ctgId\": \"0010001003\",\n" +
            "          \"name\": \"按工艺选择菜谱\",\n" +
            "          \"parentId\": \"0010001001\"\n" +
            "        },\n" +
            "        \"childs\": [\n" +
            "          {\n" +
            "            \"categoryInfo\": {\n" +
            "              \"ctgId\": \"0010001015\",\n" +
            "              \"name\": \"红烧\",\n" +
            "              \"parentId\": \"0010001003\"\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            \"categoryInfo\": {\n" +
            "              \"ctgId\": \"0010001016\",\n" +
            "              \"name\": \"炒\",\n" +
            "              \"parentId\": \"0010001003\"\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            \"categoryInfo\": {\n" +
            "              \"ctgId\": \"0010001017\",\n" +
            "              \"name\": \"煎\",\n" +
            "              \"parentId\": \"0010001003\"\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            \"categoryInfo\": {\n" +
            "              \"ctgId\": \"0010001018\",\n" +
            "              \"name\": \"炸\",\n" +
            "              \"parentId\": \"0010001003\"\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            \"categoryInfo\": {\n" +
            "              \"ctgId\": \"0010001019\",\n" +
            "              \"name\": \"焖\",\n" +
            "              \"parentId\": \"0010001003\"\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            \"categoryInfo\": {\n" +
            "              \"ctgId\": \"0010001020\",\n" +
            "              \"name\": \"炖\",\n" +
            "              \"parentId\": \"0010001003\"\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            \"categoryInfo\": {\n" +
            "              \"ctgId\": \"0010001021\",\n" +
            "              \"name\": \"蒸\",\n" +
            "              \"parentId\": \"0010001003\"\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            \"categoryInfo\": {\n" +
            "              \"ctgId\": \"0010001022\",\n" +
            "              \"name\": \"烩\",\n" +
            "              \"parentId\": \"0010001003\"\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            \"categoryInfo\": {\n" +
            "              \"ctgId\": \"0010001023\",\n" +
            "              \"name\": \"熏\",\n" +
            "              \"parentId\": \"0010001003\"\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            \"categoryInfo\": {\n" +
            "              \"ctgId\": \"0010001024\",\n" +
            "              \"name\": \"腌\",\n" +
            "              \"parentId\": \"0010001003\"\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            \"categoryInfo\": {\n" +
            "              \"ctgId\": \"0010001025\",\n" +
            "              \"name\": \"煮\",\n" +
            "              \"parentId\": \"0010001003\"\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            \"categoryInfo\": {\n" +
            "              \"ctgId\": \"0010001026\",\n" +
            "              \"name\": \"炝\",\n" +
            "              \"parentId\": \"0010001003\"\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            \"categoryInfo\": {\n" +
            "              \"ctgId\": \"0010001027\",\n" +
            "              \"name\": \"卤\",\n" +
            "              \"parentId\": \"0010001003\"\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            \"categoryInfo\": {\n" +
            "              \"ctgId\": \"0010001028\",\n" +
            "              \"name\": \"拌\",\n" +
            "              \"parentId\": \"0010001003\"\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            \"categoryInfo\": {\n" +
            "              \"ctgId\": \"0010001029\",\n" +
            "              \"name\": \"烤\",\n" +
            "              \"parentId\": \"0010001003\"\n" +
            "            }\n" +
            "          }\n" +
            "        ]\n" +
            "      }";
    public static final String recipe3 = "{\n" +
            "        \"categoryInfo\": {\n" +
            "          \"ctgId\": \"0010001004\",\n" +
            "          \"name\": \"按菜系选择菜谱\",\n" +
            "          \"parentId\": \"0010001001\"\n" +
            "        },\n" +
            "        \"childs\": [\n" +
            "          {\n" +
            "            \"categoryInfo\": {\n" +
            "              \"ctgId\": \"0010001030\",\n" +
            "              \"name\": \"鲁菜\",\n" +
            "              \"parentId\": \"0010001004\"\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            \"categoryInfo\": {\n" +
            "              \"ctgId\": \"0010001031\",\n" +
            "              \"name\": \"川菜\",\n" +
            "              \"parentId\": \"0010001004\"\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            \"categoryInfo\": {\n" +
            "              \"ctgId\": \"0010001032\",\n" +
            "              \"name\": \"粤菜\",\n" +
            "              \"parentId\": \"0010001004\"\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            \"categoryInfo\": {\n" +
            "              \"ctgId\": \"0010001033\",\n" +
            "              \"name\": \"闽菜\",\n" +
            "              \"parentId\": \"0010001004\"\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            \"categoryInfo\": {\n" +
            "              \"ctgId\": \"0010001034\",\n" +
            "              \"name\": \"浙菜\",\n" +
            "              \"parentId\": \"0010001004\"\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            \"categoryInfo\": {\n" +
            "              \"ctgId\": \"0010001035\",\n" +
            "              \"name\": \"湘菜\",\n" +
            "              \"parentId\": \"0010001004\"\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            \"categoryInfo\": {\n" +
            "              \"ctgId\": \"0010001036\",\n" +
            "              \"name\": \"上海菜\",\n" +
            "              \"parentId\": \"0010001004\"\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            \"categoryInfo\": {\n" +
            "              \"ctgId\": \"0010001037\",\n" +
            "              \"name\": \"徽菜\",\n" +
            "              \"parentId\": \"0010001004\"\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            \"categoryInfo\": {\n" +
            "              \"ctgId\": \"0010001038\",\n" +
            "              \"name\": \"京菜\",\n" +
            "              \"parentId\": \"0010001004\"\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            \"categoryInfo\": {\n" +
            "              \"ctgId\": \"0010001039\",\n" +
            "              \"name\": \"东北菜\",\n" +
            "              \"parentId\": \"0010001004\"\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            \"categoryInfo\": {\n" +
            "              \"ctgId\": \"0010001040\",\n" +
            "              \"name\": \"西北菜\",\n" +
            "              \"parentId\": \"0010001004\"\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            \"categoryInfo\": {\n" +
            "              \"ctgId\": \"0010001041\",\n" +
            "              \"name\": \"客家菜\",\n" +
            "              \"parentId\": \"0010001004\"\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            \"categoryInfo\": {\n" +
            "              \"ctgId\": \"0010001042\",\n" +
            "              \"name\": \"台湾美食\",\n" +
            "              \"parentId\": \"0010001004\"\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            \"categoryInfo\": {\n" +
            "              \"ctgId\": \"0010001043\",\n" +
            "              \"name\": \"泰国菜\",\n" +
            "              \"parentId\": \"0010001004\"\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            \"categoryInfo\": {\n" +
            "              \"ctgId\": \"0010001044\",\n" +
            "              \"name\": \"日本料理\",\n" +
            "              \"parentId\": \"0010001004\"\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            \"categoryInfo\": {\n" +
            "              \"ctgId\": \"0010001045\",\n" +
            "              \"name\": \"韩国料理\",\n" +
            "              \"parentId\": \"0010001004\"\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            \"categoryInfo\": {\n" +
            "              \"ctgId\": \"0010001046\",\n" +
            "              \"name\": \"西餐\",\n" +
            "              \"parentId\": \"0010001004\"\n" +
            "            }\n" +
            "          }\n" +
            "        ]\n" +
            "      }";
    public static final String recipe4 = "{\n" +
            "        \"categoryInfo\": {\n" +
            "          \"ctgId\": \"0010001005\",\n" +
            "          \"name\": \"按人群选择菜谱\",\n" +
            "          \"parentId\": \"0010001001\"\n" +
            "        },\n" +
            "        \"childs\": [\n" +
            "          {\n" +
            "            \"categoryInfo\": {\n" +
            "              \"ctgId\": \"0010001047\",\n" +
            "              \"name\": \"孕妇食谱\",\n" +
            "              \"parentId\": \"0010001005\"\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            \"categoryInfo\": {\n" +
            "              \"ctgId\": \"0010001048\",\n" +
            "              \"name\": \"婴幼食谱\",\n" +
            "              \"parentId\": \"0010001005\"\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            \"categoryInfo\": {\n" +
            "              \"ctgId\": \"0010001049\",\n" +
            "              \"name\": \"儿童食谱\",\n" +
            "              \"parentId\": \"0010001005\"\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            \"categoryInfo\": {\n" +
            "              \"ctgId\": \"0010001050\",\n" +
            "              \"name\": \"懒人食谱\",\n" +
            "              \"parentId\": \"0010001005\"\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            \"categoryInfo\": {\n" +
            "              \"ctgId\": \"0010001051\",\n" +
            "              \"name\": \"宵夜\",\n" +
            "              \"parentId\": \"0010001005\"\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            \"categoryInfo\": {\n" +
            "              \"ctgId\": \"0010001052\",\n" +
            "              \"name\": \"素食\",\n" +
            "              \"parentId\": \"0010001005\"\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            \"categoryInfo\": {\n" +
            "              \"ctgId\": \"0010001053\",\n" +
            "              \"name\": \"产妇食谱\",\n" +
            "              \"parentId\": \"0010001005\"\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            \"categoryInfo\": {\n" +
            "              \"ctgId\": \"0010001054\",\n" +
            "              \"name\": \"二人世界\",\n" +
            "              \"parentId\": \"0010001005\"\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            \"categoryInfo\": {\n" +
            "              \"ctgId\": \"0010001055\",\n" +
            "              \"name\": \"下午茶\",\n" +
            "              \"parentId\": \"0010001005\"\n" +
            "            }\n" +
            "          }\n" +
            "        ]\n" +
            "      }";
    public static final String recipe5 = "{\n" +
            "        \"categoryInfo\": {\n" +
            "          \"ctgId\": \"0010001006\",\n" +
            "          \"name\": \"按功能选择菜谱\",\n" +
            "          \"parentId\": \"0010001001\"\n" +
            "        },\n" +
            "        \"childs\": [\n" +
            "          {\n" +
            "            \"categoryInfo\": {\n" +
            "              \"ctgId\": \"0010001056\",\n" +
            "              \"name\": \"减肥\",\n" +
            "              \"parentId\": \"0010001006\"\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            \"categoryInfo\": {\n" +
            "              \"ctgId\": \"0010001057\",\n" +
            "              \"name\": \"便秘\",\n" +
            "              \"parentId\": \"0010001006\"\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            \"categoryInfo\": {\n" +
            "              \"ctgId\": \"0010001058\",\n" +
            "              \"name\": \"养胃\",\n" +
            "              \"parentId\": \"0010001006\"\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            \"categoryInfo\": {\n" +
            "              \"ctgId\": \"0010001059\",\n" +
            "              \"name\": \"滋阴\",\n" +
            "              \"parentId\": \"0010001006\"\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            \"categoryInfo\": {\n" +
            "              \"ctgId\": \"0010001060\",\n" +
            "              \"name\": \"补阳\",\n" +
            "              \"parentId\": \"0010001006\"\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            \"categoryInfo\": {\n" +
            "              \"ctgId\": \"0010001061\",\n" +
            "              \"name\": \"月经不调\",\n" +
            "              \"parentId\": \"0010001006\"\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            \"categoryInfo\": {\n" +
            "              \"ctgId\": \"0010001062\",\n" +
            "              \"name\": \"美容\",\n" +
            "              \"parentId\": \"0010001006\"\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            \"categoryInfo\": {\n" +
            "              \"ctgId\": \"0010001063\",\n" +
            "              \"name\": \"养生\",\n" +
            "              \"parentId\": \"0010001006\"\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            \"categoryInfo\": {\n" +
            "              \"ctgId\": \"0010001064\",\n" +
            "              \"name\": \"贫血\",\n" +
            "              \"parentId\": \"0010001006\"\n" +
            "            }\n" +
            "          }\n" +
            "        ]\n" +
            "      }";
}
