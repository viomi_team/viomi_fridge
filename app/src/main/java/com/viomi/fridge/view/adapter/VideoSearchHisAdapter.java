package com.viomi.fridge.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.viomi.fridge.R;

/**
 * Created by Mocc on 2017/8/7
 */

public class VideoSearchHisAdapter extends BaseAdapter {

    private Context context;
    private String[] array;
    private LayoutInflater inflater;

    public VideoSearchHisAdapter(Context context, String[] array) {
        this.context = context;
        this.array = array;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return array.length;
    }

    @Override
    public Object getItem(int position) {
        return array[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.video_search_history_item, null);
        TextView item_tv = (TextView) convertView.findViewById(R.id.item_tv);
        item_tv.setText((position + 1) + "." + array[position]);
        return convertView;
    }
}
