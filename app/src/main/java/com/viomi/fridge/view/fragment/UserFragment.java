package com.viomi.fridge.view.fragment;

import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.viomi.fridge.R;
import com.viomi.fridge.api.http.HttpConnect;
import com.viomi.fridge.manager.AccountManager;
import com.viomi.fridge.model.bean.ViomiUser;
import com.viomi.fridge.util.JsonUitls;
import com.viomi.fridge.util.PhoneUtil;
import com.viomi.fridge.util.ResponseCode;
import com.viomi.fridge.util.ToolUtil;
import com.viomi.fridge.util.log;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;


public class UserFragment extends BaseFragment {

    private MyHandler mHandler;
    private SimpleDraweeView user_logo;
    private TextView user_name;
    private TextView login_out;
    private RelativeLayout loading_layout;
    private RelativeLayout dialog_view;
    private RelativeLayout dialog_bg;
    private SimpleDraweeView qrcode_img;
    private CheckThread checkThread;
    private boolean isLogined;


    private static class MyHandler extends Handler {
        WeakReference<UserFragment> weakReference;

        public MyHandler(UserFragment fragment) {
            this.weakReference = new WeakReference<UserFragment>(fragment);
        }

        @Override
        public void handleMessage(Message msg) {
            UserFragment userFragment = weakReference.get();
            switch (msg.what) {

                case 0: {
                    userFragment.loading_layout.setVisibility(View.GONE);
                    log.myE("viomi0", msg.obj.toString());
                    userFragment.waitForScan((String) msg.obj);
                }
                break;

                case 1: {
                    userFragment.loading_layout.setVisibility(View.GONE);
                    Toast.makeText(userFragment.getActivity(), HttpConnect.ERROR_MESSAGE_HTTP_RESULT, Toast.LENGTH_SHORT).show();
                }
                break;

                case 2: {
                    userFragment.checkLogin();
                }
                break;
                case 3: {
                    log.myE("viomi3", msg.obj.toString());
                    userFragment.parseJSON((String) msg.obj);
                }
                break;
                case 4: {
                    log.myE("viomi4", msg.obj.toString());
                }

                break;
            }
        }
    }


    private static class CheckThread extends Thread {
        private WeakReference<UserFragment> weakReference;
        private UserFragment userFragment;

        public CheckThread(UserFragment fragment) {
            this.weakReference = new WeakReference<UserFragment>(fragment);
            userFragment = weakReference.get();
        }

        @Override
        public void run() {
            while (!isInterrupted()) {
                userFragment.mHandler.sendEmptyMessage(2);
                SystemClock.sleep(3000);
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mainView = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_user, null, false);

        user_logo = (SimpleDraweeView) mainView.findViewById(R.id.user_logo);
        user_name = (TextView) mainView.findViewById(R.id.user_name);
        login_out = (TextView) mainView.findViewById(R.id.login_btn);
        loading_layout = (RelativeLayout) mainView.findViewById(R.id.loading_layout);
        dialog_view = (RelativeLayout) mainView.findViewById(R.id.dialog_view);
        dialog_bg = (RelativeLayout) mainView.findViewById(R.id.dialog_bg);
        qrcode_img = (SimpleDraweeView) mainView.findViewById(R.id.qrcode_img);

        initListener();
        mHandler = new MyHandler(this);
        ViomiUser user = AccountManager.getViomiUser(getActivity());
        if (user == null) {
            login_out.setText("登录");
            isLogined = false;
        } else {
            login_out.setText("退出登录");
            isLogined = true;
            showUserInfo();
        }
        return mainView;
    }


    private void initListener() {
        login_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isLogined) {
                    login_out.setText("登录");
                    user_name.setText("请登录！");
                    Uri uri1 = Uri.parse("res:///" + R.drawable.dialog_write_bg);
                    user_logo.setImageURI(uri1);
                    isLogined = false;
                    AccountManager.deleteViomiUser(getActivity());
                } else {
                    requestQRImg();
                }

            }
        });

        dialog_bg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_view.setVisibility(View.GONE);
            }
        });
    }

    //请求二维码
    private void requestQRImg() {
        loading_layout.setVisibility(View.VISIBLE);

        Map<String, String> map = new HashMap<>();
        map.put("clientID", ToolUtil.formatMac(PhoneUtil.getPhoneMac()));
        map.put("type", "1");
        HttpConnect.getRequestHandler(HttpConnect.qrcode_create_url, map, mHandler, 0, 1);
    }


    //显示二维码，等待登录
    private void waitForScan(String jsonStr) {
        dialog_view.setVisibility(View.VISIBLE);
        try {
            JSONObject json = new JSONObject(jsonStr);
            JSONObject mobBaseRes = JsonUitls.getJSONObject(json, "mobBaseRes");
            String code = JsonUitls.getString(mobBaseRes, "code");
            String desc = JsonUitls.getString(mobBaseRes, "desc");
            if (ResponseCode.isSuccess(code, desc)) {
                String result = JsonUitls.getString(mobBaseRes, "result");
                Uri uri = Uri.parse(result);
                qrcode_img.setImageURI(uri);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //启动线程查询是否扫描成功
        if (checkThread != null) {
            checkThread.interrupt();
        }
        checkThread = new CheckThread(this);
        checkThread.start();
    }

    //查询是否登录成功
    private void checkLogin() {
        Map<String, String> paramsMap = new HashMap<>();
        paramsMap.put("clientID", ToolUtil.formatMac(PhoneUtil.getPhoneMac()));
        HttpConnect.postRequestHandler(HttpConnect.check_login_status_url, paramsMap, mHandler, 3, 4);
    }

    private void parseJSON(String jsonStr) {
        try {
            JSONObject json = new JSONObject(jsonStr);
            JSONObject mobBaseRes = JsonUitls.getJSONObject(json, "mobBaseRes");
            String code = JsonUitls.getString(mobBaseRes, "code");
            if ("100".equals(code)) {
                //终止轮询
                dialog_view.setVisibility(View.GONE);
                isLogined = true;
                login_out.setText("退出登录");
                if (checkThread != null) {
                    checkThread.interrupt();
                }

                String token = JsonUitls.getString(mobBaseRes, "token");
                JSONObject loginData = JsonUitls.getJSONObject(mobBaseRes, "loginData");
                String userCode = JsonUitls.getString(loginData, "userCode");
                String appendAttr = JsonUitls.getString(mobBaseRes, "appendAttr");
                JSONObject appendAttrJson = new JSONObject(appendAttr);
                String miId = JsonUitls.getString(appendAttrJson, "miId");
                String nikeName = JsonUitls.getString(appendAttrJson, "nikeName");
                String headImg = JsonUitls.getString(appendAttrJson, "headImg");
                String account = JsonUitls.getString(appendAttrJson, "account");
                int cid = JsonUitls.getInt(appendAttrJson, "cid");
                int gender = JsonUitls.getInt(appendAttrJson, "gender");
                String mobile = JsonUitls.getString(appendAttrJson, "mobile");

                ViomiUser user = new ViomiUser(miId, token, nikeName, account, headImg, userCode, mobile, cid, gender);
                AccountManager.saveViomiUser(getActivity(), user);
                showUserInfo();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void showUserInfo() {
        ViomiUser user = AccountManager.getViomiUser(getActivity());
        if (user == null) {
            return;
        }
        user_name.setText(user.getNickname());
        Uri uri = Uri.parse(user.getHeadImg());
        user_logo.setImageURI(uri);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (checkThread != null) {
            checkThread.interrupt();
        }
    }
}
