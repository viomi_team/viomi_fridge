package com.viomi.fridge.view.widget;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.viomi.fridge.R;
import com.viomi.fridge.device.AppConfig;
import com.viomi.fridge.manager.ControlManager;
import com.viomi.fridge.model.bean.FoodDetailData;
import com.viomi.fridge.util.log;
import com.viomi.fridge.view.activity.FoodManageActivity;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

/**
 * 食材编辑 Dialog
 * Created by William on 2017/6/23.
 */

public class FoodEditDialog extends BaseDialog implements View.OnClickListener, ViewTreeObserver.OnGlobalLayoutListener, DateChoseDialog.OnDateSelectedListener,
        TextWatcher {

    private static final String TAG = FoodEditDialog.class.getSimpleName();
    private static final String Path = Environment.getExternalStorageDirectory().toString() + "/viomi/" + "food_manage.xml";
    private Context context;
    private String type;   // 0: 冷藏室；1: 变温室；2: 冷冻室；
    private String name, time, path, id;  // 产品名称，保质期，图片路径
    private int position;
    private LinearLayout mLinearLayout;
    private ImageView mImageView;
    private TextView mTextView;
    private EditText mEditText;
    private MyHandler mHandler;
    private MyRunnable mRunnable;
    private LoadRunnable mLoadRunnable;
    private List<FoodDetailData> list = new ArrayList<>();
    private OnDeleteListener onDeleteListener;
    private OnModifyListener onModifyListener;
    private static final int MSG_UPDATE_IMG = 1;
    private LocalBroadcastManager mLocalBroadcastManager;

    public FoodEditDialog(Context context, String type, String name, String time, String path, String id, int position) {
        super(context);
        this.context = context;
        this.type = type;
        this.name = name;
        this.time = time;
        this.path = path;
        this.id = id;
        this.position = position;
        mHandler = new MyHandler(this);
        mRunnable = new MyRunnable(this);
        mLoadRunnable = new LoadRunnable(this);
        mLocalBroadcastManager = LocalBroadcastManager.getInstance(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = this.getWindow();
        if (window != null) {
            WindowManager.LayoutParams lp = window.getAttributes();
            lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE | WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
            window.setAttributes(lp);
            window.setBackgroundDrawable(null);
            window.setDimAmount(0f);    // 背景无模糊
            window.setGravity(Gravity.CENTER);
            window.setWindowAnimations(R.style.popupWindow_anim_style);  // Dialog 动画
        }
        setCanceledOnTouchOutside(false);

        initView();

        registerReceiver();
    }

    @Override
    public void dismiss() {
        super.dismiss();
        onDeleteListener = null;
        onModifyListener = null;
        list = null;
        mLinearLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
        mHandler.removeCallbacksAndMessages(null);
        if (mRunnable != null) mRunnable = null;
        mLocalBroadcastManager.unregisterReceiver(mReceiver);
        log.d(TAG, "dismiss success");
    }

    private void initView() {
        mHandler.post(mLoadRunnable);
        mLinearLayout = (LinearLayout) findViewById(R.id.food_edit_root);
        mImageView = (ImageView) findViewById(R.id.food_edit_img);
        mEditText = (EditText) findViewById(R.id.food_edit_name);
        mTextView = (TextView) findViewById(R.id.food_edit_guarantee);
        TextView tvType = (TextView) findViewById(R.id.food_edit_type);

        findViewById(R.id.food_edit_close).setOnClickListener(this);
        findViewById(R.id.food_edit_update).setOnClickListener(this);
        findViewById(R.id.food_edit_delete).setOnClickListener(this);
        mTextView.setOnClickListener(this);
        mEditText.addTextChangedListener(this);
        mLinearLayout.getViewTreeObserver().addOnGlobalLayoutListener(this);

        // 分类
        switch (type) {
            case "0":
                tvType.setText("冷藏室");
                break;
            case "1":
                tvType.setText("变温室");
                break;
            case "2":
                tvType.setText("冷冻室");
                break;
        }
        mEditText.setText(name);
        if (time.equals("可选填")) {
            mTextView.setTextColor(0xFFCCCCCC);
            mTextView.setText(time);
        } else {
            mTextView.setTextColor(0xFF585858);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            long stamp = Long.valueOf(time);
            String date = sdf.format(new Date(stamp));
            mTextView.setText(date);
        }
        mHandler.post(mRunnable);
    }

    private void registerReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(AppConfig.ACTION_KEY_BOARD_STATUS_CHANGE);
        mLocalBroadcastManager.registerReceiver(mReceiver, intentFilter);
    }

    @Override
    public void setView() {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_food_edit);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.food_edit_close:  // 关闭
                dismiss();
                break;
            case R.id.food_edit_guarantee:  // 选择日期
                ((FoodManageActivity) context).isFinish = false;
                setGuaranteePeriod();
                break;
            case R.id.food_edit_update: // 更新
                update();
                break;
            case R.id.food_edit_delete: // 删除
                new Thread(this::delete).start();
                break;
        }
    }

    private void setGuaranteePeriod() {
        DateChoseDialog dateChoseDialog = new DateChoseDialog(context, time.equals("可选填") ? null : time);
        dateChoseDialog.setOnDateSelectedListener(this);
        dateChoseDialog.setOnDismissListener(dialog -> ((FoodManageActivity) context).isFinish = true);
        dateChoseDialog.show();
    }

    /**
     * 更新数据
     */
    private void update() {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder db = dbFactory.newDocumentBuilder();
            Document dom = db.parse(new File(Path));
            Element root = (Element) dom.getFirstChild();
            NodeList foodList = root.getElementsByTagName("food");
            String str = "可选填", deadline = "可选填", addTime = "";
            for (int i = 0; i < foodList.getLength(); i++) {
                Element userUpdate = (Element) foodList.item(i);

                // 获取 food 节点下的所有子节点
                NodeList childNodes = userUpdate.getChildNodes();
                Node node = childNodes.item(5);
                Element childNode = (Element) node;

                if (childNode.getFirstChild().getTextContent().equals(id)) {
                    Node node1 = childNodes.item(3);
                    Element childNode1 = (Element) node1;
                    str = mEditText.getText().toString().trim();
                    str = str.equals("") ? "未命名" : str;
                    childNode1.setTextContent(str);

                    Node node2 = childNodes.item(9);
                    Element childNode2 = (Element) node2;
                    deadline = mTextView.getText().toString();
                    if (deadline.equals("可选填")) childNode2.setTextContent("可选填");
                    else {
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                        Date date = new Date(System.currentTimeMillis());
                        long start = (simpleDateFormat.parse(simpleDateFormat.format(date))).getTime();
                        long period = System.currentTimeMillis() - start;

                        DateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                        Date date1 = df.parse(deadline);
                        deadline = String.valueOf((date1.getTime() + period));
                        childNode2.setTextContent(deadline);
                    }

                    Node node3 = childNodes.item(5);
                    Element childNode3 = (Element) node3;
                    addTime = String.valueOf(System.currentTimeMillis());
                    childNode3.setTextContent(addTime);

                    Node node4 = childNodes.item(11);
                    Element childNode4 = (Element) node4;
                    childNode4.setTextContent("false");
                    break;
                }
            }
            String finalDeadline = deadline;
            String finalStr = str;
            String finalAddTime = addTime;
            // 从内存写入硬盘
            TransformerFactory tfFactory = TransformerFactory.newInstance();
            Transformer tf = tfFactory.newTransformer();
            tf.setOutputProperty(OutputKeys.ENCODING, "UTF-8");    // 编码方式
            tf.setOutputProperty(OutputKeys.INDENT, "yes");    // 是否缩进
            tf.transform(new DOMSource(dom), new StreamResult(new File(Path)));

            ((FoodManageActivity) context).runOnUiThread(() -> {
                if (onModifyListener != null)
                    onModifyListener.onModify(type, position, finalStr, finalDeadline, finalAddTime);
                Toast.makeText(context, "修改成功", Toast.LENGTH_SHORT).show();
                dismiss();
            });
            log.d(TAG, "modify success");
        } catch (ParserConfigurationException | IOException | SAXException | TransformerException | ParseException e) {
            e.printStackTrace();
            log.d(TAG, e.toString());
        }
    }

    /**
     * 删除数据
     */
    private void delete() {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder db = dbFactory.newDocumentBuilder();
            Document dom = db.parse(new File(Path));
            Element root = (Element) dom.getFirstChild();
            NodeList foodList = root.getElementsByTagName("food");

            for (int i = 0; i < foodList.getLength(); i++) {
                Element userUpdate = (Element) foodList.item(i);

                // 获取 food 节点下的所有子节点
                NodeList childNodes = userUpdate.getChildNodes();
                Node node = childNodes.item(5); // 获取唯一标识
                Element childNode = (Element) node;
                if (childNode.getFirstChild().getTextContent().equals(id)) {
                    Node pathNode = childNodes.item(7); // 获取图片路径
                    Element pathChild = (Element) pathNode;
                    File file = new File(pathChild.getFirstChild().getTextContent());
                    if (file.exists()) {
                        boolean isSuccess = file.delete();
                        log.d(TAG, "Delete food image " + isSuccess);
                    }
                    root.removeChild(foodList.item(i));
                    break;
                }
            }
            // 保存
            TransformerFactory tfFactory = TransformerFactory.newInstance();
            Transformer tf = tfFactory.newTransformer();
            tf.setOutputProperty(OutputKeys.ENCODING, "UTF-8");    // 编码方式
            tf.setOutputProperty(OutputKeys.INDENT, "yes");    // 是否缩进
            tf.transform(new DOMSource(dom), new StreamResult(new File(Path)));
            ((FoodManageActivity) context).runOnUiThread(() -> {
                if (onDeleteListener != null)
                    onDeleteListener.onDelete(this.type, position);
                Toast.makeText(context, "删除成功", Toast.LENGTH_SHORT).show();
                dismiss();
            });
            log.d(TAG, "remove success");
        } catch (ParserConfigurationException | TransformerException | IOException | SAXException e) {
            e.printStackTrace();
            log.d(TAG, "remove fail:" + e.toString());
        }
    }

    // 读取食材默认保质期
    private void loadFoodData() {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();

            // 读取文件流
            InputStream stream = context.getResources().getAssets().open("food_manage_data.xml");
            Document dom = builder.parse(stream);
            dom.normalize();

            Element root = dom.getDocumentElement();
            NodeList items = root.getElementsByTagName("food");   // 查找所有 food 节点

            for (int i = 0; i < items.getLength(); i++) {
                Element item = (Element) items.item(i);
                FoodDetailData data = new FoodDetailData();
                data.no_filter = item.getAttribute("no_filter");
                data.filter = item.getAttribute("filter");
                data.name = item.getFirstChild().getNodeValue();
                list.add(data);
            }
//            for (int i = 0; i < foodMsgList.size(); i++) {
//                log.d(TAG, "name:" + foodMsgList.get(i).name + "，filter:" + foodMsgList.get(i).filter + "，no_filter:" + foodMsgList.get(i).no_filter);
//            }
        } catch (ParserConfigurationException | IOException | SAXException e) {
            e.printStackTrace();
        }
    }

    // 根据输入名称匹配保质期
    private String matchPeriod(String name) {
        log.d(TAG, "Filter least：" + ControlManager.getInstance().getFilterLifeUsePercent());
        long current = System.currentTimeMillis();
        long deadline = 0;
        if (name.contains("鱼") || name.contains("虾") || name.contains("蟹") || name.contains("蚌")) {
            if (ControlManager.getInstance().getFilterLifeUsePercent() >= 100) {     // 滤芯用完
                if (type.equals("0")) {     // 冷藏室
                    deadline = current + 24L * 60L * 60L * 1000L;
                } else if (type.equals("2")) {      // 冷冻室
                    deadline = current + 24L * 60L * 60L * 1000L * 89L;
                }
            } else {
                if (type.equals("0")) {     // 冷藏室
                    deadline = current + 24L * 60L * 60L * 1000L * 4L;
                } else if (type.equals("2")) {      // 冷冻室
                    deadline = current + 24L * 60L * 60L * 1000L * 179L;
                }
            }
        } else if (name.contains("蛋")) {
            if (type.equals("0")) {     // 冷藏室
                deadline = current + 24L * 60L * 60L * 1000L * 20L;
            }
        } else if (name.contains("肉")) {
            if (ControlManager.getInstance().getFilterLifeUsePercent() >= 100) {     // 滤芯用完
                if (type.equals("0")) {     // 冷藏室
                    deadline = current + 24L * 60L * 60L * 1000L;
                } else if (type.equals("2")) {      // 冷冻室
                    deadline = current + 24L * 60L * 60L * 1000L * 89L;
                }
            } else {
                if (type.equals("0")) {     // 冷藏室
                    deadline = current + 24L * 60L * 60L * 1000L * 4L;
                } else if (type.equals("2")) {      // 冷冻室
                    deadline = current + 24L * 60L * 60L * 1000L * 179L;
                }
            }
        } else {
            if (type.equals("0")) {      // 冷藏室
                for (int i = 0; i < list.size(); i++) {
                    if (list.get(i).name.equals(name)) {
                        long day;
                        if (ControlManager.getInstance().getFilterLifeUsePercent() >= 100) {    // 滤芯用完
                            day = Long.valueOf(list.get(i).no_filter);
                        } else {    // 滤芯未用完
                            day = Long.valueOf(list.get(i).filter);
                        }
                        deadline = current + 24L * 60L * 60L * 1000L * (day - 1L);
                        break;
                    }
                }
            }
        }
        String time = "";
        if (deadline != 0) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            time = sdf.format(new Date(deadline));
        }
        return time;
    }

    @Override
    public void onGlobalLayout() {
        hideBar();
    }

    @Override
    public void onDateSelected(int year, int month, int day) {
        String mon = month < 10 ? "0" + month : String.valueOf(month);
        String Day = day < 10 ? "0" + day : String.valueOf(day);
        String date = year + "-" + mon + "-" + Day;
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Calendar calendar = Calendar.getInstance();
        String currentDate = calendar.get(Calendar.YEAR) + "-" + (calendar.get(Calendar.MONTH) + 1) + "-" + calendar.get(Calendar.DAY_OF_MONTH);
        try {
            Date dt1 = df.parse(date);
            Date dt2 = df.parse(currentDate);    // 获取当前时间
//            if (dt1.getTime() >= dt2.getTime()) {
            mTextView.setText(year + "-" + mon + "-" + Day);
            mTextView.setTextColor(0xFF333333);
//            } else {
//                Toast.makeText(context, "保质期至少为1天", Toast.LENGTH_SHORT).show();
//            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        String str = matchPeriod(mEditText.getText().toString().trim());
        if (!str.equals("")) {
            mTextView.setText(str);
            mTextView.setTextColor(0xFF585858);
        } else {
            mTextView.setText("可选填");
            mTextView.setTextColor(0xFFCCCCCC);
        }
    }

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            log.d(TAG, intent.getAction());
            if (intent.getAction().equals(AppConfig.ACTION_KEY_BOARD_STATUS_CHANGE)) {
                if (mEditText.getText().toString().trim().equals("未命名")) {
                    mEditText.setText("");
                } else if (mEditText.getText().toString().trim().equals("")) {
                    mEditText.setText("未命名");
                }
            }
        }
    };

    private static class MyHandler extends Handler {

        WeakReference<FoodEditDialog> weakReference;

        MyHandler(FoodEditDialog dialog) {
            this.weakReference = new WeakReference<>(dialog);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            FoodEditDialog dialog = this.weakReference.get();
            if (dialog != null) {
                switch (msg.what) {
                    case MSG_UPDATE_IMG:    //  显示图片
                        log.d(TAG, "success");
                        Bitmap bitmap = (Bitmap) msg.obj;
                        dialog.mImageView.setImageBitmap(bitmap);
                        break;
                }
            }
        }
    }

    private static class MyRunnable implements Runnable {

        private WeakReference<FoodEditDialog> weakReference;
        private FoodEditDialog dialog;

        MyRunnable(FoodEditDialog dialog) {
            this.weakReference = new WeakReference<>(dialog);
            this.dialog = this.weakReference.get();
        }

        @Override
        public void run() {
            try {
                FileInputStream fileInputStream = new FileInputStream(dialog.path);
                Bitmap bitmap = BitmapFactory.decodeResourceStream(dialog.context.getResources(), new TypedValue(),
                        fileInputStream, null, new BitmapFactory.Options());
                Message msg = dialog.mHandler.obtainMessage();
                msg.what = MSG_UPDATE_IMG;
                msg.obj = bitmap;
                dialog.mHandler.sendMessage(msg);
            } catch (FileNotFoundException e) {
                log.d(TAG, e.toString());
                e.printStackTrace();
            }
        }
    }

    private static class LoadRunnable implements Runnable {

        private WeakReference<FoodEditDialog> weakReference;
        private FoodEditDialog dialog;

        LoadRunnable(FoodEditDialog dialog) {
            this.weakReference = new WeakReference<>(dialog);
            this.dialog = this.weakReference.get();
        }

        @Override
        public void run() {
            if (dialog != null) dialog.loadFoodData();
        }
    }

    public interface OnDeleteListener {
        void onDelete(String type, int position);
    }

    public void setOnDeleteListener(OnDeleteListener onDeleteListener) {
        this.onDeleteListener = onDeleteListener;
    }

    public interface OnModifyListener {
        void onModify(String type, int position, String name, String time, String addTime);
    }

    public void setOnModifyListener(OnModifyListener onModifyListener) {
        this.onModifyListener = onModifyListener;
    }

}
