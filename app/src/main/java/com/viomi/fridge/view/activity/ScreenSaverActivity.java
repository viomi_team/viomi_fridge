package com.viomi.fridge.view.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.viomi.fridge.R;
import com.viomi.fridge.albumhttp.AlbumActivity;
import com.viomi.fridge.service.FloatButtonService;
import com.viomi.fridge.util.ToolUtil;
import com.viomi.fridge.view.adapter.ScreenVPAdapter;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ScreenSaverActivity extends AppCompatActivity {

    private List<ImageView> list;
    private ViewPager viewPager;
    private MyHandler mHandler;
    private SharedPreferences sp;
    private List<File> filelist;

    private static class MyHandler extends Handler {

        private WeakReference<ScreenSaverActivity> weakReference;
        private int i;

        public MyHandler(ScreenSaverActivity activity) {
            this.weakReference = new WeakReference<ScreenSaverActivity>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            ScreenSaverActivity mActivity = weakReference.get();
            mActivity.viewPager.setCurrentItem(i);
            i++;
            mActivity.mHandler.sendEmptyMessageDelayed(0, 10*1000);
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hideNavigationBar();
        setContentView(R.layout.activity_screen_saver);

        //透明状态栏
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        //底下屏幕透明导航栏
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);

        mHandler = new MyHandler(this);
        sp = getSharedPreferences("screen_imgs", MODE_PRIVATE);
        viewPager = (ViewPager) findViewById(R.id.viewpager);

        List<ImageView> ivlist = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            ImageView imageView = new ImageView(this);
            imageView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
            imageView.setBackgroundColor(Color.BLACK);
            ivlist.add(imageView);
        }

        List<File> flist = new ArrayList<>();
        Set<String> screen_set = sp.getStringSet("screen_set", new HashSet<String>());
        for (String value : screen_set) {
            File file = new File(value);
            if (file.exists()) {
                flist.add(file);
            }
        }

        if (flist.size()==0) {
            finish();
            Toast.makeText(this,"请先设置电子相册照片",Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this, AlbumActivity.class);
            startActivity(intent);
        }

        ScreenVPAdapter adapter = new ScreenVPAdapter(ivlist, flist);
        viewPager.setAdapter(adapter);

        mHandler.sendEmptyMessageDelayed(0, 10*1000);
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (ToolUtil.isServiceWork(ScreenSaverActivity.this, "com.viomi.fridge.service.FloatButtonService")) {
            stopService(new Intent(this, FloatButtonService.class));
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (!ToolUtil.isServiceWork(ScreenSaverActivity.this, "com.viomi.fridge.service.FloatButtonService")) {
            startService(new Intent(this, FloatButtonService.class));
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        finish();
        return super.dispatchTouchEvent(ev);
    }

    /***
     * 隐藏导航栏
     */
    public void hideNavigationBar() {
        int uiFlags = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                | View.SYSTEM_UI_FLAG_FULLSCREEN; // hide status bar

        if (android.os.Build.VERSION.SDK_INT >= 19) {
            uiFlags |= 0x00001000;    //SYSTEM_UI_FLAG_IMMERSIVE_STICKY: hide navigation bars - compatibility: building API level is lower thatn 19, use magic number directly for higher API target level
        } else {
            uiFlags |= View.SYSTEM_UI_FLAG_LOW_PROFILE;
        }

        getWindow().getDecorView().setSystemUiVisibility(uiFlags);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mHandler.removeCallbacksAndMessages(null);
    }
}
