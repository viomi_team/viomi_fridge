package com.viomi.fridge.view.fragment;


import android.graphics.Bitmap;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.os.Environment;
import android.os.Message;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.v2.clsdk.fullrelay.FullRelayProxy;
import com.v2.clsdk.fullrelay.FullRelayProxy.FullRelayProxyCallback;
import com.v2.clsdk.model.CameraInfo;
import com.v2.clsdk.model.CameraMessageInfo;
import com.v2.clsdk.p2p.OnCameraMessageListener;
import com.v2.clsdk.p2p.P2pManager;
import com.v2.clsdk.player.IVideoPlayer;
import com.v2.clsdk.player.LivePreviewVideoPlayer;
import com.v2.clsdk.player.LivePreviewVideoPlayer.GetVideoPlayerTask;
import com.v2.clsdk.xmpp.XmppDef;
import com.viomi.fridge.R;
import com.viomi.fridge.manager.CameraListManager;
import com.viomi.fridge.util.SDKInstance;
import com.viomi.fridge.util.ToolUtil;
import com.viomi.fridge.util.log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * 摄像头 Fragment
 * Created by William on 2017/11/6.
 */

public class DeviceCameraFragment extends BaseHandlerFragment implements SurfaceHolder.Callback, IVideoPlayer.IPlaybackCallback, View.OnClickListener {

    private static final String ARG_PARAM1 = "param1";
    private SurfaceView mSurfaceView;
    private View mView;
    private TextView mNameTextView, mOfflineTextView, mRecordTextView, mCropTextView; // 设备名称，设备离线提示，录像，截图
    private CameraInfo mCameraInfo; // IPC 包含信息
    private LinearLayout mOrientationLayout, mTipLayout; // 方向键布局，提示布局
    private int orientation = 0;    // 方向
    private boolean mIsShown = false;   // 是否展示
    private GetVideoPlayerTask mGetVideoPlayerTask; // 视频播放任务
    private IVideoPlayer mVideoPlayer;
    private FullRelayProxy mFullRelayProxy;
    private final int MSG_SAVE_IMG_SUCCESS = 1;

    public static DeviceCameraFragment newInstance(String did) {
        DeviceCameraFragment fragment = new DeviceCameraFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, did);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_device_camera, container, false);

        initView();

        initData();

        return mView;
    }

    @Override
    public void onResume() {
        mIsShown = true;
        if (mCameraInfo.isOnline()) {    // 在线
            whenCameraOnline();
        } else {    // 离线
            whenCameraOffline();
        }
        super.onResume();
    }

    @Override
    public void onPause() {
        mIsShown = false;
        stopLivePreview();
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        P2pManager.getInstance().removeMessageListener(mCameraMessageListener); // 移除监听
        super.onDestroyView();
    }

    @Override
    protected void processMsg(Message msg) {
        super.processMsg(msg);
        switch (msg.what) {
            case MSG_SAVE_IMG_SUCCESS:
                Toast.makeText(getActivity(), "保存截图成功", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    private void initView() {
        mSurfaceView = (SurfaceView) mView.findViewById(R.id.fragment_camera_view);
        mOrientationLayout = (LinearLayout) mView.findViewById(R.id.fragment_camera_orientation);
        mTipLayout = (LinearLayout) mView.findViewById(R.id.fragment_camera_tip_layout);
        mNameTextView = (TextView) mView.findViewById(R.id.fragment_camera_name);
        mOfflineTextView = (TextView) mView.findViewById(R.id.fragment_camera_offline);
        mRecordTextView = (TextView) mView.findViewById(R.id.fragment_camera_record);
        mCropTextView = (TextView) mView.findViewById(R.id.fragment_camera_crop);

//        mCropTextView.setOnClickListener(this);
        mView.findViewById(R.id.fragment_camera_up).setOnClickListener(this);
        mView.findViewById(R.id.fragment_camera_down).setOnClickListener(this);
        mView.findViewById(R.id.fragment_camera_left).setOnClickListener(this);
        mView.findViewById(R.id.fragment_camera_right).setOnClickListener(this);

        final SurfaceHolder surfaceHolder = mSurfaceView.getHolder();
        surfaceHolder.addCallback(this);
        surfaceHolder.setSizeFromLayout();
        surfaceHolder.setFormat(PixelFormat.RGBA_8888);
    }

    private void initData() {
        P2pManager.getInstance().addMessageListener(mCameraMessageListener);    // 添加 IPC 状态监听
        if (getArguments() != null) {
            String did = getArguments().getString(ARG_PARAM1);
            log.d(TAG, did);
            mCameraInfo = CameraListManager.getInstance().getCameraInfo(did);   // 获取 IPC 信息
        }
        // 名称
        mNameTextView.setText(mCameraInfo.getName());
        // 判断 IPC 是否支持方向转动
        if (mCameraInfo.isSupportPtz()) {
            mOrientationLayout.setVisibility(View.VISIBLE);
        }
    }

    /**
     * IPC 在线
     */
    private void whenCameraOnline() {
        mOfflineTextView.setVisibility(View.GONE);
        startLivePreview();
    }

    /**
     * IPC 离线
     */
    private void whenCameraOffline() {
        mOfflineTextView.setVisibility(View.VISIBLE);
        stopLivePreview();
    }

    private void startLivePreview() {
        mCropTextView.setEnabled(false);
        mRecordTextView.setEnabled(false);
        mTipLayout.setVisibility(View.VISIBLE);

        if (mGetVideoPlayerTask != null) {
            mGetVideoPlayerTask.cancel();
            mGetVideoPlayerTask = null;
        }

        mGetVideoPlayerTask = new GetVideoPlayerTask(mCameraInfo.getAudioFormat(), ToolUtil.getRecordDirectory(mCameraInfo.getSrcId()),
                this, player -> {
            if (mHandler != null)
                mHandler.postAtFrontOfQueue(() -> {
                    if (player != null) {
                        mVideoPlayer = player;
                        try {
                            mVideoPlayer.init();
                            if (mSurfaceView.getHolder().getSurface().isValid()) {
                                mVideoPlayer.setSurface(mSurfaceView.getHolder());
                            }

                            mFullRelayProxy = new FullRelayProxy(getActivity(), mCameraInfo);
                            mFullRelayProxy.setCallback(new FullRelayProxyCallback() {
                                @Override
                                public void onMessage(int type, int value) {
                                    if (type == FullRelayProxyCallback.MessageType_LowDownstream) {
                                        log.d(TAG, "Client low downstream.");
                                    } else if (type == FullRelayProxyCallback.MessageType_LowUpstream) {
                                        log.d(TAG, "Camera low upstream.");
                                    } else if (type == FullRelayProxyCallback.MessageType_SDCard_Playback_Busy) {
                                        log.d(TAG, "Some client is playing recorded video on sdcard");
                                    } else if (type == FullRelayProxyCallback.MessageType_Timeline_Starttime) {
                                        log.d(TAG, "Camera delete some old data on sdcard");
                                    } else if (type == FullRelayProxyCallback.MessageType_SDCard_Plug) {
                                        if (value == 0)
                                            log.d(TAG, "SDCard is Pluged out");
                                        else if (value == 1)
                                            log.d(TAG, "SDCard is Pluged in.");
                                    }
                                }

                                @Override
                                public void onAudioTalkStatusChanged(int status) {
                                    if (status == FullRelayProxyCallback.AudioTalkStatus_Connecting) {
                                        // 语音连接中
                                        log.d(TAG, "AudioTalk: connecting...");
                                    } else if (status == FullRelayProxyCallback.AudioTalkStatus_Busy) {
                                        // 语音繁忙
                                        log.d(TAG, "AudioTalk: Camera is busy. Try again later.");
                                    } else if (status == FullRelayProxyCallback.AudioTalkStatus_Idle) {
                                        // 语音开始
                                        log.d(TAG, "AudioTalk: Talking now.");
                                    }
                                }
                            });
                            ((LivePreviewVideoPlayer) mVideoPlayer).setFullRelayProxy(mFullRelayProxy);
                            mVideoPlayer.setUrl(getActivity().getApplicationContext(), mFullRelayProxy.formatPlayUrl());
                        } catch (Exception e) {
                            mTipLayout.setVisibility(View.GONE);
                            e.printStackTrace();
                        }
                    } else {
                        mTipLayout.setVisibility(View.GONE);
                    }
                });
        });
        mGetVideoPlayerTask.start();
    }

    private void stopLivePreview() {
        mRecordTextView.setEnabled(false);
        mCropTextView.setEnabled(false);

//        stopUpdateTime();
        if (mVideoPlayer != null) {
            mVideoPlayer.close();
            mVideoPlayer = null;
        }
    }

    /**
     * 截图
     */
    private void captureCrop() {
        if (mVideoPlayer != null) {
            final Bitmap bitmap = mVideoPlayer.captureFrame();
            String Path = Environment.getExternalStorageDirectory().toString() + "SDKTestbed/" + mCameraInfo.getSrcId() + "/";
            if (bitmap != null) {
                mHandler.post(() -> {
                    File dir = new File(Path);
                    boolean isExist = dir.exists() || dir.mkdirs();

                    if (isExist) {
                        String fileName = System.currentTimeMillis() + ".jpg";
                        File file = new File(dir, fileName);
                        FileOutputStream fos = null;
                        try {
                            fos = new FileOutputStream(file);
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                            fos.flush();
                            if (mHandler != null) Message.obtain(mHandler, MSG_SAVE_IMG_SUCCESS);
                        } catch (IOException e) {
                            e.printStackTrace();
                        } finally {
                            if (fos != null) {
                                try {
                                    fos.close();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                });
            }
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        if (mHandler != null) {
            mHandler.postDelayed(() -> {
                if (mVideoPlayer != null) {
                    if (mSurfaceView.getHolder().getSurface().isValid()) {
                        mVideoPlayer.setSurface(mSurfaceView.getHolder());
                    }
                }
            }, 500);
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        if (mVideoPlayer != null) {
            mVideoPlayer.setSurface(null);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_camera_up:   // 上
                orientation = 1;
            case R.id.fragment_camera_down: // 下
                orientation = 2;
            case R.id.fragment_camera_left: // 左
                orientation = 3;
            case R.id.fragment_camera_right:    // 右
                orientation = 4;
                if (mHandler != null) mHandler.post(mRunnable);
                break;
            case R.id.fragment_camera_crop: // 截图
                captureCrop();
                break;
        }
    }

    @Override
    public void onPlayerPrepared(IVideoPlayer iVideoPlayer) {
        iVideoPlayer.start();
    }

    @Override
    public void onPlayerStatusChanged(IVideoPlayer iVideoPlayer, int i) {

    }

    @Override
    public void onPlayerStatusChanged(IVideoPlayer iVideoPlayer, int newStatus, int code) {
        if (newStatus == IVideoPlayer.Player_Status_Error) {
            stopLivePreview();
            new Thread(() -> {
                SystemClock.sleep(5000);
                if (!mIsShown) return;

                getActivity().runOnUiThread(this::startLivePreview);
            }).start();
        }
    }

    @Override
    public void onPlayerBuffering(IVideoPlayer iVideoPlayer, boolean bBuffering) {
        if (bBuffering) {
            mTipLayout.setVisibility(View.VISIBLE);
        } else {
            if (iVideoPlayer.isRendering()) {
                mTipLayout.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onPlayerRendering(IVideoPlayer iVideoPlayer, boolean start) {
        if (start && !isRemoving()) {
            mCropTextView.setEnabled(true);
            mRecordTextView.setEnabled(true);
            mTipLayout.setVisibility(View.GONE);
//            startUpdateTime();
        }
    }

    @Override
    public void onPlayerMagicZoomStatusChanged(boolean b) {

    }

    @Override
    public void onPlayerTrackEnd(IVideoPlayer iVideoPlayer) {

    }

    @Override
    public void onPlayerVideoSizeChanged(IVideoPlayer iVideoPlayer, int i, int i1) {

    }

    /**
     * IPC 状态监听
     */
    private OnCameraMessageListener mCameraMessageListener = new OnCameraMessageListener() {
        @Override
        public void onCameraOnline(String srcId) {
            if (mCameraInfo.getSrcId().equalsIgnoreCase(srcId)) {
                mHandler.post(() -> whenCameraOnline());
            }
        }

        @Override
        public void onCameraOffline(String srcId) {
            if (mCameraInfo.getSrcId().equalsIgnoreCase(srcId)) {
                mHandler.post(() -> whenCameraOffline());
            }
        }

        @Override
        public void onCameraMessage(MessageType messageType, Object object) {
            if (messageType == MessageType.CameraMessage) {
                if (CameraMessageInfo.class.isInstance(object)) {
                    final CameraMessageInfo messageInfo = (CameraMessageInfo) object;
                    if (mCameraInfo.getSrcId().equalsIgnoreCase(messageInfo.getSrcId())) {
                        if (messageInfo.getType() == CameraMessageInfo.MessageType_LowCameraUpstream) {
                            log.d(TAG, "Camera low upstream");
                        }
                    }
                }
            }
        }
    };

    /**
     * IPC 方向控制线程
     */
    Runnable mRunnable = () -> {
        switch (orientation) {
            case 1: // 上
                SDKInstance.getInstance().doPtzOnce(mCameraInfo, XmppDef.PtzValue_LensPanUp);
                break;
            case 2: // 下
                SDKInstance.getInstance().doPtzOnce(mCameraInfo, XmppDef.PtzValue_LensPanDown);
                break;
            case 3: // 左
                SDKInstance.getInstance().doPtzOnce(mCameraInfo, XmppDef.PtzValue_LensPanLeft);
                break;
            case 4: // 右
                SDKInstance.getInstance().doPtzOnce(mCameraInfo, XmppDef.PtzValue_LensPanRight);
                break;
        }
    };

}
