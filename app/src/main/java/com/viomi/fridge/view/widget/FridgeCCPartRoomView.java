package com.viomi.fridge.view.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.viomi.fridge.R;
import com.viomi.fridge.common.GlobalParams;
import com.viomi.fridge.model.bean.SerialInfo;

/**
 * Created by young2 on 2017/2/24.
 */

public class FridgeCCPartRoomView extends RelativeLayout {
    private TextView mTempView;
    private  SnowRatoteView mAnimationView;
    public FridgeCCPartRoomView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context,attrs);
    }

    public FridgeCCPartRoomView(Context context, AttributeSet attrs) {
        this(context, attrs,0);
    }

    public FridgeCCPartRoomView(Context context) {
        this(context,null);
    }

    private void init(Context context, AttributeSet attrs){

        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.FridgeRoomText);
        String roomText=typedArray.getString(R.styleable.FridgeRoomText_nameText);
        typedArray.recycle();
        View.inflate(context, R.layout.view_fridge_room,this);

        mAnimationView= (SnowRatoteView) findViewById(R.id.room_set_animation);
        mTempView= (TextView) findViewById(R.id.temp_view);
        mTempView.setTypeface(GlobalParams.getInstance().mDigitalTypeface);
        TextView room= (TextView) findViewById(R.id.room_name);

        room.setText(roomText+" ℃");

    }

    public void setTemp(int temp){
        if(temp== SerialInfo.ROOM_CLOSE_TEMP){
            mTempView.setText("--");
        }else {
            mTempView.setText(""+temp);
        }
    }

    /***
     * 显示温度和开关
     * @param temp
     * @param enable
     */
    public void setTemp(int temp,boolean enable){
        if(!enable){
            mTempView.setText("--");
        }else {
            mTempView.setText(""+temp);
        }

    }

    /***
     * 显示动画
     * @param enable
     */
    public void showAnimation(boolean enable,int id,int type){
        if(enable){
            mAnimationView.setImage(id,type);
            mAnimationView.setVisibility(VISIBLE);
        }else {
            mAnimationView.setVisibility(INVISIBLE);
        }
    }

    public int getType(){
        return mAnimationView.getType();
    }

}
