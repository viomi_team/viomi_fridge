package com.viomi.fridge.view.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Environment;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.viomi.fridge.R;
import com.viomi.fridge.device.AppConfig;
import com.viomi.fridge.device.DeviceConfig;
import com.viomi.fridge.model.bean.FoodDetailData;
import com.viomi.fridge.util.FileUtil;
import com.viomi.fridge.util.log;
import com.viomi.fridge.view.adapter.FoodManageAdapter;
import com.viomi.fridge.view.widget.FoodEditDialog;
import com.viomi.fridge.view.widget.MyProgressDialog;
import com.viomi.fridge.view.widget.RecyclerSpace;
import com.viomi.fridge.view.widget.WrapContentGridLayoutManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

/**
 * 食材管理 Activity
 * Created by William on 2017/6/16.
 */

public class FoodManageActivity extends BaseHandlerActivity implements View.OnClickListener, FoodManageAdapter.OnFoodClickListener, FoodEditDialog.OnDeleteListener,
        FoodEditDialog.OnModifyListener, View.OnLayoutChangeListener {

    private static final String TAG = FoodManageActivity.class.getSimpleName();
    private static final String Path = Environment.getExternalStorageDirectory().toString() + "/viomi/" + "food_manage.xml";
    private RelativeLayout mChillRelativeLayout, mChangeRelativeLayout, mFreezeRelativeLayout;  // 冷藏室，变温室和冷冻室选择
    private LinearLayout mLinearLayout;  // 添加食材
    private RecyclerView mRecyclerView; // 食材列表
    private ImageView mImageView;   // 无食材时显示图片
    private View mLineOne, mLineTwo;    // 分割线
    private TextView mEditTextView, mDeleteTextView, mFinishTextView;  // 编辑，删除，完成
    private List<FoodDetailData> mAllList, mChoseList;
    private FoodManageAdapter mAdapter;
    private FoodEditDialog mFoodEditDialog; // 食材编辑 Dialog
    private static final int MSG_LOAD_DATA_FAIL = 0;    // 读取失败
    private static final int MSG_LOAD_DATA_SUCCESS = 1; // 读取成功
    private static final int MSG_DELAY = 2;
    private boolean isModify = false;   // 是否修改
    public boolean isFinish = true;
    private int type = 0, count = 0, outDate = 0;   // 0: 冷藏区，1: 变温区，2: 冷冻区；计算编辑个数；食材过期个数
    private DeleteRunnable mDeleteRunnable = new DeleteRunnable(this);  // 批量删除食材
    private LoadRunnable mLoadRunnable = new LoadRunnable(this); // 读取食材信息
    private MyProgressDialog mProgressDialog;   // 加载 Dialog
    private LocalBroadcastManager mLocalBroadcastManager;

    private JSONObject object = null;
    private JSONArray array = null;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_manage);

        initView();
        FileUtil.writeTxtToFile(FoodManageActivity.this, AppConfig.EventKey.EVENT_ENTER_FOODMANAGER, null);
        mHandler.post(mLoadRunnable);

        registerReceiver();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mAllList = null;
        mChoseList = null;
        mLocalBroadcastManager = null;
        if (mFoodEditDialog != null) mFoodEditDialog.dismiss();
        if (mProgressDialog != null) mProgressDialog.dismiss();
        if (mDeleteRunnable != null) mDeleteRunnable = null;
        if (mLoadRunnable != null) mLoadRunnable = null;
        FileUtil.writeTxtToFile(FoodManageActivity.this, AppConfig.EventKey.EVENT_EXIT_FOODMANAGER, null);
        unregisterReceiver(mReceiver);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (requestCode == 1010 && resultCode == 1011) {
            isModify = true;
            log.d(TAG, "return data success");
            FoodDetailData data = (FoodDetailData) intent.getSerializableExtra("new_food");
            mAllList.add(data);
            FileUtil.writeTxtToFile(FoodManageActivity.this, AppConfig.EventKey.EVENT_ADD_FOOD, data.toJson());
            if (data.type.equals(String.valueOf(type))) {
                mChoseList.add(outDate, data);
                mAdapter.notifyItemInserted(outDate);
                mAdapter.notifyItemRangeChanged(outDate, mChoseList.size() - outDate);
            }
            if (mRecyclerView.getVisibility() != View.VISIBLE && mImageView.getVisibility() == View.VISIBLE) {
                mRecyclerView.setVisibility(View.VISIBLE);
                mImageView.setVisibility(View.GONE);
            }
            if (mEditTextView.getVisibility() != View.VISIBLE)
                mEditTextView.setVisibility(View.VISIBLE);
            // 最大添加食材为50
            if (mAllList.size() >= 50) mLinearLayout.setVisibility(View.GONE);
        }
    }

    private void initView() {
        mChillRelativeLayout = (RelativeLayout) findViewById(R.id.food_manage_chill);
        mChangeRelativeLayout = (RelativeLayout) findViewById(R.id.food_manage_change);
        mFreezeRelativeLayout = (RelativeLayout) findViewById(R.id.food_manage_freeze);
        mLinearLayout = (LinearLayout) findViewById(R.id.food_manage_add);
        mLineOne = findViewById(R.id.food_manage_line_1);
        mLineTwo = findViewById(R.id.food_manage_line_2);
        mImageView = (ImageView) findViewById(R.id.food_manage_no_content);
        mEditTextView = (TextView) findViewById(R.id.food_manage_edit);
        mDeleteTextView = (TextView) findViewById(R.id.food_manage_delete);
        mFinishTextView = (TextView) findViewById(R.id.food_manage_finish);
        mRecyclerView = (RecyclerView) findViewById(R.id.food_manage_list);

        object = new JSONObject();
        array = new JSONArray();
        mAllList = new ArrayList<>();
        mChoseList = new ArrayList<>();
        mAdapter = new FoodManageAdapter(FoodManageActivity.this, mChoseList, false);

        WrapContentGridLayoutManager gridLayoutManager = new WrapContentGridLayoutManager(FoodManageActivity.this, 4);
        RecyclerSpace wideSpace = new RecyclerSpace(22, 0, 0);
        mRecyclerView.setLayoutManager(gridLayoutManager);   // 设置 RecyclerView 布局管理器
        mRecyclerView.addItemDecoration(wideSpace);  // 设置分割线
        mRecyclerView.setAdapter(mAdapter);  // 设置RecyclerView 适配器

        findViewById(R.id.food_manage_back).setOnClickListener(this);
        findViewById(R.id.food_manage_root).addOnLayoutChangeListener(this);
        mChillRelativeLayout.setOnClickListener(this);
        mChangeRelativeLayout.setOnClickListener(this);
        mFreezeRelativeLayout.setOnClickListener(this);
        mLinearLayout.setOnClickListener(this);
        mEditTextView.setOnClickListener(this);
        mFinishTextView.setOnClickListener(this);
        mDeleteTextView.setOnClickListener(this);
        mAdapter.setOnFoodClickListener(this);

        // 两门
        if (DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V3) || DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V31)
                || DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V4)) {
            mChangeRelativeLayout.setVisibility(View.GONE);
            mLineOne.setVisibility(View.GONE);
            mLineTwo.setVisibility(View.GONE);
        }
    }

    // 读取食材信息
    private void loadData() {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            if (!new File(Path).exists()) return;

            FileInputStream fileIS = new FileInputStream(Path);
            Document dom = builder.parse(fileIS);
            dom.normalize();

            Element root = dom.getDocumentElement();
            NodeList items = root.getElementsByTagName("food");   // 查找所有 food 节点

            for (int i = 0; i < items.getLength(); i++) {
                FoodDetailData data = new FoodDetailData();
                // 得到第一个 food 节点
                Element personNode = (Element) items.item(i);
                // 获取 food 节点下的所有子节点
                NodeList childNodes = personNode.getChildNodes();

                for (int j = 0; j < childNodes.getLength(); j++) {
                    Node node = childNodes.item(j); // 判断是否为元素类型
                    if (node.getNodeType() == Node.ELEMENT_NODE) {
                        Element childNode = (Element) node;
                        if ("type".equals(childNode.getNodeName())) {   // 类型
                            data.type = childNode.getFirstChild().getNodeValue();
                        } else if ("name".equals(childNode.getNodeName())) {    // 食材名称
                            data.name = childNode.getFirstChild().getNodeValue();
                        } else if ("time".equals(childNode.getNodeName())) {    // 添加时间
                            data.addTime = childNode.getFirstChild().getNodeValue();
                        } else if ("path".equals(childNode.getNodeName())) {    // 图片保存路径
                            data.imgPath = childNode.getFirstChild().getNodeValue();
                        } else if ("deadline".equals(childNode.getNodeName())) {    // 到期时间
                            data.deadLine = childNode.getFirstChild().getNodeValue();
                        } else if ("push".equals(childNode.getNodeName())) {     // 是否已推送
                            data.isPush = childNode.getFirstChild().getNodeValue();
                        }
                    }
                }
                mAllList.add(data);
            }
            fileIS.close();
            mHandler.sendEmptyMessage(MSG_LOAD_DATA_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            log.d(TAG, e.toString());
            mHandler.sendEmptyMessage(MSG_LOAD_DATA_FAIL);
        }
    }

    private void registerReceiver() {
        mLocalBroadcastManager = LocalBroadcastManager.getInstance(FoodManageActivity.this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(AppConfig.ACTION_FOOD_MANAGE_SEND_DATA);
        registerReceiver(mReceiver, intentFilter);
    }

    /**
     * 添加食材
     */
    private void addFood() {
        if (mAllList.size() >= 50) {
            Toast.makeText(FoodManageActivity.this, "食材最大添加数量为50", Toast.LENGTH_SHORT).show();
        } else {
            Intent intent = new Intent(FoodManageActivity.this, FoodPaintActivity.class);
            intent.putExtra("type", type);
            startActivityForResult(intent, 1010);
        }
    }

    /**
     * 排序(优先显示过期，再按修改时间排序)
     */
    private void sort() {
        long current = System.currentTimeMillis();
        List<FoodDetailData> list = new ArrayList<>();
        for (int i = 0; i < mChoseList.size(); i++) {
            FoodDetailData data = mChoseList.get(i);
            if (!data.deadLine.equals("可选填")) {
                long time = Long.valueOf(data.deadLine) + 24L * 60L * 60L * 1000L;
                if (current > time) {
                    list.add(data);
                    mChoseList.remove(i);
                    i = i - 1;
                }
            }
        }
        outDate = list.size();
        Collections.sort(mChoseList, (o1, o2) -> {
            if (Long.valueOf(o1.addTime) < Long.valueOf(o2.addTime)) return 1;
            return -1;
        });
        mChoseList.addAll(0, list); // 过期在前
    }

    /**
     * 批量删除食材
     */
    private void delete() {
        isModify = true;
        runOnUiThread(() -> {
            if (mProgressDialog == null) {
                mProgressDialog = new MyProgressDialog(FoodManageActivity.this);
                mProgressDialog.setCancelable(false);
            }
            mProgressDialog.show();
        });
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder db = dbFactory.newDocumentBuilder();
            Document dom = db.parse(new File(Path));
            Element root = (Element) dom.getFirstChild();
            NodeList foodList = root.getElementsByTagName("food");

            for (int j = 0; j < mChoseList.size(); j++) {
                if (mChoseList.get(j).getSelect()) {
                    // 删除文件中数据
                    for (int i = 0; i < foodList.getLength(); i++) {
                        Element userUpdate = (Element) foodList.item(i);
                        // 获取 food 节点下的所有子节点
                        NodeList childNodes = userUpdate.getChildNodes();
                        Node node = childNodes.item(5); // 获取唯一标识
                        Element childNode = (Element) node;
                        if (childNode.getFirstChild().getTextContent().equals(mChoseList.get(j).addTime)) {
                            Node pathNode = childNodes.item(7); // 获取食材图片路径
                            Element pathChild = (Element) pathNode;
                            File file = new File(pathChild.getFirstChild().getTextContent());
                            if (file.exists()) {
                                boolean isSuccess = file.delete();
                                log.d(TAG, "Delete selected food image" + isSuccess);
                            }
                            root.removeChild(foodList.item(i));
                            break;
                        }
                    }
                    if (array != null) {
                        array.put(mChoseList.get(j).toJsonObject());
                    }

                    mAllList.remove(mChoseList.get(j));
                    mChoseList.remove(j);
                    j = j - 1;
                }
            }
            if (object != null) {
                if (object.has("list")) {
                    object.remove("list");
                }
                try {
                    object.put("list", array);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            FileUtil.writeTxtToFile(FoodManageActivity.this, AppConfig.EventKey.EVENT_DELETE_FOODMANAGER, object.toString());
            // 保存数据
            TransformerFactory tfFactory = TransformerFactory.newInstance();
            Transformer tf = tfFactory.newTransformer();
            tf.setOutputProperty(OutputKeys.ENCODING, "UTF-8");    // 编码方式
            tf.setOutputProperty(OutputKeys.INDENT, "yes");    // 是否缩进
            tf.transform(new DOMSource(dom), new StreamResult(new File(Path)));
            runOnUiThread(() -> {
                if (mProgressDialog != null) mProgressDialog.dismiss();
                mHandler.sendEmptyMessage(MSG_LOAD_DATA_SUCCESS);
            });
            log.d(TAG, "remove success");
        } catch (ParserConfigurationException | TransformerException | IOException | SAXException e) {
            e.printStackTrace();
            log.d(TAG, "remove fail:" + e.toString());
        }
    }

    // 根据选择类型显示食材
    private void showFood(int type) {
        if (this.type != type) {
            this.type = type;
            mHandler.sendEmptyMessage(MSG_LOAD_DATA_SUCCESS);
        }
    }

    @Override
    protected void processMsg(Message msg) {
        switch (msg.what) {
            case MSG_LOAD_DATA_FAIL:    // 读取失败
                Toast.makeText(FoodManageActivity.this, "读取食材信息失败", Toast.LENGTH_SHORT).show();
                break;
            case MSG_LOAD_DATA_SUCCESS: // 读取成功
                mChoseList.clear();
                mAdapter.setMode(FoodManageAdapter.NORMAL); // 设置为正常模式
                switch (type) {
                    case 0:     // 冷藏室
                        for (int i = 0; i < mAllList.size(); i++)
                            if (mAllList.get(i).type.equals("0")) mChoseList.add(mAllList.get(i));
                        break;
                    case 1:     // 变温室
                        for (int i = 0; i < mAllList.size(); i++)
                            if (mAllList.get(i).type.equals("1")) mChoseList.add(mAllList.get(i));
                        break;
                    case 2:     // 冷冻室
                        for (int i = 0; i < mAllList.size(); i++)
                            if (mAllList.get(i).type.equals("2")) mChoseList.add(mAllList.get(i));
                        break;
                }
                if (mDeleteTextView.getVisibility() == View.VISIBLE)
                    mDeleteTextView.setVisibility(View.GONE);
                if (mFinishTextView.getVisibility() == View.VISIBLE)
                    mFinishTextView.setVisibility(View.GONE);
                if (mChoseList.size() == 0) {
                    outDate = 0;
                    mImageView.setVisibility(View.VISIBLE);
                    mRecyclerView.setVisibility(View.GONE);
                    mEditTextView.setVisibility(View.GONE);
                } else {
                    sort(); // 排序
                    mImageView.setVisibility(View.GONE);
                    mRecyclerView.setVisibility(View.VISIBLE);
                    mAdapter.notifyDataSetChanged();
                    mEditTextView.setVisibility(View.VISIBLE);
                }
                // 最大添加食材数量为50
                if (mAllList.size() >= 50) mLinearLayout.setVisibility(View.GONE);
                else mLinearLayout.setVisibility(View.VISIBLE);
                break;
            case MSG_DELAY:
                isFinish = true;
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.food_manage_back: // 返回
                if (isModify) sendBroadcast(new Intent(AppConfig.ACTION_FOOD_MANAGE_UPDATE));
                finish();
                break;
            case R.id.food_manage_chill:    // 冷藏室选择
                mChillRelativeLayout.setBackgroundResource(R.drawable.shape_white_fade_top);
                mChangeRelativeLayout.setBackgroundResource(0);
                mFreezeRelativeLayout.setBackgroundResource(0);
                if (!DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V3) && !DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V31)
                        && !DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V4)) {
                    mLineOne.setVisibility(View.INVISIBLE);
                    mLineTwo.setVisibility(View.VISIBLE);
                }
                showFood(0);
                break;
            case R.id.food_manage_change:   // 变温室选择
                mChillRelativeLayout.setBackgroundResource(0);
                mChangeRelativeLayout.setBackgroundResource(R.color.white_fade_20);
                mFreezeRelativeLayout.setBackgroundResource(0);
                if (!DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V3) && !DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V31)
                        && !DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V4)) {
                    mLineOne.setVisibility(View.INVISIBLE);
                    mLineTwo.setVisibility(View.INVISIBLE);
                }
                showFood(1);
                break;
            case R.id.food_manage_freeze:   // 冷冻室选择
                mChillRelativeLayout.setBackgroundResource(0);
                mChangeRelativeLayout.setBackgroundResource(0);
                mFreezeRelativeLayout.setBackgroundResource(R.drawable.shape_white_fade_bottom);
                if (!DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V3) && !DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V31)
                        && !DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V4)) {
                    mLineOne.setVisibility(View.VISIBLE);
                    mLineTwo.setVisibility(View.INVISIBLE);
                }
                showFood(2);
                break;
            case R.id.food_manage_add:  // 添加食材
                addFood();
                break;
            case R.id.food_manage_edit: // 管理食材
                mAdapter.setMode(FoodManageAdapter.EDIT);   // 编辑模式
                mEditTextView.setVisibility(View.INVISIBLE);
                mLinearLayout.setVisibility(View.GONE);
                mDeleteTextView.setVisibility(View.VISIBLE);
                mFinishTextView.setVisibility(View.VISIBLE);
                count = 0;
                for (int i = 0; i < mChoseList.size(); i++) mChoseList.get(i).isSelected = false;
                mAdapter.notifyDataSetChanged();
                break;
            case R.id.food_manage_delete:   // 删除
                mHandler.post(mDeleteRunnable);
                break;
            case R.id.food_manage_finish:   // 完成
                mAdapter.setMode(FoodManageAdapter.NORMAL); // 正常模式
                mEditTextView.setVisibility(View.VISIBLE);
                mDeleteTextView.setVisibility(View.GONE);
                mFinishTextView.setVisibility(View.GONE);
                if (mAllList.size() < 50) mLinearLayout.setVisibility(View.VISIBLE);
                else mLinearLayout.setVisibility(View.GONE);
                mAdapter.notifyDataSetChanged();
                break;
        }
    }

    @Override
    public void onChose(FoodDetailData data, int position, int mode) {
        if (mode == FoodManageAdapter.NORMAL) {
            mFoodEditDialog = new FoodEditDialog(FoodManageActivity.this, data.type, data.name, data.deadLine, data.imgPath, data.addTime, position);
            mFoodEditDialog.setOnDeleteListener(this);
            mFoodEditDialog.setOnModifyListener(this);
            mFoodEditDialog.show();
        } else {
            mChoseList.get(position).isSelected = !mChoseList.get(position).getSelect();
            if (mChoseList.get(position).getSelect()) count++;
            else count--;
            mAdapter.notifyItemChanged(position);
        }
    }

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(AppConfig.ACTION_FOOD_MANAGE_SEND_DATA)) {  // 零点刷新数据
                List<FoodDetailData> list = (List<FoodDetailData>) intent.getSerializableExtra("food_list");
                mAllList.clear();
                mAllList.addAll(list);
                mHandler.sendEmptyMessage(MSG_LOAD_DATA_SUCCESS);
            }
        }
    };

    @Override
    public void onDelete(String type, int position) {
        log.d(TAG, "food delete success");
        isModify = true;
        long current = System.currentTimeMillis();
        long deadLine = 0;
        String time = mChoseList.get(position).deadLine;
        if (!time.equals("可选填"))
            deadLine = Long.valueOf(time) + 24L * 60L * 60L * 1000L;

        if (array != null) {
            array.put(mChoseList.get(position).toJsonObject());
        }
        if (object != null) {
            if (object.has("list")) {
                object.remove("list");
            }
            try {
                object.put("list", array);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        FileUtil.writeTxtToFile(FoodManageActivity.this, AppConfig.EventKey.EVENT_DELETE_FOODMANAGER, object.toString());
        mAllList.remove(mChoseList.get(position));
        mChoseList.remove(position);
        if (mChoseList.size() == 0) {
            outDate = 0;
            mImageView.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
            mEditTextView.setVisibility(View.GONE);
        } else {
            if (!time.equals("可选填") && current >= deadLine) outDate--;
            mAdapter.notifyItemRemoved(position);
            mAdapter.notifyItemRangeChanged(position, mChoseList.size() - position);
        }
        if (mAllList.size() < 50 && mLinearLayout.getVisibility() != View.VISIBLE)
            mLinearLayout.setVisibility(View.VISIBLE);
    }

//    private String getString(FoodDetailData data) {
//        if (array!=null){
//            array.put(data.toJsonObject());
//        }
//        if (object!=null){
//            try {
//                object.put("list",array);
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//        }
//        return object.toString();
//    }

    @Override
    public void onModify(String type, int position, String name, String time, String addTime) {
        log.d(TAG, "food modify success");
        isModify = true;
        long current = System.currentTimeMillis();
        long deadLine = 0;
        int i = mAllList.indexOf(mChoseList.get(position)); // 找到下标
        if (!mChoseList.get(position).deadLine.equals("可选填"))
            deadLine = Long.valueOf(mChoseList.get(position).deadLine) + 24L * 60L * 60L * 1000L;
        if (!mChoseList.get(position).deadLine.equals("可选填") && current >= deadLine) {
            if (outDate > 0) outDate--;
            FoodDetailData data = mChoseList.get(position);
            data.deadLine = time;
            data.name = name;
            data.addTime = addTime;
            mChoseList.remove(position);
            mChoseList.add(outDate, data);
            mAdapter.notifyItemRangeChanged(position, outDate - position + 1);
            mAllList.set(i, mChoseList.get(outDate));
            if (!data.deadLine.equals("可选填") && current >= Long.valueOf(data.deadLine)) outDate++;
        } else {
            FoodDetailData data = mChoseList.get(position);
            data.deadLine = time;
            data.name = name;
            data.addTime = addTime;
            mChoseList.remove(position);
            mChoseList.add(outDate, data);
            mAdapter.notifyItemRangeChanged(outDate, position - outDate + 1);
            mAllList.set(i, mChoseList.get(outDate));
            if (!data.deadLine.equals("可选填") && current >= Long.valueOf(data.deadLine)) outDate++;
        }
    }

    @Override
    public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
        if (mFoodEditDialog != null && mFoodEditDialog.isShowing() && isFinish) {
            isFinish = false;
            mLocalBroadcastManager.sendBroadcast(new Intent(AppConfig.ACTION_KEY_BOARD_STATUS_CHANGE));
            mHandler.sendEmptyMessageDelayed(MSG_DELAY, 300);
        }
    }

    /**
     * 读取食材信息
     */
    private static class LoadRunnable implements Runnable {

        private WeakReference<FoodManageActivity> weakReference;
        private FoodManageActivity activity;

        LoadRunnable(FoodManageActivity activity) {
            this.weakReference = new WeakReference<>(activity);
            this.activity = this.weakReference.get();
        }

        @Override
        public void run() {
            if (activity != null) activity.loadData();
        }
    }

    /**
     * 批量删除食材
     */
    private static class DeleteRunnable implements Runnable {

        private WeakReference<FoodManageActivity> weakReference;
        private FoodManageActivity activity;

        DeleteRunnable(FoodManageActivity activity) {
            this.weakReference = new WeakReference<>(activity);
            this.activity = this.weakReference.get();
        }

        @Override
        public void run() {
            if (activity != null) {
                log.d(TAG, "count = " + activity.count);
                if (activity.count > 0) {
                    activity.delete();
                } else {
                    activity.runOnUiThread(() -> Toast.makeText(activity, "请选您要删除的食材", Toast.LENGTH_SHORT).show());
                }
            }
        }
    }

}
