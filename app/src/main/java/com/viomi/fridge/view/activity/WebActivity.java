package com.viomi.fridge.view.activity;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.WindowManager;
import android.webkit.ConsoleMessage;
import android.webkit.GeolocationPermissions;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amap.api.location.AMapLocationClient;
import com.tencent.smtt.sdk.CookieSyncManager;
import com.viomi.fridge.R;
import com.viomi.fridge.common.rxbus.BusEvent;
import com.viomi.fridge.common.rxbus.RxBus;
import com.viomi.fridge.device.AppConfig;
import com.viomi.fridge.device.AppConstants;
import com.viomi.fridge.model.bean.WebBaseData;
import com.viomi.fridge.util.FileUtil;
import com.viomi.fridge.util.ToastUtil;
import com.viomi.fridge.util.log;
import com.viomi.fridge.view.web.X5WebView;
import com.viomi.fridge.view.web.activity.BaseActivity;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.OnClick;
import rx.Subscription;

@SuppressLint("SetJavaScriptEnabled")
public class WebActivity extends BaseActivity implements X5WebView.OnWebViewListener  {
    private final static String TAG = VmallWebActivity.class.getSimpleName();
    private String mUrl;// 链接
    private WebBaseData mData;
    private boolean mIsError = false;// 是否网络错误
    private AMapLocationClient mLocationClientSingle;// 高德定位
    protected X5WebView mWebView;// 浏览器
//    private VmallWebActivity.VMallJavaScriptInterface mJavaScriptInterface;// JS 接口
    private Subscription mSubscription;// 消息订阅

    @BindView(R.id.browser_fail_layout)
    RelativeLayout mRelativeLayout;// 网络错误布局

    @BindView(R.id.browser_web)
    ViewGroup mViewGroup;// 浏览器

    @BindView(R.id.browser_progress)
    ProgressBar mProgressBar;// 进度条

    @BindView(R.id.title_bar_close)
    ImageView mCloseImageView;// 关闭
    private boolean isMall;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        mTitle = "";
        layoutId = R.layout.activity_browser;
        super.onCreate(savedInstanceState);
        startAssistLocation();
        isMall=getIntent().getExtras().getBoolean("isMall",false);
        mData = getIntent().getExtras().getParcelable(WebBaseData.Intent_String);
        mUrl = mData.url;

        if (isMall){
            FileUtil.writeTxtToFile(WebActivity.this, AppConfig.EventKey.EVENT_ENTER_MALL, null);
        }

        mWebView = new X5WebView(this, null);
        mViewGroup.addView(mWebView, new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));

//        mJavaScriptInterface = new VmallWebActivity.VMallJavaScriptInterface();
//        mWebView.addJavascriptInterface(mJavaScriptInterface, "H5ToNative");

        mWebView.setOnPageFinishListener(this);
        if (mUrl == null) mWebView.loadUrl(AppConstants.URL_VMALL_RELEASE);
        else mWebView.loadUrl(mUrl);
        // Cookie 同步
        CookieSyncManager.createInstance(this);
        CookieSyncManager.getInstance().sync();

        mSubscription = RxBus.getInstance().subscribe(busEvent -> {
            switch (busEvent.getMsgId()) {
                case BusEvent.MSG_LOGIN_SUCCESS: // 登录成功
                    mWebView.reload();
                    break;
                case BusEvent.MSG_LOGOUT_SUCCESS: // 注销成功
                    mWebView.reload();
                    break;
            }
        });

        if (mBackImageView != null) mBackImageView.setOnClickListener(v -> {
            if (mWebView != null && mWebView.canGoBack()) mWebView.goBack();
            else finish();
        });
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        mData = intent.getExtras().getParcelable(WebBaseData.Intent_String);
        if (mData != null && mData.url != null) {
            mUrl = mData.url;
            log.myE(TAG, "loadUrl=" + mUrl);
            mWebView.loadUrl(mUrl);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (isMall){
            FileUtil.writeTxtToFile(WebActivity.this, AppConfig.EventKey.EVENT_EXIT_MALL, null);
        }
        if (mSubscription != null) {
            mSubscription.unsubscribe();
            mSubscription = null;
        }
        if (mWebView != null) {
            mWebView.stopLoading();
            if (mViewGroup != null) mViewGroup.removeAllViews();
            mWebView.removeAllViews();
            mWebView.destroy();
            mWebView = null;
        }
        RxBus.getInstance().post(BusEvent.MSG_START_STROLL);
    }

    @OnClick(R.id.title_bar_close)
    public void close() { // 关闭
        finish();
    }

    @OnClick(R.id.browser_error_retry)
    public void retry() { // 重试
        mWebView.reload();
    }

    /**
     * 启动 H5 辅助定位
     */
    private void startAssistLocation() {
        if (mLocationClientSingle == null) {
            mLocationClientSingle = new AMapLocationClient(this.getApplicationContext());
        }
        mLocationClientSingle.startAssistantLocation();
    }

    @Override
    public void onReceivedTitle(String title) {
        if (mTitleTextView != null) mTitleTextView.setText(title);
        if (mWebView.getX5WebViewExtension() == null && title.equals("找不到网页")) { // 系统内核
            mIsError = true;
            mRelativeLayout.setVisibility(View.VISIBLE);
            mWebView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onProgressChanged(int progress) {
        mProgressBar.setVisibility(View.VISIBLE);
        mProgressBar.setProgress(progress);
        // 优化用户体验
        if (progress >= 80) mProgressBar.setVisibility(View.GONE);
        if (progress >= 30 && !mIsError) {
            if (mRelativeLayout.getVisibility() == View.VISIBLE)
                mRelativeLayout.setVisibility(View.GONE);
            if (mWebView.getVisibility() == View.GONE) mWebView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onPageStarted(String url) {
        mIsError = false;
        mProgressBar.setVisibility(View.VISIBLE);
        if (!url.contains("mi-ae.net")) {
            mWebView.stopLoading();
            mWebView.loadUrl(mUrl);
        }
    }

    @Override
    public void onPageFinished(String url) {
        mUrl = url;
        mProgressBar.setVisibility(View.GONE);
        if (mWebView.canGoBack()) mCloseImageView.setVisibility(View.VISIBLE);
        else mCloseImageView.setVisibility(View.GONE);
    }

    @Override
    public void onReceivedError() {
        if (mWebView.getX5WebViewExtension() == null) { // 系统内核
            mIsError = true;
            mRelativeLayout.setVisibility(View.VISIBLE);
            mWebView.setVisibility(View.GONE);
        }
    }
}
