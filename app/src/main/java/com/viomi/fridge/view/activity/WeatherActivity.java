package com.viomi.fridge.view.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.viomi.common.callback.AppCallback;
import com.viomi.fridge.R;
import com.viomi.fridge.broadcast.BroadcastAction;
import com.viomi.fridge.common.GlobalParams;
import com.viomi.fridge.common.ViomiApplication;
import com.viomi.fridge.manager.VoiceManager;
import com.viomi.fridge.util.JsonUitls;
import com.viomi.fridge.util.WeatherIconUtil;
import com.viomi.fridge.util.log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.Date;

public class WeatherActivity extends BaseActivity {


    private ImageView btn_back;
    private MyHandler myHandler;
    private TextView city_name;
    private TextView update_time;
    private TextView today_time;
    private ImageView weather_icon;
    private TextView tempRange;
    private TextView weather_type;
    private TextView wind_type;
    private TextView wind_level;
    private TextView humidity;
    private TextView air_quality;
    private TextView air_status;
    private LinearLayout days_weather;
    private TextView repeat;
    private static final int RESPONE_WEATHER_CODE = 1002;
    private static final int WHAT_CLOSE_ACTIVITY=100;
    private ProgressBar bar;
    private String mCity="";

    private static class MyHandler extends Handler {
        private WeakReference<WeatherActivity> weakReference;

        public MyHandler(WeatherActivity activity) {
            this.weakReference = new WeakReference<WeatherActivity>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            WeatherActivity mActivity = weakReference.get();
            switch (msg.what) {
                case 0: {
                    mActivity.bar.setVisibility(View.GONE);
                    String result = (String) msg.obj;
                    mActivity.parseJson(result);
                    break;
                }
                case 1: {
                    mActivity.bar.setVisibility(View.GONE);
                    break;
                }
                case WHAT_CLOSE_ACTIVITY :
                    mActivity.finish();
                    break;
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);


        btn_back = (ImageView) findViewById(R.id.btn_back);
        city_name = (TextView) findViewById(R.id.city_name);
        update_time = (TextView) findViewById(R.id.update_time);

        today_time = (TextView) findViewById(R.id.today_time);
        weather_icon = (ImageView) findViewById(R.id.weather_icon);
        tempRange = (TextView) findViewById(R.id.tempRange);
        weather_type = (TextView) findViewById(R.id.weather_type);
        wind_type = (TextView) findViewById(R.id.wind_type);
        wind_level = (TextView) findViewById(R.id.wind_level);
        humidity = (TextView) findViewById(R.id.humidity);
        air_quality = (TextView) findViewById(R.id.air_quality);
        air_status = (TextView) findViewById(R.id.air_status);

        days_weather = (LinearLayout) findViewById(R.id.days_weather);
        repeat = (TextView) findViewById(R.id.repeat);
        bar = (ProgressBar) findViewById(R.id.progress);

        mCity=getIntent().getStringExtra("city");
        initListener();
        myHandler = new MyHandler(this);
        loadWeather();
        myHandler.sendEmptyMessageDelayed(WHAT_CLOSE_ACTIVITY,16*1000);
    }

    private void loadWeather() {
        bar.setVisibility(View.VISIBLE);

        if(mCity==null||mCity.length()==0){
            mCity=GlobalParams.getInstance().getLocationCityName();
        }
        log.myE("SharedPreferences-------------------------------------------------", mCity + "");

        VoiceManager.getInstance().getWeather(mCity, new AppCallback<String>() {
            @Override
            public void onSuccess(String data) {
                log.myE("weather-----------------", data);
                Message msg = myHandler.obtainMessage();
                msg.what = 0;
                msg.obj = data;
                myHandler.sendMessage(msg);
                //定位位置和获取的天气是同一个城市，刷新首页的天数据
                if(GlobalParams.getInstance().getLocationCityName().equals(mCity)){
                    Intent intent=new Intent(BroadcastAction.ACTION_WEATHER_GET);
                    intent.putExtra("result",data);
                    LocalBroadcastManager.getInstance(ViomiApplication.getContext()).sendBroadcast(intent);
                }
            }

            @Override
            public void onFail(int errorCode, String msg) {
                log.myE("weather----------fail-----------", msg + "");
                myHandler.sendEmptyMessage(1);
            }
        });
    }

    private void parseJson(String result) {

        try {
            JSONObject json = new JSONObject(result);
            JSONObject data = JsonUitls.getJSONObject(json, "data");
            JSONArray resultArray = JsonUitls.getJSONArray(data, "result");

            if (resultArray.length() > 0) {
                JSONObject todayData = resultArray.getJSONObject(0);

                String city = JsonUitls.getString(todayData, "city");
                city_name.setText(city);
                String lastUpdateTime = JsonUitls.getString(todayData, "lastUpdateTime");
                update_time.setText(lastUpdateTime + " 更新");
                String date = JsonUitls.getString(todayData, "date");
                today_time.setText(date);
                String tempRange_s = JsonUitls.getString(todayData, "tempRange");
                tempRange.setText(tempRange_s);
                GlobalParams.getInstance().setOutdoorTemp(tempRange_s);
                String weather = JsonUitls.getString(todayData, "weather");
                weather_type.setText(weather);
                WeatherIconUtil.setWeatherIcon(weather_icon, weather);
                String wind = JsonUitls.getString(todayData, "wind");
                wind_type.setText(wind);
                String windLevel_s = JsonUitls.getString(todayData, "windLevel");
                wind_level.setText(windLevel_s + "级");
                String humidity_s = JsonUitls.getString(todayData, "humidity");
                humidity.setText(humidity_s);
                String pm25 = JsonUitls.getString(todayData, "pm25");
                air_quality.setText(pm25);
                String airQuality = JsonUitls.getString(todayData, "airQuality");
                air_status.setText(airQuality);
            }

            days_weather.removeAllViews();
            for (int i = 0; i < 4; i++) {
                View other_day_view = LayoutInflater.from(this).inflate(R.layout.other_weather_layout, null);
                other_day_view.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1));
                days_weather.addView(other_day_view);

                TextView s_week = (TextView) other_day_view.findViewById(R.id.s_week);
                TextView s_date = (TextView) other_day_view.findViewById(R.id.s_date);
                ImageView s_weather_icon = (ImageView) other_day_view.findViewById(R.id.s_weather_icon);
                TextView s_tempRange = (TextView) other_day_view.findViewById(R.id.s_tempRange);
                TextView wind_type = (TextView) other_day_view.findViewById(R.id.wind_type);
                TextView weather_type = (TextView) other_day_view.findViewById(R.id.weather_type);

                if (resultArray.length() > i + 1) {
                    JSONObject item = resultArray.getJSONObject(i + 1);
                    String date = JsonUitls.getString(item, "date");
                    String weather = JsonUitls.getString(item, "weather");
                    String tempRange = JsonUitls.getString(item, "tempRange");
                    String wind = JsonUitls.getString(item, "wind");
                    long dateLong = JsonUitls.getLong(item, "dateLong");

                    Date datef = new Date(dateLong * 1000);
                    String week = String.format("%tA", datef);

                    s_week.setText(week);
                    s_date.setText(date);
                    WeatherIconUtil.setWeatherIcon(s_weather_icon, weather);
                    wind_type.setText(wind);
                    weather_type.setText(weather);
                    s_tempRange.setText(tempRange);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void initListener() {
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESPONE_WEATHER_CODE);
                onBackPressed();

            }
        });

        repeat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadWeather();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        myHandler.removeCallbacksAndMessages(null);
    }


}