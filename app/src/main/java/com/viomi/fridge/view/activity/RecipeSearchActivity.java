package com.viomi.fridge.view.activity;

import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.github.jdsjlzx.interfaces.OnLoadMoreListener;
import com.github.jdsjlzx.interfaces.OnRefreshListener;
import com.github.jdsjlzx.recyclerview.LRecyclerView;
import com.github.jdsjlzx.recyclerview.LRecyclerViewAdapter;
import com.github.jdsjlzx.recyclerview.ProgressStyle;
import com.viomi.fridge.R;
import com.viomi.fridge.device.AppConfig;
import com.viomi.fridge.model.bean.recipe.RecipeDetail;
import com.viomi.fridge.model.bean.recipe.RecipeSeries;
import com.viomi.fridge.mvp.presenter.RecipeSeriesPresenter;
import com.viomi.fridge.mvp.view.RecipeSeriesView;
import com.viomi.fridge.util.FileUtil;
import com.viomi.fridge.util.ToolUtil;
import com.viomi.fridge.view.adapter.RecipeCateAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class RecipeSearchActivity extends BaseActivity implements RecipeSeriesView {

    private ImageView iv_back, iv_search;
    private EditText etKeywords;
    private String keyWords;
    private static final int PAGE_NUM = 10;
    private int page = 1;
    private LRecyclerView recyclerView;
    protected LRecyclerViewAdapter mLRecyclerViewAdapter = null;
//    private TextView tv_empty;

    //    private SimpleDraweeView sImg;
//    DraweeController draweeController;
    private RelativeLayout rl_nodata_view;
    private RecipeCateAdapter adapter;
    private List<RecipeDetail> dataList;

    RecipeSeriesPresenter presenter = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_search);
        initViews();
    }

    private void initViews() {
        presenter = new RecipeSeriesPresenter(this);
        iv_back = (ImageView) findViewById(R.id.back_icon);
        iv_search = (ImageView) findViewById(R.id.search_icon);
        etKeywords = (EditText) findViewById(R.id.et_keyword);

        etKeywords.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
        etKeywords.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH ||
                        actionId == EditorInfo.IME_ACTION_UNSPECIFIED) {
                    keyWords = getText(etKeywords);
                    if (!TextUtils.isEmpty(keyWords)) {
                        requestMusices();
                    } else {
                        showToast("关键字为空!", true);
                    }
                    return true;
                }
                return false;
            }
        });

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        iv_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                keyWords = getText(etKeywords);
                if (!TextUtils.isEmpty(keyWords)) {
                    requestMusices();
                } else {
                    showToast("关键字为空!", true);
                }
            }
        });

        rl_nodata_view = (RelativeLayout) findViewById(R.id.rl_no_data);
//        sImg = (SimpleDraweeView)
//                findViewById(R.id.sImg);
//        draweeController = Fresco.newDraweeControllerBuilder()
//                .setAutoPlayAnimations(true)
//                .setUri(Uri.parse("res://" + getPackageName() + "/" + R.drawable.loading_g))
//                .build();
//        sImg.setController(draweeController);
//        tv_empty = (TextView) findViewById(R.id.tv_empty);

        recyclerView = (LRecyclerView) findViewById(R.id.list);
        recyclerView.setEmptyView(rl_nodata_view);
        // recyclerView.setPullRefreshEnabled(false);
        recyclerView.setHeaderViewColor(R.color.material_dark, R.color.material, android.R.color.white);
        recyclerView.setRefreshProgressStyle(ProgressStyle.Pacman); //设置下拉刷新Progress的样式
        recyclerView.setFooterViewHint("拼命加载中", "我是有底线的", "网络不给力啊，点击再试一次吧");
        ((DefaultItemAnimator) recyclerView.getItemAnimator()).setSupportsChangeAnimations(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        adapter = new RecipeCateAdapter(mContext, new itemClickListener());
        mLRecyclerViewAdapter = new LRecyclerViewAdapter(adapter);
        recyclerView.setAdapter(mLRecyclerViewAdapter);
        recyclerView.setLoadMoreEnabled(true);
        recyclerView.refreshComplete(10);
        recyclerView.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        recyclerView.refreshComplete(10);
                        requestMusices();
                    }
                }, 1000);
            }
        });

        recyclerView.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        requestMore();
                    }
                }, 1000);
            }
        });
    }

    private class itemClickListener implements RecipeCateAdapter.OnItemClickListener {
        @Override
        public void ItemClick(RecipeDetail entity) {
            if (entity != null) {
                String menuid = entity.getMenuId();
                if (!TextUtils.isEmpty(menuid)) {
                    Bundle bundle = new Bundle();
                    bundle.putString("MenuId", menuid);
                    openActivity(RecipesDetailsActivity.class, bundle);
                }
            }
        }
    }

    private void requestMusices() {
        keyWords = getText(etKeywords);
        if (TextUtils.isEmpty(keyWords)) {
            showToast("关键字为空！", false);
            return;
        }
        JSONObject object=new JSONObject();
        try {
            object.put("name",keyWords);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        FileUtil.writeTxtToFile(mContext, AppConfig.EventKey.EVENT_SEARCH_RECEIPE, object.toString());
        page = 1;
        HashMap<String, String> map = new HashMap<>();
        map.put("key", getString(R.string.app_key));
        map.put("name", keyWords);
        map.put("page", "" + page);
        map.put("size", "" + PAGE_NUM);
        presenter.getRecipeSeries(map);
    }

    private void requestMore() {
        if (TextUtils.isEmpty(keyWords)) {
            showToast("关键字为空！", false);
            return;
        }
        HashMap<String, String> map = new HashMap<>();
        map.put("key", getString(R.string.app_key));
        map.put("name", keyWords);
        map.put("page", "" + page);
        map.put("size", "" + PAGE_NUM);
        presenter.getRecipeSeries(map);
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void loadData(RecipeSeries data) {
        if (page == 1) {
            initDatas(data);
        } else {
            bindDatas(data);
        }
    }

    @Override
    public void onError(String error) {

    }

    private void initDatas(RecipeSeries recipeSeries) {
        if (recipeSeries != null) {
            ArrayList<RecipeDetail> list = ToolUtil.getList(recipeSeries.getList());
            int size = list.size();
            if (list != null && size != 0) {
                if (dataList == null) {
                    dataList = new ArrayList<>();
                }
                if (size == PAGE_NUM) {
                    page++;
                    recyclerView.setNoMore(false);
                } else {
                    recyclerView.setNoMore(true);
                }
                if (dataList.size() > 0) {
                    dataList.clear();
                }
                dataList.addAll(list);
                adapter.setDataList(dataList);
            } else {
                recyclerView.setNoMore(true);
            }
        }
    }

    private void bindDatas(RecipeSeries baseOnline) {
        if (baseOnline != null) {
            ArrayList<RecipeDetail> list = ToolUtil.getList(baseOnline.getList());
            int size = list.size();
            if (list != null && size != 0) {
                if (dataList == null) {
                    dataList = new ArrayList<>();
                }
                if (size == PAGE_NUM) {
                    recyclerView.setNoMore(false);
                    page++;
                } else {
                    recyclerView.setNoMore(true);
                }
                dataList.addAll(list);
                adapter.addAll(dataList);
            } else {
                recyclerView.setNoMore(true);
            }
        }
    }
}
