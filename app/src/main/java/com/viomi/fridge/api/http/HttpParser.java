package com.viomi.fridge.api.http;

import android.util.Log;

import com.viomi.fridge.model.bean.H5UpgradeMsg;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by young2 on 2017/2/9.
 */

public class HttpParser {
    public final static String TAG=HttpParser.class.getSimpleName();

    /****
     * 解析获取二维码返回数据
     * @param json
     * @return
     */
    public static String parserGetLoginQrcodeResponse(String json){
        try {

            JSONObject jsonObject=new JSONObject(json);
            JSONObject resultJson=jsonObject.getJSONObject("mobBaseRes");
            int code=resultJson.getInt("code");
            if (code !=VmallResponseCode.SUCCESS) {
                return null;
            }
            return   resultJson.getString("result");
        } catch (JSONException e) {
            Log.e(TAG,"parserGetLoginQrcodeResponse error!");
            e.printStackTrace();
            return null;
        }
    }

    /***
     *H5信息
     * @param response
     * @return
     */
    public static H5UpgradeMsg parserH5UpgradeInfo(String response){

        H5UpgradeMsg h5UpgradeMsg=new H5UpgradeMsg();
        try {
            JSONObject jsonObject=new JSONObject(response);
            JSONArray dataList=jsonObject.getJSONArray("data");
            JSONObject data=dataList.getJSONObject(0);
            h5UpgradeMsg.url=data.getString("url");
            h5UpgradeMsg.upgradeDescription=data.getString("detail");
            h5UpgradeMsg.versionCode=data.getInt("code");
            String minApiStr=data.getString("min_api_level");
            int index=minApiStr.indexOf(",");
            h5UpgradeMsg.minAppVersion=minApiStr.substring(0,index);
        } catch (JSONException e) {
            h5UpgradeMsg.url=null;
            Log.e(TAG,"parserH5UpgradeInfo fail! msg="+e.getMessage());
        }catch (Exception e){
            h5UpgradeMsg.url=null;
            Log.e(TAG,"parserH5UpgradeInfo fail! msg="+e.getMessage());
        }
        return h5UpgradeMsg;
    }




}
