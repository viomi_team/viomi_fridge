package com.viomi.fridge.api.http;

/**
 * Created by young2 on 2015/12/31.
 */
public interface LoginCallback<T>{

    /***
     * 获取用户信息成功时调用
     * @param data 返回数据
     */
    public void onGetPeopleSuccess(T data);


    /***
     * 登录成功时调用
     */
    public void onLoginSuccess();

    /***
     * 失败时调用
     * @param errorCode 错误码
     * @param msg 错误信息
     */
    public void onFail(int errorCode, String msg);
}
