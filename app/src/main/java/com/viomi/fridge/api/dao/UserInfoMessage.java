package com.viomi.fridge.api.dao;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by young2 on 2017/6/15.
 */
@DatabaseTable(tableName = "user_info")
public class UserInfoMessage {

    @DatabaseField(generatedId = true, useGetSet = true)
    private int id;
    @DatabaseField(useGetSet = true)
    private int infoId;//消息id
    @DatabaseField(useGetSet = true)
    private String topic;//消息类型
    @DatabaseField(useGetSet = true)
    private long time;//消息推送时间
    @DatabaseField(useGetSet = true)
    private String imgUrl;//消息图片url
    @DatabaseField(useGetSet = true)
    private String linkUrl;//跳转链接url
    @DatabaseField(useGetSet = true)
    private String title;//消息标题
    @DatabaseField(useGetSet = true)
    private String content;//消息内容
    @DatabaseField
    private boolean isRead;//是否已读
    @DatabaseField
    private boolean isDelete;//是否已删除
    @DatabaseField(useGetSet = true)
    private String userId;
    @DatabaseField(useGetSet = true)//预留1
    private String reserve1;
    @DatabaseField(useGetSet = true)//预留2
    private String reserve2;
    @DatabaseField(useGetSet = true)//预留3
    private String reserve3;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getInfoId() {
        return infoId;
    }

    public void setInfoId(int infoId) {
        this.infoId = infoId;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getLinkUrl() {
        return linkUrl;
    }

    public void setLinkUrl(String linkUrl) {
        this.linkUrl = linkUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean read) {
        isRead = read;
    }

    public boolean isDelete() {
        return isDelete;
    }

    public void setDelete(boolean delete) {
        isDelete = delete;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getReserve1() {
        return reserve1;
    }

    public void setReserve1(String reserve1) {
        this.reserve1 = reserve1;
    }

    public String getReserve2() {
        return reserve2;
    }

    public void setReserve2(String reserve2) {
        this.reserve2 = reserve2;
    }

    public String getReserve3() {
        return reserve3;
    }

    public void setReserve3(String reserve3) {
        this.reserve3 = reserve3;
    }
}