package com.viomi.fridge.api.dao;

import android.content.Context;

/**
 * Created by young2 on 2017/6/21.
 */

public class AdvertInfoDao  extends BaseDao<AdvertInfoMessage,Integer>{
        public AdvertInfoDao(Context context){
            super(context, AdvertInfoMessage.class);
        }
}
