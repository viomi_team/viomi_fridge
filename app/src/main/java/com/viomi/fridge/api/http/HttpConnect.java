package com.viomi.fridge.api.http;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.miot.common.people.People;
import com.viomi.common.callback.AppCallback;
import com.viomi.common.module.okhttp.OkHttpClientManager;
import com.viomi.common.module.okhttp.progress.ProgressListener;
import com.viomi.fridge.R;
import com.viomi.fridge.common.GlobalParams;
import com.viomi.fridge.common.ViomiApplication;
import com.viomi.fridge.device.AppConfig;
import com.viomi.fridge.manager.AccountManager;
import com.viomi.fridge.model.bean.VmallResponseMsg;
import com.viomi.fridge.util.ApkUtil;
import com.viomi.fridge.util.log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

import okhttp3.Call;

/**
 * Created by young2 on 2016/12/19.
 */

public class HttpConnect {

    private final static String TAG = HttpConnect.class.getSimpleName();

    private static final String vmall_url_debug = "https://auth.mi-ae.net/services";// 扫码登陆商城测试环境
    private static final String vmall_url_real = "https://vmall-auth.mi-ae.net/services";// 扫码登陆商城正式环境
    private static final String vmall_url = GlobalParams.HTTP_DEBUG ?
            (GlobalParams.getInstance().isVmallHttpDebug() ? vmall_url_debug : vmall_url_real) : vmall_url_real;// 商城正式环境

    //    private static final String DEBUG_BASEURL = "https://vj-grey.mi-ae.net/services";// 调试服-灰度1（兼职管理员需求）
    private static final String DEBUG_BASEURL = "https://vj.viomi.com.cn/services";// 调试服 0727/by胡师傅
    private static final String RELEASE_BASEURL = "https://vmall-grey.mi-ae.net/services";// 正式服-灰度1（兼职管理员需求）
    private static final String STORE_BASEURL = GlobalParams.HTTP_DEBUG ?
            (GlobalParams.getInstance().isVmallHttpDebug() ? DEBUG_BASEURL : RELEASE_BASEURL) : RELEASE_BASEURL;// 商城正式环境

    public static final String qrcode_create_url = vmall_url + "/vmall/login/QRCode.json";// 生成商城登录二维码
    public static final String check_login_status_url = vmall_url + "/vmall/login/QRCode.json";// 轮询获取二维码登录状态
    public static final String HOME_PAGE = STORE_BASEURL + "/layout/homepage.json";// 冰箱首页接口
    public static final String HOME_PAGE_NEW = STORE_BASEURL + "/layout/v1/show.json";// 新的冰箱首页接口

    private static final String URL_BAIDU_WEATHER = "http://api.map.baidu.com/telematics/v3/weather";
    private static final String getAppUpgradeInfo = "https://app.mi-ae.com.cn/getdata";//app 升级检测 url
    private static final String device_rpc_url = "https://openapp.io.mi.com/openapp/device/rpc/";//rpc 接口
    private static final String device_data_url = "https://openapp.io.mi.com/openapp/user/get_user_device_data/";//device data 接口

    private static final String STOREMAINURL_REAL = "http://viomi-fridge-vmall.mi-ae.net";
    private static final String STOREMAINURL_DEBUG = "http://viomi-fridge-vmall-test.mi-ae.net";
//private static final String STOREMAINURL_REAL = "http://viomi-fridgex-vmall.mi-ae.net";
//    private static final String STOREMAINURL_DEBUG = "http://viomi-fridgex-vmall-test.mi-ae.net";
    public static final String STOREMAINURL = GlobalParams.HTTP_DEBUG ?
            (GlobalParams.getInstance().isVmallHttpDebug() ? STOREMAINURL_DEBUG : STOREMAINURL_REAL) : STOREMAINURL_REAL;// 商城环境

    public static final String FEEDBACK = STORE_BASEURL + "/user/feedbacks.json";

    public static final String INSTRUCTION = "https://viomi-faq.mi-ae.net/viomi/fridgeintroduce/index.html";
    public static final String INSTRUCTION428 = "http://viomi-resource.mi-ae.net/fridgehelp.html?type=428";
    public static final String INSTRUCTION455 = "http://viomi-resource.mi-ae.net/fridgehelp.html?type=455";
    public static final String INSTRUCTION462 = "http://viomi-resource.mi-ae.net/fridgehelp.html?type=462";
    public static final String UPLOAD_EVENT_FILE = "https://ms.viomi.com.cn/acquisition/fridge/up-file";

    private static final String message_url_debug = "https://vj.viomi.com.cn/information";//消息中心测试环境
    private static final String message_url_real = "https://s.viomi.com.cn/information";//消息中心正式环境
    private static final String message_url =
            GlobalParams.HTTP_DEBUG ? (GlobalParams.getInstance().isVmallHttpDebug() ? message_url_debug : message_url_real) : message_url_real;//消息中心环境
    private static final String get_checkNewInfos = message_url + "/fridge/checkNewInfos";//未读信息获取
    private static final String get_system_infos = message_url + "/fridge/system";//系统信息获取
    private static final String get_user_infos = message_url + "/fridge/user-related";//用户消息获取
    private static final String get_advert_infos = message_url + "/fridge/activity";//促销信息获取
    private static final String report_alias_url = message_url + "/alias ";//上报系统消息别名

    private static final String kfc_url_debug = "http://viomi-kfc-test.mi-ae.net/";// kfc测试环境
    private static final String kfc_url_real = "http://viomi-kfc.mi-ae.net/index.html";// kfc正式环境
    public static final String kfc_url = GlobalParams.HTTP_DEBUG ?
            (GlobalParams.getInstance().isVmallHttpDebug() ? kfc_url_debug : kfc_url_real) : kfc_url_real;//kfc环境

    public static final String WATERMAPURL = "http://analyse-static.mi-ae.net/watermappad.html";
    public static final String COOKMENUURL = "http://viomi-fridge-cookbook.mi-ae.net";

    /**
     * 菜谱系列
     */
    public static final  String RECIPE_BASE_URL="http://apicloud.mob.com/v1/cook";
    public static final String RECIPE_CATE_URL = RECIPE_BASE_URL + "/category/query";//查询菜谱的所有分类。
    public static final String RECIPE_LIST_URL = RECIPE_BASE_URL + "/menu/search";//根据标签ID/菜谱名称查询菜谱详情。。
    public static final String RECIPE_DETAIL_URL = RECIPE_BASE_URL + "/menu/query";//根据菜谱ID查询菜谱详情。

    public static final int ERROR_CODE_HTTP_REQUEST = -100;
    public static final String ERROR_MESSAGE_HTTP_REQUEST = ViomiApplication.getContext().getString(R.string.toast_http_request_error);
    public static final int ERROR_CODE_HTTP_RESULT = -101;
    public static final String ERROR_MESSAGE_HTTP_RESULT = ViomiApplication.getContext().getString(R.string.toast_http_result_error);
    public static final int FAIL_CODE_HTTP_RESULT = -102;
    public static final String FAIL_MESSAGE_HTTP_RESULT = ViomiApplication.getContext().getString(R.string.toast_http_result_fail);


    /***
     * 获取城市温度
     *
     * @param city
     * @param callback
     * @return
     */
    public static Call getCityWeather(Context context, String city, OkHttpClientManager.ResultCallback callback) {
        String url = URL_BAIDU_WEATHER + "?location=" + city + "&output=json&ak="
                + ApkUtil.getApplicationMetaDataValue(context, "com.baidu.lbsapi.API_KEY")
                + "&mcode=0F:2E:F0:BC:1B:BC:E2:C2:71:F8:E2:99:A1:0F:D1:89:F2:70:C1:DB;" + ApkUtil.getPackageName();
        log.d(TAG, "getCityWeather,url=" + url);
        return OkHttpClientManager.getAsync(url, callback);
    }

    /***
     * 获取商城登录二维码
     *
     * @param callback
     * @return
     */
    public static Call getLoginQrcode(String clientID, AppCallback<String> callback) {
        String url = qrcode_create_url + "?clientID=" + clientID + "&type=1";
        log.d(TAG, "getLoginQrcode,url=" + url);
        return getHttpRequest(url, callback);
    }

    /***
     * 轮询获取二维码登录状态
     *
     * @param callback
     * @return
     */
    public static Call checkLoginStatus(String clientID, AppCallback<String> callback) {
        String url = check_login_status_url;
        log.d(TAG, "checkLoginStatus,url=" + url);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("clientID", clientID);
            log.d(TAG, "json=" + jsonObject);
            return postHttpRequest(url, jsonObject, callback);
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e(TAG, "checkLoginStatus,JSONException=" + e.getMessage());
            callback.onFail(ERROR_CODE_HTTP_REQUEST, ERROR_MESSAGE_HTTP_REQUEST);
            return null;
        }
    }

    /***
     * 获取app升级信息
     *
     * @param appPackage
     * @param callback
     */
    public static Call getAppUpgradeInfo(String appPackage, OkHttpClientManager.ResultCallback<String> callback) {
        Log.i(TAG, "getAppUpgradeInfo,package=" + appPackage);
        String url = getAppUpgradeInfo + "?type=version&package=" + appPackage + "&p=1&l=1";
        log.d(TAG, "getAppUpgradeInfo,url=" + url);
        return OkHttpClientManager.getAsync(url, callback);
    }

    /***
     * 获取商城h5插件升级信息
     *
     * @param callback
     */
    public static Call getVmallUpgradeInfo(OkHttpClientManager.ResultCallback<String> callback) {
        Log.i(TAG, "getVmallUpgradeInfo");
        String url = getAppUpgradeInfo + "?type=pversion&package=com.viomi.fridge.h5&p=1&l=1";
        log.d(TAG, "getVmallUpgradeInfo,url=" + url);
        return OkHttpClientManager.getAsync(url, callback);
    }

    private static String filePath = "/sdcard/device/";//文件夹路径
    private static final String fileName = "temp.txt";

    /***
     * 异步下载文件
     * @param callback
     * @return
     */
//    public static Call upLoadFileAsync(OkHttpClientManager.ResultCallback callback) {
//        String strFilePath = filePath + fileName;
//        File file = new File(strFilePath);
//        if (file.exists()) {
//            try {
//                return OkHttpClientManager.postAsync(HttpConnect.UPLOAD_EVENT_FILE, callback, file, "file", null);
//            } catch (IOException e) {
//                e.printStackTrace();
//                return null;
//            }
//        } else {
//            return null;
//        }
//    }

//    public static Call upLoadFileAsync(String url, OkHttpClientManager.ResultCallback callback, File file, String fileKey) throws IOException {
//        return OkHttpClientManager.postAsync(url, callback, file, fileKey, null);
//    }

    /***
     * 异步下载文件
     *
     * @param url      下载url
     * @param filePath 存储路径
     * @param callback
     * @return
     */
    public static Call downloadFileAsync(String url, String filePath, OkHttpClientManager.ResultCallback<String> callback) {
        log.d(TAG, "downloadFileAsync,url=" + url + ",filePath=" + filePath);
        return OkHttpClientManager.downloadAsync(url, filePath, callback);
    }

    /***
     * 异步下载文件，带监听进度
     *
     * @param url      下载url
     * @param filePath 存储路径
     * @param callback
     * @return
     */
    public static Call downloadFileAsync(String url, String filePath, OkHttpClientManager.ResultCallback<String> callback, ProgressListener progressListener) {
        log.d(TAG, "downloadFileAsync,url=" + url + ",filePath=" + filePath);
        return OkHttpClientManager.downloadAsync(url, filePath, callback, progressListener);
    }

    /***
     * 设备rpc
     * @param people：用户
     * @param did：设备id
     * @param data：传值
     * @param callback：回调
     * @return Call
     */
    public static Call rpcDevice(People people, String did, String data, OkHttpClientManager.ResultCallback<String> callback) {
        long clientId;

        if (GlobalParams.getInstance().getScanPhoneType() == 0) {
            clientId = AppConfig.OAUTH_ANDROID_APP_ID;
        } else {
            clientId = AppConfig.OAUTH_IOS_APP_ID;
        }

        String url = device_rpc_url + did + "?data=" + data + "&clientId=" + clientId
                + "&accessToken=" + people.getAccessToken();
        log.d(TAG, "rpcDevice,url = " + url);

        return OkHttpClientManager.getAsync(url, callback);
    }

    /***
     * 设备Data
     * @param people：用户
     * @param data：传值
     * @param callback：回调
     * @return Call
     */
    public static Call deviceData(People people, String data, OkHttpClientManager.ResultCallback<String> callback) {
        long clientId;

        if (GlobalParams.getInstance().getScanPhoneType() == 0)
            clientId = AppConfig.OAUTH_ANDROID_APP_ID;
        else clientId = AppConfig.OAUTH_IOS_APP_ID;

        String url = device_data_url + "?data=" + data + "&clientId=" + clientId + "&accessToken=" + people.getAccessToken();
        log.d(TAG, "deviceData, url = " + url);

        return OkHttpClientManager.getAsync(url, callback);
    }

    /***
     * 通用请求-http post
     *
     * @param url
     * @param callback
     * @return
     */
    public static Call postHttpRequest(String url, JSONObject jsonObject, final AppCallback<String> callback) {
        Call call = null;
        if (url == null || callback == null) {
            return null;
        }
        call = OkHttpClientManager.postAsync(url, new OkHttpClientManager.ResultCallback<String>() {
            @Override
            public void onError(Call call, Exception e) {
                callback.onFail(ERROR_CODE_HTTP_RESULT, ERROR_MESSAGE_HTTP_RESULT);
            }

            @Override
            public void onResponse(String response) {
                log.d(TAG, "postHttpRequest,response=" + response);
                callback.onSuccess(response);
            }
        }, jsonObject);
        return call;
    }


    /***
     * 通用请求-http get
     *
     * @param url
     * @param callback
     * @return
     */
    public static Call getHttpRequest(String url, final AppCallback<String> callback) {
        Call call = null;
        if (url == null || callback == null) {
            return null;
        }
        call = OkHttpClientManager.getAsync(url, new OkHttpClientManager.ResultCallback<String>() {
            @Override
            public void onError(Call call, Exception e) {
                callback.onFail(ERROR_CODE_HTTP_RESULT, ERROR_MESSAGE_HTTP_RESULT);
            }

            @Override
            public void onResponse(String response) {
                log.d(TAG, "getHttpRequest,response=" + response);
                callback.onSuccess(response);
            }
        });
        return call;
    }


    /***
     * 通用请求-http get
     *
     * @param url
     * @param paramsMap 请求参数
     * @param callback
     * @return
     */
    public static Call getRequest(String url, Map<String, String> paramsMap, final AppCallback<String> callback) {
        if (url == null || callback == null) {
            return null;
        }
        Call call = null;

        if (paramsMap.keySet().size() > 0) {
            url = url + "?";
            for (Map.Entry<String, String> entry : paramsMap.entrySet()) {
                url = url + "&" + entry.getKey() + "=" + entry.getValue();
            }
            url.replace("?&", "?");
        }


        call = OkHttpClientManager.getAsync(url, new OkHttpClientManager.ResultCallback<String>() {
            @Override
            public void onError(Call call, Exception e) {
                callback.onFail(ERROR_CODE_HTTP_RESULT, ERROR_MESSAGE_HTTP_RESULT);
            }

            @Override
            public void onResponse(String response) {
                log.d(TAG, "getHttpRequest,response=" + response);
                callback.onSuccess(response);
            }
        });
        return call;
    }

    //get请求handler封装
    public static Call getRequestHandler(String url, Map<String, String> paramsMap, final Handler mhandler, final int successCode, final int failCode) {
        if (url == null || url.length() == 0 || mhandler == null) {
            return null;
        }
        Call call = null;
        if (paramsMap.keySet().size() > 0) {

            String paramsUrl = "";
            for (Map.Entry<String, String> entry : paramsMap.entrySet()) {
                paramsUrl = paramsUrl + "&" + entry.getKey() + "=" + entry.getValue();
            }
            if (paramsUrl.length() > 0) {
                paramsUrl = paramsUrl.substring(1, paramsUrl.length());
            }
            url = url + "?" + paramsUrl;
            log.myE("qrcodeURL", url);
        }

        call = OkHttpClientManager.getAsync(url, new OkHttpClientManager.ResultCallback<String>() {
            @Override
            public void onError(Call call, Exception e) {
                Message msg = mhandler.obtainMessage();
                msg.what = failCode;
                msg.obj = e;
                mhandler.sendMessage(msg);
            }

            @Override
            public void onResponse(String response) {
                Message msg = mhandler.obtainMessage();
                msg.what = successCode;
                msg.obj = response;
                mhandler.sendMessage(msg);
            }
        });
        return call;
    }

    //post请求handler封装
    public static Call postRequestHandler(String url, Map<String, String> paramsMap, final Handler mhandler, final int successCode, final int failCode) {
        if (url == null || url.length() == 0 || mhandler == null) {
            return null;
        }

        JSONObject paramsJson = new JSONObject();

        for (Map.Entry<String, String> entry : paramsMap.entrySet()) {
            try {
                paramsJson.put(entry.getKey(), entry.getValue());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        Call call = null;
        call = OkHttpClientManager.postAsync(url, new OkHttpClientManager.ResultCallback<String>() {
            @Override
            public void onError(Call call, Exception e) {
                Message msg = mhandler.obtainMessage();
                msg.what = failCode;
                msg.obj = e;
                mhandler.sendMessage(msg);
            }

            @Override
            public void onResponse(String response) {
                Message msg = mhandler.obtainMessage();
                msg.what = successCode;
                msg.obj = response;
                mhandler.sendMessage(msg);
            }
        }, paramsJson);
        return call;
    }

    /***
     * 获取最新的信息Id
     * @param mid 小米id
     * @param yid 云米id
     * @param callback
     * @return
     */
    public static Call checkNewInfos(String mid, String yid, AppCallback<String> callback) {
        String url = get_checkNewInfos + "/" + mid + "/" + yid;
        log.d(TAG, "checkNewInfos,url=" + url);
        return getVmallRequest(url, callback);
    }

    /***
     * 获取系统消息
     * @param mid 小米id
     * @param lastId 本地最新消息id
     * @param callback
     * @return
     */
    public static Call getSystemInfos(String mid, int lastId, AppCallback<String> callback) {
        String url = get_system_infos + "/" + mid + "/" + lastId;
        log.d(TAG, "getSystemInfos,url=" + url);
        return getVmallRequestForArray(url, callback);
    }

    /***
     * 获取用户消息
     * @param yid 云米id
     * @param lastId 本地最新消息id
     * @param callback
     * @return
     */
    public static Call getUserInfos(String yid, int lastId, AppCallback<String> callback) {
        String url = get_user_infos + "/" + yid + "/" + lastId;
        log.d(TAG, "getUserInfos,url=" + url);
        return getVmallRequestForArray(url, callback);
    }

    /***
     * 获取促销消息
     * @param lastId 本地最新消息id
     * @param callback
     * @return
     */
    public static Call getAdvertInfos(int lastId, AppCallback<String> callback) {
        String url = get_advert_infos + "/" + lastId;
        log.d(TAG, "getAdvertInfos,url=" + url);
        return getVmallRequestForArray(url, callback);
    }

    /***
     * 上报消息alias
     * @param alias
     * @param callback
     * @return
     */
    public static Call reportAlias(String alias, OkHttpClientManager.ResultCallback<String> callback) {
        String url = report_alias_url;

        OkHttpClientManager.Param param = new OkHttpClientManager.Param();
        param.key = "alias";
        param.value = alias;
        log.d(TAG, "reportDeviceAlias,url=" + url);
        return OkHttpClientManager.postAsync(url, callback, param);
    }


    /***
     * 商城接口通用请求-http get result是array
     * @param url
     * @param callback
     * @return
     */
    public static Call getVmallRequestForArray(String url, final AppCallback<String> callback) {

        if (url == null || callback == null) {
            return null;
        }
        log.d(TAG, "getVmallRequestForArray,url=" + url);
        return OkHttpClientManager.getAsync(url, new OkHttpClientManager.ResultCallback<String>() {
            @Override
            public void onError(Call call, Exception e) {
                callback.onFail(ERROR_CODE_HTTP_RESULT, ERROR_MESSAGE_HTTP_RESULT);
            }

            @Override
            public void onResponse(String response) {
                log.d(TAG, "getVmallRequestForArray,response=" + response);
                baseVmallParseJsonArray(response, callback);
            }
        });
    }

    /***
     * 通用解析 result里返回JsonArray
     * @param response
     * @param callback
     */
    private static void baseVmallParseJsonArray(String response, AppCallback<String> callback) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            JSONObject data = jsonObject.getJSONObject("mobBaseRes");
            int code = data.getInt("code");
            String desc = data.getString("desc");
            JSONArray result = data.optJSONArray("result");

            if (code == VmallResponseMsg.RESPONSE_OK) {
                String resultStr;
                if (result == null) {
                    resultStr = null;
                } else {
                    resultStr = result.toString();
                }
                callback.onSuccess(resultStr);
            } else {
                if (code == VmallResponseMsg.RESPONSE_TOKEN_EXPIRED_EXCEPTION) {
                    AccountManager.deleteViomiUser(ViomiApplication.getContext());
                }
                callback.onFail(code, desc);
            }
        } catch (JSONException e) {
            callback.onFail(FAIL_CODE_HTTP_RESULT, FAIL_MESSAGE_HTTP_RESULT);
        }
    }

    /***
     * 商城接口通用请求-http get
     * @param url
     * @param callback
     * @return
     */
    public static Call getVmallRequest(String url, final AppCallback<String> callback) {

        if (url == null || callback == null) {
            return null;
        }
        log.d(TAG, "getVmallRequest,url=" + url);
        return OkHttpClientManager.getAsync(url, new OkHttpClientManager.ResultCallback<String>() {
            @Override
            public void onError(Call call, Exception e) {
                callback.onFail(ERROR_CODE_HTTP_RESULT, ERROR_MESSAGE_HTTP_RESULT);
            }

            @Override
            public void onResponse(String response) {
                log.d(TAG, "getVmallRequest,response=" + response);
                baseVmallParseJsonObject(response, callback);
            }
        });
    }

    /***
     * 通用解析 result里返回JsonObject
     * @param response
     * @param callback
     */
    private static void baseVmallParseJsonObject(String response, AppCallback<String> callback) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            JSONObject data = jsonObject.getJSONObject("mobBaseRes");
            int code = data.getInt("code");
            String desc = data.getString("desc");
            JSONObject result = data.optJSONObject("result");

            if (code == VmallResponseMsg.RESPONSE_OK) {
                String resultStr;
                if (result == null) {
                    resultStr = null;
                } else {
                    resultStr = result.toString();
                }
                callback.onSuccess(resultStr);
            } else {
                if (code == VmallResponseMsg.RESPONSE_TOKEN_EXPIRED_EXCEPTION) {
                    AccountManager.deleteViomiUser(ViomiApplication.getContext());
                }
                callback.onFail(code, desc);
            }
        } catch (JSONException e) {
            callback.onFail(FAIL_CODE_HTTP_RESULT, FAIL_MESSAGE_HTTP_RESULT);
        }
    }

}
