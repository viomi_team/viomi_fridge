package com.viomi.fridge.api.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by zhouzy on 2014/3/26.
 */
public class DataHelper extends OrmLiteSqliteOpenHelper {
	private static final String DATABASE_NAME = "info.db";
	private static final int DATABASE_VERSION = 1;
	private static DataHelper mInstance;
	private Map<String, Dao> mDaos = new HashMap<String, Dao>();

	public DataHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	/**
	 * 单例获取该Helper
	 *
	 * @param context
	 * @return
	 */
	public static synchronized DataHelper getHelper(Context context)
	{
		context = context.getApplicationContext();
		if (mInstance == null)
		{
			synchronized (DataHelper.class)
			{
				if (mInstance == null)
					mInstance = new DataHelper(context);
			}
		}
		return mInstance;
	}


	@Override
	public void onCreate(SQLiteDatabase sqLiteDatabase,
			ConnectionSource connectionSource) {
		try {
			Log.e(DataHelper.class.getName(), "开始创建数据库");

			TableUtils.createTable(connectionSource, AdvertInfoMessage.class);
			TableUtils.createTable(connectionSource, UserInfoMessage.class);
			TableUtils.createTable(connectionSource, DeviceInfoMessage.class);
			Log.e(DataHelper.class.getName(), "创建数据库成功");

		} catch (SQLException e) {

			Log.e(DataHelper.class.getName(), "创建数据库失败", e);

		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase sqLiteDatabase,
			ConnectionSource connectionSource, int i, int i2) {
		try {
			TableUtils.dropTable(connectionSource, AdvertInfoMessage.class, true);
			TableUtils.dropTable(connectionSource, UserInfoMessage.class, true);
			TableUtils.dropTable(connectionSource, DeviceInfoMessage.class, true);
			onCreate(sqLiteDatabase, connectionSource);

			Log.e(DataHelper.class.getName(), "更新数据库成功");

		} catch (SQLException e) {

			Log.e(DataHelper.class.getName(), "更新数据库失败", e);

		}
	}

	public synchronized Dao getDao(Class clazz) throws SQLException
	{
		Dao dao = null;
		String className = clazz.getSimpleName();

		if (mDaos.containsKey(className))
		{
			dao = mDaos.get(className);
		}
		if (dao == null)
		{
			dao = super.getDao(clazz);
			mDaos.put(className, dao);
		}
		return dao;
	}


	@Override
	public void close() {
		super.close();
		for (String key : mDaos.keySet())
		{
			Dao dao = mDaos.get(key);
			dao = null;
		}
		mInstance=null;
	}
}
