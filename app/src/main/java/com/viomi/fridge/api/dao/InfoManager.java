package com.viomi.fridge.api.dao;

import com.viomi.fridge.common.ViomiApplication;

import java.util.List;

/**
 * Created by young2 on 2017/6/15.
 */

public class InfoManager {

    private static InfoManager INSTANCE;
    private AdvertInfoDao mAdvertInfoDao;
    private UserInfoDao mUserInfoDao;
    private DeviceInfoDao mDeviceInfoDao;
    private final static int MAX_RECORD=200;//每个表最大存储数量

    public static  InfoManager getInstance(){
            if(INSTANCE==null){
                synchronized (InfoManager.class){
                    if(INSTANCE==null){
                        INSTANCE=new InfoManager();
                    }
                }
            }
        return INSTANCE;
    }

    public InfoManager(){
        if(mAdvertInfoDao==null){
            mAdvertInfoDao=new AdvertInfoDao(ViomiApplication.getContext());
        }
        if(mUserInfoDao==null){
            mUserInfoDao=new UserInfoDao(ViomiApplication.getContext());
        }
        if(mDeviceInfoDao==null){
            mDeviceInfoDao=new DeviceInfoDao(ViomiApplication.getContext());
        }
    }

    public void clearDeviceNewsRecord(){
        mDeviceInfoDao.deleteList(mDeviceInfoDao.list());
    }

    public void clearUserNewsRecord(){
        mUserInfoDao.deleteList(mUserInfoDao.list());
    }

    public void clearAdvertNewsRecord(){
        mAdvertInfoDao.deleteList(mAdvertInfoDao.list());
    }

    /***
     * 获取用户消息记录,除去删除
     * @return
     */
    public List<UserInfoMessage> getUserInfoRecords() {
        return mUserInfoDao.findAllByField("isDelete",false);
    }

    /***
     * 获取系统消息记录,除去删除
     * @return
     */
    public  List<DeviceInfoMessage> getDeviceInfoRecord() {
        return mDeviceInfoDao.findAllByField("isDelete",false);
    }

    /***
     * 获取广告消息记录,除去删除
     * @return
     */
    public  List<AdvertInfoMessage> getAdvertInfoRecord() {
       return mAdvertInfoDao.findAllByField("isDelete",false);
    }


    /***
     * 获取用户消息记录总量
     * @return
     */
    public int getUserInfoRecordSize() {
        List<UserInfoMessage> list= mUserInfoDao.list();
        if(list==null){
            return 0;
        }else {
            return list.size();
        }
    }

    /***
     * 获取系统消息记录总量
     * @return
     */
    public int getDeviceInfoRecordSize() {
        List<DeviceInfoMessage> list= mDeviceInfoDao.list();
        if(list==null){
            return 0;
        }else {
            return list.size();
        }
    }

    /***
     * 获取广告消息记录总量
     * @return
     */
    public int getAdvertInfoRecordSize() {
        List<AdvertInfoMessage> list= mAdvertInfoDao.list();
        if(list==null){
            return 0;
        }else {
            return list.size();
        }
    }


    /***
     * 系统消息本地未读数
     * @return
     */
    public int getDeviceNewsNumber(){
        List<DeviceInfoMessage> list= mDeviceInfoDao.findAllByField("isRead",false);
        if(list!=null){
            return list.size();
        }
        return 0;
    }

    /***
     * 用户消息本地未读数
     * @return
     */
    public int getUserNewsNumber(){
        List<UserInfoMessage> list= mUserInfoDao.findAllByField("isRead",false);
        if(list!=null){
            return list.size();
        }
        return 0;
    }

    /***
     * 用户消息本地未读数
     * @return
     */
    public int getAdvertNewsNumber(){
        List<AdvertInfoMessage> list= mAdvertInfoDao.findAllByField("isRead",false);
        if(list!=null){
            return list.size();
        }
        return 0;
    }

    /***
     * 系统消息本地未读
     * @return
     */
    public List<DeviceInfoMessage>  getDeviceNews(){
        return mDeviceInfoDao.findAllByField("isRead",false);
    }

    /***
     * 用户消息本地未读
     * @return
     */
    public List<UserInfoMessage> getUserNews(){
        return mUserInfoDao.findAllByField("isRead",false);
    }

    /***
     * 广告消息本地未读
     * @return
     */
    public List<AdvertInfoMessage> getAdvertNews(){
        return mAdvertInfoDao.findAllByField("isRead",false);
    }


    /***
     * 添加用户消息记录
     * @param message
     */
    public boolean addUserInfoRecord(UserInfoMessage message){
        List<UserInfoMessage> list= mUserInfoDao.list();
        if(list!=null&&list.size()==MAX_RECORD){
            mUserInfoDao.delete(list.get(0));
        }
        return mUserInfoDao.createOrUpdate(message);
    }

    /***
     * 添加广告消息记录
     * @param message
     */
    public boolean addAdvertInfoRecord(AdvertInfoMessage message){
        List<AdvertInfoMessage> list= mAdvertInfoDao.list();
        if(list!=null&&list.size()==MAX_RECORD){
            mAdvertInfoDao.delete(list.get(0));
        }
        return mAdvertInfoDao.createOrUpdate(message);
    }

    /***
     * 添加系统消息记录
     * @param message
     */
    public boolean addDeviceInfoRecord(DeviceInfoMessage message){
        List<DeviceInfoMessage> list= mDeviceInfoDao.list();
        if(list!=null&&list.size()==MAX_RECORD){
            mDeviceInfoDao.delete(list.get(0));
        }
        return mDeviceInfoDao.createOrUpdate(message);
    }

    /***
     * 获取系统消息最新的id
     * @return
     */
    public int getLastDeviceInfoId(){
        List<DeviceInfoMessage> list= mDeviceInfoDao.list();
        if(list!=null&&list.size()>0){
            return list.get(list.size()-1).getInfoId();
        }else {
            return 0;
        }
    }


    /***
     * 获取用户消息最新的id
     * @return
     */
    public int getLastUserInfoId(){
        List<UserInfoMessage> list= mUserInfoDao.list();
        if(list!=null&&list.size()>0){
            return list.get(list.size()-1).getInfoId();
        }else {
            return 0;
        }
    }


    /***
     * 获取广告消息最新的id
     * @return
     */
    public int getLastAdvertInfoId(){
        List<AdvertInfoMessage> list= mAdvertInfoDao.list();
        if(list!=null&&list.size()>0){
            return list.get(list.size()-1).getInfoId();
        }else {
            return 0;
        }
    }

    /***
     * 获取系统未读消息最新的title
     * @return
     */
    public String getLastDeviceInfoTitle(){
        List<DeviceInfoMessage> list= mDeviceInfoDao.findAllByField("isRead",false);
        if(list!=null&&list.size()>0){
            return list.get(list.size()-1).getTitle();
        }else {
            return null;
        }
    }

    /***
     * 获取用户未读消息最新的Title
     * @return
     */
    public String getLastUserInfoTitle(){
        List<UserInfoMessage> list= mUserInfoDao.findAllByField("isRead",false);
        if(list!=null&&list.size()>0){
            return list.get(list.size()-1).getTitle();
        }else {
            return null;
        }
    }

    /***
     * 获取广告未读消息最新的Title
     * @return
     */
    public String getLastAdvertInfoTitle(){
        List<AdvertInfoMessage> list= mAdvertInfoDao.findAllByField("isRead",false);
        if(list!=null&&list.size()>0){
            return list.get(list.size()-1).getTitle();
        }else {
            return null;
        }
    }

    /***
     * 删除系统消息
     * @return
     */
    public boolean deleteDeviceInfo(DeviceInfoMessage info){
        info.setDelete(true);
        info.setRead(true);
       return   mDeviceInfoDao.update(info);
    }


    /***
     * 删除用户消息
     * @return
     */
    public boolean deleteUserInfo(UserInfoMessage info){
        info.setDelete(true);
        info.setRead(true);
        return   mUserInfoDao.update(info);
    }

    /***
     * 删除广告消息
     * @return
     */
    public boolean deleteAdvertInfo(AdvertInfoMessage info){
        info.setDelete(true);
        info.setRead(true);
        return   mAdvertInfoDao.update(info);
    }

    /***读系统消息
     * @return
     */
    public boolean readDeviceInfo(DeviceInfoMessage info){
        info.setRead(true);
        return   mDeviceInfoDao.update(info);
    }


    /***读用户消息
     * @return
     */
    public boolean readUserInfo(UserInfoMessage info){
        info.setRead(true);
        return   mUserInfoDao.update(info);
    }

    /***读广告消息
     * @return
     */
    public boolean readAdvertInfo(AdvertInfoMessage info){
        info.setRead(true);
        return   mAdvertInfoDao.update(info);
    }

    /***
     * 根据id查询广告消息
     * @return
     */
    public AdvertInfoMessage getAdvertInfoById(int infoId){
        List<AdvertInfoMessage> list= mAdvertInfoDao.findAllByField("infoId",infoId);
        if(list!=null&&list.size()>0){
            return list.get(0);
        }
        return null;
    }

    /***
     * 根据id查询用户消息
     * @return
     */
    public UserInfoMessage getUserInfoById(int infoId){
        List<UserInfoMessage> list= mUserInfoDao.findAllByField("infoId",infoId);
        if(list!=null&&list.size()>0){
            return list.get(0);
        }
        return null;
    }

    /***
     * 根据id查询设备消息
     * @return
     */
    public DeviceInfoMessage getDeviceInfoById(int infoId){
        List<DeviceInfoMessage> list= mDeviceInfoDao.findAllByField("infoId",infoId);
        if(list!=null&&list.size()>0){
            return list.get(0);
        }
        return null;
    }


}
