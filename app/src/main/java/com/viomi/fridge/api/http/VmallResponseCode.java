package com.viomi.fridge.api.http;

/**
 * Created by young2 on 2017/2/9.
 */

public class VmallResponseCode {
    public final  static int SUCCESS=100;//处理成功
    public final  static int SOFTWARE_UPDATE=162;//客户端强制升级
    public final  static int ORDER_VALIDATE_EXCEPTION	=604;//订单校验失败
    public final  static int FILE_IMPORT_FAIL=	818;//	文件导入失败
    public final  static int SMS_SEND_FAIL=819;//	短信发送失败
    public final  static int CALL_FAIL=820;//	接口调用失败
    public final  static int SERVER_EXCEPTION	=900;//服务器异常
    public final  static int BUSINESS_REQPARAM_EXCEPTION=901;//	非法请求参数
    public final  static int DATA_NOT_FOUND=902;//无数据记录
    public final  static int BUSINESS_EXCEPTION=915;//业务异常
    public final  static int TOKEN_ABSENT_EXCEPTION=918;//token值为空
    public final  static int TOKEN_EXPIRED_EXCEPTION	=919;//	登录已过期
    public final  static int FEEDBACK_PROCESSED=920;//该反馈已处理
    public final  static int JSON_PARSE_EXCEPTION=	930;//json解析异常
}
