/*
 * Copyright © Yan Zhenjie. All Rights Reserved
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.viomi.fridge.albumhttp;

import android.app.Service;
import android.content.Intent;
import android.content.res.AssetManager;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.viomi.fridge.albumhttp.response.RequestLoginHandler;
import com.viomi.fridge.albumhttp.response.RequestPermission;
import com.viomi.fridge.albumhttp.response.RequestUploadHandler;
import com.viomi.fridge.util.log;
import com.yanzhenjie.andserver.AndServer;
import com.yanzhenjie.andserver.Server;
import com.yanzhenjie.andserver.website.AssetsWebsite;

/**
 * <p>Server service.</p>
 * Created by Yan Zhenjie on 2017/3/16.
 */
public class CoreService extends Service implements RequestUploadHandler.DoneCallBack {

    private Server mServer;
    private AssetManager mAssetManager;

    @Override
    public void onCreate() {
        mAssetManager = getAssets();

        AndServer andServer = new AndServer.Build()
                .port(8080)
                .timeout(10 * 1000)
                .registerHandler("login", new RequestLoginHandler())
                .registerHandler("prepare_connect", new RequestPermission())
//                 .registerHandler("download", new RequestFileHandler("Your file path"))
                .registerHandler("upload_file", new RequestUploadHandler(this))
                .website(new AssetsWebsite(mAssetManager, ""))
                .listener(mListener)
                .build();
        mServer = andServer.createServer();
    }

    /**
     * Server listener.
     */
    private Server.Listener mListener = new Server.Listener() {
        @Override
        public void onStarted() {
        }

        @Override
        public void onStopped() {
        }

        @Override
        public void onError(Exception e) {
        }
    };

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        startServer();
        return START_STICKY;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        stopServer(); // Stop server.
        if (mAssetManager != null)
            mAssetManager.close();
    }

    /**
     * Start server.
     */
    private void startServer() {
        if (mServer != null) {
            if (mServer.isRunning()) {
                log.myE("CoreService", "----------------------------------服务器已经在运行------------------------------------");
            } else {
                log.myE("CoreService", "----------------------------------服务器启动------------------------------------------");
                mServer.start();
            }
        }
    }

    /**
     * Stop server.
     */
    private void stopServer() {
        if (mServer != null) {
            log.myE("CoreService", "----------------------------------服务器终止------------------------------------------");
            mServer.stop();
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public void fileRecieveDone() {
        log.myE("fileRecieveDone", "----------------------------------done--------------------666----------------------");
        sendBroadcast(new Intent("viomi_file_completion"));
    }
}
