package com.viomi.fridge.albumhttp;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;

import com.facebook.drawee.view.SimpleDraweeView;
import com.viomi.fridge.R;

import java.util.List;

/**
 * Created by viomi on 2017/3/28.
 */

public class AlbumAdapter extends BaseAdapter {
    private List<AlbumBean> list;
    private Context context;
    private boolean isedit;

    public AlbumAdapter(List<AlbumBean> list, Context context) {
        this.list = list;
        this.context = context;
    }

    public boolean isIsedit() {
        return isedit;
    }

    public void setIsedit(boolean isedit) {
        this.isedit = isedit;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.album_item_layout, null);
            holder = new ViewHolder();
            holder.iv = (SimpleDraweeView) convertView.findViewById(R.id.iv);
            holder.check_box = (CheckBox) convertView.findViewById(R.id.check_box);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.iv.setImageURI(Uri.fromFile(list.get(position).getThumbfile()));

        if (isedit) {
            holder.check_box.setVisibility(View.VISIBLE);
        } else {
            holder.check_box.setVisibility(View.GONE);
        }

        holder.check_box.setChecked(list.get(position).isselect());

        return convertView;
    }

    class ViewHolder {
        SimpleDraweeView iv;
        CheckBox check_box;
    }
}
