package com.viomi.fridge.albumhttp;

import java.io.File;

/**
 * Created by viomi on 2017/4/5.
 */

public class AlbumBean {

    private File thumbfile;
    private File showfile;
    private boolean isselect;

    public AlbumBean() {
    }

    public AlbumBean(File thumbfile, File showfile, boolean isselect) {
        this.thumbfile = thumbfile;
        this.showfile = showfile;
        this.isselect = isselect;
    }

    public File getThumbfile() {
        return thumbfile;
    }

    public void setThumbfile(File thumbfile) {
        this.thumbfile = thumbfile;
    }

    public File getShowfile() {
        return showfile;
    }

    public void setShowfile(File showfile) {
        this.showfile = showfile;
    }

    public boolean isselect() {
        return isselect;
    }

    public void setIsselect(boolean isselect) {
        this.isselect = isselect;
    }
}
