/*
 * Copyright © Yan Zhenjie. All Rights Reserved
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.viomi.fridge.albumhttp.response;

import android.os.Environment;

import com.viomi.fridge.albumhttp.CoreService;
import com.viomi.fridge.util.PhoneUtil;
import com.viomi.fridge.util.log;
import com.yanzhenjie.andserver.RequestHandler;
import com.yanzhenjie.andserver.upload.HttpFileUpload;
import com.yanzhenjie.andserver.upload.HttpUploadContext;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.ProgressListener;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.http.Header;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.entity.StringEntity;
import org.apache.http.protocol.HttpContext;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

/**
 * <p>Upload file handler.</p>
 * Created by Yan Zhenjie on 2016/6/13.
 */
public class RequestUploadHandler implements RequestHandler {

    private DoneCallBack callBack;

    public RequestUploadHandler(CoreService coreService) {
        if (coreService instanceof DoneCallBack) {
            callBack = coreService;
        }
    }

    @Override
    public void handle(HttpRequest request, HttpResponse response, HttpContext context) throws HttpException, IOException {


        if (!HttpFileUpload.isMultipartContentWithPost(request)) {
            response(403, "You must upload file.", response);
        } else {

            String did = PhoneUtil.getMiIdentify().did + "3331";
            StringBuffer result = new StringBuffer();
            try {
                MessageDigest messageDigest = MessageDigest.getInstance("md5");
                byte[] digest = messageDigest.digest(did.getBytes("UTF-8"));
                for (byte b : digest) {
                    result.append(String.format("%02x", b));
                }
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            Header[] allHeaders = request.getAllHeaders();
            boolean permission = false;

            for (int i = 0; i < allHeaders.length; i++) {
                String name = allHeaders[i].getName();
                String value = allHeaders[i].getValue();
                if ("key".equals(name) && result.toString().equals(value)) {
                    permission = true;
                }
            }

            if (permission) {
                File saveDirectory = new File(Environment.getExternalStorageDirectory(), "viomi_album");
                if (!saveDirectory.exists()) {
                    saveDirectory.mkdir();
                }

                if (saveDirectory.isDirectory()) {
                    try {
                        processFileUpload(request, saveDirectory);
                        response(200, "ok", response);
                        log.myE("recieve_file", "------------------------------done----------------------------------");
                        callBack.fileRecieveDone();

                    } catch (Exception e) {
                        e.printStackTrace();
                        response(500, "Save the file when the error occurs.", response);
                    }
                } else {
                    response(500, "The server can not save the file.", response);
                }
            } else {
                response(403, "No permission.", response);
                throw (new HttpException("No permission."));
            }
        }

    }

    private void response(int responseCode, String message, HttpResponse response) throws IOException {
        response.setStatusCode(responseCode);
        response.setEntity(new StringEntity(message, "utf-8"));
    }

    /**
     * Parse file and save.
     *
     * @param request       request.
     * @param saveDirectory save directory.
     * @throws Exception may be.
     */
    private void processFileUpload(HttpRequest request, File saveDirectory) throws Exception {
        FileItemFactory factory = new DiskFileItemFactory(1024 * 1024, saveDirectory);
        HttpFileUpload fileUpload = new HttpFileUpload(factory);

        // Set upload process listener.
        // fileUpload.setProgressListener(new ProgressListener(){...});

        fileUpload.setProgressListener(new ProgressListener() {
            @Override
            public void update(long l, long l1, int i) {
                log.myE("update", "------------------------------update----------------------------------" + l);
            }
        });

        List<FileItem> fileItems = fileUpload.parseRequest(new HttpUploadContext((HttpEntityEnclosingRequest) request));

        for (FileItem fileItem : fileItems) {
            if (!fileItem.isFormField()) { // File param.
                // Attribute.
                // fileItem.getContentType();
                // fileItem.getFieldName();
                // fileItem.getName();
                // fileItem.getSize();
                // fileItem.getString();

                File uploadedFile = new File(saveDirectory, fileItem.getName());
                // 把流写到文件上。
                fileItem.write(uploadedFile);
            } else { // General param.
                String key = fileItem.getName();
                String value = fileItem.getString();
            }
        }
    }

    public interface DoneCallBack{
        void fileRecieveDone();
    }

}
