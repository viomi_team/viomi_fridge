package com.viomi.fridge.albumhttp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.viomi.fridge.R;
import com.viomi.fridge.device.AppConfig;
import com.viomi.fridge.model.bean.ScreenSleepBean;
import com.viomi.fridge.util.FileUtil;
import com.viomi.fridge.util.ToastUtil;
import com.viomi.fridge.util.log;
import com.viomi.fridge.view.activity.BaseActivity;
import com.viomi.fridge.view.adapter.SleepTimeAdapter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class AlbumActivity extends BaseActivity {

    private GridView gridView;
    private MyHandler myHandler;
    private ImageView backIcon;
    private List<AlbumBean> list;
    private AlbumAdapter albumAdapter;
    private Spinner spinner;
    private CheckBox all_select;
    private Button set_screen;
    private Button delete;
    private SharedPreferences sp;
    private ArrayList<ScreenSleepBean> screenBeanList;
    private RadioButton screen_on;
    private RadioButton screen_off;
    private TextView img_count;
    private final static int ALBUMREPONSECODE = 1004;
    private MakeImgThread makeImgThread;
    private List<File> showList;
    private BroadcastReceiver fileReceiver;
    private ImageView album_guide;
    private RelativeLayout loading_layout;
    private LinearLayout edit_layout;
    private boolean isedit;
    private TextView edit_btn;
    private boolean isAutoAllCheck;
    private SimpleDraweeView album_detail;
    private ImageView album_detail_pre;
    private ImageView album_detail_next;
    private ImageView album_detail_close;
    private RelativeLayout album_detail_layout;
    private int showPosition;

    private static class MyHandler extends Handler {

        private WeakReference<AlbumActivity> weakReference;

        public MyHandler(AlbumActivity activity) {
            this.weakReference = new WeakReference<AlbumActivity>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            AlbumActivity mactivity = weakReference.get();

            switch (msg.what) {
                case 0: {
                    mactivity.showImg();
                    break;
                }
                case 1: {
                    //没有接受过图片
                    mactivity.showImg();
                    break;
                }
                case 2: {
                    //没有新的图片
                    mactivity.showImg();
                    break;
                }
                case 3: {
                    mactivity.loading_layout.setVisibility(View.VISIBLE);
                    break;
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album);

        backIcon = (ImageView) findViewById(R.id.backIcon);
        edit_btn = (TextView) findViewById(R.id.edit_btn);
        gridView = (GridView) findViewById(R.id.gridview);
        album_guide = (ImageView) findViewById(R.id.album_guide);
        all_select = (CheckBox) findViewById(R.id.all_select);
        spinner = (Spinner) findViewById(R.id.spinner);

        set_screen = (Button) findViewById(R.id.set_screen);
        delete = (Button) findViewById(R.id.delete);

        screen_on = (RadioButton) findViewById(R.id.screen_on);
        screen_off = (RadioButton) findViewById(R.id.screen_off);

        img_count = (TextView) findViewById(R.id.img_count);

        edit_layout = (LinearLayout) findViewById(R.id.edit_layout);

        album_detail_layout = (RelativeLayout) findViewById(R.id.album_detail_layout);
        album_detail = (SimpleDraweeView) findViewById(R.id.album_detail);
        album_detail_pre = (ImageView) findViewById(R.id.album_detail_pre);
        album_detail_next = (ImageView) findViewById(R.id.album_detail_next);
        album_detail_close = (ImageView) findViewById(R.id.album_detail_close);

        loading_layout = (RelativeLayout) findViewById(R.id.loading_layout);

        sp = getSharedPreferences("screen_imgs", MODE_PRIVATE);

        list = new ArrayList<>();

        boolean screen_switch = sp.getBoolean("screen_switch", false);
        if (screen_switch) {
            screen_on.setChecked(true);
        } else {
            screen_off.setChecked(true);
        }

        myHandler = new MyHandler(this);

        initListener();

        makeImgThread = new MakeImgThread(this, "viomi_album");
        makeImgThread.start();

        //处理历史遗留问题
        MakeImgThread historyDeal = new MakeImgThread(this, "viomi_album2");
        historyDeal.start();

        screenBeanList = new ArrayList<>();
        screenBeanList.add(new ScreenSleepBean(15 * 1000, "15秒", false));
        screenBeanList.add(new ScreenSleepBean(30 * 1000, "30秒", false));
        screenBeanList.add(new ScreenSleepBean(1 * 60 * 1000, "1分钟", false));
        screenBeanList.add(new ScreenSleepBean(2 * 60 * 1000, "2分钟", false));
        screenBeanList.add(new ScreenSleepBean(5 * 60 * 1000, "5分钟", false));
        screenBeanList.add(new ScreenSleepBean(10 * 60 * 1000, "10分钟", false));
        screenBeanList.add(new ScreenSleepBean(30 * 60 * 1000, "30分钟", false));
        spinner.setAdapter(new SleepTimeAdapter(screenBeanList, this));

        int screen_time = sp.getInt("screen_time", 1 * 60 * 1000);

        switch (screen_time) {
            case 15 * 1000:
                spinner.setSelection(0);
                break;
            case 30 * 1000:
                spinner.setSelection(1);
                break;
            case 1 * 60 * 1000:
                spinner.setSelection(2);
                break;
            case 2 * 60 * 1000:
                spinner.setSelection(3);
                break;
            case 5 * 60 * 1000:
                spinner.setSelection(4);
                break;
            case 10 * 60 * 1000:
                spinner.setSelection(5);
                break;
            case 30 * 60 * 1000:
                spinner.setSelection(6);
                break;
            default:
                spinner.setSelection(2);
                break;
        }
        registerReceiver();
        FileUtil.writeTxtToFile(mContext, AppConfig.EventKey.EVENT_ENTER_ALBUM, null);
    }


    private void registerReceiver() {
        IntentFilter filter = new IntentFilter("viomi_file_completion");
        fileReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals("viomi_file_completion")) {
                    if (makeImgThread != null) {
                        makeImgThread.interrupt();
                        makeImgThread = new MakeImgThread(AlbumActivity.this, "viomi_album");
                        makeImgThread.start();
                    }
                }
            }
        };

        registerReceiver(fileReceiver, filter);
    }

    private void unregisterReceiver() {
        unregisterReceiver(fileReceiver);
    }

    private void initListener() {
        backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
//                new Thread(new Runnable() {
//                    @Override
//                    public void run() {
//                        Instrumentation inst = new Instrumentation();
//                        inst.sendKeyDownUpSync(KeyEvent.KEYCODE_BACK);
//                    }
//                }).start();
            }
        });

        edit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isedit = !isedit;
                if (isedit) {
                    edit_btn.setText("完成");
                    edit_layout.setVisibility(View.VISIBLE);
                } else {
                    edit_btn.setText("编辑");
                    edit_layout.setVisibility(View.GONE);
                }
                albumAdapter.setIsedit(isedit);
                albumAdapter.notifyDataSetChanged();
            }
        });

        all_select.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {


                if (isChecked) {
                    for (int i = 0; i < list.size(); i++) {
                        list.get(i).setIsselect(true);
                        albumAdapter.notifyDataSetChanged();
                    }
                    log.myE("check", "true");
                } else {
                    //你慢慢看2333
                    if (!isAutoAllCheck) {
                        for (int i = 0; i < list.size(); i++) {
                            list.get(i).setIsselect(false);
                            albumAdapter.notifyDataSetChanged();
                        }
                        log.myE("check", "false");
                    }
                }
                isAutoAllCheck = false;
            }
        });

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (isedit) {
                    boolean selected = list.get(position).isselect();
                    list.get(position).setIsselect(!selected);
                    albumAdapter.notifyDataSetChanged();
                    setAllSelect();
                } else {
                    //.......
                    album_detail_layout.setVisibility(View.VISIBLE);
                    showPosition = position;
                    album_detail.setImageURI(Uri.fromFile(list.get(showPosition).getShowfile()));

                    if (showPosition == 0) {
                        album_detail_pre.setVisibility(View.GONE);
                    } else {
                        album_detail_pre.setVisibility(View.VISIBLE);
                    }

                    if (showPosition == list.size() - 1) {
                        album_detail_next.setVisibility(View.GONE);
                    } else {
                        album_detail_next.setVisibility(View.VISIBLE);
                    }
                }

                log.myE(TAG, "item_click");
            }
        });

        set_screen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Set<String> screen_set = new HashSet<String>();
                for (int i = 0; i < list.size(); i++) {
                    if (list.get(i).isselect()) {
                        screen_set.add(list.get(i).getShowfile().getAbsolutePath());
                    }
                }

                if (screen_set.size() == 0) {
                    ToastUtil.show("请点击编辑，选择图片");
                    return;
                }

                SharedPreferences.Editor editor = sp.edit();
                editor.putStringSet("screen_set", screen_set);
                editor.commit();
                notifyScreen();
                ToastUtil.show("设置成功！");
            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sp.edit();
                editor.putStringSet("screen_set", null);
                editor.commit();
                if (list != null) {
                    Set<String> screen_set = new HashSet<String>();
                    for (int i = 0; i < list.size(); i++) {
                        if (list.get(i).isselect()) {
                            screen_set.add(list.get(i).getShowfile().getAbsolutePath());
                            File thumbfile = list.get(i).getThumbfile();
                            if (thumbfile.exists()) {
                                thumbfile.delete();
                            }
                            File showfile = list.get(i).getShowfile();
                            if (showfile.exists()) {
                                showfile.delete();
                            }
                        }
                    }

                    Iterator<AlbumBean> iterator = list.iterator();
                    while (iterator.hasNext()) {
                        AlbumBean next = iterator.next();
                        if (next.isselect()) {
                            iterator.remove();
                        }
                    }

                    if (screen_set.size() == 0) {
                        ToastUtil.show("请点击编辑，选择图片");
                        return;
                    }

                    if (albumAdapter != null) {
                        albumAdapter.notifyDataSetChanged();
                    }

                    img_count.setText("图片数量：" + list.size() + "张");
                    if (list.size() == 0) {
                        album_guide.setVisibility(View.VISIBLE);
                    }
                }
            }
        });

        screen_on.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    SharedPreferences.Editor edit = sp.edit();
                    edit.putBoolean("screen_switch", true);
                    edit.commit();
                    notifyScreen();
                }
            }
        });
        screen_off.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    SharedPreferences.Editor edit = sp.edit();
                    edit.putBoolean("screen_switch", false);
                    edit.commit();
                    notifyScreen();
                }
            }
        });

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SharedPreferences.Editor editor = sp.edit();
                editor.putInt("screen_time", screenBeanList.get(position).getTime());
                editor.commit();
                notifyScreen();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        album_detail_pre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPosition--;
                if (showPosition >= 0) {
                    album_detail_next.setVisibility(View.VISIBLE);
                    album_detail.setImageURI(Uri.fromFile(list.get(showPosition).getShowfile()));
                    if (showPosition == 0) {
                        album_detail_pre.setVisibility(View.GONE);
                    }
                } else {
                    album_detail_pre.setVisibility(View.GONE);
                }

            }
        });

        album_detail_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPosition++;
                if (showPosition < list.size()) {
                    album_detail_pre.setVisibility(View.VISIBLE);
                    album_detail.setImageURI(Uri.fromFile(list.get(showPosition).getShowfile()));

                    if (showPosition == list.size() - 1) {
                        album_detail_next.setVisibility(View.GONE);
                    }
                } else {
                    album_detail_next.setVisibility(View.GONE);
                }

            }
        });

        album_detail_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                album_detail_layout.setVisibility(View.GONE);
            }
        });

        album_detail_layout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
    }

    private void setAllSelect() {
        if (list != null) {
            boolean isAllSelect = true;
            for (int i = 0; i < list.size(); i++) {
                if (!list.get(i).isselect()) {
                    isAllSelect = false;
                    break;
                }
            }
            isAutoAllCheck = true;
            all_select.setChecked(isAllSelect);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        makeImgThread.interrupt();
        myHandler.removeCallbacksAndMessages(null);
        sendBroadcast(new Intent("UPDATE_ALBUM"));
        FileUtil.writeTxtToFile(mContext, AppConfig.EventKey.EVENT_EXIT_ALBUM, null);
        unregisterReceiver();
    }

    private void showImg() {
        loading_layout.setVisibility(View.GONE);

        isedit = false;
        edit_btn.setText("编辑");
        edit_layout.setVisibility(View.GONE);

        album_detail_layout.setVisibility(View.GONE);

        File thumbDir = new File(Environment.getExternalStorageDirectory(), "viomi_album_thumb");
        File showDir = new File(Environment.getExternalStorageDirectory(), "viomi_album_show");

        list = new ArrayList<>();
        showList = new ArrayList<>();

        if (thumbDir.exists() && showDir.exists()) {
            File[] thumbfiles = thumbDir.listFiles();
            for (int i = 0; i < thumbfiles.length; i++) {

                File showfile = new File(showDir, thumbfiles[i].getName());

                list.add(new AlbumBean(thumbfiles[i], showfile, false));
            }
            Collections.reverse(list);
            albumAdapter = new AlbumAdapter(list, this);
            gridView.setAdapter(albumAdapter);

            if (list.size() == 0) {
                album_guide.setVisibility(View.VISIBLE);
                edit_btn.setVisibility(View.GONE);
            } else {
                album_guide.setVisibility(View.GONE);
                edit_btn.setVisibility(View.VISIBLE);
            }

            img_count.setText("图片数量：" + list.size() + "张");
        } else {
            album_guide.setVisibility(View.VISIBLE);
            edit_btn.setVisibility(View.GONE);
        }
    }


    //子线程图片处理
    private static class MakeImgThread extends Thread {

        private WeakReference<AlbumActivity> weakReference;
        private String fileDir;

        public MakeImgThread(AlbumActivity activity, String fileDir) {
            this.fileDir = fileDir;
            this.weakReference = new WeakReference<AlbumActivity>(activity);
        }

        @Override
        public void run() {
            AlbumActivity activity = weakReference.get();

            File tempDir = new File(Environment.getExternalStorageDirectory(), fileDir);
            File saveDir = new File(Environment.getExternalStorageDirectory(), "viomi_album_show");
            File thumbDir = new File(Environment.getExternalStorageDirectory(), "viomi_album_thumb");

            if (!tempDir.exists()) {
                //没有接受过图片,文件夹不存在
                activity.myHandler.sendEmptyMessage(1);
                return;
            }

            File[] files = null;
            if (tempDir.isDirectory()) {
                files = tempDir.listFiles(new FilenameFilter() {
                    @Override
                    public boolean accept(File dir, String name) {
                        if (name.endsWith(".jpg") || name.endsWith(".png") || name.endsWith(".jpeg")) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                });
            }

            if (files == null || files.length == 0) {
                //没有新的图片
                activity.myHandler.sendEmptyMessage(2);
                log.myE("AlbumActivity", "-------------------------没有新的图片-------------------------------------");
                return;
            }

            if (!saveDir.exists()) {
                saveDir.mkdir();
            }

            if (!thumbDir.exists()) {
                thumbDir.mkdir();
            }

            activity.myHandler.sendEmptyMessage(3);

            for (int i = 0; i < files.length; i++) {
                Bitmap bitmap = getBitmap(files[i].getAbsolutePath(), 1280, 800);
                String file_name = files[i].getName();
                File newFile = new File(saveDir, file_name);
                saveNewBitmap(files[i], newFile, bitmap);
                bitmap.recycle();

                Bitmap bitmap_thumb = getBitmap(files[i].getAbsolutePath(), 400, 400);
                File newThumbFile = new File(thumbDir, file_name);
                saveNewBitmap2(files[i], newThumbFile, bitmap_thumb);
                bitmap_thumb.recycle();

                if (interrupted()) {
                    return;
                }
            }
            activity.myHandler.sendEmptyMessage(0);
        }


        private Bitmap getBitmap(String filePath, int destWidth, int destHeight) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(filePath, options);
            int outWidth = options.outWidth;
            int outHeight = options.outHeight;
            int sampleSize = 1;
            while ((outWidth / sampleSize > destWidth) || (outHeight / sampleSize > destHeight)) {
                sampleSize *= 2;
            }

            if (outWidth < 1.5 * destWidth && outHeight < 1.5 * destHeight) {
                sampleSize = 1;
            }

            options.inJustDecodeBounds = false;
            options.inSampleSize = sampleSize;
            options.inPreferredConfig = Bitmap.Config.RGB_565;
            return BitmapFactory.decodeFile(filePath, options);
        }

        private void saveNewBitmap(File file, File newFile, Bitmap bmp) {
            try {
                FileOutputStream fos = new FileOutputStream(newFile);
                bmp.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                fos.flush();
                fos.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        private void saveNewBitmap2(File file, File newFile, Bitmap bmp) {
            try {
                file.delete();
                FileOutputStream fos = new FileOutputStream(newFile);
                bmp.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                fos.flush();
                fos.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
