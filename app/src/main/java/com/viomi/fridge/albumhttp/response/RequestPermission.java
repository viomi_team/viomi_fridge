package com.viomi.fridge.albumhttp.response;

import com.viomi.fridge.util.log;
import com.yanzhenjie.andserver.RequestHandler;

import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.entity.StringEntity;
import org.apache.http.protocol.HttpContext;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by viomi on 2017/3/30.
 */

public class RequestPermission implements RequestHandler {
    @Override
    public void handle(HttpRequest request, HttpResponse response, HttpContext context) throws HttpException, IOException {
        JSONObject json = new JSONObject();
        try {
            json.put("code", "200");
            json.put("desc", "服务端确认客户端连接请求成功");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        response.setEntity(new StringEntity(json.toString(),"utf-8"));
        log.myE("RequestPermission","----------------------------------------------------------------------");
    }

}
