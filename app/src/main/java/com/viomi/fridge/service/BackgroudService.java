package com.viomi.fridge.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.PixelFormat;
import android.graphics.drawable.AnimationDrawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tencent.smtt.sdk.QbSdk;
import com.tencent.smtt.sdk.TbsListener;
import com.viomi.common.callback.AppCallback;
import com.viomi.fridge.R;
import com.viomi.fridge.api.dao.DeviceInfoMessage;
import com.viomi.fridge.api.dao.InfoManager;
import com.viomi.fridge.api.http.HttpConnect;
import com.viomi.fridge.broadcast.BroadcastAction;
import com.viomi.fridge.common.GlobalParams;
import com.viomi.fridge.common.ViomiApplication;
import com.viomi.fridge.common.rxbus.BusEvent;
import com.viomi.fridge.common.rxbus.RxBus;
import com.viomi.fridge.device.AppConfig;
import com.viomi.fridge.device.AppConstants;
import com.viomi.fridge.device.DeviceConfig;
import com.viomi.fridge.device.DeviceManager;
import com.viomi.fridge.manager.AccountManager;
import com.viomi.fridge.manager.ControlManager;
import com.viomi.fridge.manager.PushManager;
import com.viomi.fridge.manager.SerialManager;
import com.viomi.fridge.manager.UpgradeManager;
import com.viomi.fridge.manager.VoiceManager;
import com.viomi.fridge.model.bean.AppUpgradeMsg;
import com.viomi.fridge.model.bean.PushMsg;
import com.viomi.fridge.model.bean.TalkInfo;
import com.viomi.fridge.model.bean.ViomiUser;
import com.viomi.fridge.preference.ManagePreference;
import com.viomi.fridge.util.FileUtil;
import com.viomi.fridge.util.LogUtils;
import com.viomi.fridge.util.OkHttpUtils;
import com.viomi.fridge.util.PhoneUtil;
import com.viomi.fridge.util.RxSchedulerUtil;
import com.viomi.fridge.util.ToolUtil;
import com.viomi.fridge.util.UMUtil;
import com.viomi.fridge.util.ZhugeIoUtil;
import com.viomi.fridge.util.log;
import com.viomi.fridge.view.activity.FoodManageActivity;
import com.viomi.fridge.view.activity.FridgeActivity;
import com.viomi.fridge.view.adapter.TalkAdapter;

import org.android.agoo.common.CallBack;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public class BackgroudService extends Service {
    private final static String TAG = BackgroudService.class.getSimpleName();
    SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat("HHmm");
    private int timeCount = 0;
    private Clockhread mClockThread;
    private WindowManager mWindowManager;
    private RelativeLayout mTalkDialog;//语音对话文案dialog
    private LinearLayout mVoiceTips;//唤醒文案dialog
    private LinearLayout mAlertDialog;//开门报警dialog
    private RelativeLayout mMessageDialog;//消息dialog
    private boolean isTalkDialogShow;
    private boolean isVoiceTipsShow;
    private boolean isAlertDialogShow;
    private boolean isMessageDialogShow;
    private ImageView mAanimateLeftView;
    private ImageView mAanimateRightView;
    private ListView mTalkListView;
    private ArrayList<TalkInfo> mTalkList;
    private TalkAdapter mTalkAdapter;
    private TextView mTimeCountTextView;
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");

    private TextView mOpenDoorTimeView;
    private TextView mMessageTitleTextView;
    private TextView mMessageTextView;
    private TextView mMessageTimeTextView;

    private MediaPlayer mMediaPlayer;
    private MediaPlayer mMediaPlayer1;
    private AudioManager audioManager;
    private BraodcastReceiver mBraodcastReceiver;
    private Timer mTimer, mTimer2;
    private TimerTask mTimeTask, mTimeTask2;
    private long mOpenDoorTime = 60;
    public final static int WHAT_OPEN_DOOR = 0;
    public final static int WHAT_TALK_DIALOG_ADD = 1;
    public final static int WHAT_TALK_DIALOG_UPDATE = 2;
    public final static int WHAT_TALK_DIALOG_REMOVE = 3;
    public final static int WHAT_ABOUT_TO_CLOSE = 4;
    public final static int WHAT_TALK_DIALOG_LISTENING = 5;
    public final static int WHAT_TALK_DIALOG_TALKING = 6;
    public final static int WHAT_CLOSE_INFO_MESSAGE = 7;
    private PushMsg mPushMsg;
    private int mHeartBeatCount = 1;// 心跳计时
    private CompositeSubscription mCompositeSubscription;// 统一管理订阅

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mCompositeSubscription = new CompositeSubscription();
        x5KernelDownload();// X5 内核下载
        initSubscription();// RxBus 初始化
        initTimer();//计时任务
        heartbeat();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        init();
        return START_REDELIVER_INTENT;
    }

    private void init() {
        register();
        VoiceManager.getInstance().init(this, mHandler);
        SerialManager.getInstance().open(DeviceConfig.MODEL);
        initData();
        initDevice();
        initView();
        openUploadTimer();
        if (mClockThread == null) {
            mClockThread = new Clockhread();
            mClockThread.start();
        }

    }

    /**
     * X5 内核下载
     */
    private void x5KernelDownload() {
        if (!ManagePreference.getInstance().getX5KernelState())
            QbSdk.reset(ViomiApplication.getContext());
        // 搜集本地 tbs 内核信息并上报服务器，服务器返回结果决定使用哪个内核。
        QbSdk.PreInitCallback cb = new QbSdk.PreInitCallback() {

            @Override
            public void onViewInitFinished(boolean arg0) {
                // x5 內核初始化完成的回调，为 true 表示 x5 内核加载成功，否则表示 x5 内核加载失败，会自动切换到系统内核。
                LogUtils.d(TAG, " onViewInitFinished is " + arg0);
                ManagePreference.getInstance().saveX5KernelState(arg0);
            }

            @Override
            public void onCoreInitFinished() {
                LogUtils.d(TAG, " onCoreInitFinished is ");
            }
        };
        //x5 内核初始化接口
        QbSdk.initX5Environment(getApplicationContext(), cb);
        QbSdk.setTbsListener(new TbsListener() {
            @Override
            public void onDownloadFinish(int i) {
                LogUtils.d(TAG, " onDownloadFinish");
            }

            @Override
            public void onInstallFinish(int i) {
                LogUtils.d(TAG, " onInstallFinish");
            }

            @Override
            public void onDownloadProgress(int i) {
                LogUtils.d(TAG, " onDownloadProgress：" + i);
            }
        });
    }

    /**
     * 每分钟计时器
     */
    private void initTimer() {
        Subscription timerSubscription = Observable.interval(0, 180, TimeUnit.SECONDS)
                .onBackpressureDrop() // 背压处理
                .compose(RxSchedulerUtil.SchedulersTransformer5())
                .onTerminateDetach()
                .subscribe(aLong -> {
                    LogUtils.d(TAG, "MSG_TIME_MINUTE+++");
                    RxBus.getInstance().post(BusEvent.MSG_TIME_MINUTE);
                }, throwable -> LogUtils.e(TAG, "MSG_TIME_MINUTE error! " + throwable.getMessage()));
        mCompositeSubscription.add(timerSubscription);
    }

    /**
     * RxBus 消息管理
     */
    private void initSubscription() {
        Subscription subscription = RxBus.getInstance()
                .subscribe(event -> {
                    Intent intent;
                    LogUtils.d(TAG, "event.getMsgId:" + event.getMsgId());
                    switch (event.getMsgId()) {
                        case BusEvent.MSG_TIME_MINUTE: // 每分钟监听
                            // 每天整点自动检查 App 更新
                            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HHmm", Locale.getDefault());
                            String timeStr = simpleDateFormat.format(new Date());
                            int hour = Integer.parseInt(timeStr);
                            if (hour == 0) {
//                                if (!FridgePreference.getInstance().getModel().equals(AppConstants.MODEL_JD))
//                                    checkUpdate(true);
                                x5KernelDownload();
                            }
                            break;
                    }
                });
        mCompositeSubscription.add(subscription);
    }

    /**
     * 心跳
     */
    private void heartbeat() {
        Subscription heartbeatSubscription = Observable.interval(0, 10, TimeUnit.SECONDS)
                .onBackpressureDrop() // 背压处理
                .compose(RxSchedulerUtil.SchedulersTransformer5())
                .onTerminateDetach()
                .subscribe(aLong -> {
                    mHeartBeatCount++;
                    LogUtils.d(TAG, "heartbeat=" + mHeartBeatCount);
                    Intent intent = new Intent();
                    intent.setAction("com.unilife.fridge.action.dameon.heartbeat");
                    intent.putExtra("package", "com.viomi.fridge");
                    intent.putExtra("beats", mHeartBeatCount);
                    sendBroadcast(intent);
                }, throwable -> LogUtils.e(TAG, "heartbeat error! " + throwable.getMessage()));
        mCompositeSubscription.add(heartbeatSubscription);
    }

    /**
     * 检查 App 更新
     */
//    private void checkUpdate(boolean isAuto) {
//        String packageName = ManagePreference.getInstance().getDebug() ? getPackageName() + ".debug" : getPackageName();
//        String model = FridgePreference.getInstance().getModel();// 读取冰箱 Model
//        String channel = AppConstants.CHANNEL_VIOMI;
//        if (AppConstants.MODEL_JD.equals(model)) {
//            channel = AppConstants.CHANNEL_JINGDONG;
//        }
//        Subscription subscription = ApiClient.getInstance().getApiService().checkAppUpdate(packageName, channel)
//                .compose(RxSchedulerUtil.SchedulersTransformer1())
//                .onTerminateDetach()
//                .subscribe(appUpdateResult -> {
//                    if (appUpdateResult.getData().get(0).getCode() > ToolUtil.getVersionCode(this)) {
//                        ManagePreference.getInstance().saveAppUpdate(true);
//                        if (isAuto) downLoadApk(appUpdateResult.getData().get(0).getUrl());
//                    } else ManagePreference.getInstance().saveAppUpdate(false);
//                }, throwable -> logUtil.e(TAG, throwable.getMessage()));
//        mCompositeSubscription.add(subscription);
//    }

    public static class BraodcastReceiver extends BroadcastReceiver {
        private BackgroudService mService;

        public BraodcastReceiver() {
        }

        public BraodcastReceiver(BackgroudService service) {
            mService = service;
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            log.d(TAG, "onReceive,action=" + intent.getAction());
            switch (intent.getAction()) {
                case BroadcastAction.ACTION_START_APP_UPGRADE:
                    mService.startAppUpgrade();
                    break;

                case BroadcastAction.ACTION_VOICE_VISIBLE:
                    mService.setTalkDialogVisiable(true);
                    mService.setVoiceTipsVisiable(false);

                    TalkInfo talkInfoAdd = new TalkInfo();
                    talkInfoAdd.talkType = TalkInfo.TYPE_TALK;
                    talkInfoAdd.msg = "您好";
                    mService.mTalkList.clear();
                    mService.mTalkList.add(talkInfoAdd);
                    mService.mTalkAdapter.notifyDataSetChanged();
                    mService.mTimeCountTextView.setVisibility(View.GONE);
                    break;

                case BroadcastAction.ACTION_VOICE_GONE:
                    mService.setTalkDialogVisiable(false);
                    break;

                case BroadcastAction.ACTION_VOICE_INTRODUCE_VISIBLE:
                    mService.setTalkDialogVisiable(false);
                    mService.setVoiceTipsVisiable(true);
                    break;

                case BroadcastAction.ACTION_VOICE_INTRODUCE_GONE:
                    mService.setVoiceTipsVisiable(false);
                    break;

                case BroadcastAction.ACTION_ROOM_OPEN_HAPPEN:
                    Log.d(TAG, "room open happen");
                    if (!mService.isAlertDialogShow) {
                        mService.musicPlay();
                        mService.openDoorTimer();
                    }
                    mService.setVoiceTipsVisiable(false);
                    mService.setAlertDialogVisiable(true);
                    mService.setTalkDialogVisiable(false);
                    break;
                case BroadcastAction.ACTION_UPLOAD_EVENT:
//                    mService.openUploadTimer();
                    break;
                case BroadcastAction.ACTION_ROOM_OPEN_REMOVE:
                    mService.setAlertDialogVisiable(false);
                    mService.stopTimer();
                    break;

                case BroadcastAction.ACTION_FAULT_HAPPEN:
                    mService.mMessageDialog.setVisibility(View.VISIBLE);
                    break;

                case BroadcastAction.ACTION_FAULT_REMOVE:
                    mService.mMessageDialog.setVisibility(View.GONE);
                    break;

                case BroadcastAction.ACTION_PUSH_MESSAGE:
                    PushMsg pushMsg = intent.getParcelableExtra(BroadcastAction.EXTRA_PUSH_MSG);
                    mService.mPushMsg = pushMsg;

                    if (pushMsg != null && pushMsg.type != null) {
                        if (PushManager.getInstance().isInAcceptTime()) {

                            switch (pushMsg.type) {
                                case PushMsg.PUSH_MESSAGE_TYPE_ABNORMAL:
                                case PushMsg.PUSH_MESSAGE_TYPE_FILTER:
                                case PushMsg.PUSH_MESSAGE_TYPE_FOOD:
                                    if (PushManager.getInstance().isDeviceNotifyEnable()) {
                                        mService.setiMessageDialogShowVisiable(true, pushMsg);
                                    }
                                    break;
                                case PushMsg.PUSH_MESSAGE_TYPE_ADVERT:
                                    if (PushManager.getInstance().isAdvertNotifyEnable()) {
                                        mService.setiMessageDialogShowVisiable(true, pushMsg);
                                    }
                                    break;
                                default:
                                    mService.setiMessageDialogShowVisiable(true, pushMsg);
                                    break;
                            }
                        }
                    } else {
                        mService.setiMessageDialogShowVisiable(true, pushMsg);
                    }

                    if (PushMsg.PUSH_MESSAGE_TYPE_UPDATE.equals(pushMsg.type)) {
                        if (pushMsg.link == null || (!pushMsg.link.startsWith("http"))) {
                            Log.e(TAG, "update pushMsg link error!pushMsg.link=" + pushMsg.link);
                            return;
                        }
                        AppUpgradeMsg appUpgradeMsg = new AppUpgradeMsg();
                        appUpgradeMsg.url = pushMsg.link;
                        UpgradeManager.getInstance().init(mService);
                        UpgradeManager.getInstance().upgradeApp(appUpgradeMsg, UpgradeManager.OPERATE_AUTO);
                    }
                    break;
            }

        }
    }

    private void openDoorTimer() {
        if (mTimer != null) {
            mTimer.cancel();
            mTimer = null;
        }
        if (mTimeTask != null) {
            mTimeTask.cancel();
            mTimeTask = null;
        }
        mOpenDoorTime = 60;
        mTimer = new Timer();
        mTimeTask = new TimerTask() {
            @Override
            public void run() {
                mOpenDoorTime++;
                if (mHandler != null) {
                    mHandler.sendEmptyMessage(WHAT_OPEN_DOOR);
                }
            }
        };
        mTimer.schedule(mTimeTask, 1000, 1000);
    }

    private void openUploadTimer() {
        if (mTimer2 != null) {
            mTimer2.cancel();
            mTimer2 = null;
        }
        if (mTimeTask2 != null) {
            mTimeTask2.cancel();
            mTimeTask2 = null;
        }
        mTimer2 = new Timer();
        mTimeTask2 = new TimerTask() {
            @Override
            public void run() {
                String Path = GlobalParams.LOG_FILE_PATH;
                File file = new File(Path);
                if (file.exists() && file.length() > 5 * 1048576) {//文件存在，大小大于5M进行上传
                    OkHttpUtils.getInstance().uploadFile(HttpConnect.UPLOAD_EVENT_FILE, file, new CallBack() {
                        @Override
                        public void onSuccess() {
                            FileUtil.deleteChildFile(file);
                        }

                        @Override
                        public void onFailure(String tips, String exception) {
                            Log.e(TAG,"uploadFile fail!msg="+exception);
                        }
                    });
                }
//                if (file.exists()) {//文件存在，大小大于5M进行上传
//                    OkHttpUtils.getInstance().uploadFile(HttpConnect.UPLOAD_EVENT_FILE, file, new CallBack() {
//                        @Override
//                        public void onSuccess() {
//                            FileUtil.deleteChildFile(file);
//                        }
//
//                        @Override
//                        public void onFailure(String tips, String exception) {
//
//                        }
//                    });
//                }
            }
        };
//        1000 * 60 * 30
        mTimer2.schedule(mTimeTask2, 1000, 1000 * 60 * 30);//每30分钟执行一次
    }

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case WHAT_OPEN_DOOR:
                    int hour = 0, minnut, second;
                    hour = (int) (mOpenDoorTime / 3600);
                    minnut = (int) (mOpenDoorTime % 3600 / 60);
                    second = (int) (mOpenDoorTime % 3600 % 60);
                    String hourStr, minnutStr, secondStr;

                    if (hour < 10) {
                        hourStr = "0" + hour;
                    } else {
                        hourStr = "" + hour;
                    }
                    if (minnut < 10) {
                        minnutStr = "0" + minnut;
                    } else {
                        minnutStr = "" + minnut;
                    }
                    if (second < 10) {
                        secondStr = "0" + second;
                    } else {
                        secondStr = "" + second;
                    }
                    mOpenDoorTimeView.setText(hourStr + ":" + minnutStr + ":" + secondStr);
                    break;

                case WHAT_TALK_DIALOG_ADD:
                    setTalkDialogVisiable(true);
                    setVoiceTipsVisiable(false);
                    TalkInfo talkInfoAdd = new TalkInfo();
                    talkInfoAdd.talkType = msg.arg1;
                    talkInfoAdd.msg = (String) msg.obj;
                    mTalkList.clear();
                    mTalkList.add(talkInfoAdd);
                    mTalkAdapter.notifyDataSetChanged();
                    mTimeCountTextView.setVisibility(View.GONE);
                    break;

                case WHAT_TALK_DIALOG_UPDATE:
                    log.d(TAG, "WHAT_TALK_DIALOG_UPDATE," + msg.arg1 + "," + msg.obj);
                    if ((!isTalkDialogShow) && getResources().getString(R.string.text_wakeup_ask_repeat).equals(msg.obj)) {
                        setTalkDialogVisiable(true);
                        mTalkList.clear();
                    }

                    TalkInfo talkInfo = new TalkInfo();
                    talkInfo.talkType = msg.arg1;
                    talkInfo.msg = (String) msg.obj;
                    mTalkList.add(talkInfo);
                    mTalkAdapter.notifyDataSetChanged();
                    mTimeCountTextView.setVisibility(View.GONE);
                    break;

                case WHAT_TALK_DIALOG_REMOVE:
                    setTalkDialogVisiable(false);
                    break;

                case WHAT_ABOUT_TO_CLOSE:
                    int time = (int) msg.obj;
                    if (time == 0) {
                        mTimeCountTextView.setVisibility(View.GONE);
                        setTalkDialogVisiable(false);
                    } else {
                        mTimeCountTextView.setText("休眠倒计时（S）：" + time);
                        mTimeCountTextView.setVisibility(View.VISIBLE);
                    }
                    break;

                case WHAT_TALK_DIALOG_LISTENING:
                    if (mAanimateLeftView != null) {
                        mAanimateLeftView.setVisibility(View.VISIBLE);
                    }
                    if (mAanimateRightView != null) {
                        mAanimateRightView.setVisibility(View.VISIBLE);
                    }

                    break;

                case WHAT_TALK_DIALOG_TALKING:
                    if (mAanimateLeftView != null) {
                        mAanimateLeftView.setVisibility(View.GONE);
                    }
                    if (mAanimateRightView != null) {
                        mAanimateRightView.setVisibility(View.GONE);
                    }
                    break;

                case WHAT_CLOSE_INFO_MESSAGE:
                    setiMessageDialogShowVisiable(false, null);
                    break;
            }
        }
    };

    private void stopTimer() {
        if (mTimer != null) {
            mTimer.cancel();
            mTimer = null;
        }
        if (mTimeTask != null) {
            mTimeTask.cancel();
            mTimeTask = null;
        }
    }

    private void register() {
        mBraodcastReceiver = new BraodcastReceiver(this);
        IntentFilter intentFilter = new IntentFilter();
        //        intentFilter.addAction(Intent.ACTION_TIME_TICK);
        intentFilter.addAction(BroadcastAction.ACTION_START_APP_UPGRADE);
        intentFilter.addAction(BroadcastAction.ACTION_VOICE_VISIBLE);
        intentFilter.addAction(BroadcastAction.ACTION_ROOM_OPEN_HAPPEN);
        intentFilter.addAction(BroadcastAction.ACTION_ROOM_OPEN_REMOVE);
        intentFilter.addAction(BroadcastAction.ACTION_VOICE_INTRODUCE_VISIBLE);
        intentFilter.addAction(BroadcastAction.ACTION_VOICE_INTRODUCE_GONE);
        intentFilter.addAction(BroadcastAction.ACTION_VOICE_GONE);
        intentFilter.addAction(BroadcastAction.ACTION_FAULT_HAPPEN);
        intentFilter.addAction(BroadcastAction.ACTION_FAULT_REMOVE);
        intentFilter.addAction(BroadcastAction.ACTION_PUSH_MESSAGE);
        intentFilter.addAction(BroadcastAction.ACTION_UPLOAD_EVENT);
        LocalBroadcastManager.getInstance(ViomiApplication.getContext()).registerReceiver(mBraodcastReceiver, intentFilter);
    }

    private void unRegister() {
        LocalBroadcastManager.getInstance(ViomiApplication.getContext()).unregisterReceiver(mBraodcastReceiver);
    }

    private void initView() {

        mMediaPlayer = MediaPlayer.create(this, R.raw.alert);
        mMediaPlayer1 = MediaPlayer.create(this, R.raw.bubbles);
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        mWindowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        initTalkDialog(inflater);//语音对话窗口
        initVoiceTips(inflater);//语音提示窗口
        initAlertDialog(inflater);//门口报警窗口
        initMessageDialog(inflater);//推送窗口
    }

    private void initTalkDialog(LayoutInflater inflater) {
        mTalkDialog = (RelativeLayout) inflater.inflate(R.layout.dialog_talk, null);
        mTalkDialog.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        mTalkListView = (ListView) mTalkDialog.findViewById(R.id.talkListView);
        mTalkList = new ArrayList<>();

        mTalkAdapter = new TalkAdapter(mTalkList, this);
        mTalkListView.setAdapter(mTalkAdapter);
        ImageView imageview_colse = (ImageView) mTalkDialog.findViewById(R.id.imageview_colse);
        imageview_colse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setTalkDialogVisiable(false);
            }
        });
        mTimeCountTextView = (TextView) mTalkDialog.findViewById(R.id.time_count_view);
        ImageView imageview_microphone = (ImageView) mTalkDialog.findViewById(R.id.imageview_microphone);
        imageview_microphone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        mAanimateLeftView = (ImageView) mTalkDialog.findViewById(R.id.talk_animate_left);
        mAanimateRightView = (ImageView) mTalkDialog.findViewById(R.id.talk_animate_right);
        mAanimateLeftView.setVisibility(View.VISIBLE);
        mAanimateRightView.setVisibility(View.VISIBLE);
        AnimationDrawable mAnimationDrawableLeft = (AnimationDrawable) mAanimateLeftView.getDrawable();
        mAnimationDrawableLeft.start();
        AnimationDrawable mAnimationDrawableRight = (AnimationDrawable) mAanimateRightView.getDrawable();
        mAnimationDrawableRight.start();
    }


    private void setTalkDialogVisiable(boolean visiable) {
        log.d(TAG, "visiable=" + visiable + ",isTalkDialogShow=" + isTalkDialogShow);
        if (visiable && (!isTalkDialogShow)) {
            mWindowManager.addView(mTalkDialog, getStatusBarLayoutParams(Gravity.CENTER,
                    PhoneUtil.dipToPx(this, 840), PhoneUtil.dipToPx(this, 600)));
            isTalkDialogShow = true;
        } else if ((!visiable) && isTalkDialogShow) {
            mWindowManager.removeViewImmediate(mTalkDialog);
            isTalkDialogShow = false;
        }
    }

    private void initVoiceTips(LayoutInflater inflater) {
        mVoiceTips = (LinearLayout) inflater.inflate(R.layout.dialog_vioce_tips, null);
        mVoiceTips.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        TextView tip_view = (TextView) mVoiceTips.findViewById(R.id.tip_view);
        String tips = "●今天天气怎样？\n" +
                "●定时器设为煲汤\n" +
                "●定时30分钟\n" +
                "●番茄炒蛋怎么做？\n" +
                "●我想听笑话\n" +
                "●我想看西游记\n" +
                "●我想听财经新闻\n" +
                "●打开电子相册\n" +
                "●肯德基\n" +
                "●智能互联（步骤：1、选择第几台；2、把水温调为60度）\n" +
                "●苹果英文怎么说\n" +
                "●打开猜数字游戏\n" +
                "●中秋节放假多少天\n" +
                "●帮我查一下养生知识\n" +
                "●售后电话\n";
        if (DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V2)) {
            tips += "●开启假日模式/智能模式\n" +
                    "●将冷藏室设为5℃\n" +
                    "●把冷冻室设为零下18℃\n" +
                    "●打开/关闭冷藏室\n" +
                    "●把变温室设为水果区";
        } else if (DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V1)) {
            tips += "●开启假日模式/智能模式\n" +
                    "●将冷藏室设为5℃\n" +
                    "●把冷冻室设为零下18℃\n" +
                    "●把变温室设为水果区\n" +
                    "●打开/关闭冷藏室\n" +
                    "●开启一键净化";
        } else if (DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V3) || DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V31)
                || DeviceConfig.MODEL.equals(AppConfig.VIOMI_FRIDGE_V4)) {
            tips += "●开启假日模式/智能模式\n" +
                    "●将冷藏室设为5℃\n" +
                    "●把冷冻室设为零下18℃\n" +
                    "●打开/关闭冷藏室";
        }
        tip_view.setText(tips);
        TextView buttonIKnowView = (TextView) mVoiceTips.findViewById(R.id.button_i_known);
        TextView buttonNerverTip = (TextView) mVoiceTips.findViewById(R.id.button_never_tip);
        buttonIKnowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setVoiceTipsVisiable(false);
            }
        });
        buttonNerverTip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setVoiceTipsVisiable(false);
                GlobalParams.getInstance().setVoiceNerverTips(true);
            }
        });
    }

    private void setVoiceTipsVisiable(boolean visiable) {
        if (visiable && (!isVoiceTipsShow)) {
            mWindowManager.addView(mVoiceTips, getStatusBarLayoutParams(Gravity.CENTER,
                    PhoneUtil.dipToPx(this, 800), PhoneUtil.dipToPx(this, 600)));
            isVoiceTipsShow = true;
        } else if ((!visiable) && isVoiceTipsShow) {
            mWindowManager.removeViewImmediate(mVoiceTips);
            isVoiceTipsShow = false;
        }
    }

    private void initAlertDialog(LayoutInflater inflater) {
        mAlertDialog = (LinearLayout) inflater.inflate(R.layout.dialog_alert, null);
        mAlertDialog.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        mOpenDoorTimeView = (TextView) mAlertDialog.findViewById(R.id.door_open_time);
        TextView alertButtonIKnowView = (TextView) mAlertDialog.findViewById(R.id.button_i_known);
        alertButtonIKnowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setAlertDialogVisiable(false);
            }
        });
    }


    private void setAlertDialogVisiable(boolean visiable) {
        if (visiable && (!isAlertDialogShow)) {
            mWindowManager.addView(mAlertDialog, getStatusBarLayoutParams(Gravity.CENTER,
                    PhoneUtil.dipToPx(this, 600), PhoneUtil.dipToPx(this, 400)));
            isAlertDialogShow = true;
        } else if ((!visiable) && isAlertDialogShow) {
            mWindowManager.removeViewImmediate(mAlertDialog);
            isAlertDialogShow = false;
        }
    }

    private void initMessageDialog(LayoutInflater inflater) {
        mMessageDialog = (RelativeLayout) inflater.inflate(R.layout.dialog_message_push, null);
        mMessageDialog.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        mMessageTitleTextView = (TextView) mMessageDialog.findViewById(R.id.title_text);
        mMessageTextView = (TextView) mMessageDialog.findViewById(R.id.message_text);
        mMessageTimeTextView = (TextView) mMessageDialog.findViewById(R.id.happen_time);
        RelativeLayout push_title = (RelativeLayout) mMessageDialog.findViewById(R.id.push_title);
        push_title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setiMessageDialogShowVisiable(false, null);
                informClickProcess(mPushMsg);
            }
        });
    }


    private void setiMessageDialogShowVisiable(boolean visiable, PushMsg pushMsg) {
        if (visiable && (!isMessageDialogShow)) {
            mWindowManager.addView(mMessageDialog, getStatusBarLayoutParams(Gravity.TOP,
                    ViewGroup.LayoutParams.MATCH_PARENT, PhoneUtil.dipToPx(this, 100)));
            isMessageDialogShow = true;
            if (pushMsg != null) {
                mMessageTitleTextView.setText(pushMsg.title);
                mMessageTextView.setText(pushMsg.content);
                mMessageTimeTextView.setText(simpleDateFormat.format(new Date()));
            }
            if (mHandler != null) {
                mHandler.removeMessages(WHAT_CLOSE_INFO_MESSAGE);
                mHandler.sendEmptyMessageDelayed(WHAT_CLOSE_INFO_MESSAGE, 10000);
            }
            informMessageArrivePlay();
        } else if ((!visiable) && isMessageDialogShow) {
            mWindowManager.removeViewImmediate(mMessageDialog);
            isMessageDialogShow = false;

        }
    }

    private WindowManager.LayoutParams getStatusBarLayoutParams(int gravity, int width, int height) {
        WindowManager.LayoutParams wlp = new WindowManager.LayoutParams();

        wlp.format = PixelFormat.TRANSPARENT;

        wlp.flags = WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM;

        wlp.x = 0;
        wlp.y = 0;

        wlp.height = height;
        wlp.width = width;
        wlp.type = WindowManager.LayoutParams.TYPE_PHONE;
        wlp.gravity = gravity;
        return wlp;

    }

    /***
     * 初始化数据，如启动时间等
     */
    private void initData() {
        GlobalParams.getInstance().setAppStartTime(System.currentTimeMillis() / 1000);
        ControlManager.getInstance().init();
        //如果商检模式下重启，设置为智能模式
        if (GlobalParams.getInstance().isCommodityInspection()) {
            SerialManager.getInstance().enableSmartMode(true);
            GlobalParams.getInstance().setCommodityInspection(false);
        }

        if (GlobalParams.getInstance().getAppStartTime() == GlobalParams.Value_App_Start_Time) {
            GlobalParams.getInstance().setAppStartTime(System.currentTimeMillis() / 1000);
        }
        if (GlobalParams.getInstance().getViomiLoginTime() == GlobalParams.Vlaue_Viomi_Login_Time) {
            GlobalParams.getInstance().setViomiLoginTime(System.currentTimeMillis() / 1000);
        }

    }

    /***
     * 设备初始化
     */
    private void initDevice() {
        ViomiUser viomiUser = AccountManager.getViomiUser(ViomiApplication.getContext());
        String miId = null;
        if (viomiUser != null) {
            miId = viomiUser.getMiId();
        }
        log.d(TAG, "miId=" + miId);
        DeviceManager.getInstance().initDevice(ViomiApplication.getContext(), miId, new AppCallback<String>() {
            @Override
            public void onSuccess(String data) {
                Log.e(TAG, "initDevice success");
            }

            @Override
            public void onFail(int errorCode, String msg) {
                Log.e(TAG, "initDevice fail,err=" + errorCode + ",msg=" + msg);
            }
        });
        ZhugeIoUtil.setUserInfo(viomiUser);
    }

    /***
     * app升级启动
     */
    private void startAppUpgrade() {
        Log.i(TAG, "startAppUpgrade");
        UpgradeManager.getInstance().checkAppUpgrade(this, UpgradeManager.OPERATE_AUTO);
    }

    /***
     * 开门报警
     */
    public void musicPlay() {
//     mMediaPlayer.reset();
//        Uri alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
//        try{
//            mMediaPlayer.setDataSource(this,alert);
//        }catch(Exception e){
//            e.printStackTrace();
//        }
        //final AudioManager  audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        if (audioManager.getStreamVolume(AudioManager.STREAM_MUSIC) != 0) {
            mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            try {
                mMediaPlayer.prepare();
            } catch (Exception e) {
                e.printStackTrace();
            }
            mMediaPlayer.start();

        }

    }

    /***
     * 消息到达报警
     */
    public void informMessageArrivePlay() {
        if (audioManager.getStreamVolume(AudioManager.STREAM_MUSIC) != 0) {
            mMediaPlayer1.setAudioStreamType(AudioManager.STREAM_MUSIC);
            try {
                mMediaPlayer1.prepare();
            } catch (Exception e) {
                e.printStackTrace();
            }
            mMediaPlayer1.start();

        }

    }

    /***
     * 消息通知点击处理
     * @param pushMsg
     */
    private void informClickProcess(PushMsg pushMsg) {
        if (pushMsg == null || pushMsg.type == null) {
            return;
        }
        switch (pushMsg.type) {
            case PushMsg.PUSH_MESSAGE_TYPE_ABNORMAL:
                startActivity(BackgroudService.this, FridgeActivity.class);
                break;

            case PushMsg.PUSH_MESSAGE_TYPE_FILTER:
                startActivity(BackgroudService.this, FridgeActivity.class);
                break;

            case PushMsg.PUSH_MESSAGE_TYPE_ORDER:
            case PushMsg.PUSH_MESSAGE_TYPE_COMMENT:
                if (pushMsg.link != null && pushMsg.link.startsWith("http")) {
                    UMUtil.onWebPageJump(BackgroudService.this, ViomiApplication.getContext().getString(R.string.app_name), pushMsg.link, false);
                }
                break;

            case PushMsg.PUSH_MESSAGE_TYPE_ADVERT:
                if (pushMsg.link != null && pushMsg.link.startsWith("http")) {
                    UMUtil.onCommonWebPageJump(BackgroudService.this, pushMsg.link);
                }
                break;

            case PushMsg.PUSH_MESSAGE_TYPE_FOOD:
                startActivity(BackgroudService.this, FoodManageActivity.class);
                break;
        }
    }

    private void startActivity(Context context, Class<?> cls) {
        Intent intent = new Intent();
        intent.setClass(context, cls);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "onDestroy");
        VoiceManager.getInstance().close();
        SerialManager.getInstance().close();
        ControlManager.getInstance().close();
        unRegister();
        if (mCompositeSubscription != null) {
            mCompositeSubscription.unsubscribe();
            mCompositeSubscription = null;
        }
        if (mClockThread != null) {
            mClockThread.interrupt();
            mClockThread = null;
        }
        if (mMediaPlayer != null) {
            mMediaPlayer.stop();
            mMediaPlayer = null;
        }
        if (mMediaPlayer1 != null) {
            mMediaPlayer1.stop();
            mMediaPlayer1 = null;
        }
        if (mHandler != null) {
            mHandler.removeCallbacksAndMessages(null);
        }
    }

    class Clockhread extends Thread {
        @Override
        public void run() {
            super.run();

            while (!isInterrupted()) {
                try {
                    Thread.sleep(1000 * 60);
                    String timeStr = mSimpleDateFormat.format(new Date());
                    try {
                        int hour = Integer.parseInt(timeStr);
                        if (hour == 0) {//00:00
                            if (!AppConfig.VIOMI_FRIDGE_V31.equals(DeviceConfig.MODEL)) {
                                UpgradeManager.getInstance().checkAppUpgrade(BackgroudService.this, UpgradeManager.OPERATE_AUTO);
                            }
                        } else if (hour == 100) {//01:00
                            //  UpgradeManager.getInstance().checkSystemUpgrade(BackgroudService.this, UpgradeManager.OPERATE_AUTO);
                        }
                        timeCount++;
                        if (timeCount > 60) {
                            timeCount = 0;
                            GlobalParams.getInstance().setFilterLifeTime(GlobalParams.getInstance().getFilterLifeTime() + 1);
                            GlobalParams.getInstance().setStartHour(GlobalParams.getInstance().getStartHour() + 1);

                            int useTime = ControlManager.getInstance().getFilterLifeUsedTime();
                            if (useTime == ControlManager.FILTER_LIFE_SUM_TIME) {
                                DeviceManager.getInstance().sendFilterLifeEnd();
                                PushMsg pushMsg = new PushMsg();
                                pushMsg.title = ViomiApplication.getContext().getString(R.string.text_filter_life_end_title);
                                pushMsg.content = ViomiApplication.getContext().getString(R.string.text_filter_life_end_desc);
                                pushMsg.type = PushMsg.PUSH_MESSAGE_TYPE_FILTER;
                                PushManager.getInstance().pushNotificationToStatusBar(pushMsg);

                                DeviceInfoMessage deviceInfoMessage = new DeviceInfoMessage();
                                deviceInfoMessage.setTitle(ViomiApplication.getContext().getString(R.string.text_filter_life_end_title));
                                deviceInfoMessage.setContent(ViomiApplication.getContext().getString(R.string.text_filter_life_end_desc));
                                deviceInfoMessage.setInfoId(InfoManager.getInstance().getLastDeviceInfoId() + 1);
                                deviceInfoMessage.setTopic(PushMsg.PUSH_MESSAGE_TYPE_FILTER);
                                deviceInfoMessage.setTime(System.currentTimeMillis() / 1000);
                                deviceInfoMessage.setDelete(false);
                                deviceInfoMessage.setRead(false);
                                InfoManager.getInstance().addDeviceInfoRecord(deviceInfoMessage);
                            } else if (useTime == ControlManager.FILTER_LIFE_SUM_TIME - 720) {
                                DeviceManager.getInstance().sendFilterLifeLow();
                                PushMsg pushMsg = new PushMsg();
                                pushMsg.title = ViomiApplication.getContext().getString(R.string.text_filter_life_low_title);
                                pushMsg.content = ViomiApplication.getContext().getString(R.string.text_filter_life_low_desc);
                                pushMsg.type = PushMsg.PUSH_MESSAGE_TYPE_FILTER;
                                PushManager.getInstance().pushNotificationToStatusBar(pushMsg);

                                DeviceInfoMessage deviceInfoMessage = new DeviceInfoMessage();
                                deviceInfoMessage.setTitle(ViomiApplication.getContext().getString(R.string.text_filter_life_low_title));
                                deviceInfoMessage.setContent(ViomiApplication.getContext().getString(R.string.text_filter_life_low_desc));
                                deviceInfoMessage.setInfoId(InfoManager.getInstance().getLastDeviceInfoId() + 1);
                                deviceInfoMessage.setTopic(PushMsg.PUSH_MESSAGE_TYPE_FILTER);
                                deviceInfoMessage.setTime(System.currentTimeMillis() / 1000);
                                deviceInfoMessage.setDelete(false);
                                deviceInfoMessage.setRead(false);
                                InfoManager.getInstance().addDeviceInfoRecord(deviceInfoMessage);
                            }
                        }
                        log.d(TAG, "###=" + GlobalParams.getInstance().getStartHour());

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    Log.e(TAG, "serial read fail!msg: " + e.getMessage());
                    e.printStackTrace();
                }
            }
        }
    }

}