package com.viomi.fridge.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.IBinder;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SoundEffectConstants;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.viomi.fridge.R;
import com.viomi.fridge.api.http.HttpConnect;
import com.viomi.fridge.common.ViomiApplication;
import com.viomi.fridge.device.AppConfig;
import com.viomi.fridge.device.DeviceConfig;
import com.viomi.fridge.model.bean.WebBaseData;
import com.viomi.fridge.util.ApkUtil;
import com.viomi.fridge.util.log;
import com.viomi.fridge.view.activity.CommonHeaderActivity;
import com.viomi.fridge.view.activity.MainNewV2Activity;
import com.viomi.fridge.view.activity.RecipesHealthyActivity;
import com.viomi.fridge.view.activity.UserSettingActivity;
import com.viomi.fridge.view.activity.VmallWebActivity;

public class FloatButtonService extends Service {

    private WindowManager windowManager;
    private LayoutInflater inflater;
    private View header_view;
    private WindowManager.LayoutParams layoutParams;
    private int mCurrentY;
    private WindowManager.LayoutParams layoutParams2;
    private boolean isShow;
    private View body_view;
    private ImageView header_arrow;
    private LinearLayout lin;


    static {
        log.myE("FloatButtonService", "Load_Class");
    }

    public FloatButtonService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        log.myE("FloatButtonService", "onCreate");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        init();
        log.myE("FloatButtonService", "init");
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        windowManager.removeViewImmediate(header_view);
        if (isShow) {
            windowManager.removeViewImmediate(body_view);
        }
    }

    private void init() {
        windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        inflater = LayoutInflater.from(this);
        createView();
    }

    private void createView() {
        header_view = inflater.inflate(R.layout.float_btn_header_layout, null);
        RelativeLayout header = (RelativeLayout) header_view.findViewById(R.id.header);
        header_arrow = (ImageView) header_view.findViewById(R.id.header_arrow);


        header.setOnTouchListener(new View.OnTouchListener() {
            private float x1;
            private float y1;
            private int yy;

            @Override
            public boolean onTouch(View v, MotionEvent event) {

//                mCurrentY = (int) event.getRawY();
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN: {
                        x1 = event.getRawX();
                        y1 = event.getRawY();
                        yy = (int) event.getY();
                        mCurrentY = (int) event.getRawY() - yy;
                        break;
                    }
                    case MotionEvent.ACTION_MOVE:
                        mCurrentY = (int) event.getRawY() - yy;
                        log.myE("mCurrentY", "-----------------------mCurrentY=" + mCurrentY);
                        updateFloatView();
                        break;
                    case MotionEvent.ACTION_UP: {
                        float x2 = event.getRawX();
                        float y2 = event.getRawY();
                        if (Math.abs(x1 - x2) < 5 && Math.abs(y1 - y2) < 5) {
                            mCurrentY = (int) event.getRawY() - (int) event.getY();
                            v.playSoundEffect(SoundEffectConstants.CLICK);
                            showMenu();
                        }
                        break;
                    }
                }
                return true;
            }
        });

        layoutParams = new WindowManager.LayoutParams();
        layoutParams.gravity = Gravity.RIGHT | Gravity.TOP;
        layoutParams.type = WindowManager.LayoutParams.TYPE_PHONE;
        layoutParams.format = PixelFormat.RGBA_8888;

        layoutParams.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;
        mCurrentY = layoutParams.y = 230;
        layoutParams.width = 103;
        layoutParams.height = 76;
        windowManager.addView(header_view, layoutParams);

        body_view = inflater.inflate(R.layout.float_btn_body_layout, null);
        lin = (LinearLayout) body_view.findViewById(R.id.lin);
        TextView btn1 = (TextView) body_view.findViewById(R.id.btn1);
        TextView btn2 = (TextView) body_view.findViewById(R.id.btn2);
        TextView btn3 = (TextView) body_view.findViewById(R.id.btn3);
        TextView btn4 = (TextView) body_view.findViewById(R.id.btn4);
        TextView btn5 = (TextView) body_view.findViewById(R.id.btn5);

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FloatButtonService.this, MainNewV2Activity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
            }
        });

        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(AppConfig.VIOMI_FRIDGE_V31.equals(DeviceConfig.MODEL)){
                    Intent intent=new Intent();
                    intent.setAction("android.intent.action.jd.smart.fridge.coolbuy.main");
                    intent.addCategory(Intent.CATEGORY_DEFAULT);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }else {
                    Intent intent = new Intent(FloatButtonService.this, VmallWebActivity.class);
                    WebBaseData data = new WebBaseData();
                    data.url = HttpConnect.STOREMAINURL + "/index.html";
                    intent.putExtra(WebBaseData.Intent_String, data);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent);
                }
            }
        });

        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(FloatButtonService.this, CommonHeaderActivity.class);
//                intent.putExtra("url", HttpConnect.COOKMENUURL);
//                intent.putExtra("title","健康菜谱");
//
//                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
//                startActivity(intent);
                Intent intent = new Intent(FloatButtonService.this, RecipesHealthyActivity.class);
                startActivity(intent);
            }
        });

        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String packageName = "com.qiyi.video.pad";
                if (ApkUtil.checkApkExist(ViomiApplication.getContext(), packageName)) {
                    Log.e("FloatButtonService","startOtherApp,packageName="+packageName);
                    ApkUtil.startOtherApp(FloatButtonService.this, packageName, false);
                }
//                Intent intent = new Intent(FloatButtonService.this, VideoUMActivity.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
//                startActivity(intent);
            }
        });

        btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FloatButtonService.this, UserSettingActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
            }
        });

        layoutParams2 = new WindowManager.LayoutParams();
        layoutParams2.gravity = Gravity.RIGHT | Gravity.TOP;
        layoutParams2.type = WindowManager.LayoutParams.TYPE_PHONE;
        layoutParams2.format = PixelFormat.RGBA_8888;
        layoutParams2.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;
        layoutParams2.width = 103;
        layoutParams2.height = 276;

    }

    private void showMenu() {
        isShow = !isShow;

        if (isShow) {
            if (mCurrentY > 448) {
                layoutParams2.y = mCurrentY - 276;
            } else {
                layoutParams2.y = mCurrentY + 76;
            }

            RotateAnimation ro = new RotateAnimation(0, 180, header_arrow.getWidth() / 2, header_arrow.getHeight() / 2);
            ro.setDuration(500);
            ro.setFillAfter(true);
            header_arrow.startAnimation(ro);

            ro.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    header_arrow.setImageResource(R.drawable.float_btn_0);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });

            windowManager.addView(body_view, layoutParams2);

        } else {
            RotateAnimation ro = new RotateAnimation(180, 0, header_arrow.getWidth() / 2, header_arrow.getHeight() / 2);
            ro.setDuration(500);
            ro.setFillAfter(true);
            header_arrow.startAnimation(ro);
            ro.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    header_arrow.setImageResource(R.drawable.float_btn_1);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });


            windowManager.removeViewImmediate(body_view);
        }

    }

    private void updateFloatView() {
        layoutParams.y = mCurrentY;
        windowManager.updateViewLayout(header_view, layoutParams);

        if (isShow) {
            if (mCurrentY > 448) {
                if (mCurrentY > 800-76) {
                    layoutParams2.y = 800-276-76;
                    windowManager.updateViewLayout(body_view, layoutParams2);
                    return;
                }
                layoutParams2.y = mCurrentY - 276;
            } else {
                if (mCurrentY < 0) {
                    layoutParams2.y = 76;
                    windowManager.updateViewLayout(body_view, layoutParams2);
                    return;
                }
                layoutParams2.y = mCurrentY + 76;
            }
            windowManager.updateViewLayout(body_view, layoutParams2);
        }
    }
}
