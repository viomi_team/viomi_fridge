package com.viomi.fridge.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PixelFormat;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.PowerManager;
import android.os.SystemClock;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.viomi.fridge.R;
import com.viomi.fridge.common.GlobalParams;
import com.viomi.fridge.device.DeviceManager;
import com.viomi.fridge.util.log;
import com.viomi.fridge.view.activity.Timer2Activity;

import java.util.ArrayList;
import java.util.List;


public class TimerService extends Service {

    private MyHandler mhandler;
    private int setTime;
    private int currentTimer;
    private TimerThread timerThread;

    private int track0;
    private int track1;
    private int track2;
    private int track3;
    private int track4;
    private int track5;
    private int currentTrack;
    private SoundPool soundPool;
    private SharedPreferences sp;
    private List<Integer> trackList;
    private int currentStream = -1;
    private int currentPosition;
    private WindowManager windowManager;
    private WindowManager.LayoutParams layoutParams;
    private View view;

    @Override
    public IBinder onBind(Intent intent) {
        log.myE("TimerService", "onBind");
        return new MyIBinder();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        log.myE("TimerService", "onUnbind");
        return super.onUnbind(intent);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        initSoundPool();
        mhandler = new MyHandler();
        windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        creatView();
        log.myE("TimerService", "onCreate");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        log.myE("TimerService", "onStartCommand");
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mhandler.removeCallbacksAndMessages(null);
        if (soundPool != null) {
            soundPool.release();
        }
        log.myE("TimerService", "onDestroy");
    }

    //开始计时
    public void startTimer(int setTime) {
        this.setTime = setTime;
        timerThread = new TimerThread();
        timerThread.start();
        log.myE("TimerService", "startTimer");
        GlobalParams.Clock_Start = true;
    }

    //暂停计时
    public void puaseTimer() {
        if (timerThread != null) {
            timerThread.interrupt();
            log.myE("TimerService", "puaseTimer");
        }
    }

    //取消计时
    public void cancelTimer() {
        if (timerThread != null) {
            timerThread.interrupt();
            log.myE("TimerService", "cancelTimer");
        }
        GlobalParams.Clock_Start = false;
    }

    //获取计时时间
    public int getTime() {
        log.myE("TimerService", "getTime");
        return currentTimer;
    }

    //播放声音
    public void playVioce(int position) {
        currentPosition = position;
        if (currentStream != -1) {
            soundPool.stop(currentStream);
        }
        currentStream = soundPool.play(trackList.get(position), 1, 1, 1, 0, 1);
    }

    //保存设置
    public void saveVoiceSetting() {
        currentTrack = trackList.get(currentPosition);
        SharedPreferences.Editor edit = sp.edit();
        edit.putInt("ringmodel", currentPosition);
        edit.commit();
    }

    //初始化声音池
    private void initSoundPool() {
        if (Build.VERSION.SDK_INT > 21) {
            SoundPool.Builder builder = new SoundPool.Builder();
            //并发流数量
            builder.setMaxStreams(3);
            AudioAttributes.Builder attributesBuilder = new AudioAttributes.Builder();
            attributesBuilder.setLegacyStreamType(AudioManager.STREAM_MUSIC);
            builder.setAudioAttributes(attributesBuilder.build());
            soundPool = builder.build();
        } else {
            soundPool = new SoundPool(3, AudioManager.STREAM_MUSIC, 1);
        }

        track1 = soundPool.load(this, R.raw.ok, 1);
        track2 = soundPool.load(this, R.raw.ok2, 1);
        track3 = soundPool.load(this, R.raw.ok3, 1);
        track4 = soundPool.load(this, R.raw.ok4, 1);
        track5 = soundPool.load(this, R.raw.ok5, 1);

        trackList = new ArrayList<>();
        trackList.add(track1);
        trackList.add(track2);
        trackList.add(track3);
        trackList.add(track4);
        trackList.add(track5);

        sp = getSharedPreferences("ring", MODE_PRIVATE);
        int selectPosition = sp.getInt("ringmodel", 0);

        currentTrack = trackList.get(selectPosition);
    }


    public class MyIBinder extends Binder {
        public TimerService getTimerService() {
            return TimerService.this;
        }
    }

    private class MyHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0: {
                    currentTimer = (int) msg.obj;
                    Intent broadIntent = new Intent("timerservice_timerbroadcast");
                    broadIntent.putExtra("currentTimer", currentTimer);
                    sendBroadcast(broadIntent);
                    if (currentTimer == 0) {
                        GlobalParams.Clock_Start = false;
                        DeviceManager.getInstance().sendTimerEnd(setTime);
                        showView();

                        PowerManager pm = (PowerManager) getApplicationContext().getSystemService(Context.POWER_SERVICE);
                        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.SCREEN_BRIGHT_WAKE_LOCK, "bright");
                        wl.acquire();
                        wl.release();
                    }

                    break;
                }

                case 1: {
                    soundPool.play(currentTrack, 1, 1, 1, 0, 1);
                    break;
                }

                case 2: {
                    break;
                }
            }
        }
    }

    private class TimerThread extends Thread {
        @Override
        public void run() {
            for (int i = setTime; i >= 0; i--) {
                if (isInterrupted()) {
                    return;
                }
                Message msg = mhandler.obtainMessage();
                msg.what = 0;
                msg.obj = i;
                mhandler.sendMessage(msg);
                if (!(i == 0)) {
                    SystemClock.sleep(1000);
                }
            }
            Message msg = mhandler.obtainMessage();
            msg.what = 1;
            mhandler.sendMessage(msg);
        }
    }

    private void showView() {
        windowManager.addView(view, layoutParams);
    }

    private void dismissView() {
        windowManager.removeViewImmediate(view);
    }

    private void creatView() {
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        view = layoutInflater.inflate(R.layout.timer_finish_layout, null);

        RelativeLayout dialog_layout = (RelativeLayout) view.findViewById(R.id.dialog_layout);
        LinearLayout content_layout = (LinearLayout) view.findViewById(R.id.content_layout);
        TextView timer_again = (TextView) view.findViewById(R.id.timer_again);
        TextView know_it = (TextView) view.findViewById(R.id.know_it);

        dialog_layout.setOnClickListener(v -> {
            dismissView();
        });

        content_layout.setOnTouchListener((v, e) -> {
            return true;
        });

        timer_again.setOnClickListener(v -> {
            dismissView();
            Intent intent = new Intent(TimerService.this, Timer2Activity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        });
        know_it.setOnClickListener(v -> {
            dismissView();
        });

        layoutParams = new WindowManager.LayoutParams();
        layoutParams.gravity = Gravity.LEFT;
        layoutParams.type = WindowManager.LayoutParams.TYPE_PHONE;
        layoutParams.format = PixelFormat.RGBA_8888;
        layoutParams.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;
        layoutParams.width = 1280;
        layoutParams.height = 800;
    }

}
