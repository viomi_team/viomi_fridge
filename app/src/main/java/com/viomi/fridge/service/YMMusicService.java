package com.viomi.fridge.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;

import com.viomi.fridge.R;

import java.lang.ref.WeakReference;

public class YMMusicService extends Service {

    private WindowManager mWindowManager;
    private View view;
    private MediaPlayer mediaPlayer;
    private boolean isStop = true;
    private Button play;
    private SeekBar seekBar;
    private MyHandler mhandler;
    private ImageView rotate_cd;

    public static class MyHandler extends Handler {
        private WeakReference<YMMusicService> weakReference;

        public MyHandler(YMMusicService service) {
            weakReference = new WeakReference<YMMusicService>(service);
        }

        @Override
        public void handleMessage(Message msg) {
            YMMusicService service = weakReference.get();

            switch (msg.what) {
                case 0:

                    int position = service.mediaPlayer.getCurrentPosition();

                    int time = service.mediaPlayer.getDuration();
                    int max = service.seekBar.getMax();
                    service.seekBar.setProgress(position * max / time);

                    service.mhandler.sendEmptyMessageDelayed(0, 1000);

                    break;
            }
        }
    }


    public YMMusicService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }


    @Override
    public void onCreate() {
        super.onCreate();

        if (mediaPlayer == null) {
            mediaPlayer = new MediaPlayer();
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        initView();
        return super.onStartCommand(intent, flags, startId);
    }

    private void initView() {


        mWindowManager = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);

        view = LayoutInflater.from(this).inflate(R.layout.ymmusic_play_layout, null);
        WindowManager.LayoutParams mLayoutParams = new WindowManager.LayoutParams();
        //设置View默认的摆放位置
        mLayoutParams.gravity = Gravity.LEFT | Gravity.TOP;
        //设置window type
        mLayoutParams.type = WindowManager.LayoutParams.TYPE_PHONE;//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        //设置背景为透明
        mLayoutParams.format = PixelFormat.RGBA_8888;
        //注意该属性的设置很重要，FLAG_NOT_FOCUSABLE使浮动窗口不获取焦点,若不设置该属性，屏幕的其它位置点击无效，应为它们无法获取焦点
        mLayoutParams.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;
        //设置视图的宽、高
        mLayoutParams.width = 1280;
        mLayoutParams.height = 800;
        //将视图添加到Window中
        mWindowManager.addView(view, mLayoutParams);


        ImageView dismiss = (ImageView) view.findViewById(R.id.dismiss);

        ImageView forward = (ImageView) view.findViewById(R.id.forward);
//        ImageView play = (ImageView) view.findViewById(R.id.play);
        play = (Button) view.findViewById(R.id.play);
        ImageView next = (ImageView) view.findViewById(R.id.next);
        seekBar = (SeekBar) view.findViewById(R.id.seekBar);

        rotate_cd = (ImageView) view.findViewById(R.id.rotate_cd);

        mhandler = new MyHandler(this);

        dismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopSelf();
            }
        });

        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (play.isSelected()) {
                    mediaPlayer.pause();
                    mhandler.removeMessages(0);
                    rotate_cd.clearAnimation();

                } else {

                    mediaPlayer.start();
                    mhandler.sendEmptyMessage(0);

                    RotateAnimation ra = new RotateAnimation(0, 359, RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f);
                    //设置旋转次数
                    ra.setRepeatCount(RotateAnimation.INFINITE);
                    //设置重复旋转的模式
//        ra.setRepeatMode(RotateAnimation.REVERSE);
                    ra.setDuration(3000);
                    ra.setInterpolator(new LinearInterpolator());
                    rotate_cd.startAnimation(ra);
                }
                play.setSelected(!play.isSelected());
            }
        });

        seekBar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });


        preparePlayMusic();
//        play.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (isStop) {
//                    preparePlayMusic();
//                    isStop = false;
//                } else if (mediaPlayer != null && !mediaPlayer.isPlaying()) {
//                    mediaPlayer.start();
//                }
//            }
//        });

    }

    private void preparePlayMusic() {
        //重置MediaPlayer
        mediaPlayer.reset();
        //设置播放数据源
        mediaPlayer = MediaPlayer.create(this, R.raw.music);
        //设置是否循环播放
        mediaPlayer.setLooping(false);

        //当音乐播放完成时调用
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                //播放完成之后
                isStop = true;
                play.setSelected(false);
                mhandler.removeMessages(0);
                rotate_cd.clearAnimation();
            }
        });

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        mWindowManager.removeView(view);
        if (mhandler!=null) {
            mhandler.removeCallbacksAndMessages(null);
        }
        if (mediaPlayer != null) {
            mediaPlayer.release();
        }
    }
}
