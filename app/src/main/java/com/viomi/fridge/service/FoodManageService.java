package com.viomi.fridge.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Binder;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.Nullable;

import com.viomi.fridge.R;
import com.viomi.fridge.api.dao.DeviceInfoMessage;
import com.viomi.fridge.api.dao.InfoManager;
import com.viomi.fridge.common.ViomiApplication;
import com.viomi.fridge.device.AppConfig;
import com.viomi.fridge.device.DeviceManager;
import com.viomi.fridge.manager.PushManager;
import com.viomi.fridge.model.bean.FoodDetailData;
import com.viomi.fridge.model.bean.PushMsg;
import com.viomi.fridge.util.log;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

/**
 * 食材管理后台 Service
 * Created by William on 2017/6/27.
 */

public class FoodManageService extends Service {

    private static final String TAG = FoodManageService.class.getSimpleName();
    private static final String Path = Environment.getExternalStorageDirectory().toString() + "/viomi/" + "food_manage.xml";
    private static final int MSG_LOAD_DATA_SUCCESS = 1;
    private static final int MSG_LOAD_DATA_FAIL = 2;
    private List<FoodDetailData> mList = new ArrayList<>();
    private MyHandler mHandler;
    private LoadRunnable mLoadRunnable;
    private MyBinder mBinder = new MyBinder();

    @Override
    public void onCreate() {
        super.onCreate();
        log.d(TAG, "onCreate");
        mHandler = new MyHandler(this);
        mLoadRunnable = new LoadRunnable(this);
        mHandler.post(mLoadRunnable);
        registerReceiver();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        log.d(TAG, "onStartCommand");
        return super.onStartCommand(intent, flags, startId);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        log.d(TAG, "onBind");
        return mBinder;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mHandler.removeCallbacksAndMessages(null);
        if (mHandler != null) mHandler = null;
        if (mLoadRunnable != null) mLoadRunnable = null;
        if (mList != null) mList = null;
        unregisterReceiver(mReceiver);
    }

    private void registerReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(AppConfig.ACTION_FOOD_MANAGE_UPDATE);
//        intentFilter.addAction(Intent.ACTION_TIME_TICK);
        registerReceiver(mReceiver, intentFilter);
    }

    private void loadData() {
        mList.clear();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            if (!new File(Path).exists()) return;

            FileInputStream fileIS = new FileInputStream(Path);
            Document dom = builder.parse(fileIS);
            dom.normalize();

            Element root = dom.getDocumentElement();
            NodeList items = root.getElementsByTagName("food");   // 查找所有 food 节点

            for (int i = 0; i < items.getLength(); i++) {
                FoodDetailData data = new FoodDetailData();
                // 得到第一个 food 节点
                Element personNode = (Element) items.item(i);
                // 获取 food 节点下的所有子节点
                NodeList childNodes = personNode.getChildNodes();

                for (int j = 0; j < childNodes.getLength(); j++) {
                    Node node = childNodes.item(j); // 判断是否为元素类型
                    if (node.getNodeType() == Node.ELEMENT_NODE) {
                        Element childNode = (Element) node;
                        if ("type".equals(childNode.getNodeName())) {   // 类型
                            data.type = childNode.getFirstChild().getNodeValue();
                        } else if ("name".equals(childNode.getNodeName())) {    // 食材名称
                            data.name = childNode.getFirstChild().getNodeValue();
                        } else if ("time".equals(childNode.getNodeName())) {    // 添加时间
                            data.addTime = childNode.getFirstChild().getNodeValue();
                        } else if ("path".equals(childNode.getNodeName())) {    // 图片保存路径
                            data.imgPath = childNode.getFirstChild().getNodeValue();
                        } else if ("deadline".equals(childNode.getNodeName())) {    // 到期时间
                            data.deadLine = childNode.getFirstChild().getNodeValue();
                        } else if ("push".equals(childNode.getNodeName())) {     // 是否已推送
                            data.isPush = childNode.getFirstChild().getNodeValue();
                        }
                    }
                }
                mList.add(data);
            }
            fileIS.close();
            mHandler.sendEmptyMessage(MSG_LOAD_DATA_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            log.d(TAG, e.toString());
            mHandler.sendEmptyMessage(MSG_LOAD_DATA_FAIL);
        }
    }

    /**
     * 计算过期时间
     */
    private void calculate() {
        long current = System.currentTimeMillis();
        for (int i = 0; i < mList.size(); i++) {
            FoodDetailData data = mList.get(i);
            if (data.deadLine.equals("可选填")) continue;
            long deadline = Long.valueOf(data.deadLine) + 24L * 60L * 60L * 1000L;
            if (current >= deadline && data.isPush.equals("false")) {
                data.isPush = "true";
                mList.set(i, data);
                long startTime = Long.valueOf(data.addTime) / 1000;
                int endTime = (int) (deadline / 1000);
                push(data.name, startTime, endTime, Integer.valueOf(data.type));
                ModifyRunnable modifyRunnable = new ModifyRunnable(this, data.addTime);
                mHandler.post(modifyRunnable);
            }
        }
    }

    /**
     * 食材过期推送
     *
     * @param name：食材名称
     * @param startTime：开始时间（秒）
     * @param endTime：过期时间（秒）
     * @param room：0.冷藏区,1.变温区,2.冷冻区
     */
    private void push(String name, long startTime, int endTime, int room) {
        DeviceManager.getInstance().sendFoodExpire(name, startTime, endTime, room);
        DeviceInfoMessage deviceInfoMessage = new DeviceInfoMessage();
        String roomStr = "";
        if (room == 0) {
            roomStr = ViomiApplication.getContext().getString(R.string.text_cold_closet);
        } else if (room == 1) {
            roomStr = ViomiApplication.getContext().getString(R.string.text_temp_changeable_room);
        } else if (room == 2) {
            roomStr = ViomiApplication.getContext().getString(R.string.text_freezing_room);
        }
        deviceInfoMessage.setTitle(name + ViomiApplication.getContext().getString(R.string.text_food_expend));
        deviceInfoMessage.setContent(ViomiApplication.getContext().getString(R.string.text_food_expend_desc) + roomStr);
        deviceInfoMessage.setInfoId(InfoManager.getInstance().getLastDeviceInfoId() + 1);
        deviceInfoMessage.setTopic(PushMsg.PUSH_MESSAGE_TYPE_FOOD);
        deviceInfoMessage.setTime(System.currentTimeMillis() / 1000);
        deviceInfoMessage.setDelete(false);
        deviceInfoMessage.setRead(false);
        InfoManager.getInstance().addDeviceInfoRecord(deviceInfoMessage);

        PushMsg pushMsg = new PushMsg();
        pushMsg.title = name + ViomiApplication.getContext().getString(R.string.text_food_expend);
        pushMsg.content = ViomiApplication.getContext().getString(R.string.text_food_expend_info_desc);
        pushMsg.type=PushMsg.PUSH_MESSAGE_TYPE_FOOD;
        PushManager.getInstance().pushNotificationToStatusBar(pushMsg);

    }

    // 发送推送后移除推送
    private void removePush(String id) {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder db = dbFactory.newDocumentBuilder();
            Document dom = db.parse(new File(Path));
            Element root = (Element) dom.getFirstChild();
            NodeList userList = root.getElementsByTagName("food");
            for (int i = 0; i < userList.getLength(); i++) {
                Element userUpdate = (Element) userList.item(i);

                // 获取 food 节点下的所有子节点
                NodeList childNodes = userUpdate.getChildNodes();
                Node node = childNodes.item(5);
                Element childNode = (Element) node;
                if (childNode.getFirstChild().getTextContent().equals(id)) {
                    Node node1 = childNodes.item(11); // 判断是否为元素类型
                    Element childNode1 = (Element) node1;
                    childNode1.setTextContent("true");
                }
            }

            // 从内存写入硬盘
            TransformerFactory tfFactory = TransformerFactory.newInstance();
            Transformer tf = tfFactory.newTransformer();
            tf.setOutputProperty(OutputKeys.ENCODING, "UTF-8");    // 编码方式
            tf.setOutputProperty(OutputKeys.INDENT, "yes");    // 是否缩进
            tf.transform(new DOMSource(dom), new StreamResult(new File(Path)));
            log.d(TAG, "remove push success");
        } catch (ParserConfigurationException | IOException | SAXException | TransformerException e) {
            e.printStackTrace();
        }
    }

    private static final class MyHandler extends Handler {

        private WeakReference<FoodManageService> weakReference;
        private FoodManageService service;

        MyHandler(FoodManageService service) {
            weakReference = new WeakReference<>(service);
            this.service = weakReference.get();
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (service != null) {
                switch (msg.what) {
                    case MSG_LOAD_DATA_SUCCESS:     // 读取数据成功
                        log.d(TAG, "load data success");
                        Intent intent = new Intent();
                        intent.setAction(AppConfig.ACTION_FOOD_MANAGE_SEND_DATA);
                        intent.putExtra("food_list", (Serializable) service.mList);
                        service.sendBroadcast(intent);
                        service.mHandler.postDelayed(service.mLoadRunnable, 1000 * 60);
                        service.calculate();
                        break;
                    case MSG_LOAD_DATA_FAIL:    // 读取数据失败
                        service.mHandler.post(service.mLoadRunnable);
                        break;
                }
            }
        }
    }

    public List<FoodDetailData> getData() {
        return mList;
    }

    public class MyBinder extends Binder {

        public FoodManageService getService() {
            return FoodManageService.this;
        }
    }

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            log.d(TAG, intent.getAction());
            if (intent.getAction().equals(AppConfig.ACTION_FOOD_MANAGE_UPDATE)) {
                mHandler.removeCallbacks(mLoadRunnable);
                mHandler.post(mLoadRunnable);
            }
//            else if (intent.getAction().equals(Intent.ACTION_TIME_TICK)) {    // 每分钟监听
//                long current = System.currentTimeMillis();
//                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
//                String time = simpleDateFormat.format(new Date(current));
//                String[] array = time.split(" ");
//                log.d(TAG, array[1] + "");
//                if (array[1].equals("00:00")) {     // 一天计算 1 次
//                    mHandler.removeCallbacks(mLoadRunnable);
//                    mHandler.post(mLoadRunnable);
//                }
//            }
        }
    };

    private static final class LoadRunnable implements Runnable {

        private WeakReference<FoodManageService> weakReference;
        private FoodManageService service;

        LoadRunnable(FoodManageService service) {
            weakReference = new WeakReference<>(service);
            this.service = weakReference.get();
        }

        @Override
        public void run() {
            if (service != null) {
                service.loadData();
            }
        }
    }

    private static final class ModifyRunnable implements Runnable {

        private WeakReference<FoodManageService> weakReference;
        private FoodManageService service;
        private String id;

        ModifyRunnable(FoodManageService service, String id) {
            weakReference = new WeakReference<>(service);
            this.service = weakReference.get();
            this.id = id;
        }

        @Override
        public void run() {
            if (service != null) {
                service.removePush(id);
            }
        }
    }

}
