package com.viomi.fridge.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.viomi.fridge.broadcast.BroadcastAction;
import com.viomi.fridge.manager.PushManager;
import com.viomi.fridge.util.log;


/**
 * Created by young2 on 2017/7/7.
 */

public class InfoPushService extends Service {

    private final static String TAG=InfoPushService.class.getSimpleName();
    private MyBinder mBinder = new MyBinder();
    private BraodcastReceiver mBraodcastReceiver;

    public class MyBinder extends Binder {
        public InfoPushService getService(){
            return InfoPushService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.e(TAG,"onCreate");
        init();
    }

    private void init(){
        PushManager.getInstance().init(this);
        register();
        PushManager.getInstance().setAcceptTime();
        PushManager.getInstance().advertPushSet();
        PushManager.getInstance().userPushSet();
        PushManager.getInstance().singleUpgradePushSet();
    }

    private void register() {

        mBraodcastReceiver = new BraodcastReceiver(this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BroadcastAction.ACTION_REPORT_PUSH_DEVICE);
        intentFilter.addAction(BroadcastAction.ACTION_REPORT_PUSH_USER);
        intentFilter.addAction(BroadcastAction.ACTION_REPORT_PUSH_ADVERT);
        intentFilter.addAction(BroadcastAction.ACTION_REPORT_PUSH_SET_TIME);
        registerReceiver(mBraodcastReceiver, intentFilter);

    }

    private void unRegister() {
        unregisterReceiver(mBraodcastReceiver);
    }

    public static class BraodcastReceiver extends BroadcastReceiver {
        private InfoPushService mService;

        public BraodcastReceiver() {
        }

        public BraodcastReceiver(InfoPushService service) {
            mService = service;
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getAction()) {
                case BroadcastAction.ACTION_REPORT_PUSH_DEVICE:
                    String userId=intent.getStringExtra(BroadcastAction.EXTRA_PUSH);
                    log.d(TAG,"ACTION_REPORT_PUSH_DEVICE,userId="+userId);
               //     PushManager.getInstance().devicePushSet(userId);
                    break;

                case BroadcastAction.ACTION_REPORT_PUSH_USER:
                   log.d(TAG,"ACTION_REPORT_PUSH_USER");
                    PushManager.getInstance().userPushSet();
                    break;

                case BroadcastAction.ACTION_REPORT_PUSH_ADVERT:
                    log.d(TAG,"ACTION_REPORT_PUSH_ADVERT");
                    PushManager.getInstance().advertPushSet();
                    break;

                case BroadcastAction.ACTION_REPORT_PUSH_SET_TIME:
                    log.d(TAG,"ACTION_REPORT_PUSH_SET_TIME");
                    PushManager.getInstance().setAcceptTime();
                    break;
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unRegister();
        PushManager.getInstance().close();
    }

}
